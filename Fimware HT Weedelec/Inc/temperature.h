/**
*******************************************************************************
 *
 *  IRSTEA - WeedElec
 *
 *  Firmware du module de pilotage de la tete haute tension
 *
 *  Autor		: ALCIOM
 *  Version		: 1.01
 * 	FileName    : température.h
 *	Date    	: 18/12/18
 * 	Brief   	: This file provides code for configuration and use of temperature sensor TMP112
 *
 ******************************************************************************/

#define TMP112_I2C_ADD	0x48
#define TEMP_REG_ADD	0x00
#define DEG_PER_COUNT 	0.625


int16_t Read_temperature(void);



