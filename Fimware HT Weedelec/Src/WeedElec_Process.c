/********************************************************************************
 *
 *  IRSTEA - WeedElec
 *
 *  Firmware du module de pilotage de la tete haute tension
 *
 *  Autor		: ALCIOM
 *  Version		: 1.01
 * 	FileName    : WeedElec_Process.h
 *	Date    	: 18/12/18
 * 	Brief   	: This source file defines process and message of communication protocole
 *      of Weedelec board
 *
 ******************************************************************************/
//==============================================================================
// Includes
//==============================================================================

//------------------------------------------------------------------------------
//	System includes
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//	Projects includes
//------------------------------------------------------------------------------
#include "stm32l4xx_hal.h"
#include "WeedElec_Process.h"
#include "WeedElec_CmdInterpretor.h"
#include "WeedElec_ComProtocole.h"
#include "tim.h"
#include "adc.h"
#include <string.h>
extern UART_HandleTypeDef huart1;
//==============================================================================
//	Configuration bits
//==============================================================================



//==============================================================================
// Global variables
//==============================================================================
uint8_t state=IDLE;
uint8_t TransferMode=0;
extern uint16_t cnt_shot,pulse_index;
extern uint8_t PWMEndFlag;

uint16_t Buffer0[BUF_SIZE] = { 0 };
uint16_t Buffer1[BUF_SIZE] = { 0 };
uint8_t TransmitBuffer[2*BUF_SIZE];
uint16_t Buffer_index,FirstPeriodIndex;
uint8_t Current_filled_buffer;
uint16_t FirstPeriod[20000] = { 0 };
uint8_t ADCchannel;
uint8_t OverCurrentFlag;
uint16_t current_limit;
uint16_t max_current;
uint16_t nb_sample_period;

uint32_t length_Spark=0;
uint32_t SparkVoltage=0;
uint32_t Batvoltage=0;
//==============================================================================
// Interrupts handlers
//==============================================================================



//==============================================================================
// void Adc_measurement_process
//
// Intputs: None
// Outputs: None
// Return : None
// Overview: function called for each ADC1 conversion interrupt.
//
//==============================================================================
void Adc_measurement_process(void)
{
	int32_t adc_val;


	if(ADCchannel==VOLTAGE_CHANNEL) // First interrupt = Voltage measurement
	{
		adc_val=HAL_ADC_GetValue(&hadc1)*VOLTAGE_RATIO;
		ADCchannel=CURRENT_CHANNEL;
	}
	else // second interrupt = Current measurement
	{
		adc_val=(int16_t)HAL_ADC_GetValue(&hadc1)*CURRENT_RATIO;

		// suppression of negative values due to offset calibration
		if(adc_val<0)
			adc_val=0;

		ADCchannel=VOLTAGE_CHANNEL;

		// Storage of maximum current of the sequence
		if(adc_val>max_current)
			max_current=adc_val;

		// current limitation function activated if current_limit >0
		if(current_limit!=0)
		{
			if(adc_val > current_limit)
			{
				if(OverCurrentFlag==0)
				{
					//force PWM output to 0 if adc_val>current_limit
					PWM_Stop();
					OverCurrentFlag=1;
				}
			}
		}

	}


	// If DEBUG mode activated (command <S01>)
	// Adc values stored in temporary double UART buffer1 and buffer0
	if(TransferMode==DEBUG_MODE)
	{
		if(Current_filled_buffer==BUFFER0)
		{
			Buffer0[Buffer_index++]=adc_val;
			//if temp buffer0 is full, switch to temp buffer1 and ask for buffer0 transmission
			if(Buffer_index==BUF_SIZE)
			{
				Current_filled_buffer=BUFFER1;
				state=TRANSMIT_BUFFER;
				Buffer_index=0;
				if(PWMEndFlag)
					HAL_ADC_Stop_IT(&hadc1);
			}
		}
		else
		{
			Buffer1[Buffer_index++]=adc_val;
			//if temp buffer1 is full, switch to temp buffer0 and ask for buffer1 transmission
			if(Buffer_index==BUF_SIZE)
			{
				Current_filled_buffer=BUFFER0;
				state=TRANSMIT_BUFFER;
				Buffer_index=0;
				if(PWMEndFlag)
					HAL_ADC_Stop_IT(&hadc1);
			}
		}
	}
	else // If normal mode activated (command <S00>)
	{
		// store only first period voltage and current values in "FirstPeriod" buffer
		if(FirstPeriodIndex<nb_sample_period)
		{
			FirstPeriod[FirstPeriodIndex++]=adc_val;
		}
		else
		{
			HAL_ADC_Stop_IT(&hadc1);
		}
	}
}

//==============================================================================
// void WeedElec_StateMachine(void)
//
// Intputs:  None
// Outputs: None
// Return : None
// Overview: Weedelec application state machine
//
//==============================================================================
void WeedElec_StateMachine(void)
{
	switch (state)
	{
	//***************************************************************************************************************/
	//*   IDLE : wait for command from UART interface
	//***************************************************************************************************************/
		case IDLE:
			CmdInterpretor();
			break;

	//***************************************************************************************************************/
	//*   SEQUENCE_RUNNING : UART interface busy, wait for end of sequence
	//***************************************************************************************************************/
		case SEQUENCE_RUNNING:
			CmdInterpretor();

			// End of sequence
			if(PWMEndFlag)
			{
				if(TransferMode==NORMAL_MODE)
				{
					HAL_Delay(50);
					//First period waveform analysis to extract spark informations
					Check_spark();
					// Transmit spark informations (message Sxxxxxxxxx)
					SendSequenceInfo();
				}
				state=IDLE;
			}
			break;

	//***************************************************************************************************************/
	//*   START_SEQUENCE : initialize and stard new sequence
	//***************************************************************************************************************/
		case START_SEQUENCE:

			//reset temp buffer index
			if(TransferMode==DEBUG_MODE)
				Buffer_index=0;
			else
				FirstPeriodIndex=0;

			//reset Overcurrent detection flag
		  	OverCurrentFlag=0;
		  	//reset max current value
		  	max_current=0;
		  	// First interrupt is Voltage value (ch1)
		    ADCchannel=VOLTAGE_CHANNEL;
		    // Start ADC
		  	HAL_ADC_Start_IT(&hadc1);
		  	// Start PWM sequence
			PWM_Start();

			state=SEQUENCE_RUNNING;
			break;

	//***************************************************************************************************************/
	//*   TRANSMIT_BUFFER : Transmit through UART interface temp buffer in DEBUG mode while sequence running
	//***************************************************************************************************************/
		case TRANSMIT_BUFFER:
			Send_buffer();
			state=SEQUENCE_RUNNING;
			break;

	//***************************************************************************************************************/
	//*   TRANSMIT_FIRST_PERIOD : Transmit through UART interface first period buffer when normal mode are used (command X)
	//***************************************************************************************************************/
		case TRANSMIT_FIRST_PERIOD:
			Send_FirstPeriod();
			state=IDLE;
			break;

		default:
			break;
	}

}

//==============================================================================
// void Send_buffer(void)
//
// Intputs:  None
// Outputs: None
// Return : None
// Overview: Transmit through UART interface temp buffer in DEBUG mode while sequence running
//
//==============================================================================
void Send_buffer(void)
{
	uint16_t i;
	if(Current_filled_buffer==BUFFER0)
	{
		//Convert 16 bits buffer in 2 byte buffer
		for(i=0;i<BUF_SIZE;i++)
		{
			TransmitBuffer[2*i]=Buffer1[i]>>8;
			TransmitBuffer[(2*i)+1]=Buffer1[i];
		}
	}
	else
	{
		//Convert 16 bits buffer in 2 byte buffer
		for(i=0;i<BUF_SIZE;i++)
		{
			TransmitBuffer[2*i]=Buffer0[i]>>8;
			TransmitBuffer[(2*i)+1]=Buffer0[i];
		}
	}
	HAL_UART_Transmit_IT(&huart1,(uint8_t *)&TransmitBuffer,BUF_SIZE*2);
}

//==============================================================================
// void Send_FirstPeriod(void)
//
// Intputs:  None
// Outputs: None
// Return : None
// Overview:Transmit through UART interface first period buffer when normal mode are used (command X)
//
//==============================================================================
void Send_FirstPeriod(void)
{
	uint16_t i,nbsampletosend,temp_index=0;
	nbsampletosend=FirstPeriodIndex;
	while(nbsampletosend>BUF_SIZE)
	{
		//Convert 16 bits buffer in 2 byte buffer
		for(i=0;i<BUF_SIZE;i++)
		{
			TransmitBuffer[2*i]=FirstPeriod[temp_index]>>8;
			TransmitBuffer[(2*i)+1]=FirstPeriod[temp_index];
			temp_index++;
			nbsampletosend--;
		}
		HAL_UART_Transmit(&huart1,(uint8_t *)&TransmitBuffer,BUF_SIZE*2,0xFFFF);
	}
	if(nbsampletosend>0)
	{
		//Convert 16 bits buffer in 2 byte buffer
		for(i=0;i<BUF_SIZE;i++)
		{
			TransmitBuffer[2*i]=FirstPeriod[temp_index]>>8;
			TransmitBuffer[(2*i)+1]=FirstPeriod[temp_index];
			temp_index++;
		}
		HAL_UART_Transmit(&huart1,(uint8_t *)&TransmitBuffer,nbsampletosend*2,0xFFFF);
	}

}





//==============================================================================
// uint16_t Read_Analog_input(void)
//
// Intputs:  None
// Outputs: uint16_t value of analog input in mV
// Return : None
// Overview:Allow to read analog input of WeedElec board
//
//==============================================================================
uint16_t Read_Analog_input(void)
{
	uint16_t tempval=0;
	HAL_ADC_Start(&hadc2);
	HAL_ADC_PollForConversion(&hadc2, 100);
	tempval=HAL_ADC_GetValue(&hadc2);
	HAL_ADC_Stop(&hadc2);
	return tempval*ANALOG_INPUT_RATIO;
}

//==============================================================================
// void Set_Digital_Output(void)
//
// Intputs:  None
// Outputs: Non
// Return : None
// Overview: Set digital output of WeedElec board
//
//==============================================================================
void Set_Digital_Output(void)
{
	HAL_GPIO_WritePin(Digital_Output_GPIO_Port, Digital_Output_Pin, GPIO_PIN_SET);
}

//==============================================================================
// void Reset_Digital_Output(void)
//
// Intputs:  None
// Outputs: Non
// Return : None
// Overview: reset digital output of WeedElec board
//
//==============================================================================
void Reset_Digital_Output(void)
{
	HAL_GPIO_WritePin(Digital_Output_GPIO_Port, Digital_Output_Pin, GPIO_PIN_RESET);
}


//==============================================================================
// uint8_t	Read_Digital_Input1(void)
//
// Intputs:  None
// Outputs: uint8_t state of digital input 1 (0x00 or 0x01)
// Return : None
// Overview: allow to read state of digital input 1 of WeedElec board
//
//==============================================================================
uint8_t	Read_Digital_Input1(void)
{
	return HAL_GPIO_ReadPin(Digital_Input1_GPIO_Port, Digital_Input1_Pin);
}

//==============================================================================
// uint8_t	Read_Digital_Input2(void)
//
// Intputs:  None
// Outputs: uint8_t state of digital input 2 (0x00 or 0x01)
// Return : None
// Overview: allow to read state of digital input 2 of WeedElec board
//
//==============================================================================
uint8_t	Read_Digital_Input2(void)
{
	return HAL_GPIO_ReadPin(Digital_Input2_GPIO_Port, Digital_Input2_Pin);
}


//==============================================================================
// uint16_t Read_Battery_Voltage(void)
//
// Intputs:  None
// Outputs: uint16_t value of battery voltage in 0.1V
// Return : None
// Overview: allow to read battery level of WeedElec board
//
//==============================================================================
uint16_t Read_Battery_Voltage(void)
{
	uint16_t i;
	uint32_t tempval=0;

	HAL_ADC_Start(&hadc1);
	for(i=0;i<30;i++)
	{
			HAL_ADC_PollForConversion(&hadc1, 100);
			tempval=tempval+HAL_ADC_GetValue(&hadc1);
			HAL_ADC_PollForConversion(&hadc1, 100);
			HAL_ADC_GetValue(&hadc1);
	}

	HAL_ADC_Stop(&hadc1);
	tempval=tempval/30;
	return tempval*BAT_VOLTAGE_RATIO;

}


#define NB_SLIDING_WINDOW 5

//==============================================================================
// void  Check_spark(void)
//
// Intputs:  None
// Outputs: None
// Return : None
// Overview: First period waveform analysis to extract spark informations (spark detection, spark line duration, spark line average voltage)
//
//==============================================================================
void  Check_spark(void)
{
	uint32_t i;
	uint32_t accu,previousaccu;
	uint32_t avg[NB_SLIDING_WINDOW]={0,0,0,0,0};
	uint32_t indexAccu;
	uint8_t peak_reached=0;

	SparkVoltage=0;
	length_Spark=0;
	Batvoltage=0;
	indexAccu=0;
	accu=0;
	previousaccu=0;

	// battery voltage measurement at the end of the period
	for(i=FirstPeriodIndex-50;i<FirstPeriodIndex;i=i+2)
	{
		Batvoltage=Batvoltage+FirstPeriod[i];
	}
	Batvoltage=Batvoltage/25;

	for(i=0;i<FirstPeriodIndex;i=i+2)
	{
		//sliding avarage of voltage waveform
		accu = accu - avg[indexAccu];
		avg[indexAccu] = FirstPeriod[i];
		accu = accu + avg[indexAccu];
		indexAccu = (indexAccu + 1) % NB_SLIDING_WINDOW;

		// spark detection (current value compared to batery voltage)
		if(accu> ((Batvoltage*NB_SLIDING_WINDOW)*2))
		{
			if(peak_reached)
			{
				length_Spark++;
				SparkVoltage=SparkVoltage+accu;
			}
			else
			{
				if(accu<previousaccu)
					peak_reached=1;
				else
					previousaccu=accu;
			}
		}
	}

	// if spark duration calculated less than 3 x 20 us, it is considered that spark does not take place
	if(length_Spark>3)
	{
		SparkVoltage=(SparkVoltage/length_Spark)/NB_SLIDING_WINDOW;
		length_Spark=(length_Spark+2) * 20;
	}
	else
	{
		SparkVoltage=0;
		length_Spark=0;
	}
}

//==============================================================================
// void SendSequenceInfo(void)
//
// Intputs:  None
// Outputs: None
// Return : None
// Overview:Transmit through UART interface Sequence information (spark info from Check_spark fonction)
//
//==============================================================================
void SendSequenceInfo(void)
{
	unsigned char buffer[10];
	buffer[0]=OverCurrentFlag;
	buffer[1]=max_current>>8;
	buffer[2]=max_current;
	buffer[3]=length_Spark>>8;
	buffer[4]=length_Spark;
	buffer[5]=SparkVoltage>>8;
	buffer[6]=SparkVoltage;
	buffer[7]=0;
	buffer[8]=0;
	SendMessage(START_SECQUENCE,buffer,9);
}

/************************************ EOF *************************************/
