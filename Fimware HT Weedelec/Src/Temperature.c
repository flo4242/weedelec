/**
*******************************************************************************
 *
 *  IRSTEA - WeedElec
 *
 *  Firmware du module de pilotage de la tete haute tension
 *
 *  Autor		: ALCIOM
 *  Version		: 1.01
 * 	FileName    : temp�rature.c
 *	Date    	: 18/12/18
 * 	Brief   	: This file provides code for configuration and use of temperature sensor TMP112
 *
 ******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "i2c.h"
#include "temperature.h"

/* Variables ---------------------------------------------------------*/


//*******************************************************************************************************************
// void Read_temperature(void)
//
// Intputs: None
// Outputs: tempval : temp�rature by 0.1�C step
// Return : None
// Overview: REad temperature
//
//*******************************************************************************************************************
int16_t Read_temperature(void)
{
	uint8_t data[2];
	int16_t tempval;
	uint16_t add=TMP112_I2C_ADD<<1;

	data[0]=TEMP_REG_ADD;
	HAL_I2C_Master_Transmit(&hi2c1,add, data,1, 0xFFFF);

	HAL_I2C_Master_Receive(&hi2c1,add, data, 2,  0xFFFF);

	tempval=((int16_t)(data[0] << 8) + data[1])>>4;
	/*printf("data0 = %x\n\r",data[0]);
	printf("data1 = %x\n\r",data[1]);
	printf("tempval(hex) = %x\n\r",tempval);
	printf("tempval(dec) = %d\n\r",tempval);*/
	tempval=tempval*DEG_PER_COUNT;
	/*printf("tempval2(hex) = %x\n\r",tempval);
	printf("tempval2(dec) = %d\n\r",tempval);*/


	return tempval;
}





/*****************************END OF FILE****/
