/********************************************************************************
 *
 *  IRSTEA - WeedElec
 *
 *  Firmware du module de pilotage de la tete haute tension
 *
 *  Autor		: ALCIOM
 *  Version		: 1.01
 * 	FileName    : WeedElec_ComProtocole.c
 *	Date    	: 18/12/18
 * 	Brief   	: This source file defines process and message of communication protocole
 *      of Weedelec board
 *
 ******************************************************************************/


//==============================================================================
// Includes
//==============================================================================

//------------------------------------------------------------------------------
//	System includes
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//	Projects includes
//------------------------------------------------------------------------------
#include "stm32l4xx_hal.h"
#include "WeedElec_ComProtocole.h"
#include "WeedElec_Process.h"
extern UART_HandleTypeDef huart1;
//==============================================================================
//	Configuration bits
//==============================================================================



//==============================================================================
// Global variables
//==============================================================================

volatile unsigned char com_status=FREE;
unsigned int nb_char_in;
unsigned char data_received_msb;
received_message message;


//==============================================================================
// Interrupts handlers
//==============================================================================



//==============================================================================
// void ProcessReceivedData(char data)
//
// Intputs: char data : character receive on UART
// Outputs: None
// Return : None
// Overview: This function interprets characters received on UART and detect when
// correct command is received
//
//==============================================================================
void ProcessReceivedData(char data)
{
    unsigned char character=0;

    character=data;

    if(com_status==FREE)
    {
        if(character==START_DELIMITER)
        {
            // beginning of applicative message detected
            com_status=MESSAGE_INCOMING;
            nb_char_in=0;
        }
    }
    else if(com_status==MESSAGE_INCOMING)
    {
        if(nb_char_in==0)
        {
            message.cmd=character;
            nb_char_in++;
            message.data_lenth=0;
            data_received_msb=1;
        }
        else
        {
            if(character==END_DELIMITER)
            {
                if(data_received_msb==0)
                {
                    send_Nack();
                    com_status=FREE;
                }
                else
                {
                    com_status=MESSAGE_RECEIVED;



                }
            }
            else if(nb_char_in==INPUT_BUFFER_SIZE)
            {
                send_Nack();
                com_status=FREE;
            }
            else
            {
                if((character>='0' && character<='9') || (character>='A' && character<='F'))
                {
                    if(data_received_msb==1)
                    {
                        message.data[message.data_lenth]=ascii(character)*0x10;
                        data_received_msb=0;
                    }
                    else
                    {
                        message.data[message.data_lenth]+=ascii(character);
                        nb_char_in++;
                        message.data_lenth++;
                        data_received_msb=1;

                    }
                }
                else
                {
                    send_Nack();
                    com_status=FREE;
                }
            }
        }
    }
}

//==============================================================================
// send_Ack
//
// Intputs: None
// Outputs: None
// Return : None
// Overview: Transmission of positive acknowledgment
//
//==============================================================================
void send_Ack(void)
{
    unsigned char Ack[]={START_DELIMITER,CODE_ACK,END_DELIMITER};
    HAL_UART_Transmit(&huart1,(uint8_t*)&Ack,sizeof(Ack),0xFFFF);
}

//==============================================================================
// send_Nack
//
// Intputs: None
// Outputs: None
// Return : None
// Overview: Transmission of negative acknowledgment
//
//==============================================================================
void send_Nack(void)
{
    unsigned char Nack[]={START_DELIMITER,CODE_NACK,END_DELIMITER};
    HAL_UART_Transmit(&huart1,(uint8_t*)&Nack,sizeof(Nack),0xFFFF);
}

//==============================================================================
// send_busy
//
// Intputs: None
// Outputs: None
// Return : None
// Overview:
//
//==============================================================================
void send_Busy(void)
{
    unsigned char Busy[]={START_DELIMITER,CODE_BUSY,END_DELIMITER};
    HAL_UART_Transmit(&huart1,(uint8_t*)&Busy,sizeof(Busy),0xFFFF);
}

//==============================================================================
// void SendMessage(unsigned char message_code,unsigned char *data,unsigned int length)
//
// Intputs: unsigned char message_code : Response code to send (firts charater)
//          unsigned char *data : Data to send
//          unsigned int length : Length of applicative data (without code and delimiters)
//
// Outputs: None
// Return : None
// Overview: This function convert binary data to ASCII and format output messages
//
//==============================================================================
void SendMessage(unsigned char message_code,unsigned char *data,unsigned int length)
{
   unsigned char a,b;
   unsigned int i;
   unsigned char tempbuf[OUTPUT_BUFFER_SIZE];

   tempbuf[0]=START_DELIMITER;
   tempbuf[1]=message_code;

   for(i=0;i<length;i++)
   {
      a=(data[i]/0x10);
      b= data[i] - (a * 0x10);
      tempbuf[2+(2*i)]=ascii(a);
      tempbuf[2+((2*i)+1)]=ascii(b);
   }

   tempbuf[(2*length)+2]=END_DELIMITER;
   HAL_UART_Transmit(&huart1,(uint8_t*)&tempbuf,(2*length)+3,0xFFFF);
}



//==============================================================================
// unsigned char ascii(unsigned char val)
//
// Intputs: unsigned char val : data to convert
// Outputs: unsigned char : result of conversion
// Return :
// Overview:This function convert ASCII digit ('0' to 'F') to binary number
//          and binary number (0x0 to 0xF) to ASCII digit
//
//==============================================================================
unsigned char ascii(unsigned char val)
{
   switch(val)
   {
      case 0x00:
         return 0x30;
         break;
      case 0x01:
         return 0x31;
         break;
      case 0x02:
         return 0x32;
         break;
      case 0x03:
         return 0x33;
         break;
      case 0x04:
         return 0x34;
         break;
      case 0x05:
         return 0x35;
         break;
      case 0x06:
         return 0x36;
          break;
      case 0x07:
         return 0x37;
         break;
      case 0x08:
         return 0x38;
         break;
      case 0x09:
         return 0x39;
         break;
      case 0x0A:
         return 0x41;
         break;
      case 0x0B:
         return 0x42;
         break;
      case 0x0C:
         return 0x43;
         break;
      case 0x0D:
         return 0x44;
         break;
      case 0x0E:
         return 0x45;
         break;
      case 0x0F:
         return 0x46;
         break;
      case 0x30:
         return 0x00;
         break;
      case 0x31:
         return 0x01;
         break;
      case 0x32:
         return 0x02;
         break;
      case 0x33:
         return 0x03;
         break;
      case 0x34:
         return 0x04;
         break;
      case 0x35:
         return 0x05;
         break;
      case 0x36:
         return 0x06;
         break;
      case 0x37:
         return 0x07;
         break;
      case 0x38:
         return 0x08;
         break;
      case 0x39:
         return 0x09;
         break;
      case 0x41:
         return 0x0A;
         break;
      case 0x42:
         return 0x0B;
         break;
      case 0x43:
         return 0x0C;
         break;
      case 0x44:
         return 0x0D;
         break;
      case 0x45:
         return 0x0E;
         break;
      case 0x46:
         return 0x0F;
         break;

   }
   return 0;
}

/************************************ EOF *************************************/
