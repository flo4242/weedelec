/********************************************************************************
 *
 *  IRSTEA - WeedElec
 *
 *  Firmware du module de pilotage de la tete haute tension
 *
 *  Autor		: ALCIOM
 *  Version		: 1.01
 * 	FileName    : WeedElec_CmdInterpretor.h
 *	Date    	: 18/12/18
 * 	Brief   	: This source file allows to interpret received commands and execute
 *      associated processes
 *
 ******************************************************************************/


struct WeedElecParameters
{
	uint16_t value;
	uint8_t RW_mode;
};

//==============================================================================
// Factory parameters
//==============================================================================

#define READ_ONLY	1
#define READ_WRITE	0

#define PARAM_FACTORY	{\
						{PARAM_DWELL_TIME_FACTORY,READ_WRITE},\
						{PARAM_PWM_FREQ_FACTORY,READ_WRITE},\
						{PARAM_NB_SHOTS_FACTORY,READ_WRITE},\
						{PARAM_MAX_CURRENT_FACTORY,READ_WRITE},\
						{FIRMWARE_VERSION,READ_ONLY},\
						{ADC_SAMPLING_FREQ,READ_ONLY}\
						}

#define PARAM_DWELL_TIME_FACTORY	2000
#define PARAM_PWM_FREQ_FACTORY		10
#define PARAM_NB_SHOTS_FACTORY		1
#define PARAM_MAX_CURRENT_FACTORY	0

#define FIRMWARE_VERSION            0x0001
#define ADC_SAMPLING_FREQ			50000

//==============================================================================
// Defines and constants
//==============================================================================

#define NB_PARAMETERS				6
#define PARAM_DWELL_TIME			0
#define PARAM_PWM_FREQ				1
#define PARAM_NB_SHOTS				2
#define PARAM_MAX_CURRENT			3
#define PARAM_FIRMWARE_VERSION		4
#define PARAM_ADC_SAMPLING_FREQ		5

#define WRITE_PARAMETER_MSG_LENGTH    3

#define STATUS                      '?'
#define WRITE_PARAMETER             'W'
#define READ_PARAMETER              'R'
#define START_SECQUENCE             'S'
#define START_TRANSFER_FIRST_PERIOD 'X'
#define READ_ANALOG_INPUT           'A'
#define READ_TEMP		            'T'
#define READ_TOR_INPUT              'I'
#define SET_TOR_OUTPUT              'O'
#define READ_BATTERY_VOLTAGE         'B'

//==============================================================================
// Functions prototype
//==============================================================================
void CmdInterpretor(void);
void InitParameter(void);



/************************************ EOF *************************************/
