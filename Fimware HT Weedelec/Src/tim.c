/**
*******************************************************************************
 *
 *  IRSTEA - WeedElec
 *
 *  Firmware du module de pilotage de la tete haute tension
 *
 *  Autor		: ALCIOM
 *  Version		: 1.01
 * 	FileName    : tim.c
 *	Date    	: 18/12/18
 * 	Brief   	: This file provides code for configuration and use of TIM instance
 *
 ******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "tim.h"

/* USER CODE BEGIN 0 */
uint16_t cnt_shot=0,pulse_index;
uint8_t PWMEndFlag=1;

TIM_HandleTypeDef htim5;

//*******************************************************************************************************************
// void HAL_TIM_PWM_PulseFinishedCallback(TIM_HandleTypeDef* tim_pwmHandle)
//
// Intputs: TIM_HandleTypeDef* tim_pwmHandle
// Outputs: None
// Return : None
// Overview: PWM end pulse intterupt
//
//*******************************************************************************************************************
void HAL_TIM_PWM_PulseFinishedCallback(TIM_HandleTypeDef* tim_pwmHandle)
{
	if(cnt_shot>1)
	{
		cnt_shot--;
	}
	else if(cnt_shot==1)
	{
		PWM_Stop();
		cnt_shot=0;
	}
	else
	{
		HAL_TIM_PWM_Stop_IT(&htim5, TIM_CHANNEL_1);
		PWMEndFlag=1;
	}
	pulse_index++;
}

//*******************************************************************************************************************
// void PWM_Start(void)
//
// Intputs: None
// Outputs: None
// Return : None
// Overview: Start PWM sequence
//
//*******************************************************************************************************************
void PWM_Start(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	PWMEndFlag=0;
	pulse_index=0;

	// Enable PWM for GPIO
    GPIO_InitStruct.Pin = GPIO_PIN_0;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF2_TIM5;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    HAL_TIM_PWM_Start_IT(&htim5, TIM_CHANNEL_1);
}

//*******************************************************************************************************************
// void PWM_Stop(void)
//
// Intputs: None
// Outputs: None
// Return : None
// Overview: Stop PWM sequence
//
//*******************************************************************************************************************
void PWM_Stop(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	// Force PWM output Low
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pin = GPIO_PIN_0;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_RESET);

}

//*******************************************************************************************************************
// void PWM_Init(uint16_t pwm_freq,uint16_t pulse_period,uint16_t nb_shot)
//
// Intputs: uint16_t pwm_freq : pwm frequency
//			uint16_t pulse_period : pwm pulse length
//			uint16_t nb_shot: number of pwm pulse
// Outputs: None
// Return : None
// Overview: Initialization of PWM sequence
//
//*******************************************************************************************************************
void PWM_Init(uint16_t pwm_freq,uint16_t pulse_period,uint16_t nb_shot)
{
	  SystemCoreClockUpdate();
	  htim5.Instance->CCR1 = (SystemCoreClock / 1000000) * pulse_period;
	  htim5.Instance->ARR = (uint32_t)SystemCoreClock / pwm_freq;

	  htim5.Instance->EGR = TIM_EGR_UG;
	  cnt_shot=nb_shot;
}


/* USER CODE END 0 */



/* TIM5 init function */
void MX_TIM5_Init(uint16_t pwm_freq,uint16_t pulse_period,uint16_t nb_shot)
{
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;

  cnt_shot=nb_shot;
   SystemCoreClockUpdate();
   htim5.Instance = TIM5;
   htim5.Init.Prescaler = 0;
   htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
   htim5.Init.Period = (SystemCoreClock / 1000000) * pulse_period;
   htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
   htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim5) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  	sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.Pulse = (uint32_t)SystemCoreClock / pwm_freq;;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_ENABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim5, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

 // HAL_TIM_MspPostInit(&htim5);

}

void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef* tim_pwmHandle)
{

  if(tim_pwmHandle->Instance==TIM5)
  {
  /* USER CODE BEGIN TIM5_MspInit 0 */

  /* USER CODE END TIM5_MspInit 0 */
    /* TIM5 clock enable */
    __HAL_RCC_TIM5_CLK_ENABLE();

    /* TIM5 interrupt Init */
    HAL_NVIC_SetPriority(TIM5_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(TIM5_IRQn);
  /* USER CODE BEGIN TIM5_MspInit 1 */

  /* USER CODE END TIM5_MspInit 1 */
  }
}
void HAL_TIM_MspPostInit(TIM_HandleTypeDef* timHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct;
  if(timHandle->Instance==TIM5)
  {
  /* USER CODE BEGIN TIM5_MspPostInit 0 */

  /* USER CODE END TIM5_MspPostInit 0 */
  
    /**TIM5 GPIO Configuration    
    PA0     ------> TIM5_CH1 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_0;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF2_TIM5;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* USER CODE BEGIN TIM5_MspPostInit 1 */

  /* USER CODE END TIM5_MspPostInit 1 */
  }

}

void HAL_TIM_PWM_MspDeInit(TIM_HandleTypeDef* tim_pwmHandle)
{

  if(tim_pwmHandle->Instance==TIM5)
  {
  /* USER CODE BEGIN TIM5_MspDeInit 0 */

  /* USER CODE END TIM5_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_TIM5_CLK_DISABLE();

    /* TIM5 interrupt Deinit */
    HAL_NVIC_DisableIRQ(TIM5_IRQn);
  /* USER CODE BEGIN TIM5_MspDeInit 1 */

  /* USER CODE END TIM5_MspDeInit 1 */
  }
} 

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
