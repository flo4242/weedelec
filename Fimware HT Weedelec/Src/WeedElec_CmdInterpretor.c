/********************************************************************************
 *
 *  IRSTEA - WeedElec
 *
 *  Firmware du module de pilotage de la tete haute tension
 *
 *  Autor		: ALCIOM
 *  Version		: 1.01
 * 	FileName    : WeedElec_CmdInterpretor.c
 *	Date    	: 18/12/18
 * 	Brief   	: This source file allows to interpret received commands and execute
 *      associated processes
 *
 ******************************************************************************/
//==============================================================================
// Includes
//==============================================================================

//------------------------------------------------------------------------------
//	System includes
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//	Projects includes
//------------------------------------------------------------------------------
#include "stm32l4xx_hal.h"
#include "WeedElec_ComProtocole.h"
#include "WeedElec_CmdInterpretor.h"
#include "WeedElec_Process.h"
#include "Temperature.h"
#include "tim.h"

//==============================================================================
//	Configuration bits
//==============================================================================



//==============================================================================
// Global variables
//==============================================================================
extern received_message message;
extern unsigned char com_status;
unsigned int Firmware_Version=FIRMWARE_VERSION;
extern uint8_t TransferMode;
extern uint8_t TransferFirstPeriod;
struct WeedElecParameters WeedElecParameters[NB_PARAMETERS]=PARAM_FACTORY;
extern uint16_t current_limit;
extern uint8_t state;
extern uint16_t nb_sample_period;

//==============================================================================
// Interrupts handlers
//==============================================================================

//==============================================================================
// void InitParameter(void)
//
// Intputs: None
// Outputs: None
// Return : None
// Overview:
//
//==============================================================================
void InitParameter(void)
{
	MX_TIM5_Init(WeedElecParameters[PARAM_PWM_FREQ].value,WeedElecParameters[PARAM_DWELL_TIME].value,WeedElecParameters[PARAM_NB_SHOTS].value);
}

//==============================================================================
// void CmdInterpretor(void)
//
// Intputs: None
// Outputs: None
// Return : None
// Overview: Function perodicaly call in main programm. This function interprets
//  commands received through UART and execute associated processes
//
//==============================================================================
void CmdInterpretor(void)
{
    unsigned char buffer[OUTPUT_BUFFER_SIZE/2];
    unsigned int i=0;
    unsigned int temp_val;

    if(com_status == MESSAGE_RECEIVED)
    {
    	if(state==IDLE)
    	{
			switch(message.cmd)
			{
			//***************************************************************************************************************/
			//*   Status cmd
			//****************************************************************************************************************/
				case STATUS :
					buffer[i++]=Firmware_Version<<8;
					buffer[i++]=Firmware_Version;
					SendMessage(STATUS,buffer,i);
					break;
			//****************************************************************************************************************/
			//*   WRITE PARAMETER (cmd code "W")
			//****************************************************************************************************************/
				case WRITE_PARAMETER :
					if((message.data_lenth==WRITE_PARAMETER_MSG_LENGTH) && (message.data[0]<NB_PARAMETERS) && (WeedElecParameters[message.data[0]].RW_mode==READ_WRITE))
					{
						WeedElecParameters[message.data[0]].value=message.data[1]*0x0100 + message.data[2];
						send_Ack();
					}
					else
					{
						send_Nack();
					}

					break;
			//****************************************************************************************************************/
			//*   READ PARAMETER (cmd code "R")
			//****************************************************************************************************************/
				case READ_PARAMETER :
					if((message.data_lenth==1) && (message.data[0]<NB_PARAMETERS))
					{
						 buffer[i++]=WeedElecParameters[message.data[0]].value>>8;
						 buffer[i++]=WeedElecParameters[message.data[0]].value;
						 SendMessage(READ_PARAMETER,buffer,i);
					}
					else
					{
						send_Nack(); //Invalid number of parameters
					}
					break;
			//****************************************************************************************************************/
			//*   Read analog input cmd
			//****************************************************************************************************************/
				case READ_ANALOG_INPUT :
					if(message.data_lenth==0)
					{
						temp_val=Read_Analog_input();
						buffer[0]=temp_val>>8;
				     	buffer[1]=temp_val;
				     	SendMessage(READ_ANALOG_INPUT,buffer,2);
					}
					else
					{
						send_Nack(); //Invalid number of parameters
					}
					break;

			//****************************************************************************************************************/
			//*   Read Digital inputs cmd
			//****************************************************************************************************************/
				case READ_TOR_INPUT :
					if(message.data_lenth==0)
					{
						buffer[0]=Read_Digital_Input1();
						buffer[1]=Read_Digital_Input2();
						SendMessage(READ_TOR_INPUT,buffer,2);
					}
					else
					{
						send_Nack(); //Invalid number of parameters
					}
					break;
					//****************************************************************************************************************/
					//*   Set Digital output cmd
					//****************************************************************************************************************/
				case SET_TOR_OUTPUT :
					if((message.data_lenth==1) && (message.data[0]<2))
					{
						if(message.data[0]==0)
							Reset_Digital_Output();
						else
							Set_Digital_Output();

						send_Ack();
					}
					else
					{
						send_Nack(); //Invalid number of parameters
					}
					break;
					//****************************************************************************************************************/
					//*   Read temperature cmd
					//****************************************************************************************************************/
				case READ_TEMP :
					if((message.data_lenth==0))
					{
						temp_val=Read_temperature();
						buffer[0]=temp_val>>8;
						buffer[1]=temp_val;
						SendMessage(READ_TEMP,buffer,2);
					}
					else
					{
						send_Nack(); //Invalid number of parameters
					}
					break;
					//****************************************************************************************************************/
					//*   Read battery voltage
					//****************************************************************************************************************/
				case READ_BATTERY_VOLTAGE :
					if((message.data_lenth==0))
					{
						temp_val=Read_Battery_Voltage();
						buffer[0]=temp_val>>8;
						buffer[1]=temp_val;
						SendMessage(READ_BATTERY_VOLTAGE,buffer,2);
					}
					else
					{
						send_Nack(); //Invalid number of parameters
					}
					break;


			//****************************************************************************************************************/
			//*   Start Transfer First period
			//****************************************************************************************************************/
				case START_TRANSFER_FIRST_PERIOD :

					state=TRANSMIT_FIRST_PERIOD;
					//send_Ack();
					HAL_Delay(20);

					break;


			//****************************************************************************************************************/
			//*   START_SECQUENCE
			//****************************************************************************************************************/
				case START_SECQUENCE :

					if((message.data_lenth==1) && (message.data[0]<2))
					{
						TransferMode=message.data[0];
						HAL_Delay(20);
						PWM_Init(WeedElecParameters[PARAM_PWM_FREQ].value,WeedElecParameters[PARAM_DWELL_TIME].value,WeedElecParameters[PARAM_NB_SHOTS].value);
						current_limit=WeedElecParameters[PARAM_MAX_CURRENT].value;
						nb_sample_period=(2*ADC_SAMPLING_FREQ) / WeedElecParameters[PARAM_PWM_FREQ].value;
						state=START_SEQUENCE;
					}
					else
					{
						send_Nack(); //Invalid number of parameters
					}
					break;


			//****************************************************************************************************************/
			//*	Error: Cmd not supported
			//****************************************************************************************************************/
				default :
					send_Nack();
					break;
			}
    	}
    	else
    	{
    		send_Busy();
    	}
        com_status=FREE;
    }
}


/************************************ EOF *************************************/
