/********************************************************************************
 *
 *  IRSTEA - WeedElec
 *
 *  Firmware du module de pilotage de la tete haute tension
 *
 *  Autor		: ALCIOM
 *  Version		: 1.01
 * 	FileName    : WeedElec_ComProtocole.h
 *	Date    	: 18/12/18
 * 	Brief   	: This source file defines process and message of communication protocole
 *      of Weedelec board
 *
 ******************************************************************************/

//==============================================================================
// Includes
//==============================================================================



//==============================================================================
// Defines and constants
//==============================================================================
#define INPUT_BUFFER_SIZE						1024
#define OUTPUT_BUFFER_SIZE                      1024
#define FREE                                    0
#define MESSAGE_INCOMING                        1
#define MESSAGE_RECEIVED                        2

#define START_DELIMITER                         '<'
#define END_DELIMITER                           '>'
#define CODE_ACK                                'K'
#define CODE_NACK                               'N'
#define CODE_BUSY								'Z'

typedef struct
{
   	unsigned char cmd;
   	unsigned char data[INPUT_BUFFER_SIZE];
	unsigned int data_lenth;
} received_message;

//==============================================================================
// Functions prototype
//==============================================================================
void ProcessReceivedData(char data);
void send_Ack(void);
void send_Nack(void);
void send_Busy(void);
void SendMessage(unsigned char message_code,unsigned char *data,unsigned int length);
unsigned char ascii(unsigned char val);
/************************************ EOF *************************************/
