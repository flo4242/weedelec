/********************************************************************************
 *
 *  IRSTEA - WeedElec
 *
 *  Firmware du module de pilotage de la tete haute tension
 *
 *  Autor		: ALCIOM
 *  Version		: 1.01
 * 	FileName    : WeedElec_Process.h
 *	Date    	: 18/12/18
 * 	Brief   	: This source file defines process and message of communication protocole
 *      of Weedelec board
 *
 ******************************************************************************/

//==============================================================================
// Includes
//==============================================================================



//==============================================================================
// Defines and constants
//==============================================================================

#define IDLE					0
#define START_SEQUENCE			1
#define TRANSMIT_BUFFER			2
#define TRANSMIT_FIRST_PERIOD	3
#define SEQUENCE_RUNNING 		4

#define BUF_SIZE	200
#define BUFFER0		0
#define BUFFER1		1

#define DEBUG_MODE	1
#define NORMAL_MODE	0


#define VOLTAGE_RATIO		(330)/4096
#define BAT_VOLTAGE_RATIO	(3300)/4096
#define CURRENT_RATIO	((int32_t)55000)/(int32_t)4096
#define ANALOG_INPUT_RATIO    (3300)/4096

#define VOLTAGE_CHANNEL	0
#define CURRENT_CHANNEL 1


//==============================================================================
// Functions prototype
//==============================================================================
void Adc_measurement_process(void);
void WeedElec_StateMachine(void);
void Send_buffer(void);
void Send_FirstPeriod(void);
void SendSequenceInfo(void);
uint16_t Read_Analog_input(void);
void Set_Digital_Output(void);
void Reset_Digital_Output(void);
uint8_t	Read_Digital_Input1(void);
uint8_t	Read_Digital_Input2(void);
uint16_t Read_Battery_Voltage(void);
void Check_spark(void);
/************************************ EOF *************************************/
