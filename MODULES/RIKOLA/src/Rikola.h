#ifndef __IMPORTRAW__H
#define __IMPORTRAW__H

/****************************************************************************************************
					Fonctions d'importation au format BUFIMG<unsigned short>	 
En cas d'erreur (lecture fichier, allocation...) retour avec false et message d'erreur dans msg500
*****************************************************************************************************/


#include "BufimTPL.h"	//dossier imgen

_EXPORT_ BUFIMG<unsigned short> *  ImportTiff16(char *Nf, char *errmsg500);		//ImportTIFF16.cpp


/******************************************************* SIGMA *************************************/

_EXPORT_ bool  ImportRawX3F(BUFIMG<unsigned short> &C1, BUFIMG<unsigned short> &C2,
					 BUFIMG<unsigned short> &C3, char *Nf, char *errmsg500);		//ImportX3F.cpp

/* importe les 3 canaux d'une image X3F (sigma DP1 ou DP2) � partir du fichier Nf et les
range dans les structures C1, C2, C3. Ces 3 structures peuvent avoir �t� cr��es par d�faut
 (ex: BUFIMG<unsigned short> C1, C2, C3; ); leur tableau de donn�es est r�allou�.*/


_EXPORT_ double NiveauNoirSigma(BUFIMG<unsigned short> *ImRaw);
_EXPORT_ void SoustractionNiveauNoirSigma(BUFIMG<unsigned short> *ImRaw);



/******************************************************* CANON *************************************/

_EXPORT_ bool ImportRawCR2(char *Nf, BUFIMG<unsigned short>**Tab4, char *msg1000, bool suppressionniveaunoir, bool correctiongamma);
// importe les 4 canaux d'une image CR2 (Canon) � partir du fichier Nf, sans d�mosaiquage
_EXPORT_ bool ImportRawCR2(char *Nf, BUFIMG<unsigned short>* bimg, char *msg1000, bool suppressionniveaunoir, bool correctiongamma);
// idem avec les quatre canaux regroup�s (identique � l'image du CCD) pour minimiser les allocations au niveau du programme appelant

#define NumPlanCanon(x, y) ( y%2 ? (x%2 ? 2 : 3) : (x%2 ? 1 : 0) )
//pour demultiplexer les quatre canaux regroup�s

_EXPORT_ void DemosaiquageSplineCanon(BUFIMG<float> *im, int numplan, bool Lissage, double largeurlissage);
_EXPORT_ void DemosaiquageLineaireCanon(BUFIMG<float> *Im, int numplan);

 _EXPORT_ void  DoRotation180(BUFIMG<unsigned short> * pim);
// retournement de l'image; utile lors de l'acquisition d'images a�riennes avecdes appareils justapos�s t�te-b�che

#endif