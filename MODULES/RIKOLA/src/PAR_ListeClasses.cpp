#include "PAR_ListeClasses.h"

#include <istream>
#include <strstream>

#include "winutil2.h"

extern bool GetInfoModele(char *NfModele, _list<int> &Classes, int &NbBandes, char * msg1000); //FRikola.cpp



int GetNextValue(std::istream &is, bool last)
{
	int val;
	is >> val;
	if (!last) //lecture de la virgule suivante
	{
		char dummy;
		is.get(dummy);
	}
	return val;
}

bool PAR_ListeClasses::ConversionDepuisChaine(void *dest, char *src, bool)
{
	_list<int> *pListe = (_list<int> *) dest;

	pListe->clear();

	//nb de valeurs � lire ?
	int len = strlen(src);
	int nbsep = 0;
	for (int i = 0; i < len; i++)
	{
		if (src[i] == ',') nbsep++;
	}

	std::istrstream is(src, TAILLESTRINGLISTE);

	for (int i = 0; i < nbsep; i++) pListe->push_back(GetNextValue(is, false));

	//derni�re valeur:
	pListe->push_back(GetNextValue(is, true));
	
	return true;
}

void PAR_ListeClasses::ConversionVersChaine(void *source, char *dest, bool)
{
	if (source == NULL)
	{
		dest = "";
		return;
	}


	_list<int> &Liste = * ((_list<int> *) source);

	std::ostrstream os(dest, TAILLESTRINGLISTE);

	int cnt = 0;
	BalayeListe(Liste)
	{
		os << *Liste.current();
		if (cnt<Liste.nbElem-1) os << ", ";
		cnt++;
	}
	os << '\0';
}


bool PAR_ListeClasses::Browse(char *str, HWND hwnd)   //d�clench� par le bouton	//saisie entier dans liste
{
	_list<int> ClassesPossibles;
	int NbBandes;
	char msg[1000];
	
	if (!GetInfoModele(FichierModele, ClassesPossibles, NbBandes, msg))
	{
		MessageBox(NULL, msg, "Liste classes � d�tecter", MB_OK | MB_ICONERROR);
		return false;
	}

	_list<int> TempPossibles = ClassesPossibles; //liste qui diminuera � chaque choix 

	ListeClasses = (_list<int> *) Adresse;
	ListeClasses->clear();

	while (true)	//saisie successive des num�ros de classe
	{
		//cr�ation liste de choix temporaire

		NbRubriques = TempPossibles.nbElem;
		Rubriques = new char *[NbRubriques + 1];

		int i = 0;
		BalayeListe(TempPossibles)
		{
			Rubriques[i] = new char[20];
			sprintf(Rubriques[i], "classe %d", *TempPossibles.current());
			i++;
		}

		Rubriques[NbRubriques] = NULL;
		
		//saisie nouvelle classe par multichoix classique
		char strtemp[50];
		int Choix = -1;

		Adresse = &Choix; //changement temporaire de l'adresse de variable

		bool ret = PAR_MULCHOIX_TPL<TYPEGROUPEWIN>::Browse(strtemp, hwnd);

		if (ret)	//retrouve l'entier choisi
		{
			for (i = 0; i < NbRubriques; i++)
				if (strcmp(Rubriques[i], strtemp) == 0) Choix = i;
		}

		for (i = 0; i < NbRubriques; i++) delete Rubriques[i]; //tous sauf le dernier (NULL)
		delete Rubriques; Rubriques = NULL;
		
		if(!ret)	break;

		//r�duction de la liste temporaire
		int EntierChoisi = TempPossibles[Choix].obj;
		TempPossibles.remove(EntierChoisi);

		ListeClasses->push_back(EntierChoisi);
		
	}

	Adresse = ListeClasses; //r�tablit l'adresse de variable du param�tre

	ConversionVersChaine(ListeClasses, str, false);	//false arbitraire (inutilis�)


	return true;
}


