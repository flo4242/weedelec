#include "TaskUtil.h" 
#include "ExportModuleRikola.h" //fonctions export�es pour utilisation externe (autres modules)
#include "Calage.h"


#include "drvtraiti.h" 
#include "envi2TPL.h"
#include "OutilsHyper.h"
#include "binaire.h"


int CalculClasse(double * TabSpectre, MATRICE &Modele, int NbClasses)
{
	int NbBandes = Modele.dimC() - 3; // le nombre de colonnes = 2 (classes) + Nb bandes (coeffB) + 1 (coeff b0)  (d�j� v�rifi�)
	int NbModeles = Modele.dimL();

	//extraction des coefficients de mod�les PLS
	MATRICE ModeleBb0 = Modele.ExtraitPartielle(0, 2, NbModeles, NbBandes + 1);

	//pr�paration de tableau des seuils
	double * TabSeuil = new double[NbModeles];
	for (int i = 0; i < NbModeles; i++)
		TabSeuil[i] = (Modele[i][0] + Modele[i][1]) / 2.0;

	// classification de chaque mod�le PLS
	int C = -1;
	int * Classe = new int[NbModeles];
	ARBRE2(int) arbreclasse;

	for (int i = 0; i < NbModeles; i++)
	{
		double score = 0;
		VECTEUR Bb0 = ModeleBb0.Ligne(i);
		for (int b = 0; b < NbBandes; b++)
			score += TabSpectre[b] * Bb0[b];
		score += Bb0[NbBandes];	//derni�re colonne = offset

		// seuillage sur la classification estim�e
		if (score < TabSeuil[i])
			Classe[i] = Modele[i][0];
		else
			Classe[i] = Modele[i][1];

		//Cr�ation d'un arbre pour calculer la r�p�tition de chaque classe
		arbreclasse.push(Classe[i]);
	}

	//retour de r�sultat de la classification
	if (arbreclasse.maxOccurrences != (NbClasses - 1)) // retour -1 si on ne peut pas classifier ce spectre
	{
		delete TabSeuil;
		delete Classe;
		return -1;
	}

	else
	{
		int index_classe = 0;
		for (arbreclasse.begin(); !arbreclasse.end(); arbreclasse++, index_classe++)
			if (arbreclasse.NCourant->Occurrences == (NbClasses - 1))
			{
				C = arbreclasse.NCourant->Valeur;
				break;
			}
	}

	delete TabSeuil;
	delete Classe;
	return C;
}

//fonction de discrimination
/* l'image ImRes contient au retour pour chaque pixel:

- la valeur 0 si le spectre d'entr�e est nul pour ce pixel
- le num�ro de la classe tel que sp�cifi� dans les premi�res colonnes du fichier de mod�le (suppos� compris entre 1 et 127)
- la valeur 128 si la classe est ind�termin�e
*/


bool DiscriminateWeeds(TImageHyper<float> *ImRef, IMGRIS *ImRes, TParamDetectionRikola50 &param, char *msg1000, DRVTRAITIM *pD)
{
	int dimX = ImRef->H.Samples;
	int dimY = ImRef->H.Lines;
	int NbBandes = ImRef->H.Bands;

	MATRICE Modele;
	if (!Modele.Charge(param.FichierModelesPLS)) { sprintf(msg1000, "Echec chargement %s", param.FichierModelesPLS);	return false; }

	if (Modele.dimC() != NbBandes + 3) // 2 (couple de classes) + Nb bandes (coeffB) + 1 (coeff b0)
	{
		sprintf(msg1000, "Nb de colonnes du mod�le (%d) non conforme (%d + 3 attendues))", Modele.dimC(), NbBandes);
		return false;
	}

	ARBRE2(int) arbre;

	for (int i = 0; i < Modele.dimL(); i++)
	{
		arbre.push(Modele[i][0]);
		arbre.push(Modele[i][1]);
	}

	int NbClasses = arbre.NbNoeuds;
	int NbCouples = NbClasses * (NbClasses - 1) / 2;

	if (Modele.dimL() != NbCouples)
	{
		sprintf(msg1000, "Nb de classes d�tect�es: %d;  Nb de lignes du mod�le non conforme (%d lues, %d attendues)",
								NbClasses, Modele.dimL(), NbCouples);
		return false;
	}

	for (arbre.begin(); !arbre.end(); arbre++)
		if (arbre.NCourant->Occurrences != (NbClasses - 1))
		{
			sprintf(msg1000, "Nb de mod�les PLS non conforme pour la classe %d (%d lues, %d attendues)", 
								arbre.NCourant->Valeur, arbre.NCourant->Occurrences, NbClasses - 1);
			return false;
		}

	
	////////////////////////////////////////////////
	//application du modele pour chaque pixel
	////////////////////////////////////////////////


	double *TabSpectre = new double[NbBandes];

	
	for (int y = 0; y < dimY; y++)
	{
		if(pD) pD->PrintTexte("Ligne %d / %d\n", y + 1, dimY);

		for (int x = 0; x < dimX; x++)
		{
			if (!ImRef->LitSpectre(x, y, TabSpectre))
			{
				delete TabSpectre;
								
				sprintf(msg1000, "Echec lecture contenu %s", ImRef->NomReduit); return false;
				return false;
			}

			if (SpectreNul(TabSpectre, NbBandes))
			{
				ImRes->EcritPixel(x, y, 0);
				continue;
			}


			//classification
			int C = CalculClasse(TabSpectre, Modele, NbClasses);

			if (C == -1)	ImRes->EcritPixel(x, y, 128); //indetermin�

			else			ImRes->EcritPixel(x, y, C);

			
		}
	}

	

	delete TabSpectre;
	return true;

}

POINTFLT Centre(Region R) //� mettre en membre de Region !!!
{
	float cgx = 0;
	float cgy = 0;

	BalayeListe(R.Segments)
	{
		SEGMENT *pS = R.Segments.current();
		double poidsseg = pS->xd - pS->xg +1; //xd inclus: xd = xg donne poids 1
		cgx += poidsseg * (pS->xd + pS->xg) / 2.;
		cgy += poidsseg * pS->y;
		
	}

	return POINTFLT(cgx, cgy) / R.NbPixels;
}


bool GetWeedCenters(IMGRIS *ImLabels, TParamDetectionRikola50 &param, _list<POINTFLT> &ListeWeedsImage, char *msg1000, DRVTRAITIM *pD)
{
	ListeWeedsImage.clear();

	int dimX = ImLabels->F.right;
	int dimY = ImLabels->F.bottom;

	if (param.ClassesToDetect.nbElem == 0)
	{
		sprintf(msg1000, "pas de classes de mauvaisesherbes � d�tecter specifi�es !");
		return false;
	}


	//relev� de tous les objets binaires
	TEnsembleRegions TER;

	BalayeListe(param.ClassesToDetect)
	{
		int Label = *param.ClassesToDetect.current();

		//cr�ation d'une image binaire pour la classe correspondante
		ImLabels->SeuilInf = Label;
		ImLabels->SeuilSup = Label;

		ReleveObjetsBinaires(ImLabels, &TER, true, false, false);

	}

	//tri selon les dimensions

	BalayeListe(TER.ListeRegions)
	{
		Region *pR = TER.ListeRegions.current();
		if (pR->NbPixels < param.TailleWeedMin)	continue;
		if (pR->NbPixels > param.TailleWeedMax)	continue; //FONCTION SPLIT A ECRIRE

		ListeWeedsImage.push_back(Centre(*pR));
	}
	return true;

}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void TModuleRikola::DetectionRikola50(TRetourDetectionRikola &ret, std::string FichierImageRef, DRVTRAITIM *pD, IMGRIS *imlabels)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
{
	int TimeDebut = GetTickCount();

	TParamDetectionRikola50 Param;
	if(!ChargeParametresDetection50(Param))
	{
		ret.InfoRetour = "Echec chargement param�tres d�tection";
		ret.State = TRetourState::Error;
		ret.DureeMs = GetTickCount() - TimeDebut;
		return;
	}

	//chargement de l'image en r�flectance
	char msg[1000];
	TNomFic NfRef; strcpy(NfRef, FichierImageRef.data());
	TImageHyperGen * ImH = OuvreImageHyper(NfRef, msg, 1000);
	if(!ImH)
	{
		ret.InfoRetour = msg;
		ret.State = TRetourState::Error;
		ret.DureeMs = GetTickCount() - TimeDebut;
		return;
	}

	TImageHyper<float> * ImRef = dynamic_cast<TImageHyper<float>*>(ImH);
	if (!ImRef)
	{
		ret.InfoRetour = "L'image IHS fournie n'est pas de type float:\n";  ret.InfoRetour  += NfRef;
		ret.State = TRetourState::Error;
		ret.DureeMs = GetTickCount() - TimeDebut;
		delete ImH;
		return;
	}

	if(!ImRef->ChargeEnMemoire())
	{
		ret.InfoRetour = "Echec chargement en m�moire de :\n"; ret.InfoRetour += NfRef;
		ret.State = TRetourState::Error;
		ret.DureeMs = GetTickCount() - TimeDebut;
		delete ImRef;
		return;
	}


	IMGRIS *ImRes;
	if (imlabels) ImRes = imlabels;	//image r�sultat sp�cifi�e

	else //usage local
	{
		int dimX = ImRef->H.Samples;
		int dimY = ImRef->H.Lines;
		ImRes = new IMGRISMO(dimX, dimY);
	}

	
	bool retD = DiscriminateWeeds(ImRef, ImRes, Param, msg, pD);

	if (!retD)
		{
		ret.InfoRetour = "Echec discrimination: ";  ret.InfoRetour  += msg;
		ret.State = TRetourState::Error;
		ret.DureeMs = GetTickCount() - TimeDebut;

		if (!imlabels)	delete ImRes;
		
		return;
	}

	delete ImRef;

	//on a maintenant une image contenant un niveau de gris par label

	if(!GetWeedCenters(ImRes, Param, ret.ListeWeedsImage, msg, pD))
	{
		ret.InfoRetour = msg;
		ret.State = TRetourState::Error;
		ret.DureeMs = GetTickCount() - TimeDebut;
		if (!imlabels)	delete ImRes;
		return;
	}

	if (!imlabels)	delete ImRes;

	//coordonn�es dans l'espace du robot

	TCalage2D* Calage = new Trapeze;

	if (!Calage->Charge(Param.FichierCalage))
	{
		sprintf(msg, "Echec chargement fichier \n %s", Param.FichierCalage);
		ret.InfoRetour = msg;
		ret.State = TRetourState::Error;
		ret.DureeMs = GetTickCount() - TimeDebut;
		delete Calage;
		return;
	}
	
	BalayeListe(ret.ListeWeedsImage)
	{
		POINTFLT PM = Calage->Pixels2Metres(*ret.ListeWeedsImage.current());
		TWeedRobot WR(PM, -1, -1);
		ret.ListeWeeds.push_back(WR);
	}

	delete Calage;

	ret.InfoRetour = "";
	ret.State = TRetourState::Completed;
	ret.DureeMs = GetTickCount() - TimeDebut;
	
	return;

}


TCalage2D * TModuleRikola::ChargeCalage(char * msgerr)
{

	TParamDetectionRikola50 paramD;
	/*if (!ChargeParametres(param))
	{
		sprintf("Echec chargement param�tres Rikola", msgerr);
		return NULL;
	}*/

	TCalage2D * Calage = new Trapeze;

	if (!Calage->Charge(paramD.FichierCalage))
	{
		sprintf("Echec chargement calage Sony", msgerr);
		delete Calage;
		return NULL;
	}

	return Calage;
}


IMTCOULEUR * TModuleRikola::VisualiseImage(DRVTRAITIM *pD, char *Nf, char * msgerr)
{
	sprintf(msgerr, "Visualisation image non impl�ment�e !");
	return NULL;
}

