#include "drvtraiti.h"
#include "WeedelecCommon.h"
#include "Winutil2.h"
#include "imtemplate.h"
#include "envi2TPL.h"
#include "matrice.h"
#include "Par_Region.h"
#include "VegetationRikola.h"

#define NDVI_INVALID -10

extern bool AfficheProgression(char *msg, void *pUser);


bool GereParametresVeg(HWND hwnd, ModeGereParametres modeGereParam, TParamVegRikola &param, DRVTRAITIM *pD)
/*********************************************************************************************************/
//pD n�cessaire pour la saisie PAR_Region (mode ModeGereParametres::Gere)
{

	GROUPE G(hwnd, "Param�tres segmentation v�g�tation Rikola", "RikolaVegetation.cfg",
		DBL("Seuillage NDVI certitude v�g�tation", param.seuilNDVIVeg, 0, 1),
		DBL("Seuillage NDVI certitude sol", param.seuilNDVISol, 0, 1),
		DBL("Seuillage R+NIR minimum", param.seuilRplusNIRmin, 0, 1),
		new PAR_Region("R�gion de relev� sol", &param.RegionSol, pD),

		NULL);

	bool OK;

	switch (modeGereParam)
	{
	case(ModeGereParametres::Charge): OK = G.Charge(); break;
	case(ModeGereParametres::Sauve): OK = G.Sauve(); break;
	case(ModeGereParametres::Gere): OK = G.Gere(false); break; //false: non modal pour PAR_Region
	}

	return OK;
}

BUFIMG<float> * GetNDVI(DRVTRAITIM *pD, TImageHyper<float> *ImReflectance, double seuilmin, char *msg1000)
{
	if (strcmp(ChaineExt(ImReflectance->NomComplet), "ref") != 0)
	{
		sprintf(msg1000, "L'image source n'est pas une image en r�flectance");
		return NULL;
	}

	int NbBandes = ImReflectance->H.Bands;
	int dimX = ImReflectance->H.Samples;
	int dimY = ImReflectance->H.Lines;

	//d�finition des bandes NDVI
	double CentresBandes[3] = { 640, 850 };
	double sigma = 30;

	//cr�ation vecteurs pour produit scalaire
	VECTEUR VBande[2];

	for (int i = 0; i < 2; i++)
	{
		VBande[i] = VECTEUR(NbBandes);
		double Somme = 0;
		for (int b = 0; b < NbBandes; b++)
		{
			double l = ImReflectance->H.wavelength[b];
			VBande[i][b] = exp(-(l - CentresBandes[i])*(l - CentresBandes[i]) / (2 * sigma*sigma));
			Somme += VBande[i][b];
		}

		VBande[i] = VBande[i] * (1 / Somme);
	}

	BUFIMG<float> *NDVI = new BUFIMG<float>(dimX, dimY);


	int previouspercent = -1;

	double *TabSpectre = new double[NbBandes];
	for (int y = 0; y < dimY; y++)
	{
		char st[500];
		int percent = (100 * (double)y / dimY);
		if (percent > previouspercent)
		{
			previouspercent = percent;
			sprintf(st, "Cr�ation image NDVI: %d%%\n", (int)(100 * (double)y / dimY));
			if (AfficheProgression(st, pD)) { strcpy(msg1000, "Arret utilisateur"); return false; }
		}

		for (int x = 0; x < ImReflectance->H.Samples; x++)
		{
			if (!ImReflectance->LitSpectre(x, y, TabSpectre))
			{
				sprintf(msg1000, "Erreur lecture image source");
				delete TabSpectre;
				delete NDVI;
				return NULL;
			}

			VECTEUR Val(NbBandes);  for (int b = 0; b < NbBandes; b++) Val[b] = TabSpectre[b];

			double R = (Val | VBande[0]);
			double NIR = (Val | VBande[1]);

			if (R + NIR > seuilmin)			NDVI->Tab[y][x] = (NIR - R) / (NIR + R);
			else							NDVI->Tab[y][x] = NDVI_INVALID;


		}

	}

	delete TabSpectre;

	return NDVI;


}

bool SeuillageVeg(BUFIMG<float> *ndvi, TParamVegRikola &param, TImageHyper<float> *Ref, TImageHyper<float> *Veg, TImageHyper<float> *Sol,
							char *msg)
{
	LIGNE LC = ~param.RegionSol;
	Region RegionSol(LC);
	if (RegionSol.NbPixels == 0)
	{
		sprintf(msg, "RegionSol non d�finie"); return false;
	}

	bool OKCompat = true;
	if (Veg->H.Bands != Ref->H.Bands) OKCompat = false;
	if (Sol->H.Bands != Ref->H.Bands) OKCompat = false;

	if (Ref->H.Lines != ndvi->dimY) OKCompat = false;
	if (Veg->H.Lines != ndvi->dimY) OKCompat = false;
	if (Sol->H.Lines != ndvi->dimY) OKCompat = false;
	if (Ref->H.Samples != ndvi->dimX) OKCompat = false;
	if (Veg->H.Samples != ndvi->dimX) OKCompat = false;
	if (Sol->H.Samples != ndvi->dimX) OKCompat = false;

	if(!OKCompat)
	{
		sprintf(msg, "Incompatibilit� de dimension entre les images sp�cifi�es"); return false;
	}

	//lecture et seuillage
	int Bands = Ref->H.Bands;

	double *TabSpectre = new double[Bands];
	double *SpectreNul = new double[Bands]; memset(SpectreNul, 0, Bands * sizeof(double));

	for (int y = 0; y < ndvi->dimY; y++)
	{
		for (int x = 0; x < ndvi->dimY; x++)
		{
			double val = ndvi->Tab[y][x];
			if (val == NDVI_INVALID)
			{
				Veg->EcritSpectre(x, y, SpectreNul);
				Sol->EcritSpectre(x, y, SpectreNul);
				continue;
			}

			Ref->LitSpectre(x, y, TabSpectre);

			if (val < param.seuilNDVIVeg)	Veg->EcritSpectre(x, y, SpectreNul);
			else						Veg->EcritSpectre(x, y, TabSpectre);

			if (RegionSol.ContientPoint(x, y))
			{
				if (val > param.seuilNDVISol)	Sol->EcritSpectre(x, y, SpectreNul);
				else							Sol->EcritSpectre(x, y, TabSpectre);
			}

			else Sol->EcritSpectre(x, y, SpectreNul);

		}
	}

	return true;
}
