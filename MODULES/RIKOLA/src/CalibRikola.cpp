#include "TaskUtil.h" 
#include "ExportModuleRikola.h" //fonctions export�es pour utilisation externe (autres modules)
#include "Calage.h"


#include "drvtraiti.h" 
#include "LanceExe.h" 
#include "GetListeFichiers.h" 
#include "EditTextFile.h" 

#include "envi2TPL.h"
#include "VisNir2Bufimg.h"
#include "FRikola.h"





/*******************************************************************/
/**************** CONVERSION RAW --> CALIB *************************/
/*******************************************************************/

/********* fonctions de modification fichiers hdt **************/
bool ReplaceDarkReferenceInHdt(char *NfIn, char *NfOut, char * newDarkReference)
{
	int sizeBuf;

	char *Buf = FillBuff(NfIn, sizeBuf);
	if (Buf == NULL)	return false;

	char Rubrique[] = "Dark Filename =";
	
	int linesize=0;
	int index = FindLineInBuf(Rubrique, Buf, sizeBuf, linesize, 0);

	if(index<0) { delete Buf;  return false; }

	

	// ligne de remplacement
	char newline[1000];	sprintf(newline, "%s \"%s\"\r\n", Rubrique, newDarkReference);
	

	//cr�ation d'un buffer modifi�
	if(!ReplaceLineInBuf(Buf, sizeBuf, index, newline)) { delete Buf;  return false; }

	//sizeBuf contient maintenant la nouvelle taille

	//r��criture du fichier
	FILE *F = fopen(NfOut, "wb");
	if (F==NULL)	{ delete Buf;  return false; }

	bool ret = (fwrite(Buf, 1, sizeBuf, F) == sizeBuf);

	delete Buf;   fclose(F); return ret;

}

bool ReplaceAllIntegrationTimeLinesInHdt(char *NfIn, char *NfOut, char * newIntegrationTime)
{
	int sizeBuf;

	char *Buf = FillBuff(NfIn, sizeBuf);
	if (Buf == NULL)	return false;

	char Rubrique[] = "Integration Time =";
	int debutcourant = 0;

	while (true)
	{
		int linesize = 0;
		int index = FindLineInBuf(Rubrique, Buf, sizeBuf, linesize, debutcourant);

		if (index < 0) break;

		//cr�ation d'un buffer modifi�
		if (!ReplaceLineInBuf(Buf, sizeBuf, index, newIntegrationTime)) { delete Buf;  return false; }

		debutcourant = index + strlen(newIntegrationTime);
	}

	//sizeBuf contient maintenant la derniere taille

	//r��criture du fichier
	FILE *F = fopen(NfOut, "wb");
	if (F == NULL) { delete Buf;  return false; }

	bool ret = (fwrite(Buf, 1, sizeBuf, F) == sizeBuf);

	delete Buf;   fclose(F); return ret;

}

bool GetIntegrationTimeKey(char *Nf, char *key1000)
{
	int size;
	char * buf = FillBuff(Nf, size);
	if (!buf)	return false;

	int linesize;
	int index = FindLineInBuf("Integration Time =", buf, size, linesize, 0);

	if (linesize >= 1000) { delete buf; return false; }

	if (index == -1) { delete buf; return false; }

	memset(key1000, 0, 1000);
	memcpy(key1000, buf + index, linesize);
	return true;


}

bool ConversionRaw2CalibRikola(TParamCalibRikola &param, char * NfImageRawX, char* msg1000)
/****************************************************************************************/
{
	TNomFic NfImageRaw;
	strcpy(NfImageRaw, NfImageRawX);
	RemplacementExtension(NfImageRaw, "hdt");

	char keyIntegrationTime[1000];
	if(!GetIntegrationTimeKey(NfImageRaw, keyIntegrationTime))
	{
		sprintf(msg1000, "Echec r�cup�ration cl� Integration Time:\n%s", NfImageRaw);
		return false;
	}
		
	//cr�ation �ventuelle du dossier de sortie (sous-dossier Calib du dossier de la source)

	TNomFic DossierSource; strcpy(DossierSource, NfImageRaw);
	NomPath(DossierSource);

	TNomFic DossierOut;
	sprintf(DossierOut, "%s\\%s", DossierSource, SOUS_DOSSIER_CALIB);
	if (!CreateFolder(DossierOut))
	{
		sprintf(msg1000, "Echec cr�ation dossier:\n%s", DossierOut);
		return false;
	}

	//cr�ation du fichier de config

	TNomFic FichierIni;
	sprintf(FichierIni, "%s\\clc.ini", param.CLCFolder);
	ofstream os(FichierIni);
	if (!os.good())
	{
		sprintf(msg1000, "Echec ouverture en �criture:\n%s", FichierIni);
		return false;
	}

	os << "[General]\n";
	os << "CalibPath = \"" << param.DossierDataUsine << "\"\n";
	os << "OutputPath = \"" << DossierOut << '\n';
	os << "DarkrefPath = \"" << param.DossierDarkCurrent << "\"\n";
	os.close();
	


	//r�cup�ration nom de fichier de dark current utilis�
	_list<TNf> ListeDK;
	int NbDK = GetListeFichiers(ListeDK, param.DossierDarkCurrent, "hdt", false);
	if(NbDK!=1)
	{
		if (NbDK == 0) sprintf(msg1000, "Aucun fichier hdt dans le dossier:\n%s", param.DossierDarkCurrent);
		else sprintf(msg1000, "Trop de fichiers hdt dans le dossier:\n%s", param.DossierDarkCurrent);
		return false;
	}

	TNomFic NfDK;
	strcpy(NfDK, ListeDK.head->obj);

	//modification du hdt du fichier source selon fichier dark current utilis�
	if(!ReplaceDarkReferenceInHdt(NfImageRaw, NfImageRaw, NomSansPath(NfDK)))
	{
		sprintf(msg1000, "Echec modification du nom de fichier dark current dans le hdt de l'image raw");
		return false;
	}

	//modification du hdt de dark current selon temps d'int�gration
	if(!ReplaceAllIntegrationTimeLinesInHdt(NfDK, NfDK, keyIntegrationTime))
	
	{
		sprintf(msg1000, "Echec modification integration time dans le hdt du fichier dark current:n%s", NfDK);
		return false;
	}


	//construction ligne de commande

	TNomFic NomSourceCourt; strcpy(NomSourceCourt, NomSansPath(NfImageRaw));

	//nom du fichier en sortie
	TNomFic NomDestCourt;	sprintf(NomDestCourt, "Calib%s", NomSourceCourt + strlen("Rawdata"));
	NomDestCourt[strlen(NomDestCourt) - strlen("-(autosave).hdt")] = '\0';
	strcat(NomDestCourt, ".hdt");

	TNomFic LC;
	sprintf(LC, "\"%s\\clc.exe\" -i \"%s\" -o \"%s\" -c \"%s\\clc.ini\"", 
		param.CLCFolder, NomSourceCourt, NomDestCourt, param.CLCFolder);

	// ex�cution ligne de commande et retour

	bool ret = LanceEXE(LC, true, DossierSource);

	if(!ret) sprintf(msg1000, "Echec LanceExe. Ligne de commande:\n%s", LC);

	else //m�me si l'ex�cution semble correcte, on peut avoir eu un �chec --> on teste la pr�sence du hdr de sortie
	{
		TNomFic NomDest;
		sprintf(NomDest, "%s\\%s", DossierOut, NomDestCourt);
		RemplacementExtension(NomDest, "hdr");
		if (!FichierExistant(NomDest))
		{
			sprintf(msg1000, "Echec calibration (erreur interne CLC.Exe)");
			return false;
		}
		else
		{
			RemplacementExtension(NomDest, "dat");
			strcpy(msg1000, NomDest);
			return true;
		}
	}
}


/*******************************************************************/
/**************** APPARIEMENT DES BANDES ***************************/
/*******************************************************************/

//param�tres de d�tail

bool GereParamDistorsion(HWND Hparent, DistortionSettings &param, bool charge)
/************************************************************************************************/
{
	GROUPE G(Hparent, "Distortion parameters", "DistorsionFournieRikola.cfg",
		SEPART("-------Vis camera--------"),
		DBLX("Visible: xc", param.DC1.xc, 0, 100000, 30),
		DBLX("Visible: yc", param.DC1.yc, 0, 100000, 30),
		DBLX("Visible: a", param.DC1.a, -100, 100, 30),
		DBLX("Visible: b", param.DC1.b, -100, 100, 30),
		DBLX("Visible: c", param.DC1.c, -100, 100, 30),

		SEPART("-------Nir camera--------"),
		DBLX("Nir: xc", param.DC2.xc, 0, 100000, 30),
		DBLX("Nir: yc", param.DC2.yc, 0, 100000, 30),
		DBLX("Nir: a", param.DC2.a, -100, 100, 30),
		DBLX("Nir: b", param.DC2.b, -100, 100, 30),
		DBLX("Nir: c", param.DC2.c, -100, 100, 30),
		NULL);


	G.NbCharEdition = 40;

	return (charge ? G.Charge() : G.Gere());
}


bool GereParametresAppariementRikola(HWND Hparent, RegistrationSettings &RS, bool charge)
{
	bool DistorsionFournie = false;

	char * rubriquesLevel[] = { "None", "Image level",  "Detailed", NULL };
	int choixverbose = 0;

	RS.CUDABlockSize = 512;		//par d�faut

	GROUPE G(Hparent, "Param�tres appariement", "ParamRegistrationRikola.cfg",

		SEPART("------------ Pass 0--------------"),

		PBOOL("Initial translation computation with reduced images (pass0)", RS.WithPass0),
		ENT("Reduction factor", RS.Passe0ReductionFactor, 1, 10),

		SEPART("------------ Pass 1--------------"),

		PBOOL("With pass 1", RS.WithPass1),
		ENT("Subimage size pass 1", RS.Pass1Settings.SubimageSize, 50, 1000),
		FLT("Min peak ratio for angle detection pass 1", RS.Pass1Settings.MinAngleDetectionRatio, 1, 1000000),
		FLT("Min peak ratio for translation detection pass 1", RS.Pass1Settings.MinTranslationDetectionRatio, 1, 1000000),
		PBOOL("Variable scale pass 1", RS.Pass1Settings.VariableScale),
		FLT("Min ratio [links/subimages] pass 1", RS.Pass1Settings.MinLinkNumberRatio, 0, 1),
		ENT("Ransac tolerance for homography pass 1", RS.Pass1Settings.RansacTolerance, 0, 1000),
		ENT("Ransac iteration number pass 1", RS.Pass1Settings.RansacIterationsNumber, 1000, 100000),

		SEPART("------------ Pass 2--------------"),

		PBOOL("With pass 2", RS.WithPass2),
		ENT("Subimage size pass 2", RS.Pass2Settings.SubimageSize, 50, 1000),
		FLT("Min peak ratio for angle detection pass 2", RS.Pass2Settings.MinAngleDetectionRatio, 1, 1000000),
		FLT("Min peak ratio for translation detection pass 2", RS.Pass2Settings.MinTranslationDetectionRatio, 1, 1000000),
		PBOOL("Variable scale pass 2", RS.Pass2Settings.VariableScale),
		FLT("Min ratio [links/subimages] pass 2", RS.Pass2Settings.MinLinkNumberRatio, 0, 1),
		ENT("Ransac tolerance for homography pass 2", RS.Pass2Settings.RansacTolerance, 0, 1000),
		ENT("Ransac iteration number pass 2", RS.Pass2Settings.RansacIterationsNumber, 1000, 100000),

		SEPART("----------------------------------"),

		PBOOL("With distortion adjustment", RS.WithDistortionAjustment),
		FLT("Final tolerance", RS.FinalTolerance, 0, 1000),
		FLT("Final min ratio [links/subimages]", RS.FinalLinkNumberRatio, 0, 1),
		PBOOL("Distortion coefficients provided", DistorsionFournie),

		SEPART("----------------------------------"),
		MULCHOIX("Verbose level (appariement direct)", choixverbose, rubriquesLevel),
		ENT("Taille blocs CUDA", RS.CUDABlockSize, 256, 5000),

		NULL);


	bool ret = (charge ? G.Charge() : G.Gere());

	if (ret)
	{

		if (DistorsionFournie)
		{
			DistortionSettings *DS = new  DistortionSettings;
			ret = GereParamDistorsion(Hparent, *DS, charge);
			if (ret) //nouvelle saisie ou confirmation
			{
				if (RS.pDistortion != NULL)	delete RS.pDistortion;
				RS.pDistortion = DS;
			}

			else
			{
				if (RS.pDistortion != NULL)	delete RS.pDistortion;
				RS.pDistortion = NULL;
				DistorsionFournie = false;
				G.Sauve();
			}
		}
	}

	return ret;
}


extern bool AfficheProgression(char *msg, void *pUser);		//FRikola.cpp, pour lancement d'op�rations via le menu TRikola

void ResultLine(char *st, RegistrationResult &ret, RegistrationSettings &RS)
{
	ostrstream ost(st, MSGSIZE);

	if (ret.Status == RegistrationStatus::Completed)
	{
		ost << (RS.WithPass1 ? ret.SubImagesPass1 : -1) << '\t' << (RS.WithPass2 ? ret.SubImagesPass2 : -1) << '\t' << ret.Inliers << '\t' << ret.Index1 << '\t' << ret.Index2 << '\t' << ret.RMS << '\t' << ret.MaxError;
		ost << '\t' << ret.Tx << '\t' << ret.Ty << '\t' << ret.MinAngleRatio << '\t' << ret.MinTranslationRatio << '\0';
	}

	else ost << 0 << '\t' << 0 << '\t' << 0 << '\t' << 0 << '\t' << 0 << '\t' << 0 << '\t' << 0 << '\t' << 0 << '\t' << 0 << '\t' << 0 << '\t' << 0 << '\t' << '\0';

}


bool DoAppariementRikola(DRVTRAITIM *pD, TImageHyper<float> *src, TImageHyper<float> *dst, TParamCalibRikola &Param,
							RegistrationSettings &RS, 	char *msg1000, ostream *pOsRes = NULL)
{

	int dimX = src->H.Samples;
	int dimY = src->H.Lines;

	//extraction de la bande centrale

	int NumBandeCentrale;
	if (Param.BandeMaitresseCentrale) NumBandeCentrale = src->H.Bands / 2;
	else
	{
		int bandespecifiee = Param.BandeMaitresse;
		if (bandespecifiee<0 || bandespecifiee > src->H.Bands - 1) //v�rif
		{
			sprintf(msg1000, "Bande ma�tresse sp�cifi�e hors valeurs possibles (0 � %d)", src->H.Bands - 1);
			return false;
		}
		NumBandeCentrale = bandespecifiee;
	}


	BUFIMG<float> PlanCentral(dimX, dimY);
	if (!src->LitPlan(NumBandeCentrale, &PlanCentral))
	{
		sprintf(msg1000, "Echec lecture image source");
		return false;
	}

	//recopie directe du plan central

	if (!dst->EcritPlan(NumBandeCentrale, &PlanCentral))
	{
		sprintf(msg1000, "Echec �criture image destination");
		return false;
	}



	BUFIMG<float> PlanCourantSource(dimX, dimY);
	BUFIMG<float> PlanCourantRedresse(dimX, dimY);

	TNomFic DossierOut; //m�me dossier que l'image de destination
	strcpy(DossierOut, dst->NomComplet);
	NomPath(DossierOut);

	char NomGeneriqueCouple[100];

	

	//pr�paration du callback
	RegistrationCallBack CallBack;
	VerboseLevel verboselevel;

	if(pD==NULL)
	{
		verboselevel = VerboseLevel::None;
		CallBack.progression = NULL;
		CallBack.UserInfo = NULL;
	}

	else 
	{
		verboselevel = VerboseLevel::ImageLevel;
		CallBack.progression = AfficheProgression;
		CallBack.UserInfo = (void *)pD;
	}

	//pr�paration GlobalSettings pour l'appariement

	GlobalSettings GS;
	GS.RS = RS;
	GS.US.Level = verboselevel;
	GS.US.NirGain = 1;
	GS.US.NirRotation = false;
	GS.US.WithCudaCheck = false; //juste la premi�re fois


	BUFIMG<float> BRedressee(dimX, dimY);
	BUFIMG<U8> BMask(dimX, dimY);

	char ResultSt[100];


	//appariements successifs
	for (int b = 0; b < dst->H.Bands; b++)
	{
		if (b == NumBandeCentrale) continue;

		src->LitPlan(b, &PlanCourantSource);
		sprintf(NomGeneriqueCouple, "Couple B%d-B%d", NumBandeCentrale, b);


		if (pOsRes)	*pOsRes << NomGeneriqueCouple << '\t';

		/*TNomFic NfMapErreur;
		sprintf_s(NfMapErreur, "%s\\%s.map", DossierOut, NomGeneriqueCouple);
		ofstream osMapErr(NfMapErreur);*/


		RegistrationResult Ret = MultiModalRegistration(&PlanCentral, &PlanCourantSource, &BRedressee, &BMask,
			NomGeneriqueCouple, &GS, &CallBack);


		if (pOsRes)
		{
			ResultLine(ResultSt, Ret, GS.RS);
			*pOsRes << ResultSt << '\n';
		}


		GS.US.WithCudaCheck = false; //juste la premi�re fois

		if (Ret.Status != RegistrationStatus::Completed)
		{
			strcpy(msg1000, Ret.ReturnMsg);
			return false;
		}


		dst->EcritPlan(b, &BRedressee);

		//masque pas g�r� pour l'instant //XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

	}

	return true;
}



/*******************************************************************/
/**************** CORRECTION EN REFLECTANCE ************************/
/*******************************************************************/

bool LoadReferenceReflectance(MATRICE &MR, TParamCalibRikola &Param)
{
	return MR.Charge(Param.ReferenceReflectance);
}

bool GetInterpolatedReflectance(double nm, MATRICE MR, double &value)
{
	int Nb = MR.dimL();
	double lambdamin = MR[0][0];
	double lambdamax = MR[Nb - 10][0];
	if (nm< lambdamin || nm>lambdamax)	return false;

	//recherche des deux lambda encadrant la bande

	for (int i = 0; i < Nb - 1; i++)
	{
		double l1 = MR[i][0];
		double l2 = MR[i+1][0];

		if (l1 <= nm)
			if (l2 >= nm)
			{
				double k = (nm - l1)/(l2 - l1);
				value = MR[i][1] * (1 - k) + MR[i + 1][1] * k; //interpolation lin�aire

				return true;
			}
	}
	;
}


bool CorrectionReflectance(DRVTRAITIM *pD, TImageHyper<float> *src, TImageHyper<float> *dst, TParamCalibRikola &Param, char *msg1000)
{
	MATRICE MR;
	if (!MR.Charge(Param.ReferenceReflectance))
	{
		sprintf(msg1000, "Echec chargement %s:\n", Param.ReferenceReflectance);
		return false;
	}

	if (MR.dimC() != 2)
	{
		sprintf(msg1000, "Matrice de r�flectance r�f�rence non conforme (%d colonnes au lieu de 2)%s:\n",
			MR.dimC(), Param.ReferenceReflectance);
		return false;
	}

	//relev� du spectre moyen de luminance sur la r�gion de r�f�rence

	//reconstruction de la r�gion sp�cifi�e
	LIGNE LC = ~Param.RegionReference;
	Region Rg(LC);

	int NbBandes = src->H.Bands;
	double *SpectreMoyen = new double[NbBandes];
	if (!src->SpectreMoyen(Rg, SpectreMoyen))
	{
		sprintf(msg1000, "Spectre nul sur toute la r�gion de r�f�rence!");
		delete SpectreMoyen;
		return false;
	}


	//calcul du facteur de correction pour chaque bande
	VECTEUR FacteurCorrection(NbBandes);
	for (int b = 0; b < NbBandes; b++)
	{
		//r�cup�ration de la valeur de la bande en nm
		double nm = src->H.wavelength[b];

		double value;

		if (!GetInterpolatedReflectance(nm, MR, value))
		{

			sprintf(msg1000, "Les donn�es de r�flectance de la r�f�rence [%f, %f] ne couvrent pas la bande %d (%f nm)",
				MR[0][0], MR[MR.dimL() - 1][0], b, nm);
			delete SpectreMoyen;
			return false;
		}

		FacteurCorrection[b] = value / SpectreMoyen[b];
	}

	delete SpectreMoyen;

	//application de la correction � tous les pixels

	double *TabSpectre = new double[NbBandes];
	for (int y = 0; y < dst->H.Lines; y++)
	{
		if (pD)
		{
			char st[500];
			sprintf(st, "Correction en r�flectance: %d%%\n", (int)(100 * (double)y / dst->H.Lines));
			if (AfficheProgression(st, pD)) return false; //arr�t demand�
		}

		for (int x = 0; x < dst->H.Samples; x++)
		{
			if (!src->LitSpectre(x, y, TabSpectre))
			{
				sprintf(msg1000, "Erreur lecture image source");
				delete TabSpectre;
				return false;
			}

			for (int b = 0; b < NbBandes; b++)  TabSpectre[b] *= FacteurCorrection[b];

			if (!dst->EcritSpectre(x, y, TabSpectre))
			{
				sprintf(msg1000, "Erreur �criture image destination");
				delete TabSpectre;
				return false;
			}


		}
	}

	delete TabSpectre;

	return true;
}








/////////////////////////////////////////////////////////////////////////////////////////////////////
void TModuleRikola::CalibRikola(TRetourCalibRikola &ret, std::string FichierImageRaw, DRVTRAITIM *pD)
/////////////////////////////////////////////////////////////////////////////////////////////////////
{
	int TimeDebut = GetTickCount();

	TParamCalibRikola Param;
	if (!ChargeParametresCalib(Param))
	{
		ret.InfoRetour = "Echec chargement param�tres Calib Rikola";
		ret.State = TRetourState::Error;
		ret.DureeMs = GetTickCount() - TimeDebut;
		return;
	}

	RegistrationSettings RS;
	if (Param.AvecAppariement)  ///////////// met en �chec la modif hdt du darkcurrent si effectu� via le superviseur !!!
		 
	{
		if(!GereParametresAppariementRikola(NULL, RS, true))
		{
			ret.InfoRetour = "Echec chargement param�tres appariement bandes Rikola";
			ret.State = TRetourState::Error;
			ret.DureeMs = GetTickCount() - TimeDebut;
			return;
		}

	}



	char msg[1000] = "";
	TNomFic NfRaw; strcpy(NfRaw, FichierImageRaw.data());
	if (!ConversionRaw2CalibRikola(Param, NfRaw, msg))
	{
		ret.InfoRetour = "Echec calibration: " + string(msg);
		ret.State = TRetourState::Error;
		ret.DureeMs = GetTickCount() - TimeDebut;
		return;
	}



	//calibration OK; msg contient maintenant le nom du fichier calibr� (*.dat)
	// --> on la charge
	TNomFic NfCalib; strcpy(NfCalib, msg);
	TImageHyperGen *ImH = OuvreImageHyper(NfCalib, msg, 1000);

	TImageHyper<float>* ImCalib = (ImH != NULL ? dynamic_cast<TImageHyper<float> *>(ImH) : NULL);
	if (ImCalib == NULL)
	{
		ret.InfoRetour = "Echec relecture image calib pour appariement:\n" + string(msg);
		ret.State = TRetourState::Error;
		ret.DureeMs = GetTickCount() - TimeDebut;
		if (ImH)	delete ImH;
		return;
	}

	if (Param.EnMemoire)
	{
		if (pD) pD->PrintTexte("Chargement image calib en m�moire\n\n");

		if(!ImCalib->ChargeEnMemoire())
		{
			ret.InfoRetour = "Echec chargement image calib en m�moire";
			ret.State = TRetourState::Error;
			ret.DureeMs = GetTickCount() - TimeDebut;
			delete ImCalib;
			return;
		}
	}
	



	//retrouve le dossier source pour la suite (autres conversions)
	TNomFic DossierSource;
	strcpy(DossierSource, NfRaw); NomPath(DossierSource);



	if (Param.AvecAppariement)
	{
		//NB: oblig� de changer de dossier car � partir de maintenant le byte order de l'image sera diff�rent (Intel)
		// --> changer d'extension ne suffit pas, il faut un autre fichier hdr que pour l'mage Calib

		TNomFic DossierOut;
		sprintf(DossierOut, "%s\\%s", DossierSource, SOUS_DOSSIER_PAIRED);
		if (!CreateFolder(DossierOut))
		{
			ret.InfoRetour = "Echec cr�ation dossier:\n" + string(DossierOut);
			ret.State = TRetourState::Error;
			ret.DureeMs = GetTickCount() - TimeDebut;
			return;
		}
		

		//allocation image destination
		TNomFic NfPaired;
		
		sprintf(NfPaired, "%s\\%s\\%s", DossierSource, SOUS_DOSSIER_PAIRED, NomSansPath(NfCalib));

		RemplacementExtension(NfPaired, "paired"); 
		TImageHyper<float> *dst = AlloueImageH<float>(NfPaired, ImCalib->H, HYPERMEMOIRE, HYPERBSQ);

		if (dst == NULL)
		{
			ret.InfoRetour = "Echec allocation m�moire pour appariement:\n%s" + string(NfCalib);
			ret.State = TRetourState::Error;
			ret.DureeMs = GetTickCount() - TimeDebut;
			delete ImCalib; 
			return;
		}


		if (!DoAppariementRikola(pD, ImCalib, dst, Param, RS, msg))
		{
			ret.InfoRetour = "Echec appariement:\n" + string(msg);
			ret.State = TRetourState::Error;
			ret.DureeMs = GetTickCount() - TimeDebut;
			delete ImCalib; delete dst;
			return;
		}

		if(!dst->SauveMemoire(HYPERBSQ))
		{
			ret.InfoRetour = "Echec sauvegarde image:\n" + string(NfPaired);
			ret.State = TRetourState::Error;
			ret.DureeMs = GetTickCount() - TimeDebut;
			delete ImCalib; delete dst;
			return;
		}


		/////////////////////////////
		//on remplace l'image ImCalib
		/////////////////////////////
		delete ImCalib; ImCalib = dst;

	} //if (Param.AvecAppariement)

	///////////////////////////
	//correction en r�flectance
	///////////////////////////

	//allocation image destination
	TNomFic NfRef;

	sprintf(NfRef, "%s\\%s\\%s", DossierSource, SOUS_DOSSIER_REF, NomSansPath(NfCalib));

	RemplacementExtension(NfRef, "ref");
	TImageHyper<float> *ImRef = AlloueImageH<float>(NfRef, ImCalib->H, HYPERMEMOIRE, HYPERBSQ);

	if (ImRef == NULL)
	{
		ret.InfoRetour = "Echec allocation m�moire pour appariement:\n%s" + string(NfCalib);
		ret.State = TRetourState::Error;
		ret.DureeMs = GetTickCount() - TimeDebut;
		delete ImCalib;
		return;
	}

	if (!CorrectionReflectance(pD, ImCalib, ImRef, Param, msg))
	{
		ret.InfoRetour = "Echec correction en r�flectance:\n" + string(msg);
			ret.State = TRetourState::Error;
			ret.DureeMs = GetTickCount() - TimeDebut;
			delete ImCalib; delete ImRef;
			return;
	}



	delete ImCalib;

	//cr�ation �ventuelle du dossier
	TNomFic DossierOutRef;
	sprintf(DossierOutRef, "%s\\%s", DossierSource, SOUS_DOSSIER_REF);
	if (!CreateFolder(DossierOutRef))
	{
		ret.InfoRetour = "Echec cr�ation dossier:\n" + string(DossierOutRef);
		ret.State = TRetourState::Error;
		ret.DureeMs = GetTickCount() - TimeDebut;
		return;
	}



	if (!ImRef->SauveMemoire(HYPERBSQ))
	{
		ret.InfoRetour = "Echec sauvegarde image:\n" + string(NfRef);
		ret.State = TRetourState::Error;
		ret.DureeMs = GetTickCount() - TimeDebut;
		delete ImRef;
		return;
	}

	delete ImRef;
	ret.InfoRetour = string(NfRef); //nom du fichier calibr� 
	ret.State = TRetourState::Completed;
	ret.DureeMs = GetTickCount() - TimeDebut;
	return;


}



