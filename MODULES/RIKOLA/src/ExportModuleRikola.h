#ifndef _EXPORT_MODULE_RIKOLA__H
#define _EXPORT_MODULE_RIKOLA__H

//fonctions export�es pour utilisation externe (autres modules)

#include <string>
#include "commtype.h"
#include "WeedelecCommon.h"
#include "CalageWeedelec.h"
#include "list_tpl.h"
#include "ligne.h" //POINTFLT
#include "gereweeds.h" 
#include "drvtraiti.h" 


struct TParamAcqRikola
{
	int PourcentDPI; 
	//pourcentage �tabli dans les param�tres d'affichage
	//(mise � l'�chelle et disposition / Modifier la taille du texte)
	//impossible � r�cup�rer en live par fonction Windows

	TNomFic DossierImages;
	int TimeOutMs; //pour l'acquisition d'image

	TParamAcqRikola()
	{
		DossierImages[0] = '\0';
		TimeOutMs = 15000;
		PourcentDPI = 100;
	}
};

struct TParamCalibRikola
{
	//calibration radiom�trique (raw --> Calib)
	
	TNomFic CLCFolder;	//dossier contenant l'ex�cutable clc.exe
	TNomFic DossierDataUsine; //donn�es de calibration fournies par Rikola
	TNomFic DossierDarkCurrent; //dossier contenant l'image dark current g�n�rique

	bool EnMemoire; //chargement des images hyperspectrales en m�moire pour acc�l�ration du traitement

	//appariement des bandes
	bool AvecAppariement;
	bool BandeMaitresseCentrale;
	int BandeMaitresse;

	TNomFic ReferenceReflectance;

	LIGNE RegionReference; //pour la correction en r�flectance

	TParamCalibRikola()
	{
		CLCFolder[0] = '\0';
		DossierDataUsine[0] = '\0';
		DossierDarkCurrent[0] = '\0';

		EnMemoire = true;

		AvecAppariement = true;
		BandeMaitresseCentrale = true;
		BandeMaitresse = 0;

		ReferenceReflectance[0] = '\0';
	}
};

struct TParamDetectionRikola50
{
	TNomFic FichierCalage; //pour passage pixels--> espace robot

	TNomFic FichierModelesPLS; 
	bool AvecSNV;

	_list<int>	ClassesToDetect;

	int TailleWeedMin, TailleWeedMax;


	TParamDetectionRikola50()
	{
		FichierCalage[0] = '\0';
		FichierModelesPLS[0] = '\0';
		AvecSNV = false;
		TailleWeedMin = 100;
		TailleWeedMax = 1000;

	}
};


struct TParamDetectionRikola10
{
	TNomFic FichierCalage; //pour passage pixels--> espace robot

	TNomFic FichierModele;
	TNomFic FichierModeleSol;


	int TailleWeedMin, TailleWeedMax;
	bool ExtractionBandes; //au cas o� l'image source a 50 bandes


	TParamDetectionRikola10()
	{
		FichierCalage[0] = '\0';
		FichierModele[0] = '\0';
		FichierModeleSol[0] = '\0';
		TailleWeedMin = 100;
		TailleWeedMax = 1000;
		ExtractionBandes = true;
	}
};




struct TRetourAcqRikola : public TRetourTask
{
	// InfoRetour : nom complet du fichier image raw si succ�s (state Completed) , ou sinon description de l'erreur
	int NbTotalImages; //nombre total d'images acquises dans le dossier
	int DureeMs;
};

struct TRetourCalibRikola : public TRetourTask
{
	// InfoRetour : nom complet du fichier image corrig� en r�flectance si succ�s (state Completed) , ou sinon description de l'erreur
	int DureeMs;
};

struct TRetourDetectionRikola : public TRetourTask
{
	// InfoRetour : message avec nb de weeds si succ�s (state Completed) , ou sinon description de l'erreur
	_list<TWeedRobot> ListeWeeds;
	_list<POINTFLT> ListeWeedsImage;
	int DureeMs;
};




struct _EXPORT_ TModuleRikola
{
	TParamAcqRikola Param;
	HWND HFen;


	void PrepareAcquisition(TRetourAcqRikola &retour);
	
		
	//fonctions export�es pour utilisation externe (autres modules)

	static bool ChargeParametresAcq(TParamAcqRikola &param);
	static bool ChargeParametresCalib(TParamCalibRikola &param);
	static bool ChargeParametresDetection50(TParamDetectionRikola50 &param);
	static bool ChargeParametresDetection10(TParamDetectionRikola10 &param);

	void AcquisitionRikola(TRetourAcqRikola &ret, bool AvecGPS=false, double latdeg=0, double londeg=0);

	static void CalibRikola(TRetourCalibRikola &ret, std::string FichierImageRaw, DRVTRAITIM *pD=NULL); 
	//calibration radiom�trique. Si pD sp�cifi�, affichage possible de la progression de l'appariement sur fen�tre texte

	static void DetectionRikola50(TRetourDetectionRikola &ret, std::string FichierImageRef, DRVTRAITIM *pD = NULL, IMGRIS * imlabels=NULL);
	//d�tection de mauvaises herbes dans l'image sp�cifi�e 
	//dernier argument n�cessaire si d�tection manuelle pour visualiser l'image

	static void DetectionRikola10(TRetourDetectionRikola &ret, std::string FichierImageRef, DRVTRAITIM *pD = NULL, IMGRIS * imlabels = NULL);
	//d�tection de mauvaises herbes dans l'image sp�cifi�e 
	//dernier argument n�cessaire si d�tection manuelle pour visualiser l'image

	void Stop() { ; } //demande d'arr�t utilisateur

	static IMTCOULEUR * VisualiseImage(DRVTRAITIM *pD, char *Nf, char * msg2000);

	static TCalage2D * ChargeCalage(char * msgerr);


};


#endif


