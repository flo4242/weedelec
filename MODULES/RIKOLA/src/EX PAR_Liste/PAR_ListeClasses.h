#ifndef __PAR_ListeClasses_H
#define __PAR_ListeClasses_H

/* D�finition d'une classe de param�tre utilisable dans un GROUPE Winutil,
pour la saisie et l'archivage d'une liste d'entiers.


Syntaxe d'appel dans le groupe:
	...
	new PAR_ListeClasses("Liste � saisir", &liste),
	...


IMPORTANT: pour que la saisie soit possible, la bo�te de dialogue du GROUPE ne doit pas �tre modale
--> appel avec G.Gere(false) au lieu de l'appel par d�faut G.Gere() (Gere(bool IsModal = true) )
*/


#include <Windows.h>

#include "GroupeAPI.h"
#include "list_tpl.h"

class PAR_ListeClassesnButton; //r�f�rences crois�es

class  PAR_ListeClasses : public PAR_STR_TPL<TYPEGROUPEWIN>
{
#define TAILLESTRINGLISTE 1000
	friend class PAR_ListeClassesButton;
	char *NfModele; //pour �tablir la liste de choix en d�but de saisie

public:


	PAR_ListeClasses(char * titre, void *ad, char *NfModele);

	//cr�ation d'un contr�le double: edit + bouton de browsing
	virtual int TailleDLGITEMTEMPLATE();
	//retourne la taille de la place � r�server pour ce contr�le en fin de structure DLGTEMPLATE
	// de la bo�te de dialogue
	virtual int SetDLGITEMTEMPLATE(int &ID, int x, int y, int w, int h, void *data);
	//�tablit dans data les donn�es de DLGITEMTEMPLATE n�cessaires

	virtual int CreateChildren(PAR** listepar);
	virtual bool Saisie(char *str, HWND hwnd);   //d�clench� par le bouton	//saisie r�gion sur image

	virtual bool ChaineFinaleAcceptee(char *str) { return true; }
	virtual bool ChaineEnCoursAcceptee(char *str) { return true; } //saisie directe impossible

	virtual bool ConversionDepuisChaine(void *dest, char *src, bool); //surcharge

	virtual void ConversionVersChaine(void *source, char *dest, bool); //surcharge

};



class PAR_ListeClassesButton : public PAR
{
	friend class PAR;

public:

	PAR_ListeClasses * ParMaitre;
	PAR_ListeClassesButton(PAR_ListeClasses *par);

	virtual int TailleDLGITEMTEMPLATE() { return 0; }
	virtual int SetDLGITEMTEMPLATE(int &ID, int x, int y, int w, int h, void *data) { return 0; }
	//cr�� par le param�tre PAR_Region associ�

	virtual int TraiteMessage(HWND hwndDlg, int IDcourant, WORD notification);
	//virtual ostream& SauveValeur (ostream& os)  {return os;}    //rien car non autonome
	//virtual istream& ChargeValeur (istream& is) {return is;}

	virtual int NbChar(void) { return 0; }   //inutilis�
	virtual bool ConversionDepuisChaine(void *dest, char *src, bool) { return true; }
	virtual void ConversionVersChaine(void *source, char *dest, bool) { ; }

};

#endif