#include "PAR_ListeClasses.h"

#include <istream>
#include <strstream>

#include "winutil2.h"

extern bool GetInfoModele(char *NfModele, _list<int> &Classes, int &NbBandes, char * msg1000); //FRikola.cpp



PAR_ListeClasses::PAR_ListeClasses(char * titre, void *ad, char *Nfmodele) : PAR_STR_TPL<TYPEGROUPEWIN>(titre, ad, TAILLESTRINGLISTE)
{
	
	/* ne peut �tre fait ici car NfModele, qui fait partie du m�me groupe, n'est pas encore renseign� � la construction
	--> sera fait dans la fonction saisie
	if (GetInfoModele(NfModele, Classes, NbBandes, msg))
		MessageBox(NULL, msg, "Liste classes � d�tecter", MB_OK | MB_ICONERROR);
	else	ListeChoix = Classes;*/

	NfModele = Nfmodele;
}


bool GetNextValue(std::istream &is, int & val)
{
	if (is.eof()) return false;
	is >> val;
	char dummy;
	is.get(dummy);	if (dummy != ',')	return false; //on a fini
	return true;
}

bool PAR_ListeClasses::ConversionDepuisChaine(void *dest, char *src, bool)
{
	_list<int> *pListe = (_list<int> *) dest;

	std::istrstream is(src, TAILLESTRINGLISTE);

	while (true)
	{
		int val;
		if (!GetNextValue(is, val))	break;
		pListe->push_back(val);
	}
	return true;
}

void PAR_ListeClasses::ConversionVersChaine(void *source, char *dest, bool)
{
	_list<int> &Liste = * ((_list<int> *) source);

	std::ostrstream os(dest, TAILLESTRINGLISTE);

	BalayeListe(Liste)
	{
		os << *Liste.current();
		if (!Liste.end()) os << ", ";
	}
	os << '\0';
}


int PAR_ListeClasses::TailleDLGITEMTEMPLATE()
/***********************************/
{
	ConversionVersChaine(Adresse, ChaineTampon, false);
	return PAR::DoTailleDLGITEMTEMPLATE(ChaineTampon) + PAR::DoTailleDLGITEMTEMPLATE("...");
}

int PAR_ListeClasses::SetDLGITEMTEMPLATE(int &ID, int x, int y, int w, int h, void *data)
/********************************************************************************/
{
	double LargeurBouton = 3;
	//cr�ation espace d'�dition de cha�ne
	ConversionVersChaine(Adresse, ChaineTampon, false);        //r�cup�re valeur initiale
	int LargeurEdit = w - 4 * LargeurBouton;
	int taille = PAR::DoSetDLGITEMTEMPLATE(ID, x, y, LargeurEdit, h, data, ChaineTampon, EDITBROWSE, false, ES_READONLY);
	char *data2 = (char *)data + taille;    ID++;    //pointeur et ID pour la suite
	taille += PAR::DoSetDLGITEMTEMPLATE(ID, x + LargeurEdit, y, LargeurBouton * 4, h, data2, "...", BUTTON);
	return taille;
}

int PAR_ListeClasses::CreateChildren(PAR** listepar)
/******************************************/
{

	*listepar = this;
	*(listepar + 1) = new PAR_ListeClassesButton((PAR_ListeClasses *)this);
	return 2;
}

bool PAR_ListeClasses::Saisie(char *str, HWND hwnd)   //d�clench� par le bouton	//saisie entier dans liste
{
	_list<int> Classes;
	int NbBandes;
	char msg[1000];
	
	if (!GetInfoModele(NfModele, Classes, NbBandes, msg))
	{
		MessageBox(NULL, msg, "Liste classes � d�tecter", MB_OK | MB_ICONERROR);
	}

	_list<int> Temp = Classes;
	_list<int> ListeChoisie;

	while (true)	//saisie successive des num�ros de classe
	{
		//cr�ation liste de choix temporaire

		int NbChoix = Temp.nbElem;
		char** rubriques = new char *[NbChoix + 1];

		int i = 0;
		BalayeListe(Temp)
		{
			rubriques[i] = new char[20];
			sprintf(rubriques[i], "classe %d", *Temp.current());
			i++;
		}
		rubriques[NbChoix] = NULL;
		int Choix = -1;

		GROUPE G(hwnd, "Choix classe", "",
			MULCHOIX("Ajouter classe", Choix, rubriques),
			NULL);

		bool ret = G.Gere();

		for (i = 0; i < NbChoix; i++) delete rubriques[i]; //tous sauf le dernier (NULL)
		delete rubriques;
		
		if(!ret)	break;

		//r�duction de la liste temporaire
		int EntierChoisi = Temp[Choix].obj;
		Temp.remove(EntierChoisi);

		ListeChoisie.push_back(EntierChoisi);

	}

	ConversionVersChaine(&ListeChoisie, str, false);	//false arbitraire (inutilis�)
	return true;
}


PAR_ListeClassesButton::PAR_ListeClassesButton(PAR_ListeClasses *par)
/*************************************************/
	: ParMaitre(par), PAR("", par->Adresse, BUTTON) {
	Autonome = false;
}


int PAR_ListeClassesButton::TraiteMessage(HWND hwndDlg, int IDcourant, WORD notification)
/*********************************************************************************/
{
	if (notification == BN_CLICKED)
	{
		TNomFic str;     strcpy(str, ParMaitre->ChaineTampon);
		int ret = ParMaitre->Saisie(str, hwndDlg);
		if (ret)
		{
			strcpy(ParMaitre->ChaineTampon, str);
			ParMaitre->SetItemText(hwndDlg, IDcourant - 1, str);
		}

		return 1;
	}
	else return 0;
}
