#pragma once

#include "Winutil2.h"
#include "imtemplate.h"

#include "Par_Region.h"



struct TParamVegRikola
{
	double seuilNDVIVeg;
	double seuilNDVISol;
	double seuilRplusNIRmin;
	LIGNE RegionSol;

	TParamVegRikola()
	{
		seuilNDVIVeg = 0.55;
		seuilNDVISol = 0.50;
		seuilRplusNIRmin = 0.01;

	}
};


bool GereParametresVeg(HWND hwnd, ModeGereParametres modeGereParam, TParamVegRikola &param, DRVTRAITIM *pD);
BUFIMG<float> * GetNDVI(DRVTRAITIM *pD, TImageHyper<float> *ImReflectance, double seuilmin, char *msg1000);
bool SeuillageVeg(BUFIMG<float> *ndvi, TParamVegRikola &param, TImageHyper<float> *Ref, TImageHyper<float> *Veg, TImageHyper<float> *Sol, 
					char *msg);
