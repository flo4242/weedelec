#ifndef __PAR_ListeClasses_H
#define __PAR_ListeClasses_H

/* D�finition d'une classe de param�tre utilisable dans un GROUPE Winutil,
pour la saisie et l'archivage d'une liste d'entiers.


Syntaxe d'appel dans le groupe:
	...
	new PAR_ListeClasses("Liste � saisir", &liste),
	...


IMPORTANT: pour que la saisie soit possible, la bo�te de dialogue du GROUPE ne doit pas �tre modale
--> appel avec G.Gere(false) au lieu de l'appel par d�faut G.Gere() (Gere(bool IsModal = true) )
*/


#include <Windows.h>

#include "GroupeAPI.h"
#include "list_tpl.h"


class  PAR_ListeClasses : public PAR_MULCHOIX_TPL<TYPEGROUPEWIN>
{
#define TAILLESTRINGLISTE 1000
	char *FichierModele; //pour �tablir la liste de choix en d�but de saisie
	_list<int> *ListeClasses;

public:


	PAR_ListeClasses(char * titre, _list<int> *classes, char *NfModele) : PAR_MULCHOIX_TPL<TYPEGROUPEWIN>(titre, (int*)classes, NULL)
		// les rubriques seront d�termin�es num�riquement � la saisie
		//NB: cast temporaire en (int*) pour satisfaire la classe m�re, sans signification
		//(le champ Adresse du param�tre sera modifi� temporarirement lors de la saisie)
	{
		FichierModele = NfModele;
	}

	
	virtual bool Browse(char *str, HWND hwnd);   //d�clench� par le bouton	//saisie r�gion sur image

	virtual bool ConversionDepuisChaine(void *dest, char *src, bool); //surcharge

	virtual void ConversionVersChaine(void *source, char *dest, bool); //surcharge

};



#endif