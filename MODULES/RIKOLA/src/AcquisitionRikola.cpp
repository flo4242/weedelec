#include <Windows.h>"
#include <stdio.h>"
#include <fstream>"

#include "ListeGen.h"	


#include "ExportModuleRikola.h" //fonctions export�es pour utilisation externe (autres modules)
#include "FRikola.h"			//d�finitions partag�es par les sources du projet

#include "ligne.h"			//pour POINTFLT

#include "drvtraiti.h"
extern DRVTRAITIM *pDriverG; //pour debug

struct Fenetre
{
	HWND H;
	TNomFic Titre;
	int numTab; // indentation par rapport � la racine (pour affichage)

	_list<Fenetre*> Children;

	Fenetre() {Titre[0] = '\0'; }

	void SetTitre();

};

char *ExtraitTitreG = "Rikola";

bool GetWindowSize(HWND H, int &dimx, int &dimy)
/**********************************************/
{
	RECT RR;
	if (!GetWindowRect(H, &RR)) { dimx = dimy = -1; return 0; }

	dimx = RR.right - RR.left;
	dimy = RR.bottom - RR.top;
	return true;
}



void Fenetre::SetTitre()
{
	char TexteFen[1000];
	int ret = GetWindowText(H, TexteFen, 1000);
	if (!ret) strcpy(TexteFen, "Error GetWindowText()");

	int dimx, dimy;
	GetWindowSize(H, dimx, dimy);

	char buffer[1000] = "";

	for (int i = 0; i < numTab; i++)    strcat(Titre, "/.");
	sprintf(Titre + strlen(Titre), "/%s (%dx%d)", TexteFen, dimx, dimy);
}

BOOL CALLBACK ExploreWindowProc(HWND H, LPARAM pPere); //d�pendances crois�es


void ExploreFenetresEnfants(Fenetre *Pere)
/****************************************/
{
	EnumChildWindows(Pere->H, (WNDENUMPROC)ExploreWindowProc, (LPARAM)Pere);
	//le p�re est pass� en param�tre pour �luder les "petits-enfants" oct 2011
	// (ils seront pris au stade r�cursif suivant)
}


BOOL CALLBACK ExploreWindowProc(HWND H, LPARAM pPere)
/*************************************************/
{
Fenetre *Pere = (Fenetre *) pPere; 
if(Pere->H!=NULL)	if(GetParent(H)!= Pere->H)	return true;
//les "petits-enfants" seront trait�s � l'�tage r�cursif suivant oct 2011

Fenetre *current = new Fenetre();
current->H = H;
current->numTab = Pere->numTab +1;
current->SetTitre();
Pere->Children.push_back(current);

ExploreFenetresEnfants(current);
return true;
}




//---------------------------------------------------------------------------
bool CaracteresSuivantsIdentiques(char *buf1, char *buf2, int index1, int index2, int index2max)
/**********************************************************************************************/
//fonction r�entrante
{
if(buf1[index1]!=buf2[index2])  return false;
if(index2==index2max)   return true;    //on a fini
else return CaracteresSuivantsIdentiques(buf1, buf2, index1+1, index2+1, index2max);
}

bool ContientSousChaine(char *st, unsigned taille, char *souschaine)
/******************************************************************/
{
int n = min(strlen(st), taille);
n -= strlen(souschaine)-1;        // -1: BUG corrig� ao�t 2007 (sinon, ne marche pas si cha�nes identiques)
for(int i=0; i<n; i++)
    if(CaracteresSuivantsIdentiques(st, souschaine, i, 0, strlen(souschaine)-1)) return true;
return false;
}


BOOL CALLBACK TestFenetre(HWND H, LPARAM lparam)
/**********************************************/
{

	_list<HWND> *Liste = (_list<HWND> *) lparam;
	char buffer[100]="";
	if(GetWindowText(H, buffer, 100))
			if(ContientSousChaine(buffer, 100, ExtraitTitreG))
			   Liste->push_back(H);  
	return true;
}

HWND GetHandleFenetre(char* extraitTitre)
/***********************/
{
	ExtraitTitreG = extraitTitre;
	_list<HWND> Liste;

	HWND HFen=NULL;

	EnumWindows((WNDENUMPROC)TestFenetre, (LPARAM)&Liste);
	//cast (WNDENUMPROC) n�cessaire pour C++ builder

	//s'il y a plusieurs fen�tres on ne retient que celle dont la taille est la plus grande
	int maxdimx=0, maxdimy=0;
	BalayeListe(Liste)
	{
		int dimx, dimy;
		GetWindowSize(*Liste.current(), dimx, dimy);
		if(dimx>= maxdimx && dimy>=maxdimy)
			{
				HFen = *Liste.current();
				maxdimx = dimx;
				maxdimy = dimy;
			}
	}


	return HFen;
}


bool SendClicGauche(double xrel, double yrel)
{
	//If MOUSEEVENTF_ABSOLUTE value is specified, dx and dy contain normalized absolute coordinates between 0 and 65,535.
	//The event procedure maps these coordinates onto the display surface. Coordinate (0,0) maps onto the upper-left corner of the display surface;
	//coordinate (65535,65535) maps onto the lower-right corner. In a multimonitor system, the coordinates map to the primary monitor.

	INPUT Input;
	Input.type = INPUT_MOUSE;

	Input.mi.dx = xrel * 65535;
	Input.mi.dy = yrel * 65535;
	
	//on doit commencer par une instruction de positionnement
	Input.mi.mouseData = 0;
	Input.mi.time = 0;
	Input.mi.dwFlags = MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE; //OK
	Input.mi.dwExtraInfo = 0;

	if(SendInput(1, &Input, sizeof(INPUT)) !=1) return false;

	//simuation du clic gauche
	Input.mi.dwFlags = MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTDOWN;
	if(SendInput(1, &Input, sizeof(INPUT)) !=1)	return false;

	Input.mi.dwFlags = MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTUP;
	if(SendInput(1, &Input, sizeof(INPUT)) !=1)	return false;

	return true;
}

bool IsRed(COLORREF color)
{
	double R = color &0xFF;
	double V = (color &0xFF00)>>8;
	double B = (color &0xFF0000)>>16;
	return R>2*V && R>2*B;
}

bool IsGreen(COLORREF color)
{
	double R = color &0xFF;
	double V = (color &0xFF00)>>8;
	double B = (color &0xFF0000)>>16;
	return V>2*R && V>2*B;
}


POINTINT GetRealScreenCoordinates(POINTINT coordinates, int DPIPercent)
{
	return POINTINT(coordinates.x * DPIPercent/100., coordinates.y * DPIPercent/100.);
}

bool IsPointRed(POINTINT coordinates, int DPIPercent)
{
	POINTINT P = GetRealScreenCoordinates(coordinates, DPIPercent);
	COLORREF color = GetPixel(GetDC(NULL), P.x, P.y);
	return IsRed(color);

}

bool IsPointGreen(POINTINT coordinates, int DPIPercent)
{
	POINTINT P = GetRealScreenCoordinates(coordinates, DPIPercent);
	COLORREF color = GetPixel(GetDC(NULL), P.x, P.y);
	return IsGreen(color);

}


bool IsInList(_list<TNf> liste, TNf &nf)
{
	BalayeListe(liste)
		if(strcmp(*liste.current(), nf)==0)	return true;
	return false;
}


void SetRetourRikola(TRetourAcqRikola &ret, TRetourState retstate, string errormsg, int NbImages, int dureeMs)
{

	ret.State = retstate;

	switch (retstate)
	{
	case TRetourState::Completed:
		//l'info retour sera �tablie ailleurs
		break;

	case TRetourState::Error:
		ret.InfoRetour = errormsg;
		break;

	case TRetourState::TimeOut:
		ret.InfoRetour = errormsg;
		break;

	case TRetourState::ExternalStop:
		ret.InfoRetour = "Demande d'arr�t externe";
		break;
	}

	ret.NbTotalImages = NbImages;
	ret.DureeMs = dureeMs;
}


//////////////////////////////////////////////////////////////////////////
void TModuleRikola::PrepareAcquisition(TRetourAcqRikola &retour)
//////////////////////////////////////////////////////////////////////////
{
	int TimeDebut = GetTickCount();

	if (!ChargeParametresAcq(Param))
	{
		SetRetourRikola(retour, TRetourState::Error, "Echec chargement param�tres Rikola.cfg", -1, GetTickCount() - TimeDebut);
		return;
	}


	if (Param.DossierImages[0] == '\0')
	{
		SetRetourRikola(retour, TRetourState::Error, "Dossier d'acquisition non d�fini dans Rikola.cfg", -1, GetTickCount() - TimeDebut);
		return;
	}


	//r�cup�ration fen�tre
	HFen = GetHandleFenetre("Rikola");
	
	if (HFen == NULL)
	{
		SetRetourRikola(retour, TRetourState::Error, "Echec recherche fen�tre Rikola. V�rifier si l'application est ouverte",
			-1, GetTickCount() - TimeDebut);
		return;
	}

	else
	{
		SetRetourRikola(retour, TRetourState::Completed, "",-1, GetTickCount() - TimeDebut);
		return;
	}

	
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////
void TModuleRikola::AcquisitionRikola(TRetourAcqRikola &retour, bool avecGPS, double latdeg, double londeg)
////////////////////////////////////////////////////////////////////////////////////////////////////////////
{
	int TimeDebut = GetTickCount();

	int TimeOutMs = Param.TimeOutMs;

	

	//relev� du nombre actuel de fichiers images
	_list<TNf> ListeAvant;
	int NbFichiersAvant = GetListeFichiersSelonMasque(ListeAvant, Param.DossierImages, "Rawdata*.dat", false);



	int XScreen = GetSystemMetrics(SM_CXSCREEN);
	int YScreen = GetSystemMetrics(SM_CYSCREEN); //pour test couleur sur des voyants


	
	
	/*********************************************************************************************************/
	//maximise la fen�tre et lui donne le focus (ne fonctionne que si l'exe est lanc� en administrateur !!!!!)
	/*********************************************************************************************************/

	//ShowWindow(HFen, SW_RESTORE); //indispensable pour donner le focus (SetFocus() ne fonctionne pas sur un autre thread)
	//ShowWindow(HFen, SW_MINIMIZE); //indispensable pour donner le focus (SetFocus() ne fonctionne pas sur un autre thread)

	HWND HSauve = GetForegroundWindow();

	SetForegroundWindow(HFen);
	ShowWindow(HFen, SW_MAXIMIZE);

	
	POINTFLT PSaveRawData(1480./1600, 540./900); //bouton "save raw data" du bouton "Acquire hypercube" pour test
	POINTFLT PDarkStatusAnalysis(1178./1600, 704./900); //voyant " current status" de la page Analysis (pour relev� couleur)

	POINTFLT PAcquisition(1158./1600, 528./900); //bouton "Acquire hypercube"
	POINTFLT PAcquisitionCalibrate(1158./1600, 609./900); //bouton "Acquire & calibrate hypercube"
	POINTFLT PLiveImaging(1573./1600, 82./900); //bouton pour retour vers volet Live Imagering apr�s acquisition
	

	if(!SendClicGauche(PLiveImaging.x, PLiveImaging.y))
	{
		SetForegroundWindow(HSauve);
		SetRetourRikola(retour, TRetourState::Error, "Echec fonction SendClicGauche (pour live imaging)", 
			NbFichiersAvant, GetTickCount() - TimeDebut);
		return;
	}


	HDC hdc = GetDC(NULL); //pour lecture couleurs sur �cran
	
	double DPIScaleX = GetDeviceCaps(hdc, LOGPIXELSX) / 96.0f;
	double DPIScaleY = GetDeviceCaps(hdc, LOGPIXELSY) / 96.0f;
	//voir https://docs.microsoft.com/en-us/windows/desktop/learnwin32/dpi-and-device-independent-pixels



	while(true) //attente qu'on ne soit plus sur Analysis
	{
		//GetPixel requiert les coordonn�es pixel selon la r�solution r�elle (native) de l'�cran
		// Or DPIScale n'en tient pas compte !!!! (mai 2019)

		double DPIf = Param.PourcentDPI / 100.;
		int xcol = PDarkStatusAnalysis.x * XScreen * DPIf;	// DPIScaleX;
		int ycol = PDarkStatusAnalysis.y * YScreen *DPIf;	// DPIScaleY; // sans effet !

		
		COLORREF color = GetPixel(hdc, xcol, ycol);
		if(!IsRed(color) && !IsGreen(color))	break;

		int DureeMs = GetTickCount() - TimeDebut;
		if (DureeMs > TimeOutMs)
		{
			SetForegroundWindow(HSauve);
			ReleaseDC(NULL, hdc); 
			
			SetRetourRikola(retour, TRetourState::TimeOut, "Timeout attente retour depuis volet Analysis",
				NbFichiersAvant, GetTickCount() - TimeDebut);

			return;
		}

	}



	if(!SendClicGauche(PAcquisition.x, PAcquisition.y))
	{
		SetForegroundWindow(HSauve);
		ReleaseDC(NULL, hdc);
		SetRetourRikola(retour, TRetourState::Error, "Echec fonction SendClicGauche (pour acquisition)",
															NbFichiersAvant, GetTickCount() - TimeDebut);
		return;
	}

	//test fin d'acquisition: on va observer la couleur d'un point du voyant "dark reference status", pr�sent seulement dans la fen�tre analysis
	//--> non utilisable car commutation sur fen�tre analysis avant que la sauvegarde soit termin�e !!! (la calibration reste en cours)


	while(true) //attente qu'on soit sur Analysis
	{
		int DureeMs = GetTickCount() - TimeDebut;
		if( DureeMs > TimeOutMs)
		{
			SetForegroundWindow(HSauve);
			ReleaseDC(NULL, hdc);

			SetRetourRikola(retour, TRetourState::TimeOut, "Timeout attente retour vers volet Analysis",
				NbFichiersAvant, GetTickCount() - TimeDebut);

			return;
		}

		double DPIf = Param.PourcentDPI / 100.;
		int xcol = PDarkStatusAnalysis.x * XScreen * DPIf;	// DPIScaleX;
		int ycol = PDarkStatusAnalysis.y * YScreen *DPIf;	// DPIScaleY; // sans effet !

		//pour GetPixel(), les coordonn�es pixels doivent �tre en logical units
		// --> NON: inutile de tenir compte du DPI car on travaille d�j� en relatif/r�solution �cran !!! (janvier 2019)

		COLORREF color = GetPixel(hdc, xcol, ycol);
		
		if (IsRed(color) || IsGreen(color)) break;
	}

	Sleep(100); //s�curit� ?
	

	//r�cup�ration du nom de l'image
	_list<TNf> ListeApres;
	int NbFichiersApres = GetListeFichiersSelonMasque(ListeApres, Param.DossierImages, "Rawdata*.dat", false);


	char st[1000];


	if(NbFichiersApres!= NbFichiersAvant +1)
	{
		SetForegroundWindow(HSauve);
		ReleaseDC(NULL, hdc);

		sprintf(st, "Nombre anormal de fichiers acquis (%d)", NbFichiersApres - NbFichiersAvant);


		SetRetourRikola(retour, TRetourState::Error, st, NbFichiersApres, GetTickCount() - TimeDebut);
		return;
	}

	TNomFic NomImage = "";

	BalayeListe(ListeApres)
	{
		TNf Nf = *ListeApres.current();

		if(!IsInList(ListeAvant, Nf))
		{
			strcpy(NomImage, Nf);
			break;
		}
	}

	if(NomImage[0] == '\0')
	{
		SetForegroundWindow(HSauve);
		::ReleaseDC(NULL, hdc);

		SetRetourRikola(retour, TRetourState::Error, "Echec r�cup�ration nom de l'image", NbFichiersApres, GetTickCount() - TimeDebut);

		return;
	}
	
	//////////////    ACQUISITION OK ////////////////////////:

	SetForegroundWindow(HSauve);
	::ReleaseDC(NULL, hdc);

	SetRetourRikola(retour, TRetourState::Completed, NomImage, NbFichiersApres, GetTickCount() - TimeDebut);
	retour.InfoRetour = NomImage;

	if (avecGPS)
	{
		TNomFic NfRTK; strcpy(NfRTK, NomImage);
		RemplacementExtension(NfRTK, "rtk");

		ofstream os(NfRTK);
		os.precision(16);

		os << latdeg << '\n' << londeg << '\n';
	}
}

