
#include "CalageTrapeze.h"

void TCalageTrapeze::GetH33directe(H33 &H)
{
	H.m11 = m11;
	H.m12 = m12;
	H.m13 = m13;

	H.m21 = m21;
	H.m22 = m22;
	H.m23 = m23;
	
	H.m31 = m31;
	H.m32 = m32;
	H.m33 = m33;
}

void TCalageTrapeze::GetH33inverse(H33 &H)
{
	H.m11 = n11;
	H.m12 = n12;
	H.m13 = n13;

	H.m21 = n21;
	H.m22 = n22;
	H.m23 = n23;

	H.m31 = n31;
	H.m32 = n32;
	H.m33 = n33;
}


POINTFLT TCalageTrapeze::XYVersUV(double X, double Y)
/***************************************************************/
{
/*
VECTEUR YR(3);	YR[0]=X;	YR[1]=Y;	YR[2]=1;
MATRICE XT = m*YR;
POINTFLT P; P.x = XT[0][0]/XT[2][0];	P.y = XT[1][0]/XT[2][0];
*/

double su = m11*X + m12*Y + m13;
double sv = m21*X + m22*Y + m23;
double s =  m31*X + m32*Y + m33;

return POINTFLT(su/s, sv/s);
}

POINTFLT TCalageTrapeze::UVVersXY(double u, double v)
/****************************************************/
{

double sX = n11*u + n12*v + n13;
double sY = n21*u + n22*v + n23;
double s =  n31*u + n32*v + n33;

return POINTFLT(sX/s, sY/s);
}

double ResiduG;

bool TCalageTrapeze::Init(POINTFLT *TabXY, POINTFLT *TabUV, int nbpoints, double *pResidu, double *pErreurMax)
/************************************************************************************************************/
{
/* La matrice m � d�terminer est une matrice 3x3.
On se fixe m33 = 1 --> il reste 8 param�tres ind�pendants (voir m�thode m34 en calage cam�ra)

On a alors pour chaque point:

[ u	]			[	X Y 1 0 0 0 -Xu -Yu  ]
[ v ]  = 		[	0 0 0 X Y 1 -Xv -Yv  ]   *   [ m11 m12 m13 m21 m22 m23 m31 m32 ]T

Y      =                   X                  *                 A

On r�soud par moindres carr�s:     A = (XT X)-1  XT  Y
*/

double k = 1;

MATRICE Y(2*nbpoints, 1);
MATRICE X(2*nbpoints, 8);

m33 = 1; 	//hypoth�se de calcul

for(int u=0; u<nbpoints; u++)
	{
	int i = 2*u; int j = 2*u+1;
	Y[i][0] = m33*TabUV[u].x;		Y[j][0] = m33*TabUV[u].y;

	X[i][0] = TabXY[u].x;		X[i][1] = TabXY[u].y;		X[i][2] = 1;
	X[i][3] = X[i][4] = X[i][5] = 0;
	X[i][6] = - TabXY[u].x * TabUV[u].x;		X[i][7] = - TabXY[u].y * TabUV[u].x;

	X[j][0] = X[j][1] = X[j][2] = 0;
	X[j][3] = TabXY[u].x;		X[j][4] = TabXY[u].y;		X[j][5] = 1;
	X[j][6] = - TabXY[u].x * TabUV[u].y;		X[j][7] = - TabXY[u].y * TabUV[u].y;
	}

MATCARREE XXT = (~X)*X;
double d = XXT.DET();
if(fabs(XXT.DET())<1E-100)	return false;

bool err;
char msg1000[1000];
MATRICE A = XXT.InversionCholesky(&err, msg1000) * (~X) * Y;
if(err)	return false;

m11 = A[0][0];
m12 = A[1][0];
m13 = A[2][0];
m21 = A[3][0];
m22 = A[4][0];
m23 = A[5][0];
m31 = A[6][0];
m32 = A[7][0];


//matrice inverse pour aller de UV vers XY
MATCARREE M(3);
M[0][0] = m11;	M[0][1] = m12;	M[0][2] = m13;
M[1][0] = m21;	M[1][1] = m22;	M[1][2] = m23;
M[2][0] = m31;	M[2][1] = m32;	M[2][2] = m33;

MATRICE M1 = M.InverseLU(&err, msg1000);    //impossible par Choleski
if(err)	return false;
n11 = M1[0][0];	n12 = M1[0][1];	n13 = M1[0][2];
n21 = M1[1][0];	n22 = M1[1][1];	n23 = M1[1][2];
n31 = M1[2][0];	n32 = M1[2][1];	n33 = M1[2][2];

//calcul r�sidu moyen:
if(pResidu)
	{
	double R2=0;
	for(int u=0; u<nbpoints; u++)		R2 += (TabUV[u]-XYVersUV(TabXY[u].x, TabXY[u].y)).Norme2();
	*pResidu = (R2>0 ? sqrt(R2/nbpoints) : 0);
	}

//calcul r�sidu moyen:
if(pErreurMax)
	{
	*pErreurMax=0;
	for(int u=0; u<nbpoints; u++)	*pErreurMax = max(*pErreurMax, (TabUV[u]-XYVersUV(TabXY[u].x, TabXY[u].y)).Norme());
	}

return true;
}

bool TCalageTrapeze::Sauve(char *Nf)
/**********************************/
{
ofstream os(Nf);
if(!os.good())	return false;

os<<m11<<'\t'<<m12<<'\t'<<m13<<'\n';
os<<m21<<'\t'<<m22<<'\t'<<m23<<'\n';
os<<m31<<'\t'<<m32<<'\t'<<m33<<'\n';
os<<'\n';

os<<n11<<'\t'<<n12<<'\t'<<n13<<'\n';
os<<n21<<'\t'<<n22<<'\t'<<n23<<'\n';
os<<n31<<'\t'<<n32<<'\t'<<n33<<'\n';
os.close();
return true;
}



bool TCalageTrapeze::Charge(char *Nf)
/**********************************/
{
ifstream is(Nf);
if(!is.good())	return false;

is>>m11>>m12>>m13;
is>>m21>>m22>>m23;
is>>m31>>m32>>m33;

is>>n11>>n12>>n13;
is>>n21>>n22>>n23;
is>>n31>>n32>>n33;
return true;
}

