#pragma once

#include "list_tpl.h"
#include "Waypoint.h"
#include "PositionRobot.h"
#include "RepereLocal.h"
#include "Ligne.h"
#include "KML.h"
#include <vector>
#include "H33.h"

struct TGereWeeds; //r�f�rences crois�es

enum WeedState { NONE, A_TRAITER, DONE, DONE_COMPTE};
struct TDataWeed
{
	WeedState state;
	double Surface; //cm�
	double Hauteur; // cm
	TDataWeed(double surfacecm2, double hauteurcm, WeedState state)	: Surface(surfacecm2), Hauteur(hauteurcm), state(state) {;}
};

struct TWeedWGS84 : public TDataWeed
{
	Waypoint Position;
	TWeedWGS84() : TDataWeed(-1, -1, NONE) { ; } //pour listes
	TWeedWGS84(Waypoint pos, double surfacecm2, double hauteurcm, WeedState state = NONE) : TDataWeed(surfacecm2, hauteurcm, state), Position(pos) {;}
};


struct TWeedRepereLocal : public TDataWeed
{
	POINTFLT Position; //position dans le rep�re du robot

	TWeedRepereLocal() : TDataWeed(-1, -1, NONE) { ; } //pour listes
	TWeedRepereLocal(POINTFLT pos, double surfacecm2, double hauteurcm, WeedState state = NONE) : TDataWeed(surfacecm2, hauteurcm, state), Position(pos) { ; }
};

struct TWeedRobot : public TDataWeed
{
	POINTFLT Position; //position dans le rep�re du robot

	TWeedRobot() : TDataWeed(-1, -1, NONE)  { ; } //pour listes
	TWeedRobot(POINTFLT pos, double surfacecm2, double hauteurcm, WeedState state = NONE) : TDataWeed(surfacecm2, hauteurcm, state), Position(pos) { ; }

	//Calcul de la distance euclidienne entre deux weeds
	double getDistance(const TWeedRobot & bPoint) const
	{
		const double x_diff = this->Position.x - bPoint.Position.x;
		const double y_diff = this->Position.y - bPoint.Position.y;
		return std::sqrt(x_diff * x_diff + y_diff * y_diff);
	}


};

struct TWeedBras : public TDataWeed
{
	POINTFLT Position; //position dans le rep�re du bras

	TWeedBras(POINTFLT pos, double surfacecm2=-1, double hauteurcm=-1, WeedState state=NONE) : TDataWeed(surfacecm2, hauteurcm, state), Position(pos) { ; }
	TWeedBras() : TDataWeed(-1, -1, NONE) { ; }
	double getDistance(const TWeedBras & bPoint) const
	{
		const double x_diff = this->Position.x - bPoint.Position.x;
		const double y_diff = this->Position.y - bPoint.Position.y;
		return std::sqrt(x_diff * x_diff + y_diff * y_diff);
	}
};

enum metTrans{NODEF,GPS,HOMOGRAPHIE}; //M�thode permettant de calculer ListeWeedsRobotTrans
struct TGroupeWeeds
{
	time_t TimeAcq; //timestamp au moment de l'acquisition => identifieur du cycle
	Waypoint PosAcq; //position du robot lors de la prise d'image
	double CapDegAcq; //cap du robot lors de la prise d'image
	bool PosAcqValide;
	//Les listes contiennent des objets TWeedXxxx poss�dant un enum d'�tat (NONE, A_TRAITER ou DONE)
	_list<TWeedRobot> ListeWeedsRobot; //Liste des weeds dans le rep�re du robot au moment du cycle TimeAcq
	_list<TWeedRobot> ListeWeedsRobotTrans;
	_list<TWeedBras> ListeWeedsBrasTrans;
	
										  
										  //NB: si avancement manuel, les positions weeds peuvent �tre valides alors que PosAcq ne l'est pas

	metTrans methodeTransfoWeeds;
	H33 HDeplacement; //Homographie de d�placement lorsque metTrans est sur HOMOGRAPHIE
	bool ToRepereRobotActuel(_list<TWeedRobot> &ListeWeedsHere, TPositionRobot actuelle, TRepereLocal *pRepere, char *msgerr);
	//fonction surcharg�e pour convertir diectement le membre
	bool ToRepereRobotActuelGPS(TPositionRobot actuelle, TRepereLocal *pRepere, char *msgerr);
	//fonction utilisant l'homographie pour faire le recalage
	bool ToRepereRobotActuelHomographie(H33 HDeplacement, char *msgerr);
	bool ToRepereBrasActuel(char *msgerr, float TranslationDeltaX, float TranslationDeltaY);
	
	TGroupeWeeds(metTrans metPassage=NODEF) :methodeTransfoWeeds(methodeTransfoWeeds) { ; };
	int triggerEtatDesherbage();
};


struct TGereWeeds
{

	static bool ImportKML(char *Nf, _list<TWeedRepereLocal > &Liste, TRepereLocal *pRepere,  char *msgerr)
	{
		_list<Waypoint> ListeWP;
		if (!::ImporteKML(Nf, ListeWP, msgerr))	return false;

		if (!pRepere) { printf(msgerr, "Repere local non d�fini"); return false; }

		//cr�ation d'un seul groupe, puisqu'on ne connait pas les dates d'acquisition
		BalayeListe(ListeWP)
		{
			Waypoint *pWP = ListeWP.current();
			POINTFLT pos; pRepere->WGS84VersMetres(pWP->LatDeg, pWP->LonDeg, pos.x, pos.y);
			TWeedRepereLocal WR(pos, -1, -1);
			Liste.push_back(WR);
		}
		return true;
	}

	//NB: ajout si liste destination non vide

	static void RobotVersWGS84(_list<TWeedRobot> &listeR, _list<TWeedWGS84> &listeWW, TRepereLocal *pRepere, TPositionRobot &pos);
	static void WGS84VersRobot(_list<TWeedRobot> &listeR, _list<TWeedWGS84> &listeWW, TRepereLocal *pRepere, TPositionRobot &pos);
	

};