#ifndef __XMLPARSE__H
#define __XMLPARSE__H

#ifndef NULL
#define NULL 0L
#endif

/* fonction d'interface avec le parser Expat 2.1.0 (libexpat.dll)
pour faciliter le parsing de fichiers XML - GR avril 2010 */

struct TParamParse
	{
	void (* HandlerStart)(void *userData, const char *el, const char **attr);
	/* fonction appel�e � l'entr�e d'une balise quelconque <NNN aaa=xxx bbb=yyy ...>
	avec el="NNN", et attr la liste de chaines "aaa", "xxx", bbb", "yyy", etc.
	Les chaines attr[i] avec i pair sont donc des noms d'attributs, et les cha�nes
	attr[i] avec i impair leurs valeurs associ�es.
	La liste attr se termine par NULL.             */

	void (* HandlerEnd)(void *userData, const char *el);
	// fonction appel�e � la sortie d'une balise quelconque </NNN>

	void (* HandlerChar)(void *userData, const char *s, int len);
	/* fonction appel�e lors de la lecture de caract�res entre balises.
	Attention: c'est l'utilisateur qui doit maintenir les infos suffisantes
	(via les handlers start et end)	pour savoir entre quelles balises on
	en est lors de cet appel. Par ailleurs, il peut y avoir des concat�nations
	� faire sur plusieurs appels. Enfin, la cha�ne s n'est pas termin�e par '\0',
	il faut se baser sur sa longueur len.  */


	//gestion �ventuelle des zones CDATA (si non sp�cifi�, ces zones sont saut�es)
	void (* HandlerStartCdata)(void *userData);
	void (* HandlerEndCdata)(void *userData);
	void (* HandlerCharCdata)(void *userData, const char *s, int len);


	void *pUserData;
	/* pointeur vers une variable globale utilisateur � transmettre au parser,
	 et qui sera retourn�e avec chaque handler */

	TParamParse() :  HandlerStart(NULL),HandlerEnd(NULL),
					 HandlerChar(NULL), HandlerStartCdata(NULL),
					HandlerEndCdata(NULL), pUserData(NULL)	 			{;}
	};

bool ParseFichierXML(char *Nf, 	 TParamParse &pm, char *msg5000);
bool CDATAEnCours();	//g�r� par d�faut (si Handlers Cdata non sp�cifi�s)
//permet � l'utilisateur de skipper 


#endif
