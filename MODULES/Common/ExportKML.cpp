#include <windows.h>
#include <fstream>
#include "utilfic.h"
#include "KML.h"




bool ExportKML(char *Nf, char *NomDocument, _list<Waypoint> &ListeWP,  char *msg1000)
/***********************************************************************************/
{
	std::ofstream os(Nf);
	if(!os.good())
	{
		sprintf(msg1000, "Echec ouverture %s", Nf);
		return false;
	}



	//pour assurer un codage UTF8 (important pour les caract�res accentu�s)
	///////////////////////////////////////////////////////////////////////
	//os.imbue(std::locale(std::locale::empty(), new std::codecvt_utf8<wchar_t,0x10ffff,std::generate_header>));
	//os.imbue(std::locale(std::locale::empty(), new std::codecvt_utf8<wchar_t>));

	os<<"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"<<'\n';
	os<<"<kml xmlns=\"http://www.opengis.net/kml/2.2\">"<<'\n';
	os<<"<Document>"<<'\n';
	os<<"<name>"<<NomDocument<<"</name>"<<'\n';
	os.precision(16);

	BalayeListe(ListeWP)
	{
		Waypoint &WP = *ListeWP.current();
		os<<"<Placemark>"<<'\n';

		//conversions UTF8 du nom et de la description
		wchar_t *stwName =   ConversionUnicode(WP.Name);
		char * stutf8Name =  ConversionDepuisUnicode(stwName, NULL, true );

		wchar_t *stwDesc =   ConversionUnicode(WP.Description);
		char * stutf8Desc =  ConversionDepuisUnicode(stwDesc, NULL, true );


		//os<<"<name>"<<WP.Name<<"</name>"<<'\n';
		//os<<"<description>"<<WP.Description <<"</description>"<<'\n';
		os<<"<name>"<<stutf8Name<<"</name>"<<'\n';
		os<<"<description>"<<stutf8Desc <<"</description>"<<'\n';

		delete stwName; delete stwDesc;
		delete stutf8Name; delete stutf8Desc;

		os<<"<Point>"<<'\n';
		os<<"<coordinates>"<<WP.LonDeg<<','<<WP.LatDeg<<','<<WP.elevation <<"</coordinates>"<<'\n';
		os<<"</Point>"<<'\n';

		os<<"</Placemark>"<<'\n';
	}

	os<<"</Document>"<<'\n';
	os<<"</kml>"<<'\n';

	return true;
}


bool ExportLineStringKML(char *Nf, char *NomDocument, _list<Waypoint> &ListeWP, char *msg1000)
/**************************************************************************************/
{
	std::ofstream os(Nf);
	if (!os.good())
	{
		sprintf(msg1000, "Echec ouverture %s", Nf);
		return false;
	}



	//pour assurer un codage UTF8 (important pour les caract�res accentu�s)
	///////////////////////////////////////////////////////////////////////
	//os.imbue(std::locale(std::locale::empty(), new std::codecvt_utf8<wchar_t,0x10ffff,std::generate_header>));
	//os.imbue(std::locale(std::locale::empty(), new std::codecvt_utf8<wchar_t>));

	os << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << '\n';
	os << "<kml xmlns=\"http://www.opengis.net/kml/2.2\">" << '\n';
	os << "<Document>" << '\n';
	os << "<name>" << NomDocument << "</name>" << '\n';
	os.precision(16);

	os << "<Placemark>" << '\n';
		os << "\t<name>" << NomDocument << "</name>" << '\n';
		os << "\t<LineString>" << '\n';

			os << "\t\t<coordinates>" << '\n';

			BalayeListe(ListeWP)
			{
				Waypoint &WP = *ListeWP.current();
				os << WP.LonDeg << ',' << WP.LatDeg << ',' << WP.elevation << ' ';
			}
			os << "\n\t\t</coordinates>" << '\n';

			os << "\t</LineString>" << '\n';
		os << "</Placemark>" << '\n';

	os << "</Document>" << '\n';
	os << "</kml>" << '\n';

	return true;
}


bool ExportGPX(char *Nf, char *NomDocument, _list<Waypoint> &ListeWP, char *msg1000)
/***********************************************************************************/
{
	std::ofstream os(Nf);
	if (!os.good())
	{
		sprintf(msg1000, "Echec ouverture %s", Nf);
		return false;
	}

	os << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << '\n';
	os << "<gpx creator=\"INRAE- Weedelec-2020\">\n";
	

	os.precision(16);

			
	//export sous forme d'une piste unique

	os << "\t<trk>\n"; //une trk par piste
	os << "<name>" << NomDocument << "</name>" << '\n'; 

	os << "\t\t<trkseg>\n";

	BalayeListe(ListeWP)
	{
		Waypoint *WP = ListeWP.current();

		os << "\t\t\t<trkpt lat=\"" << WP->LatDeg << "\" lon=\"" << WP->LonDeg << "\">\n";

		os << "\t\t\t\t<ele>" << WP->elevation << "</ele>\n"; 
		//elevation indispensable pour visu GoogleEarth, m�me si nulle

		//la description du WP est cens�e contenir la date au format ISO

		os << "\t\t\t\t<time>" << WP->Description << "</time>\n";

		os << "\t\t\t</trkpt>\n";
	}

	os << "\t\t</trkseg>\n";
	os << "\t</trk>\n";
	os << "</gpx>\n";


	return true;
}