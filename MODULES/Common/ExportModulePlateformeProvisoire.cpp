
#include "TaskUtil.h"
#include "ExportModulePlateformeProvisoire.h"


#define DUREE_STEP 2000	
#define DUREE_WEEDING 5000	

task_completion_event<void> *tceExtG = NULL;

//avance la plateforme jusqu'� la position d'acquisition/d�sherbage suivante
void TModulePlateforme::GoToNextStep(TRetourPlateforme &ret)
{
	if (tceExtG) delete tceExtG;	tceExtG = new task_completion_event<void>;

	CreateDelayTask(DUREE_STEP, tceExtG).wait();
	ret.State = (tceExtG->_IsTriggered() ? TRetourTask::ExternalStop : TRetourTask::Completed);
	ret.DureeMs = DUREE_STEP;
	ret.InfoRetour = "";
}


void TModulePlateforme::Weeding(_list<POINTFLT> &weeds, TRetourPlateforme &ret)
{
	if (tceExtG) delete tceExtG;	tceExtG = new task_completion_event<void>;

	CreateDelayTask(DUREE_WEEDING, tceExtG).wait();
	ret.State = (tceExtG->_IsTriggered() ? TRetourTask::ExternalStop : TRetourTask::Completed);
	ret.DureeMs = DUREE_WEEDING;
	ret.InfoRetour = "";
}
	

void TModulePlateforme::Stop() //demande d'arr�t utilisateur
{
	tceExtG->set();
};


