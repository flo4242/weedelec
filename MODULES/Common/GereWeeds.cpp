
#include "GereWeeds.h"
#include <list>
#include "DBOUT.h"
extern double CapDegSiUnSeulFixeG; //ExporteModuleControleRobot


TWeedBras PositionRobotVersPositionBras(TWeedRobot &WR, float TranslationDeltaX, float TranslationDeltaY)
{
	TWeedBras WB;
	//transfert des donn�es
	TDataWeed * pdataB = &WB;
	TDataWeed * pdataR = &WR;
	*pdataB = *pdataR;

	//transfert de la position
	WB.Position.x = WR.Position.x - TranslationDeltaX;
	WB.Position.y = -(WR.Position.y - TranslationDeltaY);
	//Transfert du flag
	WB.state = WR.state;
	// le sens de l'axe Y du rep�re de bras est inverse que celle du rep�re robot

	return WB;
}

//Passage du repere robot actuel au repere bras actuel
bool TGroupeWeeds::ToRepereBrasActuel(char *msgerr, float TranslationDeltaX, float TranslationDeltaY) {
	BalayeListe(this->ListeWeedsRobotTrans)
	{
		TWeedRobot WR = *ListeWeedsRobotTrans.current();
		TWeedBras WB = PositionRobotVersPositionBras(WR, TranslationDeltaX, TranslationDeltaY); //le flag est transf�r� durant cette �tape
		this->ListeWeedsBrasTrans.push_back(WB);
	}
	return true;
}


//Change l'enum des mauvaises herbes d'un groupe � A_TRAITER si possible
int TGroupeWeeds::triggerEtatDesherbage() {
	int countWeedsATraiter=0;
	if (this->ListeWeedsRobot.nbElem > 0)
	{
		for (this->ListeWeedsRobot.begin(); !this->ListeWeedsRobot.end(); this->ListeWeedsRobot++) {
			TWeedRobot *curWeedBras = this->ListeWeedsRobot.current();
			if (curWeedBras->state == A_TRAITER) { countWeedsATraiter++; } //on retente de la traiter
			else if (curWeedBras->state == NONE) { curWeedBras->state = A_TRAITER; countWeedsATraiter++; } //On met la weed dans la liste � traiter
		}
	}
	return countWeedsATraiter;
}

//Passage dans le rep�re actuel d'un groupeweeds � l'aide d'une homographie de recalage
bool TGroupeWeeds::ToRepereRobotActuelHomographie(H33 HDeplacement, char *msgerr) {
	BalayeListe(this->ListeWeedsRobot)
	{
		TWeedRobot *pWR = this->ListeWeedsRobot.current();
		//mauvaise herbe dont les coordonn�es ont �t� d�finies avant le d�placement du robot
		//changement des coordonn�es selon le d�placement
		TWeedRobot newWR = *pWR; //contient aussi le flag d'�tat
		HDeplacement.Applique(pWR->Position.x, pWR->Position.y, newWR.Position.x, newWR.Position.y);
		this ->ListeWeedsRobotTrans.push_back(newWR);
	}
	return true;
}




void getTransformationPositions(H33 &M, TPositionRobot const &pos1, TPositionRobot const &pos2) {
	//Translation
	double Tx = (pos2.x - pos1.x);
	double Ty= (pos2.y - pos1.y);
	double Tz = (pos2.elevation - pos1.elevation);
	double RYawn = (pos2.CapDeg - pos1.CapDeg);
	if (RYawn > 180) { RYawn -= 360;}
	if (RYawn < -180) { RYawn += 360;}
	double RPitch = (pos2.pitchDeg - pos1.pitchDeg);
	if (RPitch > 180) { RPitch -= 360; }
	if (RPitch < -180) { RPitch += 360; }
	double RRoll = (pos2.rollDeg - pos1.rollDeg);
	if (RRoll > 180) { RRoll -= 360; }
	if (RRoll < -180) { RRoll += 360; }
	DBOUT("Tx: " << Tx << "\n");
	DBOUT("Ty: " << Ty << "\n");
	DBOUT("Tz: " << Tz << "\n");
	DBOUT("RYawn: " << RYawn << "\n");
	DBOUT("RPitch: " << RPitch << "\n");
	DBOUT("RRoll: " << RRoll << "\n");


	//passage en cos/sin
	std::vector<double> vcos, vsin;
	vcos.push_back(cos(RYawn * PI / 180.0));
	vsin.push_back(sin(RYawn * PI / 180.0));
	vcos.push_back(cos(RPitch * PI / 180.0));
	vsin.push_back(sin(RPitch * PI / 180.0));
	vcos.push_back(cos(RRoll * PI / 180.0));
	vsin.push_back(sin(RRoll * PI / 180.0));

	//Matrice de transformation 3x3
	M.m11 = vcos[1] * vcos[0];
	M.m12 = vcos[1] * vsin[0];
	M.m21 = vsin[2] * vsin[1] * vcos[0] - vcos[2] * vsin[0];
	M.m22 = vsin[2] * vsin[1] * vsin[0] + vcos[2] * vcos[0];
	M.m13 = Tx;
	M.m23 = Ty;
	M.m31 = 0;
	M.m32 = 0;
	M.m33 = 1;

}

//Surcharge pour inscrire directement la nouvelle liste dans GW-> ListeWeedsRobotTrans
bool TGroupeWeeds::ToRepereRobotActuelGPS(TPositionRobot actuelle, TRepereLocal *pRepere, char *msgerr) {
	if (!pRepere) { sprintf(msgerr, "Lecture buffer weeds: rep�re local non d�fini"); return false; }
	
	if (!PosAcqValide) { sprintf(msgerr, "Lecture buffer weeds: position d'acquisition inconnue"); return false; }
	TPositionRobot PosRobotAcq;
	PosRobotAcq.CapDeg = CapDegAcq;
	pRepere->WGS84VersMetres(PosAcq.LatDeg, PosAcq.LonDeg, PosRobotAcq.x, PosRobotAcq.y);



	//Nouvelle m�thode prenant en compte les 3 angles
	//=========================================================================
	//On a les deux positions robot avec leurs 3 angles associ�s
	//On calcule la matrice de rotation que l'on associe � la translation pour obtenir la matrice de transformation entre les deux r�p�
	//H33 M33;
	//getTransformationPositions(M33,PosRobotAcq, actuelle);
	//DBOUT("matrice 3x3 obtenue: \n" << M33.m11 << " | " << M33.m12 << " | " << M33.m13 << "\n");
	//DBOUT(M33.m21 << " | " << M33.m22 << " | " << M33.m23 << "\n");
	//DBOUT(M33.m31 << " | " << M33.m32 << " | " << M33.m33 << "\n");
	////On peut alors l'appliquer aux �l�ments de ListeWeedsRobot pour obtenir ListeWeedsRobotTrans
	//BalayeListe(this->ListeWeedsRobot) {
	//	TWeedRobot *pWR = this->ListeWeedsRobot.current();
	//	TWeedRobot newWR = *pWR; //contient aussi le flag d'�tat
	//	M33.Applique(pWR->Position.x, pWR->Position.y, newWR.Position.x, newWR.Position.y);
	//	this->ListeWeedsRobotTrans.push_back(newWR);
	//}

	//DBOUT("positions avec nouvelle correction \n");
	//BalayeListe(ListeWeedsRobotTrans) {
	//	TWeedRobot *pWeed = ListeWeedsRobotTrans.current();
	//	DBOUT(pWeed->Position.x << " | " << pWeed->Position.y <<"\n");
	//}
	//=========================================================================



	//ListeWeedsRobotTrans.clear(); //POUR TESTS, A SUPPRIMER



	//Ancienne m�thode: marche tr�s bien pour les transformations 2D simples
	//=========================================================================
	_list<TWeedWGS84> listeWW;
	TGereWeeds::RobotVersWGS84(ListeWeedsRobot, listeWW, pRepere, PosRobotAcq);
	TGereWeeds::WGS84VersRobot(ListeWeedsRobotTrans, listeWW, pRepere, actuelle);
	DBOUT("positions avec ancienne correction \n");
	BalayeListe(ListeWeedsRobotTrans) {
		TWeedRobot *pWeed = ListeWeedsRobotTrans.current();
		DBOUT(pWeed->Position.x << " | " << pWeed->Position.y << "\n");
	}
	//=========================================================================




	return true;
}

bool TGroupeWeeds::ToRepereRobotActuel(_list<TWeedRobot> &ListeWeedsHere, TPositionRobot actuelle, TRepereLocal *pRepere, char *msgerr)
{
	if (!pRepere) { sprintf(msgerr, "Lecture buffer weeds: rep�re local non d�fini"); return false; }
	if (!PosAcqValide) { sprintf(msgerr, "Lecture buffer weeds: position d'acquisition inconnue"); return false; }

	//position d'acquisition du robot dans le rep�re local actuel

	//conversion de la position d'acquisition du robot dans le rep�re local
	//(NB: la PosAcq du groupe de weeds est disponible en (lat, lon), au cas o� ce groupe aurait �t� r�cup�r� sur fichier ou
	// acquis avant r�initialisation du rep�re local)

	TPositionRobot PosRobotAcq;
	PosRobotAcq.CapDeg = CapDegAcq;
	pRepere->WGS84VersMetres(PosAcq.LatDeg, PosAcq.LonDeg, PosRobotAcq.x, PosRobotAcq.y);

	_list<TWeedWGS84> listeWW;
	TGereWeeds::RobotVersWGS84(ListeWeedsRobot, listeWW, pRepere, PosRobotAcq);
	TGereWeeds::WGS84VersRobot(ListeWeedsHere, listeWW, pRepere, actuelle);

	return true;
}

void TGereWeeds::RobotVersWGS84(_list<TWeedRobot> &listeR, _list<TWeedWGS84> &listeWW, TRepereLocal *pRepere, TPositionRobot &pos)
{
	//rotation vecteur dans rep�re robot --> vecteur dans rep�re local:
	double anglerad = pos.CapDeg * (M_PI/180);
	double cosa = cos(anglerad);
	double sina = sin(anglerad);

	//translation vecteur dans rep�re robot --> vecteur dans rep�re local:
	POINTFLT TRL(pos.x, pos.y);

	BalayeListe(listeR)
	{
		TWeedRobot *pWR = listeR.current(); //rep�re robot

		//rep�re local
		POINTFLT PRL(pWR->Position.x*cosa - pWR->Position.y*sina + TRL.x, pWR->Position.x*sina + pWR->Position.y*cosa + TRL.y);

		Waypoint WP;  pRepere->MetresVersWGS84(WP.LatDeg, WP.LonDeg, PRL.x, PRL.y);

		TWeedWGS84 WW(WP, pWR->Surface, pWR->Hauteur);


		WW.state = pWR->state;
		listeWW.push_back(WW);

	}
}

void TGereWeeds::WGS84VersRobot(_list<TWeedRobot> &listeR, _list<TWeedWGS84> &listeWW, TRepereLocal *pRepere, TPositionRobot &pos)
{
	//rotation vecteur dans rep�re robot --> vecteur dans rep�re local:
	double anglerad= pos.CapDeg *(M_PI / 180);
	double cosa = cos(anglerad);
	double sina = sin(anglerad);

	//translation vecteur dans rep�re robot --> vecteur dans rep�re local:
	POINTFLT TRL(pos.x, pos.y);

	BalayeListe(listeWW)
	{
		TWeedWGS84 *pWW = listeWW.current(); //rep�re WGS84

		POINTFLT PRL; //coordonn�es weed dans rep�re local
		pRepere->WGS84VersMetres(pWW->Position.LatDeg, pWW->Position.LonDeg, PRL.x, PRL.y);

		//coordonn�es dans rep�re robot:
		PRL -= TRL;  //prise en compte translation
		POINTFLT PR(PRL.x*cosa + PRL.y*sina, -PRL.x*sina + PRL.y*cosa);

		TWeedRobot WR(PR, pWW->Surface, pWW->Hauteur);
		WR.state = pWW->state;
		listeR.push_back(WR);

	}
}




