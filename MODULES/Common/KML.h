﻿#ifndef __KML__H
#define __KML__H

#include "Waypoint.h"
#include "list_tpl.h"


bool ImporteKML(char *NfKML, _list<Waypoint> &ListeW, char *msg1000);

bool ExportKML(char *Nf, char *NomDocument, _list<Waypoint> &ListeWP, char *msg1000);
//NomDocument est celui qui apparaîtra si chargement dans Google Earth

bool ExportLineStringKML(char *Nf, char *NomDocument, _list<Waypoint> &ListeWP, char *msg1000);
//format ligne de GoogleEarth
//NomDocument est celui qui apparaîtra si chargement dans Google Earth

bool ExportGPX(char *Nf, char *NomDocument, _list<Waypoint> &ListeWP, char *msg1000);



#endif
