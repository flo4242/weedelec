
#pragma once

#include "imgris.h"
#include "list_tpl.h"

//////////////////////////////////////
//structures de calage caméra --> bras
//////////////////////////////////////

class TCalage2D //classe abstraite pour éluder les détails d'implémentation
{
public:

	virtual bool Init(POINTFLT *Pixels, POINTFLT *Metres, int NbPoints) = 0;
	virtual bool Sauve(char *Nf) = 0;
	virtual bool Charge(char *Nf) = 0;
	virtual POINTFLT Pixels2Metres(POINTFLT Pixels) = 0;

	double TCalage2D::ResiduMetres(POINTFLT *Pixels, POINTFLT *Metres, int NbPoints)
	{
		double r2 = 0;
		for (int i = 0; i < NbPoints; i++)
		{
			POINTFLT P = Pixels2Metres(Pixels[i]);
			r2 += (P - Metres[i]).Norme2();
		}

		return r2 > 1E-30 ? sqrt(r2) / NbPoints : 0;
	}



	virtual ~TCalage2D() { ; }

};



