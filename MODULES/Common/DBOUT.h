#pragma once
#include <sstream>
#include <iostream>

#define DBOUT( s )            \
{                             \
   std::ostringstream os_;    \
	os_.precision(10); \
   os_ <<  s << "\n";                   \
   OutputDebugString( os_.str().c_str() );  \
}