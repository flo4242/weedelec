#ifndef __CIBLES__RHEA__HH
#define __CIBLES__RHEA__HH

#include "imcoul.h"
#include "list_tpl.h"
#include "bufimtpl.h"
#include "GroupeTargets.h"

struct TParamRelax
	{
	bool Lissage;
	double ConstanteLissage; //pixels
	int NbIter;
	TParamRelax() : Lissage(true), ConstanteLissage(3), NbIter(10) {;} 
	};

RRECT FenetreAnalyseLocale(int xc, int yc, RRECT &Fsource, TParamCible &param );
void RGBVersHSI(IMCOULEUR *imS, IMCOULEUR *imD);	//imD et imS suppos�s de m�mes dimensions 
bool SeuilleSelonLut(IMCOULEUR *imC, IMGRIS *im, TParamCible &param, char *msg1000);
int RechercheObjetBinaire(IMGRIS *im, TParamCible &param, _list<POINTFLT> &ListeCentresCibles);

bool RelaxeCible(IMCOULEUR *imC, POINTFLT Debut, POINTFLT &Fin, TParamRelax &paramR, 
				TParamCible &param, char *msg1000 );	//RelaxeCible.cpp


bool RechercheCibles(IMCOULEUR *imC, TParamCible &param, _list<POINTFLT> &ListeCentresCibles, char *msg1000,
		_list<LIGNE> &ListeTraceEllipses, int NbPointsLigneEllipse=100);


#endif

