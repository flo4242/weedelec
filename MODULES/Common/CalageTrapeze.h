﻿
#ifndef __TCALAGETRAPEZE__H
#define __TCALAGETRAPEZE__H

#include "matrice.h"
#include "ligne.h"	//pour POINTFLT
#include "H33.h"

struct TCalageTrapeze
/*structure de relation linéaire entre images permettant de transformer
un trapèze en rectangle.
Inspiré du modèle de calage d'une caméra, mais en se limitant à 2D, soit:

su         X
sv =   m   Y
s          1

*/
{
//MATRICE m;
//pour accélérer:
double m11, m12, m13, m21, m22, m23, m31, m32, m33;

//matrice inverse de m:
double n11, n12, n13, n21, n22, n23, n31, n32, n33;

POINTFLT XYVersUV(double X, double Y);
POINTFLT UVVersXY(double u, double v);

bool Init(POINTFLT *TabXY, POINTFLT *TabUV, int nbpoints, double *pResidu=NULL, double *pErreurMax=NULL);

void GetH33directe(H33 &H);
void GetH33inverse(H33 &H);


bool Sauve(char *Nf);
bool Charge(char *Nf);

};

#endif
