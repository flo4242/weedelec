
//d'apr�s https://docs.microsoft.com/en-us/cpp/parallel/concrt/how-to-create-a-task-that-completes-after-a-delay?view=vs-2017


concurrency::task<void> CreateDelayTask(unsigned int delaiMs, task_completion_event<void> *tceExt)
{

	
	// A task completion event that is set when a timer fires, ou par le cancellation_token..;
	task_completion_event<void> tceTimer;

	// Create a non-repeating timer.
	auto fire_once = new timer<int>(delaiMs, 0, nullptr, false);

	// Create a call object that sets the completion event after the timer fires.
	auto callback = new call<int>([tceTimer](int)	{		tceTimer.set();	});

	
	// Connect the timer to the callback and start the timer.
	fire_once->link_target(callback);
	fire_once->start();

	// Create a task that completes after the completion event is set.
	//task<void> event_set(tceTimer);

	task<void> timeout_task(tceTimer);

	if (tceExt != NULL)  //possibilit� d'arr�te anticip�
		timeout_task = timeout_task || task<void>(*tceExt);
	
	// Create a continuation task that cleans up resources and
	// and return that continuation task.
	return timeout_task.then([callback, fire_once]()
	{
		delete callback;
		delete fire_once;
	});


}



template <class T> task<T> CreateTaskWithTimeout(task<T> t, cancellation_token_source &cts, unsigned int timeout, bool &timeoutReached,
	task_completion_event<void> *tceExt)
{
	//t�che d�lai qui retourne T, pour pouvoir l'associer � la t�che principale
	task<T> timeout_task = CreateDelayTask(timeout, tceExt).then([]()
	{
		T t0;	return t0;
	});


	bool *ptimeoutReached = &timeoutReached; //capture directe de timeoutReached dans .then() non accept�e

	return (timeout_task || t).then([t, cts, ptimeoutReached, tceExt](T t0)
	{
		if (t.is_done() || cts.get_token().is_canceled()) // t�che t termin�e ou annul�e de l'ext�rieur
		{
			*ptimeoutReached = false;
			//if (tceExt) tceExt->set(); //on arr�te timeout_task 
			//ligne ci-dessus supprim�e car d�valide tceExt pour des appels ult�rieurs
			//--> on laisse le timer qui n'est pas nocif, et en plus est probablement d�truit d�s la sortie de la pr�sente fonction
		}
		else
		{
			*ptimeoutReached = true;
			cts.cancel(); ////on arr�te la t�che t
		}
		
		return t0;
	});

}

template <class T> T DoTaskWithTimeout(task<T> t, cancellation_token_source &cts, unsigned int timeout, TaskWithTimeoutState &state,
	task_completion_event<void> *tceExt)
{
	T Ret;
	bool timeoutReached;

	try
	{
		Ret = CreateTaskWithTimeout(t, cts, timeout, timeoutReached, tceExt).get(); //ex�cution
		
		if(timeoutReached) state.state = TaskWithTimeoutState::Timeout;
		
		else if (cts.get_token().is_canceled())	state.state = TaskWithTimeoutState::ExternallyCancelled;

		else	state.state  = TaskWithTimeoutState::Completed;

		return Ret;
	}

	// apparemment le timeout ne g�n�re pas d'exception, mais au cas o� ... (observ� dans version pr�c�dente)

	catch (std::exception e)
	{
		if (cts.get_token().is_canceled())
		{
			if (timeoutReached)	state.state = TaskWithTimeoutState::Timeout;
			else	state.state = TaskWithTimeoutState::ExternallyCancelled;
		}

		else //c'est une erreur interne � la t�che
		{
			state.state = TaskWithTimeoutState::Error;
			state.ErrorMsg = e.what();
		}

		return Ret; //valeur par d�faut
		
	}
}


// SPECIALISATIONS POUR LE TYPE VOID

task<void> CreateTaskWithTimeout(task<void> t, cancellation_token_source &cts, unsigned int timeout, bool &timeoutReached,
	task_completion_event<void> *tceExt)
{
	//t�che d�lai qui retourne T, pour pouvoir l'associer � la t�che principale
	task<void> timeout_task = CreateDelayTask(timeout, tceExt);

	bool *ptimeoutReached = &timeoutReached; //capture directe de timeoutReached dans .then() non accept�e

	return (timeout_task || t).then([t, cts, ptimeoutReached, tceExt]()
	{
		if (t.is_done() || cts.get_token().is_canceled()) // t�che t termin�e ou annul�e de l'ext�rieur
		{
			*ptimeoutReached = false;
			//if (tceExt) tceExt->set(); //on arr�te timeout_task 
			//ligne ci-dessus supprim�e car d�valide tceExt pour des appels ult�rieurs
			//--> on laisse le timer qui n'est pas nocif, et en plus est probablement d�truit d�s la sortie de la pr�sente fonction
		}
		else
		{
			*ptimeoutReached = true;
			cts.cancel(); ////on arr�te la t�che t
		}

	});

}

 void DoTaskWithTimeout(task<void> t, cancellation_token_source &cts, unsigned int timeout, TaskWithTimeoutState &state,
	 task_completion_event<void> *tceExt)
{
	bool timeoutReached;

	try
	{
		CreateTaskWithTimeout(t, cts, timeout, timeoutReached, tceExt).wait(); //ex�cution

		if (timeoutReached) state.state = TaskWithTimeoutState::Timeout;

		else if (cts.get_token().is_canceled())	state.state = TaskWithTimeoutState::ExternallyCancelled;

		else	state.state = TaskWithTimeoutState::Completed;
		
	}



	catch (std::exception e)
	{
		if (cts.get_token().is_canceled())
		{
			if (timeoutReached)	state.state = TaskWithTimeoutState::Timeout;
			else	state.state = TaskWithTimeoutState::ExternallyCancelled;
		}

		else //c'est une erreur interne � la t�che
		{
			state.state = TaskWithTimeoutState::Error;
			state.ErrorMsg = e.what();
		}

	}
}

