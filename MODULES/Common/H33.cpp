#include "H33.h"
#include "Matrice.h"

H33 Inverse(H33 H)
{

	MATCARREE M(3);

	M[0][0] = H.m11;
	M[0][1] = H.m12;
	M[0][2] = H.m13;
	M[1][0] = H.m21;
	M[1][1] = H.m22;
	M[1][2] = H.m23;
	M[2][0] = H.m31;
	M[2][1] = H.m32;
	M[2][2] = H.m33;
		
	MATCARREE MI = M.InverseLU();

	H33 HI;
	HI.m11 = MI[0][0];
	HI.m12 = MI[0][1];
	HI.m13 = MI[0][2];
	HI.m21 = MI[1][0];
	HI.m22 = MI[1][1];
	HI.m23 = MI[1][2];
	HI.m31 = MI[2][0];
	HI.m32 = MI[2][1];
	HI.m33 = MI[2][2];

	return HI;
	
	   
}