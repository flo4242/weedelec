#pragma once
//#include<chrono>




enum TRetourState 
{
		Completed,
		TimeOut,
		Error,
		ExternalStop
};

struct TRetourTask //type de retour commun � tous les modules
{
	TRetourState State;
	std::string InfoRetour; //description de l'erreur ou info sp�cifique selon sous-classe
	std::string nomTache;
};

enum ModeGereParametres
{
	Gere, Charge, Sauve
};

