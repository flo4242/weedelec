
//ceci est un exemple d'implantation d'un callback sur un cancellation_token
//gardé pour mémoire même si en pratique, cela ne fonctionne pas sur un timer
//(callback déclenché seulement en fin de tâche)

concurrency::task<void> CreateDelayTask(unsigned int delaiMs, cancellation_token &ctExtern, task_completion_event<void> *tceExt)
{

	
	// A task completion event that is set when a timer fires, ou par le cancellation_token..;
	task_completion_event<void> tceTimer;

	// Create a non-repeating timer.
	auto fire_once = new timer<int>(delaiMs, 0, nullptr, false);

	// Create a call object that sets the completion event after the timer fires.
	auto callback = new call<int>([tceTimer](int)	{		tceTimer.set();	});

	
	//gestion du token d'arrêt externe par un callback pour arrêter le timer 
		
	struct ctcallback 
	{
		task_completion_event<void> tce;
		timer<int> *ptimer;

		ctcallback(task_completion_event<void> ttce, timer<int> *pptimer)		//le callback a besoin de paramètres pour agir
		{
			tce = ttce; ptimer = pptimer;
		}
		
		void operator()()
		{
			//::MessageBox(NULL, "coucou", "", MB_OK);
			ptimer->stop();
			tce.set(); //provoque la fin de la tâche
		}
	};
	
	ctcallback *ctCallback = new ctcallback(tceTimer, fire_once);

	
	cancellation_token_registration ctCallbackRegistration;

	if (ctExtern != cancellation_token::none())	ctCallbackRegistration = ctExtern.register_callback(*ctCallback);

	

	// Connect the timer to the callback and start the timer.
	fire_once->link_target(callback);
	fire_once->start();

	// Create a task that completes after the completion event is set.
	task<void> event_set(tceTimer);
	task<void> event2_set(*tceExt);
	
	// Create a continuation task that cleans up resources and
	// and return that continuation task.
	return (event_set||event2_set).then([callback, fire_once, ctExtern, tceTimer, ctCallbackRegistration, ctCallback]()
	{
		if (ctExtern != cancellation_token::none())
			if (ctExtern.is_canceled()) ctExtern.deregister_callback(ctCallbackRegistration); //supprime le callback sur ct

		delete callback;
		delete fire_once;
		delete ctCallback;
	});


}

