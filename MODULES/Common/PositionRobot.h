#pragma once

struct TPositionRobot
{
	double x, y;
	double elevation;
	double CapDeg; //sens trigonom�trique.
	double rollDeg;
	double pitchDeg;
	//NB: 0� si avant du robot dirig� vers le nord, ce qui correspond � la d�finition trigo standard:
	// angle nul de l'axe X du robot par rapport � l'axe X g�ographique (vers l'Est)

	int fixGauche, fixDroit, fix;

	bool CapDispo;  //septembre 2019.  Etabli et utilis� par PGPSReceiver::GetRobotPosition()
	
};
