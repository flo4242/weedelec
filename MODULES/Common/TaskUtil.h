#pragma once

#include <ppltasks.h>
#include <agents.h> //pour timer
#include <string>



using namespace concurrency;
using namespace std;


struct TaskWithTimeoutState
{
	enum 
	{
		Completed,
		ExternallyCancelled,
		Timeout,
		Error
	} state;

	std::string ErrorMsg;
};


// Creates a task that just completes after the specified delay
// possibilit� d'arr�t anticip� avec l'event tceExt si sp�cifi� (faire tceExt->set())
task<void> CreateDelayTask(unsigned int delaiMs, task_completion_event<void> *tceExt = NULL);

//NB: l'arr�t anticip� via un cancellation_token ne fonctionne pas. M�me en enregistrant un callback, ce callback
// n'est appel� qu'en fin de t�che !...
 



// Associe une t�che existante avec un timeout.
// IMPORTANT: Pour pouvoir �tre annul�e si le timeout est atteint
// - cette t�che doit �tre associ�e � un cancellation_token
// - la source de ce cancellation_token doit �tre fournie
// - si le type T n'est pas void, il doit avoir un constructeur par d�faut

//NB: si l'on veut en outre pouvoir faire un arr�t anticip� du timeout, il faut fournir un event (voir CreateDelayTask() )

template <class T> task<T> CreateTaskWithTimeout(task<T> t, cancellation_token_source &cts, unsigned int timeout, bool &timeoutReached,
													task_completion_event<void> *tceExt = NULL);


//ex�cute une t�che annulable munie d'un timeout en g�rant les exceptions d'annulation ou d'erreur interne � la t�che
//voir CreateTaskWithTimeout() pour le d�tail des param�tres

template <class T> T DoTaskWithTimeout(task<T> t, cancellation_token_source &cts, unsigned int timeout, TaskWithTimeoutState &state,
					task_completion_event<void> *tceExt = NULL);

#include "TaskUtil.hpp"