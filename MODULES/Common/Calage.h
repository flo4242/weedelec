
#pragma once

#include "imgris.h"
#include "list_tpl.h"
#include "CalageWeedelec.h"
#include "CalageTrapeze.h"
#include "WeedelecCommon.h" //ModeGereParametres
#include "imcoul.h"


class Trapeze : public TCalage2D
{
private:
	TCalageTrapeze CT;

public:
	bool Init(POINTFLT *Pixels, POINTFLT *Metres, int NbPoints)
	{
		return CT.Init(Pixels, Metres, NbPoints);
	}


	bool Sauve(char *Nf) { return CT.Sauve(Nf); }
	bool Charge(char *Nf) { return CT.Charge(Nf); }

	POINTFLT Pixels2Metres(POINTFLT Pixels) { return CT.XYVersUV(Pixels.x, Pixels.y); }


};


//////////////////////////////////////
//d�tection des cibles sur plaque mire
//////////////////////////////////////



struct TParamCalage
{
	int SurfaceMin;
	int SurfaceMax;
	double RatioAxesMax;	//pour test ellipse
	double ResiduRelatifParPoint; //pour test ellipse
	TNomFic FichierCalage;
	TNomFic FichierCalageCibles;
	TNomFic FichierLUT;
	int NbCibles; //nb de cibles attendues (test� par les programmes appelants, non par la fonction de d�tection)
	TParamCalage() : SurfaceMin(5), SurfaceMax(1000), RatioAxesMax(1.2), ResiduRelatifParPoint(1), NbCibles(15) { ; }
};

bool SeuilleSelonLut(IMCOULEUR *imC, IMGRIS *im, char *NfLut, char *msg1000);

bool GereParametresCibles(HWND hwnd, char *NfParam, ModeGereParametres modeGereParam, char *NfLUT, TParamCalage &param);
 
void RechercheCibles(IMGRIS *imBinaire, TParamCalage &param, _list<POINTFLT> &ListeCentresCibles,
	_list<LIGNE> &ListeTraceEllipses, int NbPointsLigneEllipse, _list<string> &listeErreurs);

//ordonne les cibles dans un tableau selon la num�rotation sur la plaque mire, et non selon l'ordre de d�tection
bool OrdonneCibles(_list<POINTFLT> &ListeCentresCibles, POINTFLT * TabCibles, char *msgerr);

POINTFLT * TabPositionsMire();
int NbPositionsMire();

string CalageGenerique(TParamCalage param);

void test();