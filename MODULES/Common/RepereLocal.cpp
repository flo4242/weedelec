#include <math.h>
#include "RepereLocal.h"
#include "ligne.h"
#include "DBOUT.h"

TRepereLocal::TRepereLocal(double latdeg0, double londeg0)
/********************************************************/
{
//a = 6378388.0;	b = 6356911.9461;	//donn�es ED50
a = 6378137.0;		b = 6356752.314;	//donn�es WGS84
LatDeg0 = latdeg0; 
LonDeg0 = londeg0;

double cosL = cos(LatDeg0*(M_PI/180));
double sinL = sin(LatDeg0*(M_PI/180));


//rayon du parall�le local
/* une latitude L, on a x = a.cosL et z = b.sinL;
Le rayon est tout simplement x */
RlocalH = a*cosL;

//rayon �quivalent en vertical
/* le plan tangent � l'ellipso�de en (x = a.cosL, z = b.sinL) a pour vecteur support:
T = (dx/dL, dy/dL), soit T = (- a.sinL, b.cosL);
Un d�placement ds le long de T correspond � une variation de latitude dL �gale � l'angle entre R et R + Tn.ds,
o� R est le vecteur  (x = a.cosL, z = b.sinL);

Or au premier ordre: dL = sin(dL) = (R)^(R +Tn.ds)/(|R|.|R + Tn.ds| = N/D avec:
	N = (R)^(R +Tn.ds) = (R)^(Tn.ds) = (R^Tn).ds
	D = |R|.|R + Tn.ds| =~ R�
soit dL = N/D =~ (R^Tn)/R�.ds = (Rn^Tn)/R.ds = (sin(Beta)/R).ds, o� Beta est l'angle entre R et T

On a donc RlocalV = ds/dL = R/(sin(angle(R,T));  
NB: si R et T perpendiculaires (excentricit� nulle), RlocalV = R */

POINTFLT R(a*cosL, b*sinL);
POINTFLT T(-a*sinL, b*cosL);
double sinBeta = (R^T)/(R.Norme()*T.Norme());
RlocalV = R.Norme()/sinBeta;
}


void TRepereLocal::WGS84VersMetres(double LatDeg, double LonDeg, double &Xm, double &Ym)
/**************************************************************************************/
{

	double deltaLatRad = (LatDeg -LatDeg0)*(M_PI/180);
	double deltaLonRad = (LonDeg -LonDeg0)*(M_PI/180);


	Xm = RlocalH*deltaLonRad;
	Ym = RlocalV*deltaLatRad;
}

void TRepereLocal::MetresVersWGS84(double &LatDeg, double &LonDeg, double Xm, double Ym)
/**************************************************************************************/
{

	double deltaLonRad = Xm/RlocalH;
	double deltaLatRad = Ym/RlocalV;

	LatDeg =  deltaLatRad*(180./M_PI) + LatDeg0;
	LonDeg =  deltaLonRad*(180./M_PI) + LonDeg0;

}

