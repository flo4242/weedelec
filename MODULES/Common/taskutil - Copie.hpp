
//d'apr�s https://docs.microsoft.com/en-us/cpp/parallel/concrt/how-to-create-a-task-that-completes-after-a-delay?view=vs-2017


concurrency::task<void> CreateDelayTask(unsigned int delaiMs, cancellation_token &ctExtern)
{

	
	// A task completion event that is set when a timer fires, ou par le cancellation_token..;
	task_completion_event<void> tceTimer;

	// Create a non-repeating timer.
	auto fire_once = new timer<int>(delaiMs, 0, nullptr, false);

	// Create a call object that sets the completion event after the timer fires.
	auto callback = new call<int>([tceTimer](int)	{		tceTimer.set();	});

	
	//gestion du token d'arr�t externe par un callback pour arr�ter le timer 
		
	struct ctcallback 
	{
		task_completion_event<void> tce;
		timer<int> *ptimer;

		ctcallback(task_completion_event<void> ttce, timer<int> *pptimer)		//le callback a besoin de param�tres pour agir
		{
			tce = ttce; ptimer = pptimer;
		}
		
		void operator()()
		{
			::MessageBox(NULL, "coucou", "", MB_OK);
			ptimer->stop();
			tce.set(); //provoque la fin de la t�che
		}
	};
	
	ctcallback *ctCallback = new ctcallback(tceTimer, fire_once);

	
	cancellation_token_registration ctCallbackRegistration;

	if (ctExtern != cancellation_token::none())	ctCallbackRegistration = ctExtern.register_callback(*ctCallback);

	

	// Connect the timer to the callback and start the timer.
	fire_once->link_target(callback);
	fire_once->start();

	// Create a task that completes after the completion event is set.
	//task<void> event_set(tceTimer);
	task<void> event_set(tceTimer, ctExtern);

	// Create a continuation task that cleans up resources and
	// and return that continuation task.
	return event_set.then([callback, fire_once, ctExtern, tceTimer, ctCallbackRegistration, ctCallback]()
	{
		if (ctExtern != cancellation_token::none())
			if (ctExtern.is_canceled()) ctExtern.deregister_callback(ctCallbackRegistration); //supprime le callback sur ct

		delete callback;
		delete fire_once;
		delete ctCallback;
	});


}



template <class T> task<T> CreateTaskWithTimeout(task<T> t, cancellation_token_source &cts, unsigned int timeout, bool &timeoutReached)
{
	cancellation_token_source  ctsTimeout;
	
	//t�che d�lai qui retourne T, pour pouvoir l'associer � la t�che principale
	task<T> timeout_task = CreateDelayTask(timeout, ctsTimeout.get_token()).then([]()
	{
		T t0;	return t0;
	});


	bool *ptimeoutReached = &timeoutReached; //capture directe de timeoutReached dans .then() non accept�e

	return (timeout_task || t).then([t, cts, ctsTimeout, ptimeoutReached](T t0)
	{
		if (t.is_done() || cts.get_token().is_canceled()) // t�che t termin�e ou annul�e de l'ext�rieur
		{
			*ptimeoutReached = false;
			ctsTimeout.cancel(); //on arr�te timeout_task 
		}
		else
		{
			*ptimeoutReached = true;
			cts.cancel(); ////on arr�te la t�che t
		}
		
		return t0;
	});

}

template <class T> T DoTaskWithTimeout(task<T> t, cancellation_token_source &cts, unsigned int timeout, TaskWithTimeoutState &state)
{
	T Ret;
	bool timeoutReached;

	try
	{
		Ret = CreateTaskWithTimeout(t, cts, timeout, timeoutReached).get(); //ex�cution
		
		if(timeoutReached) state.state = TaskWithTimeoutState::Timeout;
		
		else if (cts.get_token().is_canceled())	state.state = TaskWithTimeoutState::ExternallyCancelled;

		else	state.state  = TaskWithTimeoutState::Completed;

		return Ret;
	}

	// apparemment le timeout ne g�n�re pas d'exception, mais au cas o� ... (observ� dans version pr�c�dente)

	catch (std::exception e)
	{
		if (cts.get_token().is_canceled())
		{
			if (timeoutReached)	state.state = TaskWithTimeoutState::Timeout;
			else	state.state = TaskWithTimeoutState::ExternallyCancelled;
		}

		else //c'est une erreur interne � la t�che
		{
			state.state = TaskWithTimeoutState::Error;
			state.ErrorMsg = e.what();
		}

		return Ret; //valeur par d�faut
		
	}
}

task<void> CreateTaskWithTimeout(task<void> t, cancellation_token_source &cts, unsigned int timeout, bool &timeoutReached)
{
	cancellation_token_source  ctsTimeout;

	//t�che d�lai qui retourne T, pour pouvoir l'associer � la t�che principale
	task<void> timeout_task = CreateDelayTask(timeout, ctsTimeout.get_token());

	bool *ptimeoutReached = &timeoutReached; //capture directe de timeoutReached dans .then() non accept�e

	return (timeout_task || t).then([t, cts, ctsTimeout, ptimeoutReached]()
	{
		if (t.is_done() || cts.get_token().is_canceled()) // t�che t termin�e ou annul�e de l'ext�rieur
		{
			*ptimeoutReached = false;
			ctsTimeout.cancel(); //on arr�te timeout_task 
		}
		else
		{
			*ptimeoutReached = true;
			cts.cancel(); ////on arr�te la t�che t
		}

	});

}

 void DoTaskWithTimeout(task<void> t, cancellation_token_source &cts, unsigned int timeout, TaskWithTimeoutState &state)
{
	bool timeoutReached;

	try
	{
		CreateTaskWithTimeout(t, cts, timeout, timeoutReached).wait(); //ex�cution

		if (timeoutReached) state.state = TaskWithTimeoutState::Timeout;

		else if (cts.get_token().is_canceled())	state.state = TaskWithTimeoutState::ExternallyCancelled;

		else	state.state = TaskWithTimeoutState::Completed;
		
	}



	catch (std::exception e)
	{
		if (cts.get_token().is_canceled())
		{
			if (timeoutReached)	state.state = TaskWithTimeoutState::Timeout;
			else	state.state = TaskWithTimeoutState::ExternallyCancelled;
		}

		else //c'est une erreur interne � la t�che
		{
			state.state = TaskWithTimeoutState::Error;
			state.ErrorMsg = e.what();
		}

	}
}

