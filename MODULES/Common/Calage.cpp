#include "drvtraiti.h"
#include "Calage.h"
#include "binaire.h"
#include "EnsembleRegions.h"
#include "EllipseFitting.h"
#include "drvtraiti.h"
#include "PasserelleTraitimOpenCV.h"
//POUR THIN PLATE SPLINE
#include <vector>
#include <highgui.h>
#define INTER_CUBIC	2 //d'apr�s types_c.h (pas r�ussi � l'inclure simplement)
#include "CThinPlateSpline.h"  //OpenCv
#include "matrice.h"
#include "utilfic.h"
#include "winutil2.h" //GROUPE
#include "LutRVB.h"
#include "DBOUT.h"


DRVTRAITIM *pDriverG; //sert notamment � afficher des messages et � r�cup�rer puis traiter l'image en cours ici

bool SeuilleSelonLut(IMCOULEUR *imC, IMGRIS *im, char *NfLut, char *msg1000)
/********************************************************************************/
{
	TLut Lut;
	if (!Lut.Charge(NfLut)) { sprintf(msg1000, "Echec chargement lut:\n%s", NfLut);	return false; }

	Lut.Applique(imC, im);
	return true;
}

bool GereParametresCibles(HWND hwnd, char *NfParam, ModeGereParametres modeGereParam, char *NfLUT, TParamCalage &param)
{

	GROUPE G(hwnd, "Param�tres d�tection cibles", NfParam, 
		FICH("Fichier LUT", NfLUT, "lut"),
		ENT("Surface minimale (pixels)", param.SurfaceMin, 10, 50000),
		ENT("Surface maximale (pixels)", param.SurfaceMax, 10, 50000),
		NULL);

	bool OK;

	switch (modeGereParam)
	{
	case(ModeGereParametres::Charge): OK = G.Charge(); break;
	case(ModeGereParametres::Gere): OK = G.Gere(); break;
	case(ModeGereParametres::Sauve): OK = G.Sauve(); break;
	}

	return OK;
}


struct PositionCibles
{
	#define NBCIBLES 9

	POINTFLT Tab[NBCIBLES];
	int NbPoints;

	PositionCibles() //positions rep�r�es sur la plaque mire (avril 2019)
	{
		NbPoints = NBCIBLES;
		//Tab[0] = POINTFLT(28.7, 69.1);
		//Tab[1] = POINTFLT(48.5, 69);
		//Tab[0] = POINTFLT(68.3, 69.1);

		//Tab[3] = POINTFLT(30, 49);
		//Tab[4] = POINTFLT(50, 49.1);
		//Tab[5] = POINTFLT(70.1, 49.5);

		//Tab[6] = POINTFLT(30, 31.5);
		//Tab[7] = POINTFLT(49.8, 31.5);
		//Tab[8] = POINTFLT(69.8, 32.1);

		Tab[0] = POINTFLT(30, 31.5); 
		Tab[1] = POINTFLT(49.8, 31.5);
		Tab[2] = POINTFLT(69.8, 31.5);
		Tab[3] = POINTFLT(30, 48.8);
		Tab[4] = POINTFLT(50, 48.8);
		Tab[5] = POINTFLT(70.1, 48.8);
		Tab[6] = POINTFLT(28.7, 68.6);
		Tab[7] = POINTFLT(48.5, 68.6);
		Tab[8] = POINTFLT(68.3, 68.6);
		

		for (int i= 0; i < NbPoints; i++) Tab[i] /= 100; //passage en m�tres
	} 


} 		TabCiblesG;



POINTFLT * TabPositionsMire() { return TabCiblesG.Tab; }
int NbPositionsMire() { return TabCiblesG.NbPoints; }


RRECT FenetreAnalyseLocale(int xc, int yc, RRECT &Fsource, TParamCalage &param)
/****************************************************************************/
{
	RRECT F;
	int marge = sqrt(param.SurfaceMax);	//soit une marge de largeur �gale � l'objet carr� max

	F.left = max(Fsource.left, xc - marge);
	F.right = min(Fsource.right, xc + marge);
	F.top = max(Fsource.top, yc - marge);
	F.bottom = min(Fsource.bottom, yc + marge);
	return F;
}


void SetImageLocale(IMGRIS *src, RRECT FLocale, Region &R, IMGRIS *imLocale)
{
	//recopie de l'objet dans image seuill�e locale

	imLocale->Efface(0);

	for (R.Segments.begin(); !R.Segments.end(); R.Segments++)
	{
		SEGMENT *S = R.Segments.current();
		int x1 = S->xg - FLocale.left;
		int x2 = S->xd - FLocale.left;
		int y = S->y - FLocale.top;
		for (int x = x1; x <= x2; x++) if (OKxy(imLocale, x, y)) imLocale->EcritPixel(x, y, 255);
	}
}


static IMGRIS* pImG; //pour transmission  � EffaceSegment

static int EffaceSegment(int x1, int x2, int y)	//pour suivi des contours --> effacement des objets au fur et � mesure
/*********************************************/
{
	x2 = min(x2 + 1, pImG->F.right);             //dernier compris!!
	BYTE *buf = new BYTE[x2 - x1];
	memset(buf, 0, x2 - x1);
	pImG->EcritSegment(x1, x2, y, buf);
	delete[] buf;
	return 1;
}

int ContoursExternes(IMGRIS *B, _list<LIGNEFLOAT> &ListeContours, TParamCalage &param)
{
	pImG = B;

	//recherche des objets
	B->Home();
	while (B->EntreeObjet())    /** Tant qu'un d�but de contour est d�tect� **/
	{
		OBJETBIN F;
		PTCONTOUR Entree;

		Entree.x = B->X;   /* coordonn�es du pixel d'entr�e */
		Entree.y = B->Y;

		if (SuiviContour(B, &F))
			if (OrdonneContour(F, 1, 1, EffaceSegment))
				if (F.surface >= param.SurfaceMin && F.surface <= param.SurfaceMax)
				{
					//conversion du contour en LIGNEFLOAT
					LIGNEFLOAT Lf(F.NbPtsContour);
					for (int i = 0; i < F.NbPtsContour; i++) { Lf[i].x = F.pnt[i].x; Lf[i].y = F.pnt[i].y; }
					ListeContours.push_back(Lf);
				}

		if (!B->Suivant()) break;  //pour ne pas boucler sur le premier

	}    /* fin du while */

	return ListeContours.nbElem;
}


bool MatchEllipses(_list<LIGNEFLOAT> &ListeContours, TEllipseFitting &Ellipse, double &Residu, TParamCalage &param)

{
	int NbEllipses = 0;

	BalayeListe(ListeContours)
	{
		//identification
		if (Ellipse.IdentificationDirecte(*ListeContours.current(), Residu))	NbEllipses++;
	}

	return (NbEllipses == 1);
}


LIGNE LigneEllipse(TEllipseFitting &Ellipse, int NbPoints = 500)
{
	LIGNE L(NbPoints);
	double sintheta = sin(Ellipse.ThetaRad);
	double costheta = cos(Ellipse.ThetaRad);

	for (int i = 0; i < NbPoints; i++)
	{
		double phirad = i * (2 * M_PI / NbPoints);
		double x = Ellipse.Ru*cos(phirad);
		double y = Ellipse.Rv*sin(phirad);

		double nx = Ellipse.Centre.x + x * costheta - y * sintheta;
		double ny = Ellipse.Centre.y + x * sintheta + y * costheta;

		L[i].x = nx; L[i].y = ny;
	}
	return L;
}



bool AnalyseLocale(IMGRIS *im, Region &R, TParamCalage &param, double &xc, double &yc, LIGNE &LEllipse,
	int NbPointsLigneEllipse, char *info=NULL)
	/************************************************************************************************************/
{
	int xcR = (R.xMax + R.xMin) / 2;		int ycR = (R.yMax + R.yMin) / 2;	//centre approximatif de la r�gion

	//fen�tre d'analyse dans l'image d'origine
	RRECT FLocale = FenetreAnalyseLocale(xcR, ycR, im->F, param);
	if (R.NbPixels<param.SurfaceMin || R.NbPixels>param.SurfaceMax)
	{
		if (info) sprintf(info, " Region (%d, %d): Rejet surface: %d pixels", xcR, ycR, R.NbPixels);
		return false;
	}

	//cr�ation image r�duite � la fen�tre locale
	IMGRISMO imLocale(FLocale.right - FLocale.left, FLocale.bottom - FLocale.top);
	SetImageLocale(im, FLocale, R, &imLocale);

	//r�cup�ration des contours externes d'objets
	_list<LIGNEFLOAT> ListeContours;
	if (!ContoursExternes(&imLocale, ListeContours, param))
	{
		if (info) sprintf(info, " Region (%d, %d): �chec relev� des contours", xcR, ycR);
		return false;
	}		
	

	//matching ellipse
	TEllipseFitting Ellipse;
	double Residu;
	if (!MatchEllipses(ListeContours, Ellipse, Residu, param))
	{
		if (info) sprintf(info, " Region (%d, %d): �chec identification ellipse", xcR, ycR);
		return false;
	}

	if (Residu > param.ResiduRelatifParPoint)
	{
		if (info) sprintf(info, " Region (%d, %d): r�sidu trop �lev� (%f)", xcR, ycR, Residu);
		return false;
	}

	//repositionnement en absolu
	Ellipse.Centre.x += FLocale.left;
	Ellipse.Centre.y += FLocale.top;

	//s�lection de l'ellipse
	double re = Ellipse.Ru / Ellipse.Rv;
	if ((re > param.RatioAxesMax) || (1 / re > param.RatioAxesMax))
	{
		if (info) sprintf(info, " Region (%d, %d): ratio axes trop �lev� (%f)", xcR, ycR, max(re, 1/re));
		return false;
	}

	double se = M_PI * Ellipse.Ru * Ellipse.Rv;
	if (se > param.SurfaceMax || se < param.SurfaceMin)
	{
		if (info) sprintf(info, " Region (%d, %d): Rejet surface ellipse: %f pixels", xcR, ycR, se);
		return false;
	}

	xc = Ellipse.Centre.x; yc = Ellipse.Centre.y;

	//ligne pour affichage ellipse
	LEllipse = LigneEllipse(Ellipse, NbPointsLigneEllipse);	//ellipse d�j� repositionn�e en absolu par son centre

	return true;
}


void RechercheCibles(IMGRIS *imBinaire, TParamCalage &param, _list<POINTFLT> &ListeCentresCibles,
	_list<LIGNE> &ListeTraceEllipses, int NbPointsLigneEllipse, _list<string> &listeErreurs)
	/******************************************************************************************/
{
	//recherche des objets dans l'image binaire

	TEnsembleRegions TER;
	
	int nb = ReleveObjetsBinaires(imBinaire, &TER, true, true, false);
	
		
	BalayeListe(TER.ListeRegions)
	{
		double xc, yc;
		Region R = *TER.ListeRegions.current();
		LIGNE L;
		char st100[100];
		if (AnalyseLocale(imBinaire, R, param, xc, yc, L, NbPointsLigneEllipse, st100))
		{
			ListeCentresCibles.push_back(POINTFLT(xc, yc));
			ListeTraceEllipses.push_back(L);
		}
		else listeErreurs.push_back(st100);
	}

}






//THIN PLATE SPLINE (r�sultat d�cevant: r�sidu m�tres 0.8 sur ensemble complet, 27 sur ensemble test !!!)

class TPS : public TCalage2D
{
public:
	bool Init(POINTFLT *Pixels, POINTFLT *Metres, int NbPoints);

	bool Sauve(char *Nf);
	bool Charge(char *Nf);

	POINTFLT Pixels2Metres(POINTFLT Pixels);

private:

	CThinPlateSpline CTPS;

	//conserv�s pour la sauvegarde de CTPS
	std::vector<Point2f> VecteurPixels; 
	std::vector<Point2f> VecteurMetres;

};


bool TPS::Init(POINTFLT *TabPixels, POINTFLT *TabMetres, int NbPoints)
{
	
	for (int i = 0; i < NbPoints; i++)
	{
		Point2f pixel(TabPixels[i].x, TabPixels[i].y);
		VecteurPixels.push_back(pixel);
		Point2f metres(TabMetres[i].x, TabMetres[i].y);
		VecteurMetres.push_back(metres);
	}
	
	//CTPS.setCorrespondences(VecteurPixels, VecteurMetres);
	CTPS.computeSplineCoeffs(VecteurMetres, VecteurPixels);

	return true;

}

POINTFLT TPS::Pixels2Metres(POINTFLT Pixels)
{
	Point2f pixel(Pixels.x, Pixels.y);
	Point2f metre = CTPS.interpolate(pixel);
	return POINTFLT(metre.x, metre.y);
}

bool TPS::Sauve(char *Nf)
{
	//on sauve les vecteurs de correspondance

	MATRICE M(VecteurPixels.size(), 4);
	for (int i = 0; i < M.dimL(); i++)
	{
		M[i][0] = VecteurPixels.at(i).x;
		M[i][1] = VecteurPixels.at(i).y;
		M[i][2] = VecteurMetres.at(i).x;
		M[i][3] = VecteurMetres.at(i).y;
	}
		
	return M.Sauve(Nf);


	/* gard� pour m�moire
	Mat mapx, mapy;
	CTPS.getMaps(mapx, mapy);

	MATRICE MX(mapx.row, mapx.col);
	for (int i = 0; i < MX.dimL(); i++)
		for (int j = 0; j < MX.dimC(); j++)
			MX[i][j] = mapx.at<float>(i, j);

	MATRICE MY(mapy.row, mapy.col);
	for (int i = 0; i < MY.dimL(); i++)
		for (int j = 0; j < MY.dimC(); j++)
			MY[i][j] = mapy.at<float>(i, j);

	TNomFic NfGen;
	strcpy(NfGen, Nf); NomSansExt(NfGen);

	TNomFic NfX; sprintf(NfX, "%s_X.%s", NfGen, ChaineExt(Nf));
	if (!MX.Sauve(NfX))	return false;
	TNomFic NfY; sprintf(NfY, "%s_Y.%s", NfGen, ChaineExt(Nf));
	if (!MY.Sauve(NfY))	return false;*/


}

bool TPS::Charge(char *Nf)
{
	MATRICE M;
	if (!M.Charge(Nf))	return false;

	for (int i = 0; i < M.dimL(); i++)
	{
		VecteurPixels.at(i).x = M[i][0];
		VecteurPixels.at(i).y = M[i][1];
		VecteurMetres.at(i).x = M[i][2];
		VecteurMetres.at(i).y = M[i][3];
	}

	CTPS.setCorrespondences(VecteurPixels, VecteurMetres);

}



//CALAGE TRAPEZE



struct RRECTF
{
	POINTFLT HautDroit;
	POINTFLT BasGauche;
};

//retourne le rectangle d�fini par les cibles extr�mes haut-gauche et bas-droit
RRECTF CadreCibles(POINTFLT * TabIn, int nbPoints, bool yinverse)
{
	int indexHautDroit = 0;
	int indexBasGauche = 0;
	double minsomme = yinverse ? TabIn[0].x + TabIn[0].y : TabIn[0].x - TabIn[0].y;
	double maxsomme = minsomme;

	for (int i = 1; i < nbPoints; i++)
	{
		double somme = yinverse ? TabIn[i].x + TabIn[i].y : TabIn[i].x - TabIn[i].y;

		if (somme < minsomme)
		{
			minsomme = somme;
			indexHautDroit = i;
		}

		if (somme > maxsomme)
		{
			maxsomme = somme;
			indexBasGauche = i;
		}
	}

	RRECTF R;
	R.HautDroit = TabIn[indexHautDroit];
	R.BasGauche = TabIn[indexBasGauche];
	return R;
}

struct CalageLin
{
	double k;
	double cosa, sina;
	POINTFLT T;

	bool InversionY;

	POINTFLT Direct(POINTFLT src)
	{
		if (InversionY) src.y *= -1;

		//prise en compte �chelle et rotation
		double X = (cosa * src.x - sina * src.y)*k;
		double Y = (sina * src.x + cosa * src.y)*k;

		//translation
		return POINTFLT(X + T.x, Y + T.y);
	}

	CalageLin(RRECTF &R1, RRECTF &R2, bool yinverse) : InversionY(yinverse)
	{
		POINTFLT P1 = R1.BasGauche - R1.HautDroit;
		POINTFLT P2 = R2.BasGauche - R2.HautDroit;
		double n1 = P1.Norme();
		double n2 = P2.Norme();
		k = n2 / n1;
		P1 /= n1;
		if (InversionY) P1.y *= -1;
		P2 /= n2;

		cosa = P1 | P2;
		sina = P1 ^ P2;

		T = POINTFLT(0, 0);
		POINTFLT HautDroit = Direct(R1.HautDroit);
		T = R2.HautDroit - HautDroit;

	}

	

};


int IndexCiblePlusProche(POINTFLT P)
//retour avec -1 si ambiguit�
{
	int indexmin=0;
	double d2min = (P - TabCiblesG.Tab[0]).Norme2();

	for (int i = 1; i < TabCiblesG.NbPoints; i++)
	{
		double d2 = (P - TabCiblesG.Tab[i]).Norme2();

		if (d2 < d2min)
		{
			indexmin = i;
			d2min = d2;
		}
	}

	int indexPlusProche = indexmin;
	double d2PlusProche = d2min;

	//recherche du deuxi�me plus proche pour contr�le
	d2min *= 2;

	for (int i = 0; i < TabCiblesG.NbPoints; i++)
	{
		if (i == indexPlusProche)	continue;
		double d2 = (P - TabCiblesG.Tab[i]).Norme2();
		d2min = min(d2min, d2);
	}

	if (d2min < d2PlusProche * 2)	return -1;
	//le deuxi�me plus proche est trop proche !


	return indexPlusProche;

}

bool DoOrdonneCibles(POINTFLT * TabIn, POINTFLT* TabOut, int nbPoints, char *msgerr)
{
	if (nbPoints != TabCiblesG.NbPoints)
	{
		sprintf(msgerr, "Le nombre de points n'est pas celui attendu (%d au lieu de %d)", nbPoints, TabCiblesG.NbPoints);
		return false;
	}


	RRECTF RPixels = CadreCibles(TabIn, nbPoints, false);
	RRECTF RMetres = CadreCibles(TabCiblesG.Tab, TabCiblesG.NbPoints, true);

	//calage sommaire

	CalageLin C(RPixels, RMetres, true);


	//r�ordonnancement des cibles d�tect�es

	for (int i = 0; i < nbPoints; i++)
	{
		POINTFLT Pmetres = C.Direct(TabIn[i]);
		int index = IndexCiblePlusProche(Pmetres);
		if (index < 0)
		{
			sprintf(msgerr, "Ambiguit� sur une des cibles");
			return false;
		}


		TabOut[index] = TabIn[i];
	}

	return true;

}


int IndexCiblePlusProche(POINTFLT P, POINTFLT *PFichier, int NbCibles)
//retour avec -1 si ambiguit�
{
	int indexmin = 0;
	double d2min = (P - PFichier[0]).Norme2();
	for (int i = 1; i < NbCibles; i++) {
		double d2 = (P - PFichier[i]).Norme2();
		if (d2 < d2min) {
			indexmin = i;
			d2min = d2;
		}
	}
	int indexPlusProche = indexmin;
	double d2PlusProche = d2min;
	//recherche du deuxi�me plus proche pour contr�le
	d2min *= 2;

	for (int i = 0; i < NbCibles; i++)
	{
		if (i == indexPlusProche)	continue;
		double d2 = (P - PFichier[i]).Norme2();
		d2min = min(d2min, d2);
	}
	if (d2min < d2PlusProche * 2)	return -1;
	//le deuxi�me plus proche est trop proche !
	return indexPlusProche;
}


bool OrdonneCibles(POINTFLT * TabCiblesFichier, POINTFLT * TabCiblesImage, POINTFLT * TabCiblesImageOrdre, int NbCibles, char *msgerr)
{
	DBOUT("Entr�e OrdonneCiblesRaspberry " << NbCibles);
	RRECTF RPixels = CadreCibles(TabCiblesImage, NbCibles, false);
	RRECTF RMetres = CadreCibles(TabCiblesFichier, NbCibles, true);
	//calage sommaire
	CalageLin C(RPixels, RMetres, true);
	//r�ordonnancement des cibles d�tect�es
	DBOUT("Entr�e boucle ordre \n");
	for (int i = 0; i < NbCibles; i++)
	{
		POINTFLT Pmetres = C.Direct(TabCiblesImage[i]);
		int index = IndexCiblePlusProche(Pmetres, TabCiblesFichier, NbCibles);
		DBOUT("index: " << index << "\n")
			if (index < 0)
			{
				sprintf(msgerr, "Ambiguit� sur une des cibles");
				return false;
			}
		TabCiblesImageOrdre[index] = TabCiblesImage[i];
		DBOUT(TabCiblesImage[i].x << " - " << TabCiblesImage[i].y << "\n")
			DBOUT(TabCiblesImageOrdre[index].x << " - " << TabCiblesImageOrdre[index].y << "\n")
	}
	return true;
}




//Fonction de lecture d'un fichier texte contenant les centres des cibles, remplit une liste vide avec les points, renvoie false si la lecture s'est mal pass�e
bool lectureCiblesFichier(TParamCalage &paramCalage, _list<POINTFLT> &listeCibles) {
	string nomFichier = paramCalage.FichierCalageCibles; //conversion implicite vers string
	ifstream fin(nomFichier);
	if (!fin)
	{
		return false;
	}
	//on peut alors parser le fichier et cr�er la liste de points
	string line;
	while (getline(fin, line))
	{
		size_t idx(line.find(","));
		string xStr(line.substr(0, idx));
		string yStr(line.substr(idx + 1, line.size() - idx));
		float xFlt = stof(xStr) / 100;
		float yFlt = stof(yStr) / 100;
		listeCibles.push_back(POINTFLT(xFlt, yFlt));
		DBOUT("cible lue" << xFlt << " - " << yFlt << "\n");
	}
	return true;
}

IMTCOULEUR * DoRechercheCibles(_list<POINTFLT> &ListeCentresCibles, _list<LIGNE> &ListeTraceEllipses, int NbPointsLigneEllipse, TParamCalage &paramCalage)

{
	if (!pDriverG->DesigneImage("D�signer l'image de la plaque mire (version couleur)"))	return false;
	IMTCOULEUR *ImC = pDriverG->ImageActiveCouleur();
	if (!ImC)
	{
		pDriverG->Message("L'image active n'est pas une image couleur");
		return NULL;
	}
	pDriverG->PrintTexte("Seuillage binaire\n");
	//seuillage de l'image
	char msg[1000];
	IMGRISMO ImSeuillee(ImC->dimX, ImC->dimY);
	string nomFichierLUT = paramCalage.FichierLUT;
	char *fichierLUT = &nomFichierLUT[0];
	if (!SeuilleSelonLut(ImC, &ImSeuillee, fichierLUT, msg))
	{
		pDriverG->Message(msg);
		return NULL;
	}
	pDriverG->PrintTexte("Recherche des cibles");
	_list<std::string> listErreur;
	DBOUT("SMAX" << paramCalage.SurfaceMax << "\n");
	DBOUT("SMin" << paramCalage.SurfaceMin << "\n");
	RechercheCibles(&ImSeuillee, paramCalage, ListeCentresCibles, ListeTraceEllipses, 500, listErreur);
	pDriverG->PrintTexte(" --> %d cibles trouv�es\n", ListeCentresCibles.nbElem);
	return ImC;
}



string  CalageGenerique(TParamCalage paramCalage) {
	string err;
	_list<POINTFLT> listeCiblesFichier;
	_list<POINTFLT> listeCiblesImage;
	_list<LIGNE> ListeTraceEllipses;
	//Ouverture du fichier contenant les positions des cibles
	if (!lectureCiblesFichier(paramCalage, listeCiblesFichier)) {
		err = "Erreur lors de l'ouverture du fichier des cibles";
		return err;
	}
	int NbCibles = listeCiblesFichier.nbElem;
	//Initialisation des deux tableaux avec le nombre de cibles attendues
	POINTFLT * TabCiblesImage = new POINTFLT[NbCibles];
	POINTFLT * TabCiblesFichier = new POINTFLT[NbCibles];
	//D�tection des cibles sur l'image
	IMTCOULEUR *ImC = DoRechercheCibles(listeCiblesImage, ListeTraceEllipses, 500, paramCalage);
	if (!ImC) {
		err = "Erreur recherche cibles";
		return err;
	}

	BalayeListe(ListeTraceEllipses)
	{
		ImC->TraceLigne(*ListeTraceEllipses.current());
	}
	pDriverG->PrintTexte("Calage: Nombre de cibles d�tect�es dans le fichier: %d \n", listeCiblesFichier.nbElem);
	pDriverG->PrintTexte("Calage: Nombre de cibles d�tect�es dans l'image: %d \n", listeCiblesImage.nbElem);
	//V�rification: le nombre de cibles d�tect� doit �tre �gal � celui annonc� (fait avant le remplissage des tableaux de taille fixe pour �viter une erreur)
	if (listeCiblesImage.nbElem != NbCibles) {
		err = "Pas de correspondance entre le nombre de cibles anonc� et le nombre d�tect�";
		return err;
	}
	int z;
	//Remplissage des tableaux depuis les _list
	for (listeCiblesFichier.begin(), listeCiblesImage.begin(), z = 0; !listeCiblesFichier.end(); listeCiblesFichier++, listeCiblesImage++, z++) {
		TabCiblesFichier[z] = *listeCiblesFichier.current();
		TabCiblesImage[z] = *listeCiblesImage.current();
	}
	//Ordonnancement des deux tableaux pour correspondance
	char stOrd[2000];
	POINTFLT * TabCiblesImageOrdre = new POINTFLT[NbCibles];
	if (!OrdonneCibles(TabCiblesFichier, TabCiblesImage, TabCiblesImageOrdre, NbCibles, stOrd)) {
		pDriverG->Message("Probl�me lors de l'ordonnacement des cibles");
	}
	for (z = 0; z < NbCibles; z++) {
		DBOUT("TabCiblesFichier_ORD:" << "z=" << z << "  " << TabCiblesFichier[z].x << " - " << TabCiblesFichier[z].y << "\n");
		DBOUT("TabCiblesImage_ORD:" << "z=" << z << "  " << TabCiblesImageOrdre[z].x << " - " << TabCiblesImageOrdre[z].y << "\n");
		ImC->Printf(TabCiblesImageOrdre[z].y, TabCiblesImageOrdre[z].x, "%d : %d / %d", z, int(TabCiblesFichier[z].x * 100), int(TabCiblesFichier[z].y * 100));
	}


	//R�alisation du calage
	TCalage2D *Calage = new Trapeze; //penser � nettoyer apr�s l'allocation
	Calage->Init(TabCiblesImageOrdre, TabCiblesFichier, NbCibles);
	char st[2000];
	sprintf(st, "Calage complet (%d points). R�sidu: %f", NbCibles, Calage->ResiduMetres(TabCiblesImageOrdre, TabCiblesFichier, NbCibles));
	if (pDriverG->MessageOuiNon("%s\nConserver ce calage ?", st))
	{

		if (!Calage->Sauve(paramCalage.FichierCalage)) {
			err = "Echec sauvegarde calage";
			return err;
		}
	}
	delete Calage;
	return err;
}
