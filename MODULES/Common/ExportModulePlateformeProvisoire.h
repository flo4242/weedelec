#pragma once

#include <string>
#include "commtype.h"
#include "WeedelecCommon.h"
#include "list_tpl.h"
#include "ligne.h" //POINTFLT

struct TRetourPlateforme
{
	TRetourTask State;
	std::string InfoRetour; 
	int DureeMs;
};

struct _EXPORT_ TModulePlateforme
{
	//fonctions export�es pour utilisation externe (autres modules)

	//static bool ChargeParametres(TParamSony &param);

	//avance la plateforme jusqu'� la position d'acquisition/d�sherbage suivante
	static void TModulePlateforme::GoToNextStep(TRetourPlateforme &ret);

	static void TModulePlateforme::Weeding(_list<POINTFLT> &weeds, TRetourPlateforme &ret);
	//les coordonn�es des mauvaises herbes sont fournies dans l'espace du robot au moment des prises d'image
	//pr�c�dentes (� charge de la plateforme d'ajuster en fonction du d�pacement effectif depuis)

	static void TModulePlateforme::Stop(); //demande d'arr�t utilisateur


};




