#pragma once
#include <time.h>
struct H33
{
	time_t t_avant;
	time_t t_arriere;
	double m11, m12, m13;
	double m21, m22, m23;
	double m31, m32, m33;

	H33 operator* (const H33 &H)
	{
		H33 ret;

		ret.m11 = m11 * H.m11 + m12 * H.m21 + m13 * H.m31;
		ret.m12 = m11 * H.m12 + m12 * H.m22 + m13 * H.m32;
		ret.m13 = m11 * H.m13 + m12 * H.m23 + m13 * H.m33;

		ret.m21 = m21 * H.m11 + m22 * H.m21 + m23 * H.m31;
		ret.m22 = m21 * H.m12 + m22 * H.m22 + m23 * H.m32;
		ret.m23 = m21 * H.m13 + m22 * H.m23 + m23 * H.m33;

		ret.m31 = m31 * H.m11 + m32 * H.m21 + m33 * H.m31;
		ret.m32 = m31 * H.m12 + m32 * H.m22 + m33 * H.m32;
		ret.m33 = m31 * H.m13 + m32 * H.m23 + m33 * H.m33;

		return ret;
	}

	void Applique(double Xin, double Yin, double &Xout, double &Yout)
	{
		double ux = m11 * Xin + m12 * Yin + m13;
		double uy = m21 * Xin + m22 * Yin + m23;
		double u = m31 * Xin + m32 * Yin + m33;
		Xout = ux / u; Yout = uy / u;
	}

};

H33 Inverse(H33 H); //H33.cpp