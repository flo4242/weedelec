﻿#ifndef __WAYPOINT__H
#define  __WAYPOINT__H

struct Waypoint
	{
	char Name[128];
	char Description[128];
	double LatDeg, LonDeg;
	double elevation;
	Waypoint() : LatDeg(0), LonDeg(0), elevation(0) { Name[0] = Description[0] = '\0';}
	};



#endif
