#pragma once

#include <ppltasks.h>

struct TSonyStop //structure de gestion des ordres externes TModuleSony::Stop()
{
	concurrency::cancellation_token_source *Cts;
	concurrency::task_completion_event<void> *Tce;

	TSonyStop() : Cts(NULL), Tce(NULL) { ; }

	void ReEnclenche()
	{
		if (Cts != NULL)	delete Cts;
		Cts = new concurrency::cancellation_token_source;
		if (Tce != NULL)	delete Tce;
		Tce = new concurrency::task_completion_event<void>;

	}


};

extern TSonyStop SonyStopG;

