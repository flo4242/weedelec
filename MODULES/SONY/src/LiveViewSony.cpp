#include <highgui.h>
#include "opencv2/core/types_c.h"
#include "GetDimExif.h"



bool GetFrameSize(unsigned char *JPEGdata2)
int	dummy;


if (JPEG)
{
	if (!NextWORD(dummy))	return false;
	if (dummy != 0xFFD8) 	return false;	 //ce n'est pas un jpeg

	if (!NextWORD(dummy))	return false;

	/////////////////////////////////////////////////////////////////////////////////////////////////
	//skip des segments JFIF �ventuels (voir doc exif.pdf)
	//cas rencontr� nov 2011, ajout� sur  les photos par le geotagging de locr XXXXXXXXX

	//avril 2019: ajout des cas DQT (FFDB), DHT (FFC4), SOF (FFC0), SOS (FFDA), rencontr�s dans packets liveview du Sony 6300

	//while (dummy == 0xFFE0)
	while (dummy == 0xFFE0 || dummy == 0xFFDB || dummy == 0xFFC4 || dummy == 0xFFC0 || dummy == 0xFFDA)
	{
		int TailleJfif;
		if (!NextWORD(TailleJfif))	return false;
		fseek(F, TailleJfif - sizeof(WORD), SEEK_CUR);	//on saute ce qui reste de Jfif
		if (!NextWORD(dummy))	return false;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////

	if (dummy != 0xFFE1) 	return false; //marquer APP1

	int TailleExif;		if (!NextWORD(TailleExif))	return false;

	//lecture chaine "Exif"
	char Exif[6];
	if (fread(Exif, sizeof(char), 6, F) != 6) return false;
	if (strcmp(Exif, "Exif") != 0) 	return false;
	//pas de donn�es Exif
}


int ReadLiveStreamPacket(streams::istream &stream, unsigned char *JPEGData, int AvailableSize)
//retour avec la taille de JPEGData
//retour avec -1 si erreur de lecture
//retour avec -2 AvailableSize est insuffisant
//voir reference api sony page 270
{
	long dummy;

	//lecture Common Header
	dummy = stream.read().get();
	if (dummy != 0xFF) return -1;

	dummy = stream.read().get();
	if (dummy != 0x01) return -1; //liveview

	for (int i = 0; i < 6; i++) dummy = stream.read().get();

	//lecture Payload Header (128 octets)
	dummy = stream.read().get();
	if (dummy != 0x24) return -1;
	dummy = stream.read().get();
	if (dummy != 0x35) return -1;
	dummy = stream.read().get();
	if (dummy != 0x68) return -1;
	dummy = stream.read().get();
	if (dummy != 0x79) return -1;


	//lecture payload --> network byte order --> big endian

	int payloadjpeg = dummy = stream.read().get() << 16;
	payloadjpeg += stream.read().get() << 8;
	payloadjpeg += stream.read().get();

	if (payloadjpeg >= AvailableSize)	return -2; //inutile d'aller plus loin

	long paddingsize = stream.read().get();

	for (int i = 0; i < 120; i++)	stream.read().get(); //skip du reste du header



	//lecture data

	for (int i = 0; i < payloadjpeg; i++)	JPEGData[i] = stream.read().get();

	for (int i = 0; i < paddingsize; i++) dummy = stream.read().get(); //skip fin des data

	return payloadjpeg;
}



FILE *F = fopen("D:\\bof.jpg", "wb");
for (int i = 0; i < payloadjpeg; i++) fwrite(&JPEGdata[i], 1, 1, F);

fclose(F);

TDataDimExif TD;

bool ok = DoGetDimExif("D:\\bof.jpg", TD, true);

//IplImage *Imcv = cvLoadImage("D:\\bof.jpg", CV_LOAD_IMAGE_UNCHANGED); //OK

CvMat mat;
cvInitMatHeader(&mat, 640, 424, CV_8UC3, JPEGdata2, 0); //il faut connaitre la dimension !!!


IplImage *Imcv2 = cvDecodeImage(&mat);

im = cv::Mat(Imcv2);

cv::imshow("A", im);

//im = cv::imdecode(JPEGdata, CV_LOAD_IMAGE_COLOR); //non

 //im = cv::imread("D:\\bof.jpg"); //non

return true;
}




bool LiveView(string &MsgErr)
//////////////////////////////////////////
{

	SonyStopG.ReEnclenche();

	TParamSony Param;
	TModuleSony::ChargeParametres(Param);

	string stcamera = "http://" + string(Param.AdresseIP) + ":" + string(Param.PortIP) + "/sony/camera";
	utility::string_t adrcamera = conversions::to_string_t(stcamera);

	http_client ClientCamera(adrcamera);

	web::json::value result;
	string msg;


	//passage en RecMode (indispensable pour atteindre l'interface compatible avec la doc SDK)
	POSTSony post("startRecMode", "", "1.0");
	TRetourTask ret = SendRequestPOST(ClientCamera, post, result, msg, Param.TimeOutIP, SonyStopG);

	if (ret != TRetourTask::Completed)
	{
		MsgErr = "Echec passage en RecMode : " + msg;
		return false;
	}

	post = POSTSony("startLiveview", "", "1.0");
	ret = SendRequestPOST(ClientCamera, post, result, msg, Param.TimeOutIP, SonyStopG);

	if (ret != TRetourTask::Completed)
	{
		MsgErr = "Echec startLiveview : " + msg;
		return false;
	}


	_list<json::value> listeout;
	GetFieldValue(result, "result", listeout);

	if (listeout.nbElem == 0)
	{
		msg = "Pas de cl� \"result\" dans cet objet";
		return false;
	}

	//format: ["... url..."];
	json::array res = listeout.head->obj.as_array();
	string_t url_st = res.at(0).as_string();


	//string url = utility::conversions::to_utf8string(url_st);
	//::MessageBox(NULL, url.data(), "", MB_OK);


	//lance la requ�te de t�l�chargement
	web::http::client::http_client clientimg(url_st);
	http_request request(web::http::methods::GET);
	http_response response;

	pplx::task<http_response> requestTask = clientimg.request(request, SonyStopG.Cts->get_token());

	try
	{
		response = requestTask.get();
	}

	catch (exception)
	{
		msg = "Echec lancement requ�te de t�l�chargement";
		return false;
	}

	cv::Mat im;
	if (!ReadLiveStreamPacket(response.body(), im))
		::MessageBox(NULL, "Echec lecture packet", "", MB_OK);

	cv::imshow("live", im); //plante

	return true;
}

