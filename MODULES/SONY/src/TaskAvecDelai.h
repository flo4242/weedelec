#ifndef __TASK_AVEC_DELAI__H
#define __TASK_AVEC_DELAI__H

//d'apr�s https://docs.microsoft.com/en-us/cpp/parallel/concrt/how-to-create-a-task-that-completes-after-a-delay?view=vs-2017

#include "pplx\pplxtasks.h"

using namespace pplx;
using namespace std;

// Cancels the provided task after the specifed delay, if the task
// did not complete.
template<typename T>  task<T> cancel_after_timeout(task<T> t, cancellation_token_source cts, unsigned int timeout);

#include "TaskAvecDelai.hpp"

#endif