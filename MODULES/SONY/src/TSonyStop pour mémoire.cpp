struct TSonyStop //structure de gestion des ordres externes TModuleSony::Stop()
 {
	 struct ctcallback
	 {
		 task_completion_event<void> tce;

		 ctcallback(task_completion_event<void> ttce)
		 {
			 tce = ttce;
		 }

		 void operator()()
		 {
			 tce.set(); //provoque la fin de la tâche
		 }
	 };

	 cancellation_token_source *Cts;
	 task_completion_event<void> *Tce;
	 ctcallback *ctCallback;
	 cancellation_token_registration ctCallbackRegistration;

	 TSonyStop() : Cts(NULL), Tce(NULL), ctCallback(NULL) { ; }

	 //création d'un callback sur la cancellation du Cts permettant de valider un task_completion_event

	 

	 ctcallback *ctCallback = new ctcallback(*Tce);


	 


	 void ReEnclenche()
	 {
		 if (ctCallback != NULL)
		 {
			 Cts->get_token().deregister_callback(ctCallbackRegistration);
			 delete ctCallback;
		 }

		 if (Cts != NULL)	delete Cts;
		 Cts = new cancellation_token_source;
		 if (Tce != NULL)	delete Tce;
		 Tce = new task_completion_event<void>;
		 ctCallback = new ctcallback(*Tce);
		 ctCallbackRegistration = Cts->get_token().register_callback(*ctCallback);

	 }
	
	 void WaitStop()
	 {
		 task<void> TStop(*Tce);
		 TStop.wait(); //fin déclenchée par un Tce->set() de la tâche en cours
	 }


 } SonyStopG;

