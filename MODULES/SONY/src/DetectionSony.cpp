#include "TaskUtil.h" 
#include "CommonSony.h" 
#include "ExportModuleSony.h" //fonctions export�es pour utilisation externe (autres modules)
#include "Calage.h"
#include "PasserelleTraitimOpenCV.h"
#include "drvtraiti.h" 
#include "binaire.h"
#include "EnsembleRegions.h"
#include "EllipseFitting.h"
#include <vector>
#include <list>

#define DBOUT( s )            \
{                             \
   std::ostringstream os_;    \
   os_ << s;                   \
   OutputDebugString( os_.str().c_str() );  \
}

extern IMTCOULEUR * DoVisualiseImage(DRVTRAITIM *pD, char * Nf, double &FocaleRelevee, char *msg2000);
//FSony.cpp; n�cessaire pour d�tection manuelle

extern string GetNomImage(string &url); //AcquisitionSony.cpp
extern bool DownloadUrl(string url, string NfOut, string &msg); //AcquisitionSony.cpp

void RecherchePastilles(IMGRIS *imBinaire, TParamDetectionSony &param, _list<POINTFLT> &ListeCentresCibles, _list<string> &listeErreurs);
bool CorrecPositionJaune(_list<POINTFLT> &ListePastilles, _list<POINTFLT> &ListePastillesBleues, const int seuil, int dimX, int dimY);
bool AgregationCentreRegions(_list<POINTFLT> &ListeEntree, _list<POINTFLT> &ListeSortie, const int seuil_agreg);


void SetRetourDetectionNotCompletedSony(TRetourDetectionSony &retsony, TRetourState retstate, string errormsg, int dureeMs)
{
	retsony.State = retstate;

	switch (retstate)
	{

	case TRetourState::Error:
		retsony.InfoRetour = errormsg;
		break;

	case TRetourState::TimeOut:
		retsony.InfoRetour = "Time out. V�rifier la connexion et/ou le param�tre de time out";
		break;

	case TRetourState::ExternalStop:
		retsony.InfoRetour = "Demande d'arr�t externe";
		break;
	}
	retsony.DureeMs = dureeMs;
}

void TModuleSony::DetectionPastillesSony(TRetourDetectionSony &ret, std::string UrlImageReduite, DRVTRAITIM *pD)
{
	SonyStopG.ReEnclenche();
	int TimeDebut = GetTickCount();

	TParamAcquisitionSony ParamA;
	TParamDetectionSony ParamD;
	TParamCalage ParamC;
	if (!ChargeParametres(ParamA, ParamD, ParamC))
	{
		SetRetourDetectionNotCompletedSony(ret, TRetourState::Error, "Echec chargement param�tres", GetTickCount() - TimeDebut);
		return;
	}


	
	//r�cup�ration de l'image au format r�duit
	TNomFic NomImExt; strcpy(NomImExt,  GetNomImage(UrlImageReduite).data());

	TNomFic NfIm;
	
	TNomFic NomIm; 	strcpy(NomIm, NomImExt); NomSansExt(NomIm);

	sprintf(NfIm, "%s\\%s_R.%s", ParamA.DossierImages, NomIm, ChaineExt(NomImExt));

	if (!DownloadUrl(UrlImageReduite, NfIm, ret.InfoRetour))
	{
		ret.State = TRetourState::Error;
		ret.DureeMs = GetTickCount() - TimeDebut;
		return;
	}

	char msgerr[1000];

	IMTRAITIM *ImT = DoChargeImageViaOpenCV(pD, NfIm, false, msgerr);

	if (ImT == NULL)
	{
		SetRetourDetectionNotCompletedSony(ret, TRetourState::Error, msgerr, GetTickCount() - TimeDebut);
	}

	IMTCOULEUR *ImC = dynamic_cast<IMTCOULEUR *>(ImT);
	if (ImC == NULL)
	{
		SetRetourDetectionNotCompletedSony(ret, TRetourState::Error, "Image charg�e non couleur!", GetTickCount() - TimeDebut);
		return;
	}

	
	if (!DoDetectionPastillesSony(ImC, ret.ListeWeedsImage, ParamD, msgerr))
	{
		SetRetourDetectionNotCompletedSony(ret, TRetourState::Error, msgerr, GetTickCount() - TimeDebut);
		return;
	}
	DBOUT("ROUTINE DETECTION, PASTILLES OBTENUES AVANT CHANGEMENT COOR \n");
	BalayeListe(ret.ListeWeedsImage) {
		POINTFLT *p = ret.ListeWeedsImage.current();
		DBOUT("x=" << p->x <<" y="<<p->y << "\n");
	}
	DBOUT("fichier calage: " << ParamC.FichierCalage <<"\n");

	//on a trouv� des pastilles --> coordonn�es dans l'espace du robot

	TCalage2D* Calage = new Trapeze;

	if (!Calage->Charge(ParamC.FichierCalage))
	{
		sprintf(msgerr, "Echec chargement fichier \n %s", ParamC.FichierCalage);
		ret.InfoRetour = msgerr;
		ret.State = TRetourState::Error;
		ret.DureeMs = GetTickCount() - TimeDebut;
		delete Calage;
		return;
	}
	DBOUT("calage charg�");

	//adaptation du calage � la taille d'image r�duite 
	POINTFLT CoeffCalage;
	DBOUT("DIM:" << ImC->dimX << "  -   " << ImC->dimY << "\n");
	CoeffCalage.x = 6000. / ImC->dimX;
	CoeffCalage.y = 4000. / ImC->dimY;
	DBOUT("COEFF CALAGE:" << CoeffCalage.x << "  -   " << CoeffCalage.y << "\n");

	BalayeListe(ret.ListeWeedsImage)
	{
		POINTFLT *P = ret.ListeWeedsImage.current();
		POINTFLT PHR(P->x * CoeffCalage.x, P->y * CoeffCalage.y);
		POINTFLT PM = Calage->Pixels2Metres(PHR);
		TWeedRobot WR(PM, -1, -1);

		ret.ListeWeeds.push_back(WR);
	}
	DBOUT("PASTILLES OBTENUES REPERE ROBOT \n");
	BalayeListe(ret.ListeWeeds) {
		TWeedRobot *p = ret.ListeWeeds.current();
		DBOUT("x=" << p->Position.x << " y=" << p->Position.y << "\n");
	}

	delete Calage;

	ret.InfoRetour = "";
	ret.State = TRetourState::Completed;
	ret.DureeMs = GetTickCount() - TimeDebut;

	return;
	
}



bool TModuleSony::DoDetectionPastillesSony(IMCOULEUR* ImC, _list<POINTFLT> &ListePastilles, TParamDetectionSony &ParamD, char *msgerr)
{
	//seuillage de l'image
	
	int dimX = ImC->F.right - ImC->F.left;
	int dimY = ImC->F.bottom - -ImC->F.top;


	//D�tection des pastilles jaunes, tout le temps r�alis�e
	IMGRISMO ImSeuillee(dimX, dimY); //image tampon qui servira pour une ou deux d�tections
	if (!SeuilleSelonLut(ImC, &ImSeuillee, ParamD.FichierLut_jaune, msgerr))	return false;
	_list<LIGNE> ListeTraceEllipses; //non utilis�
	_list<std::string> listErreur;

	//Recherche des pastilles
	RecherchePastilles(&ImSeuillee, ParamD, ListePastilles, listErreur);



	//D�tection des pastilles bleues et correction des positions jaunes si n�cessaire
	if (ParamD.correctionBleu && !ListePastilles.empty()) {
		_list<POINTFLT> ListePastillesBleues; //liste limit�e � la fonction contenant les positions des pastilles bleues
		IMGRISMO ImSeuillee_bleu(dimX, dimY);
		if (!SeuilleSelonLut(ImC, &ImSeuillee_bleu, ParamD.FichierLut_bleu, msgerr))	return false;
		_list<LIGNE> ListeTraceEllipses; //non utilis�
		RecherchePastilles(&ImSeuillee_bleu, ParamD, ListePastillesBleues, listErreur);
		//correction des positions des pastilles jaunes si trop proche des bleues
		if (!ListePastillesBleues.empty() && !ListePastilles.empty()) {
			if (!CorrecPositionJaune(ListePastilles, ListePastillesBleues, ParamD.distanceMaxBleuJaune,dimX,dimY)) {
				return false;
			}
		}
		else {
			return false;
		}

		//Pour visualisation seulement
		//BalayeListe(ListePastillesBleues)
		//{
		//	POINTFLT *P = ListePastillesBleues.current();
		//	ImC->CurseurInverse(P->x, P->y, 60);
		//}

	}

	BalayeListe(ListePastilles)
	{
		POINTFLT *P = ListePastilles.current();
		ImC->CurseurInverse(P->x, P->y, 60);
	}



	return true;
}



TCalage2D * TModuleSony::ChargeCalage(char * msgerr)
{

	TParamDetectionSony dummy2;
	TParamAcquisitionSony dummy;
	TParamCalage paramC;
	if (!TModuleSony::ChargeParametres(dummy, dummy2, paramC))
	{
		sprintf(msgerr, "Echec chargement param�tres Sony");
		return NULL;
	}

	TCalage2D * Calage = new Trapeze;

	if (!Calage->Charge(paramC.FichierCalage))
	{
		sprintf(msgerr, "Echec chargement calage Sony" );
		delete Calage;
		return NULL;
	}

	return Calage;
}


IMTCOULEUR * TModuleSony::VisualiseImage(DRVTRAITIM *pD, char * Nf, char * msg2000)
{
	double FocaleRelevee;
	return DoVisualiseImage(pD, Nf, FocaleRelevee, msg2000);
}



//Fonctions pour que la d�tection se fasse depuis DoDetectionPastillesSony


//Fonction prenant en entr�e la liste de pastilles bleues et jaunes et d�placant la position des jaunes si la distance est inf�rieure � un seuil
bool CorrecPositionJaune(_list<POINTFLT> &ListePastilles, _list<POINTFLT> &ListePastillesBleues, const int seuil, int dimX, int dimY) {
	//balayage d'abord sur les jaunes et ensuite sur les bleus
	DBOUT("entr�e dans la fonction de cor\n");
	BalayeListe(ListePastilles) {
		//on fait la liste des points trop proches et leur distance
		std::vector<POINTFLT> bleusProches;
		std::vector<int> bleusProchesDist;
		POINTFLT curJaune = *ListePastilles.current();
		//DBOUT("Entr�e dans une cible jaune\n");
		BalayeListe(ListePastillesBleues) {
			POINTFLT curBleu = *ListePastillesBleues.current();
			//On v�rifie si la distance est inf�rieure au seuil indiqu�
			float deuclid = std::sqrt(pow((curJaune.x - curBleu.x), 2) + pow((curJaune.y - curBleu.y), 2));
			//DBOUT("Distance calcul�e" << deuclid << "\n");
			if (deuclid < seuil) {
				bleusProches.push_back(curBleu);
				bleusProchesDist.push_back(deuclid);
			}
		}
		//On peut alors modifier la position de la pastille jaune en cons�quence si n�cessaire
		//Ici on prend seulement l'�l�ment le plus proche
		if (!bleusProches.empty()) {
			//DBOUT("Correction en cours\n");
			//DBOUT("Coord initiales jaune\n");
			//DBOUT("x=" << curJaune.x << " y= " << curJaune.y << "\n");
			POINTFLT newpoint;
			int minElementIndex = std::min_element(bleusProchesDist.begin(), bleusProchesDist.end()) - bleusProchesDist.begin();
			POINTFLT plusProcheBleu = bleusProches[minElementIndex];
			//DBOUT("Coord bleu\n");
			//DBOUT("x=" << plusProcheBleu.x << " y= " << plusProcheBleu.y << "\n");
			//calcul du nouveau point
			float ratio = float(seuil) / float(bleusProchesDist[minElementIndex]);
			//DBOUT("ratio=" << ratio << "\n");
			//DBOUT("distance=" << bleusProchesDist[minElementIndex] << "\n");
			newpoint.x = plusProcheBleu.x + (curJaune.x - plusProcheBleu.x)*ratio;
			newpoint.y = plusProcheBleu.y + (curJaune.y - plusProcheBleu.y)*ratio;


			//CONDITION BORDS DE L'IMAGE 
			if (newpoint.x < 0)   newpoint.x = 0;
			if (newpoint.y < 0)   newpoint.y = 0;
			if (newpoint.x > dimX)   newpoint.x = dimX;
			if (newpoint.y > dimX)   newpoint.y = dimY;
			//


			//actualiser la valeur dans ListePastilles
			//DBOUT("Nouvelles coords jaune\n");
			//DBOUT("x=" << newpoint.x << " y= " << newpoint.y << "\n");
			POINTFLT & s(*ListePastilles.current());
			s = newpoint;

		}
	}
	return true;
}




//Calcule les distances entre points pour �ventuellement les fusionner si la distance euclidienne est inf�rieure � un seuil
//Entr�es:
// - listPointEntree: liste contenant les points d'origine
// - listPointSortie: liste vide contenant les points en sortie
bool AgregationCentreRegions(_list<POINTFLT> &listPointEntree, _list<POINTFLT> &listPointSortie, const int seuil_agreg) {
	DBOUT("Entr�e dans la fonction d'agr�gation \n");

	//v�rifier que la liste en sortie ne soit pas vide
	if (!listPointSortie.empty()) {
		return false;
	}

	//conversion en liste classique, � �liminer si on peut faire la double boucle suivante sans les const_iterators
	list<POINTFLT> listPointEntreeSTD;
	BalayeListe(listPointEntree) {
		listPointEntreeSTD.push_back(*listPointEntree.current());
	}
	BalayeListe(listPointEntree) {
		POINTFLT cur = *listPointEntree.current();
		DBOUT("xp2 =" << cur.x << " yp2=" << cur.y << "\n")
	}


	//std::cout << "bibi" << std::endl;
		//On ne touche pas � la liste en entr�e sur laquelle on boucle => const_iterators
		//A chaque �l�ment, on trouve la liste des points proches (< seuil agreg)
		//On inscrit alors la moyenne dans une nouvelle liste
		//Lorsque les points sont fusionn�s, ils rentrent dans une blacklist et ne peuvent plus �tre utilis�s
	int index_liste1 = 0;
	std::vector<int> vectorElimineIntGlobal; // contient les indices �limin�s globalement


	for (list<POINTFLT>::const_iterator iter1 = listPointEntreeSTD.begin(); iter1 != listPointEntreeSTD.end(); iter1++) {
		//On v�rifie d'abord si l'�l�ment n'a pas d�j� �t� trait�
		//std::cout << index_liste1 << std::endl;
		if (std::find(vectorElimineIntGlobal.begin(), vectorElimineIntGlobal.end(), index_liste1) == vectorElimineIntGlobal.end()) { //si on ne trouve pas l'indice dans la blacklist
			vectorElimineIntGlobal.push_back(index_liste1); //il contient au moins l'index en cours
			std::vector<POINTFLT> vectorEliminePoint; // points �limin�s dans une boucle qui seront moyenn�s
			vectorEliminePoint.push_back(*iter1); //il contient au moins le point en cours


			int index_liste2 = 0;
			//on boucle sur les appariements possibles
			for (list<POINTFLT>::const_iterator iter2 = listPointEntreeSTD.begin(); iter2 != listPointEntreeSTD.end(); iter2++) {
				if (iter1 != iter2) {
					float deuclid = std::sqrt(pow(iter1->x - iter2->x, 2) + pow(iter1->y - iter2->y, 2));
					//std::cout << "distance entre" << index_liste1 << " et " << index_liste2 << "\n" << std::endl;
					//std::cout << deuclid << "\n" << std::endl;
					if (deuclid < seuil_agreg) { //si les points sont rapproch�s au sens de la distance euclidienne
						vectorEliminePoint.push_back(*iter2); // on passe le point pour calculer la moyenne
						vectorElimineIntGlobal.push_back(index_liste2); //on inscrit l'indice dans la blacklist
					}
				}
				index_liste2++; //on passe � l'index suivant
			}


			//On peut alors faire la moyenne des �l�ments s�lectionn�s
			float meanX = 0, meanY = 0;
			for (auto i : vectorEliminePoint) {
				meanX += i.x; meanY += i.y;
			}
			meanX /= vectorEliminePoint.size(); meanY /= vectorEliminePoint.size();
			//On rajoute enfin ce point dans la nouvelle liste
			listPointSortie.push_back(POINTFLT(meanX, meanY));
		}
		index_liste1++; //on passe � l'�l�ment suivant
	}
	return true;
}





//Fonction pour la recherche des cibles sur une image binaire, permet de remplir la liste de points correspondant aux cibles
void RecherchePastilles(IMGRIS *imBinaire, TParamDetectionSony &param, _list<POINTFLT> &ListeCentresCibles, _list<string> &listeErreurs)
	/*************************************************************************************************************/
{
	//DBOUT("min =" << param.SurfaceMin << " max =" << param.SurfaceMax << "\n");
	TEnsembleRegions TER;
	int nb = ReleveObjetsBinaires(imBinaire, &TER, true, true, false);

	//code d�tection avec ellipse
		/*BalayeListe(TER.ListeRegions) {
			double xc, yc;
			Region R = *TER.ListeRegions.current();
			LIGNE L;
			if (!param.AnalyseLocaleCentre) {
				if (AnalyseLocale(imBinaire, R, param, xc, yc, L, NbPointsLigneEllipse))
				{
					ListeCentresCibles.push_back(POINTFLT(xc, yc));
					ListeTraceEllipses.push_back(L);
				}
			}
		}*/



		//on choisit la m�thode simple avec les centres de masse des r�gions de taille suffisante
		_list<POINTFLT> ListeCentresCiblesPotentiel;
		BalayeListe(TER.ListeRegions) {
			double xc, yc;
			Region R = *TER.ListeRegions.current();
			float xcR = (R.xMax + R.xMin) / 2; float ycR = (R.yMax + R.yMin) / 2;	//centre approximatif de la r�gion
			if (R.NbPixels > param.SurfaceMin && R.NbPixels < param.SurfaceMax) {
				ListeCentresCiblesPotentiel.push_back(POINTFLT(xcR, ycR));
			}
		}
		//BalayeListe(ListeCentresCiblesPotentiel) {
		//	POINTFLT cur = *ListeCentresCiblesPotentiel.current();
		//	DBOUT("xp =" << cur.x << " yp=" << cur.y << "\n")
		//}
		//On fusionne les petits segments s'ils sont proches les uns des autres (param�tre d'agr�gation)
		if (!AgregationCentreRegions(ListeCentresCiblesPotentiel, ListeCentresCibles, param.seuil_agreg)) {
			listeErreurs.push_back("Erreur agreg centres");
		}
	//DBOUT("LISTE DES CIBLES TROUVEES \n");
	//BalayeListe(ListeCentresCibles) {
	//	POINTFLT cur = *ListeCentresCibles.current();
	//	DBOUT("x =" << cur.x << " y=" << cur.y << "\n")
	//}
}