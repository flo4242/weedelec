#ifndef _GROUPE_ERX_SENSOR__H
#define _GROUPE_ERX_SENSOR__H

#include "GroupeTPL.h"
#include "commtype.h"


struct TParamSensor
{
		int UpdatePeriodMs;
		bool FromGPXRecord;
		TNomFic GPXREcord;	//enregistrement pour simulation
	

	

	TParamSensor()
	{
		UpdatePeriodMs = 300;
		FromGPXRecord = true;;
		GPXREcord[0] = '\0';
	
	}

};


template < int T >  GROUPE_TPL<T> * GroupeSensor(void * hParent, TParamSensor &param)
	{
	return  new GROUPE_TPL<T>(hParent, "Configuration erx heading sensor", "D:\\ErxSensor.cfg",
		ENT("P�riode update (ms)", param.UpdatePeriodMs, 50, 2000),
		PBOOL("Simulation (sinon temps r�el depuis capteurs robot)", param.FromGPXRecord),
		FICH("Fichier GPX pour simulation", param.GPXREcord, "gpx"),
		
		NULL);
	

	
	}


#endif