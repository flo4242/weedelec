#include "SensorAPI.h"

/*bool sensor(char * msg)
{
	// Test is test.  COM interface will be leak...
}*/

ISensor *sensor = NULL;

bool InitSensorGPS(char* msgerr)
{
	ISensorManager *manager;
	ISensorCollection *collection = NULL;

	if (FAILED(CoCreateInstance(CLSID_SensorManager, NULL, CLSCTX_INPROC_SERVER,		// cr�ation de sensor Api
		IID_ISensorManager, (void**)&manager)))
	{
		sprintf(msgerr, "Erreur en cr�ation de Sensor API");
		return false;
	}

	//if (FAILED(manager->GetSensorsByType(SENSOR_TYPE_LOCATION_GPS,&collection)))		return; //pas de r�ponse

	if (FAILED(manager->GetSensorsByCategory(SENSOR_CATEGORY_LOCATION, &collection)))
	{
		sprintf(msgerr, "Pas de capteur trouv� de cat�gorie location");
		return false; // cherche par cat�gorie de capteur
	}
	if (FAILED(manager->RequestPermissions(0, collection, FALSE)))
	{
		sprintf(msgerr, "Pas de permission d'acc�s");
		return false; // demande la permission
	}

	ULONG count = 0;
	collection->GetCount(&count);  // trouve le nombre de collection des capteurs
	if (!count)
	{
		sprintf(msgerr, "Pas de capteur disponible dans la collection");
		return false;
	}

	SENSOR_TYPE_ID type;

	for (ULONG i = 0; i < count; i++)
	{
		collection->GetAt(i, &sensor);
		if (!sensor)
		{
			sprintf(msgerr, "Sensor allocation erreur");
			return false;
		}

		if (FAILED(sensor->GetType(&type)))
		{
			sprintf(msgerr, "Erreur trouver le type de capteur");
			return false;
		}

		if (type == SENSOR_TYPE_LOCATION_GPS || type == SENSOR_TYPE_LOCATION_OTHER)  break;  // si on trouve le capteur GPS
	}

	if (type != SENSOR_TYPE_LOCATION_GPS && type != SENSOR_TYPE_LOCATION_OTHER)
	{
		sprintf(msgerr, "Pas de capteur GPS disponible dans la collection");
		return false;
	}
	
	return true;
}
	

	
bool GetDataSensorGPS(double& Latitude, double& Longitude, double& Vitesse, char* msgerr)
{

	IPortableDeviceKeyCollection* pKeys = NULL; // Output
	HRESULT Res = sensor->GetSupportedDataFields(&pKeys);

	DWORD cKeys;

	if (SUCCEEDED(Res))
	{

		Res = pKeys->GetCount(&cKeys);

		PROPERTYKEY pk;

		ISensorDataReport *report;
		Res = sensor->GetData(&report);

		bool lat_OK = false;
		bool lon_OK = false;
		bool vitesse_OK = false;

		for (DWORD i = 0; i < cKeys; i++)
		{
			HRESULT hr = pKeys->GetAt(i, &pk);

			if (SUCCEEDED(hr))
			{
				// Test for the data fields.
				if (IsEqualPropertyKey(pk, SENSOR_DATA_TYPE_LATITUDE_DEGREES))
				{
					PROPVARIANT P;
					hr = report->GetSensorValue(pk, &P);
					if (P.vt == VT_R8)
					{
						Latitude = P.dblVal; lat_OK = true;
					}
				}
				else if (IsEqualPropertyKey(pk, SENSOR_DATA_TYPE_LONGITUDE_DEGREES))
				{
					PROPVARIANT P;
					hr = report->GetSensorValue(pk, &P);
					if (P.vt == VT_R8)
					{
						Longitude = P.dblVal; lon_OK = true;
					}
						
				}
				else if (IsEqualPropertyKey(pk, SENSOR_DATA_TYPE_SPEED_KNOTS))
				{
					PROPVARIANT P;
					hr = report->GetSensorValue(pk, &P);
					if (P.vt == VT_R8)
					{
						Vitesse = P.dblVal; vitesse_OK = true;
					}
				}
			}
		}

		if (!lat_OK || !lon_OK || !vitesse_OK)
		{
			char* lat_ok = (lat_OK ? "oui" : "non");
			char* lon_ok = (lon_OK ? "oui" : "non");
			char* v_ok = (vitesse_OK ? "oui" : "non");
			sprintf(msgerr, "Pas assez donn�e retourn�e du capteur GPS (lat: %s, lon: %s, vitesse: %s)", lat_ok, lon_ok, v_ok);
			return false;
		}
	}
	else
	{
		sprintf(msgerr, "Pas de donn�e retour du capteur GPS");
		return false;
	}

	return true;
}