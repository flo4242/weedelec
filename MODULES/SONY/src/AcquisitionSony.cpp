#include <chrono>
#include <stdio.h>


#include <cpprest/details/basic_types.h>
#include <cpprest/json.h>
#include <cpprest/ws_client.h>
#include <cpprest/http_client.h>
#include <cpprest/http_msg.h>
#include <cpprest/filestream.h>

#include "ExportModuleSony.h" //fonctions export�es pour utilisation externe (autres modules)
#include "UtilficGen.h" 

#include "TaskUtil.h"
#include "CommonSony.h" 



using namespace utility;                    // Common utilities like string conversions
using namespace web;                        // Common features like URIs.
using namespace web::http;                  // Common HTTP functionality
using namespace web::http::client;          // HTTP client features
using namespace concurrency::streams;       // Asynchronous streams
using namespace std;


#define ID_WEEDELEC 100		//arbitraire

struct POSTSony
{
	string method;
	string params;
	string version;

	POSTSony(string mmethod, string pparams, string vversion) : method(mmethod), params(pparams), version(vversion) { ; }

};



utility::string_t BuildBodyPOST(POSTSony post)

//si params commence par *, on le transmet tel quel
{
	string body = "{";
	body += "\"method\": \"" + post.method + "\",";

	if (post.params.length() == 0)	body += "\"params\": [],";
	else
	{
		if (post.params[0] == '*')	body += "\"params\": [" + post.params.string::substr(1, post.params.length() - 1) + "],";
		//on enl�ve l'�toile et on transmet tel quel

		else if (post.params[0] == '{')		body += "\"params\": [" + post.params + "],";
		//on transmet tel quel

		else body += "\"params\": [\"" + post.params + "\"],";
		//on transmet entre guillements
	}

	char stid[10];
	sprintf(stid, "%d", ID_WEEDELEC);

	body += "\"id\": " + string(stid) + ",";
	body += "\"version\": \"" + post.version + "\"";
	body += "}";

	return utility::conversions::to_string_t(body);
}


http_request BuildRequestPOST(POSTSony post)
{
	try
	{
		http_request request;
		request.set_method(web::http::methods::POST);
		//GR //////		request.headers().add(L"Content-Type", "application/x-www-form-urlencoded");
		//GR //////////		request.headers().add(L"cache-control", "no-cache");
		request.set_body(BuildBodyPOST(post));
		return request;
	}
	catch (std::exception e)
	{
		MessageBox(NULL, e.what(), "BuildRequestPOST", MB_OK);
	}
}



TRetourState SendRequestPOST(http_client client, POSTSony post, web::json::value &result, string &msg, int timeout, TSonyStop &stopSony)
{
	
	http_request request = BuildRequestPOST(post);

	task<http_response> postRequest = client.request(request, stopSony.Cts->get_token());
	TaskWithTimeoutState State;
	
	http_response reponse = DoTaskWithTimeout(postRequest, *stopSony.Cts, timeout, State, stopSony.Tce);

	TRetourTask Ret;

	switch (State.state)
	{
		case TaskWithTimeoutState::Timeout:	 msg = "Timeout";	return TRetourState::TimeOut;
		case TaskWithTimeoutState::ExternallyCancelled:	msg = "External stop";	return TRetourState::ExternalStop;
		case TaskWithTimeoutState::Error:	msg = State.ErrorMsg;	return TRetourState::Error;
	}

	result = reponse.extract_json().get();
	if (result.is_null())
	{
		msg = "Pas de retour " + post.method;
		return TRetourState::Error;
	}

	msg = utility::conversions::to_utf8string(result.serialize());
	return TRetourState::Completed;
}


bool AnalyseAcquisitionResult(web::json::value result, string &fileUrl)
 {

 
	 /*	forme attendue du r�sultat:
	 {
	 "result": [
		 [
			 "http://192.168.122.1:8080/postview/memory/DCIM/100MSDCF/DSC00020.JPG?size=Scn"
		 ]
	 ],
	 "id": 1
	 }								*/

	 if (!result.is_object())	return false;
	 if (!result.has_array_field(L"result")) return false;
	 web::json::value resultdata = result.at(L"result");

	 if (!resultdata.is_array())	return false;
	 if (resultdata.size()==0)	return false;

	 web::json::value arrayfiles = resultdata[0];
	 if (!arrayfiles.is_array()) return false;
	 if (arrayfiles.size() == 0)	return false;

	 web::json::value file = arrayfiles[0];
	 if (!file.is_string())	return false;
			
	 fileUrl = utility::conversions::to_utf8string(file.as_string());
	 return true;

 }

 string GetNomImage(string &url)
 {
	//exemple de fileUrl: "http://192.168.122.1:8080/postview/memory/DCIM/100MSDCF/DSC00027.JPG?size=Scn\"

	//simulation d'un nom de fichier
	TNomFic NfImage;
	int len = url.length();
	int offset = strlen("http://");
	for (int i = offset; i < len; i++)
	{
		if (url[i] == '?') { NfImage[i - offset] = '\0';	break; }
		if (url[i] == '/') NfImage[i - offset] = '\\';
		else NfImage[i - offset] = url[i];
	}

	string nomImage = NomSansPath(NfImage);
	return nomImage;

}

 void GetFieldValue(json::value src, string key,  _list<json::value> &listeout)
 {
	 if (!src.is_array() && !src.is_object()) return;
	 


	 string_t key_t = conversions::to_string_t(key);

	 if (src.is_object())
	 {
		 json::object obj = src.as_object();
		 for each (auto val in obj)
		 {
			 if (val.first == key_t)	listeout.push_back(val.second);
			 GetFieldValue(val.second, key, listeout);	//r�cursif

		 }
	 }

	 if (src.is_array())
	 {
		 json::array obj = src.as_array();
		 for each (auto val in obj)
		 {
			 GetFieldValue(val, key, listeout);	//r�cursif

		 }
	 }

 }


 int GetImageNumber(json::value src, string &msg)
 {
	 _list<json::value> listeout;
	 GetFieldValue(src, "count", listeout);

	 if (listeout.nbElem == 0)
	 {
		 msg = "Pas de cl� \"count\" dans cet objet";
		 return -1;
	 }
	 
	 if (listeout.nbElem > 1)
	 {
		 char stmsg[500];
		 sprintf(stmsg, "Trop de cl�s \"count\" (%d) dans cet objet", listeout.nbElem);
		 msg = stmsg;
		 return -1;
	 }

	 json::value val = listeout.head->obj;
	 if (!val.is_integer())
	 {
		 msg = "La cl� \"count\" ne correspond pas � un nombre entier";
		 return -1;
	 }

	 return val.as_integer();

 }

 //pour t�l�chargement
 bool GetImageUrl(json::value src, string &url, string &imageNameout, string &msg)
 {
	 _list<json::value> listeout;
	 GetFieldValue(src, "original", listeout);

	 if (listeout.nbElem == 0)
	 {
		 msg = "Pas de cl� \"original\" dans cet objet";
		 return false;
	 }

	 if (listeout.nbElem > 1)
	 {
		 char stmsg[500];
		 sprintf(stmsg, "Trop de cl�s \"original\" (%d) dans cet objet", listeout.nbElem);
		 msg = stmsg;
		 return false;
	 }

	 json::value valOriginal = listeout.head->obj;
	

	 //r�cup�ration filename
	 listeout.clear();
	 GetFieldValue(valOriginal, "fileName", listeout);
	 if (listeout.nbElem != 1)
	 {
		 msg = "Pas ou trop de cl�(s) \"fileName\" pour cette image";
		 return false;
	 }

	 json::value val = listeout.head->obj;


	 if (!val.is_string())
	 {
		 msg = "La cl� \"fileName\" ne correspond pas � une string !!!";
		 return false;
	 }
	 imageNameout = conversions::to_utf8string(val.as_string());

	 //r�cup�ration url
	 listeout.clear();
	 GetFieldValue(valOriginal, "url", listeout);
	 if (listeout.nbElem != 1)
	 {
		 msg = "Pas ou trop de cl�(s) \"url\" pour cette image";
		 return false;
	 }

	 val = listeout.head->obj;
	 if (!val.is_string())
	 {
		 msg = "La cl� \"url\" ne correspond pas � une string !!!";
		 return false;
	 }
	 url = conversions::to_utf8string(val.as_string());
	 return true;
 }

 //pour effacement
 bool GetImageContentUri(json::value src, string &uri, string &msg)
 {
	 _list<json::value> listeout;
	 GetFieldValue(src, "uri", listeout);
	 
	 if (listeout.nbElem == 0)
	 {
		 msg = "Pas de cl� \"uri\" dans cet objet";
		 return false;
	 }

	 if (listeout.nbElem > 1)
	 {
		 char stmsg[500];
		 sprintf(stmsg, "Trop de cl�s \"uri\" (%d) dans cet objet", listeout.nbElem);
		 msg = stmsg;
		 return false;
	 }

	 json::value valUri = listeout.head->obj;

	 if(!valUri.is_string())
	 {
		 msg = "La cl� \"uri\" ne correspond pas � une string !!!";
		 return false;
	 }

	 uri = conversions::to_utf8string(valUri.as_string());
	 return true;
 }


 bool DownloadUrl(string url, string NfOut, string &msg)
//d'apr�s https://github.com/Microsoft/cpprestsdk/wiki/Getting-Started-Tutorial

//la cascade des .then() a �t� d�construite pour un meilleur suivi debug
 {

	 // Open stream to output file.

	 utility::string_t Nf = utility::conversions::to_string_t(NfOut);
	 //auto fileStream = std::make_shared<concurrency::streams::ostream>();

	 //ouverture stream
	 concurrency::task<concurrency::streams::ostream> streamOpenTask;
	 streamOpenTask = concurrency::streams::fstream::open_ostream(Nf, ios::out | ios::binary);

	 concurrency::streams::ostream  os;

	 try
	 {
		 os = streamOpenTask.get();
	 }

	 catch (exception e)
	 {
		 msg = "Echec ouverture fichier";
		 return false;
	 }


	 //lance la requ�te de t�l�chargement
	 utility::string_t url_t = utility::conversions::to_string_t(url);
	 web::http::client::http_client clientimg(url_t);
	 http_request request(web::http::methods::GET);
	 http_response response;

	 pplx::task<http_response> requestTask = clientimg.request(request, SonyStopG.Cts->get_token());

	 try
	 {
		 response = requestTask.get();
	 }

	 catch (exception)
	 {
		 msg = "Echec lancement requ�te de t�l�chargement";
		 return false;
	 }


	 //printf("Received response status code:%u\n", response.status_code());
	  // Write response body into the file.

	 pplx::task<size_t> readTask = response.body().read_to_end(os.streambuf());

	 try
	 {
		 size_t sizeread = readTask.get();
	 }

	 catch (exception)
	 {
		 msg = "Echec lecture corps de la r�ponse de requ�te";
		 return false;
	 } 

	 os.close().wait();

	 return true;
 }




 void SetRetourNotCompletedSony(TRetourAcqSony &retsony, TRetourState retstate, string errormsg, int dureeMs)
 {
	 retsony.State = retstate;

	 switch (retstate)
	 {
	
		 case TRetourState::Error:
			 retsony.InfoRetour = errormsg;
			 break;

		 case TRetourState::TimeOut:
			 retsony.InfoRetour = "Time out. V�rifier la connexion et/ou le param�tre de time out";
			 break;

		 case TRetourState::ExternalStop:
			 retsony.InfoRetour = "Demande d'arr�t externe";
			 break;
	 }
	 retsony.DureeMs = dureeMs;
 }

//////////////////////////////////////////////////////////////////////////
 void TModuleSony::AcquisitionSony(TRetourAcqSony &retour)
//////////////////////////////////////////////////////////////////////////
 {
	 SonyStopG.ReEnclenche();

	int TimeDebut = GetTickCount();

	 TParamAcquisitionSony Param;
	 TParamDetectionSony dummy;
	 TParamCalage dummy2;
	 if(!ChargeParametres(Param, dummy, dummy2))
	 {
		 SetRetourNotCompletedSony(retour, TRetourState::Error, "Echec chargement param�tres", GetTickCount() - TimeDebut);
		 return;
	 }
	
	 ////////////////////////
	 //acquisition de l'image
	 ////////////////////////

	 
	 string stcamera = "http://" + string(Param.AdresseIP) + ":" + string(Param.PortIP) + "/sony/camera";
	 utility::string_t adrcamera = conversions::to_string_t(stcamera);
	 
	 http_client ClientCamera(adrcamera);

	 web::json::value result;
	 string msg;


	 //passage en RecMode (indispensable pour atteindre l'interface compatible avec la doc SDK)
	 POSTSony post("startRecMode", "", "1.0");
	 TRetourState ret = SendRequestPOST(ClientCamera, post, result, msg, Param.TimeOutIP, SonyStopG);
	 
	 if (ret != TRetourState::Completed)
	 {
		 SetRetourNotCompletedSony(retour, ret, "Echec passage en RecMode : " + msg, GetTickCount() - TimeDebut);
		 return;
	 }
	 
	 //passage en mode capture
	 post = POSTSony("setCameraFunction", "Remote Shooting", "1.0");
	 ret = SendRequestPOST(ClientCamera, post, result, msg, Param.TimeOutIP, SonyStopG);

	 if (ret != TRetourState::Completed)
	 {
		 SetRetourNotCompletedSony(retour, ret, "Echec passage en mode capture: " + msg, GetTickCount() - TimeDebut);
		 return;
	 }

	 //shooting

	 //boucle d'attente status IDLE
	 
	 while (true)  
	 {
		 post = POSTSony("getEvent", "*false", "1.0");
		 ret = SendRequestPOST(ClientCamera, post, result, msg, Param.TimeOutIP, SonyStopG);

		 if (ret != TRetourState::Completed)
		 {
			 SetRetourNotCompletedSony(retour, ret, "Echec POST getEvent" + msg, GetTickCount() - TimeDebut);
			 return;
		 }

		 msg = utility::conversions::to_utf8string(result.serialize());

		 _list<json::value> listeout;
		 GetFieldValue(result, "cameraStatus", listeout);
		 if (listeout.nbElem != 1)
		 {
			 SetRetourNotCompletedSony(retour, TRetourState::Error, "Echec lecture cameraStatus :" + msg, GetTickCount() - TimeDebut);
			 return;
		 }
		 

		 json::value status = listeout.head->obj;
		 if (!status.is_string())
		 {
			 SetRetourNotCompletedSony(retour, TRetourState::Error, "Echec lecture cameraStatus (not a string)", GetTickCount() - TimeDebut);
			 return;
		 }


		if (GetTickCount() - TimeDebut > Param.TimeOutIP)
		{
			SetRetourNotCompletedSony(retour, TRetourState::Error, "Timeout waiting status IDLE", GetTickCount() - TimeDebut);
			return;
		}

		if (SonyStopG.Cts->get_token().is_canceled())
		{
			SetRetourNotCompletedSony(retour, TRetourState::ExternalStop, "", GetTickCount() - TimeDebut);
			return;
		}

		 if (status.as_string() == string_t(L"IDLE")) break;	//  OK --> sortie de boucle

			
	 }

	 //ordre de shooting
	 post = POSTSony("actTakePicture", "", "1.0");
	 ret = SendRequestPOST(ClientCamera, post, result, msg, Param.TimeOutIP, SonyStopG);

	 if (ret != TRetourState::Completed)
	 {
		 SetRetourNotCompletedSony(retour, ret, "Echec POST acquisition: " + msg, GetTickCount() - TimeDebut);
		 return;
	 }


	 //r�cup�ration du nom de l'image:

	 string fileUrl = "";

	 if (!AnalyseAcquisitionResult(result, fileUrl))
	 {
		 SetRetourNotCompletedSony(retour, TRetourState::Error, "Echec acquisition", GetTickCount() - TimeDebut);
		 return;
	 }
	 
	 //fileUrl ne pointe qu'une image r�duite. Pour t�l�charger l'image originale, il faut passer en mode
	 // "avContent". On r�cup�re avant le nom de l'image pour v�rif ult�rieure.
	 //exemple de fileUrl: "http://192.168.122.1:8080/postview/memory/DCIM/100MSDCF/DSC00027.JPG?size=Scn\"

	
	 retour.State = TRetourState::Completed;
	 retour.InfoRetour = fileUrl;
	 retour.DureeMs = GetTickCount() - TimeDebut;
	 return;
 }


 void SetRetourNotCompletedSony(TRetourDownloadSony &retsony, TRetourState retstate, string errormsg, int dureeMs, int nbImagesSD)
 {
	 retsony.State = retstate;

	 switch (retstate)
	 {
	 case TRetourState::Error:
		 retsony.InfoRetour = errormsg;
		 break;

	 case TRetourState::TimeOut:
		 retsony.InfoRetour = "Time out. V�rifier la connexion et/ou le param�tre de time out";
		 break;

	 case TRetourState::ExternalStop:
		 retsony.InfoRetour = "Demande d'arr�t externe";
		 break;
	 }

	 retsony.DureeMs = dureeMs;
	 retsony.NbTotalImagesSD = nbImagesSD;
 }


void TModuleSony::DownloadSony(TRetourDownloadSony &retour, std::string UrlImageReduite)
{
	SonyStopG.ReEnclenche();

	int TimeDebut = GetTickCount();

	TParamAcquisitionSony Param;
	TParamDetectionSony dummy;
	TParamCalage dummy2;
	ChargeParametres(Param, dummy, dummy2);

	string stcamera = "http://" + string(Param.AdresseIP) + ":" + string(Param.PortIP) + "/sony/camera";
	utility::string_t adrcamera = conversions::to_string_t(stcamera);
	http_client ClientCamera(adrcamera);

	web::json::value result;
	string msg;

	//passage en RecMode (indispensable pour atteindre l'interface compatible avec la doc SDK)
	
	POSTSony post("startRecMode", "", "1.0");
	TRetourState ret = SendRequestPOST(ClientCamera, post, result, msg, Param.TimeOutIP, SonyStopG);

	if (ret != TRetourState::Completed)
	{
		SetRetourNotCompletedSony(retour, ret, "Echec passage en RecMode: " + msg, GetTickCount() - TimeDebut, -1);
		return;
	}

	
	//passage en mode transfert

	post = POSTSony("setCameraFunction", "Contents Transfer", "1.0");
	ret = SendRequestPOST(ClientCamera, post, result, msg, Param.TimeOutIP, SonyStopG);

	if (ret != TRetourState::Completed)
	{
		SetRetourNotCompletedSony(retour, ret, "Echec passage en en mode transfert: " + msg, GetTickCount() - TimeDebut, -1);
		return;
	}

	//d�sormais, on envoie les POST sur un nouveau service

	string stContent = "http://" + string(Param.AdresseIP) + ":" + string(Param.PortIP) + "/sony/avContent";
	utility::string_t adrcontent = conversions::to_string_t(stContent);
	http_client ClientContent(adrcontent);

	
	//nb de photos en m�moire ?

	string params = "{\"uri\": \"storage:memoryCard1\", \"view\" : \"flat\", \"target\" : \"all\" }";

	post = POSTSony("getContentCount", params, "1.2");
	ret = SendRequestPOST(ClientContent, post, result, msg, Param.TimeOutIP, SonyStopG);

	if (ret != TRetourState::Completed)
	{
		SetRetourNotCompletedSony(retour, ret, "Echec demande nb photos sur carte SD: " + msg, GetTickCount() - TimeDebut, -1);
		return;
	}


	int NbImages = GetImageNumber(result, msg);
	if (NbImages<0)
	{
		SetRetourNotCompletedSony(retour, TRetourState::Error, "Echec d�codage nb photos sur carte SD: " + msg, GetTickCount() - TimeDebut, -1);
		return;
	}
	

	if (NbImages == 0)
	{
		SetRetourNotCompletedSony(retour, TRetourState::Error, "Pas d'image � t�l�charger sur la carte SD", GetTickCount() - TimeDebut, 0);
		return;
	}

	
	//retrouve le nom et l'url de l'image � charger
	
	/*
	{"jsonrpc": "2.0", "method" : "getContentList", "params" : [{
		"uri": "storage:memoryCard1",
			"stIdx" : 0,
			"cnt" : 50,
			"view" : "flat",
			"sort" : ""
	}], "id": 1, "version" : "1.3"} */

	char stIndexStart[10]; sprintf(stIndexStart, "%d", NbImages - 1);	//derni�re image
	params = "{\"uri\": \"storage:memoryCard1\",";
	params += "\"stIdx\":" + string(stIndexStart) + ",";
	params += "\"cnt\": 1,";
	params += "\"view\": \"flat\"";
	params += "}";

	post = POSTSony("getContentList", params, "1.3");
	ret = SendRequestPOST(ClientContent, post, result, msg, Param.TimeOutIP, SonyStopG);

	if (ret != TRetourState::Completed)
	{
		SetRetourNotCompletedSony(retour, ret, "Echec POST r�cup�ration url derni�re image sur carte SD: " + msg, GetTickCount() - TimeDebut, NbImages);
		return;
	}
	

	string UrlImage, NomImageSD;

	if(!GetImageUrl(result, UrlImage, NomImageSD, msg))
	{
		SetRetourNotCompletedSony(retour, TRetourState::Error, "Echec r�cup�ration url derni�re image sur carte SD", GetTickCount() - TimeDebut, NbImages);
		return;
	}
	

	if (UrlImageReduite.length() != 0) //le nom d'image � retrouver a �t� sp�cifi�
	{
		string NomImage = GetNomImage(UrlImageReduite);

		if (NomImageSD != NomImage)
		{
			SetRetourNotCompletedSony(retour, TRetourState::Error, "R�cup�ration image carte SD: L'url obtenue ne correspond pas au nom image attendu", GetTickCount() - TimeDebut, NbImages);
			return;
		}
	}
		

	   
	//t�l�chargement de l'image
	string NfOut = string(Param.DossierImages) + "\\" + NomImageSD;

	if(!DownloadUrl(UrlImage, NfOut, msg))
	{
		SetRetourNotCompletedSony(retour, TRetourState::Error, "Echec t�l�chargement: " + msg, GetTickCount() - TimeDebut, NbImages);
		return;
	}

	//effacement �ventuel sur la carte SD
	if (Param.EffaceImageSD)
	{
		string UriEffacement;
		if (!GetImageContentUri(result, UriEffacement, msg))
		{
			SetRetourNotCompletedSony(retour, TRetourState::Error, "Echec r�cup�ration uri d'effacement", GetTickCount() - TimeDebut, NbImages);
			return;
		}
		

		/* exemple:
		"params": [	{
			"uri":
			[
				"image:content?contentId=XXXXXXXXXX",
					"video:content?contentId=XXXXXXXXXX"
			]
		}]  */

		params = "{\"uri\": ";
		params += "[\"" + UriEffacement + "\"]";
		params += "}";

		post = POSTSony("deleteContent", params, "1.1");
		TRetourState ret = SendRequestPOST(ClientContent, post, result, msg, Param.TimeOutIP, SonyStopG);

		if (ret != TRetourState::Completed)
		{
			SetRetourNotCompletedSony(retour, ret, "Echec POST effacement derni�re image sur carte SD: " + msg, GetTickCount() - TimeDebut, NbImages);
			return;
		}
		
	}

	//demande finale nombre de photos restantes sur carte SD

	params = "{\"uri\": \"storage:memoryCard1\", \"view\" : \"flat\", \"target\" : \"all\" }";

	post = POSTSony("getContentCount", params, "1.2");
	ret = SendRequestPOST(ClientContent, post, result, msg, Param.TimeOutIP, SonyStopG);

	if (ret != TRetourState::Completed)
	{
		SetRetourNotCompletedSony(retour, ret, "Echec demande nb photos sur carte SD: " + msg, GetTickCount() - TimeDebut, NbImages);
		return;
	}

	NbImages = GetImageNumber(result, msg);
	if (NbImages < 0)
	{
		SetRetourNotCompletedSony(retour, ret, "Echec d�codage nb photos sur carte SD", GetTickCount() - TimeDebut, -1);
		return;
	}
	

	//retour en mode capture

	post = POSTSony("setCameraFunction", "Remote Shooting", "1.0");
	ret = SendRequestPOST(ClientCamera, post, result, msg, Param.TimeOutIP, SonyStopG);

	if (ret != TRetourState::Completed)
	{
		SetRetourNotCompletedSony(retour, ret, "Echec retour en mode capture: " + msg, GetTickCount() - TimeDebut, NbImages);
		return;
	}

	////////////////////////////////////////////////////////////
	retour.State = TRetourState::Completed;
	retour.InfoRetour = NfOut;
	retour.NbTotalImagesSD = NbImages;
	retour.DureeMs = GetTickCount() - TimeDebut;
	return;
	////////////////////////////////////////////////////////////
}

bool SetZoomOptiqueMax(string &MsgErr)
//////////////////////////////////////////
{

	SonyStopG.ReEnclenche();

	TParamAcquisitionSony Param;
	TParamDetectionSony dummy;
	TParamCalage dummy2;
	TModuleSony::ChargeParametres(Param, dummy, dummy2);

	string stcamera = "http://" + string(Param.AdresseIP) + ":" + string(Param.PortIP) + "/sony/camera";
	utility::string_t adrcamera = conversions::to_string_t(stcamera);

	http_client ClientCamera(adrcamera);

	web::json::value result;
	string msg;


	//passage en RecMode (indispensable pour atteindre l'interface compatible avec la doc SDK)
	POSTSony post("startRecMode", "", "1.0");
	TRetourState ret = SendRequestPOST(ClientCamera, post, result, msg, Param.TimeOutIP, SonyStopG);

	if (ret != TRetourState::Completed)
	{
		MsgErr = "Echec passage en RecMode : " + msg;
		return false;
	}



	//zoom optique setting

	string params = "{\"zoom\": \"Optical Zoom Only\"}";
	post = POSTSony("setZoomSetting", params, "1.0");
	ret = SendRequestPOST(ClientCamera, post, result, msg, Param.TimeOutIP, SonyStopG);

	if (ret != TRetourState::Completed)
	{
		MsgErr = "Echec setZoomSetting: " + msg;
		return false;
	}


	//actuation zoom

	params = "*\"in\", \"start\"";
	
	post = POSTSony("actZoom", params, "1.0");
	ret = SendRequestPOST(ClientCamera, post, result, msg, Param.TimeOutIP, SonyStopG);

	if (ret != TRetourState::Completed)
	{
		MsgErr = "Echec actZoom: " + msg;
		return false;
	}

	
	return true;
}



////////// LIVEVIEW ////////////////////////////

#include "LiveViewSony.h"

int ReadCommonPacketHeader(streams::istream &stream)
//retour avec -1 si erreur de lecture
//voir reference api sony page 270
{
	long dummy;

	//lecture Common Header

	dummy = stream.read().get();
	if (dummy != 0xFF) return -10;

	dummy = stream.read().get();
	if (dummy != 0x01) return -dummy; //liveview


	for (int i = 0; i < 6; i++) 		dummy = stream.read().get();
	return 0;
}


int ReadLiveStreamPacket(streams::istream &stream, unsigned char *JPEGData, int AvailableSize)
//retour avec la taille de JPEGData
//retour avec -1 si erreur de lecture
//retour avec -2 AvailableSize est insuffisant
//voir reference api sony page 270
{
	long dummy;

	//lecture Payload Header (128 octets)
	dummy = stream.read().get();
	if (dummy != 0x24) return -12;
	dummy = stream.read().get();
	if (dummy != 0x35) return -13;
	dummy = stream.read().get();
	if (dummy != 0x68) return -14;
	dummy = stream.read().get();
	if (dummy != 0x79) return -14;


	//lecture payload size --> network byte order --> big endian

	int payloadjpeg = dummy = stream.read().get() << 16;
	payloadjpeg += stream.read().get() << 8;
	payloadjpeg += stream.read().get();

	if (payloadjpeg >= AvailableSize)
		return -2; //inutile d'aller plus loin

	long paddingsize = stream.read().get();

	for (int i = 0; i < 120; i++)	stream.read().get(); //skip du reste du header



	//lecture data

	//for (int i = 0; i < payloadjpeg; i++)	JPEGData[i] = stream.read().get();

	auto sbuf = stream.streambuf();
	sbuf.getn(JPEGData, payloadjpeg).get();

	for (int i = 0; i < paddingsize; i++) dummy = stream.read().get(); //skip fin des data

	return payloadjpeg;
}





bool StartLiveView(unsigned char *JPEGData, int AvailableSize, bool formatlarge, callBackLiveView callback, char *MsgErr)
//MsgInfo contient le message d'erreur si retour avec false

{

	SonyStopG.ReEnclenche();

	TParamAcquisitionSony Param;
	TParamDetectionSony dummy;
	TParamCalage dummy2;
	TModuleSony::ChargeParametres(Param, dummy, dummy2);

	string stcamera = "http://" + string(Param.AdresseIP) + ":" + string(Param.PortIP) + "/sony/camera";
	utility::string_t adrcamera = conversions::to_string_t(stcamera);

	http_client ClientCamera(adrcamera);

	web::json::value result;
	string msg;


	//passage en RecMode (indispensable pour atteindre l'interface compatible avec la doc SDK)
	POSTSony post("startRecMode", "", "1.0");
	TRetourState ret = SendRequestPOST(ClientCamera, post, result, msg, Param.TimeOutIP, SonyStopG);

	if (ret != TRetourState::Completed)
	{
		sprintf(MsgErr, "Echec passage en RecMode : %s", msg.data());
		return false;
	}

	//d�marrage live view avec sp�cification du format

	char format[5];
	strcpy(format, formatlarge ? "L" : "M");

	post = POSTSony("startLiveviewWithSize", format, "1.0");
	ret = SendRequestPOST(ClientCamera, post, result, msg, Param.TimeOutIP, SonyStopG);

	if (ret != TRetourState::Completed)
	{
		sprintf(MsgErr, "Echec startLiveview : %s", msg.data());
		return false;
	}


	_list<json::value> listeout;
	GetFieldValue(result, "result", listeout);

	if (listeout.nbElem == 0)
	{
		MsgErr = "Pas de cl� \"result\" dans cet objet";
		return false;
	}

	//format: ["... url..."];
	json::array res = listeout.head->obj.as_array();
	string_t url_st = res.at(0).as_string();

	
	//lance la requ�te de t�l�chargement
	web::http::client::http_client clientimg(url_st);
	http_request request(web::http::methods::GET);
	http_response response;

	
	pplx::task<http_response> requestTask = clientimg.request(request, SonyStopG.Cts->get_token());

	try
	{
		response = requestTask.get();
	}

	catch (exception)
	{
		MsgErr = "Echec lancement requ�te de t�l�chargement";
		return false;
	}

	streams::istream stream= response.body();		//stream de lecture
	
	

	while (true)
	{
		int retcommon = ReadCommonPacketHeader(stream);
		if (retcommon < 0) { sprintf(MsgErr, "Erreur lecture common header %d", retcommon);		return false; };

		int sizeJPEGData = ReadLiveStreamPacket(stream, JPEGData, AvailableSize);

		if (sizeJPEGData == -1) { strcpy(MsgErr, "Erreur lecture stream");		return false; }
		if (sizeJPEGData == -2) { strcpy(MsgErr, "Taille buffer insuffisante");		return false;}

		if (!callback(sizeJPEGData)) { strcpy(MsgErr, "Arr�t utilisateur");		return true;	}

	}
	
	return true;
}



bool StopLiveView(char *MsgErr)
//MsgInfo contient le message d'erreur si retour avec false

{

	SonyStopG.ReEnclenche();

	TParamAcquisitionSony Param;
	TParamDetectionSony dummy;
	TParamCalage dummy2;
	TModuleSony::ChargeParametres(Param, dummy, dummy2);

	string stcamera = "http://" + string(Param.AdresseIP) + ":" + string(Param.PortIP) + "/sony/camera";
	utility::string_t adrcamera = conversions::to_string_t(stcamera);

	http_client ClientCamera(adrcamera);

	web::json::value result;
	string msg;


	//passage en RecMode (indispensable pour atteindre l'interface compatible avec la doc SDK)
	POSTSony post("startRecMode", "", "1.0");
	TRetourState ret = SendRequestPOST(ClientCamera, post, result, msg, Param.TimeOutIP, SonyStopG);

	if (ret != TRetourState::Completed)
	{
		sprintf(MsgErr, "Echec passage en RecMode : %s", msg.data());
		return false;
	}

	post = POSTSony("stopLiveview", "", "1.0");
	ret = SendRequestPOST(ClientCamera, post, result, msg, Param.TimeOutIP, SonyStopG);

	if (ret != TRetourState::Completed)
	{
		sprintf(MsgErr, "Echec startLiveview : %s", msg.data());
		return false;
	}


	_list<json::value> listeout;
	GetFieldValue(result, "result", listeout);

	if (listeout.nbElem == 0)
	{
		MsgErr = "Pas de cl� \"result\" dans cet objet";
		return false;
	}

	
	return true;
}

