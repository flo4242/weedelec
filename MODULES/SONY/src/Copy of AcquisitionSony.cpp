#include <chrono>
#include <Windows.h>"
#include <stdio.h>"

#include "ExportModuleSony.h" //fonctions export�es pour utilisation externe (autres modules)

#include "drvtraiti.h"
extern DRVTRAITIM *pDriverG; //pour debug


#include <cpprest/details/basic_types.h>
#include <cpprest/json.h>
#include <cpprest/ws_client.h>
#include <cpprest/http_client.h>
#include <cpprest/http_msg.h>
#include <cpprest/filestream.h>


using namespace utility;                    // Common utilities like string conversions
using namespace web;                        // Common features like URIs.
using namespace web::http;                  // Common HTTP functionality
using namespace web::http::client;          // HTTP client features
using namespace concurrency::streams;       // Asynchronous streams

#define ID_WEEDELEC 100		//arbitraire


utility::string_t BuildBodyPOST(string method, string params, string version)

//si params commence par *, on le transmet tel quel
{
	string body = "{";
	body += "\"method\": \"" + method + "\",";

	if(params.length()==0)	body += "\"params\": [],";
	else
	{
		if (params[0] == '*')	body += "\"params\": [" + params.string::substr(1, params.length()-1) + "],";
		//on enl�ve l'�toile et on transmet tel quel

		else if (params[0] == '{')		body += "\"params\": [" + params + "],";
		//on transmet tel quel
		
		else body += "\"params\": [\"" + params + "\"],";
		//on transmet entre guillements
	}

	char stid[10];
	sprintf(stid, "%d", ID_WEEDELEC);

	body += "\"id\": " + string(stid) + ","; 
	body += "\"version\": \"" + version + "\"";
	body += "}";

	return utility::conversions::to_string_t(body);
}


http_request BuildRequestPOST(string method, string params, string version)
{
	http_request request(web::http::methods::POST);
	//request.headers().add(L"Host", "92.168.122.1:8080");
	request.headers().add(L"Content-Type", "application/x-www-form-urlencoded");
	request.headers().add(L"cache-control", "no-cache");
	request.set_body(BuildBodyPOST(method, params, version));
	return request;
}



 bool SendRequestPOST(http_client client, string method, string params, string version, web::json::value &result, string &msg)
{
	http_request request = BuildRequestPOST(method, params, version);
	pplx::task<http_response> reponse = client.request(request);

	try
	{
		http_response rep = reponse.get();
		result = rep.extract_json().get();
		if (result.is_null())
		{
			msg = "Pas de retour " + method;
			return false;
		}
		msg = utility::conversions::to_utf8string(result.serialize());
		return true;

	}

	catch (std::exception e)
	{
		msg = e.what();
		return false;
	}
}



 bool AnalyseAcquisitionResult(web::json::value result, string &fileUrl)
 {

 
	 /*	forme attendue du r�sultat:
	 {
	 "result": [
		 [
			 "http://192.168.122.1:8080/postview/memory/DCIM/100MSDCF/DSC00020.JPG?size=Scn"
		 ]
	 ],
	 "id": 1
	 }								*/

	 if (!result.is_object())	return false;
	 if (!result.has_array_field(L"result")) return false;
	 web::json::value resultdata = result.at(L"result");

	 if (!resultdata.is_array())	return false;
	 if (resultdata.size()==0)	return false;

	 web::json::value arrayfiles = resultdata[0];
	 if (!arrayfiles.is_array()) return false;
	 if (arrayfiles.size() == 0)	return false;

	 web::json::value file = arrayfiles[0];
	 if (!file.is_string())	return false;
			
	 fileUrl = utility::conversions::to_utf8string(file.as_string());
	 return true;

 }

 string GetNomImage(string &url)
 {
	//exemple de fileUrl: "http://192.168.122.1:8080/postview/memory/DCIM/100MSDCF/DSC00027.JPG?size=Scn\"

	//simulation d'un nom de fichier
	TNomFic NfImage;
	int len = url.length();
	int offset = strlen("http://");
	for (int i = offset; i < len; i++)
	{
		if (url[i] == '?') { NfImage[i - offset] = '\0';	break; }
		if (url[i] == '/') NfImage[i - offset] = '\\';
		else NfImage[i - offset] = url[i];
	}

	string nomImage = NomSansPath(NfImage);
	return nomImage;

}

 void GetFieldValue(json::value src, string key,  _list<json::value> &listeout)
 {
	 if (!src.is_array() && !src.is_object()) return;
	 
	 string_t key_t = conversions::to_string_t(key);

	 if (src.is_object())
	 {
		 json::object obj = src.as_object();
		 for each (auto val in obj)
		 {
			 if (val.first == key_t)	listeout.push_back(val.second);
			 GetFieldValue(val.second, key, listeout);	//r�cursif

		 }
	 }

	 if (src.is_array())
	 {
		 json::array obj = src.as_array();
		 for each (auto val in obj)
		 {
			 GetFieldValue(val, key, listeout);	//r�cursif

		 }
	 }

 }


 int GetImageNumber(json::value src, string &msg)
 {
	 _list<json::value> listeout;
	 GetFieldValue(src, "count", listeout);

	 if (listeout.nbElem == 0)
	 {
		 msg = "Pas de cl� \"count\" dans cet objet";
		 return -1;
	 }
	 
	 if (listeout.nbElem > 1)
	 {
		 char stmsg[500];
		 sprintf(stmsg, "Trop de cl�s \"count\" (%d) dans cet objet", listeout.nbElem);
		 msg = stmsg;
		 return -1;
	 }

	 json::value val = listeout.head->obj;
	 if (!val.is_integer())
	 {
		 msg = "La cl� \"count\" ne correspond pas � un nombre entier";
		 return -1;
	 }

	 return val.as_integer();

 }

 //pour t�l�chargement
 bool GetImageUrl(json::value src, string &url, string &imageNameout, string &msg)
 {
	 _list<json::value> listeout;
	 GetFieldValue(src, "original", listeout);

	 if (listeout.nbElem == 0)
	 {
		 msg = "Pas de cl� \"original\" dans cet objet";
		 return false;
	 }

	 if (listeout.nbElem > 1)
	 {
		 char stmsg[500];
		 sprintf(stmsg, "Trop de cl�s \"original\" (%d) dans cet objet", listeout.nbElem);
		 msg = stmsg;
		 return false;
	 }

	 json::value valOriginal = listeout.head->obj;
	

	 //r�cup�ration filename
	 listeout.clear();
	 GetFieldValue(valOriginal, "fileName", listeout);
	 if (listeout.nbElem != 1)
	 {
		 msg = "Pas ou trop de cl�(s) \"fileName\" pour cette image";
		 return false;
	 }

	 json::value val = listeout.head->obj;


	 if (!val.is_string())
	 {
		 msg = "La cl� \"fileName\" ne correspond pas � une string !!!";
		 return false;
	 }
	 imageNameout = conversions::to_utf8string(val.as_string());

	 //r�cup�ration url
	 listeout.clear();
	 GetFieldValue(valOriginal, "url", listeout);
	 if (listeout.nbElem != 1)
	 {
		 msg = "Pas ou trop de cl�(s) \"url\" pour cette image";
		 return false;
	 }

	 val = listeout.head->obj;
	 if (!val.is_string())
	 {
		 msg = "La cl� \"url\" ne correspond pas � une string !!!";
		 return false;
	 }
	 url = conversions::to_utf8string(val.as_string());
	 return true;
 }

 //pour effacement
 bool GetImageContentUri(json::value src, string &uri, string &msg)
 {
	 _list<json::value> listeout;
	 GetFieldValue(src, "uri", listeout);
	 
	 if (listeout.nbElem == 0)
	 {
		 msg = "Pas de cl� \"uri\" dans cet objet";
		 return false;
	 }

	 if (listeout.nbElem > 1)
	 {
		 char stmsg[500];
		 sprintf(stmsg, "Trop de cl�s \"uri\" (%d) dans cet objet", listeout.nbElem);
		 msg = stmsg;
		 return false;
	 }

	 json::value valUri = listeout.head->obj;

	 if(!valUri.is_string())
	 {
		 msg = "La cl� \"uri\" ne correspond pas � une string !!!";
		 return false;
	 }

	 uri = conversions::to_utf8string(valUri.as_string());
	 return true;
 }


 bool DownloadUrl(string url, string NfOut)
//d'apr�s https://github.com/Microsoft/cpprestsdk/wiki/Getting-Started-Tutorial
 {

	 // Open stream to output file.

	 utility::string_t Nf = utility::conversions::to_string_t(NfOut);
	 auto fileStream = std::make_shared<concurrency::streams::ostream>();

	 /*concurrency::task<concurrency::streams::ostream> streamOpenTask;
	 streamOpenTask = concurrency::streams::fstream::open_ostream(Nf, ios::out | ios::binary).then([=](ostream outFile)
	 {
		 *fileStream = outFile;

//	 *fileStream = streamOpenTask.get();
	 streamOpenTask.get();

	 if (fileStream == NULL) return false;

	 */

	 // Open stream to output file.
	 pplx::task<void> requestTask = concurrency::streams::fstream::open_ostream(Nf, ios::out | ios::binary)
	.then
	 ([=](concurrency::streams::ostream outFile)
	 {
		 *fileStream = outFile;

		 // Create http_client to send the request.
		 utility::string_t url_t = utility::conversions::to_string_t(url);
		 web::http::client::http_client clientimg(url_t);

		 http_request request(web::http::methods::GET);
		 return clientimg.request(request);
	 })		 
	.then([=](http_response response)
		 // Handle response headers arriving.
		{
			//printf("Received response status code:%u\n", response.status_code());

			// Write response body into the file.
			return response.body().read_to_end(fileStream->streambuf());
	 })

	 // Close the file stream.
	 .then([=](size_t)
	 {
		 return;
		 //return fileStream->close();
	 });

	 // Wait for all the outstanding I/O to complete and handle any exceptions
	 try
	 {
		 requestTask.wait();
	 }
	 catch (const std::exception &e)
	 {
		 //printf("Error exception:%s\n", e.what());
		 return false;
	 }

	 fileStream->close();
	 return true;
 }

//////////////////////////////////////////////////////////////////////////
 void AcquisitionSony(CallbackAcquisition callback)
	 //////////////////////////////////////////////////////////////////////////
 {
	 int TimeDebut = GetTickCount();

	 TParamSony Param;
	 ChargeParametres(Param);

	 ////////////////////////
	 //acquisition de l'image
	 ////////////////////////

	 string stcamera = "http://" + string(Param.AdresseIP) + ":" + string(Param.PortIP) + "/sony/camera";
	 utility::string_t adrcamera = conversions::to_string_t(stcamera);
	 http_client ClientCamera(adrcamera);

	 web::json::value result;
	 string msg;

	 //passage en RecMode (indispensable pour atteindre l'interface compatible avec la doc SDK)

	 if (!SendRequestPOST(ClientCamera, "startRecMode", "", "1.0", result, msg))
	 {
		 callback(false, "Echec passage en RecMode", GetTickCount() - TimeDebut);
		 return;
	 }

	 //passage en mode capture
	 if (!SendRequestPOST(ClientCamera, "setCameraFunction", "Remote Shooting", "1.0", result, msg))
	 {
		 callback(false, "Echec passage en mode capture", GetTickCount() - TimeDebut);
		 return;
	 }

	 //shooting

	 //attente status IDLE

	 while (true)
	 {
		 if (!SendRequestPOST(ClientCamera, "getEvent", "*false", "1.0", result, msg))
		 {
			 callback(false, "Echec POST getEvent", GetTickCount() - TimeDebut);
			 return;
		 }


		 _list<json::value> listeout;
		 GetFieldValue(result, "cameraStatus", listeout);
		 if (listeout.nbElem != 1)
		 {
			 callback(false, "Echec lecture cameraStatus", GetTickCount() - TimeDebut);
			 return;
		 }

		 json::value status = listeout.head->obj;
		 if (!status.is_string())
		 {
			 callback(false, "Echec lecture cameraStatus", GetTickCount() - TimeDebut);
			 return;
		 }

		 if (status.as_string() == string_t(L"IDLE")) break;	//OK

		 if (GetTickCount() - TimeDebut > 2000)
		 {
			 callback(false, "Timeout waiting status IDLE", GetTickCount() - TimeDebut);
			 return;
		 }
	 }



	 if (!SendRequestPOST(ClientCamera, "actTakePicture", "", "1.0", result, msg))
	 {
		 callback(false, "Echec POST acquisition", GetTickCount() - TimeDebut);
		 return;
	 }

	 //r�cup�ration du nom de l'image:

	 string fileUrl = "";

	 if (!AnalyseAcquisitionResult(result, fileUrl))
	 {
		 callback(false, "Echec acquisition", GetTickCount() - TimeDebut);
		 return;
	 }

	 //fileUrl ne pointe qu'une image r�duite. Pour t�l�charger l'image originale, il faut passer en mode
	 // "avContent". On r�cup�re avant le nom de l'image pour v�rif ult�rieure.
	 //exemple de fileUrl: "http://192.168.122.1:8080/postview/memory/DCIM/100MSDCF/DSC00027.JPG?size=Scn\"

	 string NomImage = GetNomImage(fileUrl);
	 callback(true, NomImage, GetTickCount() - TimeDebut);
 }


void DownloadSony(CallbackDownload callback, string NomImage)
{
	int TimeDebut = GetTickCount();

	TParamSony Param;
	ChargeParametres(Param);

	string stcamera = "http://" + string(Param.AdresseIP) + ":" + string(Param.PortIP) + "/sony/camera";
	utility::string_t adrcamera = conversions::to_string_t(stcamera);
	http_client ClientCamera(adrcamera);

	web::json::value result;
	string msg;

	//passage en RecMode (indispensable pour atteindre l'interface compatible avec la doc SDK)

	if (!SendRequestPOST(ClientCamera, "startRecMode", "", "1.0", result, msg))
	{
		callback(false, "Echec passage en RecMode", -1, GetTickCount() - TimeDebut);
		return;
	}

	//passage en mode transfert

	if (!SendRequestPOST(ClientCamera, "setCameraFunction", "Contents Transfer", "1.0", result, msg))
	{
		callback(false, "Echec passage en mode transfert", -1, GetTickCount() - TimeDebut);
		return;
	}

	//d�sormais, on envoie les POST sur un nouveau service

	string stContent = "http://" + string(Param.AdresseIP) + ":" + string(Param.PortIP) + "/sony/avContent";
	utility::string_t adrcontent = conversions::to_string_t(stContent);
	http_client ClientContent(adrcontent);

	
	//nb de photos en m�moire ?

	string params = "{\"uri\": \"storage:memoryCard1\", \"view\" : \"flat\", \"target\" : \"all\" }";

	if(!SendRequestPOST(ClientContent, "getContentCount", params, "1.2", result, msg))
	{
		callback(false, "Echec demande nb photos sur carte SD", -1, GetTickCount() - TimeDebut);
		return;
	}

	int NbImages = GetImageNumber(result, msg);
	if (NbImages<0)
	{
		callback(false, "Echec d�codage nb photos sur carte SD", -1, GetTickCount() - TimeDebut);
		return;
	}

	if (NbImages == 0)
	{
		callback(false, "Pas d'images sur la carte SD", 0, GetTickCount() - TimeDebut);
		return;
	}

	//retrouve l'url de l'image � charger
	
/*
	{"jsonrpc": "2.0", "method" : "getContentList", "params" : [{
		"uri": "storage:memoryCard1",
			"stIdx" : 0,
			"cnt" : 50,
			"view" : "flat",
			"sort" : ""
	}], "id": 1, "version" : "1.3"} */

	char stIndexStart[10]; sprintf(stIndexStart, "%d", NbImages - 1);	//derni�re image
	params = "{\"uri\": \"storage:memoryCard1\",";
	params += "\"stIdx\":" + string(stIndexStart) + ",";
	params += "\"cnt\": 1,";
	params += "\"view\": \"flat\"";
	params += "}";

	if(!SendRequestPOST(ClientContent, "getContentList", params, "1.3", result, msg))
	{
		callback(false, "Echec POST r�cup�ration url derni�re image sur carte SD", NbImages, GetTickCount() - TimeDebut);
		return;
	}

	string UrlImage, NomImageSD;

	if(!GetImageUrl(result, UrlImage, NomImageSD, msg))
	{
		callback(false, "Echec r�cup�ration url derni�re image sur carte SD", NbImages, GetTickCount() - TimeDebut);
		return;
	}

	if(NomImage.length()!=0)
		if(NomImageSD != NomImage)
		{
			callback(false, "L'url obtenue ne correspond pas au nom image attendu", NbImages, GetTickCount() - TimeDebut);
			return;
		}

	   
	//t�l�chargement de l'image
	string NfOut = string(Param.DossierImages) + "\\" + NomImage;

	if(!DownloadUrl(UrlImage, NfOut))
	{
		callback(false, "Echec t�l�chargement image acquise", NbImages, GetTickCount() - TimeDebut);
		return;
	}

	//effacement �ventuel sur la carte SD
	if (Param.EffaceImageSD)
	{
		string UriEffacement;
		if (!GetImageContentUri(result, UriEffacement, msg))
		{
			callback(false, "Echec r�cup�ration uri d'effacement", NbImages, GetTickCount() - TimeDebut);
			return;
		}

		/* exemple:
		"params": [	{
			"uri":
			[
				"image:content?contentId=XXXXXXXXXX",
					"video:content?contentId=XXXXXXXXXX"
			]
		}]  */

		params = "{\"uri\": ";
		params += "[" + UriEffacement + "]";
		params += "}";


		if (!SendRequestPOST(ClientContent, "deleteContent", params, "1.1", result, msg))
		{
			callback(false, "Echec POST effacement derni�re image sur carte SD", NbImages, GetTickCount() - TimeDebut);
			return;
		}
	}

	params = "{\"uri\": \"storage:memoryCard1\", \"view\" : \"flat\", \"target\" : \"all\" }";

	if (!SendRequestPOST(ClientContent, "getContentCount", params, "1.2", result, msg))
	{
		callback(false, "Echec demande nb photos sur carte SD", -1, GetTickCount() - TimeDebut);
		return;
	}

	NbImages = GetImageNumber(result, msg);
	if (NbImages < 0)
	{
		callback(false, "Echec d�codage nb photos sur carte SD", -1, GetTickCount() - TimeDebut);
		return;
	}

	//retour en mode capture
	if (!SendRequestPOST(ClientCamera, "setCameraFunction", "Remote Shooting", "1.0", result, msg))
	{
		callback(false, "Echec retour en mode capture", NbImages, GetTickCount() - TimeDebut);
		return;
	}

	////////////////////////////////////////////////////////////
	callback(true, NfOut, NbImages, GetTickCount() - TimeDebut);
	////////////////////////////////////////////////////////////

	
}

