#include "CommonSony.h" 


#include "ExportModuleSony.h" //fonctions export�es pour utilisation externe (autres modules)

TSonyStop SonyStopG;

/////////////////////////////////////////////////////////////////////////
void TModuleSony::Stop()
//////////////////////////////////////////////////////////////////////////
{
	if (SonyStopG.Cts == NULL) return; //non enclench� --> aucune action en cours

	SonyStopG.Cts->cancel();
	SonyStopG.Tce->set();

}
