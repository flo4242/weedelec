#ifndef __EXPORT_RIKOLA_H
#define __EXPORT_RIKOLA_H

#include <string>
#include "commtype.h"
#include "WeedelecCommon.h"
#include "Calage.h"
#include "list_tpl.h"
#include "ligne.h" //POINTFLT
#include "drvtraiti.h" 
#include "gereweeds.h" 





struct _EXPORT_  TParamAcquisitionSony
{
	TNomFic DossierImages;
	char AdresseIP[100];
	char PortIP[10];
	int TimeOutIP;
	char PassWordConnexion[10];
	bool EffaceImageSD;

	TParamAcquisitionSony()
	{
		DossierImages[0] = '\0';
		strcpy(AdresseIP, "192.168.122.1");
		strcpy(PortIP, "8080");
		strcpy(PassWordConnexion, "Zc4asBfA"); //6300
		//NB: pour le 6400: 4ymJzjCG

		TimeOutIP = 2000;
		EffaceImageSD = true;

	}
};

struct _EXPORT_  TParamDetectionSony
{
	TNomFic FichierCalage; //pour passage pixels--> espace robot
	TNomFic FichierLut;  //LUT calage
	TNomFic FichierLut_jaune; //LUT permettant de d�tecter les pastilles jaunes
	TNomFic FichierLut_bleu; //LUT permettant de d�tecter les pastilles bleues
	int distanceMaxBleuJaune;
	int SurfaceMin;
	int SurfaceMax;
	bool AnalyseLocaleCentre;
	int seuil_agreg;
	bool correctionBleu;
	TParamDetectionSony() 
	{
		FichierCalage[0] = '\0';
		FichierLut[0] = '\0';
		FichierLut_jaune[0] = '\0';
		FichierLut_bleu[0] = '\0';
	}
};




struct TRetourAcqSony : public TRetourTask
{
	//InfoRetour: url image r�duite si succ�s (OK ==true) , ou sinon description de l'erreur
	int DureeMs;
};

struct TRetourDownloadSony : public TRetourTask
{
	// InfoRetour: nom complet du fichier image si succ�s (state Completed) , ou sinon description de l'erreur
	int NbTotalImagesSD; //-1 si non d�termin� � ce stade
	int DureeMs;
};

struct TRetourDetectionSony : public TRetourTask
{
	// InfoRetour : message avec nb de weeds si succ�s (state Completed) , ou sinon description de l'erreur
	_list<TWeedRobot> ListeWeeds;
	_list<POINTFLT> ListeWeedsImage;
	int DureeMs;
};


struct _EXPORT_ TModuleSony
{
	//fonctions export�es pour utilisation externe (autres modules)

	static bool ChargeParametres(TParamAcquisitionSony &paramAcq, TParamDetectionSony &paramD, TParamCalage &paramCalage);

	static void TModuleSony::AcquisitionSony(TRetourAcqSony &ret);

	static void TModuleSony::DownloadSony(TRetourDownloadSony &ret, std::string UrlImageReduite = "");
	//t�l�chargement de la derni�re image acquise; effacement sur carte SD selon param�tres
	//si NomImage fourni, utilis� pour v�rification avant t�l�chargement et effacement �ventuel

	static void TModuleSony::DetectionPastillesSony(TRetourDetectionSony &ret, std::string UrlImageReduite, DRVTRAITIM *pD);
	//t�l�chargement de la derni�re image acquise format r�duit et d�tection pastilles selon Lut param�tr�e; 
	// appelle DoDetectionPastillesSony

	static bool TModuleSony::DoDetectionPastillesSony(IMCOULEUR* Image, _list<POINTFLT> &ListePastilles, TParamDetectionSony &ParamD, char *msgerr);
	//d�tection proprement dite


	static void TModuleSony::Stop(); //demande d'arr�t utilisateur

	static IMTCOULEUR * TModuleSony::VisualiseImage(DRVTRAITIM *pD, char *Nf, char * msg2000);

	static TCalage2D * TModuleSony::ChargeCalage(char * msgerr);

};

#endif


