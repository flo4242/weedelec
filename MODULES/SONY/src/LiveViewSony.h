#pragma once

typedef bool(*callBackLiveView)(int JPEGDataSize);
//JPEGDataSize = -2 si AvailableSize a �t� d�pass�e
//JPEGDataSize = -1 si erreur de lecture
//le callback doit renvoyer false si arr�t demand�


bool StartLiveView(unsigned char *JPEGData, int AvailableSize, bool formatlarge, callBackLiveView callback, char *MsgErr);
bool StopLiveView(char *MsgErr);
//MsgInfo contient le message d'erreur si retour avec false

