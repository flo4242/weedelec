#pragma once


//fonctions export�es pour utilisation externe (autres modules)

#include <string>
#include "commtype.h"
#include "WeedelecCommon.h"
#include "imgris.h"
#include "H33.h"
#include "Calage.h"
#include <iostream>
#include <fstream>
#include <sstream>
struct TParamRecalageVisuel
{
	char AdresseServeur[100];
	double homographieTol;
	TParamRecalageVisuel()
	{
		strcpy(AdresseServeur, "http://rear.local:5000");
	}

};

struct TRetourAcquisitionAvant : public TRetourTask
{
	int DureeMs;
};
struct TRetourAcquisitionArriere : public TRetourTask
{
	int DureeMs;
	_list<H33> listH; //homographie en retour
	time_t time_retour;
};



struct _EXPORT_ TModuleRecalageRasberry
{
	TCalageTrapeze * CalageAvant;
	TCalageTrapeze * CalageArriere;
	double EcartCameras; //approximatif, �tabli � l'init
	TModuleRecalageRasberry() { CalageAvant = CalageArriere = NULL; }
	~TModuleRecalageRasberry() { if (CalageAvant) delete CalageAvant; if (CalageArriere)	delete CalageArriere; }


	static bool ChargeParametres(TParamRecalageVisuel &param, TParamCalage &paramCalageAvant, TParamCalage &paramCalageArriere);


	bool Init(TParamRecalageVisuel &param, TParamCalage &paramCalageAvant, TParamCalage &paramCalageArriere, char *msgerr);

	////////////////////////////////////////////////////////////
	//fonctions incluant une requ�te vers le Rasberry
	////////////////////////////////////////////////////////////

	// msg100 est une cha�ne de 100 caract�res max indiquant renseign�e en cas de retour false (cause de l'erreur)
	//la gestion du nombre maximum d'images et de l'effacement des images devenues inutiles est faite en interne par le Rasberry

	bool AcqImageAvant(time_t time, char *msg100);
	static bool AcqImageAvantTache(time_t time, char *msg100, TRetourAcquisitionAvant T);
	//acqui�re une image avec la cam�ra avant et la stocke en interne avec le timestamp fourni
	//NB: peut int�grer un nb max d'images en m�moire
	bool GetHomographie(_list<H33> &listH, time_t t, char *msg100);
	static bool GetHomographieTache(time_t t, char *msg100, TRetourAcquisitionArriere ret);
	//acqui�re une image avec la cam�ra arri�re et recherche parmi les images cam�ra-avant stock�es, celle qui 
	//correspond au meilleur appariement. Si t est non nul, teste uniquement l'image de timestamp t.
	//retour avec 0 si probl�me acquisition cam�ra arri�re ou si pas d'appariement correct
	//sinon, retour avec le timestamp utilis� pour l'appariement

	//acquisition et transmission d'images pour la proc�dure de calage
	bool GetImageAvant(char *Nf, char *msg100);
	bool GetImageArriere(char *Nf, char *msg100);

	bool InitTableau();

	////////////////////////////////////////////////////////////
	//fonctions exclusivement au niveau du PC
	////////////////////////////////////////////////////////////

	H33 H0;		//homographie de transformation cam�ra arri�re --> cam�ra avant, obtenue par une proc�dure de calage

	bool GetHomographieDeplacement(H33 & HDeplacement, H33 *Hpixels, std::ostringstream &stringStream); //XXXXXXXXXXXXXXXXXXXXXXXXX

	//static TCalage2D * ChargeCalageAvant(char * msgerr);
	//static TCalage2D * ChargeCalageArriere(char * msgerr);
};




/*


struct TRetourDetectionPlantNet : public TRetourTask
{
	// InfoRetour: message avec nb de weeds si succ�s (state Completed) , ou sinon description de l'erreur
	_list<POINTFLT> ListeWeeds;
	int DureeMs;
};



struct _EXPORT_ TModulePlantNet
{
	
	static void LaunchAcquisition(TRetourTask &ret, bool avecGPS = false, double LatDeg=0, double LonDeg=0);
	static void AttenteDetection(TRetourDetectionPlantNet &ret);

};

*/



