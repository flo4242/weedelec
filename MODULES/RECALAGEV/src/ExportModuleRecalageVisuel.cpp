#pragma once

#include "RecalageRasberryDevice.h"
#include "ExportModuleRecalageVisuel.h"


#define DBOUT( s )            \
{                             \
   std::ostringstream os_;    \
   os_ << s <<"\n";                   \
   OutputDebugString( os_.str().c_str() );  \
}

TRecalageRasberryDevice RasberryDevice;


extern bool GereParametres(HWND hwnd, ModeGereParametres modeGereParam, TParamRecalageVisuel &param, TParamCalage &paramCalageAvant, TParamCalage &paramCalageArriere); //FRecalageVisuel.cpp

bool TModuleRecalageRasberry::ChargeParametres(TParamRecalageVisuel &param, TParamCalage &paramCalageAvant, TParamCalage &paramCalageArriere)
{
	return GereParametres(NULL, ModeGereParametres::Charge, param, paramCalageAvant, paramCalageArriere);
}


bool TModuleRecalageRasberry::Init(TParamRecalageVisuel &param, TParamCalage &paramCalageAvant, TParamCalage &paramCalageArriere, char *msgerr)
{
	if (!RasberryDevice.Connect(param.AdresseServeur, msgerr)) return false;

	if (CalageAvant)	delete CalageAvant;
	CalageAvant = new TCalageTrapeze;

	if (!CalageAvant->Charge(paramCalageAvant.FichierCalage))
	{
		delete CalageAvant; CalageAvant = NULL;
		sprintf(msgerr, "Echec chargement fichier \n %s", paramCalageAvant.FichierCalage);
		return false;
	}

	if (CalageArriere)	delete CalageArriere;
	CalageArriere = new TCalageTrapeze;

	if (!CalageArriere->Charge(paramCalageArriere.FichierCalage))
	{
		delete CalageArriere; CalageArriere = NULL;
		sprintf(msgerr, "Echec chargement fichier \n %s", paramCalageArriere.FichierCalage);
		return false;
	}

	H33 Hav; CalageAvant->GetH33directe(Hav);
	H33 HavInv; CalageAvant->GetH33inverse(HavInv);
	H33 Har; CalageArriere->GetH33directe(Har);
	H33 HarInv; CalageArriere->GetH33inverse(HarInv);
	//POINTFLT E(Hav.m13 - Har.m13, Hav.m23 - Har.m33);
	H33 HEcart = Har * HavInv;
	EcartCameras = POINTFLT(HEcart.m13, HEcart.m23).Norme();
	return true;
}




	////////////////////////////////////////////////////////////
	//fonctions incluant une requ�te vers le Rasberry
	////////////////////////////////////////////////////////////

	// msg100 est une cha�ne de 100 caract�res max indiquant renseign�e en cas de retour false (cause de l'erreur)
	//la gestion du nombre maximum d'images et de l'effacement des images devenues inutiles est faite en interne par le Rasberry

	bool TModuleRecalageRasberry::AcqImageAvant(time_t time, char *msg100) 
	{
		if (!RasberryDevice.IsConnected()) { sprintf(msg100, "Rasberry non connect�"); return false; }
		return RasberryDevice.AcqImageAvant(time, msg100);
	}

	bool TModuleRecalageRasberry::GetHomographie(_list<H33> &listH, time_t t, char *msg100)
		//acqui�re une image avec la cam�ra arri�re et recherche parmi les images cam�ra-avant stock�es, celle qui 
		//correspond au meilleur appariement. Si t est non nul, teste uniquement l'image de timestamp t.
	{
		if (!RasberryDevice.IsConnected()) { sprintf(msg100, "Rasberry non connect�"); return -1; }
		if (!RasberryDevice.GetHomographie(listH, t, msg100)) {
			return false;
		}
		return true;
	}
	bool TModuleRecalageRasberry::GetHomographieTache(time_t t, char *msg100, TRetourAcquisitionArriere ret) {
		_list<H33> listH;
		if (!RasberryDevice.IsConnected()) { sprintf(msg100, "Rasberry non connect�"); return -1; }
		if (!RasberryDevice.GetHomographie(listH, t, msg100)) {
			return false;
		}
		return true;
		ret.listH = listH;
	}


	bool TModuleRecalageRasberry::AcqImageAvantTache(time_t time, char *msg100, TRetourAcquisitionAvant Ret)
	{
		if (!RasberryDevice.IsConnected()) { sprintf(msg100, "Rasberry non connect�"); return false; }
		return RasberryDevice.AcqImageAvant(time, msg100);
	}

	//time_t TModuleRecalageRasberry::GetHomographieTache(time_t t, char *msg100, TRetourAcquisitionArriere Ret)
	//	//acqui�re une image avec la cam�ra arri�re et recherche parmi les images cam�ra-avant stock�es, celle qui 
	//	//correspond au meilleur appariement. Si t est non nul, teste uniquement l'image de timestamp t.
	//	//retour avec 0 si probl�me acquisition cam�ra arri�re ou si pas d'appariement correct
	//	//retour avec -1 si Rasberry non connect�
	//	//sinon, retour avec le timestamp utilis� pour l'appariement
	//{
	//	if (!RasberryDevice.IsConnected()) { sprintf(msg100, "Rasberry non connect�"); return -1; }

	//	_list<H33> listHtemp;
	//	if (!RasberryDevice.GetHomographie(listHtemp, t, msg100)) {
	//		return false;
	//	}
	//	Ret.listH = listHtemp; 
	//	return true;
	//}

	//acquisition et transmission d'images pour la proc�dure de calage
	bool TModuleRecalageRasberry::GetImageAvant(char *Nf, char *msg100)
	{
		if (!RasberryDevice.IsConnected()) { sprintf(msg100, "Rasberry non connect�"); return NULL; }
		return RasberryDevice.GetImageAvant(Nf, msg100);
	}
	
	bool TModuleRecalageRasberry::GetImageArriere(char *Nf, char *msg100)
	{
		if (!RasberryDevice.IsConnected()) { sprintf(msg100, "Rasberry non connect�"); return NULL; }
		return RasberryDevice.GetImageArriere(Nf, msg100);
	}

	//Nettoie la liste de timestamps g�r�e en interne par le raspberry
	bool TModuleRecalageRasberry::InitTableau() {

		return RasberryDevice.InitTableau();
	}

	////////////////////////////////////////////////////////////
	//fonctions exclusivement au niveau du PC
	////////////////////////////////////////////////////////////


	bool TModuleRecalageRasberry::GetHomographieDeplacement(H33 & HDeplacement, H33 *Hpixels, ostringstream &stringStream)
	{
		//retourne l'homographie permettant de passer de la position robot d'un objet avant d�placement � celle apr�s d�placement

		/*principe th�orique:

		position Pav (position dans robot avant d�placement)
		PcamAvPix = inv(calageAvant) *Pav;
		PcamArPix = Hpixels * PcamAvPix; //homographie avant -> arri�re en pixels
		position Par =  calageArriere * PcamArPix (position dans robot apr�s d�placement)

		Soit au total: HDeplacement = calageArriere * Hpixels * inv(calageAvant);
		*/

		if ((CalageAvant == NULL) || (CalageArriere == NULL))
		{
			stringStream << "Calage(s) cam�ra(s) avant et/ou arri�re non d�fini(s)";
			return false;
		}
		H33 Hav; CalageAvant->GetH33directe(Hav);
		H33 HavInv; CalageAvant->GetH33inverse(HavInv);
		H33 Har; CalageArriere->GetH33directe(Har);
		HDeplacement = Har * (*Hpixels)*HavInv; //CALCUL DE LA MATRICE DE DEPLACEMENT PAR PRODUIT MATRICIEL (attention d�r�f�rencement pointeur Hpixels)
		//stringStream << "Har:\n";
		//stringStream << "m1: " << Har.m11 << " | " << Har.m12 << " | " << Har.m13 << "\n";
		//stringStream << "m1: " << Har.m21 << " | " << Har.m22 << " | " << Har.m23 << "\n";
		//stringStream << "m1: " << Har.m31 << " | " << Har.m32 << " | " << Har.m33 << "\n";
		//stringStream << "Hpixels:\n";
		//stringStream << "m1: " << Hpixels->m11 << " | " << Hpixels->m12 << " | " << Hpixels->m13 << "\n";
		//stringStream << "m1: " << Hpixels->m21 << " | " << Hpixels->m22 << " | " << Hpixels->m23 << "\n";
		//stringStream << "m1: " << Hpixels->m31 << " | " << Hpixels->m32 << " | " << Hpixels->m33 << "\n";
		//stringStream << "Hav:\n";
		//stringStream << "m1: " << Hav.m11 << " | " << Hav.m12 << " | " << Hav.m13 << "\n";
		//stringStream << "m1: " << Hav.m21 << " | " << Hav.m22 << " | " << Hav.m23 << "\n";
		//stringStream << "m1: " << Hav.m31 << " | " << Hav.m32 << " | " << Hav.m33 << "\n";
		//stringStream << "HavInv:\n";
		//stringStream << "m1: " << HavInv.m11 << " | " << HavInv.m12 << " | " << HavInv.m13 << "\n";
		//stringStream << "m1: " << HavInv.m21 << " | " << HavInv.m22 << " | " << HavInv.m23 << "\n";
		//stringStream << "m1: " << HavInv.m31 << " | " << HavInv.m32 << " | " << HavInv.m33 << "\n";
		//stringStream << "HDeplacement:\n";
		stringStream <<  HDeplacement.m11 << " | " << HDeplacement.m12 << " | " << HDeplacement.m13 << "\n";
		stringStream << HDeplacement.m21 << " | " << HDeplacement.m22 << " | " << HDeplacement.m23 << "\n";
		stringStream << HDeplacement.m31 << " | " << HDeplacement.m32 << " | " << HDeplacement.m33 << "\n";

		return true;
	}


/*


struct TRetourDetectionPlantNet : public TRetourTask
{
	// InfoRetour: message avec nb de weeds si succ�s (state Completed) , ou sinon description de l'erreur
	_list<POINTFLT> ListeWeeds;
	int DureeMs;
};



struct _EXPORT_ TModulePlantNet
{
	
	static void LaunchAcquisition(TRetourTask &ret, bool avecGPS = false, double LatDeg=0, double LonDeg=0);
	static void AttenteDetection(TRetourDetectionPlantNet &ret);

};

*/



