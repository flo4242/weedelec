#pragma once

#include "RecalageRasberryDevice.h"
#include <cpprest/filestream.h>
#include <curl/curl.h> //pour le t�l�chargement en ftp
#include <cstring>
#include <stdio.h>

using namespace concurrency::streams;       // Asynchronous streams

#define DBOUT( s )            \
	{                             \
	   std::ostringstream os_;    \
	   os_ << s;                   \
	   OutputDebugString( os_.str().c_str() );  \
	}

bool TRecalageRasberryDevice::SetClient(std::string uri, char* msgerr)
{

	if (uri.length() == 0)
	{
		sprintf(msgerr, "Recalage Rasberry: uri non d�finie");
		return false;
	}


	utility::string_t uri_stringt = utility::conversions::to_string_t(uri);

	if (pClient != NULL)
	{
		delete pClient; pClient = NULL;
	}

	try
	{
		pClient = new http_client(uri_stringt);
	}

	catch (std::exception e)
	{
		sprintf(msgerr, "Recalage Rasberry: �chec cr�ation client http\n (uri: %s)", uri.data());
		return false;
	}

	return true;
}


bool TRecalageRasberryDevice::GetKeyValueJson(web::json::value & data_in, std::string key, web::json::value& keyvalue, char *msgerr)
{
	utility::string_t Key = utility::conversions::to_string_t(key);
	if (data_in.has_field(Key))
	{
		keyvalue = data_in.at(Key);
	}
	else
	{
		sprintf(msgerr, "Echec: cl� json %s incorrecte", key.data());
		return false;
	}
	return true;
}


bool TRecalageRasberryDevice::Connect(std::string uri, char* msgerr)
{
	Connecte = SetClient(uri, msgerr);
	return Connecte;
}

void TRecalageRasberryDevice::Disconnect()
{
	if (IsConnected())
	{
		delete pClient; pClient = NULL;
		Connecte = false;
	}
}


//Fonction pour requ�tes GET envoy�es au raspberry
bool TRecalageRasberryDevice::RequestClient(string fragment, web::json::value & reponse, char * msgerr)
{
	http_response response;


	try
	{
		DBOUT("fragment" << fragment << "\n");
		utility::string_t fragment_t = utility::conversions::to_string_t(fragment);

		// ordinary `get` request
		
		response = pClient->request(methods::GET, fragment_t).get();
		//response = pClient->request(methods::GET).get();
		reponse = response.extract_json().get();
	}

	catch (std::exception e)
	{
		sprintf(msgerr, "Rasberry device request: %s:\n Echec r�ception de la r�ponse", fragment.data());
		return false;
	}

	return true;
}


//fonctions compagnon de downloadURL
struct FtpFile {
	const char *filename;
	FILE *stream;
};
//ouverture d'un stream vers un fichier et �criture du contenu du fichier t�l�charg�
static size_t my_fwrite(void *buffer, size_t size, size_t nmemb, void *stream)
{
	struct FtpFile *out = (struct FtpFile *)stream;
	if (!out->stream) {
		out->stream = fopen(out->filename, "wb");
		if (!out->stream)
			return -1; 
	}
	return fwrite(buffer, size, nmemb, out->stream);
}

bool DownloadUrlFTP(string url, string NfOut, string &msg)
//Utilisation de la biblioth�que cURL pour t�l�charger l'image en ftp
//voir libcurl_VS2017_x64_build dans le dossier doc du module pour installation en x64 VS 2017
{
	//initialisation cURL
	CURL *curl;
	CURLcode res; //code r�sultat requete

	struct FtpFile ftpfile = {
	  NfOut.c_str(), /* name to store the file as if successful */
	  NULL
	};

	curl_global_init(CURL_GLOBAL_DEFAULT);
	curl = curl_easy_init();
	DBOUT("D�but cURL ftp \n");
	DBOUT("Dossier de destination" << ftpfile.filename << "\n");
	DBOUT("url" << url << "\n");
	if (curl) {
		//Dump pour debug
		//FILE *filep = fopen("D:/raspberryim/dump", "wb");
		DBOUT("Objet CURL bien instanci�");
		curl_easy_setopt(curl, CURLOPT_URL,	url.c_str());
		//callback pour l'ouverture du fichier et son �criture
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, my_fwrite);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ftpfile);
		//verbose pour debug
		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
		//curl_easy_setopt(curl, CURLOPT_STDERR, filep);
		res = curl_easy_perform(curl);
		//Nettoyage de l'instance
		curl_easy_cleanup(curl);
		if (CURLE_OK != res) {
			msg = "erreur CURL\n";
			return false;
		}
	}
	if (ftpfile.stream)
		fclose(ftpfile.stream); //fermeture du stream vers le fichier
	curl_global_cleanup();
	return true;
}


//version HTTP
bool DownloadUrl(string url, string NfOut, string &msg)
//d'apr�s https://github.com/Microsoft/cpprestsdk/wiki/Getting-Started-Tutorial

//la cascade des .then() a �t� d�construite pour un meilleur suivi debug
{

	// Open stream to output file.

	utility::string_t Nf = utility::conversions::to_string_t(NfOut);
	//auto fileStream = std::make_shared<concurrency::streams::ostream>();

	//ouverture stream
	concurrency::task<concurrency::streams::ostream> streamOpenTask;
	streamOpenTask = concurrency::streams::fstream::open_ostream(Nf, ios::out | ios::binary);

	concurrency::streams::ostream  os;

	try
	{
		os = streamOpenTask.get();
	}

	catch (exception e)
	{
		msg = "Echec ouverture fichier";
		return false;
	}


	//lance la requ�te de t�l�chargement
	utility::string_t url_t = utility::conversions::to_string_t(url);
	web::http::client::http_client clientimg(url_t);
	http_request request(web::http::methods::GET);
	http_response response;

	pplx::task<http_response> requestTask = clientimg.request(request);

	try
	{
		response = requestTask.get();
	}

	catch (exception)
	{
		msg = "Echec lancement requ�te de t�l�chargement";
		return false;
	}


	//printf("Received response status code:%u\n", response.status_code());
	 // Write response body into the file.

	pplx::task<size_t> readTask = response.body().read_to_end(os.streambuf());

	try
	{
		size_t sizeread = readTask.get();
	}

	catch (exception)
	{
		msg = "Echec lecture corps de la r�ponse de requ�te";
		return false;
	}

	os.close().wait();

	return true;
}



	////////////////////////////////////////////////////////////
	//fonctions incluant une requ�te 
	////////////////////////////////////////////////////////////

	// msg100 est une cha�ne de 100 caract�res max indiquant renseign�e en cas de retour false (cause de l'erreur)
	//la gestion du nombre maximum d'images et de l'effacement des images devenues inutiles est faite en interne par le Rasberry
	

	//template de conversion en string pouvant �tre appliqu� aux time_t
	template<typename T>
	std::string toString(const T &t) {
		std::ostringstream oss;
		oss << t;
		return oss.str();
	}
	bool TRecalageRasberryDevice::AcqImageAvant(time_t time, char *msg100) { 
		//acqui�re une image avec la cam�ra avant et la stocke en interne avec le timestamp fourni
		//NB: peut int�grer un nb max d'images en m�moire
		//envoi de la requ�te 
		web::json::value Reponse;
		if (!RequestClient("/front/"+ toString(time), Reponse, msg100)) return false;
		//On peut alors t�l�charger l'image directement en ftp
		//string Nf = "D:/raspberryim/front_image" + toString(time) +".jpg";
		//std::string msg;


		////t�l�chargement de l'URL via FTP, � utiliser que si on souhaite caler les cam�ras pour r�cup�rer des images
		//if (!DownloadUrlFTP("ftp://rear.local/front_image"+ toString(time) +".jpg", Nf, msg))
		//{
		//	sprintf(msg100, msg.data());
		//	return false;
		//}


		DBOUT("Acquisition front OK");
		return true; 
	}





	time_t TRecalageRasberryDevice::GetHomographie(H33 &H, time_t t, char *msg100) { 
		//acqui�re une image avec la cam�ra arri�re et recherche parmi les images cam�ra-avant stock�es, celle qui 
		//correspond au meilleur appariement. Si t est non nul, teste uniquement l'image de timestamp t.
		//retour avec 0 si probl�me acquisition cam�ra arri�re ou si pas d'appariement correct
		//sinon, retour avec le timestamp utilis� pour l'appariement
		time_t t_front;
		time_t t_rear;
		web::json::value Reponse;
		if (!RequestClient("/rear/"+ toString(t), Reponse, msg100)) return false;

		//string Nf = "D:/raspberryim/rear_image" + toString(time) + ".jpg";
		//std::string msg;


		////t�l�chargement de l'URL via FTP, � utiliser que si on souhaite caler les cam�ras pour r�cup�rer des images
		//if (!DownloadUrlFTP("ftp://rear.local/rear_image" + toString(time) + ".jpg", Nf, msg))
		//{
		//	sprintf(msg100, msg.data());
		//	return false;
		//}

		//R�cup�rer le json contenant:
		//- les 9 valeurs de la matrice d'homgraphie
		//-les deux timestamps pour rear et front
		// le timestamp retour correspond � celui de l'image front
		web::json::value keyvalue;
		if (!GetKeyValueJson(Reponse, "h11", keyvalue, msg100)) { //test d'une des valeurs de la matrice
			//cas o� il n'y a pas d'homographie (r�ponse requete contient NULL)
			sprintf(msg100, "Pas d'homographie trouv�e"); 
			t_front = 0;
			return t_front;

		}
		else { //on peut extraire la matrice d'homographie
			DBOUT("Extraction de la matrice et des timestamps");
			GetKeyValueJson(Reponse, "h11", keyvalue, msg100);  H.m11 = keyvalue.as_double();
			GetKeyValueJson(Reponse, "h12", keyvalue, msg100); H.m12 = keyvalue.as_double();
			GetKeyValueJson(Reponse, "h13", keyvalue, msg100); H.m13 = keyvalue.as_double();
			GetKeyValueJson(Reponse, "h21", keyvalue, msg100); H.m21 = keyvalue.as_double();
			GetKeyValueJson(Reponse, "h22", keyvalue, msg100); H.m22 = keyvalue.as_double();
			GetKeyValueJson(Reponse, "h23", keyvalue, msg100); H.m23 = keyvalue.as_double();
			GetKeyValueJson(Reponse, "h31", keyvalue, msg100); H.m31 = keyvalue.as_double();
			GetKeyValueJson(Reponse, "h32", keyvalue, msg100); H.m32 = keyvalue.as_double();
			GetKeyValueJson(Reponse, "h33", keyvalue, msg100); H.m33 = keyvalue.as_double();
			GetKeyValueJson(Reponse, "timestamp_front", keyvalue, msg100); t_front = keyvalue.as_integer();
			GetKeyValueJson(Reponse, "timestamp_rear", keyvalue, msg100); t_rear = keyvalue.as_integer();
			DBOUT("m1: " << H.m11 << " - " << H.m12 << " - " << H.m13  << "\n");
			DBOUT("m2: " << H.m21 << " - " << H.m22 << " - " << H.m23 << "\n");
			DBOUT("m2: " << H.m31 << " - " << H.m32 << " - " << H.m33 << "\n");
			DBOUT("t_front: " << t_front << "\n");
			DBOUT("t_rear: " << t_rear << "\n");
		}
		DBOUT("Acquisition rear + homographie OK");
		return t_front; //retour du timestamp front correspondant
	}


	//acquisition et transmission d'images pour la proc�dure de calage
	bool TRecalageRasberryDevice::GetImageAvant(char *Nf, char *msg100)
	{
		DBOUT("entr�e get image avant \n");
		//envoi de la requ�te 
		web::json::value Reponse;
		if (!RequestClient("/front_photo/1", Reponse, msg100)) return false;


		web::json::value keyvalue;
		if (!GetKeyValueJson(Reponse, "retour", keyvalue, msg100)) return false;


		if (!keyvalue.is_string())
		{
			sprintf(msg100, "Retour de requ�te non conforme"); return false;
		}

		std::string st = utility::conversions::to_utf8string(keyvalue.as_string());
		DBOUT("string recupere:" << st);

		string msg;
		if (!DownloadUrlFTP(st, Nf, msg))
		{
			sprintf(msg100, msg.data());
			return false;
		}
		DBOUT("image recup avec protocole ftp");


		return true;
	}

	   	  

	bool TRecalageRasberryDevice::GetImageArriere(char *Nf, char *msg100)
	{
		//envoi de la requ�te 
		web::json::value Reponse;
		if (!RequestClient("/rear_photo/1", Reponse, msg100)) return false;


		web::json::value keyvalue;
		if (!GetKeyValueJson(Reponse, "retour", keyvalue, msg100)) return false;


		if (!keyvalue.is_string())
		{
			sprintf(msg100, "Retour de requ�te non conforme"); return false;
		}

		std::string st = utility::conversions::to_utf8string(keyvalue.as_string());

		string msg;
		if (!DownloadUrlFTP(st, Nf, msg))
		{
			sprintf(msg100, msg.data());
			return false;
		}
		return true;
	}





