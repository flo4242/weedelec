#pragma once

#include <cpprest/http_client.h>

#include <string>
#include "commtype.h"
#include "WeedelecCommon.h"
#include "imgris.h"
#include "H33.h"

using namespace web::http;                  // Common HTTP functionality
using namespace web::http::client;          // HTTP client features


class TRecalageRasberryDevice
{

private:

	http_client *pClient;
	bool Connecte;
	bool SetClient(std::string uri, char* msgerr);
	bool RequestClient(string fragment, web::json::value & reponse, char * msgerr);

	bool GetKeyValueJson(web::json::value & data_in, std::string key, web::json::value& keyvalue, char *msgerr);



public:
	TRecalageRasberryDevice() { pClient = NULL; Connecte = false; }

	~TRecalageRasberryDevice() { if (pClient)	delete pClient; }

	bool Connect(std::string uri, char* msgerr);
	//reconnexion possible sur une autre uri (pour test) m�me si d�j� connect�
	// dans le cas, pClient pr�c�dent d�truit)

	void Disconnect();

	bool IsConnected() { return Connecte; }

	   
	////////////////////////////////////////////////////////////
	//fonctions de requ�te
	////////////////////////////////////////////////////////////

	// msg100 est une cha�ne de 100 caract�res max indiquant renseign�e en cas de retour false (cause de l'erreur)
	//la gestion du nombre maximum d'images et de l'effacement des images devenues inutiles est faite en interne par le Rasberry

	bool AcqImageAvant(time_t time, char *msg100);
	//acqui�re une image avec la cam�ra avant et la stocke en interne avec le timestamp fourni
	//NB: peut int�grer un nb max d'images en m�moire

	time_t GetHomographie(H33 &H, time_t t, char *msg100);
	//acqui�re une image avec la cam�ra arri�re et recherche parmi les images cam�ra-avant stock�es, celle qui 
	//correspond au meilleur appariement. Si t est non nul, teste uniquement l'image de timestamp t.
	//retour avec 0 si probl�me acquisition cam�ra arri�re ou si pas d'appariement correct
	//sinon, retour avec le timestamp utilis� pour l'appariement

	//acquisition et transmission d'images pour la proc�dure de calage --> l'image est stock�e dans le fichier Nf
	bool GetImageAvant(char *Nf, char *msg100);
	bool GetImageArriere(char *Nf, char *msg100);

};



