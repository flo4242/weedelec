#pragma once

#include "RecalageRasberryDevice.h"
#include "ExportModuleRecalageVisuel.h"
TRecalageRasberryDevice RasberryDevice;


extern bool GereParametres(HWND hwnd, ModeGereParametres modeGereParam, TParamRecalageVisuel &param); //FRecalageVisuel.cpp

bool TModuleRecalageRasberry::ChargeParametres(TParamRecalageVisuel &param)
{
	return GereParametres(NULL, ModeGereParametres::Charge, param);
}


bool TModuleRecalageRasberry::Init(TParamRecalageVisuel &param, char *msgerr)
{
	if (!RasberryDevice.Connect(param.AdresseServeur, msgerr)) return false;

	if (CalageAvant)	delete CalageAvant;
	CalageAvant = new TCalageTrapeze;

	if (!CalageAvant->Charge(param.NfCalageAvant))
	{
		delete CalageAvant; CalageAvant = NULL;
		sprintf(msgerr, "Echec chargement fichier \n %s", param.NfCalageAvant);
		return false;
	}

	if (CalageArriere)	delete CalageArriere;
	CalageArriere = new TCalageTrapeze;

	if (!CalageArriere->Charge(param.NfCalageArriere))
	{
		delete CalageArriere; CalageArriere = NULL;
		sprintf(msgerr, "Echec chargement fichier \n %s", param.NfCalageArriere);
		return false;
	}

	H33 Hav; CalageAvant->GetH33directe(Hav);
	H33 HavInv; CalageAvant->GetH33inverse(HavInv);
	H33 Har; CalageArriere->GetH33directe(Har);
	H33 HarInv; CalageArriere->GetH33inverse(HarInv);
	//POINTFLT E(Hav.m13 - Har.m13, Hav.m23 - Har.m33);
	H33 HEcart = Har * HavInv;
	EcartCameras = POINTFLT(HEcart.m13, HEcart.m23).Norme();
	return true;
}




	////////////////////////////////////////////////////////////
	//fonctions incluant une requ�te vers le Rasberry
	////////////////////////////////////////////////////////////

	// msg100 est une cha�ne de 100 caract�res max indiquant renseign�e en cas de retour false (cause de l'erreur)
	//la gestion du nombre maximum d'images et de l'effacement des images devenues inutiles est faite en interne par le Rasberry

	bool TModuleRecalageRasberry::AcqImageAvant(time_t time, char *msg100) 
	{
		if (!RasberryDevice.IsConnected()) { sprintf(msg100, "Rasberry non connect�"); return false; }
		return RasberryDevice.AcqImageAvant(time, msg100);
	}

	time_t TModuleRecalageRasberry::GetHomographie(H33 &H, time_t t, char *msg100)
		//acqui�re une image avec la cam�ra arri�re et recherche parmi les images cam�ra-avant stock�es, celle qui 
		//correspond au meilleur appariement. Si t est non nul, teste uniquement l'image de timestamp t.
		//retour avec 0 si probl�me acquisition cam�ra arri�re ou si pas d'appariement correct
		//retour avec -1 si Rasberry non connect�
		//sinon, retour avec le timestamp utilis� pour l'appariement
	{
		if (!RasberryDevice.IsConnected()) { sprintf(msg100, "Rasberry non connect�"); return -1; }
		return RasberryDevice.GetHomographie(H, t, msg100);
	}

	//acquisition et transmission d'images pour la proc�dure de calage
	bool TModuleRecalageRasberry::GetImageAvant(char *Nf, char *msg100)
	{
		if (!RasberryDevice.IsConnected()) { sprintf(msg100, "Rasberry non connect�"); return NULL; }
		return RasberryDevice.GetImageAvant(Nf, msg100);
	}
	
	bool TModuleRecalageRasberry::GetImageArriere(char *Nf, char *msg100)
	{
		if (!RasberryDevice.IsConnected()) { sprintf(msg100, "Rasberry non connect�"); return NULL; }
		return RasberryDevice.GetImageArriere(Nf, msg100);
	}



	////////////////////////////////////////////////////////////
	//fonctions exclusivement au niveau du PC
	////////////////////////////////////////////////////////////


	bool TModuleRecalageRasberry::GetHomographieDeplacement(H33 & HDeplacement, H33 &Hpixels, char * msgerr)
	{
		//retourne l'homographie permettant de passer de la position robot d'un objet avant d�placement � celle apr�s d�placement

		/*principe th�orique:

		position Pav (position dans robot avant d�placement)
		PcamAvPix = inv(calageAvant) *Pav;
		PcamArPix = Hpixels * PcamAvPix; //homographie avant -> arri�re en pixels
		position Par =  calageArriere * PcamArPix (position dans robot apr�s d�placement)

		Soit au total: HDeplacement = calageArriere * Hpixels * inv(calageAvant);
		*/

		if ((CalageAvant == NULL) || (CalageArriere == NULL))
		{
			sprintf(msgerr, "Calage(s) cam�ra(s) avant et/ou arri�re non d�fini(s)");
			return false;
		}
		H33 HavInv; CalageAvant->GetH33inverse(HavInv);
		H33 Har; CalageArriere->GetH33directe(Har);



		HDeplacement = Har * Hpixels*HavInv;

		return true;
	}


/*


struct TRetourDetectionPlantNet : public TRetourTask
{
	// InfoRetour: message avec nb de weeds si succ�s (state Completed) , ou sinon description de l'erreur
	_list<POINTFLT> ListeWeeds;
	int DureeMs;
};



struct _EXPORT_ TModulePlantNet
{
	
	static void LaunchAcquisition(TRetourTask &ret, bool avecGPS = false, double LatDeg=0, double LonDeg=0);
	static void AttenteDetection(TRetourDetectionPlantNet &ret);

};

*/



