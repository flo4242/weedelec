#pragma once


//fonctions export�es pour utilisation externe (autres modules)

#include <string>
#include "commtype.h"
#include "WeedelecCommon.h"
#include "imgris.h"
#include "H33.h"
#include "Calage.h"

struct TParamRecalageVisuel
{
	char AdresseServeur[100];
	TNomFic NfCalageAvant;
	TNomFic NfCalageArriere;
	TNomFic LUTCalage;
	TParamRecalageVisuel()
	{
		strcpy(AdresseServeur, "http://rear.local:5000");
		strcpy(NfCalageAvant, "");
		strcpy(NfCalageArriere, "");
		strcpy(LUTCalage, "");
	}

};

struct _EXPORT_ TModuleRecalageRasberry
{
	TCalageTrapeze * CalageAvant;
	TCalageTrapeze * CalageArriere;
	double EcartCameras; //approximatif, �tabli � l'init
	TModuleRecalageRasberry() { CalageAvant = CalageArriere = NULL; }
	~TModuleRecalageRasberry() { if (CalageAvant) delete CalageAvant; if (CalageArriere)	delete CalageArriere; }


	static bool ChargeParametres(TParamRecalageVisuel &param);


	bool Init(TParamRecalageVisuel &param, char *msgerr);

	////////////////////////////////////////////////////////////
	//fonctions incluant une requ�te vers le Rasberry
	////////////////////////////////////////////////////////////

	// msg100 est une cha�ne de 100 caract�res max indiquant renseign�e en cas de retour false (cause de l'erreur)
	//la gestion du nombre maximum d'images et de l'effacement des images devenues inutiles est faite en interne par le Rasberry

	bool AcqImageAvant(time_t time, char *msg100);
	//acqui�re une image avec la cam�ra avant et la stocke en interne avec le timestamp fourni
	//NB: peut int�grer un nb max d'images en m�moire

	time_t GetHomographie(H33 &H, time_t t, char *msg100);
	//acqui�re une image avec la cam�ra arri�re et recherche parmi les images cam�ra-avant stock�es, celle qui 
	//correspond au meilleur appariement. Si t est non nul, teste uniquement l'image de timestamp t.
	//retour avec 0 si probl�me acquisition cam�ra arri�re ou si pas d'appariement correct
	//sinon, retour avec le timestamp utilis� pour l'appariement

	//acquisition et transmission d'images pour la proc�dure de calage
	bool GetImageAvant(char *Nf, char *msg100);
	bool GetImageArriere(char *Nf, char *msg100);



	////////////////////////////////////////////////////////////
	//fonctions exclusivement au niveau du PC
	////////////////////////////////////////////////////////////

	H33 H0;		//homographie de transformation cam�ra arri�re --> cam�ra avant, obtenue par une proc�dure de calage

	bool GetHomographieDeplacement(H33 & HDeplacement, H33 &Hpixels, char * msgerr); //XXXXXXXXXXXXXXXXXXXXXXXXX

	//static TCalage2D * ChargeCalageAvant(char * msgerr);
	//static TCalage2D * ChargeCalageArriere(char * msgerr);
};




/*


struct TRetourDetectionPlantNet : public TRetourTask
{
	// InfoRetour: message avec nb de weeds si succ�s (state Completed) , ou sinon description de l'erreur
	_list<POINTFLT> ListeWeeds;
	int DureeMs;
};



struct _EXPORT_ TModulePlantNet
{
	
	static void LaunchAcquisition(TRetourTask &ret, bool avecGPS = false, double LatDeg=0, double LonDeg=0);
	static void AttenteDetection(TRetourDetectionPlantNet &ret);

};

*/



