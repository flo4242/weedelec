#include "CalageRaspberry.h"
#include "Calage.h"
#include "binaire.h"
#include "EnsembleRegions.h"
#include "EllipseFitting.h"

//POUR THIN PLATE SPLINE

#include <vector>
#include <highgui.h>
#define INTER_CUBIC	2 //d'apr�s types_c.h (pas r�ussi � l'inclure simplement)
#include "CThinPlateSpline.h"  //OpenCv



#include "matrice.h"
#include "utilfic.h"
#include "winutil2.h" //GROUPE

#include "LutRVB.h"


struct PositionCiblesRaspAvant
{
#define NBCIBLES 15

	POINTFLT Tab[NBCIBLES];
	int NbPoints;

	PositionCiblesRaspAvant() //positions rep�r�es sur la plaque mire (avril 2019)
	{
		NbPoints = NBCIBLES;
		Tab[0] = POINTFLT(-48.2, 41); //L7
		Tab[1] = POINTFLT(-34.4, 35.7); //L9
		Tab[2] = POINTFLT(-16.3, 36.9); //L10
		Tab[3] = POINTFLT(15.5, 48.6); //9
		Tab[4] = POINTFLT(17.3, 34.8); //12
		Tab[5] = POINTFLT(-64.4, 33.2); //L8
		Tab[6] = POINTFLT(9.7, 23.8); //16
		Tab[7] = POINTFLT(-59.2, 22.2); //L11
		Tab[8] = POINTFLT(-40, 19.4); //L12
		Tab[9] = POINTFLT(-10.9, 15.5); //L13
		Tab[10] = POINTFLT(-66.8, 6.2); //L14
		Tab[11] = POINTFLT(-26.3, 7.2); //L15
		for (int i = 0; i < NbPoints; i++) {
			Tab[i].y -= 40; //ajout de la translation de 40 cm
			Tab[i] /= 100; //passage en m�tres
		}
	}


} TabCiblesAvant;
struct PositionCiblesRaspArriere
{
#define NBCIBLES 15

	POINTFLT Tab[NBCIBLES];
	int NbPoints;

	PositionCiblesRaspArriere() //positions rep�r�es sur la plaque mire (avril 2019)
	{
		NbPoints = NBCIBLES;
		Tab[0] = POINTFLT(-48.2, 41); //L7
		Tab[1] = POINTFLT(-34.4, 35.7); //L9
		Tab[2] = POINTFLT(-16.3, 36.9); //L10
		Tab[3] = POINTFLT(15.5, 48.6); //9
		Tab[4] = POINTFLT(-68, 47.4); //L6
		Tab[5] = POINTFLT(-64.4, 33.2); //L8
		Tab[6] = POINTFLT(9.7, 23.8); //16
		Tab[7] = POINTFLT(-59.2, 22.2); //L11
		Tab[8] = POINTFLT(-40, 19.4); //L12
		Tab[9] = POINTFLT(-10.9, 15.5); //L13
		Tab[10] = POINTFLT(-66.8, 6.2); //L14
		Tab[11] = POINTFLT(-26.3, 7.2); //L15
		for (int i = 0; i < NbPoints; i++) {
			Tab[i].y -= 170; //ajout de la translation de 170 cm
			Tab[i] /= 100; //passage en m�tres
		}
	}


} TabCiblesArriere;

POINTFLT * TabPositionsMire() { return TabCiblesAvant.Tab; }
int NbPositionsMire() { return TabCiblesAvant.NbPoints; }


//ATTENTION: duplication de code
bool DoOrdonneCiblesRaspberry(POINTFLT * TabIn, POINTFLT* TabOut, int nbPoints, char *msgerr, bool avant)
{
	//Liste de points diff�rente selon que l'on cale la cam�ra avant ou arri�re
	if (avant) {
		PositionCiblesRaspAvant TabCiblesG;
		TabCiblesG = TabCiblesAvant;
		if (nbPoints != TabCiblesG.NbPoints)
		{
			sprintf(msgerr, "Le nombre de points n'est pas celui attendu (%d au lieu de %d)", nbPoints, TabCiblesG.NbPoints);
			return false;
		}

		RRECTF RPixels = CadreCibles(TabIn, nbPoints, false);
		RRECTF RMetres = CadreCibles(TabCiblesG.Tab, TabCiblesG.NbPoints, true);
		//calage sommaire
		CalageLin C(RPixels, RMetres, true);
		//r�ordonnancement des cibles d�tect�es
		for (int i = 0; i < nbPoints; i++)
		{
			POINTFLT Pmetres = C.Direct(TabIn[i]);
			int index = IndexCiblePlusProche(Pmetres, avant);
			if (index < 0)
			{
				sprintf(msgerr, "Ambiguit� sur une des cibles");
				return false;
			}
			TabOut[index] = TabIn[i];
		}
		return true;
	}
	else {
		PositionCiblesRaspArriere TabCiblesG;
		TabCiblesG = TabCiblesArriere;
		if (nbPoints != TabCiblesG.NbPoints)
		{
			sprintf(msgerr, "Le nombre de points n'est pas celui attendu (%d au lieu de %d)", nbPoints, TabCiblesG.NbPoints);
			return false;
		}

		RRECTF RPixels = CadreCibles(TabIn, nbPoints, false);
		RRECTF RMetres = CadreCibles(TabCiblesG.Tab, TabCiblesG.NbPoints, true);
		//calage sommaire
		CalageLin C(RPixels, RMetres, true);
		//r�ordonnancement des cibles d�tect�es
		for (int i = 0; i < nbPoints; i++)
		{
			POINTFLT Pmetres = C.Direct(TabIn[i]);
			int index = IndexCiblePlusProche(Pmetres, avant);
			if (index < 0)
			{
				sprintf(msgerr, "Ambiguit� sur une des cibles");
				return false;
			}
			TabOut[index] = TabIn[i];
		}
		return true;
	}
}

bool OrdonneCiblesRaspberry(_list<POINTFLT> &ListeCentresCibles, POINTFLT * TabCibles, char *msgerr, bool avant)
{
	int NbPoints = ListeCentresCibles.nbElem;
	POINTFLT * TabIn = new POINTFLT[NbPoints];
	int cnt = 0;
	BalayeListe(ListeCentresCibles)		TabIn[cnt++] = *ListeCentresCibles.current();

	bool ret = DoOrdonneCiblesRaspberry(TabIn, TabCibles,NbPoints, msgerr, avant);
	delete TabIn;
	return ret;

}
