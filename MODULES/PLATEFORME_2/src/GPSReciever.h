#pragma once


#include <string>
#include <ostream>

#include <cpprest/http_client.h>
#include <cpprest/json.h>
#include <Windows.h>

#include "RepereLocal.h"
#include "Plateforme_2.h"

using namespace web::http;                  // Common HTTP functionality
using namespace web::http::client;          // HTTP client features



struct GPSData
{
	double LatDeg;
	double LonDeg;

	int Fix;

	long double Age;

	GPSData() : LatDeg(0), LonDeg(0), Fix(0), Age(0) {};
};


struct GPSReciever
{
	http_client *pClient;

	GPSData GPSDataG;
	GPSData GPSDataD;

	GPSReciever() { pClient = NULL; DataRecieved = false; };

	~GPSReciever() { delete pClient; };


	bool SetGPSClient(std::string uri, char* msgerr);

	bool GetGPSRawData(GPSData& GpsGauche, GPSData& GpsDroite, char* msgerr);
	
	bool GetGPSRawCoordinates(double &LatG, double &LonG, double &LatD, double &LonD, char* msgerr);

	bool GetRobotPosition(TRepereLocal Repere, TPositionRobot &pos, char* msgerr);

private:

	web::json::value Response;
	web::json::value Data;
	bool DataRecieved;

	bool RequestGPSClient(char * msgerr);
	
	bool ParceKeyValueJson(std::string key, web::json::value& data, char *msgerr);
	
	void ParceGPSAgeData(web::json::value dataAge, long double& age);
	
	void ParceGPSRTKData(web::json::value dataRTK, int& rtkfix);
	
	bool ParceJsonData(char * msgerr);

};