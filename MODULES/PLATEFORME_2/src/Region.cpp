
 #include <windows.h>

#include <string>
#include <fstream>

#include "Region.h"
#include "SuiviContour.h" //pour fonctions GetContour..()



//
// Impl�mentation de Region
//


 Region::Region()
{
    // Attributs g�om�triques
    NbPixels = 0;
    xMin = 0;
    xMax = 0;
    yMin = 0;
    yMax = 0;

    Label = 0;
}


Region::Region(RRECT &R)          //construction � partir d'un rectangle
{
	// Attributs g�om�triques
	NbPixels = (R.right-R.left)*(R.bottom-R.top);
	xMin = R.left;
	xMax = R.right-1;
	yMin = R.top;
	yMax = R.bottom-1;

	//remplissage liste segments
	for(int y=R.top; y<R.bottom; y++)
		Segments.push_back(SEGMENT(R.left, R.right-1, y));

	Label = 0;
}



Region::Region(LIGNE &L)          //construction � partir d'une ligne continue et ferm�e
/**************************************************/
//tir� de fhisto dans l'outil couleur ; GR juillet 2008
{

int NbSeg;
SEGMENT *TabSeg = OrdonneLigne(L, &NbSeg);
if(NbSeg==0)	return;
if(!TabSeg)	return;	//"ligne refus�e"

xMin = TabSeg[0].xg;
xMax = TabSeg[0].xd;
yMin = yMax = TabSeg[0].y;

NbPixels = 0;
//remplissage liste segments
for(int i = 0; i<NbSeg; i++)
	{
	SEGMENT &S = TabSeg[i];
	xMin = min(xMin, S.xg);
	xMax = max(xMax, S.xd);
	yMin = min(yMin, S.y);
	yMax = max(yMax, S.y);
	NbPixels += S.xd - S.xg +1;
	Segments.push_back(TabSeg[i]);
	}

Label = 0;
delete [] TabSeg;
}

Region::Region(POINTINT centre, int rayon)          //construction d'une r�gion circulaire
{
	//construction de la ligne
	int NbPoints = 100;
	LIGNE L(NbPoints);
	for(int i=0; i<100; i++)
	{
		double anglerad = i* 2*(M_PI/NbPoints);
		POINTINT P(rayon *cos(anglerad), rayon * sin(anglerad));
		L[i] = P+centre;
	}

	LIGNE LC = ~L;
	LC = LC + LIGNE(LC[LC.NbPoints-1], LC[0]); //garantit la fermeture
	*this = Region(LC);
}



/** Constructeur de copie.
 * @warning Le label est copi� : risque de doublons.
 */
Region::Region(Region& R)
{
    // Attributs g�om�triques
    NbPixels = R.NbPixels;
    Segments = R.Segments;
    xMin = R.xMin;
    xMax = R.xMax;
    yMin = R.yMin;
    yMax = R.yMax;

    Label = R.Label;
}



/** Op�rateur de copie.
 * @note Utile pour pour utiliser la classe dans une liste.
 * @warning Le label est copi� : risque de doublons.
 */
Region& Region::operator =(Region& R)
{
    if (&R == this) return *this;

    // Attributs g�om�triques
    Segments = R.Segments;
    NbPixels = R.NbPixels;
    xMin = R.xMin;
    xMax = R.xMax;
    yMin = R.yMin;
    yMax = R.yMax;

    Label = R.Label;

    return *this;
}



 bool Region::operator ==(Region& region)
{
    if ( (NbPixels != region.NbPixels) || (xMin != region.xMin) || (xMax != region.xMax) || (yMin != region.yMin) || (yMax != region.yMax) )
        return false;
	if(!ContientRegion(region, true))	return false;
	if(!region.ContientRegion(*this, true))	return false;
    return true;
}


 bool Region::ContientPoint(int x, int y)
/***************************************/
{
for (Segments.begin(); !Segments.end(); Segments++)
	{
	if(Segments.current()->y != y)	continue;

	SEGMENT *SS = Segments.current();
	if(SS->xg<= x)
		if(SS->xd >= x)
			return true;
	}
return false;
}


 bool Region::ContientSegment(SEGMENT &S)
/**************************************/
{
for (Segments.begin(); !Segments.end(); Segments++)
	{
	if(Segments.current()->y != S.y)	continue;

	SEGMENT *SS = Segments.current();
	if(SS->xg<= S.xg)
		if(SS->xd >= S.xd)
			return true;
	}
return false;
}



 bool Region::ContientRegion(Region &R, bool testinclusiontotale)
/**********************************************************/
{
for (R.Segments.begin(); !R.Segments.end(); R.Segments++)
	{
	SEGMENT *S = R.Segments.current();

	if(!testinclusiontotale)     return ContientSegment(*S);
	//il suffit de tester le premier

	if(!ContientSegment(*S))	return false;
	}
return true;
}


 bool Region::IntersectionSegment(SEGMENT &S, _list<SEGMENT> *intersection, _list<SEGMENT> *intersectant)
/***************************************************************************************************/
{
if(intersection)	intersection->clear();
if(intersectant)	intersectant->clear();

for (Segments.begin(); !Segments.end(); Segments++)
	{
	SEGMENT *SS = Segments.current();
	if(SS->y != S.y)	continue;

	int l1 = SS->xd - SS->xg +1;
	int l2 = S.xd - S.xg +1;

	int lUnion = max(SS->xd, S.xd) - min(SS->xg, S.xg)+1;

	if(lUnion< l1+l2)
		{
		if(intersection)
			{
        	SEGMENT Sintersection;
			Sintersection.y = S.y;
			Sintersection.xd = min(SS->xd, S.xd);
			Sintersection.xg = max(SS->xg, S.xg);
		    intersection->push_back(Sintersection);
			}

		if(intersectant)	intersectant->push_back(*SS);

		if(!intersection)
		if(!intersectant)
			return true;	//pas besoin de tester plus loin
		}
	}
if(intersection)	return (intersection->nbElem !=0);
if(intersectant)	return (intersectant->nbElem !=0);

return false;
}


 bool Region::IntersectionRegion(Region &R, Region *inter)
/*******************************************************/
{
_list<SEGMENT> *temp = NULL;

if(inter)
	{
	inter->NbPixels=0;
	inter->Segments.clear();
	temp = new _list<SEGMENT>;
	inter->xMin = xMin;
	inter->xMax = xMax;
	inter->yMin = yMin;
	inter->yMax = yMax;
	}


for (R.Segments.begin(); !R.Segments.end(); R.Segments++)
	{
	SEGMENT *S = R.Segments.current();

	if(IntersectionSegment(*S, inter ? temp : NULL))
		{
		if(!inter)	return true;   //suffit pour prouver intersection non vide

		for(temp->begin(); !temp->end(); (*temp)++)
			{
			SEGMENT* Stemp = temp->current();
			inter->Segments.push_back(*Stemp);
			inter->NbPixels += Stemp->xd - Stemp->xg +1;

			if(inter->Segments.nbElem ==1)	//premier segment stock�
				{
				inter->xMin = Stemp->xg;
				inter->xMax = Stemp->xd;
				inter->yMin = inter->yMax = Stemp->y;
				}
			else
				{
				inter->xMin = min(inter->xMin, Stemp->xg);
				inter->xMax = max(inter->xMax, Stemp->xd);
				inter->yMin = min(inter->yMin, Stemp->y);
				inter->yMax = max(inter->yMax, Stemp->y);
				}
			}
		}
	}

if(inter)	delete temp;
return (inter ? (inter->Segments.nbElem!=0) : false);
}


/*********************************/
 void Region::UnionRegion(Region &Rs)
/*********************************/
{

//ajout de tous les segments de Rs

for (Rs.Segments.begin(); !Rs.Segments.end(); Rs.Segments++)
	{
	SEGMENT *S = Rs.Segments.current();
	Segments.push_back(*S);
	}

//correction des redondances

ARBRE2(SEGMENT) arbre;
for(Segments.begin();	!Segments.end();	Segments++)
    	{
    	SEGMENT S = *Segments.current();
        arbre.Ajoute(S);
        }
//l'arbre contient maintenant les segments ordonn�s selon y, xg, puis xd
//-> on r�initialise la liste Segments en supprimant au passage les redondances

Segments.clear();

bool premier = true;
SEGMENT Sp;
for(arbre.Start(); arbre.NonFini(); arbre++)
	{
	SEGMENT S =	arbre.Valeur();
    if(premier)		{ Sp = S; premier = false;	continue;	}
	if(S.y == Sp.y)
        if(Sp.xd >= S.xg -1)	{	Sp.xd = max(Sp.xd, S.xd); continue;	}
		// on fusionne et on garde Sp pour boucle suivante

	Segments.push_back(Sp);
	Sp = S;
   	}
Segments.push_back(Sp);


//mise � jour nb de pixels et extrema
NbPixels = 0;
for (Segments.begin(); !Segments.end(); Segments++)
	{
	SEGMENT *S = Segments.current();
	NbPixels += S->xd - S->xg + 1;
	xMin = min(xMin, S->xg);
	xMax = max(xMax, S->xd);
	yMin = min(yMin, S->y);
	yMax = max(yMax, S->y);
	}
}


 


 std::ostream& operator <<(std::ostream& flux, Region& region)
{
    // enregistrement des attributs de RegionTPL
    flux <<"region: "  <<region.Label    <<std::endl;
    flux <<"pixels: " <<region.NbPixels <<std::endl;
    flux <<"boite: " <<region.xMin <<" " <<region.xMax <<" " <<region.yMin <<" " <<region.yMax <<std::endl;
    flux <<":" <<endl;

	// marque de fin d'en-t�te
    flux <<"segments:" <<std::endl;	

    // enregistrement des coordonn�es des segments
    for (region.Segments.begin(); !region.Segments.end() ; region.Segments++)
    {
        SEGMENT *seg = region.Segments.current();
        flux <<seg->xg <<" " <<seg->xd <<" " <<seg->y <<" " <<std::endl;
    }

    // marque de fin de r�gion
    flux <<":" <<endl;

    return flux;
}






/** Op�rateur d'extraction.
 * @note Le caract�re ':' sert de s�parateur entre l'intitul� du champ et sa valeur
 */
 std::istream& operator >>(std::istream& flux, Region& region)
{
    string tmp;

    // 1. attributs de RegionTPL
    int label = -1;
    unsigned long int nbPixels = 0;
    int xmin = -1;
    int xmax = -1;
    int ymin = -1;
    int ymax = -1;

	// 1.1 lecture
    while ( !flux.eof() )
    {
        std::getline(flux, tmp, ':');

        // un bug de Borland C++Builder fait que (tmp == "region") ne renvoie pas une valeur correcte
        if ( tmp.find("region") == 0 )
        {
            flux >>label;
            flux.ignore(1024, '\n');
            continue;
        }

        if ( tmp.find("pixels") == 0 )
        {
            flux >>nbPixels;
            flux.ignore(1024, '\n');
            continue;
        }

        if ( tmp.find("boite") == 0 )
        {
            flux >>xmin >>xmax >>ymin >>ymax;
            flux.ignore(1024, '\n');
            continue;
        }


		if ( tmp.find("segments") == 0 )
        {
            // fin de l'en-t�te
            flux.ignore(1024, '\n');
            break;
        }

        // ligne non reconnue = ligne ignoree
        flux.ignore(1024, '\n');
    }

    // 1.2 v�rification des valeurs
	if ( (label == -1) || (nbPixels == 0) || (xmin == -1) || (xmax == -1)
	 || (ymin == -1) || (ymax == -1) )
        return flux;

    region.Label = label;
    region.NbPixels = nbPixels;
    region.xMin = xmin;
    region.xMax = xmax;
    region.yMin = ymin;
    region.yMax = ymax;

    // 3. lecture de la liste des segments
    region.Segments.clear();

    SEGMENT segment;
    while ( !flux.eof() )
    {
        // marque de fin ?
        char c;
        flux.get(c);
        if (c == ':')
        {
            flux.ignore(1024, '\n');
            break;
        }
        else
            flux.putback(c);

        flux >>segment.xg >>segment.xd >>segment.y;
        flux.ignore(1024, '\n');
        region.Segments.push_back(segment);
    }

    return flux;
}



