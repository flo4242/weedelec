#include "ControlRobot.h" 
// il faut toujours inclure "ControlRobot.h" en premier pour que "erxCom.h" soit d�clar� en premier 
// (sinon incompatibilit� se trouve pour le module 'chrono') 

#include "drvtraiti.h"
#include "matrice.h"

#include "WeedelecCommon.h"
#include "TPlateforme_2.h"
#include "Plateforme_2.h"

#include "CommandeNavigation_2.h"
#include "CommandeHT.h"
#include "GPSReciever.h"
#include "KML.h"


DRVTRAITIM *pDriverG;

TControlRobot ControlRobotG;

TParamRobot ParamG;

extern TRepereLocal *RepereG;
extern TCommandeNavigation* CommandeNavigationG;
extern GPSReciever GPSRecieverG;

CommandeHT CommandeHTG;
TParamHT ParamHTG;



bool GereParametresHT(HWND hwnd, ModeGereParametres modeGereParam, TParamHT &paramHT);


extern "C"
{
	int _EXPORT_ Open_T_Plateforme_2(DRVTRAITIM *pD, HMODULE hinst)
		/*********************************************/
	{
		if (pD->SizeClass != sizeof(DRVTRAITIM))
		{
			char st[100];
			sprintf(st, "taille DRVTRAITIM non compatible - Driver: %d  DLL: %d",
				pD->SizeClass, sizeof(DRVTRAITIM));
			::MessageBox(NULL, st, "T_Plateforme", MB_OK);
			return 0;
		}
		pDriverG = pD;
		return 1;
	}

	int _EXPORT_ Close_T_Plateforme_2(DRVTRAITIM *) { return 1; }

	HMENU _EXPORT_ Ajoute_T_Plateforme_2(HMENU pere)
	/********************************/
	{
		HMENU Hprincipal = pDriverG->AjouteSousMenu(pere, "Plateforme_2", 
			"Param�tres", &TPlateforme::CMParametre,
			NULL);

		HMENU ret = pDriverG->AjouteSousMenu(Hprincipal, "Connexion Robot",
			"Connexion", &TPlateforme::CMConnectRobot,
			"D�connexion", &TPlateforme::CMDeconnectRobot,
			"Lecture mesures robot", &TPlateforme::CMTestMesureRobot, 
			"Active correction RTK", &TPlateforme::CMActiveRTKRobot,
			"Statut correction RTK", &TPlateforme::CMGetStatutRTKRobot,
			"Reset ground frame robot", &TPlateforme::CMResetGroundFrame,
			NULL);

		ret = pDriverG->AjouteSousMenu(Hprincipal, "GPS externe (double-antennes)",
			"Adresse Http GPS externe", &TPlateforme::CMInitHttpPostGPS,
			"Lecture mesures GPS externe", &TPlateforme::CMTestMesureGPSExterne,
			"Test calcul position robot", &TPlateforme::CMTestCalculPositionRobot,
			NULL);

		ret = pDriverG->AjouteSousMenu(Hprincipal, "Haute Tension",
			"Param�tres HT", &TPlateforme::CMParametreHT,
			"Initialisation HT", &TPlateforme::CMInitCommandeHT,
			"Lecture param�tres sur la carte HT", &TPlateforme::CMLireParametreHT,
			"Lecture condition de la carte HT", &TPlateforme::CMLireStatusHT,
			"Lance s�quence HT", &TPlateforme::CMLanceSequenceHT,
			NULL);

		ret = pDriverG->AjouteSousMenu(Hprincipal, "Weeding",
			"Contr�le de mouvement des bras", &TPlateforme::CMDialogueHWCommand,
			"D�placement manuel du bras droit avec la d�charge HT", &TPlateforme::CMDialogueControlBrasManuel,
			NULL);

		ret = pDriverG->AjouteSousMenu(Hprincipal, "G�n�ration de trajectoire",
			"Trajectoire sur fichier", &TPlateforme::CMInitTrajectoire,
			"Conversion linestring GoogleEarth to waypoints .kml", &TPlateforme::CMConvertPisteToWaypointKML,
			"Conversion points GPS .txt to waypoints .kml", &TPlateforme::CMConvertTxtToWaypointKML,
			NULL);
		
		ret = pDriverG->AjouteSousMenu(Hprincipal, "Commande de navigation",
			"Commande manuelle", &TPlateforme::CMNavigationManuelleRobot,
			"Suivi de trajectoire", &TPlateforme::CMNavigationAutoRobot,
			NULL);


		int Ajoutelist = pDriverG->AjouteListeItems(Hprincipal, "Stop Mouvement",
			&TPlateforme::CMStopMouvement, NULL);
		
		Ajoutelist = pDriverG->AjouteListeItems(Hprincipal, "TEST HTTP POST",
			&TPlateforme::TestHTTPClient, NULL);

		if (!TModulePlateforme::ChargeParametres(ParamG))
			pDriverG->Message("Echec chargement param�tres Robot");

		if(!GereParametresHT(NULL, ModeGereParametres::Charge, ParamHTG))
			pDriverG->Message("Echec chargement param�tres HT");
		
		/*if (!GereGroupeConfigPortCOM(pDriverG->HWindow(), ParamG.ParamCOMGPSExterne, true))
			pDriverG->Message("Echec chargement param�tres du port COM, GPS externe non initialis�");
			*/

		return Hprincipal;
	}
}


//////////////////////////
// G�n�rer des param�tres
//////////////////////////

bool GereParametres(HWND hwnd, ModeGereParametres modeGereParam, TParamRobot &param) //export
/***********************************************************************************/
{

	GROUPE G(hwnd, "Param�tres Module Plateforme 2 ", "Plateforme2.cfg",

		SEPART("****************************"),
		SEPART("***     Param�tres d'utilisateur     ***"),
		SEPART("****************************"),
		DBL("Vitesse nominale d'avancement (m/s)", param.VitesseMaxLineaire, 0, 5),
		DBL("Pas d'avancement pour le weeding (m)", param.PasAvancement, 0, 1E10),
		FICH("Trajectoire d�sir�e (kml)", param.NfTrajectoire, "kml"),
		FICH("Fichier log des mesures GPS (txt)", param.NfLog, "txt"),

		SEPART("      "),
		SEPART("****************************"),
		SEPART("***      Param�tres de d�tail        ***"),
		SEPART("****************************"),
		STRX("Adresse IP Wifi", param.AdresseConnexionWifi, 30),
		STRX("Adresse Ethernet", param.AdresseConnexionEth, 30),
		PBOOL("Connexion par Wifi", param.ConnexionWifi),
		STRX("Adresse Serveur GPS", param.AdresseServeurGPS, 30),
		ENT("P�riode de commande (ms)", param.incT_Commande, 0, 1000000),
		DBL("Rayon zone hexagonale du bras (m)", param.RayonHexagone, 0, 1E10),
		DBL("R�solution zone hexagonale du bras (m)", param.ResolutionHexagone, 0, 1E10),
		DBL("Translation X du centre bras dans rep�re robot (m)", param.TranslationDeltaX, -1E5, 1E5),
		DBL("Translation Y du centre bras dans rep�re robot (m)", param.TranslationDeltaY, -1E5, 1E5),

		NULL);

	bool OK;

	switch (modeGereParam)
	{
	case(ModeGereParametres::Charge): OK = G.Charge(); break;
	case(ModeGereParametres::Gere): OK = G.Gere(false); break; //modal pour pouvoir d�placer la bo�te de dialogue
	case(ModeGereParametres::Sauve): OK = G.Sauve(); break;
	}

	return OK;
}

void TPlateforme::CMParametre() {	GereParametres(HWindow(), ModeGereParametres::Gere, ParamG); }

bool TModulePlateforme::ChargeParametres(TParamRobot &param) { return GereParametres(NULL, ModeGereParametres::Charge, param); }



////////////////////////////////////////
// Initialisation et connexion du robot
////////////////////////////////////////

void TPlateforme::CMConnectRobot()
{
	char msg[1000];
	
	bool ret;

	if (!InitRobot(msg))		Message(msg);

	else Message("Robot connect�");
}

void TPlateforme::CMDeconnectRobot()
{
	char msgerr[1000];

	if (!CloseRobot(msgerr))		Message(msgerr);
	else
	{
		Message("Robot d�connect�");
	}
}

void TPlateforme::CMTestMesureRobot()
{
	char msgerr[1000];

	if (!InitRobot(msgerr))
	{
		Message(msgerr);
		return;
	}

	OuvreTexte();
	EffaceTexte();

	PrintTexte("Ici on teste les mesures de capteurs Ecorobotix\nTouche entr�e pour demarrer puis touche espace pour arr�ter\n");

	while (true)
	{
		if ((GetAsyncKeyState(VK_RETURN) & 0x8000) != 0)			break;
	}

	int TDebut = GetTickCount();
	while (true)
	{
		double latDeg, lonDeg, capDeg;
		if (!ControlRobotG.GetRobotSensorData(latDeg, lonDeg, capDeg, msgerr))
		{
			Message(msgerr); return;
		}

		PrintTexte("Time: %.3f, Mesure actuelle: Lat: %.8f;      Lon: %.8f;      Cap: %.5f\n", double(GetTickCount() - TDebut) / 1000, latDeg, lonDeg, capDeg);

		if ((GetAsyncKeyState(' ') & 0x8000) != 0) 			break;
	}
}



// activation GPS rtk et reception de statut 

void TPlateforme::CMActiveRTKRobot()
{
	if (!ControlRobotG.RobotIsConnected())
	{
		Message("Pas de connexion!");
		return;
	}
		

	ControlRobotG.ActiveRTK();
	Message("Activation RTK OK");
}

void TPlateforme::CMGetStatutRTKRobot()
{
	char msgerr[1000];
	char ntripMsg[100];
	char rtkMsg[100];

	if (!ControlRobotG.GetRTKStatus(ntripMsg, rtkMsg, msgerr))
		Message(msgerr);
	else
		Message("Ntrip message : %s\nRTK message : %s", ntripMsg, rtkMsg);
}

void TPlateforme::CMResetGroundFrame()
{
	if (!ControlRobotG.RobotIsConnected())
		return;

	ControlRobotG.m_client_manager->resetErxGroundFrame();
	Message("Reset ground frame OK");
}



///////////////////////////////////////////
// intialisation du capteur externe de GPS
///////////////////////////////////////////

void TPlateforme::CMInitHttpPostGPS()
{
	char msgerr[1000];

	GROUPE G(HWindow(), "Connexion Http Post GPS externe", NULL,
		STR("Adresse de connexion", ParamG.AdresseServeurGPS), NULL);
	
	if (!G.Gere()) return;

	GereParametres(NULL, ModeGereParametres::Sauve, ParamG);

	if(!GPSRecieverG.SetGPSClient(ParamG.AdresseServeurGPS, msgerr))
	{
		Message(msgerr);
		return;
	}
	
	Message("Connexion �tablie pour l'adresse %s", ParamG.AdresseServeurGPS);
}

void TPlateforme::CMTestMesureGPSExterne()
{
	char msgerr[1000];
	if (GPSRecieverG.pClient == NULL)
	{
		Message("Pas de connexion �tablie");
		return;
	}

	OuvreTexte();
	EffaceTexte();

	PrintTexte("Ici on teste les mesures du GPS externe\nTouche entr�e pour demarrer puis touche espace pour arr�ter\n");

	while (true)
	{
		if ((GetAsyncKeyState(VK_RETURN) & 0x8000) != 0)			break;
	}

	int TDebut = GetTickCount();
	while (true)
	{
		GPSData gpsG, gpsD;
		if (!GPSRecieverG.GetGPSRawData(gpsG, gpsD, msgerr))
		{
			Message(msgerr); return;
		}

		PrintTexte("Time: %.3f, Mesure actuelle:\n GPS gauche: Age(%.3f);  Lat(%.5f);  Lon(%.5f);  Fix(%d)\n",
			double(GetTickCount() - TDebut) / 1000, gpsG.Age, gpsG.LatDeg, gpsG.LonDeg, gpsG.Fix);
		PrintTexte(" GPS droite: Age(%.3f);  Lat(%.5f);  Lon(%.5f);  Fix(%d)\n\n",
			gpsD.Age, gpsD.LatDeg, gpsD.LonDeg, gpsD.Fix);

		if ((GetAsyncKeyState(' ') & 0x8000) != 0) 			break;
	}
}

void TPlateforme::CMTestCalculPositionRobot()
{
	if (GPSRecieverG.pClient == NULL)
	{
		Message("Pas de connexion �tablie");
		return;
	}

	char msgerr[1000];

	if (RepereG == NULL)
	{
		GPSData gpsG, gpsD;
		if (!GPSRecieverG.GetGPSRawData(gpsG, gpsD, msgerr))
		{
			Message(msgerr); return;
		}

		double LatM = (gpsG.LatDeg + gpsD.LatDeg) / 2;

		RepereG = new TRepereLocal(LatM);
	}
	
	OuvreTexte();
	EffaceTexte();

	PrintTexte("Ici on teste le calcul de position robot fourni par GPS externe\n");
	PrintTexte(" (si le rep�re local n'est pas encore initialis�, on fait la projection des coordonn�es GPS par rapport au premier point mesur�)\n");
	PrintTexte("\nTouche entr�e pour demarrer puis touche espace pour arr�ter\n");

	while (true)
	{
		if ((GetAsyncKeyState(VK_RETURN) & 0x8000) != 0)			break;
	}

	int TDebut = GetTickCount();
	while (true)
	{
		TPositionRobot pos;
		if (!GetRobotPositionEstimated(pos, msgerr))
		{
			PrintTexte(msgerr); return;
		}

		PrintTexte("Time: %.3f, Position Robot (rep�re local):\n X: %.5f;   Y: %.5f;   Cap: %.5f\n",
			double(GetTickCount() - TDebut) / 1000, pos.x, pos.y, pos.CapDeg);

		if ((GetAsyncKeyState(' ') & 0x8000) != 0) 			break;
	}
}


//////////////////////////////////
// commande de suivi trajectoire:
//////////////////////////////////

void TPlateforme::CMInitTrajectoire()
{
	char msgerr[1000];
	
	TNomFic NfKML = "";

	if (!ChoixFichierOuvrir("Initialisation de suivi de trajectoire", NfKML, "kml", "", HWindow()))
		return;

	sprintf(ParamG.NfTrajectoire, NfKML);

	Message("Fichier %s charg�", NfKML);
}


// fonction convertie des trajectoire 
#include "ConvertLineStringKML.h"

void TPlateforme::CMConvertPisteToWaypointKML()
{
	char msgerr[1000];

	TNomFic NfKML = "";
	TNomFic DossierSorti = "";

	_list<Waypoint> ListeWp;

	GROUPE G(HWindow(), "Conversion de trajectoire LineString en Waypoints (KML)", NULL,
		FICH("Fichier KML de LineString (pistes)", NfKML, "kml"),
		REPI("Dossier de fichier KML de sortie", DossierSorti),
		NULL);

	if (!G.Gere())	return;

	TNomFic Nf;
	strcpy(Nf, NomSansPath(NfKML));
	NomSansExt(Nf);

	TNomFic NfsortiKML;
	sprintf(NfsortiKML, "%s\\%s_Waypoint.kml", DossierSorti, Nf);
	
	if (!ConvertLineStringToWayPoint(NfKML, NfsortiKML, ListeWp, msgerr))
	{
		Message(msgerr);
		return;
	}
	else
		Message("%d Waypoints sauv�s dans %s", ListeWp.nbElem, NfsortiKML);

}

void TPlateforme::CMConvertTxtToWaypointKML()
{
	char msgerr[1000];

	TNomFic NfTxt = "";
	TNomFic DossierSorti = "";

	GROUPE G(HWindow(), "Conversion des points GPS .txt en Waypoints (KML)", NULL,
		FICH("Fichier txt des points GPS (pistes)", NfTxt, "txt"),
		REPI("Dossier de fichier KML de sortie", DossierSorti),
		NULL);

	if (!G.Gere())	return;

	MATRICE PointsGPS;
	if (!PointsGPS.Charge(NfTxt))
	{
		Message("Impossible de charger le fichier %s", NfTxt);
		return;
	}

	TNomFic Nf;
	strcpy(Nf, NomSansPath(NfTxt));
	NomSansExt(Nf);

	TNomFic NfsortiKML;
	sprintf(NfsortiKML, "%s\\%s_Waypoint.kml", DossierSorti, Nf);

	_list<Waypoint> ListeW;

	for (int i = 0; i < PointsGPS.dimL(); i++)
	{
		Waypoint Wp;
		Wp.LatDeg = PointsGPS[i][0];
		Wp.LonDeg = PointsGPS[i][1];
		Wp.elevation = 0;
		sprintf(Wp.Name, "P%d", i);
		sprintf(Wp.Description, "");

		ListeW.push_back(Wp);
	}

	if (!ExportKML(NfsortiKML, Nf, ListeW, msgerr))
	{
		Message(msgerr);
		return;
	}

	Message("%d Waypoints sauv�s dans %s", ListeW.nbElem, NfsortiKML);

}



///////////////////////////
// fonction stop plateforme
///////////////////////////

void TPlateforme::CMStopMouvement()
{
	StopPlateforme();
}




/*************************************************/
// commande navigation du robot
/*************************************************/
#include "ManualControlPad.h"
int TimeDebutG;  // time, variable pour la sauvegarde de fichier log du mouvement

//commande manuelle (dialogue de commande)
void TPlateforme::CMNavigationManuelleRobot()
{
	/*if (!ControlRobotG.RobotIsConnected())
	{
		Message("Robot non connect�");
		return;
	}*/

	OuvreTexte();
	EffaceTexte();

	if (CommandeNavigationG != NULL)
	{
		delete CommandeNavigationG;  CommandeNavigationG = NULL;
	}

	CommandeNavigationG = new TCommandeManuelle(ParamG.incT_Commande); // initialisation avec la commande manuelle
	TCommandeManuelle * pCom = dynamic_cast <TCommandeManuelle*> (CommandeNavigationG);

	if (ParamG.NfLog[0] != '\0') // initialise le fichier log
	{
		TimeDebutG = GetTickCount();  // initialise le temps de d�but
		TNomFic NomFicherLog;
		sprintf(NomFicherLog, "%s_%d", ParamG.NfLog, TimeDebutG);

		pCom->pFichierLog = new std::ofstream(NomFicherLog, std::ofstream::out);
	}


	// �tabli timer pour la commande
	char msgerr[1000];
	if (!pCom->DoStartCommande(msgerr))
	{
		Message(msgerr); return; 
	}
	
	DialogControlPad("T_Plateforme");	
}


struct RetNavigationAuto
// le retour de navigation automatique
{
	bool FinNavigationAuto; // indique la fin de navigation auto (soit finie avec succ�s, soit erreur)
	bool Succes; // indique si la navigation auto est bien finie sans erreurs
	std::string MsgRetour; // message de retour en cas d'erreur

	RetNavigationAuto() { FinNavigationAuto = false; Succes = false; MsgRetour = '\0'; };
	void Reset() { FinNavigationAuto = false; Succes = false; MsgRetour = '\0'; };

} RetNavigationAutoG;


void CallbackNavigationAuto(TRetourPlateforme ret)
{
	switch (ret.State)
	{
	case TRetourState::Completed:
		RetNavigationAutoG.Succes = true;
		break;

	case TRetourState::Error:
		RetNavigationAutoG.MsgRetour = ret.InfoRetour;
		break;

	case TRetourState::ExternalStop:
		RetNavigationAutoG.MsgRetour = "Stop Urgence";
		break;

	case TRetourState::TimeOut:
		RetNavigationAutoG.MsgRetour = "Time Out";
		break;

	default: break;
	}

	RetNavigationAutoG.FinNavigationAuto = true;
}

void TPlateforme::CMNavigationAutoRobot()
{
	char msgerr[1000];
	if (!ControlRobotG.RobotIsConnected() || RepereG == NULL || GPSRecieverG.pClient == NULL)
	{
		Message("Robot non initialis�");
		return;
	}

	_list<Waypoint> ListeW;

	if (!ImporteKML(ParamG.NfTrajectoire, ListeW, msgerr))
	{
		Message(msgerr);
		return;
	}

	if (ListeW.nbElem < 2)
	{
		Message("Veuillez v�rifier la d�finition de la trajectoire\nAu moins deux points de passage d�sir�s sont n�cessaires");
		return;
	}

	CommandeNavigationG = new TCommandeAutomatique(ParamG.incT_Commande, CallbackNavigationAuto); // initialisation de pointeur pour la commande automatique
	TCommandeAutomatique *pCom = dynamic_cast <TCommandeAutomatique*> (CommandeNavigationG);

	pCom->InitVitesse(ParamG.VitesseMaxLineaire);

	if(!GetRobotPositionEstimated(pCom->PosActuelle, msgerr))
	{
		Message(msgerr);
		return;
	}

	BalayeListe(ListeW)
	{
		Waypoint * Wp = ListeW.current();
		double x, y;
		RepereG->WGS84VersMetres(Wp->LatDeg, Wp->LonDeg, x, y);
		pCom->TrajDesire.push_back(POINTFLT(x, y));
		// charge de points de passage d�sir�s dans la trajectoire
	}

	RetNavigationAutoG.Reset();  // reinitialise la structure de retour navigation auto

	if (ParamG.NfLog[0] != '\0') // initialise le ficheir log
	{
		TimeDebutG = GetTickCount();  // initialise le temps de d�but
		TNomFic NomFicherLog;
		sprintf(NomFicherLog, "%s_%d", ParamG.NfLog, TimeDebutG);

		pCom->pFichierLog = new std::ofstream(NomFicherLog, std::ofstream::out);
	}


	OuvreTexte();
	EffaceTexte();

	if (!pCom->DoStartCommande(msgerr))
	{
		PrintTexte(msgerr);
		delete CommandeNavigationG; CommandeNavigationG = NULL;
		return;
	}
	PrintTexte("Commande automatique start ! \n");

	MSG msg;

	while (true)
	{
		if (PeekMessage(&msg, NULL, 0, 0, MB_OK)) DispatchMessage(&msg);

		if (RetNavigationAutoG.FinNavigationAuto) break;
	}

	if (RetNavigationAutoG.Succes)
		PrintTexte("Navigation auto finie avec la trajectoire d�sir�e parcourue");
	else
		PrintTexte("Navigation est arr�t�e pour l'erreur: %s", RetNavigationAutoG.MsgRetour.data());
}

/*************************************************/





/*********************************************************************************/
// commande et communication du bo�tier Haute Tension pour la d�charge �lectrique
/*********************************************************************************/

#include "ComPortSerieHT.h"
#include "GetListePortsCOM.h"

bool GroupeChoixPortCOM(HWND hwnd, char* PortCOM, char* msgerr)
// fonction qui cherche le port s�rie pour la communication avec le bo�tier de d�charge Haute tension
{
	// on va chercher tous les ports COM et choisir un dans la liste:
	_list<TNf> Liste;
	bool ret = GetListePortsCOM(Liste);

	if (Liste.nbElem == 0 || !ret)
	{
		sprintf(msgerr, "Pas de port COM pr�sent");
		return false;
	}

	//cr�ation de la liste multichoix
	AligneDescriptions(Liste);

	char** rubriques = new char*[Liste.nbElem + 1];
	int index = 0;

	BalayeListe(Liste)
	{
		char *temp = *Liste.current();
		while (true)
		{
			if (strncmp(temp, "COMx", 3) == 0) break;
			temp++;
		}
		rubriques[index++] = temp;
	}

	rubriques[index] = NULL;

	int IndexChoix = -1;

	GROUPE G(hwnd, "Configuration de Communication Port S�rie", NULL,
		MULCHOIX("Choix port COM USB", IndexChoix, rubriques),
		NULL);

	if (!G.Gere())
	{
		sprintf(msgerr, "Echec g�re groupe");
		delete rubriques;
		return false;
	}

	if (IndexChoix != -1)
		strcpy(PortCOM, rubriques[IndexChoix]);

	else
	{
		sprintf(msgerr, "Port COM non choisi");
		delete rubriques;
		return false;
	}

	return true;
}

bool GereParametresHT(HWND hwnd, ModeGereParametres modeGereParam, TParamHT &paramHT) 
/***********************************************************************************/
{
	GROUPE G(hwnd, "Param�tres Bo�tier Haute Tension - Weedelec", "ParametresHT.cfg",
		STR("Port COM (faut mieux ne pas changer ici)", paramHT.PortCOM),
		ENT("P�riode de charge de la bobine (us)", paramHT.DwellTime, 0, 65536),
		ENT("Fr�quence de r�p�tition des tirs (Hz)", paramHT.Frequence, 10, 1E7),
		ENT("Nombre de tirs r�alis�s par s�quence", paramHT.NbCycles, 1, 65536),
		ENT("Limite de courant dans la bobine (mA)", paramHT.CurrentLimit, 0, 65536),
		NULL);

	bool OK;

	switch (modeGereParam)
	{
	case(ModeGereParametres::Charge): OK = G.Charge(); break;
	case(ModeGereParametres::Gere): OK = G.Gere(true); break; //modal pour pouvoir d�placer la bo�te de dialogue
	case(ModeGereParametres::Sauve): OK = G.Sauve(); break;
	}

	return OK;
}

void TPlateforme::CMParametreHT()
{
	char msgerr[1000];

	if (!GroupeChoixPortCOM(HWindow(), ParamHTG.PortCOM, msgerr))
	{
		Message(msgerr);
		return;
	}
	GereParametresHT(NULL, ModeGereParametres::Sauve, ParamHTG); // sauv� le port com choisi

	if(!GereParametresHT(HWindow(), ModeGereParametres::Gere, ParamHTG))	return;

	GereParametresHT(NULL, ModeGereParametres::Sauve, ParamHTG); // sauv� tous les param�tres
}

void TPlateforme::CMInitCommandeHT()
{
	char msgerr[1000];

	if (!CommandeHTG.InitCommandeHT(ParamHTG, msgerr))
		Message(msgerr);
	else
		Message("Initialisation HT OK!");
}

void TPlateforme::CMLireParametreHT()
{
	char msgerr[1000];
	TParamHT paramHT;
	if (!CommandeHTG.ReadParamHT(paramHT, msgerr))
		Message(msgerr);
	else
	{
		Message("Param�res sur la carte:\n\nDwell_Time : %d (us)\nFrequence : %d\nNb_Cycles : %d\nCurrent_Limit : %d (mA)",
			paramHT.DwellTime, paramHT.Frequence, paramHT.NbCycles, paramHT.CurrentLimit);
	}
}

void TPlateforme::CMLireStatusHT()
{
	char msgerr[1000];
	double Temp, Voltage;

	if (!CommandeHTG.ReadTemperature(Temp, msgerr))
	{
		Message(msgerr);
		return;
	}
	
	if (!CommandeHTG.ReadVoltageBattery(Voltage, msgerr))
	{
		Message(msgerr);
		return;
	}
		
	Message("Condition actuelle de la carte:\n\nTemp�rature : %.2f (�C)\nVoltage batterie : %.2f (V)", Temp, Voltage);
}

void TPlateforme::CMLanceSequenceHT()
{
	char msgerr[1000];
	int CourantMax, VoltageTir, DureeTir;
	if (!CommandeHTG.StartSequence(CourantMax, VoltageTir, DureeTir, msgerr))
	{
		Message(msgerr);
		return;
	}
	
	Message("S�quence lanc�e avec l'info retour :\n\nCourant max: %d (mA)\nTension moyenne de s�quence : %d (V)\nDur�e de l'arc : %d (us)",
			CourantMax, VoltageTir, DureeTir);
}

/*********************************************************************************/




/**********************************************************************************/
// fonctions pour le weeding
/**********************************************************************************/

// dialogue de contr�le des bras par la fonction d'API SendHWcommand de l'Ecorobotix
#include "DialogSendHWCommand.h"
#include "IDControlPad.h"

void TPlateforme::CMDialogueHWCommand()
{
	/*if (!ControlRobotG.RobotIsConnected())
	{
		Message("Robot non connect�");
		return;
	}*/
	OuvreTexte();
	EffaceTexte();

	DialogSendHWCommand("T_Plateforme");
}

// dialogue de weeding avec la d�charge haute tension
#include "DialogueHauteTension.h"
void TPlateforme::CMDialogueControlBrasManuel()
{
	/*if (!ControlRobotG.RobotIsConnected())
	{
		Message("Robot non connect�");
		return;
	}*/
	OuvreTexte();
	EffaceTexte();

	DialogHauteTension("T_Plateforme");
}

/***********************************************************************************/





// fonctions provisoires:

void TPlateforme::TestHTTPClient()
{
	string url = "http://gps.local:5000";
	char msgerr[1000];
	GPSReciever gps;
	gps.SetGPSClient(url, msgerr);

	OuvreTexte();
	EffaceTexte();

	double lat1, lon1, lat2, lon2;
	if(!gps.GetGPSRawCoordinates(lat1, lon1, lat2, lon2, msgerr))
		PrintTexte("Error:\n%s", msgerr);
	else
		PrintTexte("Data:\n%f,  %f,  %f,  %f", lat1, lon1, lat2, lon2);

}
