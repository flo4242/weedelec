#ifndef __EXPORT_PLATEFORME_H
#define __EXPORT_PLATEFORME_H

#include <string>
#include "commtype.h"
#include "WeedelecCommon.h"
#include "list_tpl.h"
#include "ligne.h" //POINTFLT

#include "CommandeHT.h"


//fonctions export�es pour utilisation externe (autres modules)


struct TPositionRobot
{
	double x, y;
	double CapDeg;

};


struct _EXPORT_ TParamRobot
{
	/**************************/
	/*Param�tres d'utilisateur*/
	/**************************/

	double PasAvancement; // en m�tres
	double VitesseMaxLineaire; // vitesse lineaire maximale
	TNomFic NfTrajectoire; // nom de fichier pour la trajectoire d�sir�e
	TNomFic NfLog; // nom de fichier pour le log de navigation
	//TNomFic NfResultsKalman; // nom de fichier pour stocker les r�sultats de filtre Kalman


	/**************************/
	/*  Param�tres de d�tail  */
	/**************************/

	// param�tres de connexion
	char AdresseConnexionWifi[30];
	char AdresseConnexionEth[30];
	bool ConnexionWifi;

	// param�tre de l'adresse connexion pour le GPS externe:
	char AdresseServeurGPS[30];

	// p�riode de commande
	int incT_Commande; // p�riode de la commande robot

	// param�tres pour calage du bras Delta du robot
	double RayonHexagone;
	double ResolutionHexagone;
	double TranslationDeltaX;
	double TranslationDeltaY;
	
	// param�tre de commande Haute Tension
	TParamHT ParamComHT;
};


struct _EXPORT_ TRetourPlateforme :public TRetourTask
{
	int DureeMs;
	bool CropRowFinished;
};


typedef void(*pcallbackStep)(TRetourPlateforme ret);


enum TypeStep
{
	trajectoire, //fichier charg�
	cible,		// trajectoire position actuelle --> cible
	distance	// trajectoire droite avec le cap actuel
};

struct _EXPORT_ TStep
{
	TypeStep Type;
	TStep(TypeStep type = TypeStep::trajectoire) : Type(type) { ; }
};

struct _EXPORT_ TStepDistance : public TStep
{
	TStepDistance(double distance) : TStep(TypeStep::distance), Distance(distance) { ; }
	double Distance;
};

struct _EXPORT_ TStepCible : public TStep
{
	TStepCible(POINTFLT cible) : TStep(TypeStep::cible), Cible(cible) { ; }
	POINTFLT Cible;
};


struct _EXPORT_ TModulePlateforme
{
	//fonctions export�es pour utilisation externe (autres modules)

	static bool TModulePlateforme::ChargeParametres(TParamRobot &param);
	// charge des param�tres du module plateforme

	//avance la plateforme jusqu'� la position d'acquisition/d�sherbage suivante
	static void TModulePlateforme::GoToNextStep(TStep &step, TPositionRobot &posLocaleDebut, pcallbackStep cb);
	// le retour des fonctions d'avancement (GotoNextStep et AvancementManuel) se fait par la fonction retour pcallbackStep

	static void TModulePlateforme::AvancementManuel(TPositionRobot &posLocaleDebut, pcallbackStep cb);
	// avancement manuel par le panneau contr�l (pour le cas o� Gps RTK fonctionne mal)

	static void TModulePlateforme::AvancementSimple(TRetourPlateforme &ret, double distance, double vitesse);

	static void TModulePlateforme::Weeding(_list<POINTFLT> &weeds, TRetourPlateforme &ret, TPositionRobot posLocaleDebut,
		bool AvecChangementRepere, bool AvecHauteTension, bool AvecRecalageManuel);
	//les coordonn�es des mauvaises herbes sont fournies dans l'espace du robot au moment des prises d'image
	//pr�c�dentes (� charge de la plateforme d'ajuster en fonction du d�placement effectif depuis)

	static bool TModulePlateforme::GetPositionGPSActuelle(double &LatDeg, double &LonDeg, char *msgerr);

	static bool TModulePlateforme::VerifLancementPlateforme(bool AvecAvancement, bool AvecHauteTension, char *msgerr);

	static void TModulePlateforme::Stop(); //demande d'arr�t utilisateur

};


void RetourneStateError(TRetourPlateforme &ret, int timedebut, char* infoRet);
// fonction qui g�re le retour dans les cas d'erreur

#endif


