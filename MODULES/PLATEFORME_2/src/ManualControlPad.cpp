
#include "CommandeNavigation_2.h"
#include "ManualControlPad.h"
#include "IDControlPad.h"


#include "drvtraiti.h"
DRVTRAITIM *pDriverG;
extern TCommandeNavigation* CommandeNavigationG;

BOOL CALLBACK ManualControlDialogProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	TCommandeManuelle * ComManuelle = dynamic_cast<TCommandeManuelle*> (CommandeNavigationG);

	switch (uMsg)
	{
	
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
			case IDC_BUTTON1:
				pDriverG->PrintTexte("Up\n");
				ComManuelle->VitesseNominaleAdd();
				break;

			case IDC_BUTTON2:
				pDriverG->PrintTexte("Down\n");
				ComManuelle->VitesseNominaleSub();
				break;

			case IDC_BUTTON3:
				pDriverG->PrintTexte("Left\n");
				ComManuelle->VitesseDiffAdd();
				break;

			case IDC_BUTTON4:
				pDriverG->PrintTexte("Right\n");
				ComManuelle->VitesseDiffSub();
				break;

			case IDC_BUTTON5:
				pDriverG->PrintTexte("Stop\n");
				ComManuelle->StopVitesse();
				break;
		
			default:
			{
				ComManuelle->StopVitesse(); // arr�t du robot
				ComManuelle->StopTimer(); // arr�t du timer

				// r�initialise le pointeur de commande
				delete CommandeNavigationG; 
				CommandeNavigationG = NULL;

				EndDialog(hwnd, 0); // ferme la dialogue
				return TRUE;
			}

		}
		break;
	}
		
	default: return FALSE;
	}
}


void DialogControlPad(TNomFic NomModule)
{
	// r�cup�ration du nom du module dll 
	TNomFic NomComplet;
#ifdef _DEBUG
	sprintf(NomComplet, "%s_64d", NomModule);
#else
	sprintf(NomComplet, "%s_64", NomModule);
#endif

	HMODULE HVS = GetModuleHandle(NomComplet); 

	DialogBox(HVS, MAKEINTRESOURCE(IDCONTROL), NULL, (DLGPROC)ManualControlDialogProc);
}