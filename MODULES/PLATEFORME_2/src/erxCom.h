/***
*
* 
* erxComInt.h
* This is the communication interface with ecorobotix's p1 robot.  
*
****/
#pragma once

#include <map>
#include <string>
#include <atlbase.h>

#include <cpprest/streams.h>
#include <cpprest/astreambuf.h>
#include <cpprest/containerstream.h>
#include <cpprest/ws_client.h>
#include <cpprest/json.h>
#include <cpprest/asyncrt_utils.h>

#ifdef _WIN32
# define ERXEXPORTLIB __declspec( dllexport )
#else
# define ERXEXPORTLIB 
#endif

class ErxComManager
{
	public:
		ERXEXPORTLIB ErxComManager();

            /**
             * @brief      { Used to connect to the robot.
             * 				Must be called before atempting any communication with robot. }
             * @param[in]	ip_interface { ip adress of the robot to connect to including port number }
             */
		ERXEXPORTLIB void connect(utility::string_t ip_interface);

            /**
             * @brief      { Close connection to robot. }
             *
             */
		ERXEXPORTLIB void close();

            /**
             * @brief      { Connection status }
             *
             * @return     { True if connected, false otherwise. }
             */
		ERXEXPORTLIB bool isConnected();

            /**
             * @brief      { Can be called to create new connection with robot.}
             */
		ERXEXPORTLIB void reset();

            /**
             * @brief		{ Sets the robot wheel speed. This can be used to 
             * 				make a joystick }
             *
             * @param[in]  leftS   The left wheel speed [m/s]
             * @param[in]  rightS  The right wheel speed [m/s]
             */
		ERXEXPORTLIB void setSpeed(float leftS, float rightS);

            /**
             * @brief      { The robot moves in a straight line for the specified distance. }
             *
             * @param[in]	dist	{ The distance to move in a straight line [m/s] }
             */
		ERXEXPORTLIB void moveStraight(double dist);

            /**
             * @brief      { rotates the robot of a given angle with the given radius.
             * 				if angle > 0 && radius > 0 the robot turns ACW moving forward
							if angle > 0 %% radius < 0 the robot truns ACW going backwards
							if angle < 0 && radius < 0 the robot turns CW moving forward
							if angle < 0 %% radius > 0 the robot truns CW going backwards}
             *
             * @param[in]  angle   { The angle }
             * @param[in]  radius  { The radius }
             */	
		ERXEXPORTLIB void rotateAngle(double angle, double radius);

			/**
			 * @brief		Stops all current autonomous movement.
			 *
			 */
		ERXEXPORTLIB void stopMovement();

            /**
             * @brief		Gets the raw GPS and compas datas.
             *
             * @return		A json containing the current robot positions :
			 *				{
*			 *					"lat" : (double), // in degrees of latitude
*			 *					"lon" : (double), // in degrees of longitude
			 *					"orient": (double), //  in degrees from the magnetic north
			 *				}
			 *				Positions may take -1.0 when not available.
             */
		ERXEXPORTLIB web::json::value getRawAbsPosition();

			/**
			 * @brief		Gets the estimated robot positions in the Erx ground coordinates.
			 *				The estimation is done through a kalman filter.
			 *
			 * @return		A json containing the current estimated robot positions :
			 *				{ 
			 *					"x": (double), // in [m] in erx Ground coordinates
			 *					"y": (double), // in [m] in erx Ground coordinates
			 *					"orient": (double) // in degrees from the magnetic north
			 *				}
			 *				Positions may take -1.0 when not available.
			 */
		ERXEXPORTLIB web::json::value getEstimatedPosition();

            /**
             * @brief      Returns the current robot status
             * 
             * @return		A json contiaining the current robot status.
             * 				This json can take the form:
             *				{
             *					"mainStatus" : (bool), //currently not updated 
             *					"imgLog" : (bool), //returns if images are saved to the disk.
             *				}
             * 				
             */
		ERXEXPORTLIB web::json::value getStatus();

			/**
			 * @brief		Activates connection to the ntrip server. This needs an active gsm connection.
			 *
			 */
		ERXEXPORTLIB void activateRTKCorrection(bool active);

			/**
			 * @brief		Activates connection to the ntrip server. This needs an active gsm connection.
			 *
			 * @return		{	
			 *					"ntripStat": (string),	// status of connection to ntrip server.
			 *					"rtkStat" : (string) // can be either of : "no gps signal", "no rtk", "resolving rtk", "rtk fix", "rtk float", "rtk unknown"
			 *				}
			 *
			 */
		ERXEXPORTLIB web::json::value getRTKStatus();

			/**
			 * @brief		Toggles the weeding mode on or off.
			 *				Must be called before "injectWeeds". 
			 *				!!! Calling this function will automatically home the deltas !!!
			 */
		ERXEXPORTLIB void startInjectWeeds(bool active);

			/**
			 * @brief		!!! must call startInjectWeeds before !!!
			 *				Allows the user to add weeds to the weed buffer. Weeds should be represented in the "ground" referential and in [m].
			 *				after beeing injected the weeds will be sprayed by the delta arm once the weed is inside the delta's work zone.
			 *				weedArray: 
			 *					{	
			 *						0 : { "gndx" : -0.4, "gndy" : -0.6},
			 *						1 : { "gndx" : -0.34, "gndy" : 0.23} // add as many weeds as necessary following this pattern. The first integer is the weed id.
			 *					}
			 */
		ERXEXPORTLIB void injectWeeds(web::json::value weedArray);

			/**
			 * @brief		Resets the erxGround reference frame to current robot position.
			 */
		ERXEXPORTLIB void resetErxGroundFrame();

			/**
			 * @brief		Bypasses the machine control to send HW commands directly to the uC.
			 *
			 * @param[in]  HW cmd.
			 */
		ERXEXPORTLIB void sendHWcommand(utility::string_t cmd);


	private:
            
		web::experimental::web_sockets::client::websocket_client m_ws_client;
            // Utility functions
		ERXEXPORTLIB static pplx::task<void> async_do_while(std::function<pplx::task<bool>(void)> func);
		ERXEXPORTLIB static pplx::task<bool> _do_while_iteration(std::function<pplx::task<bool>(void)> func);
		ERXEXPORTLIB static pplx::task<bool> _do_while_impl(std::function<pplx::task<bool>(void)> func);
		ERXEXPORTLIB static web::json::value parseJson(const std::string& str_msg);
            /**
             * @brief       Establishes a websocket connection to a robot 
             *              handles the handshake protocol
             * @return      pplx task
             */
		ERXEXPORTLIB pplx::task<void> connectionHandshake(utility::string_t ip);
            
            /**
             * @brief       Close the websocket
             * @return 
             */
		ERXEXPORTLIB pplx::task<void> disconnect();
            
            /**
             * brief        creates a listening thread to monitor incoming
             *              messages coming through the open websocket
             */
		ERXEXPORTLIB void listenToIncomingMessage();
            
            /**
             * @brief       Send raw json command as string
             * @param		erx_command
             * @return 
             */
		ERXEXPORTLIB pplx::task<void> sendCommand(const std::string& erx_command);
            
            /**
             * @brief       Send formatted command
             * @param		erx_command
             * @return 
             */
		ERXEXPORTLIB pplx::task<void> sendCommand(web::json::value &erx_command);
            
            /**
             * @brief       Receive and decode command 
             * @param		robot_msg
             */
		ERXEXPORTLIB void receiveCommand(web::json::value robot_msg);
            
            /**
             * @brief       takes care of sending a raw position update request 
             */
		ERXEXPORTLIB void updateGPSPosition();

			/**
			 * @brief       takes care of sending a filtered position update request
			 */
		ERXEXPORTLIB void updateFilteredPosition();
            
            /**
             * @brief       takes care of sending a status update request
             */
		ERXEXPORTLIB void updateStatus();

			/**
			 * @brief       takes care of sending a RTK status update request
			 */
		ERXEXPORTLIB void updateRTKStatus();
            
		bool m_is_connected_ = false;

		web::json::value connection_status_;

		utility::string_t local_id_ = U("wlcModule");
		utility::string_t remote_peer_id_;

        utility::string_t remote_peer_instance_;

        int msg_num_ = 0;

        std::map<utility::string_t, utility::string_t> com_int_map_;
		utility::string_t current_com_int_;
            
        // Robot info to be updated via the communication channel
           
        web::json::value robot_status_;
        volatile bool is_robot_status_updated_ = false;
            
        web::json::value robot_raw_positions_;
		web::json::value robot_estimated_positions_;
		web::json::value rtk_status_;
        volatile bool is_robot_position_updated_ = false;
		volatile bool is_filtered_robot_position_updated_ = false;
};
