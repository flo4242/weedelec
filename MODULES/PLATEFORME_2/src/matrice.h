// *****************************  MATRICE.H  ********************************
#ifndef __MATRICE_H
#define __MATRICE_H

#include "commtype.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

using namespace std;

#include <fstream>
#include <strstream>

//#include <iomanip.h>
#include <string.h>
/*#ifdef _LINUX_
#  include <strstream.h>
#else
#  include <strstrea.h>
#endif*/

#define max(a,b)    (((a) > (b)) ? (a) : (b))
#define min(a,b)    (((a) < (b)) ? (a) : (b))

#define TYPE_MATRICE long double
#define TYPE_VECTEUR long double
#define ABSMIN 1E-4000  //pour long double

extern void ErreurMathutil(char *message);

/* Fonction assurant l'affichage du message suivi d'une pause.
   Cette fonction est definie dans:
   ERRMHDOS.CPP pour les projets sous DOS (printf et getch)
   ERRMHWIN.CPP pour les projets sous Win 3.1 ou 95 (MessageBox) */



class _EXPORT_  VECTEUR;  //pour r�f�rences crois�es
class _EXPORT_  MATCARREE; //
// **********************************************************************
// ****************** CLASSE MATRICE   *********************************
// **********************************************************************


class _EXPORT_  MATRICE
{
  protected:

   int *pRef;   // nombre de r�f�rences au tableau de valeurs

   void LibereM(void);
   void AlloueM(int l0, int c0, bool *err = NULL, char *msg1000 = NULL);

   int l ;  //Nombre de lignes
   int c ;  //Nombre de colonnes


  public:

   TYPE_MATRICE **M;  //accessible pour test allocation OK apr�s construction.


// *********** CONSTRUCTEURS ************
   MATRICE(int i=1 , int j=1);
   MATRICE(const MATRICE &Mat);
   ~MATRICE();

// *********** OPERATEURS SURCHARGES ****************

// op�rateur [] : permet l'acc�s direct � une �l�ment par MATRICE[i][j]
// *********************************************************************
   TYPE_MATRICE* operator[](int i)      //retourne un pointeur de ligne
      {return(M[i]);}

// op�rateur == (attention! pas de recopie des donn�es !!)
// ******************************************************
   void operator = (const MATRICE &M);


// op�rateurs de calcul
// *******************
   MATRICE operator+(const MATRICE &M);
   MATRICE operator+=(const MATRICE &M);
   MATRICE operator-(const MATRICE &M);
   MATRICE operator*(const MATRICE &M);
   MATRICE operator*(TYPE_MATRICE  k);
   MATRICE operator~(void);               // Transpos�e !!!
   MATRICE operator&(MATRICE &S);//op�ration de concat�nation "lignes".
   MATRICE MATRICE :: operator/(MATRICE &S);//op�ration de concat�nation "colonnes".


// ****** FONCTIONS MEMBRES ******************

   MATRICE  Copie(void);     //recopie les valeurs (l'op�rateur = ne le fait pas !)
   // utilisation: A = B.Copie();
   VECTEUR Colonne(int j) ;  // Colonne (j) �> Vecteur
   VECTEUR Ligne(int i) ; //Ligne (i) => Vecteur
   void Colonne(int j, VECTEUR V);//remplace la j�me colonne par V
   void Ligne(int i,VECTEUR V); 
   void Constante(TYPE_MATRICE d);   //remplit avec une valeur constante
   void InitPartielle(int l, int c, MATRICE &M); //remplit partiellement
   MATRICE ExtraitPartielle(int l, int c, int diml, int dimc);
   //extrait une partie de matrice de taille [diml][dimc]

   double Norme() ;  //retourne la racine de la somme des carr�s des termes...
   
   /*  
       Fonction calcul et retourne une matrice de covariance (carr�e) � partir de la matrice de donn�es (this).<BR>
       Les informations contenues dans la matrice de donn�es sont rang�es de la mani�re suivante:<BR>
       Chaque vecteur de donn�es correspond � une ligne de la matrice,<BR>
       Il doit donc y avoir une colonne de la matrice de donn�es pour chaque composante des vecteurs:<BR><BR>
                
       Illutration :<BR>
       si :<BR>
       -Vi est un vecteur de donn�es,<BR>
       -Vi[n] est la ni�me composante de ce vecteur,<BR>
       -Les vecteurs contiennent N composnates,<BR>
       -La matrice contient M vecteurs.<BR>

       {V0[0],          V0[1],          V0[2],  ...,    V0[n], ..., V0[N-1]} <BR>
       {V1[0],          V1[1],          V1[2],  ...,    V0[n], ..., V1[N-1]}<BR>
       ...<BR>
       {Vm[0],          Vm[1],          Vm[2],  ...,    Vm[n], ..., Vm[N-1]}<BR>
       ...<BR>
       {VM-1[0],        VM-1[1],                VM-1[2], ...,   VM-1[n], ..., VM-1[N-1]}<BR>
   */
   MATCARREE Covariance(void);

   void Affiche(ostream &os, int lignedebut=0, int colonnedebut=0 ) ;
   int dimL(void) { return(l); }
   int dimC(void) { return(c); }
   // affiche les termes � partir du terme MATRICE[a][b]
   // (10 termes maxi en ligne, 20 en colonne)
   int  MatNulle();  //test si la matrice est nulle (si oui, retourne TRUE)

   MATRICE Centre(VECTEUR &Moyenne);
   // retourne matrice centr�e et moyennes des colonnes dans Moyenne

   MATRICE Decale(VECTEUR &Moyenne);
   // soustrait � chaque colonne j la valeur Moyenne[j]

   MATRICE Normalisee(VECTEUR &E);
   //normalise chaque colonne (�cart-type de 1);
   // �cart-types de chaque colonne de d�part dans E

   MATRICE Reduit(VECTEUR &E);
   MATRICE  CoupeL(int x0,int x1);
   MATRICE  CoupeC(int x0,int x1);

   //op�ration sur fichiers
   bool Charge(char *fichier, int NbLignes=0, int NbColonnes=0, int SkipNfirstLines=0);
   bool Sauve(char *file);

   //gard� pour compatibilit�
   int ChargeF(char *fichier, int NbLignes=0, int NbColonnes=0);
   int SauveAscii(char *file);
};


// **********************************************************************
// ****************** CLASSE MATRICE CARREE ****************************
// **********************************************************************

class _EXPORT_  MATCARREE : public MATRICE
{
  public:


   MATCARREE( int m=1 ) : MATRICE( m , m ) {;}
   MATCARREE( MATCARREE& M) : MATRICE (M) {;}
   MATCARREE( MATRICE M);       //sortie si casting impossible !!
   void DiagonaleConstante(TYPE_MATRICE d) ;    //remplit la diagonale
   int DecompositionLU(MATCARREE& L ,MATCARREE& R, bool *err=NULL, char *msg1000=NULL);
   VECTEUR ResolutionSysLU(VECTEUR Y, bool *err=NULL, char *msg1000=NULL); //retourne X tq Y = A*X
   MATCARREE InverseLU(bool *err=NULL, char *msg1000=NULL);   //inversion de matrice par LU
   void Remplitdiagonale(VECTEUR &V);
   void Identite(void);
//   int DiagonaliseSymetrique(VECTEUR &ValP, MATCARREE &VectP, int nbrotmax= 100);
   //reserve aux matrices symetriques
   //retourne le nb de rotations Jacobi n�cessaires
   //(d'apres Numerical Recipes 2nde edition p467)
   // A REVOIR !!! Non op�rationnel...





   // RESERVE AUX MATRICES DEFINIES POSITIVES !!!!!!!!!
   MATCARREE DecompositionCholeski(bool *err=NULL, char *msg1000=NULL);
   //retourne la matrice triangulaire inf�rieure L tq A = L*~L
   // RESERVE AUX MATRICES DEFINIES POSITIVES !!!!!!!!!
   VECTEUR ResolutionSysCholesky(VECTEUR Y, bool *err=NULL, char *msg1000=NULL);//retourne X tq Y = A*X
   MATCARREE InversionCholesky(bool *err=NULL, char *msg1000=NULL);
   TYPE_VECTEUR  DET(bool *err=NULL, char *msg1000=NULL);
   MATCARREE Tred2(VECTEUR &V1,VECTEUR &V2);
   MATCARREE Tqli(VECTEUR &V0,VECTEUR &V1, bool *err=NULL, char *msg1000=NULL);
   MATCARREE VECTP(void);
   VECTEUR VALP(bool *err=NULL, char *msg1000=NULL);

   //calcul des valeurs propres et vecteurs propres
   void eig(VECTEUR &Val,MATCARREE &Vec);

    //classement par ordre d�croissant des valeurs et vecteurs propres
    //(� faire apr�s appel de eig() ) 
   void eigsrt(VECTEUR &V1,MATCARREE &A);
};


// **********************************************************************
// ****************** CLASSE VECTEUR   *********************************
// ************************************************************************

class VECTEUR
{
  protected:
   
   int *pRef;      // nombre de r�f�rences au tableau de valeurs
   void LibereV(void);
   void AlloueV(int l0);
   int l;  //Nombre de termes du vecteur

  public:
      
   TYPE_VECTEUR  *V ; // Accessible pour test allocation OK

   int dim()    { return(l); }

// ******* CONSTRUCTEURS *******************
   VECTEUR(int i=1);
   VECTEUR(const VECTEUR &Vect) ;
   ~VECTEUR() ;

// ********* OPERATEURS SURCHARGES ***********

   // op�rateur [] : permet l'acc�s direct � une �l�ment par: VECTEUR[i]
   TYPE_VECTEUR& operator[] (int i)   { return(V[i]); }

   // op�rateur == (attention! pas de recopie des donn�es !!)
   void operator = (const VECTEUR &V);

   // conversion en matrice colonne:
   operator MATRICE();
   //conversion en matrice ligne
   MATRICE operator~();


   //op�rateurs de calcul
   VECTEUR operator+(const VECTEUR &V) ;
   VECTEUR operator-(const VECTEUR &V) ;
   VECTEUR operator*(TYPE_VECTEUR  k) ; //multiplication par scalaire
   MATRICE operator*(MATRICE M);          //multiplication par une matrice
   VECTEUR operator/(VECTEUR &P);
   TYPE_VECTEUR operator|(const VECTEUR &Vect);   // Produit scalaire


// ******** FONCTIONS MEMBRES ******************

   VECTEUR Copie(void);   //Recopie les valeurs (l'op�rateur = ne le fait pas !)
   // utilisation: A = B.Copie();
   void  Constante(TYPE_VECTEUR d);    //Remplit avec une valeur constante
   double Norme() ;                    //Retourne la norme d'un vecteur...
   double Norme2() ;                   //Retourne le carr� de la norme d'un vecteur...
   void  Affiche(ostream &os, int n=0) ;
   //Affiche les termes du vecteur, en colonne, � partir du n�me  (20 maxi)
   int   VectNul() ;             //Teste si le vecteur est nul
   TYPE_VECTEUR Ecarttype(void);
   TYPE_VECTEUR Moy(void);
   TYPE_VECTEUR Min(void);
   TYPE_VECTEUR Max(void);
   TYPE_VECTEUR Som(void);
   TYPE_VECTEUR cor(VECTEUR &V1);

   bool Charge(char *fichier);	//en ligne ou colonne sur le fichier indiff�remment
   bool Sauve(char *file);		//sauvegarde en colonne


};
// *************************************************************************
void Tred2(MATRICE &A,VECTEUR &V1,VECTEUR &V2);
void Tqli(VECTEUR &V0,VECTEUR &V1,MATRICE &A);
void eigsrt(VECTEUR &V1,MATCARREE &A);
// *************************************************************************
#endif


