#pragma once

#include <cpprest/http_client.h>
#include <cpprest/json.h>

using namespace web::http;                  // Common HTTP functionality
using namespace web::http::client;          // HTTP client features

http_client SetHTTPClient(std::string uri);

bool GetHTTPResponse(http_client client, std::string& res);