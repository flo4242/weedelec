// dans le module plateforme2, on utilise le syst�me de deux antennes de GPS externe pour le guidage


#include "ControlRobot.h" 
// il faut toujours inclure "ControlRobot.h" en premier pour que "erxCom.h" soit d�clar� en premier 
// (sinon incompatibilit� se trouve pour le module 'chrono') 

#include "Plateforme_2.h"
#include "CommandeNavigation_2.h"
#include "ExportModulePlateforme_2.h"
#include "GetPortsCOM.h"
#include "GPSReciever.h"

#ifndef VITESSEMAX
#define VITESSEMAX  0.4   
#endif

TRepereLocal *RepereG = NULL;
Hexagone *pHexagoneBras = NULL;
GPSReciever GPSRecieverG;

extern TParamRobot ParamG;
extern TControlRobot ControlRobotG;
extern TCommandeNavigation *CommandeNavigationG;


bool InitRobot(char *msgerr) 
// Connexion du robot en chargant l'adresse saisie, cr�ation du rep�re local lisant la position initiale de capteur du robot (ou GPS externe)
// Et initialisation de filtre Kalman qui tourne tout le temps d�s la connexion jusqu'� la d�connexion (voir fonction CloseRobot())
{
	if (ControlRobotG.RobotIsConnected())
	{
		sprintf(msgerr, "Robot d�j� connect�");
		return false;
	}

	char* AdresseConnexion;

	// charge l'adresse de connexion (connexion soit par wifi soit par ethernet)
	if (ParamG.ConnexionWifi)	AdresseConnexion = ParamG.AdresseConnexionWifi;
	else	AdresseConnexion = ParamG.AdresseConnexionEth;

	// envoie de l'adresse connexion au robot et connecte
	ControlRobotG.SetConnectAddress(AdresseConnexion);

	if (!ControlRobotG.ConnectRobot(msgerr))
		return false;

	if (!GPSRecieverG.SetGPSClient(ParamG.AdresseServeurGPS, msgerr)) return false;
	
	//lecture latitude pour initialiser le rep�re local
	double latG, lonG, latD, lonD;

	if(!GPSRecieverG.GetGPSRawCoordinates(latG, lonG, latD, lonD, msgerr)) return false;
	
	double latdeg = (latG + latD) / 2; // cr�er un rep�re local en prenant la latitude comme la moyenne de celles de deux GPS
	
	// cr�ation de nouvel rep�re local
	if (RepereG != NULL)
	{
		delete RepereG; RepereG = NULL;
	}
	RepereG = new TRepereLocal(latdeg);

	// on d�finit l'espace de travail:
	if (pHexagoneBras != NULL)
	{
		delete pHexagoneBras; pHexagoneBras = NULL;
	}
	pHexagoneBras = new Hexagone(ParamG.RayonHexagone, ParamG.ResolutionHexagone);

	return true;
}

bool CloseRobot(char *msgerr)
// D�connecter le robot et d�truire, arr�ter le timer pour le filtre Kalman et d�truire le rep�re local
{
	if (!ControlRobotG.DeconnectRobot(msgerr)) return false;
	
	if (RepereG != NULL) // d�truit rep�re local
	{
		delete RepereG;
		RepereG = NULL; // permettant la reconstruction de rep�re local d�s la re-connexion
	}

	if (pHexagoneBras != NULL)
	{
		delete pHexagoneBras; pHexagoneBras = NULL;
	}

	if (CommandeNavigationG != NULL) // d�truit la commande de navigation (soit manuelle, soit automatique)
	{
		if (CommandeNavigationG->Timer_Commande) CommandeNavigationG->StopTimer();
		delete CommandeNavigationG; CommandeNavigationG = NULL;
	}

	if (ControlRobotG.ArmInitialised)
	{
		ControlRobotG.CloseRightArm();
		ControlRobotG.ArmInitialised = false;
	}

	return true;
}


bool GetRobotPositionEstimated(TPositionRobot &pos, char *msgerr)
// r�ception de position estim�e du robot par le syst�me de GPS externe, 
// dont la communication �tablie par la structure GPSReciever
{
	if (GPSRecieverG.pClient == NULL)
	{
		sprintf(msgerr, "GPS http serveur non initialis�");
		return false;
	}
	if (RepereG == NULL)
	{
		sprintf(msgerr, "Rep�re local non initialis�");
		return false;
	}

	if (!GPSRecieverG.GetRobotPosition(*RepereG, pos, msgerr)) return false;

	return true;
}


void StopPlateforme()
{
	if(CommandeNavigationG != NULL)
		CommandeNavigationG->StopTimer();
	
	if (ControlRobotG.RobotIsConnected())
		ControlRobotG.SetRobotSpeed(0, 0);
}

