
#include "GPSReciever.h"

// param�tres constants

#define		CLIENT_TIMEOUT		1.5     // timeout de 1.5 secondes
#define		RTKFIX		5
#define		RTKFLOAT	4

// les cl�s de json

#define		AGEG	"age.gauche"
#define		AGED	"age.droite"
#define		RTKG	"gauche_gps_qual"		// rtk gauche
#define		RTKD	"droite_gps_qual"		// rtk droite
#define		LATG	"lat1"
#define		LATD	"lat2"
#define		LONG	"lon1"
#define		LOND	"lon2"


bool GPSReciever::SetGPSClient(std::string uri, char* msgerr)
{
	utility::string_t uri_stringt = utility::conversions::to_string_t(uri);
	
	if (pClient != NULL)
	{
		delete pClient; pClient = NULL;
	}

	try
	{
		pClient = new http_client(uri_stringt);
	}

	catch (std::exception e)
	{
		sprintf(msgerr, "Echec cr�ation http client");
		return false;
	}

	return true;
}

bool GPSReciever::RequestGPSClient(char * msgerr)
{
	http_response response;

	try
	{
		// ordinary `get` request
		response = pClient->request(methods::GET).get();
		Response = response.extract_json().get();
	}

	catch (std::exception e)
	{
		sprintf(msgerr, "Echec r�ception de la r�ponse");
		return false;
	}

	DataRecieved = true;
	return true;
}


bool GPSReciever::ParceKeyValueJson(std::string key, web::json::value& data, char *msgerr)
{
	utility::string_t Key = utility::conversions::to_string_t(key);
	if (Response.has_field(Key))
	{
		data = Response.at(Key);
	}
	else
	{
		sprintf(msgerr, "Echec: cl� json %s incorrecte", key.data());
		return false;
	}
	return true;
}


void GPSReciever::ParceGPSAgeData(web::json::value dataAge, long double& age)
{
	std::string ret = utility::conversions::to_utf8string(dataAge.as_string());
	char *pnt = new char[ret.length() + 1];
	memcpy(pnt, ret.data(), ret.length()); *(pnt + ret.length()) = '\0';

	int indexSec, indexMin;

	for (int i = 0; i < ret.length(); i++) // recherche l'index de caract�res s�parant heure et minute
	{
		if (ret[i] == ':')
		{
			indexMin = i;
			break;
		}
	}

	for (int i = indexMin + 1; i < ret.length(); i++) // recherche l'index de caract�res s�parant minute et seconde
	{
		if (ret[i] == ':')
		{
			indexSec = i;
			break;
		}
	}

	*(pnt + indexMin) = '\0';
	*(pnt + indexSec) = '\0';

	double heure, min, sec;
	heure = atof(pnt); min = atof(pnt + indexMin + 1); sec = atof(pnt + indexSec + 1);

	age = heure * 3600 + min * 60 + sec; // l'�ge en secondes
}


void GPSReciever::ParceGPSRTKData(web::json::value dataRTK, int& rtkfix)
{
	std::string ret_rtk = utility::conversions::to_utf8string(dataRTK.as_string());
	rtkfix = atof(ret_rtk.data());
}


bool GPSReciever::ParceJsonData(char * msgerr)
{
	if (!DataRecieved)
	{
		sprintf(msgerr, "Echec: pas de donn�es r�ceptionn�es");
		return false;
	}

	// parce age
	if (!ParceKeyValueJson(AGEG, Data, msgerr))
		return false;
	ParceGPSAgeData(Data, GPSDataG.Age); // �ge gauche

	if (!ParceKeyValueJson(AGED, Data, msgerr))
		return false;
	ParceGPSAgeData(Data, GPSDataD.Age); // �ge droite

	// parce latitude 
	if (!ParceKeyValueJson(LATG, Data, msgerr))
		return false;
	GPSDataG.LatDeg = Data.as_double(); // latitude gauche

	if (!ParceKeyValueJson(LATD, Data, msgerr))
		return false;
	GPSDataD.LatDeg = Data.as_double(); // latitude droite


	// parce longitude
	if (!ParceKeyValueJson(LONG, Data, msgerr))
		return false;
	GPSDataG.LonDeg = Data.as_double(); // longitude gauche

	if (!ParceKeyValueJson(LOND, Data, msgerr))
		return false;
	GPSDataD.LonDeg = Data.as_double(); // longitude droite


	// parce rtk fix
	if (!ParceKeyValueJson(RTKG, Data, msgerr))
		return false;
	ParceGPSRTKData(Data, GPSDataG.Fix); // rtk droite

	if (!ParceKeyValueJson(RTKD, Data, msgerr))
		return false;
	ParceGPSRTKData(Data, GPSDataD.Fix); // rtk droite

	DataRecieved = false;
	return true;
}


bool GPSReciever::GetGPSRawCoordinates(double &LatG, double &LonG, double &LatD, double &LonD, char* msgerr)
{
	if (pClient == NULL)
	{
		sprintf(msgerr, "Echec: http client non �tabli");
		return false;
	}

	// r�ception et parce de donn�es au format Json
	if (!RequestGPSClient(msgerr) || !ParceJsonData(msgerr)) return false;


	if (abs(GPSDataG.Age) > CLIENT_TIMEOUT || abs(GPSDataD.Age) > CLIENT_TIMEOUT)
	// v�rification de l'�ge des donn�es r�ceptionn�es
	{
		sprintf(msgerr, "Echec: r�ception timeout (GPS �ge incoh�rent)");
		return false;
	}


	// v�rification de fix rtk (float est acceptable aussi)
	if (!(GPSDataG.Fix == RTKFIX || GPSDataG.Fix == RTKFLOAT))
	{
		sprintf(msgerr, "Echec: rtk non conforme pour le GPS gauche (fix:%d)", GPSDataG.Fix);
		return false;
	}

	if (!(GPSDataD.Fix == RTKFIX || GPSDataD.Fix == RTKFLOAT))
	{
		sprintf(msgerr, "Echec: rtk non conforme pour le GPS droite (fix:%d)", GPSDataD.Fix);
		return false;
	}

	// retour des coordonn�es GPS brutes
	LatG = GPSDataG.LatDeg; LonG = GPSDataG.LonDeg;
	LatD = GPSDataD.LatDeg; LonD = GPSDataD.LonDeg;
	
	return true;
}


bool GPSReciever::GetRobotPosition(TRepereLocal Repere, TPositionRobot &pos, char* msgerr)
{
	double latG, lonG, latD, lonD;

	if (!GetGPSRawCoordinates(latG, lonG, latD, lonD, msgerr))
		return false;

	double XGauche, YGauche, XDroite, YDroite;
	Repere.WGS84VersMetres(latG, lonG, XGauche, YGauche);
	Repere.WGS84VersMetres(latD, lonD, XDroite, YDroite);

	pos.x = (XGauche + XDroite) / 2;
	pos.y = (YGauche + YDroite) / 2;
	pos.CapDeg = atan2(YDroite - YGauche, XDroite - XGauche) * 180 / M_PI;

	return true;
}


bool GPSReciever::GetGPSRawData(GPSData& GpsGauche, GPSData& GpsDroite, char* msgerr)
{
	if (pClient == NULL)
	{
		sprintf(msgerr, "Echec: http client non �tabli");
		return false;
	}

	// http request 
	if (!RequestGPSClient(msgerr)) return false;

	// parce age: le serveur retourne au moins les informations de l'�ge
	if (!ParceKeyValueJson(AGEG, Data, msgerr))
		return false;
	ParceGPSAgeData(Data, GpsGauche.Age); // �ge gauche

	if (!ParceKeyValueJson(AGED, Data, msgerr))
		return false;
	ParceGPSAgeData(Data, GpsDroite.Age); // �ge droite


	// parce latitude 
	if (ParceKeyValueJson(LATG, Data, msgerr))
		GpsGauche.LatDeg = Data.as_double(); // latitude gauche
	else
		GpsGauche.LatDeg = -1;

	if (ParceKeyValueJson(LATD, Data, msgerr))
		GpsDroite.LatDeg = Data.as_double(); // latitude droite
	else
		GpsDroite.LatDeg = -1;


	// parce longitude
	if (ParceKeyValueJson(LONG, Data, msgerr))
		GpsGauche.LonDeg = Data.as_double(); // longitude gauche
	else
		GpsGauche.LonDeg = -1;

	if (ParceKeyValueJson(LOND, Data, msgerr))
		GpsDroite.LonDeg = Data.as_double(); // longitude droite
	else
		GpsDroite.LonDeg = -1;
	

	// parce rtk fix
	if (ParceKeyValueJson(RTKG, Data, msgerr))
		ParceGPSRTKData(Data, GpsGauche.Fix); // rtk gauche
	else
		GpsGauche.Fix = -1;

	if (ParceKeyValueJson(RTKD, Data, msgerr))
		ParceGPSRTKData(Data, GpsDroite.Fix); // rtk droite
	else
		GpsDroite.Fix = -1;

	return true;
}
