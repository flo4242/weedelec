#include <fstream>
#include "TrackPointGPX.h"

bool ExportGPX(char *Nf, char* NomTrk, _list<TrackPoint> Liste, char* msgerr)
{
	std::ofstream os(Nf);
	if (!os.good())
	{
		sprintf(msgerr, "Echec ouverture %s", Nf);
		return false;
	}

	os << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << '\n';
	os << "<gpx creator = \"Wikiloc - http://www.wikiloc.com\" version = \"1.1\" xmlns=\"http://www.topografix.com/GPX/1/1\">" << '\n'; 

	os << "<trk>" << '\n';
	os << "<name>" << NomTrk << "</name>" << '\n';
	os << "<trkseg>" << '\n';
	
	os.precision(16);
	
	BalayeListe(Liste)
	{
		TrackPoint &TrkPt = *Liste.current();
		char date[100];

		bool outputTime = true;
		if (TrkPt.STime.wYear <= 1970 || TrkPt.STime.wYear > 2050)
			outputTime = false;
		else if (TrkPt.STime.wMonth <= 0 || TrkPt.STime.wMonth > 12)
			outputTime = false;
		else if (TrkPt.STime.wDay <= 0 || TrkPt.STime.wMonth > 31)
			outputTime = false;
		else if (TrkPt.STime.wHour < 0 || TrkPt.STime.wMonth > 24)
			outputTime = false;
		else if (TrkPt.STime.wMinute < 0 || TrkPt.STime.wMonth > 60)
			outputTime = false;
		else if (TrkPt.STime.wSecond < 0 || TrkPt.STime.wMonth > 60)
			outputTime = false;
		
		if(outputTime)
			sprintf(date, "%04u-%02u-%02uT%02u:%02u:%02uZ", TrkPt.STime.wYear, TrkPt.STime.wMonth, TrkPt.STime.wDay,
				TrkPt.STime.wHour, TrkPt.STime.wMinute, TrkPt.STime.wSecond);

		os << "<trkpt lat=\"" << TrkPt.LatDeg << "\" lon=\"" << TrkPt.LonDeg << "\">" << '\n';		
		if (outputTime) os << "<time>" << date << "</time>" << '\n';

		os << "<extensions>" << '\n';
		os << "<GroundSpeed>" << TrkPt.GroundSpeed << "</GroundSpeed>" << '\n';
		os << "<heading>" << TrkPt.HeadingDeg << "</heading>" << '\n';
		os << "</extensions>" << '\n';

		os << "</trkpt>" << '\n';
	}

	os << "</trkseg>" << '\n';
	os << "</trk>" << '\n';
	os << "</gpx>" << '\n';

	return true;
}


