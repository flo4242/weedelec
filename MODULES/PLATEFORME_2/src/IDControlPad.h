﻿//{{NO_DEPENDENCIES}}
// fichier Include Microsoft Visual C++.
// Utilisé par DialogInterface.rc
//
#define IDCONTROL                       101
#define ID_HWCMD                        102
#define ID_HauteTension                 105
#define IDC_BUTTON1                     1001
#define IDC_BUTTON_ON                   1001
#define IDC_BUTTON2                     1002
#define IDC_BUTTON_OFF                  1002
#define IDC_BUTTON3                     1003
#define IDC_BUTTON_HOME                 1003
#define IDC_BUTTON4                     1004
#define IDC_BUTTON_RESET                1004
#define IDC_BUTTON5                     1005
#define IDC_CHECK_LEFT                  1005
#define IDC_CHECK_RIGHT                 1006
#define IDC_BUTTON_CMD_LEFT             1007
#define IDC_EDIT_POSX                   1008
#define IDC_BUTTON6                     1010
#define IDC_BUTTON_CMD_RIGHT            1010
#define IDC_BUTTON_BRASDOWN             1010
#define IDC_EDIT_POSY                   1011
#define IDC_STATIC_TXT                  1012
#define IDC_EDIT_POSZ                   1013
#define IDC_CHECK_POSX                  1014
#define IDC_CHECK_POSY                  1015
#define IDC_CHECK_POSZ                  1016
#define IDC_BUTTON_XPLUS                1017
#define IDC_BUTTON_YPLUS                1018
#define IDC_STATIC_TXT2                 1018
#define IDC_STATIC_TXT_2                1018
#define IDC_BUTTON_YMOINS               1019
#define IDC_BUTTON_XMOINS               1020
#define IDC_BUTTON_BRASUP               1021
#define IDC_BUTTON_CLOSE                1022
#define IDC_BUTTON_LANCEHT              1023
#define IDC_BUTTON_ZPLUS                1024
#define IDC_BUTTON_ZMOINS               1025

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        107
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1026
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
