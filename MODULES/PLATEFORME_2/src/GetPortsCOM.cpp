//#include <Winreg.h>
#include "GetPortsCOM.h"

#include "WINUTIL2.h"
#include "GetListePortsCOM.h"
#include "GetListeFichiers.h"


char* TParamPortCom::GetPort(char *DescriptionPort)
/******************************************/
{
	int l = strlen(DescriptionPort);
	for (int i = 0; i < l - 1; i++)
		if (DescriptionPort[i] == '\t') return DescriptionPort + i + 1;

	return NULL;
}

bool GereGroupePortCOM(HWND hParent, TParamPortCom &param, TNomFic NfConfig, bool charge)
/****************************************************************/
{
	//r�cup�ration de la liste des ports disponibles
	_list<TNf> Liste;
	bool ret = GetListePortsCOM(Liste);

	if (Liste.nbElem == 0 || !ret)
	{
		::MessageBox(hParent, "Pas de port COM pr�sent", "", MB_OK);
		return false;

	}

	//cr�ation de la liste multichoix
	AligneDescriptions(Liste);

	char** rubriques = new char*[Liste.nbElem + 1];
	int index = 0;
	BalayeListe(Liste)
	{
		rubriques[index++] = *Liste.current();
	}
	rubriques[index] = NULL;


	int IndexChoix = -1;

	int IndexBaud = 4;

	char *rubriquesBaud[11];
	rubriquesBaud[0] = "4800"; rubriquesBaud[1] = "9600";
	rubriquesBaud[2] = "14400"; rubriquesBaud[3] = "19200";
	rubriquesBaud[4] = "38400"; rubriquesBaud[5] = "56000";
	rubriquesBaud[6] = "57600"; rubriquesBaud[7] = "115200";
	rubriquesBaud[8] = "128000"; rubriquesBaud[9] = "256000";
	rubriquesBaud[10] = NULL;

	GROUPE G(hParent, "Configuration de Communication Port S�rie", NfConfig,
		MULCHOIX("Choix port COM USB", IndexChoix, rubriques),
		MULCHOIX("Choix port COM Baudrate", IndexBaud, rubriquesBaud),
		NULL);

	if (NfConfig == NULL)
		charge = false;

	bool Modif = (charge ? G.Charge() : G.Gere());

	if (!Modif) { delete rubriques; return false; }

	if (IndexChoix != -1)
	{
		char *ChoixCOM;
		ChoixCOM = Liste[IndexChoix].obj;
		strcpy(param.DescriptionPort, param.GetPort(ChoixCOM));
	}
	else
	{
		delete rubriques; return false;
	}


	switch (IndexBaud)  // choix baudrate
	{
	case 0:
		param.Baud = PortCOMBaudrate::EBaud4800;
		break;
	case 1:
		param.Baud = PortCOMBaudrate::EBaud9600;
		break;
	case 2:
		param.Baud = PortCOMBaudrate::EBaud14400;
		break;
	case 3:
		param.Baud = PortCOMBaudrate::EBaud19200;
		break;
	case 4:
		param.Baud = PortCOMBaudrate::EBaud38400;
		break;
	case 5:
		param.Baud = PortCOMBaudrate::EBaud56000;
		break;
	case 6:
		param.Baud = PortCOMBaudrate::EBaud57600;
		break;
	case 7:
		param.Baud = PortCOMBaudrate::EBaud115200;
		break;
	case 8:
		param.Baud = PortCOMBaudrate::EBaud128000;
		break;
	case 9:
		param.Baud = PortCOMBaudrate::EBaud256000;
		break;

	default:
		break;
	}

	delete rubriques;
	return true;
}



/*
bool GereGroupeConfigPort(HWND hParent, TParamComGPS &param, bool charge) // ouverture de port com
/****************************************************************/
/* G�re du groupe pour le choix de port COM et l'ouverture 
{
	//r�cup�ration de la liste des ports disponibles
	_list<TNf> Liste;
	GetListePortsCOM(Liste);

	if (Liste.nbElem == 0)
	{
		::MessageBox(hParent, "Pas de port COM pr�sent", "", MB_OK);
		return false;

	}

	//cr�ation de la liste multichoix
	AligneDescriptions(Liste);

	char** rubriques = new char*[Liste.nbElem + 1];
	int index = 0;
	BalayeListe(Liste)
	{
		rubriques[index++] = *Liste.current();
	}
	rubriques[index] = NULL;

	int IndexChoix = -1;

	GROUPE G(hParent, "Configuration Communication Serielle Port", "ComPortHTWeedelec.cfg",
		MULCHOIX("Choix port COM USB pour GPS externe", IndexChoix, rubriques),
		NULL);

	bool Modif = (charge ? G.Charge() : G.Gere());

	if (!Modif) { delete rubriques; return false; }


	char *ChoixCOM = "";

	if (IndexChoix != -1)
	{
		//on retrouve la description choisie
		int cnt = 0;
		BalayeListe(Liste)
		{
			if (cnt == IndexChoix)
			{
				ChoixCOM = *Liste.current();
				break;
			}
			cnt++;
		}
	}
	//fermeture �ventuelle du port pr�c�dent
	
	if (param.HCom != NULL)
	{
		CloseHandle(param.HCom);
		param.HCom = NULL;
	}

	//ouverture du nouveau
	param.HCom = InitComAsync(param.GetPort(ChoixCOM));

	if (param.HCom) // retourne de charat�res du port Com : "COMx"
	{
		char *temp = ChoixCOM;
		while(true)
		{
			if(strncmp(temp, "COMx", 3) == 0) break;
			temp++;
		}
		strcpy(param.DescriptionPort, temp);
	}
	else
	{
		char st[100];
		sprintf(st, "Echec ouverture %s", GetPort(ChoixCOM));
		::MessageBox(hParent, st, "Port NMEA", MB_OK & MB_ICONERROR);
		strcpy(param.DescriptionPort, "");
		IndexChoix = -1;
		G.Sauve();
	}
	

	delete rubriques;
	return true;
}*/