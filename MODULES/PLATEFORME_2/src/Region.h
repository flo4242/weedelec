/** @file Regions.h
 *  D�claration de la classe Region
 */
#ifndef REGIONS__H
#define REGIONS__H


/* NOTION GENERIQUE DE REGION EN TANT QUE LISTE DE SEGMENTS, INDEPENDANTE DE TOUTE NOTION D'IMAGE

                    d'apr�s Gilles RABATEL - Philippe THEVENY - Juin 2007   
					
					D�pendances: ligne.h ligne.cpp list_tpl.h Region.cpp								*/


/////////////////////////////////////////////////////////////////////////////////////////////////
// IMPORTANT: l'ensemble des notions de r�gion et de contour de cet outil s'entend en connexit� 4
/////////////////////////////////////////////////////////////////////////////////////////////////

#include <iostream>
using namespace std;

#include "list_tpl.h"

#include "ligne.h"

/************************************************************/
/*               STRUCTURES ELEMENTAIRES                    */
/************************************************************/


/** @class Region
 * Classe utilitaire.
 * Contient les donn�es g�om�triques d'une r�gion dans une image.
 * @note Cette structure est � remplir directement par l'utilisateur, elle ne
 * poss�de que les m�thodes n�cessaires pour l'enregistrer dans un conteneur
 * (_list<> ou autre).

 *Elle peut constituer la class m�re de classes plus complexes dot�es d'attributs de valeurs
 *
 * @note La r�gion est repr�sent�e sous forme d'une liste de segments horizontaux d�finis
 * par leurs extremit�s [BorneGauche, BorneDroite] inclues. C'est une repr�sentation
 * tr�s int�ressante en terme d'occupation m�moire (cf. sauvegarde fichier), qui constituera
 * l'�l�ment de base de la d�finition des TEnsembleRegions.
 *
 * @note Pour l'enregistrement/lecture dans un fichier des sous-classes, penser � red�finir
 * les op�rateurs << et >>.
 */




class _EXPORT_ Region
{
public:
    unsigned long int NbPixels;
    _list<SEGMENT> Segments;
    int xMin;
    int xMax;
    int yMin;
    int yMax;

    Region();
	Region(Region&);
	Region(RRECT &R);          //construction � partir d'un rectangle
	//NB: R.right et R.bottom non inclus

	Region(LIGNE &L);          //construction � partir d'une ligne continue et ferm�e
	Region::Region(POINTINT centre, int rayon); //construction d'une r�gion circulaire

	virtual ~Region() {;}

    /** Op�rateur de copie. N�cessaire � cause du tableau CouleurMoyenne. */
    virtual Region& operator =(Region&);

	/** op�rateur de comparaison pour les recherches dans un conteneur */
	//La comparaison s'effectue uniquement sur des crit�res g�om�triques
    virtual bool operator ==(Region&);

	/** tests d'inclusion */
	bool ContientPoint(int x, int y);
	bool ContientSegment(SEGMENT &S);
	bool ContientRegion(Region &R, bool testinclusiontotale=false);
	/* flag testinclusiontotale: indique s'il faut tester l'ensemble
	des segments de R ou si le test du premier segment suffit
	(e.g. si les r�gions sont issues du traitement d'une image binaire)*/
	/* ATTENTION: ne constitue pas un test d'inclusion pour les r�gions ayant des trous, car le test se fait
	entre segments seulement. Pour ce type d'utilisation, il faut d'abord remplir la r�gion avant de tester */

	
	bool IntersectionSegment(SEGMENT &S, _list<SEGMENT> *intersection = NULL,	_list<SEGMENT> *intersectant = NULL);
	/*indique si S a une intersection avec la r�gion.
	Si l'intersection existe:
		- si param�tre intersection sp�cifi�, contient en retour les intersections trouv�es
		- si param�tre intersectant sp�cifi�, contient en retour les segments de la r�gion
		avec lesquels il y a intersection */


	bool IntersectionRegion(Region &R, Region *inter = NULL);
	/*indique si R a une intersection avec la r�gion. Si l'intersection existe
	et si inter sp�cifi�, inter contient en retour l'intersection trouv�e.
	IMPORTANT: LE CHAMP inter->CouleurMoyenne N'EST PAS RENSEIGNE *****/

	void UnionRegion(Region &Rs);

    /** Pour utilisations � fa�on (ex; colorisation minimale) */
	int Label;

		  
};





/** Op�rateur << pour sortie sur un flux */
_EXPORT_ ostream& operator <<(ostream&, Region&);

/** Op�rateur >> pour lecture depuis un flux */
_EXPORT_ istream& operator >>(istream&, Region&);


/********************** NOTIONS D'ADJACENCE ENTRE REGIONS **************************

REMARQUE IMPORTANTE: dans tout ce qui suit, la notion de point fronti�re (listes de points ou objets LIGNE)
ne repr�sente pas un pixel de l'image, mais un "coin" de pixel. C'est la diff�rence avec la notion de contour
d�finie dans binaire.h, qui indique quant � elle les pixels de contours *internes* � une r�gion consid�r�e.

Ici, la d�finition de la fronti�re ne doit pas privil�gier une des deux r�gions qu'elle s�pare: la suite
de points fronti�re dessine une suite d'ar�tes �l�mentaires constitu�es de bords de pixels.

Par convention, un jeu de coordonn�es (x,y) repr�sente le coin haut-gauche du pixel de m�mes coordonn�es.


CONSEQUENCE:
Les LIGNEs des structures TFrontiereTPL peuvent contenir des coordonn�es de points qui sont sur le bord
(exclu) droit ou bas de l'image. Si on trace graphiquement les points fronti�res, il faut donc faire un test
pr�alable d'appartenance � l'image. Ce test est d�j� int�gr� dans la fonction TraceLigne, qui est donc recommand�e.

************************************************************************************/




#endif

