#include "ControlRobot.h" 
// il faut toujours inclure "ControlRobot.h" en premier pour que "erxCom.h" soit d�clar� en premier 
// (sinon incompatibilit� se trouve pour le module 'chrono') 

#include "CommandeNavigation_2.h"
#include "ExportModulePlateforme_2.h"
#include "Plateforme_2.h"

#include "drvtraiti.h"


#define GAIN_ORIENT  0.4  // gain de commande pour l'orientation
#define RAYON_RATTRAPAGE  2.0  // rayon de rattrapage en m�tres pour le suivi chemin
#define THETA_CST  45.0*(M_PI/180)  // constante pour la fonction exponentielle : v(err_theta) = Vmax * exp{-err_theta / THETA_CST}


extern TConsigneMoteurs ConsigneMoteursG; // structure globale pour la consigne moteurs
extern TControlRobot ControlRobotG; // structure globale pour appeler l'API Ecorobotix
extern TRepereLocal* RepereG;

extern bool StopUrgenceG;  // arr�t d'urgence
extern int TimeDebutG; // temps r�el pour la sauvegarde de fichier log
 
TCommandeNavigation *CommandeNavigationG = NULL;



//////////////////////////////////////////////
/*******fonctions externes utilis�es*********/

// fonction retour Plateforme error
void RetourPlateformeError(TRetourPlateforme &ret, int timedebut, char* infoRet)
{
	ret.State = TRetourState::Error;
	ret.InfoRetour = infoRet;
	ret.DureeMs = GetTickCount() - timedebut;
}

// fonction sauvegarde fichier log
void SauveFichierLog(ofstream *pOs, double Ts, POINTFLT CMoteurs, TPositionRobot Pos)
// format de fichier log: 
// XX.X (temps)	Consigne moteurs	Position robot (X, Y, cap)	Coordonn�es GPS
{
	char temp[100];
	double LatMes, LonMes;  // latitude et longitude mesur�es

	if (RepereG == NULL) return;

	RepereG->MetresVersWGS84(Pos.x, Pos.y, LatMes, LonMes);

	sprintf(temp, "Module Plateforme_2 Log:\t%.2f\t%.2f\t%.2f\t%.3f\t%.3f\t%.3f\t%.16f\t%.16f\n", 
		Ts, CMoteurs.x, CMoteurs.y, Pos.x, Pos.y, Pos.CapDeg, LatMes, LonMes);
	*pOs << temp;
}


/////////////////////////////////////////
/********* Fonction callback ***********/
auto callback_commande = new call<bool>([](bool)
// �tabli la fonction callback pour la commande
{
	/* cette fonction timer maintient l'envoi r�gulier des consignes moteur d'avancement (sinon les
	 moteurs s'arr�tent).
	 En mode auto, elle g�re aussi le d�tail du rattrapage de trajectoire
	 */

	if (CommandeNavigationG->CommandeManuelle)
	// si commande manuelle, les consignes de moteurs sont donn�es par le panneau contr�le
	{
		TCommandeManuelle *pComManuelle = dynamic_cast <TCommandeManuelle*> (CommandeNavigationG);
		if (pComManuelle == NULL) // v�rification de dynamic_cast de pointeur
		{
			CommandeNavigationG->StopTimer();
			return;
		}

		POINTFLT ComMot = pComManuelle->GetVitesseCommandeManuelle();
		ControlRobotG.SetRobotSpeed(ComMot.x, ComMot.y); // envoi de consigne moteurs
		ConsigneMoteursG.UpdateConsigneCourante(ComMot.x, ComMot.y);

		if (CommandeNavigationG->pFichierLog != NULL)
		// sauvegarde ficheir log
		{
			char msgerr[1000];
			TPositionRobot posRobot;
			if (!GetRobotPositionEstimated(posRobot, msgerr))
			{
				delete CommandeNavigationG->pFichierLog; CommandeNavigationG->pFichierLog = NULL;
			}
			else
				SauveFichierLog(CommandeNavigationG->pFichierLog,
					double(GetTickCount() - TimeDebutG) / 1000, ComMot, posRobot);
		}	
	}

	else // commande automatique de suivi de chemin
	{
		int TimeDebut = GetTickCount();

		TCommandeAutomatique *pComAuto = dynamic_cast <TCommandeAutomatique*> (CommandeNavigationG);
		
		TRetourPlateforme ret;
		ret.CropRowFinished = false;

		if (pComAuto == NULL) // v�rification de dynamic_cast de pointeur
		{
			CommandeNavigationG->StopTimer();
			
			RetourPlateformeError(ret, TimeDebut, "Pointeur commande non conforme!");
			if(pComAuto->callbackRetour != NULL)	 pComAuto->callbackRetour(ret);
			return;
		}

		// mise � jour du segment courant de trajectoire
		int indexActuel = pComAuto->indexSegmentTraj;
		POINTFLT P1 = pComAuto->TrajDesire[indexActuel].obj;
		POINTFLT P2 = pComAuto->TrajDesire[indexActuel + 1].obj;

		// r�cup�ration de mesures actuelles corrig�es par le gps externe, 
		// stock�e dans la variable PosActuelle
		char msgerr[1000];

		if (!GetRobotPositionEstimated(pComAuto->PosActuelle, msgerr))
		{
			CommandeNavigationG->StopTimer();
			RetourPlateformeError(ret, TimeDebut, msgerr);
			if (pComAuto->callbackRetour != NULL)	 pComAuto->callbackRetour(ret);
			return;
		}

		POINTFLT Pc(pComAuto->PosActuelle.x, pComAuto->PosActuelle.y); // point actuel

		// calcul du cap d�sir� � l'instant, le r�sultat de cap d�sir� sera stock� dans la variable PosDesiree.CapDeg
		pComAuto->CalculCapDesire(P1, P2, Pc);

		POINTFLT ComMot = pComAuto->CalculConsignesMoteurs();

		ControlRobotG.SetRobotSpeed(ComMot.x, ComMot.y); // envoie la consigne de moteurs
		ConsigneMoteursG.UpdateConsigneCourante(ComMot.x, ComMot.y); // mise � jour de consignes moteurs pour le filtre Kalman
		
		//on a maintenant �tabli le cap et la vitesse en fonction du segment de trajectoire � rattraper

		// on va enregistrer les consignes et la position robot dans le ficheir log
		if (pComAuto->pFichierLog != NULL)
		// sauvegarde ficheir log
			SauveFichierLog(pComAuto->pFichierLog, double(GetTickCount() - TimeDebutG) / 1000,
				ComMot, pComAuto->PosActuelle);


		//on va maintenant tester les conditions d'arr�t
		POINTFLT PtDebutDistance;
		double DistanceMax;
		TSubStepLocal *TS = pComAuto->Substep;

		if (TS != NULL) // si le substep pour aller � la position suivante de d�sherbage est d�fini
		// on v�rifie que le dernier substep ne d�passe pas la fin du segment courant
		{
			DistanceMax = TS->DistanceMax; // ici, distance max correspond au pas d'avancement pour le weeding

			if (DistanceMax*DistanceMax >= (P2 - Pc).Norme2())
				// si le dernier substep d�passe la fin du segment de trajectoire
				// --> on se replace dans le cas sans substep
			{
				TS = NULL;
			}
		}


		if (TS != NULL)
		// si on avanve � la position suivant par un step
		// --> on reinitialise le point de d�but par le point o� l'on d�part de derni�re position de d�sherbage
		{
			PtDebutDistance = TS->Debut;
		}

		else  // sinon le point de d�but est le P1, et distance max est remise comme la distance de P1P2 
		{
			PtDebutDistance = P1;
			DistanceMax = (P2-P1).Norme();
		}
		

		if(TestArret(PtDebutDistance, Pc, DistanceMax) || StopUrgenceG) // test d'arr�t
		{
			if (StopUrgenceG == true)  // v�rification de l'arr�t d'urgence
			// pour un arr�t d'urgence, stop robot et retour direct
			{
				pComAuto->StopTimer(); //arr�t du timer
				ControlRobotG.SetRobotSpeed(0, 0);

				ret.State = TRetourState::ExternalStop;
				ret.DureeMs = GetTickCount() - TimeDebut;
				ret.InfoRetour = "Stop Urgence!";
				if (pComAuto->callbackRetour != NULL)	pComAuto->callbackRetour(ret);
				return;
			}

			else
			{
				if (TS == NULL) // dans le cas o� le substep n'est pas valable, on passe le segment suivant � suivre 
				{
					// Tout premier�ment, on v�rifie qu'on n'a pas encore atteint la fin de la trajectoire
					if (pComAuto->indexSegmentTraj < pComAuto->TrajDesire.nbElem - 1)
						pComAuto->indexSegmentTraj++; //passage au segment suivant

					else
					{
						pComAuto->StopTimer(); //arr�t du timer
						ret.CropRowFinished = true;
					}
				}

				if (TS != NULL) // arr�ter le robot dans le cas o� l'on avance � step suivant (position suivante de d�sherbage)
				{
					pComAuto->StopTimer(); //arr�t du timer
				}


				ret.State = TRetourState::Completed;
				ret.DureeMs = GetTickCount() - TimeDebut;
				if (pComAuto->callbackRetour != NULL)	pComAuto->callbackRetour(ret);
				return;
			}
		}
	}
});


/////////////////////////////////////////////////////////////////////////////////////////////////////
/********************************Commande navigation - classe de base ******************************/

void TCommandeNavigation::SetTimer()
{
	int period = PeriodeCommande;  // p�riode de timer en milisecondes

	//cr�e le timer pour la commande de suivi de chemin
	Timer_Commande = new timer<bool>(period, NULL, callback_commande, true);

	StartTimer();
}

void TCommandeNavigation::StartTimer()
// start timer
{
	if (Timer_Commande != NULL)		Timer_Commande->start();
}

void TCommandeNavigation::StopTimer()
{
	if (Timer_Commande != NULL)		Timer_Commande->pause();
}





//////////////////////////////////////////////////////////////////////////////////
/********************************Commande manuelle ******************************/

bool TCommandeManuelle::DoStartCommande(char *msgerr)
{
	if (Timer_Commande)
	{
		delete Timer_Commande;
		Timer_Commande = NULL;
	}
	// --> on va r�initialiser le timer dans le redemarrage de mouvement du robot

	SetTimer();
	
	if (Timer_Commande == NULL)
	{
		sprintf(msgerr, "Set Timer �chou�");
		return false;
	}

	return true;
}

void TCommandeManuelle::VitesseNominaleAdd()
{
	if (SetValeurValide) VitesseNominale += UNITE_VITESSE;

	if (abs(VitesseNominale) > SEUIL_VITESSE) // contrainte sur la vitesse nominale
		VitesseNominale = SEUIL_VITESSE; // vitesse d'avancement max si le robot ne tourne pas
}

void TCommandeManuelle::VitesseNominaleSub()
{
	if (SetValeurValide) VitesseNominale -= UNITE_VITESSE;
	
	if (abs(VitesseNominale) > SEUIL_VITESSE)
		VitesseNominale = -SEUIL_VITESSE;
}

void TCommandeManuelle::VitesseDiffAdd() 
{ 
	if (SetValeurValide) VitesseDifferentielle += UNITE_VITESSE; 

	if (abs(VitesseDifferentielle) > SEUIL_VITESSE) // contrainte sur la vitesse diff�rentielle
		VitesseDifferentielle = SEUIL_VITESSE; // vitesse diff�rentielle max si le robot tourne sur place
}

void TCommandeManuelle::VitesseDiffSub()
{
	if (SetValeurValide) VitesseDifferentielle -= UNITE_VITESSE;

	if (abs(VitesseDifferentielle) > SEUIL_VITESSE)
		VitesseDifferentielle = -SEUIL_VITESSE;
}


POINTFLT TCommandeManuelle::GetVitesseCommandeManuelle()
{
	SetValeurValide = false;
	double vg = VitesseNominale - VitesseDifferentielle;
	double vd = VitesseNominale + VitesseDifferentielle;
	SetValeurValide = true;

	// mis en place du seuil de vitesse
	if (abs(vg) > SEUIL_VITESSE) vg = (vg / abs(vg))* SEUIL_VITESSE;
	if (abs(vd) > SEUIL_VITESSE) vd = (vd / abs(vd))* SEUIL_VITESSE;
	return POINTFLT(vg, vd);
}






/////////////////////////////////////////////////////////////////////////////////////////////////////
/********************************Commande Automatique de navigation*********************************/

// lance la commande automatique


bool TCommandeAutomatique::DoStartCommande(char* msgerr)
{
	if (TrajDesire.nbElem < 2 || TrajDesire.nbElem - 2 < indexSegmentTraj) 
	// au moins 2 points restant dans la trajectoire	
	{
		sprintf(msgerr, "Pas assez de points restant � suivre");
		return false;
	}

	if (Timer_Commande) 
	{
		delete Timer_Commande;
		Timer_Commande = NULL;
	}
	// --> on va r�initialiser le timer dans le redemarrage de mouvement du robot

	SetTimer();

	return true;
}

bool TCommandeAutomatique::RestartCommande(char* msgerr)
{
	indexSegmentTraj = 0; // reset index pour recommencer depuis le premier point
	return DoStartCommande(msgerr);
}



/*********************Suivi de chemin effectu� par un suivi de cap d�sir�***************************/

// v�rification de la distance d'avancement du robot par rapport au point pr�c�dent
bool TestArret(POINTFLT Ptprecedent, POINTFLT Ptcourant, double distancemax)
{
	return ((Ptcourant - Ptprecedent).Norme2() >= distancemax*distancemax);
	//permet au robot d'aller jusqu'au bout (si pas d'erreur de cap et pas de rattrapage)
}


// commande cin�matique sur le suivi de l'orientation
double GetVitesseLineaireSelonErreurCap(double ErrCapRad, double vitessenominale)
// Calculer la vitesse d'avanc�e � partir de l'erreur de l'orientation d�sir�e ()
// Fonction potentielle : y(theta) = V*exp{-theta/theta_c} ;
{
	return vitessenominale * exp(-ErrCapRad / THETA_CST);
}

POINTFLT TCommandeAutomatique::CalculConsignesMoteurs()
// Commande cap : 1/E*(vD - vG) = K*(theta_desire - theta_actuel)
// Commande vitesse d'avancement : V = 1/2*(vG + vD)  -> constante(pourrait aussi r�gler en fonction de l'erreur de suivi de cap)
// Consigne de vitesse : vG = V - E/2*K*(theta_desire - theta_actuel), vD = V + E/2*K*(theta_desire - theta_actuel)
{
	double ErrOrient = (PosDesiree.CapDeg - PosActuelle.CapDeg)*M_PI/180;

	if (abs(ErrOrient + 2 * M_PI) < abs(ErrOrient)) //v�rification de la zone de discontinuit�
		ErrOrient += 2 * M_PI;
	else if (abs(ErrOrient - 2 * M_PI) < abs(ErrOrient))
		ErrOrient -= 2 * M_PI;

	double consignV_theta = ENTRAXE_ROBOT / 2 * GAIN_ORIENT * ErrOrient;

	double V = GetVitesseLineaireSelonErreurCap(abs(ErrOrient), vitesseNominale);

	double vg, vd;

	vg = V - consignV_theta;
	vd = V + consignV_theta;

	// mis en place du seuil de vitesse
	if (abs(vg) > SEUIL_VITESSE) vg = (vg / abs(vg))* SEUIL_VITESSE;
	if (abs(vd) > SEUIL_VITESSE) vd = (vd / abs(vd))* SEUIL_VITESSE;

	return POINTFLT(vg, vd);
}



void TCommandeAutomatique::CalculCapDesire(POINTFLT P1, POINTFLT P2, POINTFLT PC)
// calcule le cap d�sir� � partir du segment de chemin actuel(d�fini par P1, P2) et du point actuel
{

	POINTFLT U = (P2 - P1) / (P2 - P1).Norme();
	POINTFLT P1C = PC - P1;
	POINTFLT PH = P1 + U * (P1C | U);

	double dist = (PC - PH).Norme();
	
	POINTFLT PCible;

	double longueur = RAYON_RATTRAPAGE * RAYON_RATTRAPAGE - dist * dist;

	if (longueur <= 0)
		PCible = PH;
	else
		PCible = PH + U * sqrt(longueur);

	PosDesiree.x = PCible.x;
	PosDesiree.y = PCible.y;
	PosDesiree.CapDeg = atan2(PC.x - PCible.x, PCible.y - PC.y) * 180 / M_PI;
}
