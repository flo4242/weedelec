#pragma once
//INCOMPATIBILITE CPPREST:boost
#undef U
#include "list_tpl.h"
#include "GereWeeds.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <set>
#include <ctime>
#include <boost/assert.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/random.hpp>
#include <boost/timer.hpp>
#include <boost/integer_traits.hpp>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/simple_point.hpp>
#include <boost/graph/metric_tsp_approx.hpp>
#include <boost/graph/graphviz.hpp>


//Fonctions utilis�es pour ordonner les points de d�hserbage selon le chemin le plus court
//On utilise la librairie boost et son support des graphes non orient�s pour cela

_list<TWeedBras> cheminWeedsAtteignables(_list<TWeedBras>&);
_list<TWeedBras*> cheminWeedsAtteignables(_list<TWeedBras*>&);





//Typedefs pour simplifier l'�criture avec boost
typedef std::vector< boost::simple_point<double> > PositionVec;
typedef boost::adjacency_matrix<boost::undirectedS, boost::no_property,boost::property <boost::edge_weight_t, double> > Graph;
typedef boost::graph_traits<Graph>::vertex_descriptor Vertex;
typedef boost::property_map<Graph, boost::edge_weight_t>::type WeightMap;
typedef boost::property_map<Graph, boost::vertex_index_t>::type VertexMap;
//typedef std::vector< Vertex > Container;

Graph buildBoostGraphe(std::vector<TWeedBras>);
Graph buildBoostGraphe(std::vector<TWeedBras*>);

std::vector<TWeedBras> boostGetShortestPath(Graph, std::vector<TWeedBras>);
std::vector<TWeedBras*> boostGetShortestPath(Graph, std::vector<TWeedBras*>);

//Fonctions template g�n�riques de conversion d'un _list en vector et inversement
template <class myType>
std::vector<myType> listToVector(_list<myType> listWeeds) {
	std::vector<myType> vectorWeeds;
	BalayeListe(listWeeds) {
		myType curWeed = *listWeeds.current();
		vectorWeeds.push_back(curWeed);
	}
	return vectorWeeds;
}

template <class myType>
_list<myType> VectorToList(std::vector<myType> vectorWeeds) {
	_list<myType> listWeeds;
	for (std::vector<myType>::iterator it = vectorWeeds.begin(); it != vectorWeeds.end(); ++it) {
		listWeeds.push_back(*it);
	}
	return listWeeds;
}