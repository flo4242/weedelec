


#include "ControlRobot.h"
// il faut toujours inclure "ControlRobot.h" en premier pour que "erxCom.h" soit d�clar� en premier 
// (sinon incompatibilit� dans le module 'chrono') 

#include "ExportModuleControleRobot.h"
#include "Plateforme_2.h"
#include "cheminWeeds.h"
#include "CommandeNavigation_2.h"
#include "KML.h"
#include "DialogueHauteTension.h"

#include <Windows.h>
#include <ppltasks.h> // pour multitasking
#include <agents.h> // pour timer

#include "matrice.h"
#include "DBOUT.h"


using namespace Concurrency;

#define TIMEOUT_STEP  10000	// time-out p�riode pour avancer � la position suivante
#define DELAY_INITARM  3000  // delay pour l'initialisation de bras
#define DELAY_RESETARM  1500  // delay pour le reset de bras (� la position d'origine)


#define VARIATION_POSROBOT  0.002    // variation de mesures position robot inf�rieure � 0.002m / SLEEP_POSROBOT
#define SLEEP_POSROBOT      1000     // delay pour �tre s�r que les mesures ne changent plus apr�s l'arr�t du robot


extern TParamRobot ParamG;
extern TControlRobot ControlRobotG;
extern CommandeHT CommandeHTG;
extern TControlBrasManuel ControlBrasManuelG;
extern TRepereLocal *RepereG;
extern TCommandeNavigation *CommandeNavigationG;
extern Hexagone *pHexagoneBras;


bool StopUrgenceG = false;


bool TestArretAvancement(POINTFLT Debut, POINTFLT PosActuelle, double distance,
	int TimeDebut, TRetourState &state)
{
	double dist2 = (PosActuelle - Debut).Norme2();

	if (dist2 > distance*distance) // v�rification d'arriv�e de point suivant
	{
		state = TRetourState::Completed;
		return true;
	}

	if (GetTickCount() - TimeDebut > TIMEOUT_STEP)
	{
		state = TRetourState::TimeOut;
		return true;
	}

	if (StopUrgenceG)
	{
		state = TRetourState::ExternalStop;
		return true;
	}

	return false;
}

TStateInitPlateforme TModulePlateforme::CheckInitialisationPlateforme()
{
	return CheckInitPlateforme(); //plateforme_2.cpp
}

TRepereLocal* TModulePlateforme::GetRepereLocal()
{
	return RepereG;
}


Trajectoire * TModulePlateforme::InitTrajectoire(char *msgerr)
{
	TPositionRobot posRobot;
	if (!PositionRobotEstimee(posRobot, msgerr))	return NULL;

	Trajectoire *T = new Trajectoire();

	if (ParamG.TrajectoireKML) 
	{
		if (!T->Init(posRobot, ParamG.NfTrajectoire, RepereG, msgerr))		{			delete T; return NULL;		}
		else return T;
	}

	else //trajectoire tout droit
	{
		if (!T->Init(posRobot, ParamG.DistanceToutDroit, msgerr)) { delete T; return NULL; }
		else return T;

		//NB: CAP  IMPOSE SI MODE GPS AVEC UN SEUL FIXE !!!!!!!!!!!!!!!!!
		// voir trajectoire.cpp
		return T;
	}

}


void RetourneStateError(TRetourPlateforme &ret, int timedebut, char* infoRet)
{
	ret.State = TRetourState::Error;
	ret.InfoRetour = infoRet;
	ret.DureeMs = GetTickCount() - timedebut;
}


bool TModulePlateforme::GetPositionRobot(double &LatDeg, double &LonDeg, double &CapDeg, char *msgerr)
{
	TPositionRobot Pos;
	if (!PositionRobotEstimee(Pos, msgerr))	return false;
	RepereG->MetresVersWGS84(LatDeg, LonDeg, Pos.x, Pos.y);
	CapDeg = Pos.CapDeg;
	return true;
}

bool TModulePlateforme::GetPositionRobot(TPositionRobot &Pos, char *msgerr)
{
	return PositionRobotEstimee(Pos, msgerr);
}

void TModulePlateforme::GoToNextStep(Trajectoire *ptrajectoire, double step, pcallbackStep cb)
{
	int TimeDebut = GetTickCount();
	TRetourPlateforme ret;
	ret.CropRowFinished = false;

	StopUrgenceG = false;

	char msgerr[1000];
	
	//on r�initialise tout � chaque step !
	if (CommandeNavigationG != NULL)		delete CommandeNavigationG;
	TCommandeAutomatique *pComAuto = new TCommandeAutomatique(ptrajectoire, ParamG.PeriodeCommande, ParamG.PeriodeLog, &step, cb);
	CommandeNavigationG = pComAuto;

	pComAuto->InitVitesse(ParamG.VitesseMaxLineaire); // initialise la vitesse d'avancement

	//contr�les de d�part

	if (RepereG == NULL || !ControlRobotG.RobotIsConnected())
	{
		RetourneStateError(ret, TimeDebut, "Robot non initialis�!");
		if(pComAuto->callbackRetour != NULL) pComAuto->callbackRetour(ret);
		delete CommandeNavigationG; CommandeNavigationG = NULL;
		return;
	}
		
	if(!PositionRobotEstimee(pComAuto->PosActuelle, msgerr))
	{
		RetourneStateError(ret, TimeDebut, msgerr);
		if (pComAuto->callbackRetour != NULL) pComAuto->callbackRetour(ret);
		delete CommandeNavigationG; CommandeNavigationG = NULL;
		return;
	}
		
	
	

	if (!pComAuto->DoStartCommande(msgerr))
	{
		RetourneStateError(ret, TimeDebut, msgerr);
		pComAuto->callbackRetour(ret);
		delete CommandeNavigationG; CommandeNavigationG = NULL;
		return;
	}

	
}



MATRICE ChangementRepere(double Tx, double Ty, double ThetaRad)
{
	MATRICE PR1R2(3, 3);
	// matrice pour le changement du rep�re, R1 rep�re robot, R2 rep�re bras
	PR1R2.Constante(0);

	PR1R2[0][0] = cos(ThetaRad); PR1R2[0][1] = sin(ThetaRad);
	PR1R2[1][0] = -sin(ThetaRad); PR1R2[1][1] = cos(ThetaRad);
	// matrice de rotation : [cos(theta)  sin(theta); -sin(theta)  cos(theta)]

	PR1R2[0][2] = -Tx; PR1R2[1][2] = -Ty;
	// on fait la rotation du rep�re dans un premier temps, et puis la translation
	// donc la translation suivant l'axe y est invers�e que celle dans le rep�re robot
	// Tx~= 0.475; Ty ~= -0.773m dans le rep�re robot, 
	// --> c'est-�-dire Tx ~= -0.475; Ty ~= 0.773m dans le rep�re de bras

	PR1R2[2][2] = 1;

	//PR1R2.Sauve("MatriceChangement.txt");

	return PR1R2;
}

TWeedBras TModulePlateforme::PositionRobotVersPositionBras(TWeedRobot &WR)
{
	TWeedBras WB;
	//transfert des donn�es
	TDataWeed * pdataB = &WB;
	TDataWeed * pdataR = &WR;
	*pdataB = *pdataR;

	//transfert de la position
	WB.Position.x = WR.Position.x - ParamG.TranslationDeltaX;
	WB.Position.y = -(WR.Position.y - ParamG.TranslationDeltaY);
	//Transfert du flag
	WB.state = WR.state;
	// la sens de l'axe Y du rep�re de bras est inverse que celle du rep�re robot

	return WB;
}




#ifdef SHIYU
void TModulePlateforme::Weeding(_list<POINTFLT> &weeds, TRetourPlateforme &ret, TPositionRobot posDebut, 
	bool AvecChangementRepere, bool AvecHauteTension, bool AvecRecalageManuel)
//les coordonn�es des mauvaises herbes sont fournies dans l'espace du robot au moment des prises d'image
//pr�c�dentes (� charge de la plateforme d'ajuster en fonction du d�placement effectif depuis)
{
	int TimeDebut = GetTickCount();
	ret.CropRowFinished = false;

	if (!ControlRobotG.RobotIsConnected())
	// v�rification de la connexion au robot
	{
		RetourneStateError(ret, TimeDebut, "Robot non initialis�");
		return;
	}

	if (AvecHauteTension)
	{
		if (CommandeHTG.ComHT == NULL)
		{
			RetourneStateError(ret, TimeDebut, "Module Haute Tension non initialis�");
			return;
		}
	}

	if (!ControlRobotG.ArmInitialised)
	{
		ControlRobotG.InitRightArm();
		ControlRobotG.ArmInitialised = true;
		Sleep(DELAY_INITARM);
	}
	// start inject weeds, on profite de la p�riode d'initialisation du bras 
	// pour que les mesures de position robot soit stables

	char msgerr[1000];
	TPositionRobot posArret;
	if (!PositionRobotEstimee(posArret, msgerr))
		// les mesures de position robot dans le rep�re local de sol fournies par Erx
	{
		RetourneStateError(ret, TimeDebut, msgerr);
		return;
	}
	
		
	MATCARREE TR1R2(3); TR1R2.Identite();
	POINTFLT TranslationRobot(0, 0);
	// translation pure qui correspond au mouvement du robot (distance d'avancement)

	if (AvecChangementRepere)
	{
		double Tx_sol = posArret.x - posDebut.x; // translation(mouvement du robot) dans le rep�re du sol relative du robot
		double Ty_sol = posArret.y - posDebut.y;

		double cap_rad = posDebut.CapDeg * M_PI / 180;
		TranslationRobot.x = cos(cap_rad) * Tx_sol + sin(cap_rad) * Ty_sol;
		TranslationRobot.y = -sin(cap_rad) * Tx_sol + cos(cap_rad) * Ty_sol;
		// changement dans le rep�re du robot, si le rep�re robot et rep�re sol n'est pas superpos�
		// si le cap du point de d�but = 0, les deux rep�res sont superpos�s

		double theta = (posArret.CapDeg - posDebut.CapDeg)*M_PI / 180; // �cart d'orientation entre position de d�but et position actuelle
		if (theta > M_PI) theta -= 2 * M_PI;
		if (theta < -M_PI) theta += 2 * M_PI;

		TR1R2 = ChangementRepere(ParamG.TranslationDeltaX, ParamG.TranslationDeltaY, theta);  
		// cr�ation de la matrice de changement de rep�re
		//TR1R2.Sauve("MatriceChangementRepere.txt");
	}

	//MATRICE Temp(weeds.nbElem + 2, 4); Temp.Constante(0);
	//int no_temp = 1;
	//Temp[0][0] = TranslationPure.x; Temp[0][1] = TranslationPure.y; Temp[0][2] = capDegCourant - posLocaleDebut.CapDeg;
	//Temp[1][0] = xCourant; Temp[1][1] = yCourant; Temp[1][2] = posLocaleDebut.x; Temp[1][3] = posLocaleDebut.y;

	_list<POINTFLT> weeds_NR; // coordonn�es des weeds dans le nouveau rep�re
	_list<POINTFLT> weeds_enleve; // liste de weeds qu'on va enlever, ce qui seraint trait�s dans ce cycle

	BalayeListe(weeds)
	{
		POINTFLT *weed = weeds.current();
		MATRICE VR1(3,1);

		VR1[0][0] = weed->x - TranslationRobot.x;
		VR1[1][0] = weed->y - TranslationRobot.y;
		// prise en compte le mouvement effectu� du robot, seulement si le param�tre AvecChangementRepere = true
		VR1[2][0] = 1;

		MATRICE VR2 = TR1R2 * VR1;
		VECTEUR Weed = VR2.Colonne(0);

		Weed[1] = -Weed[1];
		// la sens de l'axe Y du rep�re de bras est inverse que celle du rep�re robot

		//Temp[++no_temp][0] = weed->x; Temp[no_temp][1] = weed->y;
		//Temp[no_temp][2] = Weed[0]; Temp[no_temp][3] = Weed[1];

		if (pHexagoneBras->Contient(Weed[0], Weed[1]))   // on v�rifie si le weed se trouve dans l'espace de travail
		{
			weeds_enleve.push_back(*weed);
			weeds_NR.push_back(POINTFLT(Weed[0], Weed[1]));  
		}
		else
		{
			weed->x = Weed[0] + ParamG.TranslationDeltaX;
			weed->y = -Weed[1] + ParamG.TranslationDeltaY;
			// si les coordonn�es du weed est inatteignable, on retrouve ses coordonn�es dans le rep�re du robot
			// et on repasse dans la liste de weeds � traiter pour essayer de le traiter � la position suivante du weeding 
		}
	}

	BalayeListe(weeds_enleve) // on enl�ve les weeds qui seraint trait�s dans ce cycle
	{
		POINTFLT *weed = weeds_enleve.current();
		weeds.remove(*weed);
	}

	//Temp.Sauve("Coordonn�es_Weeds.txt");

	if (weeds_NR.nbElem == 0)
	// v�rification de l'existance de weeds atteignables dans le liste
	{
		ret.InfoRetour = "Liste de weeds atteignables vide";
		ret.State = TRetourState::Completed;
		ret.DureeMs = GetTickCount() - TimeDebut;
		return;
	}


	if (AvecRecalageManuel) 
	// on positionne relativement le bras sur le weed et puis on fait le r�calage manuel de la position
	{
		BalayeListe(weeds_NR)
		{
			POINTFLT weed = *weeds_NR.current();
			ControlRobotG.SetPositionXYWeedR(weed);

			ControlBrasManuelG.UpdateConsigneCourante(weed);
			ControlBrasManuelG.ActiveControlManuel = true;

			DialogHauteTension("T_Plateforme");

			MSG msg;

			while (true)
			{
				if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 	DispatchMessage(&msg);
				
				if (!ControlBrasManuelG.ActiveControlManuel)
					break;

				Sleep(300);
			}
		}	
	}

	else  // sinon, on passe le positionnement auto
	{
		if (AvecHauteTension)
		{
			BalayeListe(weeds_NR)
			{
				POINTFLT Weed = *weeds_NR.current();
				ControlRobotG.SetPositionXYWeedR(Weed);
				Sleep(DELAY_POSITIONING);
				ControlRobotG.SetPositionZWeedingR();
				Sleep(DELAY_POSITIONING);

				int CurrentMax = 0, SparkVoltage = 0, SparkDuration = 0;
				CommandeHTG.StartSequence(CurrentMax, SparkVoltage, SparkDuration, msgerr);
				Sleep(DELAY_WEEDING);
			}
		}
		else // si on ne lance pas le d�charge Haute tension, on a simplement un tempon sur les herbes � d�truire
			ControlRobotG.TestWeedingRightArm(weeds_NR);
	}
		
	ControlRobotG.SetPositionAwaitedR();
	Sleep(DELAY_RESETARM);

	ret.State = TRetourState::Completed;
	ret.DureeMs = GetTickCount() - TimeDebut;
	return;
}
#endif



bool TModulePlateforme::ResetBrasDroit(TRetourPlateforme &ret) {
	int TimeDebut = GetTickCount();
	if (!ControlRobotG.RobotIsConnected())
		// v�rification de la connexion au robot
	{
		RetourneStateError(ret, TimeDebut, "Robot non initialis�");
		return false;
	}
	if (!ControlRobotG.ArmInitialised)
	{
		ControlRobotG.InitRightArm();
		ControlRobotG.ArmInitialised = true;
		Sleep(DELAY_INITARM);
	}
	DBOUT("resets et home du bras initiaux effectu�s");
	return true;
}


//FONCTION WEEDING SURCHARGEE SELON LE TYPE DE LISTE EN ENTREE (on pourrait aussi utiliser une sp�cialisation de template)
//Version prenant une liste de groupes de weeds en entr�e, utilis�e dans le superviseur
//Les fonctions sont appel�es avec une liste de pointeurs vers des TWeedBrAS
//Le flag des mauvaises herbes d�tect�es est fix� � DONE afin de ne plus les traiter pour les prochains cycles
void TModulePlateforme::WeedingSuperviseur(_list<TGroupeWeeds> *PileGroupeWeeds, TRetourPlateforme &ret, bool AvecHauteTension, bool AvecRecalageManuel)
//les coordonn�es des mauvaises herbes sont fournies dans l'espace du robot au moment des prises d'image
//pr�c�dentes (� charge de la plateforme d'ajuster en fonction du d�placement effectif depuis)
{
	DBOUT("Rentr�e dans WeedingSuperviseur\n");
	DBOUT("POINTEUR VERS PILEGROUPWEEDS 2 DANS TACHE:" << PileGroupeWeeds);
	int TimeDebut = GetTickCount();
	ret.CropRowFinished = false;



	//Normalement le robot est d�j� connect� mais possible source de bug
	if (!ControlRobotG.RobotIsConnected())
	{
		RetourneStateError(ret, TimeDebut, "Robot non initialis�");
		return;
	}
	if (AvecHauteTension)
	{
		if (CommandeHTG.ComHT == NULL)
		{
			RetourneStateError(ret, TimeDebut, "Module Haute Tension non initialis�");
			return;
		}
	}
	if (!ControlRobotG.ArmInitialised)
	{
		ControlRobotG.InitRightArm();
		ControlRobotG.ArmInitialised = true;
		Sleep(DELAY_INITARM);
	}


	//Cr�ation de deux listes de pointeurs contenant seulement les mauvaises herbes atteignables	
	_list<TWeedBras*> WeedsAtteignablesSansOrdre;
	_list<TWeedBras*> WeedsAtteignables;

	DBOUT("Liste de pointeurs vides cr��es\n");
	//ajouter un point de d�part si on doit trouver le plus court chemin
	int tailleListe = WeedsAtteignablesSansOrdre.size();
	//if (ParamG.getShortest) { WeedsAtteignablesSansOrdre.push_back(TWeedBras*(POINTFLT(0, 0))); }
	//On boucle sur les derniers cycles
	DBOUT("Nombre �l�ments pile:"<< PileGroupeWeeds->nbElem <<"\n");
	int z;
	int zmax = PileGroupeWeeds->nbElem;
	for (PileGroupeWeeds->begin();!PileGroupeWeeds->end(); (*PileGroupeWeeds)++) {
		TGroupeWeeds *curGW = PileGroupeWeeds->current();
		DBOUT("Nombre �l�ments pile:" << PileGroupeWeeds->nbElem << "\n");
		DBOUT("Nombre �l�ments liste:" << curGW->ListeWeedsBrasTrans.nbElem << "\n");
		for (curGW->ListeWeedsBrasTrans.begin(); !curGW->ListeWeedsBrasTrans.end(); curGW->ListeWeedsBrasTrans++)
		{
			TWeedBras *pWeed = curGW->ListeWeedsBrasTrans.current(); // on v�rifie si la weed se trouve dans l'espace de travail
			//La weed doit �tre dans l'hexagone et avoir le bon flag
			if (pHexagoneBras->Contient(pWeed->Position.x, pWeed->Position.y)) {
				DBOUT("test hexagon pos ok");
			}
			if (pWeed->state == A_TRAITER) {
				DBOUT("test state ok");
			}

			if (pHexagoneBras->Contient(pWeed->Position.x, pWeed->Position.y) && (pWeed->state == A_TRAITER)) {
				WeedsAtteignablesSansOrdre.push_back(pWeed); //On ajoute le pointeur vers la weed
				DBOUT("Weed A_TRAITER  accept�e (hexagone th�orique): " << pWeed->Position.x << " | " << pWeed->Position.y << "\n");
			}
			else {
				DBOUT("Weed A_TRAITER ignor�e (hexagone th�orique): " << pWeed->Position.x << " | " << pWeed->Position.y << "\n");
			}
		}
	}
	DBOUT("On a la liste de pointeurs vers des weeds avec le flag A_TRAITER:\n");
	BalayeListe(WeedsAtteignablesSansOrdre) {
		TWeedBras *curWeedPtr=*WeedsAtteignablesSansOrdre.current();
		DBOUT("ptr:" <<curWeedPtr << " - " << curWeedPtr->Position.x << " - " << curWeedPtr->Position.y << " - " << curWeedPtr->state << "\n")
	}

	
	if (ParamG.getShortest) {
		//Calcul du chemin le plus court en partant du point (0,0)
	//====================
		DBOUT("Calcul du chemin le plus court \n");
		WeedsAtteignables = cheminWeedsAtteignables(WeedsAtteignablesSansOrdre);
		DBOUT("\n Chemin optimal trouv� \n");
		//====================
	}
	else {
		WeedsAtteignables = WeedsAtteignablesSansOrdre;
	}

	DBOUT("Nombre de weeds � traiter" << WeedsAtteignables.nbElem)

		if (WeedsAtteignables.nbElem == 0)
		{
			ret.InfoRetour = "Aucune weed atteignable depuis cette position";
			ret.State = TRetourState::Completed;
			ret.DureeMs = GetTickCount() - TimeDebut;

			DBOUT("AUCUNE WEED ATTEIGNABLE SUR CE CYCLE \n");

			return;
		}

	DBOUT("MOUVEMENT BRAS \n");

	if (AvecRecalageManuel)
		// on positionne relativement le bras sur le weed et puis on fait le r�calage manuel de la position
	{
		BalayeListe(WeedsAtteignables)
		{
			DBOUT("balaye liste weeds atteignables");
			TWeedBras *weedPtr = *WeedsAtteignables.current(); //on ne d�r�f�rence qu'une fois pour obtenir le pointeur
			if (!ControlRobotG.SetPositionXYWeedR(weedPtr->Position, POINTFLT(0, 0))) {
				//On fait un reset du bras (+ home et on passe � la weed suivante)
				ControlRobotG.ResetRightArm();
				DBOUT("continue");
				continue; //on passe � l'�lement suivant sans d�clencher la HT, on pourrait aussi break si on veut passer au cycle suivant
			}
			DBOUT("SetPositionXYWeedR ok selon les callbacks");
			ControlBrasManuelG.UpdateConsigneCourante(weedPtr->Position);
			ControlBrasManuelG.ActiveControlManuel = true;

			DialogHauteTension("T_Plateforme");

			MSG msg;
			while (true)
			{
				if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 	DispatchMessage(&msg);

				if (!ControlBrasManuelG.ActiveControlManuel)
					break;

				Sleep(300);
			}
		//Si on arrive jusque l�, la mauvaise herbe a �t� trait�e
		weedPtr->state = DONE;
		DBOUT("Weed flag DONE \n");
		}
	}

	else  // sinon, on passe au positionnement auto
	{
		DBOUT("positionnement auto");
		//Point de d�part d'une s�quence
		POINTFLT posActuelle(0, 0);
		//DBOUT("weeds atteignable \n");
		BalayeListe(WeedsAtteignables)
		{
			TWeedBras *weedPtr = *WeedsAtteignables.current(); //on ne d�r�f�rence qu'une fois pour obtenir le pointeur
			//POINTFLT pos = WeedsAtteignables.current()->Position;
			DBOUT("D�placement X: " << weedPtr->Position.x << " Y:" << weedPtr->Position.y << "\n");
			DBOUT("==================\n");
			if (!ControlRobotG.SetPositionXYWeedR(weedPtr->Position, posActuelle)) {
				ControlRobotG.ResetRightArm();
				DBOUT("D�placement termin� avec erreur, pas de lancement HT et passage � la weed suivante \n");
				continue; //on passe � l'�lement suivant sans d�clencher la HT, on pourrait aussi break si on veut passer au cycle suivant
			}
			DBOUT("D�placement termin� sans erreur \n");
			DBOUT("==================\n");
			Sleep(DELAY_POSITIONING);
			//On ajuste le Z pour le weeding
			DBOUT("D�placement Z: " << ParamG.ZWeeding << "\n");
			if (!ControlRobotG.SetPositionZWeedingR(ParamG.ZWeeding)) {
				ControlRobotG.ResetRightArm();
				DBOUT("D�placement z weeding termin� avec erreur, pas de lancement HT et passage � la weed suivante \n");
				continue;
			}
			if (AvecHauteTension)
			{
				int CurrentMax = 0, SparkVoltage = 0, SparkDuration = 0;

				char msgerr[1000]; //inutilis�
				CommandeHTG.StartSequence(CurrentMax, SparkVoltage, SparkDuration, msgerr);

				Sleep(DELAY_WEEDING);
			}
			else {
				Sleep(DELAY_WEEDING);
			}
			if (ParamG.getZero) {
				DBOUT("retour au zero du robot \n");
				//Le bras revient au point (0,0) avant de passer � la suite
				//ControlRobotG.SetPositionXYWeedR(POINTFLT(0,0));
				ControlRobotG.SetPositionAwaitedR();
			}
			else {
				DBOUT("Pas de retour du bras \n");
				//Le bras ne revient pas au point 0
				posActuelle.x = weedPtr->Position.x;
				posActuelle.y = weedPtr->Position.y;
				//on met la position actuelle en m�moire
			}
			//Si on arrive jusque l�, la mauvaise herbe a �t� trait�e
			weedPtr->state = DONE;
			DBOUT("nouvel �tat weed:" << weedPtr->state << "\n");
		}
	}

	//revenir au zero � la fin si il n'y est pas
	if (!ParamG.getZero) {
		ControlRobotG.SetPositionAwaitedR();
	}

	ret.State = TRetourState::Completed;
	ret.DureeMs = GetTickCount() - TimeDebut;
	return;
}
//Version prenant une simple liste de weeds en entr�e, utilis�e hors superviseur
//Les fonctions sont appel�es avec une liste de TWeedBras




void TModulePlateforme::Weeding(_list<TWeedBras> ListeWeeds, TRetourPlateforme &ret, bool AvecHauteTension, bool AvecRecalageManuel)

//les coordonn�es des mauvaises herbes sont fournies dans l'espace du robot au moment des prises d'image
//pr�c�dentes (� charge de la plateforme d'ajuster en fonction du d�placement effectif depuis)
{

	int TimeDebut = GetTickCount();
	ret.CropRowFinished = false;

	if (!ControlRobotG.RobotIsConnected())
		// v�rification de la connexion au robot
	{
		RetourneStateError(ret, TimeDebut, "Robot non initialis�");
		return;
	}

	if (AvecHauteTension)
	{
		if (CommandeHTG.ComHT == NULL)
		{
			RetourneStateError(ret, TimeDebut, "Module Haute Tension non initialis�");
			return;
		}
	}

	if (!ControlRobotG.ArmInitialised)
	{
		ControlRobotG.InitRightArm();
		ControlRobotG.ArmInitialised = true;
		Sleep(DELAY_INITARM);
	}
	

	//Cr�ation d'une liste contenant seulement les mauvaises herbes atteignables	
	_list<TWeedBras> WeedsAtteignablesSansOrdre;
	//ajouter un point de d�part si on doit trouver le plus court chemin
	int tailleListe = WeedsAtteignablesSansOrdre.size();
	if (ParamG.getShortest) { WeedsAtteignablesSansOrdre.push_back(TWeedBras(POINTFLT(0, 0))); }
	int i = 0;


	for(ListeWeeds.begin(); !ListeWeeds.end(); (ListeWeeds)++)
	{
		TWeedBras *pWeed = ListeWeeds.current(); // on v�rifie si le weed se trouve dans l'espace de travail
		if (ParamG.hexagoneReel) {
			if (pHexagoneBras->ContientReel(pWeed->Position.x, pWeed->Position.y)) {
				WeedsAtteignablesSansOrdre.push_back(*pWeed);
				DBOUT("Weed accept�e (hexagone reel): " << pWeed->Position.x << " \ " << pWeed->Position.y << "\n");
			}
			else {
				DBOUT("Weed ignor�e (hexagone reel): " << pWeed->Position.x << " | " << pWeed->Position.y << "\n");
			}
		}
		else {
			if (pHexagoneBras->Contient(pWeed->Position.x, pWeed->Position.y)) {
				WeedsAtteignablesSansOrdre.push_back(*pWeed);
				DBOUT("Weed accept�e (hexagone th�orique): " << pWeed->Position.x << " | " << pWeed->Position.y << "\n");
			}
			else {
				DBOUT("Weed ignor�e (hexagone th�orique): " << pWeed->Position.x << " | " << pWeed->Position.y << "\n");
			}
		}
		i++;
	}
	_list<TWeedBras> WeedsAtteignables;
	if (ParamG.getShortest) {
		//Calcul du chemin le plus court en partant du point (0,0)
	//====================
		DBOUT("Calcul du chemin le plus court \n");

		WeedsAtteignables = cheminWeedsAtteignables(WeedsAtteignablesSansOrdre);
		DBOUT("\n Chemin optimal trouv� \n");
		//====================
	}
	else {
		WeedsAtteignables = WeedsAtteignablesSansOrdre;
	}
	
	DBOUT("Nombre de weeds � traiter" << WeedsAtteignables.nbElem)

	if (WeedsAtteignables.nbElem == 0)
	{
		ret.InfoRetour = "Aucune weed atteignable depuis cette position";
		ret.State = TRetourState::Completed;
		ret.DureeMs = GetTickCount() - TimeDebut;
		
		DBOUT("AUCUNE WEED ATTEIGNABLE SUR CE CYCLE \n");

		return;
	}


	if (AvecRecalageManuel)
		// on positionne relativement le bras sur le weed et puis on fait le r�calage manuel de la position
	{
		BalayeListe(WeedsAtteignables)
		{
			POINTFLT pos = WeedsAtteignables.current()->Position;
			if (!ControlRobotG.SetPositionXYWeedR(pos, POINTFLT(0, 0))) {
				//On fait un reset du bras (+ home et on passe � la weed suivante)
				DBOUT("reset et passage au suivant");
				ControlRobotG.ResetRightArm();
				break; //on passe � l'�lement suivant sans d�clencher la HT
			}
			ControlBrasManuelG.UpdateConsigneCourante(pos);
			ControlBrasManuelG.ActiveControlManuel = true;

			DialogHauteTension("T_Plateforme");

			MSG msg;

			while (true)
			{
				if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 	DispatchMessage(&msg);

				if (!ControlBrasManuelG.ActiveControlManuel)
					break;

				Sleep(300);
			}
		}
	}

	else  // sinon, on passe au positionnement auto
	{
		DBOUT("positionnement auto");
		//Point de d�part d'une s�quence
		POINTFLT posActuelle(0, 0);
		//DBOUT("weeds atteignable \n");
		BalayeListe(WeedsAtteignables)
		{
			POINTFLT pos = WeedsAtteignables.current()->Position;
			DBOUT("D�placement X: " << pos.x << " Y:" << pos.y<< "\n");
			DBOUT("==================\n");
			if (!ControlRobotG.SetPositionXYWeedR(pos, posActuelle)) {
				ControlRobotG.ResetRightArm();
				DBOUT("D�placement termin� avec erreur, pas de lancement HT et passage � la weed suivante \n");
				break;
			}
			DBOUT("D�placement termin� sans erreur \n");
			DBOUT("==================\n");
			Sleep(DELAY_POSITIONING);
			//On ajuste le Z pour le weeding
			DBOUT("D�placement Z: " << ParamG.ZWeeding <<"\n");
			if (!ControlRobotG.SetPositionZWeedingR(ParamG.ZWeeding)) {
				ControlRobotG.ResetRightArm();
				DBOUT("D�placement z weeding termin� avec erreur, pas de lancement HT et passage � la weed suivante \n");
				break;
			}
			if (AvecHauteTension)
			{
				int CurrentMax = 0, SparkVoltage = 0, SparkDuration = 0;

				char msgerr[1000]; //inutilis�
				CommandeHTG.StartSequence(CurrentMax, SparkVoltage, SparkDuration, msgerr);

				Sleep(DELAY_WEEDING);
			}
			if (ParamG.getZero) {
				//Le bras revient au point (0,0) avant de passer � la suite
				DBOUT("retour au zero du robot \n");
				//ControlRobotG.SetPositionXYWeedR(POINTFLT(0,0));
				ControlRobotG.SetPositionAwaitedR();
			}
			else {
				DBOUT("Pas de retour du bras \n");
				posActuelle.x = pos.x;
				posActuelle.y = pos.y;
			}
		}
	}

	//revenir au zero � la fin si il n'y est pas
	if (!ParamG.getZero) {
		ControlRobotG.SetPositionAwaitedR();
	}

	ret.State = TRetourState::Completed;
	ret.DureeMs = GetTickCount() - TimeDebut;
	return;
}



void TModulePlateforme::Stop() //demande d'arr�t utilisateur
{
	StopUrgenceG = true;
	ControlRobotG.SetRobotSpeed(0, 0);
	ControlRobotG.CloseRightArm();
	ControlRobotG.ArmInitialised = false;
}



#include "ManualControlPad.h"

void TModulePlateforme::AvancementManuel(DRVTRAITIM *pD, pcallbackStep cb)
{
	int TimeDebut = GetTickCount();
	TRetourPlateforme ret;

	if (!ControlRobotG.RobotIsConnected())
	{
		RetourneStateError(ret, TimeDebut, "Robot non connect�");
		if (cb != NULL) cb(ret);
		return;
	}

	if (CommandeNavigationG != NULL)  // on r�initialise le pointeur de commande dans tous les cas
	{
		delete CommandeNavigationG; CommandeNavigationG = NULL;
	}

	CommandeNavigationG = new TCommandeManuelle(ParamG.PeriodeCommande, ParamG.PeriodeLog); // initialisation avec la commande manuelle
	TCommandeManuelle * pCom = dynamic_cast <TCommandeManuelle*> (CommandeNavigationG);
	
	char msgerr[1000];
	if (!pCom->DoStartCommande(msgerr))
	{
		RetourneStateError(ret, TimeDebut, msgerr);
		if (cb != NULL) cb(ret);
		delete CommandeNavigationG; CommandeNavigationG = NULL;
		return;
	}
	
	
	DialogControlPad("T_ControleRobot", pD->HWindow());  // ouverture dialogue de commande

	/*MSG msg;

	while (true)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 	DispatchMessage(&msg);

		if (CommandeNavigationG == NULL)	break;
	}*/

	ret.DureeMs = GetTickCount() - TimeDebut;
	ret.State = TRetourState::Completed;
	if (cb != NULL) cb(ret);  
	return;
}

bool TModulePlateforme::VerificationHauteTension()
{
	return (CommandeHTG.ComHT != NULL);
}

bool TModulePlateforme::getGPSRTKPosition(TPositionRobot Pos, char* msgerr)
{
	double x, y, CapDeg;
	if (!ControlRobotG.GetRobotEstimatedPosition(x, y, CapDeg, msgerr)) {
		return false;
	}
	Pos.x = x;
	Pos.y = y;
	Pos.CapDeg = CapDeg;
}



//bool TModulePlateforme::getEtatBras(TRetourPlateforme &res) {
//	try
//	{
//		res.ErrorDelta = (ControlRobotG.CheckDeltaError());
//		return true;
//	}
//	catch (exception& e)
//	{
//	}
//	return false;
//}



void TModulePlateforme::Weeding_pallier(_list<TWeedBras> ListeWeeds, TRetourPlateforme &ret, bool AvecHauteTension, bool AvecRecalageManuel, float intervaleDescente, float startH, float limitH, float p) {
	//les coordonn�es des mauvaises herbes sont fournies dans l'espace du robot au moment des prises d'image
//pr�c�dentes (� charge de la plateforme d'ajuster en fonction du d�placement effectif depuis)
		int TimeDebut = GetTickCount();
		ret.CropRowFinished = false;

		if (!ControlRobotG.RobotIsConnected())
			// v�rification de la connexion au robot
		{
			RetourneStateError(ret, TimeDebut, "Robot non initialis�");
			return;
		}

		if (AvecHauteTension)
		{
			if (CommandeHTG.ComHT == NULL)
			{
				RetourneStateError(ret, TimeDebut, "Module Haute Tension non initialis�");
				return;
			}
		}

		if (!ControlRobotG.ArmInitialised)
		{
			ControlRobotG.InitRightArm();
			ControlRobotG.ArmInitialised = true;
			Sleep(DELAY_INITARM);
		}


		//Cr�ation d'une liste contenant seulement les mauvaises herbes atteignables	
		_list<TWeedBras> WeedsAtteignablesSansOrdre;
		//ajouter un point de d�part si on doit trouver le plus court chemin
		int tailleListe = WeedsAtteignablesSansOrdre.size();
		if (ParamG.getShortest) { WeedsAtteignablesSansOrdre.push_back(TWeedBras(POINTFLT(0, 0))); }
		int i = 0;


		for (ListeWeeds.begin(); !ListeWeeds.end(); (ListeWeeds)++)
		{
			TWeedBras *pWeed = ListeWeeds.current(); // on v�rifie si le weed se trouve dans l'espace de travail
			if (ParamG.hexagoneReel) {
				if (pHexagoneBras->ContientReel(pWeed->Position.x, pWeed->Position.y)) {
					WeedsAtteignablesSansOrdre.push_back(*pWeed);
					DBOUT("Weed  accept�e (hexagone reel): " << pWeed->Position.x << " - " << pWeed->Position.y << "\n");
				}
				else {
					DBOUT("Weed  ignor�e (hexagone reel): " << pWeed->Position.x << " - " << pWeed->Position.y << "\n");
				}
			}
			else {
				if (pHexagoneBras->Contient(pWeed->Position.x, pWeed->Position.y)) {
					WeedsAtteignablesSansOrdre.push_back(*pWeed);
					DBOUT("Weed  accept�e (hexagone th�orique): " << pWeed->Position.x << " - " << pWeed->Position.y << "\n");
				}
				else {
					DBOUT("Weed  ignor�e (hexagone th�orique): " << pWeed->Position.x << " - " << pWeed->Position.y << "\n");
				}
			}
			i++;
		}
		_list<TWeedBras> WeedsAtteignables;
		if (ParamG.getShortest) {
			//Calcul du chemin le plus court en partant du point (0,0)
		//====================
			DBOUT("Calcul du chemin le plus court \n");

			WeedsAtteignables = cheminWeedsAtteignables(WeedsAtteignablesSansOrdre);
			DBOUT("\n Chemin optimal trouv� \n");
			//====================
		}
		else {
			WeedsAtteignables = WeedsAtteignablesSansOrdre;
		}

		DBOUT("Nombre de weeds � traiter" << WeedsAtteignables.nbElem)

			if (WeedsAtteignables.nbElem == 0)
			{
				ret.InfoRetour = "Aucune weed atteignable depuis cette position";
				ret.State = TRetourState::Completed;
				ret.DureeMs = GetTickCount() - TimeDebut;

				DBOUT("AUCUNE WEED ATTEIGNABLE SUR CE CYCLE \n");

				return;
			}


		if (AvecRecalageManuel)
			// on positionne relativement le bras sur le weed et puis on fait le r�calage manuel de la position
		{
			BalayeListe(WeedsAtteignables)
			{
				POINTFLT pos = WeedsAtteignables.current()->Position;
				if (!ControlRobotG.SetPositionXYWeedR(pos, POINTFLT(0, 0))) {
					//On fait un reset du bras (+ home et on passe � la weed suivante)
					DBOUT("reset et passage au suivant");
					ControlRobotG.ResetRightArm();
					break; //on passe � l'�lement suivant sans d�clencher la HT
				}
				ControlBrasManuelG.UpdateConsigneCourante(pos);
				ControlBrasManuelG.ActiveControlManuel = true;

				DialogHauteTension("T_Plateforme");

				MSG msg;

				while (true)
				{
					if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 	DispatchMessage(&msg);

					if (!ControlBrasManuelG.ActiveControlManuel)
						break;

					Sleep(300);
				}
			}
		}

		else  // sinon, on passe au positionnement auto
		{
			DBOUT("positionnement auto");
			//Point de d�part d'une s�quence
			POINTFLT posActuelle(0, 0);
			//DBOUT("weeds atteignable \n");
			BalayeListe(WeedsAtteignables)
			{
				POINTFLT pos = WeedsAtteignables.current()->Position;
				DBOUT("D�placement X: " << pos.x << " Y:" << pos.y << "\n");
				DBOUT("==================\n");
				if (!ControlRobotG.SetPositionXYWeedR(pos, posActuelle)) {
					ControlRobotG.ResetRightArm();
					DBOUT("D�placement termin� avec erreur, pas de lancement HT et passage � la weed suivante \n");
					break;
				}
				DBOUT("D�placement termin� sans erreur \n");
				DBOUT("==================\n");
				Sleep(DELAY_POSITIONING);
				//On ajuste le Z pour le weeding
				DBOUT("D�placement Z: " << ParamG.ZWeeding << "\n");
				if (!ControlRobotG.SetPositionZWeedingR_pallier(intervaleDescente, startH, limitH, p)) {
					ControlRobotG.ResetRightArm();
					DBOUT("D�placement z weeding termin� avec erreur, pas de lancement HT et passage � la weed suivante \n");
					break;
				}
				if (AvecHauteTension)
				{
					int CurrentMax = 0, SparkVoltage = 0, SparkDuration = 0;

					char msgerr[1000]; //inutilis�
					CommandeHTG.StartSequence(CurrentMax, SparkVoltage, SparkDuration, msgerr);

					Sleep(DELAY_WEEDING);
				}
				else {
					Sleep(DELAY_WEEDING);
				}
				if (ParamG.getZero) {
					//Le bras revient au point (0,0) avant de passer � la suite
					DBOUT("retour au zero du robot \n");
					//ControlRobotG.SetPositionXYWeedR(POINTFLT(0,0));
					ControlRobotG.SetPositionAwaitedR();
				}
				else {
					DBOUT("Pas de retour du bras \n");
					posActuelle.x = pos.x;
					posActuelle.y = pos.y;
				}
			}
		}

		//revenir au zero � la fin si il n'y est pas
		if (!ParamG.getZero) {
			ControlRobotG.SetPositionAwaitedR();
		}

		ret.State = TRetourState::Completed;
		ret.DureeMs = GetTickCount() - TimeDebut;
		return;
	return;
}
