
#ifndef __CONTROLROBOT_H_
#define __CONTROLROBOT_H_

#include <boost\math\special_functions\detail\round_fwd.hpp>


#include "erxCom.h"
#include "list_tpl.h"
#include "ligne.h" //POINTFLT
#include "drvtraiti.h"

#ifndef POSITIONZ_WEEDING
#define POSITIONZ_WEEDING  0.97
#endif

#ifndef POSITIONZ_POSITIONING
#define POSITIONZ_POSITIONING  0.8
#endif

#define DELAY_POSITIONING  1500 //1000
#define DELAY_WEEDING  2000
#define DBOUT( s )            \
{                             \
   std::ostringstream os_;    \
   os_ << s;                   \
   OutputDebugString( os_.str().c_str() );  \
}




struct TControlRobot
{
	ErxComManager* m_client_manager;

	std::string connectAddress;

	bool ArmInitialised;

	TControlRobot() : ArmInitialised(false)
	{
		m_client_manager = new ErxComManager;
	};

	~TControlRobot() 
	{ 
		if (m_client_manager->isConnected())
			m_client_manager->close();
			
		if (m_client_manager)
			delete m_client_manager;
	};

	// connexion :
	bool ConnectRobot(char* msg);

	void SetConnectAddress(char* address) { connectAddress = address; }

	bool DeconnectRobot(char* msgerr);

	bool RobotIsConnected() { return m_client_manager->isConnected(); }

	// recup�ration de donn�es capteur Erx:
	bool GetRobotSensorData(double& Lat, double& Lon, double& CapDeg, char* msgerr);

	// get position estim�e par rapport au rep�re du sol
	bool GetRobotEstimatedPosition(double& x, double& y, double& CapDeg, char* msgerr);

	// activation gps rtk:
	void ActiveRTK();

	bool GetRTKStatus(char* ntripState, char* rtkState, char* msgerr);

	// reset ground frame
	void ResetGroundFrame();

	// mouvement simple:
	void Avancement(double distance);

	void RotateAngle(double angle, double radius);

	void StopMouvement();

	// envoie de la consignes de moteurs
	void SetRobotSpeed(double vL, double vR);


	// ex fonction de bas niveau pour le weeding
	void DoWeeding(_list<POINTFLT> weeds, double z);


	// fonction bas niveau pour l'envoi de commande au bras
	void SendHWCommand(std::string cmd);


	// Commandes de base des bras :
	void InitRightArm();

	void ResetRightArm(); // la fonction seulement utilis�e si le bras est bloqu� en une position inatteignable

	void CloseRightArm();

	void InitLeftArm();

	void ResetLeftArm();

	void CloseLeftArm();


	// test du weeding :
	void TestWeedingRightArm(_list<POINTFLT> weeds, double z);

	void TestWeedingLeftArm(_list<POINTFLT> weeds, double z);


	// envoi de coordonn�es XYZ de bras:
	bool SetPositionXYWeedR(POINTFLT weed, POINTFLT posActuelle);
	bool SetPositionXYWeedR_old(POINTFLT weed, POINTFLT posActuelle);

	void SetPositionAxis(int axis, float pt);

	bool SetPositionZWeedingR(double z);
	bool SetPositionZWeedingR_pallier(float intervaleDescente, float debutH, float limitH,  float p);
	
	bool SetHeightR(double dis);

	void SetPositionAwaitedR();


	void SetPositionXYWeedL(POINTFLT weed);

	void SetPositionZWeedingL(double z);

	void SetHeightL(double dis);

	void SetPositionAwaitedL();
	
	//Gestion des erreurs sur le bras
	//bool TControlRobot::CheckDeltaError();

	//bool TControlRobot::SolveDeltaError();


};

#endif