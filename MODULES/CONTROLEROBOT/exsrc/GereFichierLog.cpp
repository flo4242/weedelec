#include <time.h>  
#include <strstream>

#include "GereFichierLog.h"
#include "UtilficGen.h"
#include "commtype.h"



void TGereFichierLog::SetLigneTitreLog(ostream *os)
{
	*os << "Time" << '\t' <<"Time UTC"<< "MoteurG" << '\t' << "MoteurD" << '\t' << "x" << '\t' << "y" << '\t' << "Cap" <<'\t'<< "Cap voulu"<< '\t';
	*os << "Lat" << '\t' << "Lon" << '\t' << "FixG" << '\t' << "FixD";
}



ofstream * TGereFichierLog::InitFichierLog(char *DossierLog, char* NfOut)
{
	TNomFic NomFichierLog;

	time_t datetime = time(NULL);
	struct tm *ldt = localtime(&datetime);

	sprintf(NomFichierLog, "%s\\Log_%02d-%02d-%4d_%02d-%02d-%02d.log",
		DossierLog, ldt->tm_mday, ldt->tm_mon + 1, ldt->tm_year + 1900, ldt->tm_hour, ldt->tm_min, ldt->tm_sec);

	ofstream * os = new std::ofstream(NomFichierLog, std::ofstream::out);

	if (!os->good()) return NULL;

	SetLigneTitreLog(os);
	*os << '\n';

	strcpy(NfOut, NomFichierLog);
	return os;
}

void TGereFichierLog::WriteLine
(ofstream *pos, double time, char *UTCTime, double CG, double CD, double x, double y, double Cap, double CapDesire, double Lat, double Lon, int fixG, int fixD)
{
	char temp[500];
	sprintf(temp, "%.2f\t%s\t%.2f\t%.2f\t%.3f\t%.3f\t%.3f\t%.3f\t%.16f\t%.16f\t%d\t%d\n", time, UTCTime, CG, CD, x, y, Cap, CapDesire, Lat, Lon, fixG, fixD);
	*pos << temp;

}

bool TGereFichierLog::ImporteLog(char *NfLog, _list<Waypoint> &ListeW, char *msgerr)
{
	ifstream is(NfLog);
	if (!is.good())
	{
		sprintf(msgerr, "Echec ouverture %s", NfLog);
		return false;
	}

	//V�rification ligne titre
	char ligne[500];
	is.getline(ligne, 500);

	//titre attendu:
	char titre[500]; memset(titre, 0, 500);
	ostrstream ost(titre, 500);
	SetLigneTitreLog(&ost);

	if (strcmp(ligne, titre) != 0)
	{
		sprintf(msgerr, "Format non conforme (fichier %s)", NfLog);
		return false;
	}

	//lecture des lignes suivantes
	int cnt = 1;
	while (true)
	{
		is.getline(ligne, 500);
		if (strlen(ligne) == 0) return true; //fin du fichier

		istrstream is(ligne);

		is.precision(16);

		double time;
		double CG, CD, x, y, Cap, CapDesire;
		int fixG, fixD;
		Waypoint WP;
		is >> time;
		is >> WP.Description;
		is >> CG;
		is >> CD;
		is >> x;
		is >> y;
		is >> Cap;
		is >> CapDesire; 
		is >> WP.LatDeg >> WP.LonDeg;
		is >> fixG >> fixD;

		WP.elevation = 0;
		if (cnt < 1000)		sprintf(WP.Name, "P%03d", cnt); //cas le plus fr�quent
		else sprintf(WP.Name, "P%d", cnt); //au cas o�
		sprintf(WP.Description, "");

		ListeW.push_back(WP);
		cnt++;
	}

}


