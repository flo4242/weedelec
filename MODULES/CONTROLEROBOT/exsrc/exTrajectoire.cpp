#include "Trajectoire.h" 

#include "KML.h" 
#include "GereFichierLog.h" 
#include "UtilficGen.h" 
#include "ExportModuleControleRobot.h" //pour structure TGereFichierLog
#include <algorithm>


bool Trajectoire::Init(TPositionRobot &Pos, char* Nf, TRepereLocal* R, char *msg)
{
	if (!Pos.CapDispo)
	{
		sprintf(msg, "Mode trajectoire sur fichier non utilisable si un seul fix GPS (cap non dispo)");
		return false;
	}

	if(strlen(Nf)==0)
	{
		sprintf(msg, "Fichier de trajectoire non d�fini");
		return false;
	}

	_list<Waypoint> ListeW;

	char *ext = ChaineExt(Nf);

	if (strcmp(ext, "kml") == 0)
	{
		if (!ImporteKML(Nf, ListeW, msg))	return false;
	}
	else if(strcmp(ext, "log") == 0)
	{
		if (!TGereFichierLog::ImporteLog(Nf, ListeW, msg))	return false;
	}

	else { sprintf(msg, " Extension fichier log non reconnue"); return false; }

	if (ListeW.nbElem < 1)
	{
		sprintf(msg, "Pas de waypoints dans le fichier;\n %s", Nf);
		return false;
	}


	//On souhaite commencer le trajet au point de passage le plus proche afin de ne pas avoir � revenir en arri�re

	
	NbPoints = ListeW.nbElem ; 
	vector<POINTFLT> PointsPassageTemp; //Contient les points de passage non filtr�s
	vector<float> pairesDistance; //Contient les points de passage non filtr�s
	vector<float> pointsAngles; //Contient les points de passage non filtr�s
	

	int i=1;
	int j;
	double X, Y; //stockage temporaire de coordonn�es projet�es
	BalayeListe(ListeW)
	{
		Waypoint *pW = ListeW.current();
		//R->WGS84VersMetres(pW->LatDeg, pW->LonDeg, PointsPassage[i].x, PointsPassage[i].y);
		R->WGS84VersMetres(pW->LatDeg, pW->LonDeg, X, Y);
		PointsPassageTemp.push_back(POINTFLT(X, Y));
		DBOUT("point de passage " << i << ":" << X << " - " << Y);
		if (i > 1) {
			//Somme des distances aux deux points de passage
			pairesDistance.push_back((POINTFLT(Pos.x, Pos.y) - PointsPassageTemp[i - 2]).Norme() + (POINTFLT(Pos.x, Pos.y) - PointsPassageTemp[i-1]).Norme());
			DBOUT("distance � la paire " << i << "-" << i + 1 << ":" << pairesDistance.back());
		}
		//Angle trigonom�trique entre le robot et le point de passage
		pointsAngles.push_back(atan2(Pos.y- PointsPassageTemp[i - 1].y, Pos.x - PointsPassageTemp[i - 1].x)* (180 / M_PI));
		DBOUT("angle au point" << i << ":" << pointsAngles.back());
		i++;
	}

	//On cherche la paire avec la distance minimale
	int minElementIndex = std::min_element(pairesDistance.begin(), pairesDistance.end()) - pairesDistance.begin();
	//float minElement = *std::min_element(pairesDistance.begin(), pairesDistance.end());
	float angleDiff= pointsAngles[minElementIndex] - pointsAngles[minElementIndex + 1];
	if (angleDiff <= -180) angleDiff += 360;
	if (angleDiff >= 180) angleDiff -= 360;

	int premierPoint;
	if (angleDiff > 90) { //On consid�re que l'on est au milieu d'un segment, le deuxi�me point de la paire sera le point de d�part
		premierPoint = minElementIndex + 1;
	}
	else { //les deux points sont globalement dans la m�me direction, le premier point de la paire sera le point de d�part
		premierPoint = minElementIndex;
	}
	DBOUT("point de d�part retenu:" << premierPoint);
	//On peut alors cr�er la liste finale des points de passage
	PointsPassage = new POINTFLT[NbPoints- premierPoint +1]; //contient la position du robot puis les points de passage filtr�s, allou� sur le tas pour �tre conserv�
	PointsPassage[0] = POINTFLT(Pos.x, Pos.y);
	
	for (i = minElementIndex, j=1; i < PointsPassageTemp.size(); i++,j++) {
		PointsPassage[j] = PointsPassageTemp[i];
	}


	P1 = PointsPassage[0];
	P2 = PointsPassage[1];
	LongueurSegment = (P2 - P1).Norme();
	IndexCourant = 0;

	sprintf(msg, "Trajectoire charg�e: %d points de passage (%d �cart�s)", NbPoints - minElementIndex, minElementIndex);

	return true;
}

bool Trajectoire::Init(TPositionRobot &Pos, double distance, char *msg)
{
	NbPoints = 2;
	PointsPassage = new POINTFLT[NbPoints];

	PointsPassage[0] = POINTFLT(Pos.x, Pos.y);
	PointsPassage[1] = PointDistantSelonDirection(PointsPassage[0], Pos.CapDeg, distance);
	P1 = PointsPassage[0];
	P2 = PointsPassage[1];
	LongueurSegment = (P2 - P1).Norme();
	return true;

}

bool Trajectoire::SegmentSuivant()
{
	if (IndexCourant >= NbPoints - 2) return false; //pas d'autre segment
	IndexCourant++;
	P1 = PointsPassage[IndexCourant];
	P2 = PointsPassage[IndexCourant+1];
	LongueurSegment = (P2 - P1).Norme();

}


double DirectionDegVersPointDistant(POINTFLT PRob, POINTFLT Pdistant)
{
	double capdeg = atan2(Pdistant.y - PRob.y, Pdistant.x - PRob.x)* (180 / M_PI);
	//capdeg indique ici l'angle par rapport � l'axe X g�ographique. Il faut le traduire en cap pour le robot
	capdeg -= 90;
	if (capdeg <= -180) capdeg += 360;
	return capdeg;
}

POINTFLT PointDistantSelonDirection(POINTFLT PRob, double DirectionDeg, double distance)
{
	//traduction de DirectionDeg en angle par rapport � l'axe X g�ographique.
	DirectionDeg += 90;
	double AngleRad = DirectionDeg * (M_PI / 180);
	return PRob + POINTFLT(cos(AngleRad), sin(AngleRad))*distance;
}