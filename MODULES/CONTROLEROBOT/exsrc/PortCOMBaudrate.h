#pragma once

enum PortCOMBaudrate
{
	EBaudUnknown = -1,			// Unknown
	EBaud110 = CBR_110,		// 110 bits/sec
	EBaud300 = CBR_300,		// 300 bits/sec
	EBaud600 = CBR_600,		// 600 bits/sec
	EBaud1200 = CBR_1200,	// 1200 bits/sec
	EBaud2400 = CBR_2400,	// 2400 bits/sec
	EBaud4800 = CBR_4800,	// 4800 bits/sec
	EBaud9600 = CBR_9600,	// 9600 bits/sec
	EBaud14400 = CBR_14400,	// 14400 bits/sec
	EBaud19200 = CBR_19200,	// 19200 bits/sec (default)
	EBaud38400 = CBR_38400,	// 38400 bits/sec
	EBaud56000 = CBR_56000,	// 56000 bits/sec
	EBaud57600 = CBR_57600,	// 57600 bits/sec
	EBaud115200 = CBR_115200,	// 115200 bits/sec
	EBaud128000 = CBR_128000,	// 128000 bits/sec
	EBaud256000 = CBR_256000,	// 256000 bits/sec  
};
