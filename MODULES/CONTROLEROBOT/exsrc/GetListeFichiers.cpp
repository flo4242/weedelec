﻿#include "GetListeFichiers.h"
#include <stdio.h>"

/********************************************************************************/
/*						RECHERCHE DE DOSSIERS									*/
/********************************************************************************/

static int DoGetListeSousDossiers(_list<TNf> &ListeDossiers, char * dossier, bool recursif, char *masque);
//références croisées

static bool TesteEtStockeSousDossier(WIN32_FIND_DATA &WFD, char * dossierpere,
					_list<TNf> &ListeDossiers, bool recursif)
/******************************************************************************/
{
if(WFD.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) //c'est un sous-dossier
	if(strcmp(WFD.cFileName, ".")!=0)     //dossier parent
		if(strcmp(WFD.cFileName, "..")!=0)   //dossier parent
			{
			TNf Nf;		strcpy(Nf, dossierpere);  strcat(Nf, "\\");
			strcat(Nf, WFD.cFileName);  ListeDossiers.push_back(Nf);

			//REENTRANCE à partir du sous-dossier Nf
			if(recursif)	DoGetListeSousDossiers(ListeDossiers, Nf.Nf, recursif, NULL);
			return true;
			}
return false;
}

static int DoGetListeSousDossiers(_list<TNf> &ListeDossiers, char * dossier, bool recursif, char *masque)
/************************************************************************************************************/
{
TNomFic cible;
strcpy(cible, dossier); strcat(cible, "\\");
strcat(cible, masque ? masque : "*.*");

WIN32_FIND_DATA WFD;
HANDLE H=FindFirstFileEx(
  cible,
  FindExInfoStandard, //FINDEX_INFO_LEVELS fInfoLevelId,
  &WFD,					//LPVOID lpFindFileData,
   FindExSearchLimitToDirectories,	//FINDEX_SEARCH_OPS fSearchOp,
  NULL,
  0);

if(H==INVALID_HANDLE_VALUE)    return 0; 	//rien trouvé
else TesteEtStockeSousDossier(WFD, dossier, ListeDossiers, recursif);
// NB: FindFirstFileEx() est censé permettre de limiter la recherche aux dossiers.
// En fait, ca ne fonctionne pas toujours --> on teste quand même (conseillé dans aide de la fonction !!)


//fichiers suivants (le handle H a conservé les conditions de recherche)
while( FindNextFile(H, &WFD))	TesteEtStockeSousDossier(WFD, dossier, ListeDossiers, recursif);

FindClose(H);
return ListeDossiers.nbElem;
}

/*************************************************************************************************/
int GetListeSousDossiers(_list<TNf> &ListeDossiers, char * dossier, char *masque, bool recursif)
/*************************************************************************************************/
{
//si on spécifie un masque sur le nom de dossier, on ne peut effectuer directement une recherche récursive,
//car alors la recherche s'arrête aux étages supérieurs s'ils ne satisfont pas la condition.
// --> il faut donc établir la liste récursive de tous les sous-dossiers quel que soit leur nom
// puis balayer cette liste avec le masque
//--> fait dans les cas si récursif pour éviter les tests "*" et "*.*"

DoGetListeSousDossiers(ListeDossiers, dossier, false, masque); //répertoire principal

if(recursif)
	{
	_list<TNf> ListeTousDossiers;
	DoGetListeSousDossiers(ListeTousDossiers, dossier, true, NULL);	//tous sous-dossiers (pas de masque)

	//recherche non récursive avec le masque dans chacun des sous dossiers
	for(ListeTousDossiers.begin(); !ListeTousDossiers.end(); ListeTousDossiers++)
		DoGetListeSousDossiers(ListeDossiers, ListeTousDossiers.current()->Nf, false, masque);
	}


return ListeDossiers.nbElem;
}


/********************************************************************************/
/*						RECHERCHE DE FICHIERS									*/
/********************************************************************************/

static bool TesteEtStockeFichier(WIN32_FIND_DATA &WFD, char * dossierpere,
					_list<TNf> &ListeDossiers)
/******************************************************************************/
{
if(!(WFD.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) //ce n'est pas un sous-dossier
			{
			TNf Nf;		strcpy(Nf, dossierpere);  strcat(Nf, "\\");
			strcat(Nf, WFD.cFileName);  ListeDossiers.push_back(Nf);
			return true;
			}
else return false;
}


static int DoGetListeFichiers(_list<TNf> &ListeFichiers, char * dossier, char *masque)
/***************************************************************************************/
// NON RECURSIF !!
{
TNomFic cible;

//JAN 2015; ajout possibilité de travailler dans le dossier courant (chaîne dossier vide)
if(strlen(dossier)!=0) {strcpy(cible, dossier); strcat(cible, "\\"); }
else  strcpy(cible, "");

strcat(cible, masque ? masque : "*.*");

WIN32_FIND_DATA WFD;
HANDLE H = FindFirstFile(cible, &WFD);
if(H==INVALID_HANDLE_VALUE)    return 0; 	//rien trouvé
else TesteEtStockeFichier(WFD, dossier, ListeFichiers);

//fichiers suivants (le handle H a conservé les conditions de recherche)
while( FindNextFile(H, &WFD))		TesteEtStockeFichier(WFD, dossier, ListeFichiers);

FindClose(H);
return ListeFichiers.nbElem;
}


/********************************************************************************************************/
int GetListeFichiersSelonMasque(_list<TNf> &ListeFichiers, char * dossier, char *masque, bool recursif)
/********************************************************************************************************/
{
//si on spécifie un masque sur le nom de fichier, on ne peut effectuer directement une recherche récursive,
//car alors la recherche s'arrête aux étages supérieurs s'il n'y a pas de fichier correspondant
// --> il faut donc établir la liste récursive de tous les sous-dossiers puis chercher dans ces sous-dossiers.

DoGetListeFichiers(ListeFichiers, dossier, masque);	//dossier principal

if(recursif)
	{
	_list<TNf> ListeSousDossiers;
	DoGetListeSousDossiers(ListeSousDossiers, dossier, true, NULL);	//tous sous-dossiers (pas de masque)

	//recherche non récursive avec le masque dans chacun des sous dossiers
	for(ListeSousDossiers.begin(); !ListeSousDossiers.end(); ListeSousDossiers++)
		DoGetListeFichiers(ListeFichiers, ListeSousDossiers.current()->Nf, masque);
	}

return ListeFichiers.nbElem;
}

/***********************************************************************************************/
int GetListeFichiers(_list<TNf> &ListeFichiers, char * dossier, char * ext, bool recursif)
/***********************************************************************************************/
{

char masque[20] = "*."; strcat(masque,  (ext ==NULL ? "*" : (ext[0]=='\0' ? "*" : ext))       );
return GetListeFichiersSelonMasque(ListeFichiers, dossier, masque, recursif);
}


//VERSIONS DIALOGUE CHOIX MULTIPLE AVEC RETOUR SOUS FORME DE LISTE
//(voir utilific.h pour le détail des autres paramètres)

bool DoChoixFichierMultiple (char *titre, _list<TNf> &Liste, char *extension,
						   char *initpath, DWORD taillebuf, HWND hwnd, DWORD flags, int* pIndex)
/********************************************************************************************/
{

	char * ListeNfIn = new char[taillebuf];
	memset(ListeNfIn, 0, taillebuf);

	Liste.clear();

	int fileoffset;
	if(DoChoixFichierMultiple(titre, ListeNfIn, fileoffset, extension,  initpath, taillebuf, hwnd, flags, pIndex))
	{

		//pour debug: sauvegarde sur fichier de tout le buffer de retour
		FILE *F = fopen("bufout.txt", "w");
		for(int i=0; i< taillebuf-1; i++)
		{
			//if(ListeNfIn[i]=='\0' && ListeNfIn[i+1]=='\0') break;

			fwrite(ListeNfIn+i, 1, 1, F);
		}
		fclose(F);





		// Contenu du paramètre 'listenoms' après validation d'une sélection de fichier(s):
		// Dans le cas d'une sélection de plusieurs fichiers : 
		// (Les chaines sont séparées par le caractère '\0', et en fin de liste se trouve un double '\0').
		// -> la première chaine est le path des images,
		// -> les chaines suivantes sont les noms des fichiers seulement.
		//  Dans le cas de la sélection d'un seul fichier:
		// -> il n'y a alors qu'une seule chaine de caractères qui est le path complet du fichier (rep + nomFichier).


		//y-a-t-il un seul fichier ou plusieurs ?

		TNomFic PremiereChaine;
		strcpy(PremiereChaine, ListeNfIn); //lecture jusqu'au premier caractère '\0';

		if(strlen(PremiereChaine)>fileoffset) //la première chaîne contient aussi le nom de fichier -> un seul fichier)
			Liste.push_back(PremiereChaine);
		//NB: le path ne peut être vide (au moins une lettre de lecteur)

		else
		{
			//la première chaîne contient seulement le path; les noms de fichiers suivent
			int debut = fileoffset;
			while(true)
			{
				TNf NfCourt; strcpy(NfCourt, ListeNfIn + debut);
				int l = strlen(NfCourt);
				if(l==0) 			break; //on a rencontré un double '\0'
				

				TNomFic Nf;
				sprintf(Nf, "%s\\%s", PremiereChaine, NfCourt);
				Liste.push_back(Nf);
				debut += l+1;
				if(debut >= taillebuf)	break;
					
			}
		}

		
	}	//if(DoChoixFichierMultiple()

	delete ListeNfIn;

	return (Liste.nbElem >0);
}
	



bool ChoixFichierMultiple (char *titre, _list<TNf> &Liste, char *extension, char *fichrep, DWORD taillebuf,
                             HWND hwnd, DWORD flags, int* pIndex)
/*****************************************************************************************************************/
{
   TNomFic initpath="";
   ChargeNomRepertoire (initpath, fichrep);
   bool ret = DoChoixFichierMultiple (titre, Liste, extension, initpath, taillebuf, hwnd, flags, pIndex);
   if (ret)
	{	
		// Récupération du path
		TNomFic openRep;
		strcpy(openRep, Liste.head->obj.Nf); //premier des fichiers
		NomPath(openRep);     
		strcat(openRep, "\\");	// Ajout du séparateur.
		SauveNomRepertoire (openRep, fichrep);
	}
   return ret;
}