// dans le module plateforme2, on utilise le syst�me de deux antennes de GPS externe pour le guidage


#include "ControlRobot.h" 
// il faut toujours inclure "ControlRobot.h" en premier pour que "erxCom.h" soit d�clar� en premier 
// (sinon incompatibilit� dans le module 'chrono') 

#include "Plateforme_2.h"
#include "CommandeNavigation_2.h"
#include "ExportModuleControleRobot.h"
#include "GetPortsCOM.h"
#include "GPSReceiverS.h"

#include "NmeaPositionTCPIP.h"

#ifndef VITESSEMAX
#define VITESSEMAX  0.4   
#endif
//externs d�finis ailleurs et r�cup�r�s ici
extern TParamRobot ParamG;
extern TControlRobot ControlRobotG;
extern TCommandeNavigation *CommandeNavigationG;

TRepereLocal *RepereG = NULL;
Hexagone *pHexagoneBras = NULL;

GPSReceiverSeptentrio GPSReceiverS;






TStateInitPlateforme InitPlateformeRobot(DRVTRAITIM *pD, TParamRobot &param, bool TempoDebut) {
	TStateInitPlateforme ret;
	char msg[1000];
	pD->PrintTexte("Connexion du robot ...\n");
	MSG MsgWin;
	while (PeekMessage(&MsgWin, pD->HWindow(), 0, 0, PM_REMOVE)) 	DispatchMessage(&MsgWin);
	//pour affichage imm�diat ligne d'avertissement attente ci-dessus

	ControlRobotG.SetConnectAddress(param.ConnexionWifi ? param.AdresseConnexionWifi : param.AdresseConnexionEth);

	ret.RobotConnecte = ControlRobotG.ConnectRobot(msg);

	if (!ret.RobotConnecte) { pD->PrintTexte(msg);}
	else {pD->PrintTexte("Robot connect�");}
	return ret;
}

TStateInitPlateforme InitPlateformeGPS(DRVTRAITIM *pD, TParamRobot &param, bool TempoDebut)
{
	
	TStateInitPlateforme ret;
	char msg[1000];
	pD->PrintTexte("\n\nConnexion du module GPS Septentrio ...\n");

	bool retS = GPSReceiverS.Connect(param.adresseSeptentrio, param.portSeptentrio, msg);

	if(!retS) pD->PrintTexte("Echec initialisation: %s \n", msg);
	
	
	ret.SeptentrioGPSConnecte = retS;
	NMEAData dataInit;

	Sleep(500);
	if (!GPSReceiverS.GetRawData(dataInit, msg)) {
		RepereG = new TRepereLocal(dataInit.LatDeg, dataInit.LonDeg);
		ret.RepereLocalInitialise = true;
		pD->PrintTexte("repere local init \n");
	}
	else {
		ret.RepereLocalInitialise = false;
	}


	

	return ret;
}



TStateInitPlateforme InitPlateforme(DRVTRAITIM *pD, TParamRobot &param, bool TempoDebut) //appel� au lancement du module, initialise le robot, le GPS et le rep�re local
{
	TStateInitPlateforme ret,retCombi; //retour sur l'�tat des 3 composantes
	pD->OuvreTexte();
	pD->EffaceTexte();

	pD->PrintTexte("INITIALISATION DU ROBOT, DU GPS ET DU REPERE LOCAL \n");

	//Possibilit� d'annuler l'op�ration si la fonction est appell�e au d�marrage du logiciel
	if (TempoDebut)
	{
		pD->PrintTexte("      (touche espace ou clic gauche avant 3 secondes pour annuler)\n");
		//test d'abandon
		DWORD debut = GetTickCount();
		while (GetTickCount() - debut < 3000)
		{
			if (   ((GetAsyncKeyState(' ') & 0x8000) != 0) || (pD->TestCliqueGauche()) )
			{
				pD->PrintTexte(" \n\n Abandon utilisateur ");
				return ret;
			}
		}
	}
	//CONNEXION ROBOT
	ret=InitPlateformeRobot(pD, param, TempoDebut);
	retCombi.RobotConnecte = ret.RobotConnecte;
	//CONNEXION GPS
	ret=InitPlateformeGPS(pD, param, TempoDebut);
	retCombi.SeptentrioGPSConnecte = ret.SeptentrioGPSConnecte;
	// d�finition de l'espace de travail pour weeding:
	if (pHexagoneBras != NULL)
	{
		delete pHexagoneBras; pHexagoneBras = NULL;
	}
	pHexagoneBras = new Hexagone(ParamG.RayonHexagone, ParamG.ResolutionHexagone);
	ret.RepereLocalInitialise = true;
	retCombi.RepereLocalInitialise = ret.RepereLocalInitialise;
	pD->PrintTexte("\n\nHexagone de travail du bras initialis�");
	return ret;
}


TStateInitPlateforme CheckInitPlateforme()
{
	TStateInitPlateforme ret;
	//ret.RobotConnecte = ControlRobotG.RobotIsConnected();
	ret.RobotConnecte = true;
	ret.SeptentrioGPSConnecte = true;
	ret.SeptentrioGPSConnecte = GPSReceiverS.IsConnected();
	ret.RepereLocalInitialise = (RepereG != NULL);
	return ret;
}


void ReleasePlateforme()
{
	//d�connexion du robot
	char msgerr[1000]; ControlRobotG.DeconnectRobot(msgerr);


	
	if (RepereG != NULL) // d�truit rep�re local
	{
		delete RepereG;
		RepereG = NULL; // permettra la reconstruction d'un rep�re local d�s la reconnexion
	}

	if (pHexagoneBras != NULL)
	{
		delete pHexagoneBras; pHexagoneBras = NULL;
	}

	if (CommandeNavigationG != NULL) // d�truit la commande de navigation (soit manuelle, soit automatique)
	{
		if (CommandeNavigationG->Timer_Commande) CommandeNavigationG->StopTimer();
		delete CommandeNavigationG; CommandeNavigationG = NULL;
	}

	if (ControlRobotG.ArmInitialised)
	{
		ControlRobotG.CloseRightArm();
		ControlRobotG.ArmInitialised = false;
	}

}

bool PositionRobotEstimee(TPositionRobot &pos, char *msgerr, char *UTCTime50)
// r�ception de position estim�e du robot par le syst�me de GPS externe, 
// dont la communication �tablie par la structure GPSReceiver
{
	if (RepereG == NULL)
		
	{
		DBOUT("repere non initialis�");
		sprintf(msgerr, "Rep�re local non initialis�");
		return false;
	}
	
	if (!GPSReceiverS.IsConnected())
	{
		sprintf(msgerr, "GPS Septentrio non connect�");
		return false;
	}

	NMEAData Data;
	if (!GPSReceiverS.GetRawData(Data, msgerr)) {
		sprintf(msgerr, "Erreur recup position");
		return false;
	}
	
	RepereG->WGS84VersMetres(Data.LatDeg, Data.LonDeg, pos.x, pos.y);
	//DBOUT("\nposition transform�e: x=" << X << " y=" << Y << "\n");
	pos.CapDeg = Data.CapDeg;
	pos.CapDispo = true;
	pos.fix=pos.fixDroit = pos.fixGauche = Data.Fix;

	if (UTCTime50 != NULL)	strcpy(UTCTime50, Data.UTCTimeISO);

	//On regarde si les donn�es obtenues sont coh�rentes
	if (!(Data.Fix == 2 || Data.Fix == 4)) {
		DBOUT("TRAME RECUE SANS FIX 2 OU 4");
	}

	
	return true;
	
	
}

void StopPlateforme()
{
	if(CommandeNavigationG != NULL)
		CommandeNavigationG->StopTimer();
	
	if (ControlRobotG.RobotIsConnected())
		ControlRobotG.SetRobotSpeed(0, 0);
}

