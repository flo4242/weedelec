#include "ControlRobot.h"
#include "utilfic.h"
#include <string>
#include <boost/timer/timer.hpp>




bool callbackReturn;
std::stringstream callbackError;
// connexion:
bool TControlRobot::ConnectRobot(char* errmsg)
{
	if(connectAddress.length()==0)
	{
		sprintf(errmsg, "Adresse du connexion du robot non d�finie");
		return false;
	}


	utility::string_t temp = utility::conversions::to_string_t(connectAddress);

	m_client_manager->connect(temp);
	if (!RobotIsConnected())
	{
		char addtemp[100];
		strcpy(addtemp, connectAddress.c_str());
		sprintf(errmsg, "Connexion �chou�e pour l'adresse %s", addtemp);
		return false;
	}

	return true;
		
}


bool TControlRobot::DeconnectRobot(char* msgerr)
{
	if (!RobotIsConnected())
	{
		sprintf(msgerr, "Pas de connexion en cours avec le robot");
		return false;
	}
	m_client_manager->close();
	return true;
}


// rec�ption de donn�es du capteur Erx => pas utile si l'on consid�re le GPS septentrio utilis�
bool TControlRobot::GetRobotSensorData(double& Lat, double& Lon, double& CapDeg, char* msgerr)
{
	if (!RobotIsConnected())
	{
		sprintf(msgerr, "Pas de connexion au robot");
		return false;
	}
	web::json::value temp = m_client_manager->getRawAbsPosition();

	Lat = temp.at(L"lat").as_double();
	Lon = temp.at(L"lon").as_double();
	CapDeg = temp.at(L"orient").as_double();

	return true;
}


bool TControlRobot::GetRobotEstimatedPosition(double& x, double& y, double& CapDeg, char* msgerr)
{
	if (!RobotIsConnected())
	{
		sprintf(msgerr, "Pas de connexion au robot");
		return false;
	}

	web::json::value Pos = m_client_manager->getEstimatedPosition();
	x = Pos.at(L"x").as_double();
	y = Pos.at(L"y").as_double();
	CapDeg = Pos.at(L"orient").as_double();

	return true;
}



// activation du GPS rtk
void TControlRobot::ActiveRTK()
{
	if (RobotIsConnected())
		m_client_manager->activateRTKCorrection(true);
}

bool TControlRobot::GetRTKStatus(char* ntripState, char* rtkState, char* msgerr)
{
	if (!RobotIsConnected())
	{
		sprintf(msgerr, "Pas de connexion en cours avec le robot");
		return false;
	}
	web::json::value rtkStatus = m_client_manager->getRTKStatus();

	if (rtkStatus.has_field(L"rtkStat"))
	{
		utility::string_t rtk = rtkStatus.at(L"rtkStat").as_string();
		std::string rtkString = utility::conversions::to_utf8string(rtk);
		strcpy(rtkState, rtkString.c_str());
	}

	if (rtkStatus.has_field(L"ntripStat"))
	{
		utility::string_t ntrip = rtkStatus.at(L"ntripStat").as_string();
		std::string ntripString = utility::conversions::to_utf8string(ntrip);
		strcpy(ntripState, ntripString.c_str());
	}
	
	return true;
}


void TControlRobot::ResetGroundFrame()
{
	if (!RobotIsConnected()) return;
	m_client_manager->resetErxGroundFrame();
}

// mouvement simple
void TControlRobot::Avancement(double distance)
{
	if(RobotIsConnected())
		m_client_manager->moveStraight(distance);
}

void TControlRobot::RotateAngle(double angle, double radius)
{
	if (RobotIsConnected())
		m_client_manager->rotateAngle(angle, radius);
}

void TControlRobot::StopMouvement()
{
	if (RobotIsConnected())
		m_client_manager->stopMovement();
}


// envoi des consignes moteurs gauche et droite
void TControlRobot::SetRobotSpeed(double vL, double vR)
{
	if (RobotIsConnected())
		m_client_manager->setSpeed(vL, vR);
}


void TControlRobot::SendHWCommand(std::string cmd)
{
	utility::string_t command = utility::conversions::to_string_t(cmd);
	m_client_manager->sendHWcommand(command);
}


// initialisation de bras
void TControlRobot::InitRightArm()
{
	std::string cmd = "d2on";
	SendHWCommand(cmd);

	cmd = "d2home";
	SendHWCommand(cmd);

	char msg_cmd[100];
	sprintf(msg_cmd, "d2tz %f", POSITIONZ_POSITIONING);
	SendHWCommand(msg_cmd);

	ArmInitialised = true;
}

void TControlRobot::InitLeftArm()
{
	std::string cmd = "d1on";
	SendHWCommand(cmd);

	cmd = "d1home";
	SendHWCommand(cmd);

	char msg_cmd[100];
	sprintf(msg_cmd, "d1tz %f", POSITIONZ_POSITIONING);
	SendHWCommand(msg_cmd);
}


// reset de bras
void TControlRobot::ResetRightArm()
{
	std::string cmd = "d2reset";
	SendHWCommand(cmd);

	cmd = "d2home";
	SendHWCommand(cmd);
}

void TControlRobot::ResetLeftArm()
{
	std::string cmd = "d1reset";
	SendHWCommand(cmd);

	cmd = "d1home";
	SendHWCommand(cmd);

}


// fermeture de bras
void TControlRobot::CloseRightArm()
{
	std::string cmd = "d2reset";
	SendHWCommand(cmd);

	cmd = "d2home";
	SendHWCommand(cmd);

	cmd = "d2off";
	SendHWCommand(cmd);
}

void TControlRobot::CloseLeftArm()
{
	std::string cmd = "d1reset";
	SendHWCommand(cmd);

	cmd = "d1home";
	SendHWCommand(cmd);

	cmd = "d1off";
	SendHWCommand(cmd);
}



//Callback pass� � l'API lorsque l'on demande le mouvement du bras. Il permet de prendre en compte un �ventuel probl�me
void callbackMouvementBras(int i) {
	switch (i) { //note: i est un enum en r�alit�
	case 1: callbackError << " OK -";  return; //pas d'erreur 
	case 0: callbackReturn = false; callbackError << " timeout bras -"; return; //TIMEOUT
	case 2: callbackReturn = false; callbackError << " erreur -";  return; //DERR
	}
}

//D�placement du bras sur l'un des axes avec utilisation d'un callback pour g�rer l'�tat en retour
void TControlRobot::SetPositionAxis(int axis, float pt) {
	boost::timer::cpu_timer timer; //le chrono d�marre d�s que l'objet est instanti�
	callbackError << "\n D�placement axe: " << axis << "- ";
	if (callbackReturn) {
		m_client_manager->setD2Position(axis, pt, callbackMouvementBras);
	}
	callbackError << "Chrono callback:"<< timer.format() <<"\n"; //ajout du temps d'appel du callback, en r�el et en CPU (pour info seulement, c'est la partie ERX qui g�re son propre timer)
}






bool TControlRobot::SetPositionXYWeedR(POINTFLT weed, POINTFLT posActuelle)
{ 	//utilise la nouvelle version de l'API d'Octobre 2019 avec impl�mentation callback

	callbackError.str(std::string()); //initialisation message erreur
	callbackError << "Callback return:";
	callbackReturn = true; //on consid�re que les op�rations vont �tre r�alis�es sans erreur
	//le bool�en passe � false si une des op�rations se passe mal => les d�placements suivants ne sont pas r�alis�s


	//DEPLACEMENT AXE Z
	//=================
	if (callbackReturn) { SetPositionAxis(2, POSITIONZ_POSITIONING); }  //position de base lors du d�placement du bras
	//=================

	//DEPLACEMENT AXES X ET Y
	//=================
	//IMPORTANT: l'ordre de la s�quence est adapt� pour ne pas sortir de l'hexagone
	if ((posActuelle.y > 0.25 && (posActuelle.y > weed.y)) || (posActuelle.y < -0.25 && (posActuelle.y < weed.y))) {
		if (callbackReturn && (weed.y != posActuelle.y)) { SetPositionAxis(1, weed.y); }
		if (callbackReturn && (weed.x != posActuelle.x)) { SetPositionAxis(0, weed.x); }
	}
	else {
		if (callbackReturn && (weed.x != posActuelle.x)) { SetPositionAxis(0, weed.x); }
		if (callbackReturn && (weed.y != posActuelle.y)) { SetPositionAxis(1, weed.y); }
	}
	//=================

	DBOUT(callbackError.str());
	if (!callbackReturn) { //une op�ration s'est mal pass�e
		return false;
	}
	return true;
}


bool TControlRobot::SetPositionZWeedingR(double Z)
{
	callbackReturn = true;
	callbackError.str(std::string()); //initialisation message erreur
	callbackError << "Callback return:";
	SetPositionAxis(2, Z);
	DBOUT(callbackError.str());
	if (!callbackReturn) { //une op�ration s'est mal pass�e
		std::string callbackErrorStr = callbackError.str();
		return false;
	}
	return true;
}

bool TControlRobot::SetPositionZWeedingR_pallier(float intervaleDescente,float debutH,float limitH,float p)
{
	for (float h = debutH; h  <= limitH; h=h + intervaleDescente) {
		//if (rand() / double(RAND_MAX) < p) { break; }
		DBOUT("nouvelle descente � h= "<<h << "\n");
		callbackReturn = true;
		callbackError.str(std::string()); //initialisation message erreur
		callbackError << "Callback return:";
		SetPositionAxis(2, h);
		DBOUT(callbackError.str());
		if (!callbackReturn) { //une op�ration s'est mal pass�e
			std::string callbackErrorStr = callbackError.str();
			return false;
		}
	}
	return true;
}


bool TControlRobot::SetHeightR(double dis)
{
	char cmd[100];
	sprintf(cmd, "d2tz %f", dis);
	SendHWCommand(cmd);
	return true;
}


//Permet d'assurer le d�placement au 0 entre deux d�placements
void TControlRobot::SetPositionAwaitedR()
{
	SetPositionZWeedingR(POSITIONZ_POSITIONING);
	Sleep(DELAY_POSITIONING);

	SendHWCommand("d2tx 0");
	SendHWCommand("d2ty 0");

	Sleep(DELAY_POSITIONING);

	return;
}






//Gauche

void TControlRobot::SetPositionXYWeedL(POINTFLT weed)
{
	char msg_cmd[100];

	sprintf(msg_cmd, "d1tz %f", POSITIONZ_POSITIONING);
	SendHWCommand(msg_cmd);

	sprintf(msg_cmd, "d1tx %f", weed.x);
	SendHWCommand(msg_cmd);
	sprintf(msg_cmd, "d1ty %f", weed.y);
	SendHWCommand(msg_cmd);

	return;
}


void TControlRobot::SetPositionZWeedingL(double z)
{
	//SetHeightL(POSITIONZ_WEEDING);
	SetHeightL(z);
	Sleep(DELAY_POSITIONING);
}


void TControlRobot::SetHeightL(double dis)
{
	char cmd[100];
	sprintf(cmd, "d1tz %f", dis);
	SendHWCommand(cmd);
}


void TControlRobot::SetPositionAwaitedL()
{
	SetHeightL(POSITIONZ_POSITIONING);
	Sleep(DELAY_POSITIONING);

	SendHWCommand("d1tx 0");
	SendHWCommand("d1ty 0");

	Sleep(DELAY_POSITIONING);

	return;
}


//Vieilles fonctions
//====================
// weeding 
void TControlRobot::TestWeedingRightArm(_list<POINTFLT> weeds, double z)
// fonction test pour le weeding : simple tempo � la place de la d�charge �lectrique
{
	char msg_cmd[100];

	BalayeListe(weeds)
	{
		POINTFLT *Weed = weeds.current();
		sprintf(msg_cmd, "d2tx %f", Weed->x);
		SendHWCommand(msg_cmd);
		sprintf(msg_cmd, "d2ty %f", Weed->y);
		SendHWCommand(msg_cmd);

		Sleep(DELAY_POSITIONING);

		sprintf(msg_cmd, "d2tz %f", z);
		SendHWCommand(msg_cmd);
		Sleep(DELAY_WEEDING);

		sprintf(msg_cmd, "d2tz %f", POSITIONZ_POSITIONING);
		SendHWCommand(msg_cmd);
		Sleep(DELAY_POSITIONING);
	}

	return;
}

void TControlRobot::TestWeedingLeftArm(_list<POINTFLT> weeds, double z)
{
	char msg_cmd[100];

	BalayeListe(weeds)
	{
		POINTFLT *Weed = weeds.current();
		sprintf(msg_cmd, "d1tx %f", Weed->x);
		SendHWCommand(msg_cmd);
		sprintf(msg_cmd, "d1ty %f", Weed->y);
		SendHWCommand(msg_cmd);
		Sleep(DELAY_POSITIONING);

		sprintf(msg_cmd, "d1tz %f", z);
		SendHWCommand(msg_cmd);
		Sleep(DELAY_WEEDING);

		sprintf(msg_cmd, "d1tz %f", POSITIONZ_POSITIONING);
		SendHWCommand(msg_cmd);
		Sleep(DELAY_POSITIONING);
	}

	return;
}
