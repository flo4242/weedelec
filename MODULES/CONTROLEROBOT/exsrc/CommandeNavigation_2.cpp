#include "ControlRobot.h" 
// il faut toujours inclure "ControlRobot.h" en premier pour que "erxCom.h" soit d�clar� en premier 
// (sinon incompatibilit� se trouve pour le module 'chrono') 

#include "CommandeNavigation_2.h"
#include "ExportModuleControleRobot.h"
#include "Plateforme_2.h"


#define GAIN_ORIENT  0.4  // gain de commande pour l'orientation
#define RAYON_RATTRAPAGE  1.0  // rayon de rattrapage en m�tres pour le suivi chemin
#define THETA_CST  (25.0*(M_PI/180))  // constante pour la fonction exponentielle : v(err_theta) = Vmax * exp{-err_theta / THETA_CST}



extern TControlRobot ControlRobotG; // structure globale pour appeler l'API Ecorobotix
extern TRepereLocal* RepereG;
extern TParamRobot ParamG;

extern bool StopUrgenceG;  // arr�t d'urgence
extern int TimeDebutG; // temps r�el pour la sauvegarde de fichier log
 
TCommandeNavigation *CommandeNavigationG = NULL;

#include "GereFichierLog.h"
#include "KML.h"

TCommandeNavigation::~TCommandeNavigation()
{
	while (CallbackTimerEnCours) { ; }
	StopTimer();
	if (Timer_Commande) delete Timer_Commande;
	if (pFichierLog)
	{
		pFichierLog->close(); delete pFichierLog;

		//stockage suppl�mentaire en kml
		_list<Waypoint> ListeW;
		char msg[1000];
		if (TGereFichierLog::ImporteLog(NfLog, ListeW, msg))
		{
			NomSansExt(NfLog);
			char NfLS[1000];	sprintf(NfLS, "%s_LS.kml", NfLog);
			char NomDocument[1000]; strcpy(NomDocument, NomSansPath(NfLS));	NomSansExt(NomDocument);
			ExportLineStringKML(NfLS, NomDocument, ListeW, msg);

			//export GPX contenant les timestamp pour g�otagging
			char NfGPX[1000];	sprintf(NfGPX, "%s.gpx", NfLog);
			ExportGPX(NfGPX, NomDocument, ListeW, msg);
		}

	}
}


struct TStep //structure permettant l'avanc�e pas � pas le long d'une trajectoire
{
	POINTFLT Debut;  // position de d�but du step
	double LongueurMetres;
};

bool TestArret(POINTFLT Ptprecedent, POINTFLT Ptcourant, double distancemax, double &kR);


//////////////////////////////////////////////
/*******fonctions externes utilis�es*********/

// fonction retour Plateforme error
void RetourPlateformeError(TRetourPlateforme &ret, int timedebut, char* infoRet)
{
	ret.State = TRetourState::Error;
	ret.InfoRetour = infoRet;
	ret.DureeMs = GetTickCount() - timedebut;
}

// fonctions pour la sauvegarde de fichier log


void SauveFichierLog(ofstream *pOs, double Ts, char *UTCTime, POINTFLT CMoteurs, TPositionRobot Pos, double capEstime)
// format de fichier log:  Time	Consigne moteurs	Position robot (X, Y, cap)	Coordonn�es GPS
{
	char temp[100];
	double LatMes, LonMes;  // latitude et longitude mesur�es

	if (RepereG == NULL) return;

	RepereG->MetresVersWGS84(LatMes, LonMes, Pos.x, Pos.y);

	TGereFichierLog::WriteLine(pOs, Ts, UTCTime, CMoteurs.x, CMoteurs.y, Pos.x, Pos.y, Pos.CapDeg, capEstime, LatMes, LonMes, Pos.fixGauche, Pos.fixDroit);
}



/////////////////////////////////////////
/********* Fonction callback ***********/
/////////////////////////////////////////


auto callback_commande = new call<bool>([](bool)
// �tabli la fonction callback pour la commande (appel�e selon la p�riode de commande param�tr�e)
{
	/* cette fonction timer maintient l'envoi r�gulier des consignes moteur d'avancement (sinon les
	 moteurs s'arr�tent).
	 En mode auto, elle g�re aussi le d�tail du rattrapage de trajectoire
	 */

	if (CommandeNavigationG == NULL)	return;
	//dans certains cas, en fin de parcours, le callback est appel� apr�s destruction (empilage des appels ?)

	CommandeNavigationG->CallbackTimerEnCours = true;

	if (CommandeNavigationG->CommandeManuelle)
	// si commande manuelle, les consignes de moteurs sont donn�es par le panneau de contr�le
	{
		TCommandeManuelle *pComManuelle = dynamic_cast <TCommandeManuelle*> (CommandeNavigationG);
		if (pComManuelle == NULL) // v�rification de dynamic_cast de pointeur
		{
			CommandeNavigationG->StopTimer();
			CommandeNavigationG->CallbackTimerEnCours = false;
			return;
		}

		if ((GetAsyncKeyState(' ') & 0x8000) != 0)
		{
			pComManuelle->StopVitesse(); // arr�t du robot
		}

		POINTFLT ComMot = pComManuelle->GetVitesseCommandeManuelle();
		ControlRobotG.SetRobotSpeed(ComMot.x, ComMot.y); // envoi de consigne moteurs

		if (CommandeNavigationG->pFichierLog != NULL) // sauvegarde fichier log
		{
			int now = GetTickCount();
			int &precedent = CommandeNavigationG->TickLogPrecedent; //facilit� �criture

			if ((precedent == -1) || (now - precedent) >= CommandeNavigationG->PeriodeLog)
				//premier passage ou dernier log obsol�te --> on enregistre ce passage
			{
				// NB: le seul cas en commande manuelle o� on a besoin de la position actuelle
				//si non accessible � cet appel timer, ce n'est pas r�dhibitoire --> on se contente de ne rien stocker cette fois-ci
				char msgerr[1000];
				TPositionRobot posRobot;

				char UTCTime[50];

				if (PositionRobotEstimee(posRobot, msgerr, UTCTime))
				{
					SauveFichierLog(CommandeNavigationG->pFichierLog, double(now - TimeDebutG) / 1000, UTCTime, ComMot, posRobot,0);
					precedent = now;
				}

			}
		}

		CommandeNavigationG->CallbackTimerEnCours = false; //on a fini ce cycle
		
	}

	else // commande automatique de suivi de chemin
	{
		int TimeDebutCycle = GetTickCount();

		TCommandeAutomatique *pComAuto = dynamic_cast <TCommandeAutomatique*> (CommandeNavigationG);
		
		TRetourPlateforme ret;
		ret.CropRowFinished = false;

		if (pComAuto == NULL) // v�rification du dynamic_cast de pointeur
		{
			CommandeNavigationG->StopTimer();
			
			RetourPlateformeError(ret, TimeDebutCycle, "Pointeur commande non conforme!");
//			if(pComAuto->callbackRetour != NULL)	 pComAuto->callbackRetour(ret);
			//impossible � appeler puisque pComAuto NULL !!!!
			CommandeNavigationG->CallbackTimerEnCours = false;
			return;
		}

		//test direct d'arr�t au clavier
		if ((GetAsyncKeyState(' ') & 0x8000) != 0)
		{
			pComAuto->StopTimer(); //arr�t du timer
			ControlRobotG.SetRobotSpeed(0, 0);

			ret.State = TRetourState::ExternalStop;
			ret.DureeMs = GetTickCount() - TimeDebutG;
			ret.InfoRetour = "Stop manuel !";
			if (pComAuto->callbackRetour != NULL)	pComAuto->callbackRetour(ret);
			CommandeNavigationG->CallbackTimerEnCours = false;
			return;
		}

		////////////////////////////
		//suivi du segment courant
		///////////////////////////
				
		POINTFLT P1 = pComAuto->pTrajectoire->P1;
		POINTFLT P2 = pComAuto->pTrajectoire->P2;
		
		// lecture position actuelle, et stockage dans la variable PosActuelle
		char msgerr[1000];
		char UTCTime[50];
		
		if (!PositionRobotEstimee(pComAuto->PosActuelle, msgerr, UTCTime))
		{
			CommandeNavigationG->StopTimer();
			RetourPlateformeError(ret, TimeDebutCycle, msgerr);
			if (pComAuto->callbackRetour != NULL)	 pComAuto->callbackRetour(ret);
			CommandeNavigationG->CallbackTimerEnCours = false;
			return;
		}

		//calcul et envoi des consignes moteur temps r�el

		POINTFLT Pactuel(pComAuto->PosActuelle.x, pComAuto->PosActuelle.y); // point actuel
		double CapDesireDeg = pComAuto->CalculCapDesire(P1, P2, Pactuel); // cap d�sir� � cet instant
		

		
		//on va maintenant tester les conditions de changement de segment et d'arr�t
		////////////////////////////////////////////////////////////////////////////

		double kRStep = 1;
		double kRSegment = 1;

		bool FinStep = pComAuto->Step == NULL ? false : TestArret(pComAuto->DebutStep, Pactuel, *pComAuto->Step, kRStep);
		bool FinSegment = TestArret(P1, Pactuel, pComAuto->pTrajectoire->LongueurSegment, kRSegment);
		//modif supprim�e car entraine trop de retard de rattrapage lors des changemensts de direction: 
		//if (pComAuto->pTrajectoire->IndexCourant < pComAuto->pTrajectoire->NbPoints - 2) //nbPoints -2: dernier index			kRSegment = 1;
		//ce n'est pas le dernier: on ne ralentit pas !!!

		POINTFLT ComMot = pComAuto->CalculConsignesMoteurs(CapDesireDeg, pComAuto->PosActuelle.CapDispo)* min(kRStep, kRSegment);

		ControlRobotG.SetRobotSpeed(ComMot.x, ComMot.y); // envoie la consigne de moteurs, avec att�nuation si on s'approche de l'arr�t

		//fichier log
		if (pComAuto->pFichierLog != NULL) // enregistrement consignes et position robot dans le fichier log
			SauveFichierLog(pComAuto->pFichierLog, double(GetTickCount() - TimeDebutG) / 1000, UTCTime, ComMot, pComAuto->PosActuelle, CapDesireDeg);

		if (FinSegment) //� faire avant les tests d'arr�t sur step
			if(!pComAuto->pTrajectoire->SegmentSuivant()) //on a fini la trajectoire
			{
				pComAuto->StopTimer(); //arr�t du timer
				ControlRobotG.SetRobotSpeed(0, 0);
				ret.CropRowFinished = true;
				ret.State = TRetourState::Completed;
				ret.DureeMs = GetTickCount() - TimeDebutG;
				ret.InfoRetour = "Trajectoire termin�e";
				if (pComAuto->callbackRetour != NULL)	pComAuto->callbackRetour(ret);
				CommandeNavigationG->CallbackTimerEnCours = false;
				return;
			}

			else //affichage nouveau segment
			{
				if (pComAuto->pDriver)
				{
					POINTFLT &newP1 = pComAuto->pTrajectoire->P1;
					POINTFLT &newP2 = pComAuto->pTrajectoire->P2;
					pComAuto->pDriver->PrintTexte("Segment n� %d: P1=(%.2f, %.2f) P2=(%.2f, %.2f) Longueur: %.2f\n",
						pComAuto->pTrajectoire->IndexCourant + 1, newP1.x, newP1.y, newP2.x, newP2.y, (newP2 - newP1).Norme());
				}

			}

		if(FinStep || StopUrgenceG) // Arr�t de la t�che
		{
			pComAuto->StopTimer(); //arr�t du timer
			ControlRobotG.SetRobotSpeed(0, 0);

			ret.State = StopUrgenceG ? TRetourState::ExternalStop : TRetourState::Completed;
			ret.DureeMs = GetTickCount() - TimeDebutG;
			ret.InfoRetour = StopUrgenceG ? "Stop Urgence!" : "Step termin�";
			if (pComAuto->callbackRetour != NULL)	pComAuto->callbackRetour(ret);
			CommandeNavigationG->CallbackTimerEnCours = false;
			return;
		}

	}
});


/////////////////////////////////////////////////////////////////////////////////////////////////////
/********************************Commande navigation - classe de base ******************************/

void TCommandeNavigation::SetTimer()
{
	//cr�e le timer pour la commande de suivi de chemin
	Timer_Commande = new timer<bool>(PeriodeCommande, NULL, callback_commande, true);
	StartTimer();
}

void TCommandeNavigation::StartTimer()
// start timer
{
	if (Timer_Commande != NULL)		Timer_Commande->start();
}

void TCommandeNavigation::StopTimer()
{
	if (Timer_Commande != NULL)		Timer_Commande->pause();
}





//////////////////////////////////////////////////////////////////////////////////
/********************************Commande manuelle ******************************/

bool TCommandeManuelle::DoStartCommande(char *msgerr)
{
	if (Timer_Commande)		delete Timer_Commande;
	SetTimer();
	
	if (Timer_Commande == NULL)
	{
		sprintf(msgerr, "Set Timer �chou�");
		return false;
	}

	return true;
}

void TCommandeManuelle::VitesseNominaleAdd()
{
	double NouvelleVitesse = VitesseNominale;
	NouvelleVitesse += UNITE_VITESSE;
	NouvelleVitesse = min(NouvelleVitesse, SEUIL_VITESSE);
	VitesseNominale = NouvelleVitesse;
}

void TCommandeManuelle::VitesseNominaleSub()
{
	double NouvelleVitesse = VitesseNominale;
	NouvelleVitesse -= UNITE_VITESSE;
	NouvelleVitesse = max(NouvelleVitesse, -SEUIL_VITESSE);
	VitesseNominale = NouvelleVitesse;
}

void TCommandeManuelle::VitesseDiffAdd() 
{ 
	double NouvelleVitesse = VitesseDifferentielle;
	NouvelleVitesse += UNITE_VITESSE;
	NouvelleVitesse = min(NouvelleVitesse, SEUIL_VITESSE);
	VitesseDifferentielle = NouvelleVitesse;
}

void TCommandeManuelle::VitesseDiffSub()
{
	double NouvelleVitesse = VitesseDifferentielle;
	NouvelleVitesse -= UNITE_VITESSE;
	NouvelleVitesse = max(NouvelleVitesse, -SEUIL_VITESSE);
	VitesseDifferentielle = NouvelleVitesse;
}


POINTFLT TCommandeManuelle::GetVitesseCommandeManuelle()
{
	double vg = VitesseNominale - VitesseDifferentielle;
	double vd = VitesseNominale + VitesseDifferentielle;
	
	// mise en place du seuil de vitesse
	double attenuation = 1;
	if (abs(vg) > SEUIL_VITESSE) attenuation = min(attenuation, SEUIL_VITESSE / abs(vg));
	if (abs(vd) > SEUIL_VITESSE) attenuation = min(attenuation, SEUIL_VITESSE / abs(vd));
	
	return POINTFLT(vg, vd)*attenuation;
}






/////////////////////////////////////////////////////////////////////////////////////////////////////
/********************************Commande Automatique de navigation*********************************/

// lance la commande automatique


bool TCommandeAutomatique::DoStartCommande(char* msgerr)
{
	if (Timer_Commande) 	delete Timer_Commande;
	SetTimer();

	if (Step != NULL)
	{
		TPositionRobot Pos;
		if (!PositionRobotEstimee(Pos, msgerr))	return false;

		DebutStep = POINTFLT(Pos.x, Pos.y);
	}

	return true;
}



/*********************Suivi de chemin effectu� par un suivi de cap d�sir�***************************/

// v�rification de la distance d'avancement du robot par rapport au point pr�c�dent
bool TestArret(POINTFLT Ptprecedent, POINTFLT Ptcourant, double distancemax, double &kR)
{

	//ao�t 2019: on inclut un facteur de r�duction de la vitesse lorsqu'on approche

	double ecart = distancemax - (Ptcourant - Ptprecedent).Norme(); //distance restante
	if (ecart > 0 && ecart <2) //On autorise une tol�rance dans l'arriv�e du robot 
	{
		kR = exp(-ParamG.ConstanteRalentissement / ecart); //d'autant plus faible que l'on approche
		kR = max(kR, ParamG.RalentissementMin);
		return false;
	}
	else if (ecart >= 2) {
		kR = exp(-ParamG.ConstanteRalentissement / ecart);
		return false;
	}
	else if (ecart <=0) {
		return true;
	}


	/////return ((Ptcourant - Ptprecedent).Norme2() >= distancemax*distancemax);
	//permet au robot d'aller jusqu'au bout (si pas d'erreur de cap et pas de rattrapage)
}


// commande cin�matique sur le suivi de l'orientation
double GetVitesseLineaireSelonErreurCap(double ErrCapRad, double vitessenominale)
// Calculer la vitesse d'avanc�e � partir de l'erreur de l'orientation d�sir�e ()
// Fonction exponentielle : y(theta) = V*exp{-theta/theta_c} ;
{
	
	return vitessenominale * exp(-ErrCapRad / (THETA_CST));
}

POINTFLT TCommandeAutomatique::CalculConsignesMoteurs(double CapDesireDeg, bool CapDispo)
// Commande cap : 1/E*(vD - vG) = K*(theta_desire - theta_actuel)
// Commande vitesse d'avancement V = 1/2*(vG + vD)  modul�e en fonction de l'erreur de suivi de cap)
// Consigne de vitesse : vG = V - E/2*K*(theta_desire - theta_actuel), vD = V + E/2*K*(theta_desire - theta_actuel)
{

	double vg, vd;

	if (CapDispo)
	{
		double ErrOrient = (CapDesireDeg - PosActuelle.CapDeg)*M_PI / 180;
		
		if (abs(ErrOrient + 2 * M_PI) < abs(ErrOrient)) //v�rification de la zone de discontinuit�
			ErrOrient += 2 * M_PI;
		else if (abs(ErrOrient - 2 * M_PI) < abs(ErrOrient))
			ErrOrient -= 2 * M_PI;

		


		double consignV_theta = ENTRAXE_ROBOT / 2 * GAIN_ORIENT * ErrOrient;

		double V = GetVitesseLineaireSelonErreurCap(abs(ErrOrient), vitesseNominale);


		vg = V - consignV_theta;
		vd = V + consignV_theta;
	}

	else
	{
		vg = vd = vitesseNominale; //on va tout droit
	}

	// mis en place du seuil de vitesse
	double kR = 1;
	if (abs(vg) > SEUIL_VITESSE) kR = min(kR, SEUIL_VITESSE / abs(vg));
	if (abs(vd) > SEUIL_VITESSE) kR = min(kR, SEUIL_VITESSE / abs(vd));
	return POINTFLT(vg, vd)*kR;
}



double  TCommandeAutomatique::CalculCapDesire(POINTFLT P1, POINTFLT P2, POINTFLT Pactuel)
// calcul le cap d�sir� � partir du segment de chemin actuel (d�fini par P1, P2) et du point actuel
{

	POINTFLT U = (P2 - P1) / (P2 - P1).Norme(); //vecteur unitaire su segment P1P2
	POINTFLT P1C = Pactuel - P1;
	POINTFLT PH = P1 + U * (P1C | U);  //projection de Pactuel sur le segment P1P2

	double dist = (Pactuel - PH).Norme(); //distance entre Pactuel et le segment P1P2
	
	POINTFLT PCible;

	double longueur2 = RAYON_RATTRAPAGE * RAYON_RATTRAPAGE - dist * dist;

	if (longueur2 <= 0)
		PCible = PH;
	else
		PCible = PH + U * sqrt(longueur2);


	return DirectionDegVersPointDistant(Pactuel, PCible);
	
	/*double CapDegDesire = atan2(Pactuel.x - PCible.x, PCible.y - Pactuel.y) * 180 / M_PI;
	//int�gre le passage angle trigo --> cap
	return CapDegDesire;*/
}
