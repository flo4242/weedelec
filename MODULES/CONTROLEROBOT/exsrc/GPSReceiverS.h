#pragma once

/////////////////////////////////////////////////////////////////////////
//Acc�s aux donn�es du syst�me � deux gps Emlid pilot� par Rasberry dedi�
/////////////////////////////////////////////////////////////////////////

#include <string>
#include <ostream>

#include "RepereLocal.h"
#include "PositionRobot.h"
#include "NmeaPositionTCPIP.h"



#include "DBOUT.h"




class GPSReceiverSeptentrio
{
public:

	GPSReceiverSeptentrio() : pNmeaPos(NULL), Connecte(false) {; }
	~GPSReceiverSeptentrio() { if (pNmeaPos != NULL) delete pNmeaPos; }

	bool GetRawData(NMEAData& data, char* msgerr)
	{
		NMEAData databrut;

		

		NmeaPositionState state = pNmeaPos->GetData(databrut);
		if (state != NmeaPositionState::OK)
		{
			sprintf(msgerr, pNmeaPos->GetStateString().data());
			return false;
		}

		data = databrut;
		//Correction du cap du GPS:
		// - passage dans le sens antihoraire
		// - Application d'un offset
		data.CapDeg = 360 - databrut.CapDeg;
		if (data.CapDeg > 180) data.CapDeg -= 360;
		if (data.CapDeg < -180) data.CapDeg += 360;

		//correction antenne -> centre robot (tir� de RepereLocal.cpp)
		float a = 6378137.0;
		float b = 6356752.314;	//donn�es WGS84

		double cosL = cos(databrut.LatDeg*(M_PI / 180));
		double sinL = sin(databrut.LatDeg*(M_PI / 180));

		double RlocalH = a * cosL;
		POINTFLT R(a*cosL, b*sinL);
		POINTFLT T(-a * sinL, b*cosL);
		double sinBeta = (R^T) / (R.Norme()*T.Norme());
		double RlocalV = R.Norme() / sinBeta;

		//correction � �tablir en m�tres
		float offset = 1.12; //selon axe X du robot
		double thetarad = data.CapDeg * (M_PI / 180);

		double dx = -offset * cos(thetarad);
		double dy = -offset * sin(thetarad);

		double deltaLatRad = dy / RlocalV;
		double deltaLonRad = dx / RlocalH;

		
		data.LatDeg += deltaLatRad * (180 / M_PI);
		data.LonDeg += deltaLonRad * (180 / M_PI);
		return true;

	}
	bool GetRobotPosition(TRepereLocal Repere, TPositionRobot &pos, char* msgerr)
	{
		pos.CapDispo = true; //toujours vrai avec le Septentrio

		NMEAData Data;

		if (!GetRawData(Data, msgerr))	return false;

		pos.CapDeg = Data.CapDeg;

		pos.fixDroit = pos.fixGauche = Data.Fix; //indiff�renci�s sur le Septentrio

		return true;
	}


	bool Connect(std::string adresse, int port, char* msgerr)
	{
		if (pNmeaPos != NULL)	delete pNmeaPos;
		pNmeaPos = new NmeaPositionTCPIP((char*)adresse.data(), port, 1000);

		NMEAData Data;		NmeaPositionState state =  pNmeaPos->GetData(Data);

		if (state == NmeaPositionState::ConnectionError)
		{
			sprintf(msgerr, "Echec connexion Septentrio � l'adresse %s:%d", adresse.data(), port);
			Connecte = false;
			return false;
		}

		Connecte = true;
		return true;

	}

	bool IsConnected()	{ return Connecte;	}


private:

	NmeaPositionTCPIP *pNmeaPos;
	bool Connecte;
	
};



