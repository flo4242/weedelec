
#include <string>
#include <ostream>

#include "HttpClient.h"


http_client SetHTTPClient(std::string uri)
{
	utility::string_t uri_stringt = utility::conversions::to_string_t(uri);
	http_client client(uri_stringt);

	return client;
}

bool GetHTTPResponse(http_client client, std::string& res)
{
	http_response response;
	utility::string_t resp;

	//try
	//{
		// ordinary `get` request
		response = client.request(methods::GET).get();
		web::json::value resjson = response.extract_json().get();
		resp = resjson.at(L"age.gauche").as_string();
	//}
		res = utility::conversions::to_utf8string(resp);
	//catch (std::exception e)
	//{
		//res = "Echec Get Response";
		//return false;
	//}
	
	// traduction des donn�es depuis le json
	return true;
}


