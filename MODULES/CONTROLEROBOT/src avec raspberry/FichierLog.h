// structure pour la gestion des fichiers log

#pragma once

#include <ofstream>;
#include "list_tpl.h"
#include "Waypoint.h"

struct TFichierLog
{
	static void SetLigneTitreLog(std::ofstream *os);

	static ofstream * InitFichierLog(char *DossierLog);// initialise le fichier log

	static bool ImporteLog(char *NfLog, _list<Waypoint> &ListeW, char *msgerr);

	static void WriteLine(int time, double CG, double CD, double x, double y, double Cap, double Lat, double Lon, int fixG, int fixD);
	/*sprintf(temp, "%.2f\t%.2f\t%.2f\t%.3f\t%.3f\t%.3f\t%.16f\t%.16f\t%d\t%d\n",
		Ts, CMoteurs.x, CMoteurs.y, Pos.x, Pos.y, Pos.CapDeg, LatMes, LonMes, Pos.fixGauche, Pos.fixDroit);
	*pOs << temp;*/
};
