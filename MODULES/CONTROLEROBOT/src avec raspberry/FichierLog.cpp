
// fonctions pour la sauvegarde de fichier log

void TCommandeNavigation::SetLigneTitreLog(ofstream *os)
{

	sprintf(temp, "%.2f\t%.2f\t%.2f\t%.3f\t%.3f\t%.3f\t%.16f\t%.16f\t%d\t%d\n",
		Ts, CMoteurs.x, CMoteurs.y, Pos.x, Pos.y, Pos.CapDeg, LatMes, LonMes, Pos.fixGauche, Pos.fixDroit);
	*pOs << temp;

}

ofstream * TCommandeNavigation::InitFichierLog(char *DossierLog) // initialise le fichier log
{
	TNomFic NomFichierLog;

	time_t datetime = time(NULL);
	struct tm *ldt = localtime(&datetime);

	sprintf(NomFichierLog, "%s\\Log_%d-%d-%d_%d-%d-%d.log",
		DossierLog, ldt->tm_mday, ldt->tm_mon, ldt->tm_year, ldt->tm_hour, ldt->tm_min, ldt->tm_sec);

	ofstream * os = new std::ofstream(NomFichierLog, std::ofstream::out);

	if (!os->good()) return NULL;

	*os << "Time" << '\t' << "MoteurG" << '\t' << "MoteurD" << '\t' << "x" << '\t' << "y" << '\t' << "Cap" << '\t';
	*os << "Lat" << '\t' << "Lon" << '\t' << "FixG" << '\t' << "FixD" << '\n';


	return os;

}

