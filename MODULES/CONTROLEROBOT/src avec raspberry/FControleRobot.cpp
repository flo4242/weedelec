
		////////////////////////////////////
		#define NOM_MODULE "T_ControleRobot"
		////////////////////////////////////

#include "ControlRobot.h" 
// il faut toujours inclure "ControlRobot.h" en premier pour que "erxCom.h" soit d�clar� en premier 
// (sinon incompatibilit� se trouve pour le module 'chrono') 


#include "matrice.h"
//#include <boost/lexical_cast.hpp>
#include "ExportModuleControleRobot.h"

#include "WeedelecCommon.h"
#include "TControleRobot.h"
#include "Plateforme_2.h"
#include <time.h>
#include <stdio.h>
#include "CommandeNavigation_2.h"
#include "CommandeHT.h"
#include "GPSReceiver.h"
#include "GereFichierLog.h"
#include "KML.h"

#include <chrono>

DRVTRAITIM *pDriverG;

TControlRobot ControlRobotG;

TParamRobot ParamG;

extern TRepereLocal *RepereG;
extern TCommandeNavigation* CommandeNavigationG;
extern GPSReceiver GPSReceiverG;

CommandeHT CommandeHTG;
TParamHT ParamHTG;



bool GereParametresHT(HWND hwnd, ModeGereParametres modeGereParam, TParamHT &paramHT);


extern "C"
{
	int _EXPORT_ Open_T_ControleRobot(DRVTRAITIM *pD, HMODULE hinst)
		/*********************************************/
	{
		if (pD->SizeClass != sizeof(DRVTRAITIM))
		{
			char st[100];
			sprintf(st, "taille DRVTRAITIM non compatible - Driver: %d  DLL: %d",
				pD->SizeClass, sizeof(DRVTRAITIM));
			::MessageBox(NULL, st, NOM_MODULE, MB_OK);
			return 0;
		}
		pDriverG = pD;
		return 1;
	}

	int _EXPORT_ Close_T_ControleRobot(DRVTRAITIM *) { ReleasePlateforme();  return 1; }

	HMENU _EXPORT_ Ajoute_T_ControleRobot(HMENU pere)
	{
		HMENU Hprincipal = pDriverG->AjouteSousMenu(pere, "Controle Robot", 
			"Param�tres", &TPlateforme::CMParametre,
			NULL);

		HMENU ret = pDriverG->AjouteSousMenu(Hprincipal, "Initialisation Robot",
			"Initialisation", &TPlateforme::CMInitRobot,
			"Release", &TPlateforme::CMReleaseRobot,
			"Etat actuel", &TPlateforme::CMCheckInitRobot,
			"Lecture mesures robot", &TPlateforme::CMTestMesureRobot,
			/*"Active correction RTK", &TPlateforme::CMActiveRTKRobot,
			"Statut correction RTK", &TPlateforme::CMGetStatutRTKRobot,
			"Reset ground frame robot", &TPlateforme::CMResetGroundFrame,*/
			NULL);

		ret = pDriverG->AjouteSousMenu(Hprincipal, "Systeme GPS Rasberry",
			"Adresse et connexion", &TPlateforme::CMInitGPSRasberry,
			"Lecture mesures brutes", &TPlateforme::CMTestDonneesBrutesGPSRasberry,
			"Lecture position robot", &TPlateforme::CMTestPositionnementGPSRasberry,
			NULL);


		
		ret = pDriverG->AjouteSousMenu(Hprincipal, "Haute Tension",
			"Param�tres HT", &TPlateforme::CMParametreHT,
			"Initialisation HT", &TPlateforme::CMInitCommandeHT,
			"Lecture param�tres sur la carte HT", &TPlateforme::CMLireParametreHT,
			"Lecture condition de la carte HT", &TPlateforme::CMLireStatusHT,
			"Lance s�quence HT", &TPlateforme::CMLanceSequenceHT,
			NULL);

		ret = pDriverG->AjouteSousMenu(Hprincipal, "Weeding",
			"Contr�le de mouvement des bras", &TPlateforme::CMDialogueHWCommand,
			"D�placement manuel du bras droit avec la d�charge HT", &TPlateforme::CMDialogueControlBrasManuel,
			"S�quence weeding", &TPlateforme::CMStressTestWeeding,
			"Chrono s�quences al�atoires", &TPlateforme::CMTimerRandomWeeding,
			NULL);

		
		ret = pDriverG->AjouteSousMenu(Hprincipal, "Commandes de navigation",
			"Commande manuelle", &TPlateforme::CMNavigationManuelleRobot,
			"Suivi de trajectoire", &TPlateforme::CMNavigationAuto,
			"Relev� cap par d�faut", &TPlateforme::CMSetCapDefaut,
			"Conversion Log vers Kml", &TPlateforme::CMConversionLog,
			NULL);

		int Ajoutelist = pDriverG->AjouteListeItems(Hprincipal, "Stop Mouvement",
			&TPlateforme::CMStopMouvement, NULL);
	

		if (!TModulePlateforme::ChargeParametres(ParamG))
			pDriverG->Message("Echec chargement param�tres Robot");

		if(!GereParametresHT(NULL, ModeGereParametres::Charge, ParamHTG))
			pDriverG->Message("Echec chargement param�tres HT");
		
		
		InitPlateforme(pDriverG, ParamG, true); //plateforme_2.cpp

		return Hprincipal;
	}
}


//////////////////////////
// Gestion des param�tres
//////////////////////////

bool GereParametres(HWND hwnd, ModeGereParametres modeGereParam, TParamRobot &param) //export
/***********************************************************************************/
{

	GROUPE G(hwnd, "Param�tres Contr�le Robot ", "ControleRobot.cfg",

		SEPART("****************************"),
		SEPART("***     Param�tres utilisateur     ***"),
		SEPART("****************************"),
		DBL("Vitesse nominale d'avancement (m/s)", param.VitesseMaxLineaire, 0, 5),
		PBOOL("Trajectoire sur fichier (sinon, tout droit)", param.TrajectoireKML),
		DBL("Distance si trajectoire tout droit (m)", param.DistanceToutDroit, 0.1, 50),
		FICH("Trajectoire d�sir�e (kml ou log)", param.NfTrajectoire, "|KML|*.kml|Log|*.log|"),
		PBOOL("Enregistrement fichier log", param.AvecLog),
		REPI("Dossier log de navigation", param.DossierLog, true),
		ENT("P�riode de log (ms)", param.PeriodeLog, 100, 100000),
		DBL("Valeur Z pour weeding (en m�tres, croissant vers le bas)", param.ZWeeding,  0.85, 1),


		SEPART("      "),
		SEPART("****************************"),
		SEPART("***      Param�tres de d�tail        ***"),
		SEPART("****************************"),
		STRX("Adresse IP Wifi", param.AdresseConnexionWifi, 30),
		STRX("Adresse Ethernet", param.AdresseConnexionEth, 30),
		PBOOL("Connexion par Wifi", param.ConnexionWifi),

		STRX("Adresse Rasberry GPS", param.AdresseServeurGPS, 30),
		DBL("Tol�rance erreur distance inter-antennes (m)", param.ToleranceDistanceInterAntennes, 0.01, 5),


		ENT("P�riode de commande (ms)", param.PeriodeCommande, 10, 1000),
		DBL("Constante de ralentissement avant arr�t (m)", param.ConstanteRalentissement, 0.001, 1),
		DBL("Facteur de ralentissement max (sans unit�)", param.RalentissementMin, 0, 1),
		DBL("Rayon zone hexagonale du bras (m)", param.RayonHexagone, 0, 1E10),
		DBL("R�solution zone hexagonale du bras (m)", param.ResolutionHexagone, 0, 1E10),
		DBL("Translation X du centre bras dans rep�re robot (m)", param.TranslationDeltaX, -1E5, 1E5),
		DBL("Translation Y du centre bras dans rep�re robot (m)", param.TranslationDeltaY, -1E5, 1E5),
		PBOOL("Replacement au zero du bras entre chaque d�placement", param.getZero),
		PBOOL("Chemin le plus court (seulement si pas de replacement au zero)", param.getShortest),
		FICH("Fichier coordonn�es test weeding", param.fichierCoor, "txt"),
		PBOOL("Utilisation de l'hexagone r�el", param.hexagoneReel),
		NULL);

	bool OK;

	switch (modeGereParam)
	{
	case(ModeGereParametres::Charge): OK = G.Charge(); break;
	case(ModeGereParametres::Gere): OK = G.Gere(false); break; //modal pour pouvoir d�placer la bo�te de dialogue
	case(ModeGereParametres::Sauve): OK = G.Sauve(); break;
	}

	return OK;
}

void TPlateforme::CMParametre() {	GereParametres(HWindow(), ModeGereParametres::Gere, ParamG); }

bool TModulePlateforme::ChargeParametres(TParamRobot &param) { return GereParametres(NULL, ModeGereParametres::Charge, param); }

////////////////////////////////////////
// Initialisation et connexion du robot
////////////////////////////////////////

void TPlateforme::CMInitRobot()
{
	char msg[1000];
	
	TStateInitPlateforme ret = InitPlateforme(this, ParamG, false);
	
}

void TPlateforme::CMCheckInitRobot()
{
	TStateInitPlateforme ret = CheckInitPlateforme();
	char st[5000];
	ostrstream ost(st, 5000);

	ost << "Connexion robot initialis�e:       \t" << (ret.RobotConnecte ? "OUI" : "NON") << '\n';
	ost << "Connexion Rasberry GPS initialis�e: \t" << (ret.RasberryGPSConnecte ? "OUI" : "NON") << '\n';
	ost << "Rep�re local initialis�:            \t" << (ret.RepereLocalInitialise ? "OUI" : "NON") << '\n';

	ost << '\0';
	Message(st);
}



void TPlateforme::CMReleaseRobot()
{
	ReleasePlateforme();
	Message("Op�ration effectu�e");
}




void TPlateforme::CMTestMesureRobot()
{
	char msgerr[1000];

	if (!ControlRobotG.RobotIsConnected())
	{
		Message("Robot non connect�");
		return;
	}

	OuvreTexte();
	EffaceTexte();

	PrintTexte("Ici on teste les mesures de capteurs Ecorobotix\nTouche entr�e pour demarrer puis touche espace pour arr�ter\n");

	while (true)
	{
		if ((GetAsyncKeyState(VK_RETURN) & 0x8000) != 0)			break;
	}

	int TDebut = GetTickCount();
	while (true)
	{
		double latDeg, lonDeg, capDeg;
		if (!ControlRobotG.GetRobotSensorData(latDeg, lonDeg, capDeg, msgerr))
		{
			Message(msgerr); return;
		}

		PrintTexte("Time: %.3f, Mesure actuelle: Lat: %.8f;      Lon: %.8f;      Cap: %.5f\n", double(GetTickCount() - TDebut) / 1000, latDeg, lonDeg, capDeg);

		if ((GetAsyncKeyState(' ') & 0x8000) != 0) 			break;
	}
}



// activation GPS rtk et reception de statut 

void TPlateforme::CMActiveRTKRobot()
{
	if (!ControlRobotG.RobotIsConnected())
	{
		Message("Pas de connexion!");
		return;
	}
		

	ControlRobotG.ActiveRTK();
	Message("Activation RTK OK");
}

void TPlateforme::CMGetStatutRTKRobot()
{
	char msgerr[1000];
	char ntripMsg[100];
	char rtkMsg[100];

	if (!ControlRobotG.GetRTKStatus(ntripMsg, rtkMsg, msgerr))
		Message(msgerr);
	else
		Message("Ntrip message : %s\nRTK message : %s", ntripMsg, rtkMsg);
}

void TPlateforme::CMResetGroundFrame()
{
	if (!ControlRobotG.RobotIsConnected())
		return;

	ControlRobotG.m_client_manager->resetErxGroundFrame();
	Message("Reset ground frame OK");
}



///////////////////////////////////////////
// Capteur GPS Rasberry
///////////////////////////////////////////

void TPlateforme::CMInitGPSRasberry()
{
	char msgerr[1000];

	GROUPE G(HWindow(), "Connexion GPS Emlid", NULL,
		STR("Adresse de connexion du Rasberry", ParamG.AdresseServeurGPS), NULL);
	
	if (!G.Gere()) return;

	GereParametres(NULL, ModeGereParametres::Sauve, ParamG); //sauvegarde si modifi�

	if(!GPSReceiverG.Connect(ParamG.AdresseServeurGPS, msgerr))
	{
		Message(msgerr);
		return;
	}
	
	Message("Connexion �tablie pour l'adresse %s", ParamG.AdresseServeurGPS);
}

/*
void TPlateforme::CMTestDonneesBrutesGPSRasberry()
{
	char msgerr[1000];
	if (!GPSReceiverG.IsConnected())
	{
		Message("Pas de connexion �tablie");
		return;
	}

	
	OuvreTexte();
	EffaceTexte();

	//lancement
	int TDebut = GetTickCount();
	while (true)
	{
		MSG msg;
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 	DispatchMessage(&msg);

		GPSData gpsG, gpsD;
		if (!GPSReceiverG.GetGPSRawData(gpsG, gpsD, msgerr))
		{
			Message(msgerr); return;
		}

		PrintTexte("Time: %.3f, Mesure actuelle:\n GPS gauche: Age(%.3f);  Lat(%.8f);  Lon(%.8f);  Fix(%d)\n",
			double(GetTickCount() - TDebut) / 1000, gpsG.Age, gpsG.LatDeg, gpsG.LonDeg, gpsG.Fix);
		PrintTexte(" GPS droite: Age(%.3f);  Lat(%.8f);  Lon(%.8f);  Fix(%d)\n\n",
			gpsD.Age, gpsD.LatDeg, gpsD.LonDeg, gpsD.Fix);




		if ((GetAsyncKeyState(' ') & 0x8000) != 0) 			break;
	}

	PrintTexte("\n Arr�t utilisateur");


}*/

#include "NmeaPositionTCPIP.h"

void TPlateforme::CMTestDonneesBrutesGPSRasberry()
{
	char msgerr[1000];
	NmeaPositionTCPIP Pos((char*)"127.0.0.1", 28784);

	NMEAData Data;
	double lat0, lon0;
	double dx = 0;
	double dy = 0;

	bool premier = true;


	OuvreTexte();
	EffaceTexte();

	char st[5000];

	//lancement
	int TDebut = GetTickCount();
	while (true)
	{
		MSG msg;
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 	DispatchMessage(&msg);

		//tempo pour affichage
		int deb = GetTickCount();
		while (GetTickCount() - deb < 500) { ; }


		NmeaPositionState state = Pos.GetData(Data);


		ostrstream ost(st, 5000);

		ost.precision(10);

		if (state == NmeaPositionState::OK)
		{
			if (premier)
			{
				lat0 = Data.LatDeg;
				lon0 = Data.LonDeg;
				premier = false;
			}

			else
			{
				dx = (Data.LonDeg - lon0) * 60 * 1852 * cos(Data.LatDeg *3.1416 / 180.);
				dy = (Data.LatDeg - lat0) * 60 * 1852;
			}


			ost << "Lat: " << Data.LatDeg << '\t' << "  Lon: " << Data.LonDeg << '\t';
			ost << "  Altitude (m): " << Data.Altitude << '\t' << "    Fix: " << Data.Fix;

			char std[100];
			sprintf_s(std, "  dx(m): %.2f dy(m): %.2f CapDeg: %.2f", dx, dy, Data.CapDeg);
			ost << '\t' << std << '\n';
		}

		else ost << Pos.GetLastError() << '\n';


		ost << '\0';

		PrintTexte(st);
		
		if ((GetAsyncKeyState(' ') & 0x8000) != 0) 			break;
	}

	PrintTexte("\n Arr�t utilisateur");

	

}

void TPlateforme::CMTestPositionnementGPSRasberry()
{
	if (!GPSReceiverG.IsConnected())
	{
		Message("Pas de connexion �tablie");
		return;
	}

	char st[5000];
	ostrstream ost(st, 5000);
	ost << "Affichage temps r�el des positions robot(si fix suffisant)\n";
	ost << "(si n�cessaire, initialisation rep�re local sur premi�re position mesur�e)\n";
	ost << "Touche espace pour arr�ter\nD�marrer?";
	ost << '\0';
	//if (!MessageOuiNon(st)) return; //lourd � l'utilisation

	
	OuvreTexte();
	EffaceTexte();

	char msgerr[1000];

	if (RepereG == NULL) //initialisation du rep�re local
	{
		GPSData gpsG, gpsD;
		if (!GPSReceiverG.GetGPSRawData(gpsG, gpsD, msgerr))
		{
			Message(msgerr); return;
		}

		double LatM = (gpsG.LatDeg + gpsD.LatDeg) / 2;
		double LonM = (gpsG.LonDeg + gpsD.LonDeg) / 2;

		RepereG = new TRepereLocal(LatM, LonM);
	}

	int TDebut = GetTickCount();
	while (true)
	{
		MSG msg;
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 	DispatchMessage(&msg);
		TPositionRobot pos;

		double EcartAntennes = GPSReceiverG.GetRobotPosition(*RepereG, pos, msgerr, NULL, NULL);
		if(EcartAntennes==-1)
		{
			PrintTexte(msgerr); return;
		}

		PrintTexte("Time: %.3f, Position Robot (rep�re local):\n X: %.5f; Y: %.5f; Cap: %.5f  Ecart antennes: %.3f Fix: %d (G) %d (D)\n",
			double(GetTickCount() - TDebut) / 1000, pos.x, pos.y, pos.CapDeg, EcartAntennes, pos.fixGauche, pos.fixDroit);

		if ((GetAsyncKeyState(' ') & 0x8000) != 0) 			break;
	}

	PrintTexte("\n Arr�t utilisateur");
}


///////////////////////////
// fonction stop plateforme
///////////////////////////

void TPlateforme::CMStopMouvement()
{
	StopPlateforme();
}


/*************************************************/
// commande navigation du robot
/*************************************************/
#include "ManualControlPad.h"
int TimeDebutG;  // time, variable pour la sauvegarde de fichier log du mouvement

//commande manuelle (dialogue de commande)
void TPlateforme::CMNavigationManuelleRobot()
{
	if (!ControlRobotG.RobotIsConnected())
	{
		Message("Robot non connect�");
		return;
	}

	

	OuvreTexte();
	EffaceTexte();

	if (CommandeNavigationG != NULL)
	{
		delete CommandeNavigationG;  CommandeNavigationG = NULL;
	}

	CommandeNavigationG = new TCommandeManuelle(ParamG.PeriodeCommande, ParamG.PeriodeLog); // initialisation avec la commande manuelle
	TCommandeManuelle * pCom = dynamic_cast <TCommandeManuelle*> (CommandeNavigationG);

	if (ParamG.AvecLog) // initialise le fichier log
	{
		TNomFic NfLog;
		pCom->pFichierLog =  TGereFichierLog::InitFichierLog(ParamG.DossierLog, CommandeNavigationG->NfLog);
		if (!pCom->pFichierLog)
			if (!MessageOuiNon("Echec ouverture fichier Log. Continuer quand m�me ?"))
			{
				delete CommandeNavigationG;  CommandeNavigationG = NULL; return;
			}
	}

	TimeDebutG = GetTickCount();  // initialise le temps de d�but


	


	// �tablit timer pour la commande
	char msgerr[1000];
	if (!pCom->DoStartCommande(msgerr))
	{
		Message(msgerr); return; 
	}
	
	DialogControlPad("T_ControleRobot", HWindow());	//nom actuel du module utilisateur

	CommandeNavigationG->StopTimer();

	while (CommandeNavigationG->CallbackTimerEnCours) { ; }

	delete CommandeNavigationG;
	CommandeNavigationG = NULL;

}


struct RetNavigationAuto
// le retour de navigation automatique
{
	bool FinNavigationAuto; // indique la fin de navigation auto (soit finie avec succ�s, soit erreur)
	bool Succes; // indique si la navigation auto est bien finie sans erreurs
	std::string MsgRetour; // message de retour en cas d'erreur

	RetNavigationAuto() { FinNavigationAuto = false; Succes = false; MsgRetour = '\0'; };
	void Reset() { FinNavigationAuto = false; Succes = false; MsgRetour = '\0'; };

} RetNavigationAutoG;


void CallbackNavigationAuto(TRetourPlateforme ret)
{
	switch (ret.State)
	{
	case TRetourState::Completed:
		RetNavigationAutoG.Succes = true;
		break;

	case TRetourState::Error:
		RetNavigationAutoG.MsgRetour = ret.InfoRetour;
		break;

	case TRetourState::ExternalStop:
		RetNavigationAutoG.MsgRetour = "Stop Urgence";
		break;

	case TRetourState::TimeOut:
		RetNavigationAutoG.MsgRetour = "Time Out";
		break;

	default: break;
	}

	RetNavigationAutoG.FinNavigationAuto = true;
}



void TPlateforme::DoNavigationAutoRobot(Trajectoire &Trajet, double *step)
{
	if (CommandeNavigationG != NULL)  	delete CommandeNavigationG;

	// initialisation de pointeur pour la commande automatique
	TCommandeAutomatique *pCom = 
		new TCommandeAutomatique(&Trajet, ParamG.PeriodeCommande, ParamG.PeriodeLog, step, CallbackNavigationAuto, this);
	CommandeNavigationG = pCom;

	pCom->InitVitesse(ParamG.VitesseMaxLineaire);

	RetNavigationAutoG.Reset();  // reinitialise la structure de retour navigation auto

	TimeDebutG = GetTickCount();  // initialise le temps de d�but
	
	if (ParamG.AvecLog) // initialise le fichier log
	{
		pCom->pFichierLog = TGereFichierLog:: InitFichierLog(ParamG.DossierLog, CommandeNavigationG->NfLog);
		if (!pCom->pFichierLog)
			if (!MessageOuiNon("Echec ouverture fichier Log. Continuer quand m�me ?"))
			{
				delete CommandeNavigationG;  CommandeNavigationG = NULL; return;
			}
	}


	TPositionRobot Pos;
	char msgerr[1000];
	if (!PositionRobotEstimee(Pos, msgerr)) { Message(msgerr); return; }
	POINTFLT Debut(Pos.x, Pos.y);

	OuvreTexte();
	EffaceTexte();

	
	if (!pCom->DoStartCommande(msgerr))
	{
		Message(msgerr);
		delete CommandeNavigationG; CommandeNavigationG = NULL;
		return;
	}

	PrintTexte("D�marrage navigation auto ! \n");

	MSG msg;

	while (true)
	{
		if (PeekMessage(&msg, NULL, 0, 0, MB_OK)) DispatchMessage(&msg);

		if (RetNavigationAutoG.FinNavigationAuto) break;

		if (!PositionRobotEstimee(Pos, msgerr)) { Message(msgerr); return; }
		POINTFLT Now(Pos.x, Pos.y);
		///PrintTexte("Distance parcourue (� vol d'oiseau): %.2f\n", (Now - Debut).Norme());
		PrintTexte("Actuel: (%.2f , %.2f)\n", Now.x, Now.y);
	}

	if (!PositionRobotEstimee(Pos, msgerr)) { Message(msgerr); return; }
	POINTFLT Fin(Pos.x, Pos.y);

	if (RetNavigationAutoG.Succes)
		PrintTexte("Navigation auto : trajectoire d�sir�e parcourue");
	else
		PrintTexte("Navigation auto stopp�e. Erreur: %s", RetNavigationAutoG.MsgRetour.data());

	if (MessageOuiNon("Distance totale parcourue � vol d'oiseau ? (apr�s arr�t complet)"))
	{
		if (!PositionRobotEstimee(Pos, msgerr)) { Message(msgerr); return; }
		POINTFLT Fin(Pos.x, Pos.y);
		Message("Parcouru:  %.2f\n", (Fin - Debut).Norme());
	}

}


void TPlateforme::CMNavigationAuto()
{
	char st[5000];
	if (ParamG.TrajectoireKML)
		sprintf(st, "Param�tres actuels de trajectoire: fichier %s", ParamG.NfTrajectoire);
	else sprintf(st, "Param�tres actuels de trajectoire: tout droit sur %f", ParamG.DistanceToutDroit);

	if (!MessageOuiNon("%s\nContinuer?", st))	return;

	
	
	TStateInitPlateforme State = CheckInitPlateforme();
	if (!(State.RobotConnecte && State.RepereLocalInitialise && State.RasberryGPSConnecte))
	{
		Message("Robot non initialis�");
		return;
	}

	
	TPositionRobot Pos; if (!PositionRobotEstimee(Pos, st)) { Message(st); return; }
	

	Trajectoire Trajet;
	if (ParamG.TrajectoireKML)
	{
		if (!Trajet.Init(Pos, ParamG.NfTrajectoire, RepereG, st)) { Message(st); return; }
		else Message(st); //affiche le nombre de points
	}


	else	if (!Trajet.Init(Pos, ParamG.DistanceToutDroit, st)) { Message(st); return; }


	if (ParamG.TrajectoireKML)
		if (Trajet.LongueurSegment > 5)
		{
			sprintf(st, "Premier point de la trajectoire � plus de 5 m�tres (%f m)", Trajet.LongueurSegment);
			if (!MessageOuiNon("%s\nContinuer?", st))	return;
		}

	double Step = 0.5;

	if(MessageOuiNon("D�finir un step de test (0.5 m)?")) DoNavigationAutoRobot(Trajet, &Step);
	else DoNavigationAutoRobot(Trajet, NULL);
	
}


void TPlateforme::CMSetCapDefaut()
{
	if (!MessageOuiNon("Cap par d�faut actuel: %.2f �. Mettre � jour ?", CapDegSiUnSeulFixeG)) return;


	TPositionRobot PosDebut, PosFin;
	char st[1000];

	if (!PositionRobotEstimee(PosDebut, st)) { Message(st); return; }

	if (!MessageOuiNon(" Avancer manuellement le robot d'au moins un m�tre...\n\\nAvance effectu�e?"))return;

	if (!PositionRobotEstimee(PosFin, st)) { Message(st); return; }

	double d = (POINTFLT(PosDebut.x, PosDebut.y) - POINTFLT(PosFin.x, PosFin.y)).Norme();

	if(d<1) { Message("Distance de mesure insuffisante (%.2f)", d); return; }

	double CapRad = atan2(PosFin.y - PosDebut.y, PosFin.x - PosDebut.x);

	CapDegSiUnSeulFixeG = CapRad * (180 / M_PI) - 90; //0 degr�s --> vers le Nord et non vers l'Est

}

void TPlateforme::CMConversionLog()
{
	TNomFic NfLog = "";
	if (!DoChoixFichierOuvrir("Fichier Log", NfLog, "log", ParamG.DossierLog)) return;

	char msg[1000];
	_list<Waypoint> Liste;
	if (!TGereFichierLog::ImporteLog(NfLog, Liste, msg))	{	Message(msg); return;	}

	NomSansExt(NfLog);

	TNomFic NfKml; sprintf(NfKml, "%s_LS.kml", NfLog);
	if(!ExportLineStringKML(NfKml, NomSansPath(NfLog), Liste, msg)) { Message(msg); return; }

	Message("%d points export�s dans:\n %s", Liste.nbElem, NfKml);


}


/*********************************************************************************/
// commande et communication du bo�tier Haute Tension pour la d�charge �lectrique
/*********************************************************************************/

#include "ComPortSerieHT.h"
#include "GetListePortsCOM.h"

bool GroupeChoixPortCOM(HWND hwnd, char* PortCOM, char* msgerr)
// fonction qui cherche le port s�rie pour la communication avec le bo�tier de d�charge Haute tension
{
	// on va chercher tous les ports COM et choisir un dans la liste:
	_list<TNf> Liste;
	bool ret = GetListePortsCOM(Liste);

	if (Liste.nbElem == 0 || !ret)
	{
		sprintf(msgerr, "Pas de port COM pr�sent");
		return false;
	}

	//cr�ation de la liste multichoix
	AligneDescriptions(Liste);

	char** rubriques = new char*[Liste.nbElem + 1];
	int index = 0;

	BalayeListe(Liste)
	{
		char *temp = *Liste.current();
		while (true)
		{
			if (strncmp(temp, "COMx", 3) == 0) break;
			temp++;
		}
		rubriques[index++] = temp;
	}

	rubriques[index] = NULL;

	int IndexChoix = -1;

	GROUPE G(hwnd, "Configuration de Communication Port S�rie", NULL,
		MULCHOIX("Choix port COM USB", IndexChoix, rubriques),
		NULL);

	if (!G.Gere())
	{
		sprintf(msgerr, "Echec g�re groupe");
		delete rubriques;
		return false;
	}

	if (IndexChoix != -1)
		strcpy(PortCOM, rubriques[IndexChoix]);

	else
	{
		sprintf(msgerr, "Port COM non choisi");
		delete rubriques;
		return false;
	}

	return true;
}

bool GereParametresHT(HWND hwnd, ModeGereParametres modeGereParam, TParamHT &paramHT) 
/***********************************************************************************/
{
	GROUPE G(hwnd, "Param�tres Bo�tier Haute Tension - Weedelec", "ParametresHT.cfg",
		STR("Port COM (faut mieux ne pas changer ici)", paramHT.PortCOM),
		ENT("P�riode de charge de la bobine (us)", paramHT.DwellTime, 0, 65536),
		ENT("Fr�quence de r�p�tition des tirs (Hz)", paramHT.Frequence, 10, 1E7),
		ENT("Nombre de tirs r�alis�s par s�quence", paramHT.NbCycles, 1, 65536),
		ENT("Limite de courant dans la bobine (mA)", paramHT.CurrentLimit, 0, 65536),
		NULL);

	bool OK;

	switch (modeGereParam)
	{
	case(ModeGereParametres::Charge): OK = G.Charge(); break;
	case(ModeGereParametres::Gere): OK = G.Gere(true); break; //modal pour pouvoir d�placer la bo�te de dialogue
	case(ModeGereParametres::Sauve): OK = G.Sauve(); break;
	}

	return OK;
}

void TPlateforme::CMParametreHT()
{
	char msgerr[1000];

	if (!GroupeChoixPortCOM(HWindow(), ParamHTG.PortCOM, msgerr))
	{
		Message(msgerr);
		return;
	}
	GereParametresHT(NULL, ModeGereParametres::Sauve, ParamHTG); // sauv� le port com choisi

	if(!GereParametresHT(HWindow(), ModeGereParametres::Gere, ParamHTG))	return;

	GereParametresHT(NULL, ModeGereParametres::Sauve, ParamHTG); // sauv� tous les param�tres
}

void TPlateforme::CMInitCommandeHT()
{
	char msgerr[1000];

	if (!CommandeHTG.InitCommandeHT(ParamHTG, msgerr))
		Message(msgerr);
	else
		Message("Initialisation HT OK!");
}

void TPlateforme::CMLireParametreHT()
{
	char msgerr[1000];
	TParamHT paramHT;
	if (!CommandeHTG.ReadParamHT(paramHT, msgerr))
		Message(msgerr);
	else
	{
		Message("Param�res sur la carte:\n\nDwell_Time : %d (us)\nFrequence : %d\nNb_Cycles : %d\nCurrent_Limit : %d (mA)",
			paramHT.DwellTime, paramHT.Frequence, paramHT.NbCycles, paramHT.CurrentLimit);
	}
}

void TPlateforme::CMLireStatusHT()
{
	char msgerr[1000];
	double Temp, Voltage;

	if (!CommandeHTG.ReadTemperature(Temp, msgerr))
	{
		Message(msgerr);
		return;
	}
	
	if (!CommandeHTG.ReadVoltageBattery(Voltage, msgerr))
	{
		Message(msgerr);
		return;
	}
		
	Message("Condition actuelle de la carte:\n\nTemp�rature : %.2f (�C)\nVoltage batterie : %.2f (V)", Temp, Voltage);
}

void TPlateforme::CMLanceSequenceHT()
{
	char msgerr[1000];
	int CourantMax, VoltageTir, DureeTir;
	if (!CommandeHTG.StartSequence(CourantMax, VoltageTir, DureeTir, msgerr))
	{
		Message(msgerr);
		return;
	}
	
	Message("S�quence lanc�e avec l'info retour :\n\nCourant max: %d (mA)\nTension moyenne de s�quence : %d (V)\nDur�e de l'arc : %d (us)",
			CourantMax, VoltageTir, DureeTir);
}

/*********************************************************************************/




/**********************************************************************************/
// fonctions pour le weeding
/**********************************************************************************/

// dialogue de contr�le des bras par la fonction d'API SendHWcommand de l'Ecorobotix
#include "DialogSendHWCommand.h"
#include "IDControlPad.h"

void TPlateforme::CMDialogueHWCommand()
{
	if (!ControlRobotG.RobotIsConnected())
	{
		Message("Robot non connect�");
		return;
	}
	OuvreTexte();
	EffaceTexte();

	DialogSendHWCommand(NOM_MODULE);
}

// dialogue de weeding avec la d�charge haute tension
#include "DialogueHauteTension.h"
void TPlateforme::CMDialogueControlBrasManuel()
{
	if (!ControlRobotG.RobotIsConnected())
	{
		Message("Robot non connect�");
		return;
	}
	OuvreTexte();
	EffaceTexte();

	DialogHauteTension(NOM_MODULE);
}

//Lancement de commandes pour �valuer le d�placement du bras dans des cas difficiles
//Test de valeurs proches du centre du robot
//Test de valeurs � la limite de l'hexagone
//Test de valeurs en dehors de l'hexagone
//On fera descendre le robot au maximum � chaque fois afin de pouvoir v�rifier si on trouve le bon point
void TPlateforme::CMStressTestWeeding() {
	if (!ControlRobotG.RobotIsConnected())
	{
		Message("Robot non connect�");
		return;
	}
	bool AvecHauteTension = false;
	bool AvecRecalageManuel = false;
	_list<TWeedBras> ListeWeedsTest;
	//ouverture du fichier contenant la liste des mauvaises herbes
	string chemin_list_weeds = ParamG.fichierCoor;
	ifstream fin(chemin_list_weeds);
	if (!fin)
	{
		PrintTexte("Impossible d'ouvrir le fichier %s \n", chemin_list_weeds);
		return;
	}
	//on peut alors parser le fichier et cr�er la liste de points
	string line;
	PrintTexte("Liste weeds fichier \n");
	PrintTexte("=====================\n");
	while (getline(fin, line))
	{
		size_t idx(line.find(","));
		string xStr(line.substr(0, idx));
		string yStr(line.substr(idx + 1, line.size() - idx));
		//PrintTexte("%s - %s \n", xStr, yStr);
		float xFlt = stof(xStr);
		float yFlt = stof(yStr);
		PrintTexte("%f - %f \n", xFlt, yFlt);
		ListeWeedsTest.push_back(TWeedBras(POINTFLT(xFlt, yFlt)));
	}
	PrintTexte("=====================\n");
	////On fait la duplication du vector pour les r�p�titions
	//std:vector<TWeedBras> ListeWeedsTestRep;
	//for (int i = 0; i < 6; i++) {
	//	ListeWeedsTestRep.insert(ListeWeedsTestRep.end(), ListeWeedsTest.begin(), ListeWeedsTest.end());
	//}
	TRetourPlateforme ret;
	if (!ControlRobotG.RobotIsConnected())
	{
		Message("Robot non connect�");
		return;
	}
	OuvreTexte();
	EffaceTexte();

	PrintTexte("Test de weeding avec %d positions renseign�es \n", ListeWeedsTest.nbElem);
	PrintTexte("Hauteur de weeding %f \n", ParamG.ZWeeding);
	PrintTexte("Resolution de l'hexagone %f \n", ParamG.ResolutionHexagone);
	//PrintTexte("Replacement du bras %s \n", ParamG.getZero);
	PrintTexte("===============\n");
	BalayeListe(ListeWeedsTest) {
		TWeedBras *pwe = ListeWeedsTest.current();
		DBOUT(pwe->Position.x << " - " << pwe->Position.y << "\n");
	}
	TModulePlateforme::Weeding(ListeWeedsTest, ret, AvecHauteTension, AvecRecalageManuel);


	PrintTexte("Fin de l'�valuation");

	return;
}



void TPlateforme::CMTimerRandomWeeding() {
	int tailleSequence = 10;
	//On alimente la liste
	bool flag = true;
	PrintTexte("Test temps de weeding sur une s�quence al�atoire de %i weeds \n", tailleSequence);
	_list<TWeedBras> ListeWeedsTest;
	bool AvecHauteTension = false;
	bool AvecRecalageManuel = false;
	double dif;
	if (!ControlRobotG.RobotIsConnected())
	{
		Message("Robot non connect�");
		return;
	}

	int lowY = -49;
	int highY = 49;
	int lowX = -43;
	int highX = 43;
	for (int z=0;z< tailleSequence;z++) {
		//g�n�rer un point
		float y = (lowY + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (highY - lowY))))/100;
		float x = (lowX + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (highX - lowX))))/100;
		PrintTexte("x= %f, y= %f \n", x,y);
		//ce point pourrait �tre en dehors de l'hexagone, il devra �tre supprim� si n�cessaire
		ListeWeedsTest.push_back(TWeedBras(POINTFLT(x, y)));
	}
	PrintTexte("Fin de la g�n�ration de points \n");
	PrintTexte("================================\n");
	Sleep(3000);
	PrintTexte("TEST 1 - AVEC RETOUR AU ZERO \n");
	ParamG.getZero = true;
	ParamG.getShortest = false;
	time_t debut, fin;
	TRetourPlateforme ret;
	vector<bool> masqueWeedsTraitees;
	time(&debut);
	TModulePlateforme::Weeding(ListeWeedsTest,  ret, AvecHauteTension, AvecRecalageManuel);
	time(&fin);
	dif = difftime(fin, debut);
	PrintTexte("Temps �coul� = %f \n", dif);

	Sleep(5000);
	PrintTexte("TEST 2 - SANS RETOUR AU ZERO \n");
	ParamG.getZero = false;
	ParamG.getShortest = false;
	time(&debut);
	TModulePlateforme::Weeding(ListeWeedsTest, ret, AvecHauteTension, AvecRecalageManuel);
	time(&fin);
	dif = difftime(fin, debut);
	PrintTexte("Temps �coul� = %f \n", dif);


	Sleep(5000);
	PrintTexte("TEST 3 - SANS RETOUR AU ZERO - CHEMIN OPTIMAL \n");
	ParamG.getZero = false;
	ParamG.getShortest = true;
	time_t debutChemin, finChemin;
	time(&debutChemin);
	TModulePlateforme::Weeding(ListeWeedsTest,  ret, AvecHauteTension, AvecRecalageManuel);
	time(&finChemin);
	float difChemin = difftime(finChemin, debutChemin);
	PrintTexte("Temps �coul� = %f \n", difChemin);

	PrintTexte("============ \n");
	PrintTexte("FIN DES TESTS \n");

}




