#ifndef  __TPLATEFORME_H__
#define __TPLATEFORME_H__

#include "drvtraiti.h"
#include "ExportModuleControleRobot.h"
#include "Plateforme_2.h"
#include "Waypoint.h"


extern TParamRobot ParamG;


struct TPlateforme : public DRVTRAITIM
{
	void CMParametre();

	// fonctions pour la connexion, la r�ception des mesures du robot
	void CMInitRobot();
	void CMReleaseRobot();
	void CMCheckInitRobot();
	void CMTestMesureRobot();


	// fonctions pour activation de RTK Erx
	void CMActiveRTKRobot();
	void CMGetStatutRTKRobot();

	// reinitialisation de rep�re de sol Erx
	void CMResetGroundFrame();



	// fonctions pour utiliser le capteur GPS externe
	void CMInitGPSRasberry();
	void CMTestDonneesBrutesGPSRasberry();
	void CMTestPositionnementGPSRasberry();


	// stop
	void CMStopMouvement();


	// commandes de navigation du robot
	void DoNavigationAutoRobot(Trajectoire &Trajet, double *step);
	void CMNavigationAuto();

	void CMNavigationManuelleRobot();
	void CMSetCapDefaut();
	void CMConversionLog();


	// fonctions pour le weeding
	void CMDialogueHWCommand();
	void CMDialogueControlBrasManuel();
	void CMStressTestWeeding();
	void CMTimerRandomWeeding();

	// commande de la t�te Haute tension
	void CMParametreHT();
	void CMInitCommandeHT();
	void CMLireParametreHT();
	void CMLireStatusHT();
	void CMLanceSequenceHT();


	//fonctions diverses de test
	void CMGetStatutRobot();

};






#endif 

