#ifndef __PLATEFORME_H__
#define __PLATEFORME_H__

#include "WeedelecCommon.h"
#include "ExportModuleControleRobot.h"
#include "RepereLocal.h"
#include "Region.h"
#include <fstream>

#ifndef ENTRAXE_ROBOT
#define ENTRAXE_ROBOT   2.112528 // entraxe en m�tres
#endif

#define ECART_ANTENNES_GPS 2.3

#define UNITE_MOUVEMENT_BRAS  0.01     // unit� de mouvement du bras en cm

#define DBOUT( s )            \
{                             \
   std::ostringstream os_;    \
   os_ << s;                   \
   OutputDebugString( os_.str().c_str() );  \
}
class Hexagone
{
private:
	Region R;
	double Resolution; //n�cessaire car les Regions sont d�finies en pixels

public:
	Region RReel;
	Hexagone(double rayon, double resolution) : Resolution(resolution)
	{
		//Construction de l'hexagone th�orique
		//d�termination des 6 sommets
		POINTFLT Sf[6];
		for (int i = 0; i < 6; i++)
		{
			double anglerad = i * M_PI / 3 + M_PI / 6;
			Sf[i] = POINTFLT(rayon * cos(anglerad), rayon*sin(anglerad));
		}

		//passage en coordonn�es enti�res
		LIGNE L(7);
		for (int i = 0; i < 6; i++)
		{
			L[i].x = Sf[i].x / resolution;
			L[i].y = Sf[i].y / resolution;
		}
		L[6] = L[0]; //fermeture de la ligne
		R = Region(~L);

		//On renseigne aussi la r�gion r�elle de l'hexagone du robot, r�cup�r�e dans un fichier txt
		//Voir delta_zone.xlsx pour les d�tails
		Region RReel;
		string line;
		string chemin_list_weeds = "D:/Rabatel/LangageC/WeedelecSoft/CONFIG/hexagone_reel_bras_droit.csv";
		ifstream fin(chemin_list_weeds);
		LIGNE LReel(35);
		int i(0);
		while (getline(fin, line))
		{
			size_t idx(line.find(","));
			string xStr(line.substr(0, idx));
			string yStr(line.substr(idx + 1, line.size() - idx));
			float xFlt = stof(xStr);
			float yFlt = stof(yStr);
			LReel[i].x = xFlt;
			LReel[i].y = yFlt;
			i++;
		}
		LReel[34] = LReel[0]; //fermeture
		RReel = Region(~LReel);
	}

	bool Contient(double x, double y)
	{
		int xi = x / Resolution;
		int yi = y / Resolution;
		return R.ContientPoint(xi, yi);
	}
	bool ContientReel(double x, double y)
	{	
		return RReel.ContientPoint(x, y);
	}
};


struct TControlBrasManuel
{
	POINTFLT PositionBras;
	double Hauteur;

	bool UpdatePositionBras;

	bool ActiveControlManuel;

	TControlBrasManuel() 
	{
		PositionBras.x = 0; PositionBras.y = 0;
		Hauteur = 0;
		this->UpdatePositionBras = true;
		this->ActiveControlManuel = false;
	}

	void UpdateConsigneCourante(POINTFLT pos)
	{
		if (this->UpdatePositionBras) { PositionBras = pos; }
	}

	POINTFLT GetConsignePositionBras()
	{
		this->UpdatePositionBras = false;
		POINTFLT pos = PositionBras;
		this->UpdatePositionBras = true;
		return pos;
	}

	void PositionXPlus() { if (this->UpdatePositionBras) PositionBras.x += UNITE_MOUVEMENT_BRAS; }
	void PositionXMoins() { if (this->UpdatePositionBras) PositionBras.x -= UNITE_MOUVEMENT_BRAS; }
	void PositionYPlus() { if (this->UpdatePositionBras)  PositionBras.y += UNITE_MOUVEMENT_BRAS; }
	void PositionYMoins() { if (this->UpdatePositionBras) PositionBras.y -= UNITE_MOUVEMENT_BRAS; }
	
	void PositionZPlus() { if (this->UpdatePositionBras) Hauteur -= UNITE_MOUVEMENT_BRAS; }
	void PositionZMoins() { if (this->UpdatePositionBras) Hauteur += UNITE_MOUVEMENT_BRAS; }

	void ResetPositionBras() { PositionBras.x = 0; PositionBras.y = 0; };

} ControlBrasManuelG;




TStateInitPlateforme InitPlateforme(DRVTRAITIM *pD, TParamRobot &param, bool TempoDebut);
TStateInitPlateforme CheckInitPlateforme();
void ReleasePlateforme();
void StopPlateforme();

bool PositionRobotEstimee(TPositionRobot &pos, char *msgerr);



#endif // !__PLATEFORME_H__

