#pragma once

/////////////////////////////////////////////////////////////////////////
//Acc�s aux donn�es du syst�me � deux gps Emlid pilot� par Rasberry dedi�
/////////////////////////////////////////////////////////////////////////

#include <string>
#include <ostream>

#include <cpprest/http_client.h>
#include <cpprest/json.h>
#include <Windows.h>

#include "RepereLocal.h"
#include "PositionRobot.h"

using namespace web::http;                  // Common HTTP functionality
using namespace web::http::client;          // HTTP client features



struct GPSData
{
	double LatDeg;
	double LonDeg;

	int Fix;

	long double Age;

	GPSData() : LatDeg(0), LonDeg(0), Fix(0), Age(0) {};
};


struct GPSReceiver
{

	GPSReceiver() { pClient = NULL; Connecte = false; };

	~GPSReceiver() { if(pClient)	delete pClient; };

	bool GPSReceiver::Connect(std::string uri, char* msgerr);
	//reconnexion possible sur une autre uri (pour test) m�me si d�j� connect�
	// dans le cas, pClient pr�c�dent d�truit)

	void GPSReceiver::Disconnect();

	bool GPSReceiver::IsConnected() { return Connecte; }

	bool GetGPSRawData(GPSData& GpsGauche, GPSData& GpsDroite, char* msgerr);
	
	bool GetGPSRawCoordinates(double &LatG, double &LonG, double &LatD, double &LonD, int &fixG, int &fixD, char* msgerr);
	//inclut le test d'�ge des donn�es det de validit� du Fix

	double GetRobotPosition(TRepereLocal Repere, TPositionRobot &pos, char* msgerr, double *pdistanceNominale, double *tolerance);
	//inclut le test d'�ge des donn�es de validit� du Fix SUPPRIME PROVISOIREMENT !!!!!!!!!!!!!!!!!!!!!!!!!!!!

	//�tablit CapDispo ssi les 2 gps sont en fix 4. Dans ce cas:
		//valeur de retour: distance mesur�e entre antennes, ou -1 si erreur
		//si pdistanceNominale non nul et si capDispo, test de la distance inter_antennes selon la tol�rance fournie
	
	//si un seul gps en fix 4 --> CapDispo false. Dans ce cas, retour avec distance -2 entre antennes
	//et position pas�e sur le seul gps en fix 4

private:

	http_client *pClient;
	bool Connecte;


	bool SetGPSClient(std::string uri, char* msgerr);

	bool RequestGPSClient(web::json::value & reponse, char * msgerr);
	
	bool GetKeyValueJson(web::json::value & data_in, std::string key, web::json::value& keyvalue, char *msgerr);
	
	void ParseGPSAge(web::json::value dataAge, long double& age);
	
	void ParseGPSFix(web::json::value dataRTK, int& rtkfix);
	

};