#include "Trajectoire.h" 

#include "KML.h" 
#include "GereFichierLog.h" 
#include "UtilficGen.h" 
#include "ExportModuleControleRobot.h" //pour structure TGereFichierLog



bool Trajectoire::Init(TPositionRobot &Pos, char* Nf, TRepereLocal* R, char *msg)
{
	if (!Pos.CapDispo)
	{
		sprintf(msg, "Mode trajectoire sur fichier non utilisable si un seul fix GPS (cap non dispo)");
		return false;
	}

	if(strlen(Nf)==0)
	{
		sprintf(msg, "Fichier de trajectoire non d�fini");
		return false;
	}

	_list<Waypoint> ListeW;

	char *ext = ChaineExt(Nf);

	if (strcmp(ext, "kml") == 0)
	{
		if (!ImporteKML(Nf, ListeW, msg))	return false;
	}
	else if(strcmp(ext, "log") == 0)
	{
		if (!TGereFichierLog::ImporteLog(Nf, ListeW, msg))	return false;
	}

	else { sprintf(msg, " Extension fichier log non reconnue"); return false; }

	if (ListeW.nbElem < 1)
	{
		sprintf(msg, "Pas de waypoints dans le fichier;\n %s", Nf);
		return false;
	}

	NbPoints = ListeW.nbElem + 1; //on y rajoute le point de d�part
	PointsPassage = new POINTFLT[NbPoints];

	PointsPassage[0] = POINTFLT(Pos.x, Pos.y);

	int i=1;
	BalayeListe(ListeW)
	{
		Waypoint *pW = ListeW.current();
		R->WGS84VersMetres(pW->LatDeg, pW->LonDeg, PointsPassage[i].x, PointsPassage[i].y);
		i++;
	}

	P1 = PointsPassage[0];
	P2 = PointsPassage[1];
	LongueurSegment = (P2 - P1).Norme();
	IndexCourant = 0;

	sprintf(msg, "Trajectoire charg�e: %d points de passage", ListeW.nbElem);

	return true;
}

bool Trajectoire::Init(TPositionRobot &Pos, double distance, char *msg)
{
	if (!Pos.CapDispo)
		if(CapDegSiUnSeulFixeG==-1000)
		{
			sprintf(msg, "Mode avec un seul fix GPS (cap non dispo) --> cap par d�faut non d�fini");
			return false;
		}
	NbPoints = 2;
	PointsPassage = new POINTFLT[NbPoints];

	PointsPassage[0] = POINTFLT(Pos.x, Pos.y);
	if(Pos.CapDispo)			PointsPassage[1] = PointDistantSelonDirection(PointsPassage[0], Pos.CapDeg, distance);
	else PointsPassage[1] = PointDistantSelonDirection(PointsPassage[0], CapDegSiUnSeulFixeG, distance);

	P1 = PointsPassage[0];
	P2 = PointsPassage[1];
	LongueurSegment = (P2 - P1).Norme();
	return true;

}

bool Trajectoire::SegmentSuivant()
{
	if (IndexCourant >= NbPoints - 2) return false; //pas d'autre segment
	IndexCourant++;
	P1 = PointsPassage[IndexCourant];
	P2 = PointsPassage[IndexCourant+1];
	LongueurSegment = (P2 - P1).Norme();

}


double DirectionDegVersPointDistant(POINTFLT PRob, POINTFLT Pdistant)
{
	double capdeg = atan2(Pdistant.y - PRob.y, Pdistant.x - PRob.x)* (180 / M_PI);
	//capdeg indique ici l'angle par rapport � l'axe X g�ographique. Il faut le traduire en cap pour le robot
	capdeg -= 90;
	if (capdeg <= -180) capdeg += 360;
	return capdeg;
}

POINTFLT PointDistantSelonDirection(POINTFLT PRob, double DirectionDeg, double distance)
{
	//traduction de DirectionDeg en angle par rapport � l'axe X g�ographique.
	DirectionDeg += 90;
	double AngleRad = DirectionDeg * (M_PI / 180);
	return PRob + POINTFLT(cos(AngleRad), sin(AngleRad))*distance;
}