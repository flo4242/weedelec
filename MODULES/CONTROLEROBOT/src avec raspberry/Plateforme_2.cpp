// dans le module plateforme2, on utilise le syst�me de deux antennes de GPS externe pour le guidage


#include "ControlRobot.h" 
// il faut toujours inclure "ControlRobot.h" en premier pour que "erxCom.h" soit d�clar� en premier 
// (sinon incompatibilit� dans le module 'chrono') 

#include "Plateforme_2.h"
#include "CommandeNavigation_2.h"
#include "ExportModuleControleRobot.h"
#include "GetPortsCOM.h"
#include "GPSReceiver.h"

#ifndef VITESSEMAX
#define VITESSEMAX  0.4   
#endif

TRepereLocal *RepereG = NULL;
Hexagone *pHexagoneBras = NULL;
GPSReceiver GPSReceiverG;

extern TParamRobot ParamG;
extern TControlRobot ControlRobotG;
extern TCommandeNavigation *CommandeNavigationG;


TStateInitPlateforme InitPlateforme(DRVTRAITIM *pD, TParamRobot &param, bool TempoDebut) //appel� au lancement du module
{
	TStateInitPlateforme ret;

	pD->OuvreTexte();
	pD->EffaceTexte();

	pD->PrintTexte("      INITIALISATION DU ROBOT\n");

	if (TempoDebut)
	{
		pD->PrintTexte("      (touche espace ou clic gauche avant 3 secondes pour annuler)\n");

		//test d'abandon
		DWORD debut = GetTickCount();

		while (GetTickCount() - debut < 3000)
		{
			if (   ((GetAsyncKeyState(' ') & 0x8000) != 0) || (pD->TestCliqueGauche()) )
			{
				pD->PrintTexte(" \n\n Abandon utilisateur ");
				return ret;
			}
		}
	}

	char msg[1000];

	//connexion robot

	pD->PrintTexte("Connexion du robot ...\n");

	MSG MsgWin;
	while (PeekMessage(&MsgWin, pD->HWindow(), 0, 0, PM_REMOVE)) 	DispatchMessage(&MsgWin);
	//pour affichage imm�diat ligne d'avertissement attente ci-dessus

	ControlRobotG.SetConnectAddress(param.ConnexionWifi ? param.AdresseConnexionWifi : param.AdresseConnexionEth);

	ret.RobotConnecte = ControlRobotG.ConnectRobot(msg);

	if (!ret.RobotConnecte)  pD->PrintTexte(msg);
	else pD->PrintTexte("Robot connect�");

	//connexion rasberry GPS

	pD->PrintTexte("\n\nConnexion du module GPS Rasberry ...\n");
	while (PeekMessage(&MsgWin, pD->HWindow(), 0, 0, PM_REMOVE)) 	DispatchMessage(&MsgWin);
	//pour affichage imm�diat ligne d'avertissement attente ci-dessus

	ret.RasberryGPSConnecte = GPSReceiverG.Connect(param.AdresseServeurGPS, msg);

	if (!ret.RasberryGPSConnecte) 	pD->PrintTexte(msg);

	else pD->PrintTexte("Module GPS Rasberry connect�");

	//rep�re local
	if (ret.RasberryGPSConnecte)
	{
		pD->PrintTexte("\n\nInitialisation du rep�re local ...\n");
		while (PeekMessage(&MsgWin, pD->HWindow(), 0, 0, PM_REMOVE)) 	DispatchMessage(&MsgWin);
		//pour affichage imm�diat ligne d'avertissement attente ci-dessus

		//lecture latitude actuelles
		double latG, lonG, latD, lonD;
		int fixG, fixD; //inutilis� ici
		if (!GPSReceiverG.GetGPSRawCoordinates(latG, lonG, latD, lonD, fixG, fixD, msg))	pD->PrintTexte(msg);

		else
		{
			// cr�ation d'un nouveau rep�re local
			if (RepereG != NULL)
			{
				delete RepereG; RepereG = NULL;
			}

			if ((fixG == 4) && (fixD == 4) )
			{
				double latdeg = (latG + latD) / 2;
					double londeg = (lonG + lonD) / 2;
					RepereG = new TRepereLocal(latdeg, londeg);
					pD->PrintTexte("Repere local initialis�");
			}

			else if (fixG == 4)
			{
				double latdeg = latG;
				double londeg = lonG;
				RepereG = new TRepereLocal(latdeg, londeg);
				pD->PrintTexte("Repere local initialis� selon fix gauche");
			}

			else if (fixD == 4)
			{
				double latdeg = latD;
				double londeg = lonD;
				RepereG = new TRepereLocal(latdeg, londeg);
				pD->PrintTexte("Repere local initialis� selon fix droit");
			}

			else if ( (fixG == 5) && (fixD == 5))
			{
				double latdeg = (latG + latD) / 2;
				double londeg = (lonG + lonD) / 2;
				RepereG = new TRepereLocal(latdeg, londeg);
				pD->PrintTexte("Repere local initialis� avec fix gauche et droit � 5 !!!");
			}
		}
	}

	// d�finition de l'espace de travail pour weeding:
	if (pHexagoneBras != NULL)
	{
		delete pHexagoneBras; pHexagoneBras = NULL;
	}


	pHexagoneBras = new Hexagone(ParamG.RayonHexagone, ParamG.ResolutionHexagone);

	pD->PrintTexte("\n\nHexagone de travail du bras initialis�");

	return ret;
}


TStateInitPlateforme CheckInitPlateforme()
{
	TStateInitPlateforme ret;
	ret.RobotConnecte = ControlRobotG.RobotIsConnected();
	ret.RasberryGPSConnecte = GPSReceiverG.IsConnected();
	ret.RepereLocalInitialise = (RepereG != NULL);
	return ret;
}


void ReleasePlateforme()
{
	//d�connexion du robot
	char msgerr[1000]; ControlRobotG.DeconnectRobot(msgerr);


	
	if (RepereG != NULL) // d�truit rep�re local
	{
		delete RepereG;
		RepereG = NULL; // permettra la reconstruction d'un rep�re local d�s la reconnexion
	}

	if (pHexagoneBras != NULL)
	{
		delete pHexagoneBras; pHexagoneBras = NULL;
	}

	if (CommandeNavigationG != NULL) // d�truit la commande de navigation (soit manuelle, soit automatique)
	{
		if (CommandeNavigationG->Timer_Commande) CommandeNavigationG->StopTimer();
		delete CommandeNavigationG; CommandeNavigationG = NULL;
	}

	if (ControlRobotG.ArmInitialised)
	{
		ControlRobotG.CloseRightArm();
		ControlRobotG.ArmInitialised = false;
	}

}

bool PositionRobotEstimee(TPositionRobot &pos, char *msgerr)
// r�ception de position estim�e du robot par le syst�me de GPS externe, 
// dont la communication �tablie par la structure GPSReceiver
{
	if (!GPSReceiverG.IsConnected())
	{
		sprintf(msgerr, "GPS http serveur non initialis�");
		return false;
	}
	if (RepereG == NULL)
	{
		sprintf(msgerr, "Rep�re local non initialis�");
		return false;
	}

	double EcartAntennesNominal = ECART_ANTENNES_GPS;
	double EcartAntennes = GPSReceiverG.GetRobotPosition(*RepereG, pos, msgerr, &EcartAntennesNominal, &ParamG.ToleranceDistanceInterAntennes);
	if (EcartAntennes ==-1) return false; //erreur
	else 	return true; //m�me si EcartAntennes = -2 (mode sans cap dispo)
}

void StopPlateforme()
{
	if(CommandeNavigationG != NULL)
		CommandeNavigationG->StopTimer();
	
	if (ControlRobotG.RobotIsConnected())
		ControlRobotG.SetRobotSpeed(0, 0);
}

