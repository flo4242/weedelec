////////////// TGereFichierLog ///////////////////////////

#pragma once

#include <fstream>
#include "list_tpl.h"
#include "Waypoint.h"




using namespace std;

struct TGereFichierLog
{
	static void SetLigneTitreLog(ostream *os);

	static ofstream * InitFichierLog(char *DossierLog, char *NfOut); // initialise le fichier log

	static void WriteLine
		(ofstream *pOs, double time, double CG, double CD, double x, double y, double Cap, double Lat, double Lon, int fixG, int fixD);

	static bool ImporteLog(char *NfLog, _list<Waypoint> &ListeW, char *msgerr);


};
