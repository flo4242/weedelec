
#ifndef __GETPORTSCOM_H
#define __GETPORTSCOM_H


//#include <Winreg.h>
#include <stdio.h> //pour sprintf
#include "WINUTIL2.h"
#include "PortCOMBaudrate.h"

//#include "GetListePortsCOM.h"
//#include "GetListeFichiers.h"


struct TParamPortCom
{
	TNomFic DescriptionPort;
	PortCOMBaudrate Baud;
	TParamPortCom(){ DescriptionPort[0] = '\0'; }

	char *GetPort(char *DescriptionPort); //retourne "COMxx"
};

bool GereGroupePortCOM(HWND hParent, TParamPortCom &param, TNomFic NfConfig, bool charge);

#endif