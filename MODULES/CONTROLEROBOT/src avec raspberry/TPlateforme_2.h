#ifndef  __TPLATEFORME_H__
#define __TPLATEFORME_H__

#include "drvtraiti.h"
#include "ExportModulePlateforme_2.h"
#include "Plateforme_2.h"


extern TParamRobot ParamG;


struct TPlateforme : public DRVTRAITIM
{
	void CMParametre();

	// fonctions pour la connexion, la r�ception des mesures du robot
	void CMConnectRobot();
	void CMDeconnectRobot();
	void CMTestMesureRobot();


	// fonctions pour activation de RTK Erx
	void CMActiveRTKRobot();
	void CMGetStatutRTKRobot();

	// reinitialisation de rep�re de sol Erx
	void CMResetGroundFrame();



	// fonctions pour utiliser le capteur GPS externe
	void CMInitGPSRasberry();
	void CMTestDonneesBrutesGPSRasberry();
	void CMTestPositionnementGPSRasberry();


	// g�n�ration de trajectoire

	void CMConvertPisteToWaypointKML();
	void CMConvertTxtToWaypointKML();
	void CMInitTrajectoire();


	// stop
	void CMStopMouvement();


	// commande de navigation du robot
	void CMNavigationAutoRobot();
	void CMNavigationManuelleRobot();


	// fonctions pour le weeding
	void CMDialogueHWCommand();
	void CMDialogueControlBrasManuel();


	// commande de la t�te Haute tension
	void CMParametreHT();
	void CMInitCommandeHT();
	void CMLireParametreHT();
	void CMLireStatusHT();
	void CMLanceSequenceHT();


	// fonctions provisoires 
	void TestHTTPClient();
};






#endif 

