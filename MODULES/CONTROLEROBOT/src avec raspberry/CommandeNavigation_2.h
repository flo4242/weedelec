#ifndef __COMMANDENAVIGATION_H__
#define __COMMANDENAVIGATION_H__

#include <Windows.h>
#include <ppltasks.h> 
#include <agents.h> // pour timer

#include "ligne.h"
#include "Plateforme_2.h"




using namespace Concurrency;

#define UNITE_VITESSE  0.05   // unit� de contr�le manuel de vitesse (incr�ment de vitesse 0.05m/s pour chaque action)
#define SEUIL_VITESSE  0.4    // vitesse max (contrainte du robot Erx)





struct TCommandeNavigation
	// structure de base qui g�re le timer pour la commande de navigation du robot
{

	timer<bool> *Timer_Commande;

	int PeriodeCommande; // en milisecondes
	bool CommandeManuelle;
	ofstream *pFichierLog;
	int PeriodeLog;
	int TickLogPrecedent;

	char NfLog[1000]; //�tabli si pFichierLog non nul. Utile pour exportation syst�matique en kml en fin de navigation

	bool CallbackTimerEnCours; //pour �viter destruction de la commande alors que timer en cours... 

	TCommandeNavigation(int periode_commande, int periode_log, bool commande_manuelle) : PeriodeCommande(periode_commande),
		PeriodeLog(periode_log), CommandeManuelle(commande_manuelle), CallbackTimerEnCours(false)
	{
		Timer_Commande = NULL; pFichierLog = NULL; TickLogPrecedent = -1;
	}

	~TCommandeNavigation();
	
	virtual bool DoStartCommande(char* msgerr) = 0;

	void SetTimer();
	void StartTimer();
	void StopTimer();
	
};


struct TCommandeManuelle : public TCommandeNavigation
// classe d�riv�e pour la commande manuelle
{

	double VitesseNominale;			// vitesse d'avancement si le robot ne tourne pas
	double VitesseDifferentielle;	// positive dans le sens anti-horaire (trigo)

	
	TCommandeManuelle(int periode_commande, int periode_log) : VitesseNominale(0), VitesseDifferentielle(0), 
		TCommandeNavigation(periode_commande, periode_log, true) { ; };
	
	void StopVitesse() { VitesseNominale = 0; VitesseDifferentielle = 0; }

	void VitesseNominaleAdd(); 
	void VitesseNominaleSub(); 
	void VitesseDiffAdd(); 
	void VitesseDiffSub();
	

	virtual bool DoStartCommande(char *msgerr);

	POINTFLT GetVitesseCommandeManuelle();
};


struct TCommandeAutomatique : public TCommandeNavigation
// classe d�riv�e pour la commande automatique
{
	DRVTRAITIM* pDriver; //pour affichages �ventuels pendant le trajet (changements de segment)

	Trajectoire *pTrajectoire;

	TPositionRobot PosActuelle; //mise � jour par le timer
	

	pcallbackStep callbackRetour;

	double* Step; // � d�finir si l'on veut ex�cuter la trajectoire pas � pas
	POINTFLT DebutStep; //sera �tabli par DoStartCommande() si Step est non nul

	double vitesseNominale;
	
	
	// constructeur
	TCommandeAutomatique(Trajectoire *pT, int periode_commande, int periode_log, double *step = NULL, pcallbackStep cb = NULL, DRVTRAITIM* pD = NULL)
		: vitesseNominale(0), callbackRetour(cb), pTrajectoire(pT), TCommandeNavigation(periode_commande, periode_log, false)
	{
		pDriver = pD;

		if (step != NULL)
		{
			Step = new double; *Step = *step;
			//doit �tre r�allou� localement car la perennit� de la variable point�e par step lors de l'appel constructeur
			//n'est pas garantie
		}
		else Step = NULL;
	}

	~TCommandeAutomatique() {	if (Step) delete Step;	}

	void InitVitesse(double vitesse) { vitesseNominale = vitesse; };

	virtual bool DoStartCommande(char* msgerr);

	bool RestartCommande(char* msgerr);


	// suivi de chemin 
	POINTFLT CalculConsignesMoteurs(double CapDesireDeg, bool CapDispo);
	//�tablit la consigne des moteurs gauche et droite en fonction de l'�cart au cap actuel 
	//NB: la vitesse d'avancement V = 1/2*(vG + vD)  est modul�e en fonction de l'erreur de suivi de cap

	double CalculCapDesire(POINTFLT P1, POINTFLT P2, POINTFLT Pactuel);
	// calcul le cap d�sir� � partir du segment de chemin en cours (d�fini par P1, P2) et du point actuel
	//(en tenant compte du param�tre RAYON_RATTRAPAGE pour un rattrapage progressif du segment)
};
 

bool TestArret(POINTFLT Ptprecedent, POINTFLT Ptcourant, double DistanceMax);

double DirectionDegVersPointDistant(POINTFLT PRob, POINTFLT Pdistant);
POINTFLT PointDistantSelonDirection(POINTFLT PRob, double DirectionDeg, double distance);

#endif