#pragma once

#include <Windows.h>
#include "drvtraiti.h"

BOOL CALLBACK SendHWCommandDialogProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

HMODULE DialogSendHWCommand(TNomFic NomModule);