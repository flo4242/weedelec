
#include "ComPortSerieHT.h"

PortComHT::PortComHT(char *NomPort)
// Les caractéristiques de la liaison seront les suivants :
// • 3 Mbps = 3000000 bits/s
// • 8 bits 
// • 1 bit de stop 
// • Sans parité 
// • contrôle de flux hardware(RTS / CTS)
{
	this->Connected = false;

	this->HCom = CreateFileA(static_cast<LPCSTR>(NomPort),
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (this->HCom == INVALID_HANDLE_VALUE)
	{
		if (GetLastError() == ERROR_FILE_NOT_FOUND) 
		{
			char msgerr[100];
			sprintf(msgerr, "ERROR: Handle was not attached. Reason: %s not available\n", NomPort);
			this->Errors = msgerr;
		}

		else
		{
			this->Errors = "ERROR: Invalid Handle Value!!!\nVerify if aleardy opened in another program!\n";
		}
	}

	else 
	{
		DCB dcbSerialParameters = { 0 };

		if (!GetCommState(this->HCom, &dcbSerialParameters)) {
			this->Errors = "ERROR: Failed to get current serial parameters";
		}

		else 
		{
			dcbSerialParameters.BaudRate = HT_COMMUNICATION_SPEED;  // communication en 3Mbps avec la carte
			dcbSerialParameters.ByteSize = 8;
			dcbSerialParameters.StopBits = ONESTOPBIT;
			dcbSerialParameters.Parity = NOPARITY;
			dcbSerialParameters.fDtrControl = RTS_CONTROL_ENABLE;
			
			if (!SetCommState(HCom, &dcbSerialParameters))
			{
				this->Errors = "ALERT: could not set Serial port parameters\n";
			}
			else
			{
				this->Connected = true;
				PurgeComm(this->HCom, PURGE_RXCLEAR | PURGE_TXCLEAR);
			}
		}
	}
}


PortComHT::~PortComHT()
{
	if (this->Connected) 
	{
		this->Connected = false;
		CloseHandle(this->HCom);
	}
}


int PortComHT::ReadMessage(char *buffer, unsigned int buf_size)
{
	DWORD bytesRead;
	unsigned int toRead = 0;

	ClearCommError(this->HCom, &this->COMError, &this->COMStatus);

	if (this->COMStatus.cbInQue > 0) 
	{
		if (this->COMStatus.cbInQue > buf_size) 
		{
			toRead = buf_size;
		}
		else toRead = this->COMStatus.cbInQue;
	}

	memset(buffer, 0, buf_size);

	if (ReadFile(this->HCom, buffer, toRead, &bytesRead, NULL)) 
		return int(bytesRead);

	return 0;
}

bool PortComHT::WriteMessage(char *buffer, unsigned int buf_size)
{
	DWORD bytesSend;

	if (!WriteFile(this->HCom, buffer, buf_size, &bytesSend, NULL)) 
	{
		ClearCommError(this->HCom, &this->COMError, &this->COMStatus);
		return false;
	}
 
	return (buf_size == bytesSend);
}

bool PortComHT::isConnected(char *msgerr)
{
	if (!ClearCommError(this->HCom, &this->COMError, &this->COMStatus))
		this->Connected = false;

	if (!this->Connected)
	{
		sprintf(msgerr, this->Errors.data());
	}

	return this->Connected;
}
