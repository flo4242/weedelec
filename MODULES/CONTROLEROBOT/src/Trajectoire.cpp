#include "Trajectoire.h" 

#include "KML.h" 
#include "GereFichierLog.h" 
#include "UtilficGen.h" 
#include "ExportModuleControleRobot.h" //pour structure TGereFichierLog
#include <algorithm>


double Distance2Segment(POINTFLT P, POINTFLT P1, POINTFLT P2, POINTFLT &PH)
//retourne la distance entre le point P et le segment (P1,P2), ainsi que la projection PH du point P sur le segment
//(tir� de TCommandeAutomatique::CalculCapDesire())
{
	POINTFLT U = (P2 - P1) / (P2 - P1).Norme(); //vecteur unitaire du segment P1P2
	POINTFLT P1C = P - P1;
	PH = P1 + U * (P1C | U);  //projection de Pactuel sur le segment P1P2

	return  (P - PH).Norme(); //distance entre P et le segment P1P2
}


bool Trajectoire::Init(TPositionRobot &Pos, char* Nf, TRepereLocal* R, char *msg)
{
	if (!Pos.CapDispo)
	{
		sprintf(msg, "Mode trajectoire sur fichier non utilisable si un seul fix GPS (cap non dispo)");
		return false;
	}

	if (strlen(Nf) == 0)
	{
		sprintf(msg, "Fichier de trajectoire non d�fini");
		return false;
	}

	_list<Waypoint> ListeW;

	char *ext = ChaineExt(Nf);

	if (strcmp(ext, "kml") == 0)
	{
		if (!ImporteKML(Nf, ListeW, msg))	return false;
	}
	else if (strcmp(ext, "log") == 0)
	{
		if (!TGereFichierLog::ImporteLog(Nf, ListeW, msg))	return false;
	}

	else { sprintf(msg, " Extension fichier log non reconnue"); return false; }

	if (ListeW.nbElem < 1)
	{
		sprintf(msg, "Pas de waypoints dans le fichier;\n %s", Nf);
		return false;
	}


	if (ListeW.nbElem > 1)
	{
		//Plusieurs segments --> on souhaite commencer le trajet le long du segment le plus proche afin de ne pas avoir � revenir en arri�re

		//tableau temporaire des points de passage sp�cifi�s par le fichier

		int NbPointsTemp = ListeW.nbElem;

		POINTFLT *PointsPassageTemp = new POINTFLT[NbPointsTemp];
		int i = 0;
		BalayeListe(ListeW)
		{
			Waypoint *pW = ListeW.current();
			R->WGS84VersMetres(pW->LatDeg, pW->LonDeg, PointsPassageTemp[i].x, PointsPassageTemp[i].y);
			i++;
		}

		//recherche du segment le plus proche

		POINTFLT PActuel(Pos.x, Pos.y);

		double dmin = 1E30; //arbitrairement grand

		int indexmin = -1;
		POINTFLT PHmin;

		for (i = 0; i < NbPointsTemp - 1; i++)
		{
			POINTFLT PH; //point projet�
			double d = Distance2Segment(PActuel, PointsPassageTemp[i], PointsPassageTemp[i + 1], PH);
			if (d < dmin)
			{
				//le point projet� est-il sur le segment ?
				double LongueurS = (PointsPassageTemp[i] - PointsPassageTemp[i + 1]).Norme();
				if (((PointsPassageTemp[i] - PH).Norme() <= LongueurS) && ((PointsPassageTemp[i + 1] - PH).Norme() <= LongueurS))
				{
					dmin = d; indexmin = i; PHmin = PH;
				}
			}
		}

		//analyse du r�sultat

		bool SituationDeRattrapage; //indique si l'on est assez proche d'un segment pour consid�rer qu'on va pouvoir le suivre de l� o� l'on est
		int IndexDebut = -1;  //si pas de rattrapage, point de d�but de trajectoire (si -1 , d�but = PHMin)
		bool DebutExistant = true; //indique si le point de d�but est d�j� sur le fichier de trajectoire

		if (indexmin == -1)
		{
			SituationDeRattrapage = false; //on n'a pas trouv� de segment ad�quat
			IndexDebut = 0;
		}

		else //on a un segment proche � consid�rer
		{
			if ((PHmin - PActuel).Norme() >= 0.5) //on est trop loin pour assurer un rattrapage du segment courant (compromettrait le test d'arr�t)
			{
				SituationDeRattrapage = false;
				if ((PHmin - PointsPassageTemp[indexmin + 1]).Norme() < 0.5) IndexDebut = indexmin + 1;
				//presqu'au bout du segment --> on d�marrera au suivant
			}

			else SituationDeRattrapage = true;
		}


		//initialisation effective de la trajectoire

		if (SituationDeRattrapage)
		{
			DBOUT("situation de rattrapage\n");
			//on recopie les points de passage tels quels � partir du segment retenu
			NbPoints = NbPointsTemp - indexmin;
			PointsPassage = new POINTFLT[NbPoints];
			for (i = 0; i < NbPoints; i++) PointsPassage[i] = PointsPassageTemp[i + indexmin];

			IndexCourant = 0;
			P1 = PointsPassage[0];
			P2 = PointsPassage[1];
			LongueurSegment = (P2 - P1).Norme();
			for (i = 0; i < NbPoints; i++) DBOUT("point de passage" << PointsPassage[i].x << " | " << PointsPassage[i].y <<"\n"); 

			sprintf(msg, "D�part en rattrapage le long du segment n� %d\n %d points de passage restants ", IndexCourant, NbPoints - 1);
		}

		else
		{
			DBOUT("pas de situation de rattrapage\n");
			if (IndexDebut == -1) //point de d�part en milieu de segment
			{
				DBOUT("/point de d�part en milieu de segment\n");
				NbPoints = NbPointsTemp - indexmin + 1; //on y rajoute le point de d�part
				PointsPassage = new POINTFLT[NbPoints];
				PointsPassage[0] = PActuel;
				PointsPassage[1] = PHmin; //remplace le d�but du segment s�lectionn�

				for (i = 2; i < NbPoints; i++) PointsPassage[i] = PointsPassageTemp[i -1 + indexmin]; //suite inchang�e
				for (i = 0; i < NbPoints; i++) DBOUT("point de passage" << PointsPassage[i].x << " | " << PointsPassage[i].y << "\n");
			}

			else
			{
				DBOUT("on rajoute le point de d�part\n");
				NbPoints = NbPointsTemp - IndexDebut + 1; //on y rajoute le point de d�part
				PointsPassage = new POINTFLT[NbPoints];
				PointsPassage[0] = PActuel;
				for (i = 1; i < NbPoints; i++) PointsPassage[i] = PointsPassageTemp[i -1 + IndexDebut]; //suite inchang�e
				for (i = 0; i < NbPoints; i++) DBOUT("point de passage" << PointsPassage[i].x << " | " << PointsPassage[i].y << "\n");
			}


			IndexCourant = 0;
			P1 = PointsPassage[0];
			P2 = PointsPassage[1];
			LongueurSegment = (P2 - P1).Norme();

			sprintf(msg, "%d points de passage retenus ", NbPoints - 1);
		}

		delete PointsPassageTemp;


	}//if (ListeW.nbElem > 1)

	if (ListeW.nbElem == 1)
	{
		NbPoints = 2; //on y rajoute le point de d�part
		PointsPassage = new POINTFLT[NbPoints];
		PointsPassage[0] = POINTFLT(Pos.x, Pos.y);
		Waypoint W = ListeW.head->obj;

		R->WGS84VersMetres(W.LatDeg, W.LonDeg, PointsPassage[1].x, PointsPassage[1].y);

		IndexCourant = 0;
		P1 = PointsPassage[0];
		P2 = PointsPassage[1];
		LongueurSegment = (P2 - P1).Norme();

		sprintf(msg, "Trajectoire sans points de passage (point cible unique sp�cifi� dans le fichier)");

	}


	return true;



#ifdef METHODE_FLORIAN

	vector<POINTFLT> PointsPassageTemp; //Contient les points de passage non filtr�s
	vector<float> pairesDistance; //Contient les points de passage non filtr�s
	vector<float> pointsAngles; //Contient les points de passage non filtr�s


	int i = 1;
	int j;
	double X, Y; //stockage temporaire de coordonn�es projet�es
	BalayeListe(ListeW)
	{
		Waypoint *pW = ListeW.current();

		R->WGS84VersMetres(pW->LatDeg, pW->LonDeg, X, Y);
		PointsPassageTemp.push_back(POINTFLT(X, Y));
		DBOUT("point de passage " << i << ":" << X << " - " << Y);
		if (i > 1) {
			//Somme des distances aux deux points de passage
			pairesDistance.push_back((POINTFLT(Pos.x, Pos.y) - PointsPassageTemp[i - 2]).Norme() + (POINTFLT(Pos.x, Pos.y) - PointsPassageTemp[i - 1]).Norme());
			DBOUT("distance � la paire " << i << "-" << i + 1 << ":" << pairesDistance.back());
		}
		//Angle trigonom�trique entre le robot et le point de passage
		pointsAngles.push_back(atan2(Pos.y - PointsPassageTemp[i - 1].y, Pos.x - PointsPassageTemp[i - 1].x)* (180 / M_PI));
		DBOUT("angle au point" << i << ":" << pointsAngles.back());
		i++;
	}

	//On cherche la paire avec la distance minimale
	int minElementIndex = std::min_element(pairesDistance.begin(), pairesDistance.end()) - pairesDistance.begin();
	//float minElement = *std::min_element(pairesDistance.begin(), pairesDistance.end());
	float angleDiff = pointsAngles[minElementIndex] - pointsAngles[minElementIndex + 1];
	if (angleDiff <= -180) angleDiff += 360;
	if (angleDiff >= 180) angleDiff -= 360;

	int premierPoint;
	if (angleDiff > 90) { //On consid�re que l'on est au milieu d'un segment, le deuxi�me point de la paire sera le point de d�part
		premierPoint = minElementIndex + 1;
	}
	else { //les deux points sont globalement dans la m�me direction, le premier point de la paire sera le point de d�part
		premierPoint = minElementIndex;
	}
	DBOUT("point de d�part retenu:" << premierPoint);
	//On peut alors cr�er la liste finale des points de passage
	PointsPassage = new POINTFLT[NbPoints - premierPoint + 1]; //contient la position du robot puis les points de passage filtr�s, allou� sur le tas pour �tre conserv�
	PointsPassage[0] = POINTFLT(Pos.x, Pos.y);

	for (i = premierPoint, j = 1; i < PointsPassageTemp.size(); i++, j++) {
		PointsPassage[j] = PointsPassageTemp[i];
	}


	P1 = PointsPassage[0];
	P2 = PointsPassage[1];
	LongueurSegment = (P2 - P1).Norme();
	IndexCourant = 0;

	sprintf(msg, "Trajectoire charg�e: %d points de passage (%d �cart�s)", NbPoints - minElementIndex, minElementIndex);

	return true;

#endif


}



bool Trajectoire::Init(TPositionRobot &Pos, double distance, char *msg)
{
	//DBOUT("init trajet LIGNE");
	NbPoints = 2;
	PointsPassage = new POINTFLT[NbPoints];

	PointsPassage[0] = POINTFLT(Pos.x, Pos.y);
	PointsPassage[1] = PointDistantSelonDirection(PointsPassage[0], Pos.CapDeg, distance);
	//DBOUT("point 0: " << PointsPassage[0].x << " - " << PointsPassage[0].y << "\n");
	//DBOUT("point 1: " << PointsPassage[1].x << " - " << PointsPassage[1].y << "\n");
	P1 = PointsPassage[0];
	P2 = PointsPassage[1];
	LongueurSegment = (P2 - P1).Norme();
	//DBOUT("longueurSegment:"<< LongueurSegment);
	return true;

}

bool Trajectoire::SegmentSuivant()
{
	//DBOUT("index courant:" << IndexCourant << "\n");
	//DBOUT("nbPoints:" << NbPoints << "\n");
	if (IndexCourant >= NbPoints - 2) return false; //pas d'autre segment
	IndexCourant++;
	P1 = PointsPassage[IndexCourant];
	P2 = PointsPassage[IndexCourant + 1];
	LongueurSegment = (P2 - P1).Norme();

}


double DirectionDegVersPointDistant(POINTFLT PRob, POINTFLT Pdistant)
{
	double capdeg = atan2(Pdistant.y - PRob.y, Pdistant.x - PRob.x)* (180 / M_PI);
	//capdeg indique ici l'angle par rapport � l'axe X g�ographique. Il faut le traduire en cap pour le robot
	capdeg -= 90;
	if (capdeg <= -180) capdeg += 360;
	return capdeg;
}

POINTFLT PointDistantSelonDirection(POINTFLT PRob, double DirectionDeg, double distance)
{
	//traduction de DirectionDeg en angle par rapport � l'axe X g�ographique.
	DirectionDeg += 90;
	double AngleRad = DirectionDeg * (M_PI / 180);
	return PRob + POINTFLT(cos(AngleRad), sin(AngleRad))*distance;
}