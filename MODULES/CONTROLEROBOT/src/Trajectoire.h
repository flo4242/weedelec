#pragma once

#include "ligne.h" //POINTFLT
#include "RepereLocal.h" 
#include "PositionRobot.h" 
#include "DBOUT.h"



struct Trajectoire
{
	POINTFLT P1, P2; //segment courant
	double LongueurSegment; //pour acc�l�rer les tests arr�t dans timer de commande
	int IndexCourant;

	int NbPoints;
	POINTFLT *PointsPassage;

	Trajectoire() : PointsPassage(NULL), IndexCourant(0) { ; }
	~Trajectoire() { if (PointsPassage != NULL) delete PointsPassage; }

	bool Init(TPositionRobot &Pos, char* Nf, TRepereLocal* R, char *msgerr); //selon fichier de trajectoire (kml ou log)
	bool Init(TPositionRobot &Pos, double distance, char *msgerr); //trajet tout droit selon direction actuelle du robot

	bool SegmentSuivant();

};

double DirectionDegVersPointDistant(POINTFLT PRob, POINTFLT Pdistant);

POINTFLT PointDistantSelonDirection(POINTFLT PRob, double DirectionDeg, double distance);
