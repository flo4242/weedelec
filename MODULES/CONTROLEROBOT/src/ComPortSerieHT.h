// Communication Port Serie pour l'envoi des commandes au bo�tier Haute Tension Weedelec
// Code inspir� par Manash Kumar Mandal, r�f�rence : https://github.com/manashmndl/SerialPort

#ifndef __COMPORTSERIEHT_H
#define __COMPORTSERIEHT_H


#define	 HT_COMMUNICATION_SPEED  3000000    // vitesse de communication : 3 Mbps 

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>

#include "PortCOMBaudrate.h"
using namespace std;


struct PortComHT
// port communication pour l'envoi des commandes de Haute Tension Weedelec par la liaison UART standard(niveaux TTL).
{
	HANDLE HCom;
	string Errors;
	bool Connected;

	PortComHT(char *NomPort);

	~PortComHT();

	int ReadMessage(char *buffer, unsigned int buf_size);
	bool WriteMessage(char *buffer, unsigned int buf_size);
	bool isConnected(char *msgerr);


private:
	DWORD COMError;
	COMSTAT COMStatus;
};




#endif // !
