#ifndef __EXPORT_PLATEFORME_H
#define __EXPORT_PLATEFORME_H

#include <string>
#include "commtype.h"
#include "WeedelecCommon.h"
#include "list_tpl.h"
#include "ligne.h" //POINTFLT

#include "CommandeHT.h"
#include "PositionRobot.h"
#include "Trajectoire.h"
#include "GereWeeds.h"
#include <vector>

#include "drvtraiti.h"

_EXPORT_ double CapDegSiUnSeulFixeG = -1000;



struct _EXPORT_ TParamRobot
{
	/**************************/
	/*Param�tres d'utilisateur*/
	/**************************/

	double VitesseMaxLineaire; // vitesse lin�aire maximale

	bool TrajectoireKML;
	double DistanceToutDroit;
	TNomFic NfTrajectoire;

	bool AvecLog;
	TNomFic DossierLog; // dossier pour les logs de navigation
	int PeriodeLog;

	double ZWeeding;


	/**************************/
	/*  Param�tres de d�tail  */
	/**************************/

	// param�tres de connexion
	char AdresseConnexionWifi[30];
	char AdresseConnexionEth[30];
	bool ConnexionWifi;

	// param�tre de l'adresse connexion pour le GPS externe:
	char AdresseServeurGPS[30];
	double ToleranceDistanceInterAntennes;

	//Param�tres de connexion au GPS septentrio
	char adresseSeptentrio[30];
	int portSeptentrio;
	bool guidageFloatAccepte;

	// p�riode de commande
	int PeriodeCommande; // p�riode de la commande robot
	double ConstanteRalentissement;
	double RalentissementMin;

	// param�tres pour calage du bras Delta du robot
	double RayonHexagone;
	double ResolutionHexagone;
	double TranslationDeltaX;
	double TranslationDeltaY;
	bool getZero;
	bool  getShortest;
	TNomFic fichierCoor;
	bool hexagoneReel;
	// param�tre de commande Haute Tension
	TParamHT ParamComHT;

	TParamRobot() : VitesseMaxLineaire(0.4), TrajectoireKML(false), DistanceToutDroit(0.5), AvecLog(false), ConnexionWifi(false)
	{
		NfTrajectoire[0] = '\0';
		DossierLog[0] = '\0';
		PeriodeLog = 1000;
		ZWeeding = 0.92;

		// param�tres de connexion
		sprintf(AdresseConnexionWifi, "192.168.8.8:8080");
		sprintf(AdresseConnexionEth, "erxbot-036.local:8080");

		// param�tre de l'adresse connexion pour le GPS externe:
		sprintf(AdresseServeurGPS, "http://gps.local:5000");

		ToleranceDistanceInterAntennes = 0.2;
		PeriodeCommande = 100;
		ConstanteRalentissement = 0.1; //m�tres
		RalentissementMin = 0.2; //sans unit�, pour �viter vitesse infiniment faible avant l'arr�t


		// param�tres pour calage du bras Delta du robot
		RayonHexagone = 0.5;
		ResolutionHexagone = 0.001;
		TranslationDeltaX = 0.475;
		TranslationDeltaY = -0.785;
		getZero = true;
	}
};


struct _EXPORT_ TRetourPlateforme :public TRetourTask
{
	int DureeMs;
	bool CropRowFinished;
	bool ErrorDelta;
};

struct _EXPORT_ TStateInitPlateforme
{
	bool RobotConnecte;
	bool SeptentrioGPSConnecte;
	bool RasberryGPSConnecte;
	bool RepereLocalInitialise;
	TStateInitPlateforme() : RobotConnecte(false), RasberryGPSConnecte(false), RepereLocalInitialise(false) { ; }
};


typedef void(*pcallbackStep)(TRetourPlateforme ret);

struct _EXPORT_ TModulePlateforme
{


	//valeur utilis�e pour le d�placement tout droit et les changements de rep�re en weeding si un seul fix GPS est disponible.
	//la valeur par d�faut -1000 permet de d�tecter l'absence d'initialisation. L'initialisation peut se faire par pilotage manuel
	//dans une fonction du menu "Commandes de navigation"

	//fonctions export�es pour utilisation externe (autres modules)

	static bool TModulePlateforme::ChargeParametres(TParamRobot &param);
	// charge des param�tres du module plateforme

	static TStateInitPlateforme CheckInitialisationPlateforme(); 

	//avance la plateforme jusqu'� la position d'acquisition/d�sherbage suivante
	static void TModulePlateforme::GoToNextStep(Trajectoire *ptrajectoire, double step, pcallbackStep cb);
	// le retour des fonctions d'avancement (GotoNextStep et AvancementManuel) se fait par la fonction retour pcallbackStep

	//static void TModulePlateforme::AvancementManuel(TRetourPlateforme &ret, DRVTRAITIM *pD, TPositionRobot &posLocaleDebut, pcallbackStep cb);
	// avancement manuel par le panneau contr�l (pour le cas o� Gps RTK fonctionne mal)

	static void TModulePlateforme::AvancementManuel(DRVTRAITIM *pD, pcallbackStep cb);

	static Trajectoire * TModulePlateforme::InitTrajectoire(char *msgerr);

	
	static bool TModulePlateforme::GetPositionRobot(TPositionRobot &Pos, char *msgerr);

	static bool TModulePlateforme::GetPositionRobot(double &LatDeg, double &LonDeg, double &CapDeg, char *msgerr);

	static TWeedBras TModulePlateforme::PositionRobotVersPositionBras(TWeedRobot &WR);


	static bool  TModulePlateforme::ResetBrasDroit(TRetourPlateforme &ret);
	static void TModulePlateforme::WeedingSuperviseur(_list<TGroupeWeeds> *PileGroupeWeeds, TRetourPlateforme &ret, bool AvecHauteTension, bool AvecRecalageManuel);
	static void TModulePlateforme::Weeding(_list<TWeedBras> ListeWeeds, TRetourPlateforme &ret, bool AvecHauteTension, bool AvecRecalageManuel);
	static void TModulePlateforme::Weeding_pallier(_list<TWeedBras> ListeWeeds, TRetourPlateforme &ret, bool AvecHauteTension, bool AvecRecalageManuel, float intervaleDescente, float startH, float limitH, float p);


	//static bool TModulePlateforme::GetPositionGPSActuelle(double &LatDeg, double &LonDeg, char *msgerr);

	static TRepereLocal* TModulePlateforme::GetRepereLocal();

	static bool TModulePlateforme::VerificationHauteTension();

	static void TModulePlateforme::Stop(); //demande d'arr�t utilisateur

	static bool TModulePlateforme::getEtatBras(TRetourPlateforme &res);
	static bool TModulePlateforme::getGPSRTKPosition(TPositionRobot Pos, char* msgerr);
};


void RetourneStateError(TRetourPlateforme &ret, int timedebut, char* infoRet);
// fonction qui g�re le retour dans les cas d'erreur

#endif


