#include "GetListePortsCOM.h"

#include <Winreg.h>
#include <stdio.h> //pour sprintf



char * GetPort(char *DescriptionPort)
/************************************/
	{
		int l = strlen(DescriptionPort);
		for(int i=0; i<l-1; i++)
			if(DescriptionPort[i]=='\t') return DescriptionPort+i+1;

		return NULL;
	}


void AligneDescriptions(_list<TNf> &ListeDescriptions)
/****************************************************/
{

	//taille max de la partie descriptive ?
	int tmax=0;
	BalayeListe(ListeDescriptions)
	{
		char *pnt = *ListeDescriptions.current();
		char *pnt2 = GetPort(pnt);
		if(pnt2)	tmax = max(tmax, pnt2 -  pnt) ;
		else		tmax = max(tmax, strlen(pnt)) ;
	}

	//et modification de la liste
	BalayeListe(ListeDescriptions)
	{
		char *pnt = *ListeDescriptions.current();
		char *pnt2 = GetPort(pnt);
		TNomFic Temp;
		if(pnt2)
		{
			TNomFic Temp;
			int Linitiale = pnt2 - pnt -1;
			for(int i=0; i<Linitiale; i++) Temp[i] = pnt[i];
			int nbEspaces = (tmax-Linitiale)*2 + 5; 
			//1.5: compense le fait que les espaces sont plus petits que la taille moyenne d'un caract�re
			//+5: le TAB est sans effet dans les bo�tes de dialogue

			int Lfinal = Linitiale+nbEspaces;

			for(int i=Linitiale; i<Lfinal; i++) Temp[i] = ' '; //compl�ment avec des espaces
			sprintf(Temp+Lfinal, "\t%s", pnt2);
			strcpy(pnt, Temp);
		}
	}

}


bool GetListePortsCOM(_list<TNf> &Liste)
/**************************************/
{
	Liste.clear();
	
	HKEY KeyHandle = NULL;
	if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, "HARDWARE\\DEVICEMAP\\SERIALCOMM", 0, KEY_READ, &KeyHandle) !=  ERROR_SUCCESS)
		return false;

	int index = 0;
	
	while(true)
	{

		char name[256];
		DWORD size = 256;
	
		DWORD type;
	
		BYTE value[100];
		DWORD sizevalue = 100;
		
		int ret = RegEnumValue(KeyHandle, index++, name, &size, NULL, &type, value, &sizevalue);

		if (ret == ERROR_NO_MORE_ITEMS)	break;  //on sort directement depuis l� si pas de p�riph�rie existe

		if (type != REG_SZ)	continue;			//on attend une cha�ne

		if (ret == ERROR_MORE_DATA)	continue;	//trop longue ...

		if (ret != ERROR_SUCCESS)
		{
			RegCloseKey(KeyHandle);
			return false;
		}
	
	TNomFic Total;
	sprintf(Total,"%s\t%s", name, value);
	Liste.push_back(Total);
	
	}
	RegCloseKey(KeyHandle);
	return true;
}
	
	/*TRegistry *Registry = new TRegistry(KEY_READ);
      AnsiString portNb ="";
 
      Registry->RootKey = HKEY_LOCAL_MACHINE;
      // The com ports are administered in the following directory.
      // false, because no entry is supposed to be created.
      Registry->OpenKey("HARDWARE\\DEVICEMAP\\SERIALCOMM", false);
 
      // Extract the COM port names and store them.
      Registry->GetValueNames(availableComPorts);
      // List the available COM ports in the corresponding combo box.
      for (int i(0); i < availableComPorts->Count; i++)
      {
          portNb = Registry->ReadString(availableComPorts->Strings[i]);
          ComboBox1->AddItem(portNb, NULL);
      }
      delete Registry;
      delete availableComPorts;*/
	  
	  