#include "ControlRobot.h"

#include "DialogSendHWCommand.h"
#include "IDControlPad.h"

extern TControlRobot ControlRobotG;

DRVTRAITIM *pDriverG;

struct CmdRobotArm
{
	bool LeftArm;
	bool RightArm;

	CmdRobotArm() { LeftArm = false; RightArm = false; };

} RobotArmG;

BOOL CALLBACK SendHWCommandDialogProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{

	case WM_INITDIALOG:
	{
			if (RobotArmG.LeftArm == true)
				CheckDlgButton(hwnd, IDC_CHECK_LEFT, BST_CHECKED);
			else
				CheckDlgButton(hwnd, IDC_CHECK_LEFT, BST_UNCHECKED);

			if (RobotArmG.RightArm == true)
				CheckDlgButton(hwnd, IDC_CHECK_RIGHT, BST_CHECKED);
			else
				CheckDlgButton(hwnd, IDC_CHECK_RIGHT, BST_UNCHECKED);

			return TRUE;
	}


	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
			//commandes d'initialisation

		case IDC_BUTTON_ON: //bouton ON: allume les bras qui sont valid�s
		{
			char msg_temp[20];
			char msg_cmd[20] = "";

			RobotArmG.LeftArm = (IsDlgButtonChecked(hwnd, IDC_CHECK_LEFT) == BST_CHECKED);
			RobotArmG.RightArm = (IsDlgButtonChecked(hwnd, IDC_CHECK_RIGHT) == BST_CHECKED);
			
			if (!RobotArmG.LeftArm && !RobotArmG.RightArm)
				break;

			if (RobotArmG.LeftArm)
			{
				sprintf(msg_cmd, "d1on");
				pDriverG->PrintTexte("%s\n", msg_cmd);
				ControlRobotG.SendHWCommand(msg_cmd);
			}

			if (RobotArmG.RightArm)
			{
				sprintf(msg_cmd, "d2on");
				pDriverG->PrintTexte("%s\n", msg_cmd);
				ControlRobotG.SendHWCommand(msg_cmd);
			}

			break;
		}
		case IDC_BUTTON_OFF: //bouton OFF: �teint les bras qui sont valid�s
		{
			char msg_cmd[20] = "";

			if (!RobotArmG.LeftArm && !RobotArmG.RightArm)
				break;

			if (RobotArmG.LeftArm)
			{
				sprintf(msg_cmd, "d1off");
				pDriverG->PrintTexte("%s\n", msg_cmd);
				ControlRobotG.SendHWCommand(msg_cmd);
			}

			if (RobotArmG.RightArm)
			{
				sprintf(msg_cmd, "d2off");
				pDriverG->PrintTexte("%s\n", msg_cmd);
				ControlRobotG.SendHWCommand(msg_cmd);
			}

			break;
		}


		case IDC_BUTTON_HOME: //bouton HOME: met en position HOME les bras qui sont valid�s
		{
			char msg_cmd[20] = "";

			if (!RobotArmG.LeftArm && !RobotArmG.RightArm)
				break;

			if (RobotArmG.LeftArm)
			{
				sprintf(msg_cmd, "d1home");
				pDriverG->PrintTexte("%s\n", msg_cmd);
				ControlRobotG.SendHWCommand(msg_cmd);
			}

			if (RobotArmG.RightArm)
			{
				sprintf(msg_cmd, "d2home");
				pDriverG->PrintTexte("%s\n", msg_cmd);
				ControlRobotG.SendHWCommand(msg_cmd);
			}

			break;
		}

		case IDC_BUTTON_RESET: //bouton RESET: reset les bras qui sont valid�s
		{
			char msg_cmd[20] = "";

			if (!RobotArmG.LeftArm && !RobotArmG.RightArm)
				break;

			if (RobotArmG.LeftArm)
			{
				sprintf(msg_cmd, "d1reset");
				pDriverG->PrintTexte("%s\n", msg_cmd);
				ControlRobotG.SendHWCommand(msg_cmd);
			}

			if (RobotArmG.RightArm)
			{
				sprintf(msg_cmd, "d2reset");
				pDriverG->PrintTexte("%s\n", msg_cmd);
				ControlRobotG.SendHWCommand(msg_cmd);
			}

			break;
		}

		
		//commandes de mouvement
		
		case IDC_BUTTON_CMD_LEFT:  //lance le mouvement vers la position sp�cifi�e pour le bras gauche
		{
			char msg_cmd[100] = "";

			if (!RobotArmG.LeftArm)
				break;


			TCHAR PosX[100], PosY[100], PosZ[100];

			// ordre de commande : 1� suivant l'axe z pour d�scendre, ensuite on passe la commande suivant l'axe x et puis y
			if (IsDlgButtonChecked(hwnd, IDC_CHECK_POSZ) == BST_CHECKED)
			{
				GetDlgItemText(hwnd, IDC_EDIT_POSZ, PosZ, 101);
				sprintf(msg_cmd, "d1tz %s", PosZ);
				pDriverG->PrintTexte("%s\n", msg_cmd);
				ControlRobotG.SendHWCommand(msg_cmd);
			}

			if (IsDlgButtonChecked(hwnd, IDC_CHECK_POSX) == BST_CHECKED)
			{
				if (IsDlgButtonChecked(hwnd, IDC_CHECK_POSY) == BST_CHECKED)
				{
					GetDlgItemText(hwnd, IDC_EDIT_POSX, PosX, 101);
					GetDlgItemText(hwnd, IDC_EDIT_POSY, PosY, 101);
					sprintf(msg_cmd, "d1tx %s", PosX);
					pDriverG->PrintTexte("%s\n", msg_cmd);
					ControlRobotG.SendHWCommand(msg_cmd);

					sprintf(msg_cmd, "d1ty %s", PosY);
					pDriverG->PrintTexte("%s\n", msg_cmd);
					ControlRobotG.SendHWCommand(msg_cmd);
				}
				else
				{
					GetDlgItemText(hwnd, IDC_EDIT_POSX, PosX, 101);
					sprintf(msg_cmd, "d1tx %s", PosX);
					pDriverG->PrintTexte("%s\n", msg_cmd);
					ControlRobotG.SendHWCommand(msg_cmd);
				}
			}
			else if (IsDlgButtonChecked(hwnd, IDC_CHECK_POSY) == BST_CHECKED)
			{
				GetDlgItemText(hwnd, IDC_EDIT_POSY, PosY, 101);
				sprintf(msg_cmd, "d1ty %s", PosY);
				pDriverG->PrintTexte("%s\n", msg_cmd);
				ControlRobotG.SendHWCommand(msg_cmd);
			}
	
			break;
		}

		case IDC_BUTTON_CMD_RIGHT: //lance le mouvement vers la position sp�cifi�e pour le bras droit
		{
			char msg_cmd[100] = "";

			if (!RobotArmG.RightArm)
				break;

			TCHAR PosX[100], PosY[100], PosZ[100];

			// ordre de commande : 1� suivant l'axe z pour descendre, ensuite on passe la commande suivant l'axe x et puis y
			if (IsDlgButtonChecked(hwnd, IDC_CHECK_POSZ) == BST_CHECKED)
			{
				GetDlgItemText(hwnd, IDC_EDIT_POSZ, PosZ, 101);
				sprintf(msg_cmd, "d2tz %s", PosZ);
				pDriverG->PrintTexte("%s\n", msg_cmd);
				ControlRobotG.SendHWCommand(msg_cmd);
			}

			if (IsDlgButtonChecked(hwnd, IDC_CHECK_POSX) == BST_CHECKED)
			{
				if (IsDlgButtonChecked(hwnd, IDC_CHECK_POSY) == BST_CHECKED)
				{
					GetDlgItemText(hwnd, IDC_EDIT_POSX, PosX, 101);
					GetDlgItemText(hwnd, IDC_EDIT_POSY, PosY, 101);
					sprintf(msg_cmd, "d2tx %s", PosX);
					pDriverG->PrintTexte("%s\n", msg_cmd);
					ControlRobotG.SendHWCommand(msg_cmd);
					
					sprintf(msg_cmd, "d2ty %s", PosY);
					pDriverG->PrintTexte("%s\n", msg_cmd);
					ControlRobotG.SendHWCommand(msg_cmd); 
				}
				else
				{
					GetDlgItemText(hwnd, IDC_EDIT_POSX, PosX, 101);
					sprintf(msg_cmd, "d2tx %s", PosX);
					pDriverG->PrintTexte("%s\n", msg_cmd);
					ControlRobotG.SendHWCommand(msg_cmd);
				}
			}
			else if (IsDlgButtonChecked(hwnd, IDC_CHECK_POSY) == BST_CHECKED)
			{
				GetDlgItemText(hwnd, IDC_EDIT_POSY, PosY, 101);
				sprintf(msg_cmd, "d2ty %s", PosY);
				pDriverG->PrintTexte("%s\n", msg_cmd);
				ControlRobotG.SendHWCommand(msg_cmd);
			}

			break;
		}

		case IDC_CHECK_LEFT:
			break;

		case IDC_CHECK_RIGHT:
			break;

		case IDC_EDIT_POSX:
			break;

		case IDC_EDIT_POSY:
			break;

		case IDC_EDIT_POSZ:
			break;

		case IDC_CHECK_POSX:
			break;

		case IDC_CHECK_POSY:
			break;

		case IDC_CHECK_POSZ:
			break;

		default: 
			EndDialog(hwnd, 0); 
			return TRUE;
		}
		break;
	}

	default: return FALSE;
	}
}

HMODULE DialogSendHWCommand(TNomFic NomModule)
{
	// r�cup�ration du nom du module dll 
	TNomFic NomComplet;

#ifdef _DEBUG
	sprintf(NomComplet, "%s_64d", NomModule);
#else
	sprintf(NomComplet, "%s_64", NomModule);
#endif

	HMODULE HVS = GetModuleHandle(NomComplet);

	DialogBox(HVS, MAKEINTRESOURCE(ID_HWCMD), NULL, (DLGPROC)SendHWCommandDialogProc);

	return HVS;
}