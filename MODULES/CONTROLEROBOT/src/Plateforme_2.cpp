// dans le module plateforme2, on utilise le syst�me de deux antennes de GPS externe pour le guidage


#include "ControlRobot.h" 
// il faut toujours inclure "ControlRobot.h" en premier pour que "erxCom.h" soit d�clar� en premier 
// (sinon incompatibilit� dans le module 'chrono') 

#include "Plateforme_2.h"
#include "CommandeNavigation_2.h"
#include "ExportModuleControleRobot.h"
#include "GetPortsCOM.h"
#include "GPSReceiverS.h"

#include "NmeaPositionTCPIP.h"

#ifndef VITESSEMAX
#define VITESSEMAX  0.4   
#endif
//externs d�finis ailleurs et r�cup�r�s ici
extern TParamRobot ParamG;
extern TControlRobot ControlRobotG;
extern TCommandeNavigation *CommandeNavigationG;

TRepereLocal *RepereG = NULL;
Hexagone *pHexagoneBras = NULL;

GPSReceiverSeptentrio GPSReceiverS;





TStateInitPlateforme InitPlateformeRobot(DRVTRAITIM *pD, TParamRobot &param, bool TempoDebut) {
	TStateInitPlateforme ret;
	char msg[1000];
	pD->PrintTexte("Connexion du robot ...\n");
	MSG MsgWin;
	while (PeekMessage(&MsgWin, pD->HWindow(), 0, 0, PM_REMOVE)) 	DispatchMessage(&MsgWin);
	//pour affichage imm�diat ligne d'avertissement attente ci-dessus

	ControlRobotG.SetConnectAddress(param.ConnexionWifi ? param.AdresseConnexionWifi : param.AdresseConnexionEth);

	ret.RobotConnecte = ControlRobotG.ConnectRobot(msg);

	if (!ret.RobotConnecte) { pD->PrintTexte(msg);}
	else {pD->PrintTexte("Robot connect�");}
	return ret;
}

TStateInitPlateforme InitPlateformeGPS(DRVTRAITIM *pD, TParamRobot &param, bool TempoDebut)
{
	
	TStateInitPlateforme ret;
	char msg[1000];
	pD->PrintTexte("\n\nConnexion du module GPS Septentrio ...\n");

	GPSReceiverS.SetConnectAddress(param.adresseSeptentrio, param.portSeptentrio);
	bool retS = GPSReceiverS.Connect(msg);

	if(!retS) pD->PrintTexte("Echec initialisation: %s \n", msg);
	
	
	ret.SeptentrioGPSConnecte = retS;
	NMEAData dataInit;

	Sleep(500);
	if (GPSReceiverS.GetRawData(dataInit, msg)) {
		RepereG = new TRepereLocal(dataInit.LatDeg, dataInit.LonDeg);
		ret.RepereLocalInitialise = true;
		pD->PrintTexte("repere local init \n");
	}
	else {
		pD->PrintTexte("repere local NON init, conditions fix/float insuffisantes \n");
		ret.RepereLocalInitialise = false;
	}


	

	return ret;
}



TStateInitPlateforme InitPlateforme(DRVTRAITIM *pD, TParamRobot &param, bool TempoDebut) //appel� au lancement du module, initialise le robot, le GPS et le rep�re local
{
	TStateInitPlateforme ret,retCombi; //retour sur l'�tat des 3 composantes
	pD->OuvreTexte();
	pD->EffaceTexte();

	pD->PrintTexte("INITIALISATION DU ROBOT, DU GPS ET DU REPERE LOCAL \n");

	//Possibilit� d'annuler l'op�ration si la fonction est appell�e au d�marrage du logiciel
	if (TempoDebut)
	{
		pD->PrintTexte("      (touche espace ou clic gauche avant 3 secondes pour annuler)\n");
		//test d'abandon
		DWORD debut = GetTickCount();
		while (GetTickCount() - debut < 3000)
		{
			if (   ((GetAsyncKeyState(' ') & 0x8000) != 0) || (pD->TestCliqueGauche()) )
			{
				pD->PrintTexte(" \n\n Abandon utilisateur ");
				return ret;
			}
		}
	}
	//CONNEXION ROBOT
	ret=InitPlateformeRobot(pD, param, TempoDebut);
	retCombi.RobotConnecte = ret.RobotConnecte;
	//CONNEXION GPS
	ret=InitPlateformeGPS(pD, param, TempoDebut);
	retCombi.SeptentrioGPSConnecte = ret.SeptentrioGPSConnecte;
	// d�finition de l'espace de travail pour weeding:
	if (pHexagoneBras != NULL)
	{
		delete pHexagoneBras; pHexagoneBras = NULL;
	}
	pHexagoneBras = new Hexagone(ParamG.RayonHexagone, ParamG.ResolutionHexagone);
	ret.RepereLocalInitialise = true;
	retCombi.RepereLocalInitialise = ret.RepereLocalInitialise;
	pD->PrintTexte("\n\nHexagone de travail du bras initialis�");
	return ret;
}


TStateInitPlateforme CheckInitPlateforme()
{
	TStateInitPlateforme ret;
	//ret.RobotConnecte = ControlRobotG.RobotIsConnected();
	ret.RobotConnecte = true;
	ret.SeptentrioGPSConnecte = true;
	ret.SeptentrioGPSConnecte = GPSReceiverS.IsConnected();
	ret.RepereLocalInitialise = (RepereG != NULL);
	return ret;
}


void ReleasePlateforme()
{
	//d�connexion du robot
	char msgerr[1000]; ControlRobotG.DeconnectRobot(msgerr);


	
	if (RepereG != NULL) // d�truit rep�re local
	{
		delete RepereG;
		RepereG = NULL; // permettra la reconstruction d'un rep�re local d�s la reconnexion
	}

	if (pHexagoneBras != NULL)
	{
		delete pHexagoneBras; pHexagoneBras = NULL;
	}

	if (CommandeNavigationG != NULL) // d�truit la commande de navigation (soit manuelle, soit automatique)
	{
		if (CommandeNavigationG->Timer_Commande) CommandeNavigationG->StopTimer();
		delete CommandeNavigationG; CommandeNavigationG = NULL;
	}

	if (ControlRobotG.ArmInitialised)
	{
		ControlRobotG.CloseRightArm();
		ControlRobotG.ArmInitialised = false;
	}

}






bool FailPosErrCallback(PositionRobotState statePosition, TPositionRobot &pos,  char *UTCTime) {
	switch (statePosition) {
	case PositionRobotState::PositionOK: return true;
	default: return false; //tout ce qui n'est pas ok est fatal
	}
}
bool FixPosErrCallback(PositionRobotState statePosition,  TPositionRobot &pos, char *UTCTime) {
		int timeLimit = 30;
		int timeInterval = 1;
		int tentative = 0;
		DWORD debut = GetTickCount();
		char msg[1000];
		bool flagFix = true;
		switch (statePosition) {
			case PositionRobotState::PositionOK: return true;
			case PositionRobotState::ConnexionErr:
				//Boucle de reconnexion au GPS
				while (!GPSReceiverS.Connect(msg))
				{
					tentative++;
					DBOUT("tentative reconnexion GPS" << tentative <<"\n");
					if (((GetAsyncKeyState(' ') & 0x8000) != 0) || (GetTickCount() - debut > timeLimit * 1000) )
					{
						flagFix = false;
						break;
					}
					Sleep(timeInterval * 1000); //pause d'une seconde entre les tentatives
				}
				if (flagFix) {
					//On tente de rer�cup�rer la position et �ventuellement d'arranger un autre probl�me
					return PositionRobotEstimee(pos, msg, FixPosErrCallback, UTCTime) == PositionRobotState::PositionOK;
				}
				return false;
			case PositionRobotState::FixFloatErr:
				//Boucle de relecture
				while (!PositionRobotEstimee(pos, msg, FailPosErrCallback, UTCTime) )
				{
					tentative++;
					DBOUT("Tentative retour fix/float" << tentative << "\n");
					if (((GetAsyncKeyState(' ') & 0x8000) != 0) || (GetTickCount() - debut > timeLimit * 1000))
					{
						flagFix = false;
						break;
					}
					Sleep(timeInterval * 1000);
				}
				if (flagFix) {
					return PositionRobotEstimee(pos, msg, FixPosErrCallback, UTCTime) == PositionRobotState::PositionOK;
				}
				return false;
			case PositionRobotState::NMEARecupErr:
				//Boucle de relecture
				while (!PositionRobotEstimee(pos, msg, FailPosErrCallback, UTCTime))
				{
					tentative++;
					DBOUT("Tentative r�cup trame NMEA valide" << tentative << "\n");
					if (((GetAsyncKeyState(' ') & 0x8000) != 0) || (GetTickCount() - debut > timeLimit * 1000))
					{
						flagFix = false;
						break;
					}
					Sleep(timeInterval * 1000);
				}
				if (flagFix) {
					return PositionRobotEstimee(pos, msg, FixPosErrCallback, UTCTime) == PositionRobotState::PositionOK;
				}
				return false;
		}
		return false; //erreur inconnue, fatal
}


bool PositionRobotEstimee(TPositionRobot &pos, char *msgerr, std::function<bool(PositionRobotState, TPositionRobot&, char*)> callback, char *UTCTime50)
// r�ception de position estim�e du robot par le syst�me de GPS externe, 
// dont la communication �tablie par la structure GPSReceiver
{
	if (!GPSReceiverS.IsConnected())
	{
		//PrintTexte("Exception PositionRobotEstimee: GPS non connect� \n");
		sprintf(msgerr, "GPS Septentrio non connect�");
		return callback(PositionRobotState::ConnexionErr, pos, UTCTime50);
	}

	if (RepereG == NULL) { //erreur fatale si on perd le repere local, ne peut �tre rattrap�
		//PrintTexte("Exception fatale PositionRobotEstimee: rep�re non initialis� \n");
		sprintf(msgerr, "Rep�re local non initialis�");
		return false;
	}



	NMEAData Data;
	if (!GPSReceiverS.GetRawData(Data, msgerr)) {
		//PrintTexte("Exception PositionRobotEstimee: erreur GPSReceiverS.GetRawData \n");
		sprintf(msgerr, "Erreur recup position GPSReceiverS.GetRawData");
		return callback(PositionRobotState::NMEARecupErr, pos, UTCTime50);
	}
	//V�rification GPS, tout ce qui n'est pas fix ou float sera automatiquement rejet�
	//Le float peut �tre rejet� selon la valeur de ParamG.guidageFloatAccepte
	if (!(Data.Fix == 2 || Data.Fix == 4)) {
		//PrintTexte("Exception PositionRobotEstimee: pas de fix/float dispo \n");
		DBOUT("Erreur recup position: pas de fix/float dispo");
		sprintf(msgerr, "Erreur recup position: pas de fix/float dispo");
		return callback(PositionRobotState::FixFloatErr, pos, UTCTime50);
		//DBOUT("TRAME RECUE SANS FIX 2 OU 4");
	}
	if (!ParamG.guidageFloatAccepte &&  Data.Fix == 2) {
		//PrintTexte("Exception PositionRobotEstimee: float non accept� \n");
		DBOUT("Erreur recup position: float non accept�");
		sprintf(msgerr, "Erreur recup position: float non accept�");
		return callback(PositionRobotState::FixFloatErr, pos, UTCTime50);
	}

	RepereG->WGS84VersMetres(Data.LatDeg, Data.LonDeg, pos.x, pos.y);
	//DBOUT("\nposition transform�e: x=" << X << " y=" << Y << "\n");
	pos.CapDeg = Data.CapDeg;
	pos.rollDeg=Data.rollDeg;
	pos.pitchDeg=Data.pitchDeg;
	
	pos.CapDispo = true;
	pos.elevation = Data.Altitude;
	pos.fix = pos.fixDroit = pos.fixGauche = Data.Fix;

	if (UTCTime50 != NULL)	strcpy(UTCTime50, Data.UTCTimeISO);


	return true;
}

void StopPlateforme()
{
	if(CommandeNavigationG != NULL)
		CommandeNavigationG->StopTimer();
	
	if (ControlRobotG.RobotIsConnected())
		ControlRobotG.SetRobotSpeed(0, 0);
}

