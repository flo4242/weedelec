
#include "ControlRobot.h"
#include "Plateforme_2.h"
#include "CommandeHT.h"

#include "DialogueHauteTension.h"
#include "IDControlPad.h"

extern TControlBrasManuel ControlBrasManuelG;
extern CommandeHT CommandeHTG;
extern TControlRobot ControlRobotG;

extern DRVTRAITIM *pDriverG;


#ifndef POSITIONZ_WEEDING
#define POSITIONZ_WEEDING   0.92
#endif

#ifndef POSITIONZ_POSITIONING
#define POSITIONZ_POSITIONING  0.8
#endif


BOOL CALLBACK HauteTensionDialogProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{

		case WM_INITDIALOG:
		{
			if (!ControlRobotG.RobotIsConnected())
			{
				pDriverG->OuvreTexte();
				pDriverG->EffaceTexte();
				pDriverG->PrintTexte("Echec : Robot non initialis�");
				return FALSE;
			}

			if (!ControlBrasManuelG.ActiveControlManuel)
				ControlBrasManuelG.ActiveControlManuel = true;

			if (!ControlRobotG.ArmInitialised)
			{
				ControlRobotG.InitRightArm();
				ControlRobotG.ArmInitialised = true;
			}
			
			return TRUE;
		}


		case WM_COMMAND:
		{
			switch (LOWORD(wParam))
			{
				case IDC_BUTTON_BRASUP:
				{
					ControlBrasManuelG.Hauteur = POSITIONZ_POSITIONING;
					ControlRobotG.SetHeightR(ControlBrasManuelG.Hauteur);
					break;
				}

				case IDC_BUTTON_BRASDOWN:
				{
					ControlBrasManuelG.Hauteur = POSITIONZ_WEEDING;
					ControlRobotG.SetHeightR(ControlBrasManuelG.Hauteur);
					break;
				}

				case IDC_BUTTON_XPLUS:
				{
					ControlBrasManuelG.PositionXPlus();
					POINTFLT posBras = ControlBrasManuelG.GetConsignePositionBras();
					ControlRobotG.SetPositionXYWeedR(posBras,POINTFLT(0,0));
					break;
				}

				case IDC_BUTTON_XMOINS:
				{
					ControlBrasManuelG.PositionXMoins();
					POINTFLT posBras = ControlBrasManuelG.GetConsignePositionBras();
					ControlRobotG.SetPositionXYWeedR(posBras, POINTFLT(0, 0));
					break;
				}

				case IDC_BUTTON_YPLUS:
				{
					ControlBrasManuelG.PositionYMoins();  // attention : la sens du bras est invers�e !!
					POINTFLT posBras = ControlBrasManuelG.GetConsignePositionBras();
					ControlRobotG.SetPositionXYWeedR(posBras, POINTFLT(0, 0));
					break;
				}

				case IDC_BUTTON_YMOINS:
				{
					ControlBrasManuelG.PositionYPlus();  // attention : la sens du bras est invers�e !!
					POINTFLT posBras = ControlBrasManuelG.GetConsignePositionBras();
					ControlRobotG.SetPositionXYWeedR(posBras, POINTFLT(0, 0));
					break;
				}

				case IDC_BUTTON_LANCEHT:
				{
					if (CommandeHTG.ComHT == NULL) break;
					else
					{
						int courantMax, duration, voltage;
						char msgerr[1000];
						CommandeHTG.StartSequence(courantMax, voltage, duration, msgerr);
						pDriverG->PrintTexte("Info S�quence : \nCourant max: %d (mA); Tension moyenne: %d (V); Dur�e: %d (us)\n",
							courantMax, voltage, duration);
					}
					break;
				}

				case IDC_BUTTON_ZPLUS:
				{
					if (CommandeHTG.ComHT == NULL) break;
					else
					{
						ControlBrasManuelG.PositionZPlus();  
						ControlRobotG.SetHeightR(ControlBrasManuelG.Hauteur);
					}
					break;
				}

				case IDC_BUTTON_ZMOINS:
				{
					if (CommandeHTG.ComHT == NULL) break;
					else
					{
						ControlBrasManuelG.PositionZMoins();
						ControlRobotG.SetHeightR(ControlBrasManuelG.Hauteur);
					}
					break;
				}
				default:
				{
					ControlBrasManuelG.ActiveControlManuel = false;
					ControlRobotG.SetPositionAwaitedR();
					ControlBrasManuelG.ResetPositionBras();
					ControlBrasManuelG.Hauteur = POSITIONZ_POSITIONING;  // retourne � la position Wait
					EndDialog(hwnd, 0); // fermer la dialogue
					return TRUE;
				}
			}
			break;
		}

		default:
			return FALSE;
	}
}


void DialogHauteTension(TNomFic NomModule)
{
	// r�cup�ration du nom du module dll 
	TNomFic NomComplet;

#ifdef _DEBUG
	sprintf(NomComplet, "%s_64d", NomModule);
#else
	sprintf(NomComplet, "%s_64", NomModule);
#endif

	HMODULE HVS = GetModuleHandle(NomComplet);

	DialogBox(HVS, MAKEINTRESOURCE(ID_HauteTension), NULL, (DLGPROC)HauteTensionDialogProc);

	return;
}