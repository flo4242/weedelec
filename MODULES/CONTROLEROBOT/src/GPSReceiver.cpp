
#include "GPSReceiver.h" //classe GPSReceiver et GPSReceiverSeptentrio
#include "commtype.h" //pour M_PI
// param�tres constants

#define		CLIENT_TIMEOUT		1.5     // timeout de 1.5 secondes
#define		RTKFIX		4
#define		RTKFLOAT	5

// les cl�s de json

#define		AGEG	"age.gauche"
#define		AGED	"age.droite"
#define		RTKG	"gauche_gps_qual"		// rtk gauche
#define		RTKD	"droite_gps_qual"		// rtk droite
#define		LATG	"lat1"
#define		LATD	"lat2"
#define		LONG	"lon1"
#define		LOND	"lon2"


// FONCTIONS MEMBRES PRIVEES

bool GPSReceiver::SetGPSClient(std::string uri, char* msgerr)
{

	if (uri.length() == 0)
	{
		sprintf(msgerr, "GPSReceiver: uri non d�finie");
		return false;
	}


	utility::string_t uri_stringt = utility::conversions::to_string_t(uri);

	if (pClient != NULL)
	{
		delete pClient; pClient = NULL;
	}

	try
	{
		pClient = new http_client(uri_stringt);
	}

	catch (std::exception e)
	{
		sprintf(msgerr, "GPSReceiver: �chec cr�ation client http\n (uri: %s)", uri.data());
		return false;
	}

	return true;
}

bool GPSReceiver::RequestGPSClient(web::json::value & reponse, char * msgerr)
{
	http_response response;

	try
	{
		// ordinary `get` request
		response = pClient->request(methods::GET).get();
		reponse = response.extract_json().get();
	}

	catch (std::exception e)
	{
		sprintf(msgerr, "GPSReceiver request: �chec r�ception de la r�ponse");
		return false;
	}

	return true;
}

bool GPSReceiver::GetKeyValueJson(web::json::value & data_in, std::string key, web::json::value& keyvalue, char *msgerr)
{
	utility::string_t Key = utility::conversions::to_string_t(key);
	if (data_in.has_field(Key))
	{
		keyvalue = data_in.at(Key);
	}
	else
	{
		sprintf(msgerr, "Echec: cl� json %s incorrecte", key.data());
		return false;
	}
	return true;
}

void GPSReceiver::ParseGPSAge(web::json::value dataAge, long double& agesecondes)
{
	std::string ret = utility::conversions::to_utf8string(dataAge.as_string());
	char *pnt = new char[ret.length() + 1];
	memcpy(pnt, ret.data(), ret.length()); *(pnt + ret.length()) = '\0';

	int indexSec, indexMin;

	for (int i = 0; i < ret.length(); i++) // recherche l'index de caract�res s�parant heure et minute
	{
		if (ret[i] == ':')
		{
			indexMin = i;
			break;
		}
	}

	for (int i = indexMin + 1; i < ret.length(); i++) // recherche l'index de caract�res s�parant minute et seconde
	{
		if (ret[i] == ':')
		{
			indexSec = i;
			break;
		}
	}

	*(pnt + indexMin) = '\0';
	*(pnt + indexSec) = '\0';

	double heure, min, sec;
	heure = atof(pnt); min = atof(pnt + indexMin + 1); sec = atof(pnt + indexSec + 1);

	agesecondes = heure * 3600 + min * 60 + sec; // l'�ge en secondes
}

void GPSReceiver::ParseGPSFix(web::json::value dataFix, int& rtkfix)
{
	std::string ret_rtk = utility::conversions::to_utf8string(dataFix.as_string());
	rtkfix = atof(ret_rtk.data());
}




// FONCTIONS MEMBRES PUBLIQUES


bool GPSReceiver::Connect(std::string uri, char* msgerr)
{
	if (!SetGPSClient(uri, msgerr))	return false;

	//test connexion
	GPSData GpsGauche, GpsDroite;
	Connecte = GetGPSRawData(GpsGauche, GpsDroite, msgerr);
	return Connecte;
}

void GPSReceiver::Disconnect()
{
	if (IsConnected())
	{
		delete pClient; pClient = NULL;
		Connecte = false;
	}
}

bool GPSReceiver::GetGPSRawData(GPSData& GpsGauche, GPSData& GpsDroite, char* msgerr)
{
	if (pClient == NULL)
	{
		sprintf(msgerr, "Echec: http client non �tabli");
		return false;
	}

	// http request 
	web::json::value  Reponse;
	if (!RequestGPSClient(Reponse, msgerr)) return false;

	web::json::value keyvalue;

	// R�cup�ration des valeurs, en commen�ant par l'�ge (info minimale fournie par le serveur)

	if (!GetKeyValueJson(Reponse, AGEG, keyvalue, msgerr))	return false;
	ParseGPSAge(keyvalue, GpsGauche.Age); // �ge gauche

	if (!GetKeyValueJson(Reponse, AGED, keyvalue, msgerr))	return false;
	ParseGPSAge(keyvalue, GpsDroite.Age); // �ge droite


	// Latitudes
if (GetKeyValueJson(Reponse, LATG, keyvalue, msgerr))
GpsGauche.LatDeg = keyvalue.as_double(); // latitude gauche
else
GpsGauche.LatDeg = -1;

if (GetKeyValueJson(Reponse, LATD, keyvalue, msgerr))
GpsDroite.LatDeg = keyvalue.as_double(); // latitude droite
else
GpsDroite.LatDeg = -1;


// Longitudes
if (GetKeyValueJson(Reponse, LONG, keyvalue, msgerr))
GpsGauche.LonDeg = keyvalue.as_double(); // longitude gauche
else
GpsGauche.LonDeg = -1;

if (GetKeyValueJson(Reponse, LOND, keyvalue, msgerr))
GpsDroite.LonDeg = keyvalue.as_double(); // longitude droite
else
GpsDroite.LonDeg = -1;


// Valeurs de fix
if (GetKeyValueJson(Reponse, RTKG, keyvalue, msgerr))
ParseGPSFix(keyvalue, GpsGauche.Fix); // rtk gauche
else
GpsGauche.Fix = -1;

if (GetKeyValueJson(Reponse, RTKD, keyvalue, msgerr))
ParseGPSFix(keyvalue, GpsDroite.Fix); // rtk droite
else
GpsDroite.Fix = -1;

return true;
}


bool GPSReceiver::GetGPSRawCoordinates(double &LatG, double &LonG, double &LatD, double &LonD, int &fixG, int &fixD, char* msgerr)
{

	GPSData GpsGauche, GpsDroite;

	if (!GetGPSRawData(GpsGauche, GpsDroite, msgerr))	return false;

	fixG = GpsGauche.Fix;
	fixD = GpsDroite.Fix;

	// v�rification de l'�ge des donn�es r�ceptionn�es
	/******** supprim� tant qu'on n'a pas r�solu le probl�me de mise � l'heure du rasberry
	if (abs(GpsGauche.Age) > CLIENT_TIMEOUT || abs(GpsDroite.Age) > CLIENT_TIMEOUT)
	{
		sprintf(msgerr, "Echec: donn�es GPS trop �g�es (GPS gauche: %4.2f  GPS droite: %4.2f)", GpsGauche.Age, GpsDroite.Age);
		return false;
	}******/


	// v�rification des fix rtk 
	if (!(GpsGauche.Fix == RTKFIX || GpsGauche.Fix == RTKFLOAT))
		//if (GpsGauche.Fix != RTKFIX)
	{
		sprintf(msgerr, "Echec: fix rtk insuffisant pour le GPS de gauche (fix: %d)", GpsGauche.Fix);
		return false;
	}

	if (!(GpsDroite.Fix == RTKFIX || GpsDroite.Fix == RTKFLOAT))
		//if (GpsDroite.Fix != RTKFIX)
	{
		sprintf(msgerr, "Echec: fix rtk insuffisant pour le GPS de droite (fix: %d)", GpsDroite.Fix);
		return false;
	}

	

	// retour des coordonn�es GPS brutes
	LatG = GpsGauche.LatDeg; LonG = GpsGauche.LonDeg;
	LatD = GpsDroite.LatDeg; LonD = GpsDroite.LonDeg;

	return true;
}

//double GetRobotPosition(TRepereLocal Repere, TPositionRobot &pos, char* msgerr, double *pdistanceNominale, double *tolerance);

double GPSReceiver::GetRobotPosition(TRepereLocal Repere, TPositionRobot &pos, char* msgerr, double *pdistanceNominale, double *tolerance)
{
	pos.CapDispo = false;

	double latG, lonG, latD, lonD;
	int fixG, fixD;
	double XGauche, YGauche, XDroite, YDroite;

	if (!GetGPSRawCoordinates(latG, lonG, latD, lonD, pos.fixGauche, pos.fixDroit, msgerr))
		return -1;

	Repere.WGS84VersMetres(latG, lonG, XGauche, YGauche);
	Repere.WGS84VersMetres(latD, lonD, XDroite, YDroite);

	if ((pos.fixGauche == 4) && (pos.fixDroit == 4))
	{
		pos.CapDispo = true;
		

		pos.x = (XGauche + XDroite) / 2;
		pos.y = (YGauche + YDroite) / 2;

		pos.CapDeg = atan2(YDroite - YGauche, XDroite - XGauche) *(180 / M_PI);

		//distance entre les deux antennes
		double d2 = (XGauche - XDroite)*(XGauche - XDroite) + (YGauche - YDroite)*(YGauche - YDroite);
		double d = (d2 > 1E-30 ? sqrt(d2) : 0);

		if (pdistanceNominale)		//demande de test
		{
			if (fabs(*pdistanceNominale - d) >= *tolerance)
			{
				sprintf(msgerr, "Distance mesur�e entre antenne incoh�rente (%.2f attendu, %.2f obtenu)", *pdistanceNominale, d);
				return -1;
			}

			return d;
		}
	} //if ((pos.fixGauche == 4) && (pos.fixDroit == 4))

	else if (pos.fixGauche == 4)
	{
		pos.x = XGauche;
		pos.y = YGauche;
		return -2;

	}

	else if (pos.fixDroit == 4)
	{
		pos.x = XDroite;
		pos.y = YDroite;
		return -2;
	}

	else return -1;

	
	
}



bool GPSReceiverSeptentrio::Connect(std::string adresse, int port, char* msgerr) {
	boost::system::error_code ec;
	socket->connect(tcp::endpoint(boost::asio::ip::address::from_string(adresse), port), ec);
	if (ec)
	{
		DBOUT("erreur connec:" << ec);
		Connecte = true;
		return false;
	}
	else {
		DBOUT("connexion ok");
		Connecte = true;
		return true;
	}
	
	
}

double NDEG2Deg(double Ndeg)
{
	int sgn = Ndeg < 0 ? -1 : 1;
	Ndeg *= sgn;	//valeur absolue
	int deg = floor(Ndeg / 100);
	double min = Ndeg - 100 * deg;
	return sgn * (deg + min / 60);
}

//#include "nmea\parse.h"
bool GPSReceiverSeptentrio::GetGPSData(GPSData& GpsSeptentrio, char* msgerr) {
	//boost::system::error_code error;
	//char mesgerr[1000];
	//std::vector<std::string> NMEAbufferList;
	//GetNMEABuffer(NMEAbufferList, mesgerr);
	////On peut alors parser les trames NMEA obtenues dans NMEAbufferList
	////et mettre � jour les champs de GpsSeptentrio
	//bool HRPFlag = false;
	//bool GGAFlag = false;
	//for (std::vector<std::string>::iterator it = NMEAbufferList.begin(); it != NMEAbufferList.end(); it++) {
	//	std::string cur_string = (*it);
	//	if (!cur_string.substr(3, 6).compare("SN,HRP")) {
	//		cur_string.replace(1, 5, "GP");
	//		//DBOUT("SN,HRP");
	//		//DBOUT("curString: " << cur_string);
	//		nmeaGPHRP pack;
	//		nmea_parse_GPHRP((cur_string).c_str(), 1000, &pack);
	//		//GpsSeptentrio.Cap = pack.heading;
	//		GpsSeptentrio.Cap = 90 - pack.heading; //r�cup�ration du cap
	//		//if (GpsSeptentrio.Cap >360) {
	//		//	GpsSeptentrio.Cap = GpsSeptentrio.Cap - 360;
	//		//}
	//		//GpsSeptentrio.Cap -= 180;
	//		HRPFlag = true;

	//	}
	//	else if (!cur_string.substr(3, 3).compare("GGA")) {
	//		cur_string.replace(1, 2, "GP");
	//		//DBOUT("GGA");
	//		//DBOUT("curString: " << cur_string);
	//		nmeaGPGGA pack;
	//		nmea_parse_GPGGA((cur_string).c_str(), 1000, &pack);
	//		nmeaINFO info;
	//		nmea_GPGGA2info(&pack, &info);
	//		GpsSeptentrio.Fix = info.sig; //r�cup�ration du fix
	//		GpsSeptentrio.LatDeg = NDEG2Deg(info.lat);
	//		GpsSeptentrio.LonDeg = NDEG2Deg(info.lon);
	//		GGAFlag = true;
	//	}
	//}

	//if (!HRPFlag || GGAFlag) {
	//	msgerr="champs pas tous actualises";
	//}
	return true;
}


//start_delim: $
//stop_delim: \n
std::string getStrNMEA(std::string &s,
	const std::string &start_delim,
	const std::string &stop_delim)
{
	//On cherche le premier $ dans la chaine de caract�re
	unsigned first_delim_pos = s.find_first_of(start_delim);
	//DBOUT("first delim: " << first_delim_pos);
	//On cherche le premier \n apr�s le $ trouv�
	unsigned last_delim_pos = s.find_first_of(stop_delim, first_delim_pos);
	//DBOUT("last delim: " << last_delim_pos);
	std::string retString;
	//On renvoie une chaine nulle
	if (first_delim_pos == -1 || last_delim_pos==-1) {
		//On se retrouve avec une chaine partielle
		//Il n'y a plus rien d'int�ressant � r�cup�rer
		retString = "";
		s = "";
	}
	else {
		retString = s.substr(first_delim_pos,
			last_delim_pos - first_delim_pos);
		//On efface la partie correspondante
		s.erase(0, last_delim_pos);
	}
	return retString;
}


//Note: une trame NMEA ne fait jamais plus de 80 char, auquel on ajoute les deux caract�res de terminaison
//remplir un buffer de 240 char assure donc que les deux messages envoy�s p�riodiquement soient bien re�us au moins une foi
bool GPSReceiverSeptentrio::GetNMEABuffer(std::vector<std::string> &NMEAStringList, char* msgerr) {
	boost::system::error_code error;
	//On commence par vider le buffer pour �viter sa corruption
	boost::array<char, 240> recv_buf;
	//on le remplit avec les donn�es disponibles
	//DBOUT("receive");
	int octetsLus = socket->receive(boost::asio::buffer(recv_buf));
	//DBOUT("octets lus stream tcp:" << octetsLus);
	std::string rawBuffer = recv_buf.data();
	//DBOUT("contenu lu:" << buffer);
	//on d�coupe le message de fa�on � isoler les trames
	//On boucle jusqu'� arriver � la fin du fichier
	std::string curNMEAString;
	//DBOUT("entree boucle");
	while (!rawBuffer.empty()) {
		//DBOUT("boucle");
		//DBOUT("buffer: " << rawBuffer);
		curNMEAString = getStrNMEA(rawBuffer, "$", "\n");
		if (!curNMEAString.empty()) {
			//DBOUT("ajout de " << curNMEAString);
			NMEAStringList.push_back(curNMEAString);
		}
	}
	//DBOUT("Nombre de trames " << NMEAStringList.size());
	return true;
}

void GPSReceiverSeptentrio::Disconnect() {
	socket->close();
}