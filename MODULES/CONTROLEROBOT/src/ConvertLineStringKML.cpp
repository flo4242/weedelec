
#include "drvtraiti.h"
#include "ConvertLineStringKML.h"

bool ConvertLineStringToWayPoint(char *NfKML, char *NfSortiKML, _list<Waypoint> &ListeW, char *msg1000)
{
	if (!ImporteKML(NfKML, ListeW, msg1000)) return false;
	
	TNomFic NomDoc;
	strcpy(NomDoc, NomSansPath(NfKML));
	NomSansExt(NomDoc);

	if (!ExportKML(NfSortiKML, NomDoc, ListeW, msg1000)) return false;

	return true;
}