//Fonctions pour le calcul d'un chemin optimal sur une liste de mauvaises herbes dans l'espace du bras
#include <boost\math\special_functions\detail\round_fwd.hpp>

#include "cheminWeeds.h"

using namespace boost;
#define DBOUT( s )            \
{                             \
   std::ostringstream os_;    \
   os_ << s;                   \
   OutputDebugString( os_.str().c_str() );  \
}

//FONCTIONS BOOST
//===================
template<typename PointType>
struct cmpPnt
{
	bool operator()(const boost::simple_point<PointType>& l,
		const boost::simple_point<PointType>& r) const
	{
		return (l.x > r.x);
	}
};

//ajout des connexions dans le graph
template<typename VertexListGraph, typename PointContainer,
	typename WeightMap, typename VertexIndexMap>
	void connectAllEuclidean(VertexListGraph& g,
		const PointContainer& points,
		WeightMap wmap,            // Property maps passed by value
		VertexIndexMap vmap,       // Property maps passed by value
		int /*sz*/)
{
	using namespace boost;
	using namespace std;
	typedef typename graph_traits<VertexListGraph>::edge_descriptor Edge;
	typedef typename graph_traits<VertexListGraph>::vertex_iterator VItr;

	Edge e;
	bool inserted;

	pair<VItr, VItr> verts(vertices(g));
	for (VItr src(verts.first); src != verts.second; src++)
	{
		for (VItr dest(src); dest != verts.second; dest++)
		{
			if (dest != src)
			{
				double weight(sqrt(pow(
					static_cast<double>(points[vmap[*src]].x -
						points[vmap[*dest]].x), 2.0) +
					pow(static_cast<double>(points[vmap[*dest]].y -
						points[vmap[*src]].y), 2.0)));

				boost::tie(e, inserted) = add_edge(*src, *dest, g);

				wmap[e] = weight;
			}

		}

	}
}

template <typename PositionVec>
void checkAdjList(PositionVec v)
{
	using namespace std;
	using namespace boost;

	Container c;
	EdgeWeightMap w_map;
	VertexIndexMap v_map;
	VPropertyMap v_pmap(v_map);
	EWeightPropertyMap w_pmap(w_map);

	Graph g(v.size());

	//create vertex index map
	VItr vi, ve;
	int idx(0);
	for (boost::tie(vi, ve) = vertices(g); vi != ve; ++vi)
	{
		Vertex v(*vi);
		v_pmap[v] = idx;
		idx++;
	}

	connectAllEuclidean(g, v, w_pmap,
		v_pmap, v.size());

	metric_tsp_approx_from_vertex(g,
		*vertices(g).first,
		w_pmap,
		v_pmap,
		tsp_tour_visitor<back_insert_iterator<Container > >
		(back_inserter(c)));

	cout << "adj_list" << endl;
	for (Container::iterator itr = c.begin(); itr != c.end(); ++itr) {
		cout << v_map[*itr] << " ";
	}
	cout << endl << endl;

	c.clear();
}
//===================


//FONCTIONS PERSO, SURCHARGEES POUR LE CAS LISTE DE shared_PTR
//===================


//Fonctions surcharg�es selon l'utilisation de pointeurs ou non
_list<TWeedBras*> cheminWeedsAtteignables(_list<TWeedBras*>& listWeeds) {
	TWeedBras *zero = new TWeedBras(POINTFLT(0, 0));
	listWeeds.push_front(zero);
	//converstion en vector de pointeurs
	std::vector<TWeedBras*> vectorWeeds = listToVector(listWeeds);
	//construction du graphe
	Graph g = buildBoostGraphe(vectorWeeds);
	//Chemin le plus court entre les points en partant de (0,0) (toujours � l'indice 0)
	std::vector<TWeedBras*> newVectorWeeds = boostGetShortestPath(g, vectorWeeds);
	//Retour en liste
	_list<TWeedBras*> listWeedsNew = VectorToList(newVectorWeeds);
	delete zero;
	return listWeedsNew;
}
_list<TWeedBras> cheminWeedsAtteignables(_list<TWeedBras>& listWeeds) {
	listWeeds.push_back(TWeedBras(POINTFLT(0, 0)));
	//converstion en vector
	std::vector<TWeedBras> vectorWeeds = listToVector(listWeeds);
	//construction du graphe
	Graph g=buildBoostGraphe(vectorWeeds);
	//Chemin le plus court entre les points en partant de (0,0) (toujours � l'indice 0)
	std::vector<TWeedBras> newVectorWeeds =boostGetShortestPath(g, vectorWeeds);
	//Retour en liste
	_list<TWeedBras> listWeedsNew=VectorToList(newVectorWeeds);
	return listWeedsNew;
}

Graph buildBoostGraphe(std::vector<TWeedBras*> vectorWeeds) {

	std::string line;
	PositionVec position_vec;
	//On boucle sur les points
	int n(0);
	for (std::vector<TWeedBras*>::iterator it = vectorWeeds.begin(); it != vectorWeeds.end(); ++it) {
		simple_point<double> vertex;
		//Double d�r�f�rencement pour obtenir les valeurs de position
		vertex.x = (**it).Position.x;
		vertex.y = (**it).Position.y;
		position_vec.push_back(vertex);
		n++;
	}
	Graph g(position_vec.size());
	WeightMap weight_map(get(edge_weight, g));
	VertexMap v_map = get(vertex_index, g);

	connectAllEuclidean(g, position_vec, weight_map, v_map, n);
	return g;
}
Graph buildBoostGraphe(std::vector<TWeedBras> vectorWeeds) {

	std::string line;
	PositionVec position_vec;
	//On boucle sur les points
	int n(0);
	for (std::vector<TWeedBras>::iterator it = vectorWeeds.begin(); it != vectorWeeds.end(); ++it) {
		simple_point<double> vertex;
		vertex.x = (*it).Position.x;
		vertex.y= (*it).Position.y;
		position_vec.push_back(vertex);
		n++;
	}
	Graph g(position_vec.size());
	WeightMap weight_map(get(edge_weight, g));
	VertexMap v_map = get(vertex_index, g);

	connectAllEuclidean(g, position_vec, weight_map, v_map, n);
	return g;
}


std::vector<TWeedBras*> boostGetShortestPath(Graph g, std::vector<TWeedBras*> VectorWeeds) {

	std::vector<TWeedBras*> newVectorWeeds;
	std::vector< Vertex > c;
	metric_tsp_approx_tour(g, std::back_inserter(c));
	//on obtient un vector des indices des vertices dans l'ordre
	//on peut alors cr�er un nouveau vector des pointeurs dans le bon ordre
	for (std::vector<Vertex>::iterator itr = c.begin(); itr != c.end(); ++itr)
	{
		if ((*itr) > 0) { //On ne garde pas la position (0,0)
			DBOUT((*itr) << "ptrver(" << (*VectorWeeds[(*itr)]).Position.x << "," << (*VectorWeeds[(*itr)]).Position.y << ") \n");
			newVectorWeeds.push_back(VectorWeeds[(*itr)]);  //On lui rajoute le pointeur
		}

	}
	return newVectorWeeds;
}
std::vector<TWeedBras> boostGetShortestPath(Graph g, std::vector<TWeedBras> VectorWeeds) {
	
	std::vector<TWeedBras> newVectorWeeds;
	std::vector< Vertex > c;
	metric_tsp_approx_tour(g, std::back_inserter(c));
	//on obtient un vector des indices des vertices dans l'ordre
	//on peut alors cr�er un nouveau vector dans le bon ordre
	for (std::vector<Vertex>::iterator itr = c.begin(); itr != c.end(); ++itr)
	{
		if ((*itr) > 0) { //On ne garde pas la position (0,0)
			DBOUT((*itr) << "(" << VectorWeeds[(*itr)].Position.x << "," << VectorWeeds[(*itr)].Position.y << ") \n");
			newVectorWeeds.push_back(VectorWeeds[(*itr)]);
		}

	}
	return newVectorWeeds;
}

//===================