#include "routinesSuperviseur.h"
#include <algorithm>
#include <list>
#include <vector>
#include <iostream>
#include <sstream>
#include <tuple>
#include "ExportModuleControleRobot.h"
#include "DBOUT.h"


template <class myType>
void debugWeedList(_list<myType> &ListeWeeds) {
	if (ListeWeeds.nbElem > 0) {
		BalayeListe(ListeWeeds) {
			myType *ptr = ListeWeeds.current();
			DBOUT("\n x=" << ptr->Position.x << " | y=" << ptr->Position.y);
		}
	}
	return;
}


//Fonction du superviseur permettant de faire le passage pour chaque groupe de weed du rep�re robot local au rep�re robot actuel
//Une homographie peut �tre fournie afin de r�aliser une transformation plus pr�cise pour un timestamp donn� (=un cycle de la pile)
bool TransfoPileWeeds(char msgerr[1000], _list<TGroupeWeeds> &PileGW, const TPositionRobot PosAcq, TRepereLocal &pRepere) {
	TParamRobot ParamR; 
	if (!TModulePlateforme::ChargeParametres(ParamR))
	{
		sprintf("\n R�cup�ration param�tres: %s", msgerr); return false;
	}

	//On boucle sur les �l�ments de la pile
	for (PileGW.begin(); !PileGW.end(); PileGW++) {
		TGroupeWeeds *curGroupWeedptr = PileGW.current();
		//On nettoie d'abord la liste pour effacer toute trace d'une transformation pr�c�dente
		curGroupWeedptr->ListeWeedsRobotTrans.clear();
		curGroupWeedptr->ListeWeedsBrasTrans.clear();
		if (curGroupWeedptr->ListeWeedsRobot.nbElem > 0)
		{
			switch (curGroupWeedptr->methodeTransfoWeeds) {
				case metTrans::HOMOGRAPHIE:
					//TRANSFORMATION PAR HOMOGRAPHIE
					DBOUT("==Transformation homographie== \n");
					DBOUT("Rep�re initial \n");
					H33 Htest = curGroupWeedptr->HDeplacement;
					debugWeedList(curGroupWeedptr->ListeWeedsRobot);
					DBOUT(Htest.m11 << "|" << Htest.m12 << "|" << Htest.m13 << "\n");
					DBOUT(Htest.m21 << "|" << Htest.m22 << "|" << Htest.m23 << "\n");
					DBOUT(Htest.m31 << "|" << Htest.m32 << "|" << Htest.m33 << "\n");

					if (!curGroupWeedptr->ToRepereRobotActuelHomographie(Htest, msgerr)) {
						sprintf("\n Transfo homographie: %s", msgerr); return false;
					}
					DBOUT("Rep�re transform�: \n");
					debugWeedList(curGroupWeedptr->ListeWeedsRobotTrans);
					DBOUT("==================== \n");
				case metTrans::GPS:
					DBOUT("==Transformation GPS== \n");
					DBOUT("Rep�re initial:");
					debugWeedList(curGroupWeedptr->ListeWeedsRobot);
					//TRANSFORMATION GPS
					if (!curGroupWeedptr->ToRepereRobotActuelGPS(PosAcq, &pRepere, msgerr)) //permet de remplir GW->ListeWeedsRobotTrans
					{
						sprintf("\nTransfo GPS: %s", msgerr); return false;
					}
					DBOUT("Rep�re transform�: \n");
					debugWeedList(curGroupWeedptr->ListeWeedsRobotTrans);
					DBOUT("==================== \n");

			}
			//On fait enfin simplement le passage du rep�re du robot au rep�re du bras
			DBOUT("==Transformation BRAS== \n");
			DBOUT("Rep�re initial: \n");
			debugWeedList(curGroupWeedptr->ListeWeedsRobotTrans);
			if (!curGroupWeedptr->ToRepereBrasActuel(msgerr, ParamR.TranslationDeltaX, ParamR.TranslationDeltaY)) {
				sprintf("\n Transfo robot bras: %s", msgerr); return false;
			}
			DBOUT("Rep�re transform� bras: \n");
			debugWeedList(curGroupWeedptr->ListeWeedsBrasTrans);
		}
	}
	return true;
}



bool FiltreWeedsDupliquees(TGroupeWeeds &lastGW,_list<TGroupeWeeds> &PileGW, TRepereLocal *pRepere, const TPositionRobot PosAcq, const int maxCyclesPile, const float tolDist) {
	char msgerr[1000];
	//On fait d'abord une sauvegarde de la liste des weeds (sera supprim�e en sortie de fonction)
	DBOUT("sauvegarde liste weeds cycle actuel\n");
	_list<TWeedRobot> copyListWeed = lastGW.ListeWeedsRobot;
	lastGW.ListeWeedsRobot.clear(); //puis on r�initialise la liste
	//On boucle sur les nouvelles weeds du cycle pour les v�rifier
	for (copyListWeed.begin(); !copyListWeed.end(); copyListWeed++) {
		DBOUT("\nWeed actuelle:\n");
		bool flagRemove = false; //par d�faut elle sera gard�e
		TWeedRobot *curWeedPtr = copyListWeed.current();
		DBOUT("x=" << curWeedPtr->Position.x << "  y="<< curWeedPtr->Position.y <<"\n");
		DBOUT("============\n");
		int iter;
		DBOUT("Boucles cycles pass�s:\n");
		for (iter = 0, PileGW.begin(); !PileGW.end() && iter < maxCyclesPile; PileGW++, iter++) {
			TGroupeWeeds curPileGW = *PileGW.current();
			//On doit d'abord v�rifier si ListeWeedsRobotTrans a �t� d�fini par une transformation (GPS ou homographie)
			if (curPileGW.methodeTransfoWeeds != NODEF) {
				for (curPileGW.ListeWeedsRobotTrans.begin(); !curPileGW.ListeWeedsRobotTrans.end(); curPileGW.ListeWeedsRobotTrans++) {
					TWeedRobot *curWeedOld = curPileGW.ListeWeedsRobotTrans.current();
					DBOUT(" --- x=" << curWeedOld->Position.x << "  y=" << curWeedOld->Position.y << "\n");
					DBOUT("Distance:" << curWeedPtr->getDistance(*curWeedOld) << "\n");
					if (curWeedPtr->getDistance(*curWeedOld) < tolDist / 100) { //Car toldist est en cm
						flagRemove = true; //weed � ne pas remettre dans la liste
					}
				}

			}
		}
		//on peut alors alimenter la liste en sortie
		if (!flagRemove) {
			DBOUT("Weed gard�e:\n");
			lastGW.ListeWeedsRobot.push_back(*curWeedPtr); 
		}
	}
	return true;
}


//Fonction template surcharg�e permettant de supprimer des �l�ments d'une list
//version avec un vecteur de bool�en (�l�ment enlev� si true)
template <class myType>
bool RemoveFromList(std::list<myType> &a, const std::vector<bool> &vectorFiltre) {
	//On doit v�rifier que la list et le vector ont la m�me taille
	if (a.size() != vectorFiltre.size()) {
		return false;
	}
	//Boucle principale
	std::list<myType>::iterator it = a.begin();
	std::vector<bool>::iterator itVec = vectorFiltre.begin();
	while (it != a.end()) {
		if ((*itVec)) {
			it = a.erase(it); //permet de mettre � jour it au moment o� on efface un �l�ment
		}
		else
			it++;
		itVec++; //on pase de m�me � la cellule suivante du vecteur
	}
	return true;
}

//surcharge pour un vecteur d'indices
template <class myType>
bool RemoveFromList(std::list<myType> &a, const std::vector<size_t> &vectorFiltre) {
	//on doit v�rifier que les valeurs du vector sont entre 0 et la taille de la liste
	int vectorMax = *std::max_element(vectorFiltre.begin(), vectorFiltre.end());
	int vectorMin = *std::min_element(vectorFiltre.begin(), vectorFiltre.end());
	if ((vectorMax> a.size() ) || (vectorMin<0)) {
		return false;
	}
	//Boucle principale
	std::list<myType>::iterator it = a.begin();
	int pos = 0;
	while (it != a.end()) {
		//recherche d'un indice �gal � la position actuelle
		for (std::vector<bool>::iterator itVec = vectorFiltre.begin(); itVec != vectorFiltre.end(); itVec++) {
			if (pos == (*itVec)) {
				it = a.erase(it); //permet de mettre � jour it au moment o� on efface un �l�ment
				break;
			}
			else {
				it++;
			}
		}
		pos++;
	}

	return true;
}