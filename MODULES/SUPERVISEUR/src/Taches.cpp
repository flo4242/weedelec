#include "Taches.h" 
#include "TaskUtil.h"
#include <boost/timer/timer.hpp>
#include "DBOUT.h"
#define TOUCHE_ARRET ' '  //barre d'espace, pour t�che test clavier


//Lambda g�n�rique pour la mesure du temps d'ex�cution des taches
auto mesureTempsFonction =[](auto&& func, auto&&... params) {
	const auto& debut = high_resolution_clock::now();
	std::forward<decltype(func)>(func)(std::forward<decltype(params)>(params)...);
	const auto& fin = high_resolution_clock::now();
	return fin - debut;
};


/***************  DEFINITION DES TACHES ****************/

//toutes les t�ches ont pour retour TRetourTask

//******** t�ches de monitoring *********/

//cr�e une t�che qui ne se termine que si le clavier a �t� appuy�
//utilis�e en parall�le de toutes les autres t�ches
task<TRetourTask> CreateTaskTestClavier(unsigned int period, int *pcntTimer)
{
	// �tabli si action clavier
	task_completion_event<void> tce;
	 

	// Create a call object for timer events.
	// cr�� par new car doit survivre � la construction de la t�che 
	auto callback = new call<bool>([tce, pcntTimer](bool)
	{
		if(pcntTimer)	(*pcntTimer)++;
		if( (GetAsyncKeyState(TOUCHE_ARRET) & 0x8000)!=0)	tce.set();
	});


	// Create a repeating timer.
	
	timer<bool> *Timer = new timer<bool>(period, NULL, callback, true);
	// cr�� par new car doit survivre � la construction de la t�che 
	//bool�en arbitraitrement car doit forc�ment avoir un type non void (2�me argument: T&)
	
	Timer->start();

	// Create a task that completes after the completion event is set.
	// Create a task that completes after the completion event is set.
	task<void> event_set(tce);

	// Create a continuation task that cleans up resources and
	// and return that continuation task.
	return event_set.then([callback, Timer]()
	{
		delete callback;
		delete Timer;
		TRetourTask Ret;
		Ret.State = TRetourState::ExternalStop;
		Ret.InfoRetour = "Arr�t utilisateur";
		return Ret;
	});
}

//cr�e une t�che d'affichage de l'�tat des t�ches courantes
task<TRetourTask> CreateTaskAffiche(TAffichageTaches *pAffiche, unsigned int period, cancellation_token &ctExtern)
{
	// �tabli si action clavier
	task_completion_event<void> tce;

	// Create a call object for timer events.
	// cr�� par new car doit survivre � la construction de la t�che 
	auto callback = new call<bool>([pAffiche, tce, ctExtern](bool)
	{
		if (ctExtern.is_canceled())	tce.set();
		else pAffiche->AfficheTaches();
	});


	// Create a repeating timer.

	timer<bool> *Timer = new timer<bool>(period, NULL, callback, true);
	// cr�� par new car doit survivre � la construction de la t�che 
	//bool�en arbitraitrement car doit forc�ment avoir un type non void (2�me argument: T&)

	Timer->start();

	// Create a task that completes after the completion event is set (sur token ext�rieur)
	task<void> event_set(tce);

	// Create a continuation task that cleans up resources and
	// and return that continuation task.
	return event_set.then([callback, Timer]()
	{
		delete callback;
		delete Timer;
		TRetourTask Ret;
		Ret.State = TRetourState::Completed;
		return Ret;
	});
}


bool StateOK(TRetourState state, bool TimeOutAccepte)
{
	if(TimeOutAccepte)	return (state == TRetourState::Completed || state == TRetourState::TimeOut);
	else	return (state == TRetourState::Completed);
}

//******** t�che tempo d'usage g�n�ral *********/
task<TRetourTask> CreateTacheTempo(TAffichageTaches *pAffiche, int delaiMs, char *NomTache)
{
	char infoRet[100];
	sprintf(infoRet, "Fin tempo %", NomTache);
	TRetourTask ret; ret.State = TRetourState::Completed; ret.InfoRetour = infoRet;
	task<TRetourTask> t = CreateDelayTask(2000).then([ret]() {return ret; });
	pAffiche->AjouteTache(t, NomTache);
		return t;
}



//******** t�ches Sony *********/

task<TRetourTask> CreateAcquisitionSony(TAffichageTaches *pAffiche)
{
	//t�che d'acquisition

	task<TRetourTask> acq = create_task([]()
	{
		TRetourAcqSony Ret;
		Ret.nomTache = "AcqSony";
		TModuleSony::AcquisitionSony(Ret); //lance et ex�cute la proc�dure d'acquisition
		return (TRetourTask)Ret;
	});

	pAffiche->AjouteTache(acq, "Acquisition Sony");
	return acq;

}

task<TRetourTask> CreateDownloadSony(TAffichageTaches *pAffiche, string nomImage, bool AvecGeo, double LatDeg, double LonDeg)
{
	task<TRetourTask>  downl = create_task([nomImage]()
	{
		TRetourDownloadSony Ret;
		Ret.nomTache = "DownSony";
		TModuleSony::DownloadSony(Ret, nomImage);
		//// A FAIRE if(AvecGeo) TModuleSony::GeoTagSony(nomImage);
		return (TRetourTask)Ret;
	});

	pAffiche->AjouteTache(downl, "DownloadAcquisition Sony");

	return downl;

}


task<TRetourTask> CreateDetectionSony(TAffichageTaches *pAffiche, string UrlReduite, DRVTRAITIM *pD, _list<TWeedRobot> *pListeWeeds)
{
	task<TRetourTask>  detect = create_task([UrlReduite, pD, pListeWeeds]()
	{
		TRetourDetectionSony Ret;
		Ret.nomTache = "DetecSony";
		TModuleSony::DetectionPastillesSony(Ret, UrlReduite, pD);
		*pListeWeeds = Ret.ListeWeeds;
		return (TRetourTask)Ret;
	});

	pAffiche->AjouteTache(detect, "Detection pastilles Sony");

	return detect;

}


#ifdef DOWNLOAD

//t�che enchainant acquisition (pas d'avancement) + download (avancement possible)
//l'event en argument permet d'informer de la fin de l'acquisition (� faire robot arr�t�)
task<TRetourTask> CreateTaskTotaleSony(TAffichageTaches *pAffiche, task_completion_event<TRetourTask> finacquisition, 
						bool AvecDownload, bool TimeOutAccepte, bool AvecGeo, double LatDeg, double LonDeg)
{
	
	task<TRetourTask> acq = CreateAcquisitionSony(pAffiche);

	return acq.then([acq, pAffiche, finacquisition, AvecDownload, TimeOutAccepte, AvecGeo, LatDeg, LonDeg](TRetourTask retacq)
	{
		bool timeoutOK = StateOK(retacq.State, TimeOutAccepte) && (retacq.State == TRetourState::TimeOut);
		TRetourTask temp;
		task<TRetourTask> acqtemp;
		
		if (timeoutOK)
		{
				
			temp.State = TRetourState::Completed;
			temp.InfoRetour = "Timeout OK";
			finacquisition.set(temp); //pour acceptation en fin de cycle 1

			acqtemp = create_task([temp]() {}).then([temp]() {return temp; });

		}
			
		else finacquisition.set(retacq);

		if (!AvecDownload) return acq;


		if (retacq.State != TRetourState::Completed)
			return timeoutOK ? acqtemp : acq; //inutile d'aller plus loin

		return CreateDownloadSony(pAffiche, retacq.InfoRetour, AvecGeo, LatDeg, LonDeg);

	});
	


}
#endif

//t�che enchainant acquisition (pas d'avancement) + d�tection pastilles
//l'event en argument permet d'informer de la fin de l'acquisition (� faire robot arr�t�)
task<TRetourTask> CreateTaskTotaleSony(TAffichageTaches *pAffiche, task_completion_event<TRetourTask> finacquisition,
	DRVTRAITIM *pD, _list<TWeedRobot> *pListeWeeds)
	
{

	task<TRetourTask> acq = CreateAcquisitionSony(pAffiche);

	return acq.then([acq, pAffiche, finacquisition, pD, pListeWeeds](TRetourTask retacq)
	{
		bool timeoutOK = StateOK(retacq.State, false) && (retacq.State == TRetourState::TimeOut);
		TRetourTask temp;
		task<TRetourTask> acqtemp;

		if (timeoutOK)
		{

			temp.State = TRetourState::Completed;
			temp.InfoRetour = "Timeout OK";
			finacquisition.set(temp); //pour acceptation en fin de cycle 1

			acqtemp = create_task([temp]() {}).then([temp]() {return temp; });

		}

		else finacquisition.set(retacq);

		if (retacq.State != TRetourState::Completed)
			return timeoutOK ? acqtemp : acq; //inutile d'aller plus loin


		return CreateDetectionSony(pAffiche, retacq.InfoRetour, pD, pListeWeeds);

	});



}

//******** t�ches PlantNet *********/

task<TRetourTask> CreateAcquisitionPlantNet(TAffichageTaches *pAffiche, TGroupeWeeds *groupePlantNet)
{

	task<TRetourTask> acq = create_task([groupePlantNet]()
	{
		TRetourDetectionPlantNet Ret;
		Ret.nomTache = "DetecPlantNet";
		TModulePlantNet::LaunchAcquisition(Ret);
		groupePlantNet->ListeWeedsRobot = Ret.groupeWeeds;
		DBOUT("tache termin�e plantnet");
		DBOUT(groupePlantNet->TimeAcq);
		DBOUT(groupePlantNet->ListeWeedsRobot.nbElem);
		return (TRetourTask)Ret;
	});
	pAffiche->AjouteTache(acq, "Acquisition PlantNet");
	return acq;
}


//******** t�ches Rikola *********/

task<TRetourTask> CreateAcquisitionRikola(TAffichageTaches *pAffiche, TModuleRikola *module, bool AvecGeo, double LatDeg, double LonDeg)
{
	//t�che d'acquisition

	task<TRetourTask> acq = create_task([module, AvecGeo, LatDeg, LonDeg]()
	{
		TRetourAcqRikola Ret;
		module->AcquisitionRikola(Ret, AvecGeo, LatDeg, LonDeg); //lance et ex�cute la proc�dure d'acquisition

		return (TRetourTask)Ret;
	});

	pAffiche->AjouteTache(acq, "Acquisition Rikola");
	return acq;

}

task<TRetourTask> CreateDetectionRikola(TAffichageTaches *pAffiche, string NfImageRaw, _list<TWeedRobot> *pListeWeeds)
{

	task<TRetourTask>  calib = create_task([NfImageRaw]()
	{
		TRetourCalibRikola RetC;
		TModuleRikola::CalibRikola(RetC, NfImageRaw);
		return TRetourTask(RetC);
	});

	task<TRetourTask> trait = calib.then([pListeWeeds](TRetourTask retC) //lancement d�tection
	{

		if (retC.State != TRetourState::Completed) return TRetourTask(retC); //inutile d'aller plus loin

		
		TRetourDetectionRikola Ret;
		TModuleRikola::DetectionRikola50(Ret, retC.InfoRetour);

		if (Ret.State == TRetourState::Completed)
		{
			*pListeWeeds = Ret.ListeWeeds;
		}
		return (TRetourTask)Ret;
	});

	pAffiche->AjouteTache(trait, "D�tection Rikola");
	return trait;
}




//t�che enchainant acquisition (pas d'avancement) + traitement (avancement possible)
//l'event en argument permet d'informer de la fin de l'acquisition (� faire robot arr�t�)
task<TRetourTask> CreateTaskTotaleRikola(TAffichageTaches *pAffiche, TModuleRikola *module, task_completion_event<TRetourTask> finacquisition,
					bool avecDetection, bool TimeOutAccepte, _list<TWeedRobot> *pListeWeeds, bool AvecGeo, double LatDeg, double LonDeg)
{
	task<TRetourTask> acq = CreateAcquisitionRikola(pAffiche, module, AvecGeo, LatDeg, LonDeg);
	return acq.then([acq, pAffiche, finacquisition, avecDetection, TimeOutAccepte, pListeWeeds](TRetourTask retacq)
	{
		finacquisition.set(retacq);
		if (!StateOK(retacq.State, TimeOutAccepte))	return acq; //inutile d'aller plus loin
		if (!avecDetection)	return acq; //inutile d'aller plus loin
		return CreateDetectionRikola(pAffiche, retacq.InfoRetour, pListeWeeds);
	});
}


//******** t�ches Raspberry Homographie *********/
//On r�alise d'abord l'acquisition cam�ra avant puis l'acquisition cam�ra arri�re
//Ces acquisitions sont indispensables avant de d�clencher le weeding si on a choisi le recalage par les raspberry

task<TRetourTask> CreateTaskTotaleHomographie(TAffichageTaches *paffiche,  task_completion_event<TRetourTask> finacquisition, time_t timestamp_avant, time_t timestamp_arriere, _list<H33> *listH, time_t *time_retour)
{
	//cr�ation de la t�che acquisition avant, elle d�clenche l'appareil photo raspberry avant seulement
	task<TRetourTask> acqavant = createAcquisitionRaspberryAvant(paffiche, timestamp_avant);
	return acqavant.then([acqavant, paffiche, finacquisition, timestamp_arriere, listH, time_retour](TRetourTask retacq)
	{
		finacquisition.set(retacq);
		//cr�ation de la t�che arri�re, elle d�clenche l'appareil arri�re et renvoie une homographie
		return createAcquisitionRaspberryArriere(paffiche, timestamp_arriere, listH, time_retour);
	});
}


////Tache d'acquisition � l'avant seulement
task<TRetourTask> createAcquisitionRaspberryAvant(TAffichageTaches *pAffiche, time_t timestamp_avant)
{
	task<TRetourTask> acq = create_task([timestamp_avant]()
	{
		char msg100[1000];
		TRetourAcquisitionAvant Ret;
		Ret.nomTache = "RaspAv";
		TModuleRecalageRasberry::AcqImageAvantTache(timestamp_avant, msg100,Ret); //lance et ex�cute la proc�dure d'acquisition
		return (TRetourTask)Ret;
	});

	pAffiche->AjouteTache(acq, "Acquisition raspberry avant");
	return acq;
}


////Tache d'acquisition � l'arri�re et de calcul de l'homographie (d�l�gu� au raspberry)
task<TRetourTask> createAcquisitionRaspberryArriere(TAffichageTaches *pAffiche, time_t timestamp_arriere, _list<H33> *listH, time_t *time_retour)
{
	task<TRetourTask> acq = create_task([timestamp_arriere, time_retour, listH]()
	{
		TRetourAcquisitionArriere Ret;
		Ret.nomTache = "RaspArr";
		char msg100[1000];
		TModuleRecalageRasberry::GetHomographieTache(timestamp_arriere,msg100, Ret); //lance et ex�cute la proc�dure d'acquisition
		*listH = Ret.listH; //on r�cup�re la matrice d'homographie et on met � jour la variable captur�e dans la lambda
		*time_retour = Ret.time_retour;
		return (TRetourTask)Ret; 
	});

	pAffiche->AjouteTache(acq, "Acquisition raspberry arriere + homographie");
	return acq;
}


//******** t�ches Plateforme *********/

task_completion_event<TRetourPlateforme> *tceStepG = NULL;

void CallbackStep(TRetourPlateforme ret)
{
	tceStepG->set(ret);
}


task<TRetourTask> CreateStepManuel(TAffichageTaches *pAffiche, DRVTRAITIM *pD)
{
	if (tceStepG != NULL) delete tceStepG;

	tceStepG = new task_completion_event<TRetourPlateforme>;

	//lancement du mouvement
	TModulePlateforme::AvancementManuel(pD, CallbackStep);

	task<TRetourPlateforme> tasktce(*tceStepG);

	task<TRetourTask> tstep = tasktce.then([](TRetourPlateforme ret)
	{
		ret.nomTache = "StepManuel";
		return (TRetourTask)ret;
	});

	pAffiche->AjouteTache(tstep, "Avancement manuel");

	return tstep;
}


task<TRetourTask> CreateStepAuto(TAffichageTaches *pAffiche, Trajectoire *ptrajectoire, double step, bool *pCropFinished)
{
	if (tceStepG != NULL) delete tceStepG;

	tceStepG = new task_completion_event<TRetourPlateforme>;

	TModulePlateforme::GoToNextStep(ptrajectoire, step, CallbackStep);

	task<TRetourPlateforme> tasktce(*tceStepG);

	task<TRetourTask> tstep = tasktce.then([pCropFinished](TRetourPlateforme ret)
	{
		ret.nomTache = "StepAuto";
		*pCropFinished = ret.CropRowFinished;
		return (TRetourTask)ret;
	});

	pAffiche->AjouteTache(tstep, "Avancement auto");
	return tstep;
}



task<TRetourTask> CreateWeeding(TAffichageTaches *pAffiche, _list<TGroupeWeeds> *PileGroupeWeeds, bool AvecHauteTension, bool AvecRecalageManuelBras)
{

	task<TRetourTask> tWeed = create_task([PileGroupeWeeds, AvecHauteTension,AvecRecalageManuelBras]
	{
		TRetourPlateforme Ret;
		Ret.nomTache = "Weeding";
		TModulePlateforme::WeedingSuperviseur(PileGroupeWeeds, Ret, AvecHauteTension, AvecRecalageManuelBras);
		return (TRetourTask)Ret;
	});
	pAffiche->AjouteTache(tWeed, "Desherbage");
	return tWeed;
}



