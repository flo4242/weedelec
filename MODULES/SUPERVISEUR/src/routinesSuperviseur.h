#pragma once
#include "CalageWeedelec.h"
#include "GereWeeds.h"
#include "list_tpl.h"
#include <tuple>

//Fonction permettant d'enlever les weeds d�j� d�tect�es dans des cycles pr�c�dents
//Exemple: avec un pas de 33cm environ, les mauvaises herbes seront pr�sentes sur deux images cons�cutives
// TGroupeWeeds GW => Groupe actuel contenant la liste des weeds d�tect�es dans le cycle en cours
// _list<TGroupeWeeds> list_GW => Liste des groupes pass�s contenant toutes les listes de weeds d�tect�s
// TRepereLocal *pRepere rep�re robot du cycle en cours
// TPositionRobot PosAcq Position absolue du cycle en cours
bool FiltreWeedsDupliquees(TGroupeWeeds &lastGW, _list<TGroupeWeeds> &PileGW, TRepereLocal *pRepere, const TPositionRobot PosAcq, const int maxCyclesPile, const float tolDist);
bool TransfoPileWeeds(char msgerr[1000], _list<TGroupeWeeds> &PileGW, const TPositionRobot PosAcq, TRepereLocal &pRepere);
