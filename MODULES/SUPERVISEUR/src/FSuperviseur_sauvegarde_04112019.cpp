#define PERIODE_TIMER_CLAVIER 100
#define PERIODE_TIMER_AFFICHE 200



#include "Taches.h"

#include "WeedelecCommon.h"
#include "TaskUtil.h"

//fonctions export�es pour utilisation externe (autres modules)
#include "ExportModulePlantNet.h" 
#include "ExportModuleSony.h" 
#include "ExportModuleRikola.h"
#include "ExportModuleControleRobot.h"
#include "ExportModuleRecalageVisuel.h"
#include "CalageWeedelec.h"
#include "GereWeeds.h"
#include "DBOUT.h"

#include <array>
#include <list>

#include "list_tpl.h"

#include <math.h>  //pour fonction round

#include "routinesSuperviseur.h"
#include <chrono>
#include <ctime>    
#include <iostream>
#include <fstream>
#include <sstream>


using sc = std::chrono::system_clock; //Pour g�rer les conversions chrono vers string

using namespace Concurrency;

DRVTRAITIM *pDriverG;

struct TParamSuperviseur
{

	//param�tres exclusifs
	bool AcquisitionPlantNet;  
	bool AcquisitionSony; 
	bool AcquisitionSonySD; // sans t�l�chargement


	bool AcquisitionRikola; 
	// NB: d�placement de la plateforme si il y a au moins un type d'acquisition (�ventuellement avec timeout)

	//param�tres exclusifs
	bool DetectionPlantNet; 
	bool DetectionRikola;
	bool DetectionPastillesSony;
	bool DetectionManuelle;

	//param�tres exclusifs
	bool AvancementAuto;
	bool AvancementManuel;

	double step; //m�tres
	bool Weeding;
	bool RecalageManuelBras;
	bool HauteTension;
	bool RecalageRasberry;

	bool TimeOutAccepte;

	bool DesherbageKML;
	bool DesherbageFixe;
	bool DesherbageParTempo; //simulation
	//coordonn�es impos�es pour le mode Fixe (m�tres)
	double XWeed1Fixe, YWeed1Fixe;
	double XWeed2Fixe, YWeed2Fixe;

	bool logSuperviseur;
	int maxCyclesPile;
	float tolDist;
	TParamSuperviseur()
	{
		AcquisitionPlantNet = false;
		AcquisitionSony = false;
		AcquisitionSonySD = false;

		AcquisitionRikola = false;

		DetectionPlantNet=false;
		DetectionRikola = false; 
		DetectionManuelle = false;

		AvancementAuto = false;
		AvancementManuel = false;

		step = 0.7;

		Weeding = false;
		RecalageManuelBras = false;
		HauteTension = false;

		TimeOutAccepte = false;

		logSuperviseur = true;
		maxCyclesPile = 5;
		tolDist = 3;
		XWeed1Fixe = -0.3; YWeed1Fixe = 0.2;
		XWeed2Fixe = 0.2; YWeed2Fixe = -0.15;
	}
	

	bool AvecDetection() { return DetectionPlantNet || DetectionRikola || DetectionPastillesSony || DetectionManuelle; }
	bool WeedingHorsDetection() {	return DesherbageKML || DesherbageParTempo || DesherbageFixe;	}

} ParamG;


struct TSuperviseur : public DRVTRAITIM
{
	void CMGo();
	void CMParametres();

};

//fonctions canoniques export�es en tant qu'outil Traitimexport�es
extern "C"
{
	int _EXPORT_ Open_T_Superviseur(DRVTRAITIM *pD, HMODULE hinst)
		/*********************************************/
	{
		if (pD->SizeClass != sizeof(DRVTRAITIM))
		{
			char st[100];
			sprintf(st, "taille DRVTRAITIM non compatible - Driver: %zd  DLL: %d",
				pD->SizeClass, sizeof(DRVTRAITIM));
			::MessageBox(NULL, st, "T_Superviseur", MB_OK);
			return 0;
		}
		pDriverG = pD; 	 return 1;
	}



	int _EXPORT_ Close_T_Superviseur(DRVTRAITIM *)	{ return 1;}

	HMENU _EXPORT_ Ajoute_T_Superviseur(HMENU pere)
	/********************************/
	{
		HMENU Hprincipal = pDriverG->AjouteSousMenu(pere, "Superviseur",
		"Param�tres", &TSuperviseur::CMParametres,
		"Go", &TSuperviseur::CMGo,
		NULL);

		return Hprincipal;
	}
}

bool GereParametres(HWND hwnd, ModeGereParametres modeGereParam, TParamSuperviseur &param)
/*****************************************************************************************/
{
	bool OK;

	while (true)
	{

		GROUPE G(hwnd, "Param�tres superviseur", "Superviseur.cfg",

			SEPART("***       ACQUISITION D'IMAGES       ***"),
			PBOOL("Acquisition PlantNet", param.AcquisitionPlantNet),
			PBOOL("Acquisition Sony Wifi avec t�l�chargement", param.AcquisitionSony),
			PBOOL("Acquisition Sony Wifi SD seulement", param.AcquisitionSonySD),

			PBOOL("Acquisition Rikola", param.AcquisitionRikola),

			SEPART("***   DETECTION MAUVAISES HERBES     ***"),

			PBOOL("D�tection PlantNet", param.DetectionPlantNet),
			PBOOL("D�tection Rikola", param.DetectionRikola),
			PBOOL("D�tection pastilles Sony", param.DetectionPastillesSony),
			PBOOL("D�tection manuelle", param.DetectionManuelle),

			SEPART("***   AVANCEMENT ET DESHERBAGE       ***"),

			PBOOL("Avancement auto", param.AvancementAuto),
			PBOOL("Avancement manuel", param.AvancementManuel),
			DBL("Pas d'avancement (mode auto)", param.step, 0.1, 2),
			PBOOL("D�sherbage", param.Weeding),
			PBOOL("Recalage manuel position bras", param.RecalageManuelBras),
			PBOOL("Recalage visuel via Rasberry", param.RecalageRasberry),
			PBOOL("Lancement haute tension", param.HauteTension),

			PBOOL("Timeout accept� (sinon, consid�r� comme erreur)", param.TimeOutAccepte),

			SEPART("*** DESHERBAGE HORS DETECTION ***"),
			PBOOL("Liste weeds sur fichier kml", param.DesherbageKML),
			PBOOL("Simulation par tempo (2 s)", param.DesherbageParTempo),
			PBOOL("Coordonn�es fixes", param.DesherbageFixe),
			//DBL("Coordonn�e X weed 1 (mode fixe)", param.XWeed1Fixe, -0.4, 0.4),
			//DBL("Coordonn�e Y weed 1 (mode fixe)", param.YWeed1Fixe, -0.4, 0.4),
			//DBL("Coordonn�e X weed 2 (mode fixe)", param.XWeed2Fixe, -0.4, 0.4),
			//DBL("Coordonn�e Y weed 2 (mode fixe)", param.YWeed2Fixe, -0.4, 0.4),
			SEPART("*** AUTRES ***"),
			ENT("Nombre max de cycles en arri�re pour la recherche de weeds � d�sherber", param.maxCyclesPile,1, 20),
			FLT("Tol�rance pour la d�tection de la m�me weed entre deux cycles (cm)",param.tolDist,0,50),
			PBOOL("Cr�ation fichiers log superviseur",param.logSuperviseur),

			NULL);

		

		switch (modeGereParam)
		{
		case(ModeGereParametres::Charge): OK = G.Charge(); break;
		case(ModeGereParametres::Sauve): OK = G.Sauve(); break;
		case(ModeGereParametres::Gere): OK = G.Gere(false); break; //false pour avoir une saisie de groupe non modale
		}

		//v�rification incompatibilit�s
		if (OK)
		{
			char st[5000] = "Incompatibilit�s d�tect�es:";
			bool Pb1 = param.AcquisitionSony && param.AcquisitionSonySD;
			if (Pb1) strcat(st, "\n- Acquisition Sony Wifi: choisir entre t�l�chargement et SD seulement");

			int intPb2 = 0;
			if (param.DetectionPlantNet)	intPb2 += 1;
			if (param.DetectionRikola) intPb2 += 1;
			if (param.DetectionManuelle) intPb2 += 1;
			bool Pb2 = (intPb2 > 1);
			if (Pb2) strcat(st, "\n- choisir un mode unique de d�tection (ou aucun pour coordonn�es fixes)");

			bool Pb3 = param.AvancementAuto && param.AvancementManuel;
			if (Pb3) strcat(st, "\n- Avancement: choisir entre auto et manuel");

			bool Pb4 = false;
			if ((param.AcquisitionSony || param.AcquisitionSonySD) && param.AcquisitionPlantNet)
			{
				bool OK4 = pDriverG->MessageOuiNon
				("Acquisitions Sony et Plantnet simultan�es possibles si et seulement si l'acquisition PlantNet ne concerne pas le Sony.\n Valider?");
				Pb4 = !OK4;
			}
			if (Pb4) strcat(st, "\n- choisir entre acquisition PlantNet et Sony");

			bool Pb5 = false;
			if (param.DetectionManuelle && param.AcquisitionPlantNet)
			{
				bool OK5 = pDriverG->MessageOuiNon
		("D�tection manuelle (via Sony live Wifi) et acquisition Plantnet compatibles seulement si l'acquisition PlantNet ne concerne pas le Sony.\n Valider?");
				Pb5 = !OK5;
			}
			if (Pb5) strcat(st, "\n- choisir entre acquisition PlantNet et d�tection manuelle (via Sony Wifi)");

			bool Pb6 = param.DetectionRikola && (!param.AcquisitionRikola);
			if (Pb6) strcat(st, "\n- d�tection Rikola impossible sans acquisition Rikola");

			bool Pb7 = param.DetectionPlantNet && (!param.AcquisitionPlantNet);
			if (Pb7) strcat(st, "\n- d�tection PlantNet impossible sans acquisition PlantNet");

			bool Pb8 = ParamG.AvecDetection() && ParamG.WeedingHorsDetection();
			if (Pb8) strcat(st, "\n- d�tection et d�sherbage hors d�tection simultan�s impossibles");

			int cnt = 0;
			cnt += param.DesherbageKML ? 1 : 0;
			cnt += param.DesherbageParTempo ? 1 : 0;
			cnt += param.DesherbageFixe ? 1 : 0;
			bool Pb9 = cnt > 1;
			if (Pb9) strcat(st, "\n- choisir un seul mode de d�sherbage hors d�tection");

			bool Pb10 = ParamG.RecalageRasberry && ParamG.WeedingHorsDetection();
			if (Pb10) strcat(st, "\n- recalage visuel Rasberry impossible si d�sherbage hors d�tection");

			if (Pb1 || Pb2 || Pb3 || Pb4 || Pb5 || Pb6 || Pb7 || Pb8 || Pb9 || Pb10)	pDriverG->Message(st);

			else break;
		}

		
	}//while

	return OK;
}


//attente t�che avec gestion messages

void AttenteTacheMsg(task<vector<TRetourTask>> t)
{
	while (true)
	{
		MSG msg;
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 	DispatchMessage(&msg);
		if (t.is_done())	break;

	}

}

void TSuperviseur::CMParametres() {	GereParametres(HWindow(), ModeGereParametres::Gere, ParamG); }


//Fonction template pour le log d'une liste de weeds (robot ou bras) dans un stream
template <class myType>
void logWeedList(ofstream &logWeeding, _list<myType> &ListeWeeds) {
	BalayeListe(ListeWeeds) {
		TWeedRobot *ptr = ListeWeeds.current();
		logWeeding << "\n x=" << ptr->Position.x << " | y=" << ptr->Position.y;
	}
}
std::string datetime()
{
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[80];

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	strftime(buffer, 80, "%d-%m-%Y %H-%M-%S", timeinfo);
	return std::string(buffer);
}


/****************************************/
void TSuperviseur::CMGo()
/****************************************/
{
	try {
		OuvreTexte();
		EffaceTexte();

		//Heure d�but
		auto debutSuperviseur = std::chrono::system_clock::now();
		std::time_t debutSuperviseurTime_t = std::chrono::system_clock::to_time_t(debutSuperviseur);


		//stats superviseur (totalWeedsTraitees <= totalWeedsDetectees)
		int totalWeedsDetectees = 0;
		int totalWeedsTraitees = 0;


		char msgerr[1000];
		bool AvecGeo = true; //par d�faut. Sera d�valid� avec warning si pas de g�olocalisation possible


		TModuleRecalageRasberry TRR;
		TParamRecalageVisuel ParamRV;
		TParamRobot ParamR;
		_list<TWeedRepereLocal> ListeWeedsKML; //si import kml
		_list<TGroupeWeeds> PileGroupeWeeds; //historique des objets TGroupeWeeds sur chaque cycle, les premiers �l�ments sont les plus r�cents



		//Chargement des param�tres superviseur et recalage
		if (!GereParametres(HWindow(), ModeGereParametres::Gere, ParamG))	return;
		if (!TModulePlateforme::ChargeParametres(ParamR))
		{
			Message("Echec cahrgement param�tres recalage visuel");
			return;
		}
		if (ParamG.RecalageRasberry)
		{
			if (!TModuleRecalageRasberry::ChargeParametres(ParamRV))
			{
				Message("Echec cahrgement param�tres recalage visuel");
				return;
			}

			if (!TRR.Init(ParamRV, msgerr))
			{
				Message(msgerr);
				return;
			}

			else PrintTexte("\n Init Raberry recalage OK");
		}


		//Cr�ation de fichiers de log des cycles du superviseur
		bool logFlag = ParamG.logSuperviseur;
		ofstream logWeeding;
		ofstream logTasks;
		ofstream logTableau;
		PrintTexte(logFlag ? "Avec log\n" : "sans log\n");
		if (logFlag) {
			try {
				string path = "D:/Rabatel/LangageC/WeedelecSoft/LOG/SUPERVISEUR/";
				string logWeedingName = path + "weeding_"+ datetime() + ".log";
				logWeeding.open(logWeedingName);
				string logTasksName = path + "tasks_" + datetime() + ".log";				//string logTasksName = "D:/logSup_Task_.txt";
				PrintTexte(logTasksName.c_str());
				logTasks.open(logTasksName);
				string logTableauName = path + "tableau_" + datetime() + ".log";				//string logTableauName = "D:/logSup_Tableau_.txt";
				logTableau.open(logTableauName);
				logTasks << "D�but superviseur:" << std::ctime(&debutSuperviseurTime_t) << "\n";
				logTableau << "NOTES \n Les timestamps en premier sont les plus r�cents \n Un �chantillon avec comme flag A_TRAITER en fin de cycle correspond � une erreur \n";
				logTableau.close();
				PrintTexte("Fichiers log cr��s\n");
			}
			catch (exception &e) {
				PrintTexte("Impossible de cr�er les fichiers de log, log abandonn�\n");
				logFlag = false;
			}

		}





		//Affichage
		IMTGRIS * ImTaches = NouvelleImageGris("T�ches", 500, 200);
		int TickDebut = GetTickCount();
		int cntTimerG = 0;
		TAffichageTaches AT(ImTaches, &cntTimerG, TickDebut);



		////////////////////////////////////////////////////
		//VERIFICATIONS AVANT LANCEMENT SELON LES PARAMETRES
		////////////////////////////////////////////////////
		//v�rification et initialisation du module Rikola
		TModuleRikola ModuleRikola;
		if (ParamG.AcquisitionRikola)
		{
			TRetourAcqRikola Ret;
			ModuleRikola.PrepareAcquisition(Ret);
			if (Ret.State != TRetourState::Completed)
			{
				Message("Echec pr�paration acquisition Rikola:\n%s", Ret.InfoRetour);
				return;
			}
		}
		//v�rification GPS et robot
		TStateInitPlateforme check = TModulePlateforme::CheckInitialisationPlateforme();
		if (!(check.RasberryGPSConnecte || check.RepereLocalInitialise))
			if (ParamG.AvancementAuto)
			{
				PrintTexte("Echec positionnement GPS RTK --> avancement automatique impossible\n", msgerr);
				return;
			}
		if (!check.RobotConnecte)
			if (ParamG.AvancementAuto || ParamG.Weeding)
			{
				PrintTexte("Robot non connect� --> avancement ou weeding impossible\n", msgerr);
				return;
			}
		// v�rification haute tension
		if (ParamG.HauteTension)
			if (!TModulePlateforme::VerificationHauteTension())
			{
				PrintTexte("Commande haute tension non initialis�e\n", msgerr);
				return;
			}
		//v�rification image live et calage disponibles pour d�tection manuelle
		//unique_ptr<IMTCOULEUR> ImDetectionManuelle = make_unique<IMTCOULEUR>(nullptr);
		//unique_ptr<TCalage2D> CalageManuel = make_unique<TCalage2D>(nullptr);
		IMTCOULEUR *ImDetectionManuelle = NULL;
		TCalage2D *CalageManuel = NULL;
		POINTFLT CoeffCalageManuel;
		TRepereLocal *pRepere = TModulePlateforme::GetRepereLocal(); //�ventuellement nul. R�dhibitoire ou non selon options

		//Gestion d�tection manuelle
		if (ParamG.DetectionManuelle)
		{
			if (!pDriverG->DesigneImage("D�signer image live Sony"))
			{
				Message("Image live Sony n�cessaire pour la d�tection manuelle !"); return;
			}

			ImDetectionManuelle = pDriverG->ImageActiveCouleur();
			if (!ImDetectionManuelle)
			{
				Message("L'image design�e n'est pas une image couleur!"); return;
			}


			if (!ImDetectionManuelle->Indestructible)
			{
				Message("Image live destructible --> pas de live view en cours!"); return;
			}

			char msg[1000];
			CalageManuel = TModuleSony::ChargeCalage(msg);
			if (!CalageManuel)
			{
				Message(msg); return;
			}

			//r�cup�ration de la taille d'image live
			CoeffCalageManuel.x = 6000. / ImDetectionManuelle->dimX;
			CoeffCalageManuel.y = 4000. / ImDetectionManuelle->dimY;
		}


		//v�rification trajectoire disponible
		Trajectoire *Trajet = NULL;
		if (ParamG.AvancementAuto)
		{
			Trajet = TModulePlateforme::InitTrajectoire(msgerr);
			if (Trajet == NULL)
			{
				PrintTexte("Echec initialisation trajectoire --> avancement automatique impossible\n(%s)", msgerr);
				return;
			}

			/////////else PrintTexte("%s\n", msgerr); //affiche le nombre de points

		}

		if (ParamG.Weeding)
		{
			if (ParamG.DesherbageKML)
			{
				TNomFic NfWeeds = "";
				if (ChoixFichierOuvrir("Liste weeds", NfWeeds, "kml", "FichiersWeeds.cfg"))
				{
					_list<Waypoint> Liste;
					if (!TGereWeeds::ImportKML(NfWeeds, ListeWeedsKML, pRepere, msgerr))
					{
						Message(msgerr);
						return;
					}
				}
			}

		}



		//////////////////////
		//  BOUCLE D'ACTION
		//////////////////////


		//pr�paration t�ches communes pour boucle d'action
		cancellation_token_source ctArretClavier;
		task<TRetourTask> tClavier = CreateTaskTestClavier(PERIODE_TIMER_CLAVIER, &cntTimerG);
		task<TRetourTask> tAffiche = CreateTaskAffiche(&AT, PERIODE_TIMER_AFFICHE, ctArretClavier.get_token());




		//boucle des cycles
		int boucleCount{ 0 };
		while (true)
		{
			AT.Clear(); //r�init affichage

			boucleCount++;

			if (logFlag) { logWeeding << "\nCYCLE NUMERO:" << boucleCount; }

			///////////////////
			//lancement cycle 1 (avant mouvement: acquisition d'image + weeding d�tection pr�c�dente)
			///////////////////


			std::list<task<TRetourTask>> Cycle1;
			_list<char *> NamesCycle1; //pour retrouver le nom en cas d'arr�t

			PrintTexte("DEBUT BOUCLE %d \n", boucleCount);
			PrintTexte("=================================");
			PrintTexte("Lancement partie A cycle: Acquisitions et Weeding \n");



			//Obtention de la position du robot via le GPS
			char msgerr[1000];
			TPositionRobot PosAcq;
			if (AvecGeo) {
				if (!TModulePlateforme::GetPositionRobot(PosAcq, msgerr))
				{
					PrintTexte("Echec r�cup�ration position GPS: %s\n", msgerr);
					AvecGeo = false;
				}
			}


			if (!AvecGeo)
			{
				if (ParamG.AvancementAuto)	break;	//r�dhibitoire

				if (ParamG.AcquisitionPlantNet || ParamG.AcquisitionRikola || ParamG.AcquisitionSony)
					if (!MessageOuiNon("G�olocalisation des images impossible\n Continuer quand m�me ?"))
						break;
			}



			//le cycle 1 doit se terminer si toutes les t�ches concern�es sont termin�es (op�rateur &&)
			//en revanche, il doit se terminer d�s que la tache de test clavier se termine.
			//il faut donc faire avant un op�rateur || entre chaque t�che et la t�che clavier 

			task<TRetourTask> tPlantNet;

			task<TRetourTask> TotaleAcquisitionTraitementSony;
			task_completion_event<TRetourTask> tceFinAcquisitionSony;
			//�tabli en fin de t�che acquisition; permettra de lancer le mouvement robot
			// bien que la t�che acquisition soit continu�e avec le download de l'image;

			task<TRetourTask> TotaleAcquisitionTraitementRikola;
			task_completion_event<TRetourTask> tceFinAcquisitionRikola;
			//�tabli en fin de t�che acquisition; permettra de lancer le mouvement robot
			// bien que la t�che acquisition soit continu�e avec le traitement de l'image;


			task<TRetourTask> FinAcquisition; //�tabli soit pour Sony, soit pour Rikola

			task<TRetourTask> tWeeding;
			//t�ches d'acquisition /d�tection


			TGroupeWeeds GW; //groupe des weeds qui seront d�tect�es lors de ce cycle, dans le rep�re du robot
			GW.PosAcqValide = AvecGeo;
			//Passage m�tres WGS84
			if (AvecGeo)
			{
				pRepere->MetresVersWGS84(GW.PosAcq.LatDeg, GW.PosAcq.LonDeg, PosAcq.x, PosAcq.y);
				GW.CapDegAcq = PosAcq.CapDeg;
			}

			//Timestamp du cycle
			GW.TimeAcq = time(NULL);  
			PrintTexte("Timestamp avant: %d \n", GW.TimeAcq);

			if (ParamG.AcquisitionPlantNet)
			{
				tPlantNet = CreateAcquisitionPlantNet(&AT, AvecGeo, GW.PosAcq.LatDeg, GW.PosAcq.LonDeg);
				Cycle1.push_back(tPlantNet || tClavier);

				NamesCycle1.push_back("Acquisition PlantNet");
			}


			if (ParamG.AcquisitionSony || ParamG.AcquisitionSonySD)
			{

				if (ParamG.DetectionPastillesSony)
				{
					TotaleAcquisitionTraitementSony = CreateTaskTotaleSony(&AT, tceFinAcquisitionSony, pDriverG, &GW.ListeWeedsRobot);
					FinAcquisition = task<TRetourTask>(tceFinAcquisitionSony);
					Cycle1.push_back(FinAcquisition || tClavier);
				}

				else
				{
					TotaleAcquisitionTraitementSony = CreateAcquisitionSony(&AT);
					Cycle1.push_back(TotaleAcquisitionTraitementSony || tClavier);
				}


				NamesCycle1.push_back("Acquisition Sony");
			}



			if (ParamG.AcquisitionRikola)
			{
				TotaleAcquisitionTraitementRikola = CreateTaskTotaleRikola(&AT, &ModuleRikola, tceFinAcquisitionRikola,
					ParamG.DetectionRikola, ParamG.TimeOutAccepte, &GW.ListeWeedsRobot, AvecGeo, GW.PosAcq.LatDeg, GW.PosAcq.LonDeg);

				FinAcquisition = task<TRetourTask>(tceFinAcquisitionRikola);
				Cycle1.push_back(FinAcquisition || tClavier);
				NamesCycle1.push_back("Acquisition Rikola");
			}



			//Acquisition d'une image raspberry � l'avant en routine pour le timestamp GW.TimeAcq
			if (ParamG.RecalageRasberry) {
				if (!TRR.AcqImageAvant(GW.TimeAcq, msgerr))
				{
					PrintTexte("Recalage Rasberry: %s", msgerr);
					break;
				}
			}



			//Pr�paration de la t�che de weeding
			if (ParamG.Weeding)
			{
				int countWeedsATraiter = 0;
				//homographie de recalage �ventuelle
				H33 Hpixels;
				time_t t1Homographie;
				bool OKHomographie = false;



				//renseign� avec desherbage fixe, liste kml ou liste weeds d�tect�es

				if (ParamG.DesherbageParTempo)	//t�che de weeding simul�e par une tempo
				{
					tWeeding = CreateTacheTempo(&AT, 2000, "Weeding simule");
					Cycle1.push_back(tWeeding || tClavier);
					NamesCycle1.push_back("Weeding simule");
				}

				else if (ParamG.DesherbageFixe)
				{
					GW.ListeWeedsBrasTrans.push_back(TWeedBras(POINTFLT(ParamG.XWeed1Fixe, ParamG.YWeed1Fixe)));
					GW.ListeWeedsBrasTrans.push_back(TWeedBras(POINTFLT(ParamG.XWeed2Fixe, ParamG.YWeed2Fixe)));
				}

				else if (ListeWeedsKML.nbElem > 0) //GESTION LISTE DE WEEDS IMPORTEES VIA KML
				{

					//	QQ
					PrintTexte(" Gestion weeds sur kml non impl�ment�e pour l'instant!!!");
					break;


				}

				//Le d�sherbage commence � partir du deuxi�me cycle au minimum
				//On transforme alors les listes de weeds des cycles ant�rieurs dans l'espace robot actuel
				


				else if (PileGroupeWeeds.nbElem > 0) //GESTION LISTE DE WEEDS DETECTEES
				{

					if (ParamG.RecalageRasberry) //le recalage est-il possible ?
					{
						//tableau des timestamp disponibles, rang�s en ordre inverse (temps d�croissants)
						int nbT = PileGroupeWeeds.nbElem;
						//time_t *Tab = new time_t[nbT];
						//std::make_unique permet de ne pas avoir � g�rer manuellement la dur�e de vie du pointeur avant la fin de la port�e
						unique_ptr<time_t[]> Tab = make_unique<time_t[]>(nbT);
						int i;
						for (i = 0, PileGroupeWeeds.begin(); !PileGroupeWeeds.end(); PileGroupeWeeds++, i++)
							Tab[i] = PileGroupeWeeds.current()->TimeAcq;



						//recherche de l'homographie pour le recalage
						if (ParamG.AvancementAuto)
						{
							//combien de pas correspondent � l'�cart cam�ra ?
							int nbpas = std::nearbyint(TRR.EcartCameras / ParamG.step);
							//nbpas = 3; //XXXXXXXXXXXXXXXXXXXX
							if (nbpas == 0)
							{
								PrintTexte("\nLe param�tre de step (%.2f) est trop grand pour un recalage visuel !!!", ParamG.step);
								PrintTexte("\n(�cart cam�ras Rasberry: %.2f", TRR.EcartCameras);
								//non r�dhibitoire � ce stade
							}

							else if (nbpas > nbT) { ; } //pas assez de cycles effectu�s

							else
							{
								time_t tret = TRR.GetHomographie(Hpixels, Tab[nbpas - 1], msgerr);
								PrintTexte("Timestamp arri�re: %d \n", Tab[nbpas - 1]);
								if (tret == 0)
								{
									PrintTexte("\nEchec r�cup�ration homographie (%d steps en arri�re)", nbpas);
									//non r�dhibitoire � ce stade
								}

								else if (tret != Tab[nbpas - 1])
								{
									PrintTexte("Mauvais timestamp");
								}

								else
								{
									OKHomographie = true;
									t1Homographie = Tab[nbpas - 1];
								}
							}
						}


						if (ParamG.AvancementManuel)
						{
							//on va tester toutes les images disponibles
							t1Homographie = TRR.GetHomographie(Hpixels, 0, msgerr);

							if (t1Homographie > 0) 	OKHomographie = true;

							else
							{
								PrintTexte("\nEchec r�cup�ration homographie");
								//non r�dhibitoire � ce stade
							}
						}


						//delete Tab; 

						//on a maintenant (ou pas ! ) l'homographie de recalage...

					} //if (ParamG.RecalageRasberry) 


					//conversion des coordonn�es weeds dans le rep�re du bras


					//On place un flag sur les weeds � traiter pour les derniers cycles
					if (!OKHomographie) //positionnement des weeds dans le rep�re actuel selon position d'acquisition
					{
						if (logFlag) { logWeeding << "\nRecalage sans homographie"; }
						bool OKConversion = true;


						int i;
						for (PileGroupeWeeds.begin(), i = 0; (!PileGroupeWeeds.end() && i < ParamG.maxCyclesPile); PileGroupeWeeds++, i++)
						{
							TGroupeWeeds *curGw = PileGroupeWeeds.current();

							//On marque les weeds � traiter et on les convertit dans le rep�re actuel robot puis bras
							if (curGw->ListeWeedsRobot.nbElem > 0)
							{
								for (curGw->ListeWeedsRobot.begin(); curGw->ListeWeedsRobot.end(); curGw->ListeWeedsRobot++) {
									TWeedRobot *curWeedBras = curGw->ListeWeedsRobot.current();
									if (curWeedBras->state == NONE) { curWeedBras->state = A_TRAITE; countWeedsATraiter++; } //On met la weed dans la liste � traiter
								}
								if (!curGw->ToRepereRobotActuel(PosAcq, pRepere, msgerr)) //permet de remplir GW->ListeWeedsRobotTrans
								{
									PrintTexte("\n%s", msgerr); OKConversion = false;  break; //sortie boucle sur liste
								}
								if (!curGw->ToRepereBrasActuel(msgerr, ParamR.TranslationDeltaX, ParamR.TranslationDeltaY)) {
									PrintTexte("\n%s", msgerr); OKConversion = false;  break; //sortie boucle sur liste
								}
							}

						} //fin boucle sur les groupes de weeds pass�s
						if (!OKConversion)	break; //sortie boucles t�ches

						//on a maintenant dans ListeWeedsRobot l'ensemble des weeds d�tect�es jusqu'� pr�sent dans le rep�re actuel du robot.

					} //if (!OKHomographie)

					else //limitation aux mauvaises herbes d�tect�es lors de la prise d'image � l'instant t1Homographie;
					{
						bool OKConversion = true;
						if (logFlag) { logWeeding << "\nRecalage avec homographie"; }
						TGroupeWeeds *GwSelectionne = NULL;

						BalayeListe(PileGroupeWeeds)
						{
							TGroupeWeeds *curGw = PileGroupeWeeds.current();
							if (curGw->TimeAcq == t1Homographie) { GwSelectionne = curGw; break; }
						}

						if (GwSelectionne) //on va convertir les positions directement sans utiliser le positionnement GPS
						{
							H33 HDeplacement;
							if (!TRR.GetHomographieDeplacement(HDeplacement, Hpixels, msgerr))
							{
								PrintTexte("\nRecalage visuel: %s", msgerr); break; //sortie boucle des t�ches
							}
							if (logFlag) {
								logWeeding << HDeplacement.m11 << "|" << HDeplacement.m12 << "|" << HDeplacement.m13 << "\n";
								logWeeding << HDeplacement.m21 << "|" << HDeplacement.m22 << "|" << HDeplacement.m23 << "\n";
								logWeeding << HDeplacement.m31 << "|" << HDeplacement.m32 << "|" << HDeplacement.m33 << "\n";
							}

							//On marque les weeds � traiter et on les convertit dans le rep�re actuel robot puis bras
							if (GwSelectionne->ListeWeedsRobot.nbElem > 0)
							{
								for (GwSelectionne->ListeWeedsRobot.begin(); GwSelectionne->ListeWeedsRobot.end(); GwSelectionne->ListeWeedsRobot++) {
									TWeedRobot *curWeedBras = GwSelectionne->ListeWeedsRobot.current();
									if (curWeedBras->state == NONE) { curWeedBras->state = A_TRAITE; countWeedsATraiter++; } //On met la weed dans la liste � traiter
								}
								if (!GwSelectionne->ToRepereRobotActuelHomographie(HDeplacement, msgerr)) //permet de remplir GW->ListeWeedsRobotTrans
								{
									PrintTexte("\n%s", msgerr); OKConversion = false;  break; //sortie boucle sur liste
								}
								if (!GwSelectionne->ToRepereBrasActuel(msgerr, ParamR.TranslationDeltaX, ParamR.TranslationDeltaY)) {
									PrintTexte("\n%s", msgerr); OKConversion = false;  break; //sortie boucle sur liste
								}
							}

						}
					}//OKHomographie
				}

				//Lancement de la tache de weeding s'il y a au moins un �l�ment de PileGroupeWeeds avec le flag A_TRAITER
				if (countWeedsATraiter > 0)
				{
					PrintTexte("D�clenchement tache de weeding");
					task<TRetourTask> tWeeding = CreateWeeding(&AT, &PileGroupeWeeds, ParamG.HauteTension, ParamG.RecalageManuelBras);
					Cycle1.push_back(tWeeding || tClavier);

					char stw[50];
					sprintf(stw, "Weeding %d weed(s)", countWeedsATraiter);

					NamesCycle1.push_back(stw);
				}

			} //if (ParamG.Weeding)


			//Passage des listes de weeds dans la pile (ListeWeedsRobot) vers le rep�re robot actuel (ListeWeedsRobotTrans) puis le rep�re bras actuel (ListeWeedsBrasTrans)
			

			//Lancement de la t�che de weeding si des weeds ont �t� marqu�es

			
			/////////////////////
			//attente fin cycle 1
			/////////////////////

			auto var = when_all(begin(Cycle1), end(Cycle1));
			AttenteTacheMsg(var);

			vector<TRetourTask> res = var.get();

			bool OKCycle1 = true;
			PrintTexte("Fin des %i taches", res.size());
			for (int i = 0; i < res.size(); i++) {
				if (res.at(i).State != TRetourState::Completed)
				{
					OKCycle1 = false;
					PrintTexte("\n\ARRET CYCLE1:  %s (Task N�%d: %s)", res.at(i).InfoRetour.data(), i, NamesCycle1[i].obj);
				}
				//R�cup�ration des temps d'ex�cution de chaque tache et inscription au log
				if (logFlag) {
					if (!res.at(i).nomTache.empty()) {
						logTasks << "\n" << res.at(i).nomTache << "de" << sc::to_time_t(res.at(i).debutTask) << ":" << sc::to_time_t(res.at(i).finTask);
					}
				}
			}

			if (OKCycle1)	//tout s'est bien pass�
			{
				if (ParamG.DetectionManuelle)
				{
					//on intercale une phase de d�tection manuelle

					while (true)
					{
						POINTINT P;
						if (!pDriverG->DesignePoint(P, "Choisir un point sur l'image live")) break;
						//trac� du point sur l'image
						ImDetectionManuelle->CurseurInverse(P.x, P.y, 300);
						//conversion dans le rep�re du robot
						POINTFLT PM = CalageManuel->Pixels2Metres(POINTFLT(P.x * CoeffCalageManuel.x, P.y * CoeffCalageManuel.y));
						TWeedRobot TR(PM, -1, -1, NONE);
						GW.ListeWeedsRobot.push_back(TR);
						

						if (pDriverG->GetImageActive() != dynamic_cast<IMTRAITIM*>(ImDetectionManuelle))
						{
							Message("D�signer un point sur l'image live !!!");
							continue;
						}

					}
					logWeeding << "D�tections manuelles cycle: \n";
					logWeedList(logWeeding, GW.ListeWeedsRobot);
				} //fin d�tection manuelle
				if (ParamG.DetectionPastillesSony) {
					logWeeding << "D�tections SONY cycle: \n";
					logWeedList(logWeeding, GW.ListeWeedsRobot);
				}

			}


			if (!OKCycle1)	//erreur ou arr�t utilisateur
			{

				if (ParamG.AvancementAuto || ParamG.AvancementManuel)	TModulePlateforme::Stop();

				if (ParamG.AcquisitionRikola)	ModuleRikola.Stop();
				if (ParamG.AcquisitionSony || ParamG.AcquisitionSonySD)		TModuleSony::Stop();


				//attente arr�t effectif des t�ches en cours

				///////////////////if (ParamG.Weeding)		tWeeding.wait();
				if (ParamG.AcquisitionRikola)		TotaleAcquisitionTraitementRikola.wait();
				if (ParamG.AcquisitionSony || ParamG.AcquisitionSonySD)		TotaleAcquisitionTraitementSony.wait();

				//arr�t t�che affiche
				ctArretClavier.cancel();
				tAffiche.wait();

				AT.ListeValide = false;
				break;
			}




			///////////////////
			//lancement cycle 2
			///////////////////
			
			//le cycle 2 concerne l'avancement et la fin des traitements commenc�s en fin d'acquisition
			//il faut donc faire avant un op�rateur || entre chaque t�che et la t�che clavier 

			PrintTexte("\nLancement partie B cycle...\n");

			std::list<task<TRetourTask>> Cycle2; //avant et/ou pendant mouvement
			_list<char *> NamesCycle2; //pour retrouver le nom en cas d'arr�t



			// avancement plateforme

			bool CropFinished = false;

			task<TRetourTask> tStep;

			if (ParamG.AvancementManuel)
			{
				PrintTexte("Lancement tache avancement manuel\n");
				tStep = CreateStepManuel(&AT, this);
				Cycle2.push_back(tStep || tClavier);
				NamesCycle2.push_back("Avancement manuel");
			}

			if (ParamG.AvancementAuto)
			{
				PrintTexte("Lancement tache avancement automatique\n");
				tStep = CreateStepAuto(&AT, Trajet, ParamG.step, &CropFinished);
				Cycle2.push_back(tStep || tClavier);
				NamesCycle2.push_back("Avancement auto");

			}

			//si il y a des d�tections pr�vues, on devra attendre leur fin avant de repasser au cycle 1
			if (ParamG.DetectionRikola)
			{
				Cycle2.push_back(TotaleAcquisitionTraitementRikola || tClavier);
				NamesCycle2.push_back("D�tection Rikola");
			}

			if (ParamG.AcquisitionSony) //download Sony ou d�tection
			{
				Cycle2.push_back(TotaleAcquisitionTraitementSony || tClavier);
				NamesCycle2.push_back("Download Sony");
			}



			///////////////////
			//attente fin cycle 2
			///////////////////


			if (!Cycle2.empty())
			{
				auto var2 = when_all(begin(Cycle2), end(Cycle2));
				AttenteTacheMsg(var2);
				vector<TRetourTask> res2 = var2.get();

				bool OKCycle2 = true;
				PrintTexte("Fin des %i taches", res2.size());
				for (int i = 0; i < res2.size(); i++) {
					if (res2.at(i).State != TRetourState::Completed)
					{
						OKCycle2 = false;

						PrintTexte("\n\ ARRET CYCLE 2: %s (Task N�%d: %s)", res2.at(i).InfoRetour.data(), i, NamesCycle2[i].obj);
					}
					//R�cup�ration des temps d'ex�cution de chaque tache et inscription au log
					if (logFlag) {
						if (!res.at(i).nomTache.empty()) {
							logTasks << "\n" << res.at(i).nomTache << "de" << sc::to_time_t(res.at(i).debutTask) << ":" << sc::to_time_t(res.at(i).finTask);
						}
					}
				}

				if (!OKCycle2)	//erreur ou arr�t utilisateur

				{
					if (ParamG.AvancementAuto || ParamG.AvancementManuel)
						TModulePlateforme::Stop();	//il y a eu demande d'avancement auto

					if (ParamG.DetectionRikola)		ModuleRikola.Stop();
					if (ParamG.AcquisitionSony)		TModuleSony::Stop();


					//attente arr�t effectif des t�ches en cours

					/////////////////if (ParamG.AcquisitionRikola || ParamG.AcquisitionSony)		tStep.wait();
					if (ParamG.DetectionRikola) TotaleAcquisitionTraitementRikola.wait();
					if (ParamG.AcquisitionSony)		TotaleAcquisitionTraitementSony.wait();

					//arr�t t�che affiche
					ctArretClavier.cancel();
					tAffiche.wait();

					AT.ListeValide = false;
					break;
				}
			}



			//Filtrage de la liste de weeds d�tect�e durant ce cycle
			//Les duplicats sont supprim�s selon un param�tre de tol�rance
			if (PileGroupeWeeds.nbElem > 0) {
				PrintTexte("Nombre de weeds d�tect�es avant filtrage duplications: %i \n", GW.ListeWeedsRobot.nbElem);
				if (logFlag) {
					logWeeding << "\nD�tection avant filtrage";
					logWeedList(logWeeding, GW.ListeWeedsRobot);
				}
				if (!FiltreWeedsDupliquees(GW, PileGroupeWeeds, pRepere, PosAcq, ParamG.maxCyclesPile, ParamG.tolDist)) {
					PrintTexte("Erreur dans le filtrage \n");
					break;
				}
				PrintTexte("Nombre de weeds d�tect�es apr�s filtrage duplications: %i \n", GW.ListeWeedsRobot.nbElem);
				if (logFlag) {
					logWeeding << "\nD�tection apr�s filtrage";
					logWeedList(logWeeding, GW.ListeWeedsRobot);
				}
			}


			//Ajout du groupe en cours au DEBUT de la liste globale (on peut ainsi facilement acc�der aux n derniers �l�ments)
			//Les weeds de cette pile n'ont bien s�r pas encore �t� marqu�es pour d�sherbage (flag NONE)
			//========================
			PileGroupeWeeds.push_front(GW);
			if (PileGroupeWeeds.nbElem > ParamG.maxCyclesPile) {
				//On enl�ve le dernier �l�ment du tableau pour rester � ParamG.maxCyclesPile �l�ments
				PileGroupeWeeds.pop_back();
			}
			//========================



			//Compte du nombre de weeds trait�es parmis les d�tect�es

			int compteWeedsTraitees = 0;
			totalWeedsDetectees = 0;
			int zz = 0;
			PrintTexte("Log tableau");
			BalayeListe(PileGroupeWeeds) {
				TGroupeWeeds *curGWptr = PileGroupeWeeds.current();
				_list<TWeedRobot> tempListWeed = curGWptr->ListeWeedsRobot;
				for (tempListWeed.begin(); tempListWeed.end(); tempListWeed++) {
					TWeedRobot * tempCurWeed = tempListWeed.current();
					if (tempCurWeed->state == DONE) { ++compteWeedsTraitees; tempCurWeed->state = DONE_COMPTE; } //pour ne pas la compter plusieurs fois
					if (logFlag) {
						DBOUT("log tableau ligne ok");
						logTableau << "ID=" << ++zz << "T=" << curGWptr->TimeAcq << ",FLAG=" << tempCurWeed->state << "\n";
					}
				}
				totalWeedsDetectees += curGWptr->ListeWeedsRobot.nbElem;
			}
			PrintTexte("Log weeding");
			//log des informations de r�sum� sur le cycle
			if (logFlag) {
				logWeeding << "Lattitude: " << GW.PosAcq.LatDeg << "  Longitude: " << GW.PosAcq.LonDeg << "  Cap" << GW.CapDegAcq << "  Timestamp: " << GW.TimeAcq << "\n";
				logWeeding << "Nombre de mauvaises herbes d�tect�es dans ce cycles (apr�s filtrage)" << GW.ListeWeedsRobot.nbElem << "\n";
				logWeeding << "Nombre de mauvaises herbes trait�es dans ce cycle" << compteWeedsTraitees - totalWeedsTraitees << "\n";
				totalWeedsTraitees = compteWeedsTraitees;
				logWeeding << "Nombre TOTAL de mauvaises herbes d�tect�es" << totalWeedsDetectees << "\n";
				logWeeding << "Nombre TOTAL de mauvaises herbes trait�es" << totalWeedsTraitees << "\n";
				logWeeding << "============================\n";
			}



			if (CropFinished)
			{
				PrintTexte("FIN DU TRAJET EN MODE AUTOMATIQUE");
				AT.ListeValide = false;
				break;
			}

			//Inscription des streams sans fermer et reouvrir les fichiers
			logTasks.flush(); logWeeding.flush(); logTableau.flush();

			PrintTexte("Passage au cycle suivant \n");
		} //while (true)

		//fermeture des logs
		if (logTasks.is_open()) { logTasks.close(); }
		if (logWeeding.is_open()) { logWeeding.close(); }
		if (logTableau.is_open()) { logTableau.close(); }


		//Nettoyage des allocations manuelles
		if (CalageManuel)	delete CalageManuel;
		if (Trajet)	delete Trajet;
	}
	catch (exception &e) { //Gestion d'exception g�n�rale
		//Abandon de la s�quence
		PrintTexte("\nERREUR\n");
		PrintTexte(e.what());
		PrintTexte("\nFermeture du superviseur"); 
		//NB: les logs passent hors de port�e et le destructeur appelle automatiquement close()
		return;
	}
}
