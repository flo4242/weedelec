#define PERIODE_TIMER_CLAVIER 100
#define PERIODE_TIMER_AFFICHE 200
#define GPS_MESURE_INIT_DELAI 1000
#define GPS_MESURE_DELAI 100
#define GPS_MESURE_NOMBRE 5


#include "Taches.h"
#include "WeedelecCommon.h"
#include "TaskUtil.h"
#include <boost/timer/timer.hpp>
//fonctions export�es pour utilisation externe (autres modules)
#include "ExportModulePlantNet.h" 
#include "ExportModuleSony.h" 
#include "ExportModuleRikola.h"
#include "ExportModuleControleRobot.h"
#include "ExportModuleRecalageVisuel.h"
#include "CalageWeedelec.h"
#include "GereWeeds.h"
#include "DBOUT.h"

#include <array>
#include <list>

#include <numeric>
#include "list_tpl.h"

#include <math.h>  //pour fonction round

#include "routinesSuperviseur.h"
#include <ctime>    
#include <iostream>
#include <fstream>
#include <sstream>


//using sc = std::chrono::system_clock; //Pour g�rer les conversions chrono vers string

using namespace Concurrency;

DRVTRAITIM *pDriverG;

struct TParamSuperviseur
{

	//param�tres exclusifs
	bool AcquisitionPlantNet;  
	bool AcquisitionSony; 
	bool AcquisitionSonySD; // sans t�l�chargement


	bool AcquisitionRikola; 
	// NB: d�placement de la plateforme si il y a au moins un type d'acquisition (�ventuellement avec timeout)

	//param�tres exclusifs
	bool DetectionPlantNet; 
	bool DetectionRikola;
	bool DetectionPastillesSony;
	bool DetectionManuelle;

	//param�tres exclusifs
	bool AvancementAuto;
	bool AvancementManuel;

	double step; //m�tres
	bool Weeding;
	bool RecalageManuelBras;
	bool HauteTension;
	bool FiltreDuplication;
	bool RecalageRasberry;
	bool RecalageRasberryMultHom;
	bool prioriteDesherbageGPS;
	bool TimeOutAccepte;

	bool DesherbageKML;
	bool DesherbageFixe;
	bool DesherbageParTempo; //simulation
	//coordonn�es impos�es pour le mode Fixe (m�tres)
	double XWeed1Fixe, YWeed1Fixe;
	double XWeed2Fixe, YWeed2Fixe;

	bool logSuperviseur;
	int maxCyclesPile;
	float tolDist;
	TParamSuperviseur()
	{
		AcquisitionPlantNet = false;
		AcquisitionSony = false;
		AcquisitionSonySD = false;

		AcquisitionRikola = false;

		DetectionPlantNet=false;
		DetectionRikola = false; 
		DetectionManuelle = false;

		AvancementAuto = false;
		AvancementManuel = false;

		step = 0.7;

		Weeding = false;
		RecalageManuelBras = false;
		HauteTension = false;
		FiltreDuplication = false;
		TimeOutAccepte = false;


		logSuperviseur = true;
		maxCyclesPile = 5;
		tolDist = 3;
		XWeed1Fixe = -0.3; YWeed1Fixe = 0.2;
		XWeed2Fixe = 0.2; YWeed2Fixe = -0.15;
	}
	

	bool AvecDetection() { return DetectionPlantNet || DetectionRikola || DetectionPastillesSony || DetectionManuelle; }
	bool WeedingHorsDetection() {	return DesherbageKML || DesherbageParTempo || DesherbageFixe;	}

} ParamG;


struct TSuperviseur : public DRVTRAITIM
{
	void CMGo();
	void CMParametres();

};

//fonctions canoniques export�es en tant qu'outil Traitimexport�es
extern "C"
{
	int _EXPORT_ Open_T_Superviseur(DRVTRAITIM *pD, HMODULE hinst)
		/*********************************************/
	{
		if (pD->SizeClass != sizeof(DRVTRAITIM))
		{
			char st[100];
			sprintf(st, "taille DRVTRAITIM non compatible - Driver: %zd  DLL: %d",
				pD->SizeClass, sizeof(DRVTRAITIM));
			::MessageBox(NULL, st, "T_Superviseur", MB_OK);
			return 0;
		}
		pDriverG = pD; 	 return 1;
	}



	int _EXPORT_ Close_T_Superviseur(DRVTRAITIM *)	{ return 1;}

	HMENU _EXPORT_ Ajoute_T_Superviseur(HMENU pere)
	/********************************/
	{
		HMENU Hprincipal = pDriverG->AjouteSousMenu(pere, "Superviseur",
		"Param�tres", &TSuperviseur::CMParametres,
		"Go", &TSuperviseur::CMGo,
		NULL);

		return Hprincipal;
	}
}

bool GereParametres(HWND hwnd, ModeGereParametres modeGereParam, TParamSuperviseur &param)
/*****************************************************************************************/
{
	bool OK;

	while (true)
	{

		GROUPE G(hwnd, "Param�tres superviseur", "Superviseur.cfg",

			SEPART("***       ACQUISITION D'IMAGES       ***"),
			PBOOL("Acquisition PlantNet", param.AcquisitionPlantNet),
			PBOOL("Acquisition Sony Wifi avec t�l�chargement", param.AcquisitionSony),
			PBOOL("Acquisition Sony Wifi SD seulement", param.AcquisitionSonySD),

			PBOOL("Acquisition Rikola", param.AcquisitionRikola),

			SEPART("***   DETECTION MAUVAISES HERBES     ***"),

			PBOOL("D�tection PlantNet", param.DetectionPlantNet),
			PBOOL("D�tection Rikola", param.DetectionRikola),
			PBOOL("D�tection pastilles Sony", param.DetectionPastillesSony),
			PBOOL("D�tection manuelle", param.DetectionManuelle),

			SEPART("***   AVANCEMENT ET DESHERBAGE       ***"),

			PBOOL("Avancement auto", param.AvancementAuto),
			PBOOL("Avancement manuel", param.AvancementManuel),
			DBL("Pas d'avancement (mode auto)", param.step, 0.1, 2),
			PBOOL("D�sherbage", param.Weeding),
			PBOOL("Recalage manuel position bras", param.RecalageManuelBras),
			PBOOL("Recalage visuel via Rasberry", param.RecalageRasberry),
			PBOOL("Lancement haute tension", param.HauteTension),

			PBOOL("Timeout accept� (sinon, consid�r� comme erreur)", param.TimeOutAccepte),

			SEPART("*** DESHERBAGE HORS DETECTION ***"),
			PBOOL("Liste weeds sur fichier kml", param.DesherbageKML),
			PBOOL("Simulation par tempo (2 s)", param.DesherbageParTempo),
			PBOOL("Coordonn�es robot al�atoires", param.DesherbageFixe),
			//DBL("Coordonn�e X weed 1 (mode fixe)", param.XWeed1Fixe, -0.4, 0.4),
			//DBL("Coordonn�e Y weed 1 (mode fixe)", param.YWeed1Fixe, -0.4, 0.4),
			//DBL("Coordonn�e X weed 2 (mode fixe)", param.XWeed2Fixe, -0.4, 0.4),
			//DBL("Coordonn�e Y weed 2 (mode fixe)", param.YWeed2Fixe, -0.4, 0.4),
			SEPART("*** AUTRES ***"),
			ENT("Nombre max de cycles en arri�re pour la recherche de weeds � d�sherber", param.maxCyclesPile,1, 100),
			PBOOL("Filtrage de la duplication des d�tections", param.FiltreDuplication),
			FLT("Tol�rance pour le filtre de duplication (cm)",param.tolDist,0,50),
			PBOOL("Cr�ation fichiers log superviseur",param.logSuperviseur),
			PBOOL("Recalage multi homographies raspberry",param.RecalageRasberryMultHom),
			PBOOL("Utiliser en priorit� le recalage GPS",param.prioriteDesherbageGPS),


			
			NULL);

		

		switch (modeGereParam)
		{
		case(ModeGereParametres::Charge): OK = G.Charge(); break;
		case(ModeGereParametres::Sauve): OK = G.Sauve(); break;
		case(ModeGereParametres::Gere): OK = G.Gere(false); break; //false pour avoir une saisie de groupe non modale
		}

		//v�rification incompatibilit�s
		if (OK)
		{
			char st[5000] = "Incompatibilit�s d�tect�es:";
			bool Pb1 = param.AcquisitionSony && param.AcquisitionSonySD;
			if (Pb1) strcat(st, "\n- Acquisition Sony Wifi: choisir entre t�l�chargement et SD seulement");

			int intPb2 = 0;
			if (param.DetectionPlantNet)	intPb2 += 1;
			if (param.DetectionRikola) intPb2 += 1;
			if (param.DetectionManuelle) intPb2 += 1;
			bool Pb2 = (intPb2 > 1);
			if (Pb2) strcat(st, "\n- choisir un mode unique de d�tection (ou aucun pour coordonn�es fixes)");

			bool Pb3 = param.AvancementAuto && param.AvancementManuel;
			if (Pb3) strcat(st, "\n- Avancement: choisir entre auto et manuel");

			bool Pb4 = false;
			if ((param.AcquisitionSony || param.AcquisitionSonySD) && param.AcquisitionPlantNet)
			{
				bool OK4 = pDriverG->MessageOuiNon
				("Acquisitions Sony et Plantnet simultan�es possibles si et seulement si l'acquisition PlantNet ne concerne pas le Sony.\n Valider?");
				Pb4 = !OK4;
			}
			if (Pb4) strcat(st, "\n- choisir entre acquisition PlantNet et Sony");

			bool Pb5 = false;
			if (param.DetectionManuelle && param.AcquisitionPlantNet)
			{
				bool OK5 = pDriverG->MessageOuiNon
		("D�tection manuelle (via Sony live Wifi) et acquisition Plantnet compatibles seulement si l'acquisition PlantNet ne concerne pas le Sony.\n Valider?");
				Pb5 = !OK5;
			}
			if (Pb5) strcat(st, "\n- choisir entre acquisition PlantNet et d�tection manuelle (via Sony Wifi)");

			bool Pb6 = param.DetectionRikola && (!param.AcquisitionRikola);
			if (Pb6) strcat(st, "\n- d�tection Rikola impossible sans acquisition Rikola");

			bool Pb7 = param.DetectionPlantNet && (!param.AcquisitionPlantNet);
			if (Pb7) strcat(st, "\n- d�tection PlantNet impossible sans acquisition PlantNet");

			bool Pb8 = ParamG.AvecDetection() && ParamG.WeedingHorsDetection();
			if (Pb8) strcat(st, "\n- d�tection et d�sherbage hors d�tection simultan�s impossibles");

			int cnt = 0;
			cnt += param.DesherbageKML ? 1 : 0;
			cnt += param.DesherbageParTempo ? 1 : 0;
			cnt += param.DesherbageFixe ? 1 : 0;
			bool Pb9 = cnt > 1;
			if (Pb9) strcat(st, "\n- choisir un seul mode de d�sherbage hors d�tection");

			bool Pb10 = ParamG.RecalageRasberry && ParamG.WeedingHorsDetection();
			if (Pb10) strcat(st, "\n- recalage visuel Rasberry impossible si d�sherbage hors d�tection");

			if (Pb1 || Pb2 || Pb3 || Pb4 || Pb5 || Pb6 || Pb7 || Pb8 || Pb9 || Pb10)	pDriverG->Message(st);

			else break;
		}

		
	}//while

	return OK;
}


//attente t�che avec gestion messages

void AttenteTacheMsg(task<vector<TRetourTask>> t)
{
	while (true)
	{
		MSG msg;
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 	DispatchMessage(&msg);
		if (t.is_done())	break;

	}

}

void TSuperviseur::CMParametres() {	GereParametres(HWindow(), ModeGereParametres::Gere, ParamG); }


//Fonction template pour le log d'une liste de weeds (robot ou bras) dans un stream
template <class myType>
void logWeedList(ofstream &logWeeding, _list<myType> &ListeWeeds, WeedState mode) {
	logWeeding << "\n";
	if (ListeWeeds.nbElem > 0) {
		BalayeListe(ListeWeeds) {
			myType *ptr = ListeWeeds.current();
			if (ptr->state == mode) {
				logWeeding << "x=" << ptr->Position.x << " | y=" << ptr->Position.y << "("<< mode<<")" << "\n";
			}
		}
	}
	return;
}


std::string datetime()
{
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[80];

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	strftime(buffer, 80, "%d-%m-%Y %H-%M-%S", timeinfo);
	return std::string(buffer);
}




/****************************************/
void TSuperviseur::CMGo()
/****************************************/
{
	try {
		OuvreTexte();
		EffaceTexte();
		int totalWeedsDetectees = 0;
		int totalWeedsTraitees = 0;
		char msgerr[1000];
		bool AvecGeo = true; //Indique si l'on souhaite utiliser le GPS pour l'avancement ou le d�sherbage 
		if (ParamG.AvancementManuel && !(ParamG.DesherbageKML || ParamG.Weeding)) {
			AvecGeo = false;
			PrintTexte("Pas de d�sherbage et avancement manuel => Fonctionnalit�s GPS non utilis�es");
		}
		bool desherbageGPSOK = false;  //Modes accept�s : fix, float si option TParamRobot::guidageFloatAccepte
		bool navigationGPSOK = false; //Modes accept�s: fix seulement pour pr�cision centim�trique de d�sherbage. Dans le cas contraire le recalage par homographie sera utilis� si possible
		//Cr�ation de fichiers de log des cycles du superviseur
		bool logFlag = ParamG.logSuperviseur;
		ofstream logWeeding;
		PrintTexte(logFlag ? "Avec log\n" : "sans log\n");
		if (logFlag) {
			try {
				string path = "D:/Rabatel/LangageC/WeedelecSoft/LOG/SUPERVISEUR/";
				string logWeedingName = path + "weeding_" + datetime() + ".log";
				logWeeding.open(logWeedingName);
				PrintTexte("Fichier log cr��\n");
				logWeeding << "==================\n";
				logWeeding << "ENUM FLAG: 0=NONE, 1=A_TRAITER, 2=DONE, 3=DONE_COMPTE \n";
				logWeeding << "ENUM MET: 0=NODEF, 1=GPS, 2=HOMOGRAPHIE \n";
				logWeeding << "==================\n";
			}
			catch (exception &e) {
				PrintTexte("Impossible de cr�er le fichier de log, log abandonn�\n");
				logFlag = false;
			}

		}
		//Initialisation des param�tres
		TParamCalage ParamCalageAvant;
		TParamCalage ParamCalageArriere;
		TModuleRecalageRasberry TRR;
		TParamRecalageVisuel ParamRV;
		TParamRobot ParamR;
		TModuleSony TSONY;
		TParamDetectionSony ParamSonyDet;
		TParamAcquisitionSony ParamSonyAcq;
		_list<TWeedRepereLocal> ListeWeedsKML; //si import kml
		_list<TGroupeWeeds> PileGroupeWeeds; //historique des objets TGroupeWeeds sur chaque cycle, les premiers �l�ments sont les plus r�cents



		//Chargement des param�tres superviseur et recalage
		if (!GereParametres(HWindow(), ModeGereParametres::Gere, ParamG))	return;
		if (!TModulePlateforme::ChargeParametres(ParamR))
		{
			Message("Echec cahrgement param�tres recalage visuel");
			return;
		}
		if (ParamG.RecalageRasberry)
		{
			if (!TModuleRecalageRasberry::ChargeParametres(ParamRV, ParamCalageAvant, ParamCalageArriere))
			{
				Message("Echec chargement param�tres recalage visuel");
				return;
			}

			if (!TRR.Init(ParamRV, ParamCalageAvant, ParamCalageArriere, msgerr))
			{
				Message(msgerr);
				return;
			}

			if (logFlag) { //Inscription des deux matrices de calage au log
				logWeeding << "Matrice de calage du Raspberry arri�re: \n";
				logWeeding << TRR.CalageArriere->m11 << "|" << TRR.CalageArriere->m12 << "|" << TRR.CalageArriere->m13 << "\n";
				logWeeding << TRR.CalageArriere->m21 << "|" << TRR.CalageArriere->m22 << "|" << TRR.CalageArriere->m23 << "\n";
				logWeeding << TRR.CalageArriere->m31 << "|" << TRR.CalageArriere->m32 << "|" << TRR.CalageArriere->m33 << "\n";
				logWeeding << "Matrice de calage du Raspberry avant: \n";
				logWeeding << TRR.CalageAvant->m11 << "|" << TRR.CalageAvant->m12 << "|" << TRR.CalageAvant->m13 << "\n";
				logWeeding << TRR.CalageAvant->m21 << "|" << TRR.CalageAvant->m22 << "|" << TRR.CalageAvant->m23 << "\n";
				logWeeding << TRR.CalageAvant->m31 << "|" << TRR.CalageAvant->m32 << "|" << TRR.CalageAvant->m33 << "\n";
			}
		}






		//Affichage
		IMTGRIS * ImTaches = NouvelleImageGris("T�ches", 500, 200);
		int TickDebut = GetTickCount();
		int cntTimerG = 0;
		TAffichageTaches AT(ImTaches, &cntTimerG, TickDebut);



		////////////////////////////////////////////////////
		//VERIFICATIONS AVANT LANCEMENT SELON LES PARAMETRES
		////////////////////////////////////////////////////
		//v�rification et initialisation du module Rikola
		TModuleRikola ModuleRikola;
		if (ParamG.AcquisitionRikola)
		{
			TRetourAcqRikola Ret;
			ModuleRikola.PrepareAcquisition(Ret);
			if (Ret.State != TRetourState::Completed)
			{
				Message("Echec pr�paration acquisition Rikola:\n%s", Ret.InfoRetour);
				return;
			}
		}
		//v�rification GPS et robot
		TStateInitPlateforme check = TModulePlateforme::CheckInitialisationPlateforme();
		if (!(check.SeptentrioGPSConnecte || check.RepereLocalInitialise)) {
			if (ParamG.AvancementAuto)
			{
				PrintTexte("Echec positionnement GPS RTK --> avancement automatique impossible\n", msgerr);
				return;
			}
		}
		if (!check.RobotConnecte) {
			if (ParamG.AvancementAuto || ParamG.Weeding)
			{
				PrintTexte("Robot non connect� --> avancement ou weeding impossible\n", msgerr);
				return;
			}
		}
		// v�rification haute tension
		if (ParamG.HauteTension) {
			if (!TModulePlateforme::VerificationHauteTension())
			{
				PrintTexte("Commande haute tension non initialis�e\n");
				return;
			}
		}


		TRetourPlateforme retInit;
		if (!TModulePlateforme::ResetBrasDroit(retInit)) {
			PrintTexte("Echec reset et home initiaux du bras\n");
			return;
		}

		//v�rification image live et calage disponibles pour d�tection manuelle
		//unique_ptr<IMTCOULEUR> ImDetectionManuelle = make_unique<IMTCOULEUR>(nullptr);
		//unique_ptr<TCalage2D> CalageManuel = make_unique<TCalage2D>(nullptr);
		IMTCOULEUR *ImDetectionManuelle = NULL;
		TCalage2D *CalageManuel = NULL;
		POINTFLT CoeffCalageManuel;
		TRepereLocal *pRepere = TModulePlateforme::GetRepereLocal(); //�ventuellement nul. R�dhibitoire ou non selon options

		//Gestion d�tection manuelle
		if (ParamG.DetectionManuelle)
		{
			if (!pDriverG->DesigneImage("D�signer image live Sony"))
			{
				Message("Image live Sony n�cessaire pour la d�tection manuelle !"); return;
			}

			ImDetectionManuelle = pDriverG->ImageActiveCouleur();
			if (!ImDetectionManuelle)
			{
				Message("L'image design�e n'est pas une image couleur!"); return;
			}


			if (!ImDetectionManuelle->Indestructible)
			{
				Message("Image live destructible --> pas de live view en cours!"); return;
			}

			char msg[1000];
			CalageManuel = TModuleSony::ChargeCalage(msg);
			if (!CalageManuel)
			{
				Message(msg); return;
			}

			//r�cup�ration de la taille d'image live
			CoeffCalageManuel.x = 6000. / ImDetectionManuelle->dimX;
			CoeffCalageManuel.y = 4000. / ImDetectionManuelle->dimY;
		}


		//v�rification trajectoire disponible
		Trajectoire *Trajet = NULL;
		if (ParamG.AvancementAuto)
		{
			Trajet = TModulePlateforme::InitTrajectoire(msgerr);
			if (Trajet == NULL)
			{
				PrintTexte("Echec initialisation trajectoire --> avancement automatique impossible\n(%s)", msgerr);
				return;
			}
		}

		if (ParamG.Weeding)
		{
			if (ParamG.DesherbageKML)
			{
				TNomFic NfWeeds = "";
				if (ChoixFichierOuvrir("Liste weeds", NfWeeds, "kml", "FichiersWeeds.cfg"))
				{
					_list<Waypoint> Liste;
					if (!TGereWeeds::ImportKML(NfWeeds, ListeWeedsKML, pRepere, msgerr))
					{
						Message(msgerr);
						return;
					}
				}
			}

		}



		//////////////////////
		//  BOUCLE D'ACTION
		//////////////////////


		//pr�paration t�ches communes pour boucle d'action
		cancellation_token_source ctArretClavier;
		task<TRetourTask> tClavier = CreateTaskTestClavier(PERIODE_TIMER_CLAVIER, &cntTimerG);
		task<TRetourTask> tAffiche = CreateTaskAffiche(&AT, PERIODE_TIMER_AFFICHE, ctArretClavier.get_token());

		//Vider le tableau des timestamps g�r� par le raspberry arri�re
		if (ParamG.RecalageRasberry) {
			if (!TRR.InitTableau()) {
				PrintTexte("Echec initialisation tableaux timestamp raspberry");
			}
		}



		//boucle des cycles
		int boucleCount{ 0 };
		while (true)
		{
			AT.Clear(); //r�init affichage
			boucleCount++;
			if (logFlag) { logWeeding << "\nCYCLE NUMERO:" << boucleCount << "\n"; }
			///////////////////
			//lancement cycle 1 (avant mouvement: acquisition d'image + weeding d�tection pr�c�dente)
			///////////////////
			std::list<task<TRetourTask>> Cycle1;
			_list<char *> NamesCycle1; //pour retrouver le nom en cas d'arr�t

			PrintTexte("DEBUT BOUCLE %d \n", boucleCount);
			PrintTexte("=================================\n");
			PrintTexte("Lancement partie A cycle: Acquisitions et Weeding \n");



			//Obtention de la position du robot via le GPS
			char msgerr[1000];
			TPositionRobot PosAcq;
			double cumul_x, cumul_y = 0;
			//On v�rifie la qualit� des trames GPS
			if (AvecGeo) {
				//Petite pause pour s'assurer que la trame r�cup�r�e corresponde au robot � l'arr�t
				Sleep(GPS_MESURE_INIT_DELAI);
				TPositionRobot PosAcqTemp;
				std::vector<TPositionRobot> listePositionsCycle;
				for (int i_gps = 0; i_gps < GPS_MESURE_NOMBRE; i_gps++) {
					if (!TModulePlateforme::GetPositionRobot(PosAcqTemp, msgerr))
					{
						PrintTexte("Echec r�cup�ration position GPS: %s\n", msgerr);
						navigationGPSOK = false; //Guidage GPS impossible
					}
					else { //la position est un fix ou un float (par contraintes dans la r�cup�ration de la librairie NMEAPositionTCPIP)
						navigationGPSOK=true;
						//On v�rifie que la position obtenue corresponde � un fix RTK (code 4)
						if (PosAcqTemp.fix == 4 || (PosAcqTemp.fix == 2 && ParamR.guidageFloatAccepte)) {
							listePositionsCycle.push_back(PosAcqTemp);
							Sleep(GPS_MESURE_DELAI);
							desherbageGPSOK = true;
						}
						else {
							PrintTexte("Position GPS float disponible pour la navigation mais absence de fix non autoris�e");
							desherbageGPSOK = false; //Cela n'aurait plus de sens de faire le recalage des mauvaises herbes par GPS
						}
					}
				}
				if (desherbageGPSOK) {
					//Calcul de la position moyenne
					auto lambdaMean = [&](TPositionRobot a, TPositionRobot b) {
						return TPositionRobot{ a.x + b.x / listePositionsCycle.size(),
							a.y + b.y / listePositionsCycle.size(),
							a.elevation + b.elevation / listePositionsCycle.size(),
							a.CapDeg + b.CapDeg / listePositionsCycle.size(),
							a.rollDeg + b.rollDeg / listePositionsCycle.size(),
							a.pitchDeg + b.pitchDeg / listePositionsCycle.size(),
							a.fix, a.fixDroit, a.fixGauche, a.CapDispo }; };
					PosAcq.x = 0; PosAcq.y = 0; PosAcq.CapDeg = 0; 
					PosAcq = std::accumulate(listePositionsCycle.begin(), listePositionsCycle.end(), PosAcq, lambdaMean);
					PosAcq.CapDispo = true;
					PosAcq.fix = listePositionsCycle.begin()->fix;
					PrintTexte("Position enregistree lors du cycle: %f  |  %f \n", PosAcq.x, PosAcq.y);
				}
			}

			//le cycle 1 doit se terminer si toutes les t�ches concern�es sont termin�es (op�rateur &&)
			//en revanche, il doit se terminer d�s que la tache de test clavier se termine.
			//il faut donc faire avant un op�rateur || entre chaque t�che et la t�che clavier 

			task<TRetourTask> tPlantNet;

			task<TRetourTask> TotaleAcquisitionTraitementSony;
			task_completion_event<TRetourTask> tceFinAcquisitionSony;
			//�tabli en fin de t�che acquisition; permettra de lancer le mouvement robot
			// bien que la t�che acquisition soit continu�e avec le download de l'image;

			task<TRetourTask> TotaleAcquisitionTraitementRikola;
			task_completion_event<TRetourTask> tceFinAcquisitionRikola;
			//�tabli en fin de t�che acquisition; permettra de lancer le mouvement robot
			// bien que la t�che acquisition soit continu�e avec le traitement de l'image;


			task<TRetourTask> FinAcquisition; //�tabli soit pour Sony, soit pour Rikola

			task<TRetourTask> tWeeding;
			//t�ches d'acquisition /d�tection


			TGroupeWeeds GW; //groupe des weeds qui seront d�tect�es lors de ce cycle, dans le rep�re du robot
			GW.PosAcqValide = desherbageGPSOK;
			//Passage m�tres WGS84
			if (desherbageGPSOK)
			{
				pRepere->MetresVersWGS84(GW.PosAcq.LatDeg, GW.PosAcq.LonDeg, PosAcq.x, PosAcq.y);
				GW.CapDegAcq = PosAcq.CapDeg;
			}

			//Timestamp du cycle
			GW.TimeAcq = time(NULL);  
			PrintTexte("Timestamp avant: %d \n", GW.TimeAcq);

			if (ParamG.AcquisitionPlantNet)
			{
				logWeeding << "Entr�e tache d'acquisition Plantnet\n";
				tPlantNet = CreateAcquisitionPlantNet(&AT, &GW); //t�che plantnet => invoque une autre t�che pour la requ�te vers l'ordi plantNet et le renvoi d'un json des mauvaises herbe
				Cycle1.push_back(tPlantNet || tClavier);

				NamesCycle1.push_back("Acquisition PlantNet");
			}

			if (ParamG.DesherbageFixe)
			{
				//par d�faut en �tat NONE si non sp�cifi�
				/*GW.ListeWeedsRobot.push_back(TWeedRobot(POINTFLT(0, 0),0,0));
				GW.ListeWeedsRobot.push_back(TWeedRobot(POINTFLT(-0.3, 0.3), 0, 0));
				GW.ListeWeedsRobot.push_back(TWeedRobot(POINTFLT(0, 0.5), 0, 0));
				GW.ListeWeedsRobot.push_back(TWeedRobot(POINTFLT(0.5, 0.5), 0, 0));
				GW.ListeWeedsRobot.push_back(TWeedRobot(POINTFLT(0.9, 0.9), 0, 0));*/
				int tailleSequence = 5;
				int lowY = 0;
				int highY = 100;
				int lowX = 0;
				int highX = 100;
				double dif;
				for (int z = 0; z < tailleSequence; z++) {
					//g�n�rer un point
					float y = (lowY + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (highY - lowY)))) / 100;
					float x = (lowX + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (highX - lowX)))) / 100;
					PrintTexte("x= %f, y= %f \n", x, y); 
					GW.ListeWeedsRobot.push_back(TWeedRobot(POINTFLT(x, y),0,0));
				}


			}

			if (ParamG.AcquisitionSony || ParamG.AcquisitionSonySD)
			{

				if (ParamG.DetectionPastillesSony)
				{
					logWeeding << "Entr�e tache d'acquisition Sony\n";
					TotaleAcquisitionTraitementSony = CreateTaskTotaleSony(&AT, tceFinAcquisitionSony, pDriverG, &GW.ListeWeedsRobot);
					FinAcquisition = task<TRetourTask>(tceFinAcquisitionSony);
					Cycle1.push_back(FinAcquisition || tClavier);
				}

				else
				{
					TotaleAcquisitionTraitementSony = CreateAcquisitionSony(&AT);
					Cycle1.push_back(TotaleAcquisitionTraitementSony || tClavier);
				}


				NamesCycle1.push_back("Acquisition Sony");
			}



			if (ParamG.AcquisitionRikola)
			{
				TotaleAcquisitionTraitementRikola = CreateTaskTotaleRikola(&AT, &ModuleRikola, tceFinAcquisitionRikola,
					ParamG.DetectionRikola, ParamG.TimeOutAccepte, &GW.ListeWeedsRobot, AvecGeo, GW.PosAcq.LatDeg, GW.PosAcq.LonDeg);

				FinAcquisition = task<TRetourTask>(tceFinAcquisitionRikola);
				Cycle1.push_back(FinAcquisition || tClavier);
				NamesCycle1.push_back("Acquisition Rikola");
			}



			//===============
			//Acquisition d'une image raspberry � l'avant en routine pour le timestamp GW.TimeAcq
			if (ParamG.RecalageRasberry) {
				boost::timer::cpu_timer timer;
				PrintTexte("Acquisition raspberry avant \n");
				if (!TRR.AcqImageAvant(GW.TimeAcq, msgerr))
				{
					PrintTexte("Recalage Rasberry: %s", msgerr);
					break;
				}
				DBOUT("Acquisition raspberry avant: " << timer.format());
			}
			//===============


			//Section d�di�e au d�sherbage
			//g�re le recalage des positions weeds lors des d�placements et le lancement du bras
			if (ParamG.Weeding)
			{
				//Pr�paration de la t�che de weeding
				int countWeedsATraiter = 0;
				_list<H33> list_Hpixels;
				time_t t1Homographie;
				bool OKHomographie = false;	bool flagWeeding = true;
				if (ParamG.DesherbageParTempo)	//t�che de weeding simul�e par une tempo
				{
					tWeeding = CreateTacheTempo(&AT, 2000, "Weeding simule");
					Cycle1.push_back(tWeeding || tClavier);
					NamesCycle1.push_back("Weeding simule");
				}
				else if (ListeWeedsKML.nbElem > 0) //GESTION LISTE DE WEEDS IMPORTEES VIA KML
				{
					PrintTexte(" Gestion weeds sur kml non impl�ment�e pour l'instant!!!");
					break;
				}

				else if (PileGroupeWeeds.nbElem > 0) { //On ne commence � traiter les weeds d�tect�es qu'au 2�me cycle
					//On essaie de trouver une homographie avec un cycle pass�
					if (ParamG.RecalageRasberry) //le recalage par homographie est-il possible ?
					{
						if (ParamG.AvancementAuto)
						{
							//tableau des timestamp disponibles, rang�s en ordre inverse (temps d�croissants)
							int nbT = PileGroupeWeeds.nbElem;
							//time_t *Tab = new time_t[nbT];
							//note: std::make_unique permet de ne pas avoir � g�rer manuellement la dur�e de vie du pointeur avant la fin de la port�e
							unique_ptr<time_t[]> Tab = make_unique<time_t[]>(nbT);
							int i;
							for (i = 0, PileGroupeWeeds.begin(); !PileGroupeWeeds.end(), i<ParamG.maxCyclesPile; PileGroupeWeeds++, i++)
								Tab[i] = PileGroupeWeeds.current()->TimeAcq;
							//recherche de l'homographie pour le recalage
							//combien de pas correspondent � l'�cart cam�ra ?
							int nbpas = std::nearbyint(TRR.EcartCameras / ParamG.step);
							if (nbpas == 0)
							{
								PrintTexte("\nLe param�tre de step (%.2f) est trop grand pour un recalage visuel !!!", ParamG.step);
								PrintTexte("\n(�cart cam�ras Rasberry: %.2f", TRR.EcartCameras);
								//non r�dhibitoire � ce stade
							}
							else if (nbpas > nbT) { ; } //pas assez de cycles effectu�s
							else
							{
								//=============================================
								//Acquisition arri�re et calcul de l'homographie
								boost::timer::cpu_timer timer;
								PrintTexte("\nTentative de r�cup�ration homographie avec l'image %d : \n", Tab[nbpas - 1]);
								time_t timestampCommande;

								timestampCommande =  0;
								//timestampCommande = ParamG.RecalageRasberryMultHom ? 0: Tab[nbpas - 1];


								if (TRR.GetHomographie(list_Hpixels, timestampCommande, msgerr)) {
									PrintTexte("\nNombre d'homographie r�cup�r�es: %d \d", list_Hpixels.nbElem);
									OKHomographie = true;
								}
								else {
									PrintTexte("\Pas d'homographie r�cup�r�e");
								}
								DBOUT("Acquisition raspberry arri�re et homographie: " << timer.format());
								//=============================================
							}
						}
						if (ParamG.AvancementManuel) 
						{
							//On tente de d�terminer des homographies sur les 4 derni�res images avant
							PrintTexte("\nTentative de r�cup�ration homographie avec les images avant.");
							if (TRR.GetHomographie(list_Hpixels, 0, msgerr)) {
								PrintTexte("\nRecuperation de %d homographies", list_Hpixels.nbElem);
								OKHomographie = true;
							}
							else
							{
								PrintTexte("\nPas d'homographie r�cup�r�e");
								//non r�dhibitoire � ce stade
							}
						}

					} //if (ParamG.RecalageRasberry) 


					//On marque les m�thodes de recalage pour chaque groupe (GPS, HOMOGRAPHIE ou NONE) et les mauvaises herbes qui seront � traiter
					if (!OKHomographie && !desherbageGPSOK) { //Cas o� l'on ne peut faire aucun recalage, le d�sherbage n'aura donc pas lieu ce cycle
						PrintTexte("\nPas de recalage possible par homographie ou GPS, d�sherbage non r�alis� ! \n");
						flagWeeding = false;
					}
					else { //On boucle sur chaque groupe de la pile pour en tirer le meilleur mode de transformation
						// on doit g�rer les cas ou OKHomographie est false (pas d'homographie) et ou desherbageGPSOK est false (pas de GPS)
						int iPile, countGroupeATRAITER;
						for (PileGroupeWeeds.begin(), iPile = 0; (!PileGroupeWeeds.end() && iPile < ParamG.maxCyclesPile); PileGroupeWeeds++, iPile++)
						{
							TGroupeWeeds *curGw = PileGroupeWeeds.current();
							//On regarde alors dans la liste des homographies re�ues si le timestamp est pr�sent
							bool Hflag=false;
							if (list_Hpixels.nbElem > 0) {
								BalayeListe(list_Hpixels) {
									H33 *Hptr = list_Hpixels.current();
									if (Hptr->t_avant == curGw->TimeAcq) {
										Hflag = true;
										H33 HDeplacement;
										ostringstream stringStream;
										//Calcul de l'homographie de d�placement totale
										if (!TRR.GetHomographieDeplacement(HDeplacement, Hptr, stringStream))
										{
											PrintTexte("\nProbl�me homographie d�placement"); break; //sortie boucle des t�ches
										}
										logWeeding << "\n - Traitement du groupe : " << curGw->TimeAcq << "(n -" << iPile+1 << "), transfo HOMOGRAPHIE \n";
										if (logFlag) {
											logWeeding << "\n   Matrice d�placement avec " << curGw->TimeAcq << ": \n";
											logWeeding << stringStream.str(); //on inscrit les matrices du calcul dans le log
										}
										curGw->HDeplacement = HDeplacement;
										curGw->methodeTransfoWeeds = HOMOGRAPHIE;
									}
								}
							}

							
							if (ParamG.prioriteDesherbageGPS) { //le recalage par GPS est prioritaire
								if (desherbageGPSOK) { 
									logWeeding << "\n - Traitement du groupe : " << curGw->TimeAcq << "(n -" << iPile + 1 << "), transfo GPS (prioritaire) \n";
									curGw->methodeTransfoWeeds = GPS;
								}
							}
							else { //correction par GPS si pas d'homographie trouv�e pour ce groupe
								if (!Hflag && desherbageGPSOK) { 
									logWeeding << "\n - Traitement du groupe : " << curGw->TimeAcq << "(n -" << iPile + 1 << "), transfo GPS (en absence d'homographie) \n";
									curGw->methodeTransfoWeeds = GPS;
								}
							}

							if (curGw->methodeTransfoWeeds==NODEF) { //pas de correction possible pour ce groupe
								logWeeding << "\n - Traitement du groupe : " << curGw->TimeAcq << "(n -" << iPile+1 << "), pas de transfo \n";
							}
							else { //si correction, on fait passer les weeds en �tat A_TRAITER si possible
								countGroupeATRAITER = curGw->triggerEtatDesherbage();
								countWeedsATraiter += countGroupeATRAITER;
								logWeeding << "\n   Nombre de mauvaises herbes � traiter dans ce groupe: " << countGroupeATRAITER;
							}
						}
						//On peut alors transformer les coordonn�es dans le rep�re robot actuel
						PrintTexte("Fonction transformation rep�re weeds \n");
						char msgerrTransfo[1000];
						if (!TransfoPileWeeds(msgerrTransfo, PileGroupeWeeds, PosAcq, *pRepere)) {
							PrintTexte("\n%s", msgerrTransfo);  break; //sortie boucle sur liste
						}

						for (PileGroupeWeeds.begin(), iPile = 0; (!PileGroupeWeeds.end() && iPile < ParamG.maxCyclesPile); PileGroupeWeeds++, iPile++)
						{
							TGroupeWeeds *curGw = PileGroupeWeeds.current();
							if (curGw->methodeTransfoWeeds != NODEF) {
								logWeeding << "\n---------------\nTransformations du groupe: \n" << curGw->TimeAcq << "(n -" << iPile + 1 << ")\n";
								logWeeding << "\nCoordonn�es des A TRAITER dans le rep�re robot actuel: \n";
								logWeedList(logWeeding, curGw->ListeWeedsRobotTrans, A_TRAITER);
								logWeeding << "\nCoordonn�es des A TRAITER dans le rep�re bras actuel: \n";
								logWeedList(logWeeding, curGw->ListeWeedsBrasTrans, A_TRAITER);
								logWeeding << "\n---------------\n";
							}
						}

					}

					//Lancement de la tache de weeding s'il y a au moins une mauvaise herbe avec le flag A_TRAITER
					if ((countWeedsATraiter > 0) && (flagWeeding))
					{
						PrintTexte("D�clenchement tache de weeding");
						//d�ploiement tache de weeding, elle devra �tre termin�e pour pouvoir passer au cycle 2
						logWeeding << "Entr�e tache de Weeding pour "<< countWeedsATraiter <<" potentielles mauvaises herbes \n";
						task<TRetourTask> tWeeding = CreateWeeding(&AT, &PileGroupeWeeds, ParamG.HauteTension, ParamG.RecalageManuelBras);
						Cycle1.push_back(tWeeding || tClavier);

						char stw[50];
						//sprintf(stw, "Weeding %d weed(s)", countWeedsATraiter);

						NamesCycle1.push_back(stw);
					}
				}
			}


			/////////////////////
			//attente fin cycle 1
			/////////////////////

			auto var = when_all(begin(Cycle1), end(Cycle1));
			AttenteTacheMsg(var);

			vector<TRetourTask> res = var.get();

			bool OKCycle1 = true;
			PrintTexte("Fin des %i taches", res.size());

			//if (logFlag) { logWeeding << "Taches premi�re partie:\n"; }
			for (int i = 0; i < res.size(); i++) {
				if (res.at(i).State != TRetourState::Completed)
				{
					OKCycle1 = false;
					PrintTexte("\n\ARRET CYCLE1:  %s (Task N�%d: %s)", res.at(i).InfoRetour.data(), i, NamesCycle1[i].obj);
				}
				//R�cup�ration des temps d'ex�cution de chaque tache et inscription au log
			}

			if (OKCycle1)	//tout s'est bien pass�
			{
				
				if (ParamG.DetectionManuelle)
				{
					//on intercale une phase de d�tection manuelle

					while (true)
					{
						POINTINT P;
						if (!pDriverG->DesignePoint(P, "Choisir un point sur l'image live")) break;
						//trac� du point sur l'image
						ImDetectionManuelle->CurseurInverse(P.x, P.y, 300);
						//conversion dans le rep�re du robot
						POINTFLT PM = CalageManuel->Pixels2Metres(POINTFLT(P.x * CoeffCalageManuel.x, P.y * CoeffCalageManuel.y));
						TWeedRobot TR(PM, -1, -1, NONE);
						GW.ListeWeedsRobot.push_back(TR);
						

						if (pDriverG->GetImageActive() != dynamic_cast<IMTRAITIM*>(ImDetectionManuelle))
						{
							Message("D�signer un point sur l'image live !!!");
							continue;
						}

					}
				} //fin d�tection manuelle
				DBOUT("Acquisition nombre weeds: " << GW.ListeWeedsRobot.nbElem)
			}


			if (!OKCycle1)	//erreur ou arr�t utilisateur
			{

				if (ParamG.AvancementAuto || ParamG.AvancementManuel)	TModulePlateforme::Stop();

				if (ParamG.AcquisitionRikola)	ModuleRikola.Stop();
				if (ParamG.AcquisitionSony || ParamG.AcquisitionSonySD)		TModuleSony::Stop();


				//attente arr�t effectif des t�ches en cours

				///////////////////if (ParamG.Weeding)		tWeeding.wait();
				if (ParamG.AcquisitionRikola)		TotaleAcquisitionTraitementRikola.wait();
				if (ParamG.AcquisitionSony || ParamG.AcquisitionSonySD)		TotaleAcquisitionTraitementSony.wait();

				//arr�t t�che affiche
				ctArretClavier.cancel();
				tAffiche.wait();

				AT.ListeValide = false;
				break;
			}




			///////////////////
			//lancement cycle 2
			///////////////////
			
			//le cycle 2 concerne l'avancement et la fin des traitements commenc�s en fin d'acquisition
			//il faut donc faire avant un op�rateur || entre chaque t�che et la t�che clavier 

			PrintTexte("\nLancement partie B cycle...\n");

			std::list<task<TRetourTask>> Cycle2; //avant et/ou pendant mouvement
			_list<char *> NamesCycle2; //pour retrouver le nom en cas d'arr�t



			// avancement plateforme

			bool CropFinished = false;

			task<TRetourTask> tStep;

			if (ParamG.AvancementManuel)
			{
				PrintTexte("Lancement tache avancement manuel\n");
				logWeeding << "Entr�e tache de d�placement manuel\n";
				tStep = CreateStepManuel(&AT, this);
				Cycle2.push_back(tStep || tClavier);
				NamesCycle2.push_back("Avancement manuel");
			}

			if (ParamG.AvancementAuto)
			{
				if (navigationGPSOK) {
					PrintTexte("Lancement tache avancement automatique\n");
					tStep = CreateStepAuto(&AT, Trajet, ParamG.step, &CropFinished);
					Cycle2.push_back(tStep || tClavier);
					NamesCycle2.push_back("Avancement auto");
				}
				else {
					//Note: le robot va r�p�ter des cycles � l'arr�t � l'infini dans ce cas
					//Il faudrait g�rer le probl�me des d�tections multiples que �a engrange en th�orie
					PrintTexte("Impossible de lancer la t�che d'avancement automatique, �tat GPS insuffisant\n");
				}

			}

			//si il y a des d�tections pr�vues, on devra attendre leur fin avant de repasser au cycle 1
			if (ParamG.DetectionRikola)
			{
				Cycle2.push_back(TotaleAcquisitionTraitementRikola || tClavier);
				NamesCycle2.push_back("D�tection Rikola");
			}

			if (ParamG.AcquisitionSony) //download Sony ou d�tection
			{
				Cycle2.push_back(TotaleAcquisitionTraitementSony || tClavier);
				NamesCycle2.push_back("Download Sony");
			}



			///////////////////
			//attente fin cycle 2
			///////////////////


			if (!Cycle2.empty())
			{
				auto var2 = when_all(begin(Cycle2), end(Cycle2));
				AttenteTacheMsg(var2);
				vector<TRetourTask> res2 = var2.get();

				bool OKCycle2 = true;

				PrintTexte("Fin des %i taches", res2.size());
				//if (logFlag) { logWeeding << "Taches seconde partie:\n"; }
				for (int i = 0; i < res2.size(); i++) {
					if (res2.at(i).State != TRetourState::Completed)
					{
						OKCycle2 = false;

						PrintTexte("\n\ ARRET CYCLE 2: %s (Task N�%d: %s)", res2.at(i).InfoRetour.data(), i, NamesCycle2[i].obj);
					}
				}

				if (!OKCycle2)	//erreur ou arr�t utilisateur

				{
					if (ParamG.AvancementAuto || ParamG.AvancementManuel)
						TModulePlateforme::Stop();	//il y a eu demande d'avancement auto

					if (ParamG.DetectionRikola)		ModuleRikola.Stop();
					if (ParamG.AcquisitionSony)		TModuleSony::Stop();


					//attente arr�t effectif des t�ches en cours

					/////////////////if (ParamG.AcquisitionRikola || ParamG.AcquisitionSony)		tStep.wait();
					if (ParamG.DetectionRikola) TotaleAcquisitionTraitementRikola.wait();
					if (ParamG.AcquisitionSony)		TotaleAcquisitionTraitementSony.wait();

					//arr�t t�che affiche
					ctArretClavier.cancel();
					tAffiche.wait();

					AT.ListeValide = false;
					break;
				}
			}





			PrintTexte("Nombre de weeds d�tect�es: %i \n", GW.ListeWeedsRobot.nbElem);
			if (logFlag) {
				logWeeding << "D�tections: \n";
				logWeedList(logWeeding, GW.ListeWeedsRobot, NONE);
			}


			//Filtrage de la liste de weeds d�tect�e durant ce cycle (seulement s'il y a d�j� eu un autre cycle avant)
			//Les duplicats sont supprim�s selon un param�tre de tol�rance


			if (PileGroupeWeeds.nbElem > 0 && ParamG.FiltreDuplication) {
				if (!FiltreWeedsDupliquees(GW, PileGroupeWeeds, pRepere, PosAcq, ParamG.maxCyclesPile, ParamG.tolDist)) {
					PrintTexte("Erreur dans le filtrage \n");
					break;
				}
				PrintTexte("Nombre de weeds d�tect�es apr�s filtrage duplications: %i \n", GW.ListeWeedsRobot.nbElem);
			}

			//Compte du nombre de weeds trait�es parmi les d�tect�es
			int compteWeedsTraitees = 0;
			int zz = 0;
			DBOUT("Log tableau\n");
			//Double boucle  pour acc�der � chaque mauvaise herbe contenue dans la structure
			//if (logFlag) { logWeeding << "Liste weeds cycles pr�c�dents dans la pile:\n"; }
			for (PileGroupeWeeds.begin(); !PileGroupeWeeds.end(); PileGroupeWeeds++) {
				TGroupeWeeds *curGW = PileGroupeWeeds.current();
				for (curGW->ListeWeedsRobot.begin(), curGW->ListeWeedsRobotTrans.begin(), curGW->ListeWeedsBrasTrans.begin(); !curGW->ListeWeedsRobot.end(), !curGW->ListeWeedsRobotTrans.end(), !curGW->ListeWeedsBrasTrans.end(); curGW->ListeWeedsRobot++, curGW->ListeWeedsRobotTrans++, curGW->ListeWeedsBrasTrans++) {
					TWeedRobot *tempCurWeed = curGW->ListeWeedsRobot.current();
					TWeedRobot *tempCurWeedRobotTrans = curGW->ListeWeedsRobotTrans.current();
					TWeedBras *tempCurWeedBrasTrans = curGW->ListeWeedsBrasTrans.current();
					if (tempCurWeedBrasTrans->state == DONE) { //pour ne pas la compter plusieurs fois
						++compteWeedsTraitees; tempCurWeed->state = DONE_COMPTE; tempCurWeedRobotTrans->state = DONE_COMPTE; tempCurWeedBrasTrans->state = DONE_COMPTE;
					} 
					if (tempCurWeedBrasTrans->state == A_TRAITER) {
						tempCurWeed->state = A_TRAITER; tempCurWeedRobotTrans->state = A_TRAITER;
					}
					//if (logFlag) {
					//	if (curGW->methodeTransfoWeeds == NODEF) {
					//		logWeeding << "ID= " << ++zz << ", T= " << curGW->TimeAcq << ", FLAG= " << tempCurWeed->state << ", MET= " << curGW->methodeTransfoWeeds << ", xROB= " << tempCurWeed->Position.x << ", yROB= " << tempCurWeed->Position.y << ", xROBt= ---, yROBt= ---,  xBRASt= ---,  yBRASt= --- \n";
					//	}
					//	else {
					//		logWeeding << "ID= " << ++zz << ", T= " << curGW->TimeAcq << ", FLAG= " << tempCurWeed->state << ", MET= " << curGW->methodeTransfoWeeds << ", xROB= " << tempCurWeed->Position.x << ", yROB= " << tempCurWeed->Position.y << ", xROBt= " << tempCurWeedRobotTrans->Position.x << ", y= " << tempCurWeedRobotTrans->Position.y << ", xBRASt= " << tempCurWeedBrasTrans->Position.x << ", yBRASt= " << tempCurWeedBrasTrans->Position.y << "\n";
					//	}
					//}
				}
			}

			//Ajout du groupe en cours au DEBUT de la liste globale (on peut ainsi facilement acc�der aux n derniers �l�ments)
			//Les weeds de cette pile n'ont bien s�r pas encore �t� marqu�es pour d�sherbage (flag NONE)
			//========================
			PileGroupeWeeds.push_front(GW);
			DBOUT("taille pile et composition apr�s push_back:" << PileGroupeWeeds.nbElem);
			if (PileGroupeWeeds.nbElem > ParamG.maxCyclesPile) {
				//On enl�ve le dernier �l�ment du tableau pour rester � ParamG.maxCyclesPile �l�ments
				int cnt_ATRAITE = 0;
				int cnt_DONE = 0;
				logWeeding << "Suppression du dernier groupe de la pile: " << PileGroupeWeeds.back() ->TimeAcq<<"\n";
				logWeeding << "Nombre �l�ment dans pile: " << PileGroupeWeeds.nbElem << "\n";
;				for (PileGroupeWeeds.back()->ListeWeedsRobot.begin(); !PileGroupeWeeds.back()->ListeWeedsRobot.end(); PileGroupeWeeds.back()->ListeWeedsRobot++) {
					TWeedRobot * pWeed=PileGroupeWeeds.back()->ListeWeedsRobot.current();
					switch (pWeed->state) {
						case WeedState::A_TRAITER: cnt_ATRAITE++;
						case WeedState::DONE_COMPTE: cnt_DONE++;
					}
				}
				logWeeding << "Weeds trait�es dans le groupe avant �limination: " << cnt_DONE << "\n";
				logWeeding << "Weeds non trait�es dans le groupe avant �limination: " << cnt_ATRAITE << "\n";
				PileGroupeWeeds.pop_back();
				DBOUT("taille pile et composition apr�s pop_back:" << PileGroupeWeeds.nbElem);

			}

			//========================

			totalWeedsDetectees += GW.ListeWeedsRobot.nbElem;
			PrintTexte("Log weeding\n");
			//log de r�sum�
			if (logFlag) {
				logWeeding << "\nR�sum� cycle:\n";
				logWeeding << "Lattitude: " << GW.PosAcq.LatDeg << "  Longitude: " << GW.PosAcq.LonDeg << "  Cap" << GW.CapDegAcq << "  Timestamp: " << GW.TimeAcq << "\n";
				logWeeding << "Nombre de mauvaises herbes d�tect�es dans ce cycles (apr�s filtrage): " << GW.ListeWeedsRobot.nbElem << "\n";
				logWeeding << "Nombre de mauvaises herbes trait�es dans ce cycle: "  << compteWeedsTraitees << "\n";
				totalWeedsTraitees += compteWeedsTraitees;
				logWeeding << "Nombre TOTAL de mauvaises herbes d�tect�es: " << totalWeedsDetectees << "\n";
				logWeeding << "Nombre TOTAL de mauvaises herbes trait�es: " << totalWeedsTraitees << "\n";
				logWeeding << "\n============================\n============================\n";
			}

			//On r�initialise ensuite l'enum de m�thode du groupe de mauvaises herbes, on ne pourra plus utiliser les listes transform�es tant qu'une nouvelle transformation n'est pas trouv�e
			for (PileGroupeWeeds.begin(); !PileGroupeWeeds.end(); PileGroupeWeeds++) {
				TGroupeWeeds *curGroupWeedptr = PileGroupeWeeds.current();
				curGroupWeedptr->methodeTransfoWeeds = NODEF;
			}


			if (CropFinished)
			{
				PrintTexte("FIN DU TRAJET EN MODE AUTOMATIQUE");
				AT.ListeValide = false;
				break;
			}

			//Ecriture des donn�es dans le fichier sans fermer et reouvrir les fichiers
			logWeeding.flush();

			

			PrintTexte("Passage au cycle suivant \n");
		} //while (true)

		//fermeture des logs
		if (logWeeding.is_open()) { logWeeding.close(); }


		//Nettoyage des allocations manuelles
		if (CalageManuel)	delete CalageManuel;
		if (Trajet)	delete Trajet;
	}
	catch (exception &e) { //Gestion d'exception g�n�rale
		//Abandon de la s�quence
		PrintTexte("\nERREUR GLOBALE SUPERVISEUR:\n");
		PrintTexte(e.what());
		PrintTexte("\nFermeture du superviseur"); 
		//NB: les logs passent hors de port�e et le destructeur appelle automatiquement close()
		return;
	}
}
