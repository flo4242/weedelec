#pragma once
#include "ppltasks.h"  //� mettre en t�te

#include "WeedelecCommon.h"
#include "AfficheTaches.h"

#include "ExportModulePlantNet.h" 
#include "ExportModuleSony.h" 
#include "ExportModuleRikola.h"
#include "ExportModuleControleRobot.h"
#include "ExportModuleRecalageVisuel.h"
#include "GereWeeds.h"

using namespace Concurrency;

/***************  DEFINITION DES TACHES ****************/

//toutes les t�ches ont pour retour TRetourTask

//******** t�ches de monitoring *********/

//cr�e une t�che qui ne se termine que si le clavier a �t� appuy�
//utilis�e en parall�le de toutes les autres t�ches
task<TRetourTask> CreateTaskTestClavier(unsigned int period, int *pcntTimer);
	
//cr�e une t�che d'affichage de l'�tat des t�ches courantes
task<TRetourTask> CreateTaskAffiche(TAffichageTaches *pAffiche, unsigned int period, cancellation_token &ctExtern);

//****** t�che tempo d'usage g�n�ral *********/
task<TRetourTask> CreateTacheTempo(TAffichageTaches *pAffiche, int delaiMs, char *NomTache);


//******** t�ches d'acquisition et traitement d'image *********/
// NB: la position d'acquisition du robot peut �tre fournie sur demande pour toutes les t�ches d'acquisition
// (pour information et validation OPEROSE)


//******** t�ches Sony *********/

task<TRetourTask> CreateAcquisitionSony(TAffichageTaches *pAffiche);


task<TRetourTask> CreateDownloadSony(TAffichageTaches *pAffiche, string nomImage, bool AvecGeo, double LatDeg, double LonDeg);
//NB: Geotagging non g�r� pour l'instant !!!!!!!!!!!!!!!!!!!!!!!!!

/*
//t�che enchainant acquisition (plateforme � l'arr�t) + download (plateforme �ventuellement en mouvement)
//l'event en argument permet d'informer de la fin de l'acquisition (� faire robot arr�t�)
task<TRetourTask> CreateTaskTotaleSony(TAffichageTaches *pAffiche, task_completion_event<TRetourTask> finacquisition,
	bool AvecDownload, bool TimeOutAccepte, bool AvecGeo, double LatDeg, double LonDeg);*/

task<TRetourTask> CreateTaskTotaleSony(TAffichageTaches *pAffiche, task_completion_event<TRetourTask> finacquisition,
	DRVTRAITIM *pD, _list<TWeedRobot> *pListeWeeds);


//******** t�ches PlantNet *********/

task<TRetourTask> CreateAcquisitionPlantNet(TAffichageTaches *pAffiche, TGroupeWeeds *groupePlantNet);

//******** t�ches Rikola *********/

task<TRetourTask> CreateAcquisitionRikola(TAffichageTaches *pAffiche, TModuleRikola *module, bool AvecGeo, double LatDeg, double LonDeg);
task<TRetourTask> CreateDetectionRikola(TAffichageTaches *pAffiche, string NfImageRaw, _list<TWeedRobot> *pListeWeeds);

//t�che enchainant acquisition (plateforme � l'arr�t) + traitement (plateforme �ventuellement en mouvement)
//l'event en argument permet d'informer de la fin de l'acquisition (� faire robot arr�t�)
task<TRetourTask> CreateTaskTotaleRikola(TAffichageTaches *pAffiche, TModuleRikola *module, task_completion_event<TRetourTask> finacquisition,
	bool avecDetection, bool TimeOutAccepte, _list<TWeedRobot> *pListeWeeds, bool AvecGeo, double LatDeg, double LonDeg);

//******** t�ches Raspberry Homographie *********/
task<TRetourTask> createAcquisitionRaspberryAvant(TAffichageTaches *pAffiche, time_t timestamp_avant);
task<TRetourTask> createAcquisitionRaspberryArriere(TAffichageTaches *pAffiche, time_t timestamp_arriere, _list<H33> *listH, time_t *time_retour);
task<TRetourTask> CreateTaskTotaleHomographie(TAffichageTaches *paffiche, task_completion_event<TRetourTask> finacquisition, time_t timestamp_avant, time_t timestamp_arriere, _list<H33> *listH, time_t *time_retour);

//******** t�ches Plateforme *********/

task<TRetourTask> CreateStepManuel(TAffichageTaches *pAffiche, DRVTRAITIM *pD);


task<TRetourTask> CreateStepAuto(TAffichageTaches *pAffiche, Trajectoire *ptrajectoire, double step, bool *pCropFinished);



task<TRetourTask> CreateWeeding(TAffichageTaches *pAffiche, _list<TGroupeWeeds> *PileGroupeWeeds, bool AvecHauteTension, bool AvecRecalageManuelBras);


