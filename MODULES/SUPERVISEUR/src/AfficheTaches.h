#pragma once

////////// affichage des t�ches ///////////////
#include "ppltasks.h"  //� mettre en t�te

#include "drvtraiti.h"
#include "list_tpl.h"
#include "WeedelecCommon.h"

struct TacheAffichee : public Concurrency::task< TRetourTask>
{
	char Nom[100];

	TacheAffichee() { ; }


	TacheAffichee(char *nom, task< TRetourTask> t)
	{
		task< TRetourTask> *pt = this;
		*pt = t;
		strcpy(Nom, nom);
	}

};

struct TAffichageTaches
{
	IMTGRIS * ImTaches;
	_list< TacheAffichee> Liste;
	bool ListeValide;
	int *pCntTimer; //pour affichage. Pointeur car mise � jour externe (timer)
	int TickDebutCycle;

	TAffichageTaches(IMTGRIS *im, int *pcnttimer, int tickdebut)
		: ImTaches(im), pCntTimer(pcnttimer), ListeValide(false)
	{
		ImTaches->Indestructible = true;
		Clear();
	}

	~TAffichageTaches() { ImTaches->Indestructible = false; }

	void AjouteTache(Concurrency::task< TRetourTask> t, char *NomTache); //ajoute � la liste

	void AfficheTaches();

	void Clear()
	{
		ListeValide = false;
		Liste.clear();
		ImTaches->Efface(0);
		ImTaches->Redessine();

		TickDebutCycle = GetTickCount();
	}

};

