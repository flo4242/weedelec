#include "Labellisation.h"


void Proba2Label(BUFIMG<float> *probaCulture, BUFIMG<float> *probaWeed, IMGRIS *ImLabels)
{
	//actuellement, simple seuillage des probas. A terme, pourra contenir des traitements spatiaux

	for(int y =0; y < probaCulture->dimY; y++)
		for (int x = 0; x < probaCulture->dimX; x++)
		{
			float pC = probaCulture->Tab[y][x];
			float pW = probaWeed->Tab[y][x];
			float pS = 1 - pC - pW;


			int Label = pC > pW ? (pC > pS ? TypeLabel::Culture : TypeLabel::Sol) : (pW > pS ? TypeLabel::Weed : TypeLabel::Sol);
			ImLabels->EcritPixel(x, y, Label);
		}


}

//surcharge IMTGRIS
void Proba2Label(IMTGRIS *probaCulture, IMTGRIS *probaWeed, IMGRIS *ImLabels)
{
	for (int y = 0; y < probaCulture->dimY; y++)
		for (int x = 0; x < probaCulture->dimX; x++)
		{
			float pC = probaCulture->LitPixel(y,x);
			float pW = probaWeed->LitPixel(y, x);
			float pS = 1 - pC - pW;


			int Label = pC > pW ? (pC > pS ? TypeLabel::Culture : TypeLabel::Sol) : (pW > pS ? TypeLabel::Weed : TypeLabel::Sol);
			ImLabels->EcritPixel(x, y, Label);
		}


}