#include <strstream>
#include "FRikolaDrone.h"                                 

#include "winutil2.h"
#include "DBOUT.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

extern "C"
{
   int _EXPORT_ Open_T_RikolaDrone(DRVTRAITIM *pD,HMODULE);
   HMENU _EXPORT_ Ajoute_T_RikolaDrone(HMENU pere);
   int _EXPORT_ Close_T_RikolaDrone(DRVTRAITIM *);
  }


DRVTRAITIM *pDriverG;



int _EXPORT_ Open_T_RikolaDrone(DRVTRAITIM *pD, HMODULE)
/***********************************************/
{
   if(pD->SizeClass!=sizeof(DRVTRAITIM))
   {
      char st[100];
      sprintf(st, "taille DRVTRAITIM non compatible - Driver: %d  DLL: %d",
              pD->SizeClass, sizeof(DRVTRAITIM));
      ::MessageBox(NULL, st, "T_RikolaDrone", MB_OK);
      return 0;
   }
   pDriverG = pD; return 1;
}

int _EXPORT_ Close_T_RikolaDrone(DRVTRAITIM *) { return 1;}
/**************************************/


HMENU _EXPORT_ Ajoute_T_RikolaDrone(HMENU pere)
/*******************************/
{
HMENU HRikolaDrone = pDriverG->AjouteSousMenu(pere, "RikolaDrone",
			
			NULL);

HMENU HCarteCF = pDriverG->AjouteSousMenu(HRikolaDrone, "Carte CF",
	"Lecture donn�es carte SD", &TAvecRikolaDrone::CMCarteCF,
	NULL);

HMENU HTraitement = pDriverG->AjouteSousMenu(HRikolaDrone, "Traitement images",
	"Param�tres de traitement", &TAvecRikolaDrone::CMParamTraitement,
	"Traitement image active", &TAvecRikolaDrone::CMTraitementActive,
	"Traitement fichier UF", &TAvecRikolaDrone::CMTraitementFichierUF,
	"Traitement batch dossier images", &TAvecRikolaDrone::CMTraitementBatch,
	"Traitement pipeline dossier images", &TAvecRikolaDrone::CMTraitementBatchPipeline,
	NULL);


HMENU HDetectionRangs = pDriverG->AjouteSousMenu(HRikolaDrone, "D�tection des rangs",
	"Param�tres Hough", &TAvecRikolaDrone::CMParamHough,
	"Param�tres Filtre Max", &TAvecRikolaDrone::CMParamMaxFilter,
	"Chargement image proba", &TAvecRikolaDrone::CMLoadImageProba,
	"Cr�ation accumulateur", &TAvecRikolaDrone::CMInitAcc,
	"Affichage points acc dans image", &TAvecRikolaDrone::CMAffichageLignes,
	"Recherche des maxima (m�thode GR)", &TAvecRikolaDrone::CMGetMaximaHoughGR,
	"Recherche des maxima (m�thode FR)", &TAvecRikolaDrone::CMGetMaximaHoughFR,
	"Migration/fusion des maxima", &TAvecRikolaDrone::CMMigrationMaximaHough,
	"Analyse profils de ligne", &TAvecRikolaDrone::CMAnalyseProfils,
	"Destruction ImProba", &TAvecRikolaDrone::CMDetruitImageProba,
	 NULL);



return HRikolaDrone;
}


