#pragma once

#include "ligne.h"
#include "bufimtpl.h"  



class TAccumulateurHough
{



	/*
	Pour rappel, chaque point de l'accumulateur repr�sente une droite dans l'image associ�e.

	Toute droite de l'image est repr�sent�e par l'�quation:   (M - C).V = rho
	o�:
	- M = (x,y)
	- C est le centre de l'image
	- V = (cos theta, sin theta) est un vecteur unitaire perpendiculaire � la droite
	- rho repr�sente la distance de la droite au centre de l'image

	Pour faciliter des op�ration de filtrage spatial ou recherche de maxima sur l'accumulateur, celui-ci est centr� sur le type de droites que l'on va
	rechercher en priorit� dans l'image, � savoir des droites d'orientation verticale (x=constante).
	Pour cela:
	- l'abscisse theta est d�finie de -90 � +90� inclus (0� au centre de l'accumulateur --> droite verticale)
	- l'ordonn�e rho est d�finie pour de -rhomax � +rhomax inclus (rho =0 au centre de l'accumulateur --> droite passant par le centre de l'image)

	*/

protected:

	float rhomax; //distance maximale entre une droite repr�sent�e et le centre de l'image: d�pen de la taille de l'image
	int dimTheta; //dimX de l'accumulateur, repr�sentant des angles de -90 � +90� inclus
	int dimRho;  //dimY de l'accumulateur, repr�sentant des valeurs rho de -rhomax � +rhomax inclus


	//image associ�e
	int dimX, dimY;
	POINTFLT imageCenter;

public:
	bool Initialise;


	BUFIMG<float>	*AccProba;
	BUFIMG<int>		*AccNbPoints;
	//ATTENTION! les bufimg sont des pointeurs pour permettre la persistance de visualisation apr�s destruction de TAccumulateurHough
	//En contrepartie, leur destruction devra �tre assur�e par le programme utilisateur


	TAccumulateurHough() : Initialise(false), AccProba(NULL), AccNbPoints(NULL) { ; }


	void Init(int dimXimage, int dimYimage, int dimXTheta, int dimYrho)
	{
		dimX = dimXimage;
		dimY = dimYimage;
		dimTheta = dimXTheta + dimXTheta % 2 + 1; //arrondi au nombre impair sup�rieur pour avoir un centre entier et autant de part et d'autre
		dimRho = dimYrho + dimYrho % 2 + 1; //arrondi au nombre impair sup�rieur pour avoir un centre entier et autant de part et d'autre

		rhomax = (sqrt(2.0) / 2) * (dimX > dimY ? dimX : dimY);
		imageCenter = POINTFLT(dimX / 2., dimY / 2.);

		AccProba = new BUFIMG<float>(dimTheta, dimRho);
		AccNbPoints = new BUFIMG<int>(dimTheta, dimRho);

		for (int y = 0; y < dimRho; y++)
		{
			memset(AccProba->Tab[y], 0, dimTheta*sizeof(float));
			memset(AccNbPoints->Tab[y], 0, dimTheta*sizeof(int));
		}

		Initialise = true;
	}



	bool ThetaVersAcc(float thetaRad, int &xtheta)
	{
		if (abs(thetaRad) > M_PI / 2)
		{
			if (abs(thetaRad) - M_PI / 2 < 1E-5)	//peut-�tre d� � impr�cision sur valeur limite 90�
			{
				xtheta = (thetaRad > 0 ? dimTheta : 0);	return true;
			}

			else return false;
		}

		xtheta = (dimTheta / 2) + (dimTheta / 2) * (thetaRad / (M_PI / 2));
		return true;
	}

	bool RhoVersAcc(float rho, int &yrho)
	{
		if (abs(rho) > rhomax) return false;
		yrho = (dimRho / 2) + (dimRho / 2) * (rho / rhomax);
		return true;
	}


	float ThetaFromAcc(int xtheta)
	{
		return  (xtheta - dimTheta / 2) * ((M_PI / 2) / (dimTheta / 2));
	}

	float RhoFromAcc(int yrho)
	{
		return (yrho - dimRho / 2) * (rhomax / (dimRho / 2));
	}


	void AddImagePoint(int x, int y, float proba)
		//ajoute dans l'accumulateur toutes les droites possibles passant par le point (x,y)
	{
		for (int xtheta = 0; xtheta < dimTheta; xtheta++)
		{
			float thetaRad = ThetaFromAcc(xtheta);
			float rho = (x - imageCenter.x)*cos(thetaRad) + (y - imageCenter.y)*sin(thetaRad);
			//NB: produit scalaire de (P(x,y) - imageCenter) avec le vecteur unitaire d'orientation thetaRad

			int yrho;		RhoVersAcc(rho, yrho);

			AccProba->Tab[yrho][xtheta] += proba;
			AccNbPoints->Tab[yrho][xtheta]++;
		}
	}

	POINTINT GetImagePointCurveMax(int x, int y, float angleMaxDeg)
		//retourne la position dans l'accumulateur du maximum le long de la courbe des droites possibles g�n�r�e par le point (x,y)
	{
		int xAccMin, xAccMax;
		ThetaVersAcc(-angleMaxDeg*(M_PI/180), xAccMin);
		ThetaVersAcc(angleMaxDeg * (M_PI / 180), xAccMax);

		float maxacc = 0;
		int xthetamax=0, yrhomax=0;

		for (int xtheta = xAccMin; xtheta <= xAccMax; xtheta++)
		//for (int xtheta = 0; xtheta <= xAccMin; xtheta++)
		{
			float thetaRad = ThetaFromAcc(xtheta);
			float rho = (x - imageCenter.x)*cos(thetaRad) + (y - imageCenter.y)*sin(thetaRad);
			int yrho;		RhoVersAcc(rho, yrho);

			float proba = AccProba->Tab[yrho][xtheta];
			if (proba > maxacc)
			{
				maxacc = proba;
				xthetamax = xtheta;
				yrhomax = yrho;
			}
			
		}

		return POINTINT(xthetamax, yrhomax);
	}



	void Normalise()
	{
		//somme totale des valeurs dans AccProba
		float sum_proba_line = 0;
		for (int i = 0; i <dimRho; i++) {
			for (int j = 0; j < dimTheta; j++) {
				sum_proba_line += AccProba->Tab[i][j];
			}
		}

		if (sum_proba_line == 0)	return; //s�curit�
		//normalisation
		for (int i = 0; i < dimRho; i++) {
			for (int j = 0; j < dimTheta; j++) {
				AccProba->Tab[i][j] /= sum_proba_line;
			}
		}



	};

	void NormaliseMax()
	{
		float valmin, valmax;
		
		AccProba->MinMax(valmin, valmax);

		
		if (valmax == 0)	return; //s�curit�
		//normalisation
		for (int i = 0; i < dimRho; i++) 
			for (int j = 0; j < dimTheta; j++) 
				AccProba->Tab[i][j] /= valmax;
			
		
	};

	LIGNE SetLine(POINTINT PointDansAcc)
	{
		POINTINT P1, P2; //extremit�s de droite � trouver

		float rho = RhoFromAcc(PointDansAcc.y);

		if (PointDansAcc.x == dimTheta / 2) //on �vacue tout de suite le cas theta = 0 (droite verticale)
		{
			int xh = imageCenter.x + rho;
			P1 = POINTINT(xh , 0);
			P2 = POINTINT(xh, dimY-1);
		}

		else if (PointDansAcc.x == 0 || PointDansAcc.x == dimTheta-1) //et le cas theta = +- 90� (droite horizontale)
		{
			int yh = imageCenter.y + rho;
			P1 = POINTINT(0, yh);
			P2 = POINTINT(dimX-1, yh);
		}

		else
		{

			float thetaRad = ThetaFromAcc(PointDansAcc.x);

			//equation de la droite: (M-C)*V = rho; soit: y = Cy + [rho - (x-Cx)*cos]/sin
			//pour x=0:   
			int y0 = imageCenter.y  + (rho + imageCenter.x *cos(thetaRad)) / sin(thetaRad);

			//pour x=dimX: 
			int y1 = imageCenter.y + (rho - (dimX- imageCenter.x) *cos(thetaRad)) / sin(thetaRad);

			P1 = POINTINT(0, y0);
			P2 = POINTINT(dimX, y1);
		}

		LIGNE L(P1, P2);

		//on ne retient que les points dans l'image (facultatif pour TraceLigne, mais plus propre)
		int NbPointsOK = 0;
		for (int i = 0; i < L.NbPoints; i++) if (OKDimxy(dimX, dimY, L[i].x, L[i].y))	NbPointsOK++;

		LIGNE LR(NbPointsOK);
		int j = 0;
		for (int i = 0; i < L.NbPoints; i++) if (OKDimxy(dimX, dimY, L[i].x, L[i].y))	LR[j++] = L[i];

		return LR;

	}

};
