#pragma once                            

#include <ostream>

#include "bufimtpl.h"  
#include "matrice.h"  
#include "ENVI2TPL.h"


#include "regionUFH.h"
#include "regionUFTPL.h"

enum TypeCulture
{
	Mais,
	Haricots
};

//d�finition d'une fonction callback g�n�rique d'affichage dans un traitement
//tir� de TVisNir2
// st1000: cha�ne de caract�res envoy�e par le callback
// niveau: niveau de d�tail (non g�r� pour l'instant)
// pUser: pointeur � usage sp�cifique transmise pr�alablement par la fonction appelant le traitement
// valeur de retour: arr�t demand� si false


#define RegionUFHyperProba RegionUFTPL_proba<TValHyper>


typedef  bool(*FProgression)(char *st1000, int niveau, void*pUser);


//Classe d�riv�e du template de r�gion UF d�fini dans traitim.
//permet d'inclure la notion de probabilit� � une r�gion sans avoir � modifier traitim
template <typename T>
class RegionUFTPL_proba : public RegionUFTPL<T>
{
public:
	std::vector<float> prob_vec;
	RegionUFTPL_proba(RegionUFTPL<T>&objet) : RegionUFTPL<T>(objet) {	}
	RegionUFTPL_proba()	: RegionUFTPL<T>()	{	}
};




struct TraitementHyper
{
	char Nom[100];

	TraitementHyper() { Nom[0] = '\0'; }

	bool SpectreNul(float * TabSpectre, int nbBandes)
	{
		for (int b = 0; b < nbBandes; b++)	if (TabSpectre[b] != 0)	return false;
		return true;
	}
	bool SpectreNul(double * TabSpectre, int nbBandes)
	{
		for (int b = 0; b < nbBandes; b++)	if (TabSpectre[b] != 0)	return false;
		return true;
	}
	virtual bool GereParametres(HWND hwnd, bool charge)=0; 
	virtual bool ChargeModele(TypeCulture type) = 0;

	virtual bool TraitementRegions(TEnsembleRegionsTPL<RegionUFHyper> *ER, BUFIMG<float> *probaCulture, BUFIMG<float> *probaWeed,
		char *msg1000, void *pUser, FProgression fprogression = NULL) = 0;
	virtual bool TraitementBalayage(TImageHyper<float> *srcVeg, BUFIMG<float> *probaCulture, BUFIMG<float> *probaWeed, 
					char *msg1000,	void *pUser, FProgression fprogression=NULL) = 0;
	virtual bool TraitementSpectre(TValHyper &valH, vector<float> &classProb, char *msg1000) = 0;
	//l'image source hyperspectrale est suppos�e ne contenir que de la v�g�tation (sinon, spectre nul)
	// les probas weed et culture sont donc compl�mentaires hors spectres nuls
	//msg1000: utilis� en cas d'erreur (1000 caract�res max)
	//fprogression: utilis� si non nul pour retour d'infos de progression, en liaison avec pUser
};


struct ParamTraitement
{
	TraitementHyper * Traitement;
	TypeCulture Culture;
};


struct ParamUnionFind
{
	bool fDistanceSAM;
	double SeuilEuclidien;
	double Seuilk1k2;
	double SeuilSAMDeg;
	unsigned int surfacemin;
	bool SeuillageSpectresNuls;
	double NormeSpectreMin;
	int IndexMetrique;
	double Seuil;
	TMetriqueSpectre* Metrique;
};

struct ParamBatch
{
	TNomFic DossierIn;
	TNomFic DossierOut;
	bool sortieProbas, sortieDecision, sortieER, sortieResume;
	char extension[100];
	ofstream resume;
};


struct ParamPipeline
{
	int g_imagePipelineLimit = 5;
};



template<typename Func>
double TimedRun(Func test)
{
	LARGE_INTEGER begin, end, freq;
	QueryPerformanceCounter(&begin);
	test();
	QueryPerformanceCounter(&end);
	QueryPerformanceFrequency(&freq);
	double result = (end.QuadPart - begin.QuadPart) / (double)freq.QuadPart;
	return result;
}