#pragma once

#include <agents.h>
#include <concrt.h>
#include <string>
#include <iostream>
#include <fstream>
#include <ppl.h>

#include "PipelineGovernor.h"

#include <chrono>
#include <thread>

bool AffichePipeline(char *st)
{
	//pas d'affichage � faire dans ce callback
	return true;
}
namespace pipeline_hyper
{
	using namespace ::std;
	using namespace ::Concurrency;
	using namespace ::PipelineUtilities;
	using namespace std::this_thread; 
	using namespace std::chrono;

	//Enum d'�tat inclut dans les packages
	enum etatPile { en_cours, fin, erreur };


	//d�finition de la classe template d'habillage des packages avec un �tat
	template  <class T> class WrappedPackage {
	private:
		etatPile etatPackage = en_cours;
		T Package;
	public:
		int id_package;
		std::string nom_package;
		void setEtat(etatPile newEtat) {etatPackage = newEtat;}
		etatPile getEtat() {return etatPackage;	}
		const T& getPackage()  const { return Package; } //r�cup�ration d'une ref const de l'instance
		void wrapPackage(T &newPackage) { Package = newPackage; }
	};




	//Classes de traitement du pipeline d�riuv�es de agent
	class readHyperImage : public agent
	{
	private:
		_list<TNf> m_list_file;
		PipelineGovernor& m_governor;
		ITarget<WrappedPackage<TImageHyperGen*>>& m_HSI_output;
	public:
		readHyperImage(_list<TNf> list_file, PipelineGovernor& governor, ITarget<WrappedPackage<TImageHyperGen*>> &HSI_output) :
			m_list_file(list_file),
			m_governor(governor),
			m_HSI_output(HSI_output)
		{
		}

		void run()
		{
			int nb_elements = m_list_file.nbElem;
			int i_element = 0;
			for (m_list_file.begin(); !m_list_file.end(); m_list_file++)
			{ //la classe agent � la base de la chaine devra �puiser la liste de fichiers � traiter
				i_element++;
				TNf *cur_fichier = m_list_file.current();
				DBOUT("AGENT 1 : reception package : "<< i_element << " - nom: " << cur_fichier->Nf);
				char msg[1000];
				TImageHyperGen *ImD = OuvreImageHyper(cur_fichier->Nf, msg, 80);
				ImD->ChargeEnMemoire(); //les �tage sup�rieurs du pipeline auront la responsabilit� de g�rer le pointeur et la m�moire
				WrappedPackage<TImageHyperGen*> pack;
				pack.wrapPackage(ImD);
				pack.setEtat(i_element == nb_elements ? fin : en_cours);
				pack.id_package = i_element;
				pack.nom_package = cur_fichier->Nf; //conversion implicite en string


				//sleep_until(system_clock::now() + seconds(3));

				m_governor.WaitForAvailablePipelineSlot(); //attendre avant d'envoyer si le pipeline est engorg� (=g_imagePipelineLimit �l�ments)
							   
				asend(m_HSI_output, pack); 
			}
			done(); //il n'y a plus rien � envoyer au pipeline, l'agent a termin�
			DBOUT("AGENT 1: FIN");
		}

	};


	class UnionFindHyperImage : public agent
	{
	private:

		ISource< WrappedPackage<TImageHyperGen*>>& m_HSI_output;
		ITarget< WrappedPackage<TEnsembleRegionsTPL<RegionUFHyper>>>& m_ER_output;
		ParamUnionFind& m_ParamUnionFindG;
	public:
		UnionFindHyperImage(ISource< WrappedPackage<TImageHyperGen*>> &HSI_output, ITarget< WrappedPackage<TEnsembleRegionsTPL<RegionUFHyper>>>& ER_output, ParamUnionFind& ParamUnionFindG) :
			m_HSI_output(HSI_output),
			m_ER_output(ER_output),
			m_ParamUnionFindG(ParamUnionFindG)
		{
		}

		void run()
		{
			while (true)
			{
				WrappedPackage<TImageHyperGen*> inputPackage = receive(m_HSI_output);
				DBOUT("AGENT 2 : reception package " << inputPackage.id_package);
				TImageHyperGen* inputHSI = inputPackage.getPackage();
				IMUFHyper inputHSI_IMU((TImageHyperUF*)inputHSI);

				ParamValHyper ParamUF(inputHSI->H.Bands, m_ParamUnionFindG.Metrique);
				TValHyper::Init(&ParamUF);
				//UNIONFIND
				TEnsembleRegionsTPL<RegionUFHyper> ER;
				if (inputHSI_IMU.Segmente(m_ParamUnionFindG.Seuil, m_ParamUnionFindG.surfacemin, &ER, AffichePipeline, false) < 0) //mode sans verbose pour all�ger la fen�tre de messages
				{
					return;
				}
				DBOUT("AGENT 2 : fin UF package " << inputPackage.id_package << " - nb regions: " << ER.ListeRegions.nbElem);
				WrappedPackage<TEnsembleRegionsTPL<RegionUFHyper>> pack;
				pack.wrapPackage(ER);
				pack.setEtat(inputPackage.getEtat()); //transfert de l'�tat
				pack.id_package = inputPackage.id_package;
				pack.nom_package = inputPackage.nom_package;
				asend(m_ER_output, pack);
				//nettoyage de l'image (ne sera pas plus utilis�e par la suite du pipeline vu qu'on a les r�gions)
				inputHSI->LiberationMemoire();
				if (inputPackage.getEtat() == fin) { //Fin du flux de donn�es pour cet �tage
					break;
				}
			}
			DBOUT("AGENT 2: FIN");
			done();
		}

	};



	class ClassifHyperImage : public agent
	{
	private:
		ISource<WrappedPackage<TEnsembleRegionsTPL<RegionUFHyper>>>& m_ER_input;
		ITarget< WrappedPackage<TEnsembleRegionsTPL<RegionUFHyperProba>>>& m_ER_output;
		ParamTraitement& m_ParamTraitementG;
	public:
		ClassifHyperImage(ISource< WrappedPackage<TEnsembleRegionsTPL<RegionUFHyper>>> &ER_input, ITarget< WrappedPackage<TEnsembleRegionsTPL<RegionUFHyperProba>>>& ER_output, ParamTraitement& ParamTraitementG) :
			m_ER_input(ER_input),
			m_ER_output(ER_output),
			m_ParamTraitementG(ParamTraitementG)
		{
		}
		void run()
		{
			while (true)
			{
				WrappedPackage<TEnsembleRegionsTPL<RegionUFHyper>> inputPackage = receive(m_ER_input);
				DBOUT("AGENT 3 : reception package " << inputPackage.id_package);
				TEnsembleRegionsTPL<RegionUFHyper> ER = inputPackage.getPackage();
				TEnsembleRegionsTPL<RegionUFHyperProba> ER_proba(ER.dimX, ER.dimY);

				//CLASSIFICATION
				char msg[1000];
				for (ER.ListeRegions.begin(); !ER.ListeRegions.end(); ER.ListeRegions++)
				{
					_list_cell< RegionUFHyper> *cell = ER.ListeRegions.curPos;
					RegionUFHyper &R = cell->obj;
					RegionUFHyperProba R_proba(R);
					TValHyper & valH = R.Valeur;
					if ((R.NbPixels < (ER.dimX*ER.dimY) / 2) & (!m_ParamTraitementG.Traitement->SpectreNul(valH.Tab, valH.DimTab))) {
						vector<float> proba;
						if (!m_ParamTraitementG.Traitement->TraitementSpectre(valH, proba, msg))
						{
							return;
						}
						R_proba.prob_vec.push_back(proba[0]);
						R_proba.prob_vec.push_back(proba[1]);
					}
					ER_proba.ListeRegions.push_back(R_proba);
				}

				
				
				WrappedPackage<TEnsembleRegionsTPL<RegionUFHyperProba>> pack;
				pack.wrapPackage(ER_proba);
					

				pack.setEtat(inputPackage.getEtat()); //transfert de l'�tat
				pack.id_package = inputPackage.id_package;
				pack.nom_package = inputPackage.nom_package;

				//sleep_until(system_clock::now() + seconds(3));

				asend(m_ER_output, pack);
				if (inputPackage.getEtat() == fin) { //Fin du flux de donn�es pour cet �tage
					break;
				}
			}
			DBOUT("AGENT 3: FIN");
			done();
		}


	};


	class SaveResultsHyperImage : public agent
	{
	private:
		ISource< WrappedPackage<TEnsembleRegionsTPL<RegionUFHyperProba>>>& m_ER_input;
		PipelineGovernor& m_governor;
		ParamBatch& m_ParamBatchG;
		TAvecRikolaDrone* m_pDriver;
	public:
		SaveResultsHyperImage(PipelineGovernor& governor, ISource< WrappedPackage<TEnsembleRegionsTPL<RegionUFHyperProba>>>& ER_input, ParamBatch& ParamBatchG, TAvecRikolaDrone *pDriver) :
			m_governor(governor),
			m_ER_input(ER_input),
			m_ParamBatchG(ParamBatchG),
			m_pDriver(pDriver)
		{
		}
		void run()
		{
			char msg[1000];
			int compte_culture_all = 0;
			float surface_weed_all = 0;
			int compte_images = 0;
			while (true) {
				WrappedPackage<TEnsembleRegionsTPL<RegionUFHyperProba>> inputPackage = receive(m_ER_input);
				TEnsembleRegionsTPL<RegionUFHyperProba> ER_input = inputPackage.getPackage();

			
				string img_name = inputPackage.nom_package;
				DBOUT("AGENT 4: Reception package : " << inputPackage.id_package << " ,nombre regions:" << ER_input.ListeRegions.nbElem << " , nom image: " << img_name);
				size_t last_slash_idx = img_name.find_last_of("\\/");
				if (std::string::npos != last_slash_idx)
				{
					img_name.erase(0, last_slash_idx + 1);
				}
				size_t period_idx = img_name.rfind('.');
				if (std::string::npos != period_idx)
				{
					img_name.erase(period_idx);
				}
				char *Nf_noext = &img_name[0]; //c++ 11 (sinon risque de manquer le NULL de fin)

				IMTGRIS *ProbaCulture = m_pDriver->AlloueImageGris("sortie_culture", ER_input.dimX, ER_input.dimY);
				ProbaCulture->CouleurSeuilHaut = 0;ProbaCulture->Seuille();
				IMTGRIS *ProbaWeed = m_pDriver->AlloueImageGris("sortie_weed", ER_input.dimX, ER_input.dimY);
				ProbaWeed->CouleurSeuilHaut = 0; ProbaWeed->Seuille();
				IMTGRIS *DecisionCulture = m_pDriver->AlloueImageGris("decision_culture", ER_input.dimX, ER_input.dimY);
				DecisionCulture->CouleurSeuilHaut = 0; DecisionCulture->Seuille();
				IMTGRIS *DecisionWeed = m_pDriver->AlloueImageGris("decision_weed", ER_input.dimX, ER_input.dimY);
				DecisionWeed->CouleurSeuilHaut = 0; DecisionWeed->Seuille();

				int compte_culture = 0;
				float surface_weed = 0;
				for (ER_input.ListeRegions.begin(); !ER_input.ListeRegions.end(); ER_input.ListeRegions++)
				{
					_list_cell< RegionUFHyperProba> *cell = ER_input.ListeRegions.curPos;
					RegionUFHyperProba &R = cell->obj;
					if (!R.prob_vec.empty()) {
						if (m_ParamBatchG.sortieDecision || m_ParamBatchG.sortieProbas) {
							BalayeListe(R.Segments) //On remplit la r�gion en bouclant sur les segments
							{
								SEGMENT *pS = R.Segments.current();
								for (int x = pS->xg; x <= pS->xd; x++) {
									ProbaCulture->EcritPixel(x, pS->y, R.prob_vec[0] * 255);
									ProbaWeed->EcritPixel(x, pS->y, R.prob_vec[1] * 255);
									DecisionCulture->EcritPixel(x, pS->y, (R.prob_vec[0] > 0.5) * 255);
									DecisionWeed->EcritPixel(x, pS->y, (R.prob_vec[1] > 0.5) * 255);
								}
							}
						}
						if (R.prob_vec[0] > 0.5)
						{
							compte_culture++;
						}
						else {
							surface_weed += R.NbPixels;
						}
					}
				}

				if (m_ParamBatchG.sortieResume) {
					m_ParamBatchG.resume << "Image " << img_name << "\n";
					m_ParamBatchG.resume << "Nombre de r�gions culture: " << compte_culture << "\n";
					m_ParamBatchG.resume << "Taux de pr�sence weeds (pourcent): " << surface_weed / ((float)ER_input.dimX*(float)ER_input.dimY) * 100 << "\n";
					m_ParamBatchG.resume << "===========\n";
				}

				//Enregistrement d'images en sortie si souhait�
				if (m_ParamBatchG.sortieProbas) {
					TNomFic NfOutProbaCulture;
					sprintf(NfOutProbaCulture, "%s\\%s_culture.png", m_ParamBatchG.DossierOut, Nf_noext);
					if (!DoSauveImageViaOpenCV(m_pDriver, ProbaCulture, NfOutProbaCulture, msg)) {
						m_pDriver->Message("Echec sauvegarde: %s", NfOutProbaCulture);
						return;
					}

					TNomFic NfOutProbaWeeds;
					sprintf(NfOutProbaWeeds, "%s\\%s_weed.png", m_ParamBatchG.DossierOut, Nf_noext);
					if (!DoSauveImageViaOpenCV(m_pDriver, ProbaWeed, NfOutProbaWeeds, msg)) {
						m_pDriver->Message("Echec sauvegarde: %s", NfOutProbaWeeds);
						return;
					}
				}
				if (m_ParamBatchG.sortieDecision) {
					TNomFic NfOutDecisionCulture;
					sprintf(NfOutDecisionCulture, "%s\\%s_culture_d.png", m_ParamBatchG.DossierOut, Nf_noext);
					if (!DoSauveImageViaOpenCV(m_pDriver, DecisionCulture, NfOutDecisionCulture, msg)) {
						m_pDriver->Message("Echec sauvegarde: %s", NfOutDecisionCulture);
						return;
					}

					TNomFic NfOutDecisionWeeds;
					sprintf(NfOutDecisionWeeds, "%s\\%s_weed_d.png", m_ParamBatchG.DossierOut, Nf_noext);
					if (!DoSauveImageViaOpenCV(m_pDriver, DecisionWeed, NfOutDecisionWeeds, msg)) {
						m_pDriver->Message("Echec sauvegarde: %s", NfOutDecisionWeeds);
						return;
					}
				}
				//nettoyage des images
				m_pDriver->DetruitImage(ProbaCulture);
				m_pDriver->DetruitImage(ProbaWeed);
				m_pDriver->DetruitImage(DecisionWeed);
				m_pDriver->DetruitImage(DecisionCulture);
				if (m_ParamBatchG.sortieER) {
					TNomFic NfOutER;
					sprintf(NfOutER, "%s\\%s_regions.txt", m_ParamBatchG.DossierOut, Nf_noext);
					ER_input.Sauve(NfOutER);
				}
				compte_images++;
				compte_culture_all += compte_culture;
				surface_weed_all += surface_weed / ((float)ER_input.dimX*(float)ER_input.dimY) * 100;
				if (inputPackage.getEtat() == fin) { //Fin du flux de donn�es pour cet �tage
					break;
				}

				//sleep_until(system_clock::now() + seconds(3));

				m_governor.FreePipelineSlot(); //indique au gouverneur qu'une partie du pipeline est lib�r�e
			}
			//sortie du bilan une fois que le batch est termin�
			if (m_ParamBatchG.sortieResume) {
				m_ParamBatchG.resume << "BILAN: \n";
				m_ParamBatchG.resume << "Nombre de r�gions culture: " << compte_culture_all << "\n";
				m_ParamBatchG.resume << "Taux de pr�sence weeds (pourcent): " << surface_weed_all / compte_images << "\n";
				m_ParamBatchG.resume << "===========\n";
			}
			if (m_ParamBatchG.resume.is_open()) { m_ParamBatchG.resume.close(); }
			DBOUT("AGENT 4: FIN");


			done();
		}


	};


}