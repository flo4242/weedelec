#include <chrono> //� mettre en t�te

#include "FRikolaDrone.h"                                 
#include "winutil2.h"
#include "commtype.h"
#include <ctype.h>
#include <stdlib.h>
#include "TraitementImages.h"
#include "ObjVisHyper.h"
#include "Labellisation.h"
#include "DBOUT.h"

#include "EnsembleRegions.h"

#include "PasserelleTraitimOpenCV.h"
#include "imufh.h"

//Pipeline de traitement d'images en parall�le
#include "Pipeline.h"
#include "PipelineGovernor.h"

#include "ImTemplate.h"


using namespace ::std;
using namespace ::pipeline_hyper;
using namespace ::PipelineUtilities;

//membres statiques
int TValHyper::DimTab;
TMetriqueSpectre * TValHyper::Metrique;


double RegionUFHyper::Seuil;




bool SaveProba(TImageHyper<float> *ImH, BUFIMG<float> *proba, char *msg1000)
{
	int dimX = proba->dimX;
	int dimY = proba->dimY;	//facilit� �criture


	float *data = new float[dimX*dimY];

	for (int y = 0; y < dimY; y++)
		for (int x = 0; x < dimX; x++)
			data[dimX* y + x] = proba->Tab[y][x];

	TNomFic NfProba; strcpy(NfProba, ImH->NomComplet);
	RemplacementExtension(NfProba, "proba");

	bool ret = SauveImageTIF((unsigned char*)data, dimX, dimY, sizeof(float) * 8, NfProba, msg1000);
	delete data;
	return ret;
}

BUFIMG<float> * LoadProba(char* Nf, char *msg1000)
{

	int dimx, dimy, type, bitsperpixel, nbCanaux, orientation = 0;

	if (!LitTailleImageTIF(Nf, dimx, dimy, type, bitsperpixel, nbCanaux, orientation, msg1000))	return NULL;

	if (type != NB || bitsperpixel != 8 * sizeof(float) || nbCanaux != 1)
	{
		sprintf(msg1000, "Image proba non conforme\n");
		sprintf(msg1000 + strlen(msg1000), "type: %d;  bits/pixel: %d; Nb canaux: %d\n", type, bitsperpixel, nbCanaux);
		sprintf(msg1000 + strlen(msg1000), "(attendu: type 0; 32 bits/pixel ; 1 canal)");
		return NULL;
	}

	float *buffer = new float[dimx*dimy];

	int OrientationImposee = 0;

	if (!ChargeImageTIF((unsigned char *)buffer, Nf, msg1000, orientation))
	{
		delete buffer;
		return false;
	}

	BUFIMG<float> *Proba = new BUFIMG<float>(dimx, dimy);

	for (int y = 0; y < dimy; y++) {
		for (int x = 0; x < dimx; x++) {
			Proba->Tab[y][x] = buffer[dimx* y + x];
		}
	}
	delete buffer;
	return Proba;
}



bool LitValeur(TImageHyperGen * im, int x, int y, TValHyper &Val)
/**********************************************************************/
{
	if (Val.Tab == NULL) Val.Tab = new double[TValHyper::DimTab]; //allou� � la premi�re lecture image
	return im->LitSpectre(x, y, Val.Tab);
}

bool EcritValeur(TImageHyperGen * im, int x, int y, TValHyper &Val)
/**********************************************************************/
{
	return im->EcritSpectre(x, y, Val.Tab);
}

#include "THyper2.h"
#include "regionUFTPL.h"






#define LABEL_SOL		0
#define LABEL_CULTURE	1
#define LABEL_WEED		2

//AJOUTER ICI LES FONCTIONS DE CONSTRUCTION DES TRAITEMENTS DISPOS 
extern TraitementHyper* GetTraitement_PLSGlobale();
extern TraitementHyper* GetTraitement_SVM();
extern TraitementHyper* GetTraitement_LDA();



struct ListeTraitements
{
	_list< TraitementHyper *> Liste;

	ListeTraitements() //AJOUTER ICI LES TRAITEMENTS DISPOS 
	{
		Liste.push_back(GetTraitement_PLSGlobale());
		Liste.push_back(GetTraitement_SVM());
		Liste.push_back(GetTraitement_LDA());
	}

	~ListeTraitements()	{		BalayeListe(Liste)		delete *Liste.current(); 	}

} TraitementsDisposG;



ParamTraitement ParamTraitementG;
ParamUnionFind ParamUnionFindG;
ParamBatch ParamBatchG;
ParamPipeline ParamPipelineG;

bool TAvecRikolaDrone::GereParametresUnionFind(HWND hwnd, bool charge)
{
	char *RubriquesMetriques[] = { "Euclidienne", "SAM", "k1k2 (r�sidu de S2 = aS1 + b)", NULL };
	GROUPE G_UF(HWindow(), "UF Hyper", "HyperUF.cfg",
		MULCHOIX("Choix m�trique", ParamUnionFindG.IndexMetrique, RubriquesMetriques),
		DBL("Seuil distance euclidienne", ParamUnionFindG.SeuilEuclidien, 0.001, 100000000),
		DBL("Seuil distance SAM (degr�s)", ParamUnionFindG.SeuilSAMDeg, 0.001, 180),
		DBL("Seuil distance k1k2", ParamUnionFindG.Seuilk1k2, 0.001, 100000000),
		DBL("Norme minimale SAM", MetriqueSAM->NormeMin, 0.001, 100000),
		ENTU32("Surface minimale des r�gions", ParamUnionFindG.surfacemin, 1, 1000000),
		PBOOL("Seuillage spectres quasi-nuls", ParamUnionFindG.SeuillageSpectresNuls),
		DBL("Seuil norme spectres nuls", ParamUnionFindG.NormeSpectreMin, 0, 1000),
		NULL);
	if (!G_UF.Gere())	return false;

	switch (ParamUnionFindG.IndexMetrique)
	{
	case 0: ParamUnionFindG.Metrique = MetriqueRMS; ParamUnionFindG.Seuil = ParamUnionFindG.SeuilEuclidien; break;
	case 1: ParamUnionFindG.Metrique = MetriqueSAM;  ParamUnionFindG.Seuil = ParamUnionFindG.SeuilSAMDeg * M_PI / 180; break;
	case 2: ParamUnionFindG.Metrique = Metriquek1k2; ParamUnionFindG.Seuil = ParamUnionFindG.Seuilk1k2; break;
	}
	return true;
}

bool TAvecRikolaDrone::GereParametresBatch(HWND hwnd, bool charge)
{
	GROUPE G_batch(HWindow(), "Batch classification binaire", "hyper_weed_classif_batch.cfg",
		REPI("Dossier images hyper", ParamBatchG.DossierIn),
		REPI("Dossier de sortie", ParamBatchG.DossierOut),
		STR("Extension", ParamBatchG.extension),
		PBOOL("Sortie des cartes de proba", ParamBatchG.sortieProbas),
		PBOOL("Sortie des cartes de d�cision", ParamBatchG.sortieDecision),
		PBOOL("Sortie fichiers ER (avec probas)", ParamBatchG.sortieER),
		PBOOL("Sortie fichier ER r�sum� infestation", ParamBatchG.sortieResume),
		NULL);
	if (!G_batch.Gere()) return false;
	return true;
}

bool TAvecRikolaDrone::GereParametresPipeline(HWND hwnd, bool charge)
{
	GROUPE G_pipeline(HWindow(), "Batch classification binaire", "hyper_weed_classif_pipeline.cfg",
		ENT("Nombre max images dans le pipeline", ParamPipelineG.g_imagePipelineLimit,1,100),
		NULL);
	if (!G_pipeline.Gere()) return false;
	return true;
}

bool TAvecRikolaDrone::GereParametresTraitement(HWND hwnd, bool charge)
{
	//choix du type de traitement
	_list< TraitementHyper *> &ListeT = TraitementsDisposG.Liste; //facilit� �criture
	int NbT= ListeT.nbElem;
	char** rubriquesT = new char*[NbT + 1];
	TraitementsDisposG.Liste.begin();
	for (int i = 0; i < NbT; i++)
	{
		TraitementHyper *T = *ListeT.current();
		rubriquesT[i] = T->Nom;
		ListeT++;
	}
	rubriquesT[NbT] = NULL;
	int IndexT = -1;

	//choix du type de culture
	char* rubriquesC[] = { "Ma�s", "Haricots", NULL };
	int IndexC = -1;


	GROUPE Param(hwnd, "Param�tres", "TraitementRikolaDrone.cfg",
		MULCHOIX("Traitement", IndexT, rubriquesT),
		MULCHOIX("Culture", IndexC, rubriquesC),
		NULL);

	
	bool ret;

	do
	{
		ret = (charge ? Param.Charge() : Param.Gere());
		if (IndexT == -1) { charge = false;	 ::MessageBox(hwnd, "D�finir obligatoirement un type de traitement", "", MB_OK); }
		if (IndexC == -1) { charge = false;	 ::MessageBox(hwnd, "D�finir obligatoirement un type de culture", "", MB_OK); }
	} while (IndexT == -1 || IndexC == -1);

	
	ParamTraitementG.Traitement = ListeT[IndexT].obj;
	ParamTraitementG.Culture = (IndexC == 0 ? TypeCulture::Mais : TypeCulture::Haricots);
		
	delete[] rubriquesT;


	//chargement des param�tres pour le traitement choisi:
	if (ret) 	ret = ParamTraitementG.Traitement->GereParametres(hwnd, charge);


	return ret;
}




void TAvecRikolaDrone::CMParamTraitement()
{
	GereParametresTraitement(HWindow(), false);

}



TImageHyper<float> * TAvecRikolaDrone::GetImageActiveReflectance()
{

	TImageHyperGen * ImH = ImageHyperActive(this); if (!ImH)	return NULL;
	TImageHyper<float> * ImHF = dynamic_cast<TImageHyper<float>*>(ImH);
	if (!ImHF)
	{
		Message("L'image active n'est pas une IHS de type float");
		return NULL;
	}

	/*bool OK = strcmp(ChaineExt(ImHF->NomComplet), "ref") == 0;
	OK |= strcmp(ChaineExt(ImHF->NomComplet), "veg") == 0;
	if (!OK)
	{
		Message("L'image active n'est pas une image de v�g�tation");
		return NULL;
	}*/

	return ImHF;
}


bool ProgressionTraitement(char *st1000, int niveau, void*pUser)
{
	DRVTRAITIM *pD = (DRVTRAITIM *)pUser;

	pD->PrintTexte(st1000);

	if (pD->TestCliqueGauche())
		if (pD->MessageOuiNon("Arr�ter le traitement "))
			return false;

	return true;

}

bool Affiche(char *st)
{
	//pas d'affichage � faire dans ce callback
	return true;
}


void Proba2Label(BUFIMG<float> *probaCulture, IMGRIS *ImLabels)
{




}


void TAvecRikolaDrone::CMTraitementFichierUF()
{
	//Chargement de l'ensemble de r�gions
	TNomFic NfRegions = "";
	if (!ChoixFichierOuvrir("Fichier de r�gions", NfRegions, "txt", "DossierRegions.cfg")) return;

	TEnsembleRegionsTPL<RegionUFHyper> ER;

	if (!ER.Charge(NfRegions))
	{
		Message("Echec chargement:\n%s", NfRegions);
		return;
	}

	DBOUT("taille image: " << ER.dimX << " -- " << ER.dimY);

	if (!GereParametresTraitement(HWindow(), false)) return;

	if (!ParamTraitementG.Traitement->ChargeModele(ParamTraitementG.Culture))
	{
		Message("Echec chargement mod�le %s pour %s", ParamTraitementG.Culture == TypeCulture::Mais ? "Ma�s" : "Haricots",
			ParamTraitementG.Traitement->Nom);
		return;
	}


	OuvreTexte();
	EffaceTexte();

	char msg[1000];


	BUFIMG<float> *ProbaCulture = new BUFIMG<float>(ER.dimX, ER.dimY);
	//allou� sur le tas pour �tre conserv� en visualisation
	BUFIMG<float> ProbaWeed(ER.dimX, ER.dimY);

	if (!ParamTraitementG.Traitement->TraitementRegions(&ER, ProbaCulture, &ProbaWeed, msg, this, ProgressionTraitement))
	{
		Message(msg);
		return;
	}
	//visu proba
	new IMTPLGRIS<float>(this, ProbaCulture, "Proba culture", MVISUPLUS, true, true);


	//visu des labels 

	IMTGRIS *ImLabels = NouvelleImageGris("Image des labels", ER.dimX, ER.dimY);
	ImLabels->EcritAussiSurEcran = false;
	Proba2Label(ProbaCulture, &ProbaWeed, ImLabels);

	//repr�sentation avec palette couleur:

	BYTE R[256], V[256], B[256];
	memset(R, 0, 256);	memset(V, 0, 256);	memset(B, 0, 256);

	R[TypeLabel::Weed] = 255;		//weeds rouges
	V[TypeLabel::Culture] = 255;	//culture verte
	R[TypeLabel::Sol] = V[TypeLabel::Sol] = B[TypeLabel::Sol] = 128; //sol gris

	R[255] = V[255] = B[255] = 255; //pour trac� curseur

	ImLabels->VisuViaPalette(R, V, B);
	ImLabels->Redessine();

	return;

}


void TAvecRikolaDrone::CMTraitementActive()
{

	if (!DesigneImage("D�signer l'image source"))	return;

	TImageHyper<float> *ImH = GetImageActiveReflectance();
	if (!ImH)	return;

	int dimX = ImH->H.Samples;
	int dimY = ImH->H.Lines;

	if (!GereParametresTraitement(HWindow(), false)) return;

	if (!ParamTraitementG.Traitement->ChargeModele(ParamTraitementG.Culture))
	{
		Message("Echec chargement mod�le %s pour %s", ParamTraitementG.Culture == TypeCulture::Mais ? "Ma�s" : "Haricots",
			ParamTraitementG.Traitement->Nom);
		return;
	}

	
	OuvreTexte();
	EffaceTexte();

	char msg[1000];

	BUFIMG<float> *ProbaCulture = new BUFIMG<float>(dimX, dimY);
	//allou� sur le tas pour �tre conserv� en visualisation

	BUFIMG<float> ProbaWeed(dimX, dimY);


	if (!ParamTraitementG.Traitement->TraitementBalayage(ImH, ProbaCulture, &ProbaWeed, msg, this, ProgressionTraitement))
	{
		Message(msg);
		return;
	}

	//visu proba
	new IMTPLGRIS<float>(this, ProbaCulture, "Proba culture", MVISUPLUS, true, true);

	
	//visu des labels 

	IMTGRIS *ImLabels = NouvelleImageGris("Image des labels", dimX, dimY);
	ImLabels->EcritAussiSurEcran = false;
	Proba2Label(ProbaCulture, &ProbaWeed, ImLabels);

	//repr�sentation avec palette couleur:

	BYTE R[256], V[256], B[256];
	memset(R, 0, 256);	memset(V, 0, 256);	memset(B, 0, 256);

	R[TypeLabel::Weed] = 255;		//weeds rouges
	V[TypeLabel::Culture] = 255;	//culture verte
	R[TypeLabel::Sol] = V[TypeLabel::Sol] = B[TypeLabel::Sol] = 128; //sol gris

	R[255] = V[255] = B[255] = 255; //pour trac� curseur

	ImLabels->VisuViaPalette(R, V, B);
	ImLabels->Redessine();

	//sauvegarde image proba
	if (!SaveProba(ImH, ProbaCulture, msg))	Message(msg);
	
}



//Application d'un traitement de classification en boucle sur un dossier d'image
void TAvecRikolaDrone::CMTraitementBatch() {

	//GESTION PARAMETRES BATCH
	if (!GereParametresBatch(HWindow(), false)) return;
	int compte_regions; //compteur r�gions
	int compte_images;
	if (ParamBatchG.sortieResume) {
		TNomFic logFile;
		sprintf(logFile, "%s\\resume.txt", ParamBatchG.DossierOut);
		ParamBatchG.resume.open(logFile);
	}

	//GESTION PARAMETRES UNIONFIND
	if (!GereParametresUnionFind(HWindow(), false)) return;

	//GESTION PARAMETRES TRAITEMENT
	if (!GereParametresTraitement(HWindow(), false)) return;
	if (!ParamTraitementG.Traitement->ChargeModele(ParamTraitementG.Culture))
	{
		Message("Echec chargement mod�le %s pour %s", ParamTraitementG.Culture == TypeCulture::Mais ? "Ma�s" : "Haricots",
			ParamTraitementG.Traitement->Nom);
		return;
	}


	//Mesure du temps d'ex�cution du batch, on capture tout par r�f�rence dans la lambda
	PrintTexte("Debut execution pipeline");
	int result = TimedRun([&]() { TAvecRikolaDrone::DoTraitementBatch(); });
	PrintTexte("Temps execution batch sequentiel: %d", result);



}

void TAvecRikolaDrone::DoTraitementBatch()
{
	OuvreTexte();
	EffaceTexte();
	char msg[1000]; //pour gestion exceptions
	char stProg[1000]; //pour affichage
	_list<TNf> Liste;
	if (!GetListeFichiers(Liste, ParamBatchG.DossierIn, ParamBatchG.extension, false))
	{
		Message("pas d'images � traiter dans %s", ParamBatchG.DossierIn);
		return;
	}

	PrintTexte("Nombre de fichiers � traiter: %d\n", Liste.nbElem);

	//BOUCLE FICHIERS
	int compte_culture_all = 0;
	float surface_weed_all = 0;
	int compte_images = 0;
	BalayeListe(Liste)
	{
		if (ProgressionTraitement)
		{
			sprintf(stProg, "IMAGE %d/%d\n ============== \n", compte_images + 1, Liste.nbElem);

			if (!ProgressionTraitement(stProg, 0, this)) { sprintf(msg, "Arr�t utilisateur");		break; }
		}

		//sleep_until(system_clock::now() + seconds(9));


		char stProg[1000];
		//Ouverture de l'image
		char *Nf = *Liste.current();

		//On isole le nom de fichier sans extension en repassant par un string
		//note: on pourrait le mettre dans une fonction g�n�rique et renvoyer un pointeur
		//vers un tableau allou� avec malloc ? (mais chiant pour nettoyer)
		std::string Nf_str(Nf);
		size_t last_slash_idx = Nf_str.find_last_of("\\/");
		if (std::string::npos != last_slash_idx)
		{
			Nf_str.erase(0, last_slash_idx + 1);
		}
		size_t period_idx = Nf_str.rfind('.');
		if (std::string::npos != period_idx)
		{
			Nf_str.erase(period_idx);
		}
		char *Nf_noext = &Nf_str[0]; //c++ 11 (sinon risque de manquer le NULL de fin)


		TImageHyperGen *ImD = OuvreImageHyper(Nf, msg, 80);
		ImD->ChargeEnMemoire();
		int dimX = ImD->H.Samples;
		int dimY = ImD->H.Lines;
		int NbBandes = ImD->H.Bands;


		//A remplacer par des BUFIMG
		IMTGRIS *ProbaCulture = this->AlloueImageGris("sortie_culture", dimX, dimY);
		IMTGRIS *ProbaWeed = this->AlloueImageGris("sortie_weed", dimX, dimY);
		IMTGRIS *DecisionCulture = this->AlloueImageGris("decision_culture", dimX, dimY);
		IMTGRIS *DecisionWeed = this->AlloueImageGris("decision_weed", dimX, dimY);


		//Application de l'unionfind
		ParamValHyper ParamUF(ImD->H.Bands, ParamUnionFindG.Metrique);
		TValHyper::Init(&ParamUF);
		TEnsembleRegionsTPL<RegionUFHyper> ER;
		IMUFHyper ImUF((TImageHyperUF*)ImD);
		PrintTexte("Debut Unionfind\n");
		if (ImUF.Segmente(ParamUnionFindG.Seuil, ParamUnionFindG.surfacemin, &ER, Affiche, false) < 0) //mode sans verbose pour all�ger la fen�tre de messages
		{
			Message("Arr�t utilisateur");
			return;
		}

		//Boucle sur les r�gions UnionFind
		PrintTexte("Debut classification\n");
		int compte_culture = 0;
		float surface_weed = 0;
		for (ER.ListeRegions.begin(); !ER.ListeRegions.end(); ER.ListeRegions++)
		{

			_list_cell< RegionUFHyper> *cell = ER.ListeRegions.curPos;
			RegionUFHyper &R = cell->obj;
			TValHyper & valH = R.Valeur;
			if ((R.NbPixels < (dimX*dimY) / 2) & (!ParamTraitementG.Traitement->SpectreNul(valH.Tab, valH.DimTab))) {
				vector<float> proba;
				if (!ParamTraitementG.Traitement->TraitementSpectre(valH, proba, msg))
				{
					Message(msg);
					return;
				}
				if (ParamBatchG.sortieDecision || ParamBatchG.sortieProbas) {
					BalayeListe(R.Segments) //On remplit la r�gion en bouclant sur les segments
					{
						SEGMENT *pS = R.Segments.current();
						for (int x = pS->xg; x <= pS->xd; x++) {
							ProbaCulture->EcritPixel(x, pS->y, proba[0] * 255);
							ProbaWeed->EcritPixel(x, pS->y, proba[1] * 255);
							DecisionCulture->EcritPixel(x, pS->y, (proba[0] > 0.5) * 255);
							DecisionWeed->EcritPixel(x, pS->y, (proba[1] > 0.5) * 255);
						}
					}
				}
				if (proba[0] > 0.5)
				{
					compte_culture++;
				}
				else {
					surface_weed += R.NbPixels;
				}
			}
		}

		if (ParamBatchG.sortieResume) {
			ParamBatchG.resume << "Image " << Nf << "\n";
			ParamBatchG.resume << "Nombre de r�gions culture: " << compte_culture << "\n";
			ParamBatchG.resume << "Taux de pr�sence weeds (pourcent): " << surface_weed / ((float)dimX*(float)dimY) * 100 << "\n";
			ParamBatchG.resume << "===========\n";
		}

		//Enregistrement d'images en sortie si souhait�
		if (ParamBatchG.sortieProbas) {
			TNomFic NfOutProbaCulture;
			sprintf(NfOutProbaCulture, "%s\\%s_culture.png", ParamBatchG.DossierOut, Nf_noext);
			if (!DoSauveImageViaOpenCV(this, ProbaCulture, NfOutProbaCulture, msg)) {
				Message("Echec sauvegarde: %s", NfOutProbaCulture);
				return;
			}

			TNomFic NfOutProbaWeeds;
			sprintf(NfOutProbaWeeds, "%s\\%s_weed.png", ParamBatchG.DossierOut, Nf_noext);
			if (!DoSauveImageViaOpenCV(this, ProbaWeed, NfOutProbaWeeds, msg)) {
				Message("Echec sauvegarde: %s", NfOutProbaWeeds);
				return;
			}
		}
		if (ParamBatchG.sortieDecision) {
			TNomFic NfOutDecisionCulture;
			sprintf(NfOutDecisionCulture, "%s\\%s_culture_d.png", ParamBatchG.DossierOut, Nf_noext);
			if (!DoSauveImageViaOpenCV(this, DecisionCulture, NfOutDecisionCulture, msg)) {
				Message("Echec sauvegarde: %s", NfOutDecisionCulture);
				return;
			}

			TNomFic NfOutDecisionWeeds;
			sprintf(NfOutDecisionWeeds, "%s\\%s_weed_d.png", ParamBatchG.DossierOut, Nf_noext);
			if (!DoSauveImageViaOpenCV(this, DecisionWeed, NfOutDecisionWeeds, msg)) {
				Message("Echec sauvegarde: %s", NfOutDecisionWeeds);
				return;
			}
			this->DetruitImage(ProbaCulture);
			this->DetruitImage(ProbaWeed);
			this->DetruitImage(ProbaCulture);
			this->DetruitImage(DecisionCulture);
		}
		if (ParamBatchG.sortieER) {
			TNomFic NfOutER;
			sprintf(NfOutER, "%s\\%s_regions.txt", ParamBatchG.DossierOut, Nf_noext);
			ER.Sauve(NfOutER);
		}
		compte_images++;

		ImD->LiberationMemoire();
		compte_culture_all += compte_culture;
		surface_weed_all += surface_weed / ((float)dimX*(float)dimY) * 100;
	} //fin boucle images

	if (ParamBatchG.sortieResume) {
		ParamBatchG.resume << "BILAN: \n";
		ParamBatchG.resume << "Nombre de r�gions culture: " << compte_culture_all << "\n";
		ParamBatchG.resume << "Taux de pr�sence weeds (pourcent): " << surface_weed_all / compte_images << "\n";
		ParamBatchG.resume << "===========\n";
	}

	if (ParamBatchG.resume.is_open()) { ParamBatchG.resume.close(); }
}

void TAvecRikolaDrone::CMTraitementBatchPipeline() {

	//GESTION PARAMETRES BATCH
	if (!GereParametresBatch(HWindow(), false)) return;
	int compte_regions; //compteur r�gions
	int compte_images;
	if (ParamBatchG.sortieResume) {
		TNomFic logFile;
		sprintf(logFile, "%s\\resume.txt", ParamBatchG.DossierOut);
		ParamBatchG.resume.open(logFile);
	}

	//GESTION PARAMETRES UNIONFIND
	if (!GereParametresUnionFind(HWindow(), false)) return;

	//GESTION PARAMETRES TRAITEMENT
	if (!GereParametresTraitement(HWindow(), false)) return;
	if (!ParamTraitementG.Traitement->ChargeModele(ParamTraitementG.Culture))
	{
		Message("Echec chargement mod�le %s pour %s", ParamTraitementG.Culture == TypeCulture::Mais ? "Ma�s" : "Haricots",
			ParamTraitementG.Traitement->Nom);
		return;
	}

	//GESTION PARAMETRES PIPELINE
	if (!GereParametresPipeline(HWindow(), false)) return;


	//Mesure du temps d'ex�cution du batch, on capture tout par r�f�rence dans la lambda
	PrintTexte("Debut execution pipeline");
	int result=TimedRun([&]() { TAvecRikolaDrone::DoTraitementBatchPipeline(); });
	PrintTexte("Temps execution pipeline: %d", result);
}

void TAvecRikolaDrone::DoTraitementBatchPipeline()
{
	OuvreTexte();
	EffaceTexte();
	char msg[1000]; //pour gestion exceptions
	char stProg[1000]; //pour affichage
	_list<TNf> Liste;
	if (!GetListeFichiers(Liste, ParamBatchG.DossierIn, ParamBatchG.extension, false))
	{
		Message("pas d'images � traiter dans %s", ParamBatchG.DossierIn);
		return;
	}

	PrintTexte("Nombre de fichiers � traiter: %d\n", Liste.nbElem);


	//Utilisation du gouverneur:
	//permet de controler le pipeline pour �viter par exemple qu'une �tape goulot d'�tranglement soit satur�e de demandes
	// Tr�s important ici car l'�tape unionfind couteuse en temps se charge de lib�rer la m�moire des images hyperspectrales !
	PipelineGovernor governor(ParamPipelineG.g_imagePipelineLimit);


	DBOUT("Nombre de fichiers � traiter: " << Liste.nbElem);
	//Buffers de WrappedPackage entre les �tages
	unbounded_buffer<WrappedPackage<TImageHyperGen*>> bufferPackage1;
	unbounded_buffer<WrappedPackage<TEnsembleRegionsTPL<RegionUFHyper>>> bufferPackage2;
	unbounded_buffer<WrappedPackage<TEnsembleRegionsTPL<RegionUFHyperProba>>> bufferPackage3;


	//Cr�ation des agents
	//Le gouverneur intervient � la premi�re et la derni�re �tape
	readHyperImage agent1(Liste, governor, bufferPackage1);
	UnionFindHyperImage agent2(bufferPackage1, bufferPackage2, ParamUnionFindG);
	ClassifHyperImage agent3(bufferPackage2, bufferPackage3, ParamTraitementG);
	SaveResultsHyperImage agent4(governor, bufferPackage3, ParamBatchG, this);

	DBOUT("agents cr��s");

	//On d�marre tout
	agent1.start();
	agent2.start();
	agent3.start();
	agent4.start();
	agent* agents[4] = { &agent1, &agent2, &agent3, &agent4 };

	//On attend qu'ils aient tous fini
	//Rappel: les packages contiennent un enum d'�tat, le dernier fichier trait� par le premier �tage du pipeline aura l'enum fin
	//qui se propagera dans les �tages et entrainera la fin en cascade de tous les agents
	agent::wait_for_all(4, agents);

	DBOUT("FIN PIPELINE");
}


