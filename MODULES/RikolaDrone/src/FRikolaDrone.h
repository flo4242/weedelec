#pragma once

#include "drvtraiti.h" 
#include "ENVI2TPL.h"

struct TAvecRikolaDrone : DRVTRAITIM
	{

	//lecture des images acquises sur carte CF Rikola
	void CMCarteCF()		{		Message("Tout est dans l'extension Rikola de l'outil g�n�ral Hyperspectral...");	}


	//traitement des images en r�flectance (TraitementImages.cpp)

	bool GereParametresTraitement(HWND hwnd, bool charge);
	bool GereParametresUnionFind(HWND hwnd, bool charge);
	bool GereParametresBatch(HWND hwnd, bool charge);
	bool GereParametresPipeline(HWND hwnd, bool charge);

	void CMParamTraitement();

	
	TImageHyper<float> * GetImageActiveReflectance();
	void CMTraitementActive();
	void CMTraitementFichierUF();
	void CMTraitementBatch();
	void DoTraitementBatch();
	void CMTraitementBatchPipeline();
	void DoTraitementBatchPipeline();

	//d�tection rangs

	bool GereParametresHough(HWND hwnd, bool charge);
	bool GereParametresMaxFilter(HWND hwnd, bool charge);
	void DoGetMaximaHough(int methode);// 0: GR 1: FR

	void CMParamMaxFilter();
	void CMParamHough();
	void CMLoadImageProba();
	void CMInitAcc();
	void CMAffichageLignes();
	void CMGetMaximaHoughGR();
	void CMGetMaximaHoughFR();
	void CMMigrationMaximaHough();
	void CMAnalyseProfils();
	void CMDetruitImageProba();
	
	};

//dans TraitementImages.cpp
extern bool SaveProba(TImageHyper<float> *ImH, BUFIMG<float> *proba, char *msg1000);
extern BUFIMG<float> * LoadProba(char* Nf, char *msg1000);
