#pragma once

#include "bufimtpl.h"  
#include "imtraiti.h"
enum TypeLabel
{
	Culture,
	Weed,
	Sol
};

void Proba2Label(BUFIMG<float> *probaCulture, BUFIMG<float> *probaWeed, IMGRIS *ImLabels);
void Proba2Label(IMTGRIS *probaCulture, IMTGRIS *probaWeed, IMGRIS *ImLabels);