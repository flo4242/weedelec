                            

#include "TraitementImages.h"
#include "DBOUT.h"


class PLSGlobale : public TraitementHyper
{

protected:

	TNomFic NfModeleMais, NfModeleHaricots;

	VECTEUR B;
	double b0;

public:

	PLSGlobale() { strcpy(Nom, "traitement_PLS"); NfModeleMais[0] = '\0'; NfModeleHaricots[0] = '\0'; }

	virtual bool GereParametres(HWND hwnd, bool charge);
	virtual bool ChargeModele(TypeCulture type);
	virtual bool TraitementBalayage(TImageHyper<float> *src, BUFIMG<float> *probaCulture, BUFIMG<float> *probaWeed, 
						char *msg1000, void *pUser, FProgression fprogression);
	virtual bool TraitementRegions(TEnsembleRegionsTPL<RegionUFHyper> *ER, BUFIMG<float> *probaCulture, BUFIMG<float> *probaWeed,
		char *msg1000, void *pUser, FProgression fprogression);
	virtual bool TraitementSpectre(TValHyper &valH, vector<float> &classProb, char *msg1000);
};


TraitementHyper* GetTraitement_PLSGlobale() { return new PLSGlobale; }

bool PLSGlobale::GereParametres(HWND hwnd, bool charge)
{
	GROUPE Param(hwnd, "Param�tres", "traitement_PLS.cfg",
		FICH("Mod�le ma�s", NfModeleMais, "txt"),
		FICH("Mod�le haricots", NfModeleHaricots, "txt"),

		NULL);

	Param.NbCharEdition = 100; // utile si saisie de noms de fichiers

	return (charge ? Param.Charge() : Param.Gere());
}





bool PLSGlobale::ChargeModele(TypeCulture type)
{
	MATRICE M;

	bool ret;

	switch (type)
	{
	case TypeCulture::Mais: 		ret = M.Charge(NfModeleMais); break;
	case TypeCulture::Haricots: 	ret = M.Charge(NfModeleHaricots); break;
	default:						ret = false;
	}

	if (!ret)	return false;
	
	B = (M.ExtraitPartielle(0, 0, 1, M.dimC() - 1).Ligne(0));
	b0 = M[0][M.dimC() - 1];

	return true;
}

bool PLSGlobale::TraitementRegions(TEnsembleRegionsTPL<RegionUFHyper> *ER, BUFIMG<float> *probaCulture, BUFIMG<float> *probaWeed,
	char *msg1000, void *pUser, FProgression fprogression) {
	DBOUT("traitement r�gion PLS");
	int NbRegions = ER->ListeRegions.nbElem;
	char stProg[1000];
	double yest;
	int y = 0;
	float proba;
	BalayeListe(ER->ListeRegions)
	{
		if (fprogression)
		{
			sprintf(stProg, "R�gion %d/%d\n", y + 1, ER->ListeRegions.nbElem);

			if (!fprogression(stProg, 0, pUser)) { sprintf(msg1000, "Arr�t utilisateur");		break; }
		}
		_list_cell< RegionUFHyper> *cell = ER->ListeRegions.curPos;
		RegionUFHyper &R = cell->obj;
		TValHyper & valH = R.Valeur;
		if ((R.NbPixels < (ER->dimX*ER->dimY) / 2) & (!SpectreNul(valH.Tab, valH.DimTab))) {
			yest = b0;
			for (int b = 0; b < valH.DimTab; b++) {
				yest += valH.Tab[b] * B[b];
			}
			//conversion en proba
			proba = yest > 1 ? 1 : yest < -1 ? 0 : yest / 2 + 0.5;
			BalayeListe(R.Segments) //On remplit la r�gion en bouclant sur les segments
			{
				SEGMENT *pS = R.Segments.current();
				for (int x = pS->xg; x <= pS->xd; x++) {
					probaCulture->EcritPixel(x, pS->y, proba);
					probaWeed->EcritPixel(x, pS->y, 1 - proba);
				}
			}
		}
		y++;
	}

	return true; 
}

bool PLSGlobale::TraitementBalayage(TImageHyper<float> *src, BUFIMG<float> *probaCulture, BUFIMG<float> *probaWeed,
							char *msg1000, void *pUser, FProgression fprogression)
{
	int dimX = src->H.Samples;
	int dimY = src->H.Lines;
	int NbBandes = src->H.Bands;

	float *TabSpectre = new float[NbBandes];
	char stProg[1000];

	for (int y = 0; y < dimY; y++)
	{
		if (fprogression)
		{
			sprintf(stProg, "Ligne %d/%d\n", y + 1, dimY);

			if (!fprogression(stProg, 0, pUser)) {	sprintf(msg1000, "Arr�t utilisateur");		break; 			}
		}
		

		for (int x = 0; x < dimX; x++)
		{
			double yest = b0;
			src->LitPixel(x, y, TabSpectre);

			if (SpectreNul(TabSpectre, NbBandes))
			{
				probaCulture->Tab[y][x] = 0;
				probaWeed->Tab[y][x] = 0;
				continue;
			}

			for (int b = 0; b < NbBandes; b++)
				yest += TabSpectre[b] * B[b];

			//conversion en proba
			probaCulture->Tab[y][x] = yest > 1 ? 1 : yest < -1 ? 0 : yest/2 + 0.5;
			probaWeed->Tab[y][x] = 1 - probaCulture->Tab[y][x];
			
		}

	}

	delete TabSpectre;

	
	return true; //pas d'erreur possible pour traitement PLSGlobale
}


bool PLSGlobale::TraitementSpectre(TValHyper &valH, vector<float> &classProb, char *msg1000) {
	double yest = b0;
	for (int b = 0; b < valH.DimTab; b++)
		yest += valH.Tab[b] * B[b];
	//conversion en proba
	classProb.push_back( yest > 1 ? 1 : yest < -1 ? 0 : yest / 2 + 0.5);
	classProb.push_back(1 - classProb[0]);
	return true;
}