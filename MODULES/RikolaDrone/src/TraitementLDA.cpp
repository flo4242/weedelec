

#include "TraitementImages.h"
#include "DBOUT.h"
#include <ctype.h>
#include <stdlib.h>


class traitementLDA : public TraitementHyper
{

protected:
	float seuilMais;
	float seuilHaricot;
	float seuil;
	TNomFic NfMatriceReductionMais;
	TNomFic NfMatriceReductionHaricot;
	MATRICE M; //r�duction de dimensionnalit�
public:

	traitementLDA() {
		strcpy(Nom, "traitement_LDA"); NfMatriceReductionMais[0] = '\0'; NfMatriceReductionHaricot[0] = '\0';
	}


	virtual bool GereParametres(HWND hwnd, bool charge);
	virtual bool ChargeModele(TypeCulture type);
	virtual bool TraitementBalayage(TImageHyper<float> *src, BUFIMG<float> *probaCulture, BUFIMG<float> *probaWeed,
		char *msg1000, void *pUser, FProgression fprogression);
	virtual bool TraitementRegions(TEnsembleRegionsTPL<RegionUFHyper> *ER, BUFIMG<float> *probaCulture, BUFIMG<float> *probaWeed,
		char *msg1000, void *pUser, FProgression fprogression);
	virtual bool TraitementSpectre(TValHyper &valH, vector<float> &classProb, char *msg1000);
	bool dimReduc(vector<float> &TabSpectre, float &valReduc, MATRICE &M);
	bool read_txt_mat(const std::string &filename, vector<vector<double>> &out);
};

bool traitementLDA::dimReduc(vector<float> &TabSpectre, float &valReduc, MATRICE &M) {
		valReduc = 0;
		for (int z = 0; z < TabSpectre.size(); z++) { //dimensions de base
			valReduc += M[z][0] * TabSpectre[z];
		}
		return true;
}

TraitementHyper* GetTraitement_LDA() { return new traitementLDA; }


bool traitementLDA::GereParametres(HWND hwnd, bool charge)
{
	GROUPE Param(hwnd, "Param�tres", "traitement_LDA.cfg",
		FICH("Matrice ma�s", NfMatriceReductionMais, "txt"),
		FICH("Matrice haricots", NfMatriceReductionHaricot, "txt"),
		FLT("Seuil ma�s", seuilMais, -8000,8000),
		FLT("Seuil haricots", seuilHaricot, -8000, 8000),
		NULL);

	Param.NbCharEdition = 100; // utile si saisie de noms de fichiers

	return (charge ? Param.Charge() : Param.Gere());
}





bool traitementLDA::ChargeModele(TypeCulture type)
{
	bool ret=true;

	DBOUT("Chargement matrice");
	switch (type)
	{ //utilisation d'open cv pour cr�er le modele
	case TypeCulture::Mais:ret = M.Charge(NfMatriceReductionMais); seuil = seuilMais; break;
	case TypeCulture::Haricots: ret = M.Charge(NfMatriceReductionHaricot); seuil = seuilHaricot;	 break;
	default:						ret = false;
	}

	if (!ret)	return false;

	return true;
}

bool traitementLDA::TraitementRegions(TEnsembleRegionsTPL<RegionUFHyper> *ER, BUFIMG<float> *probaCulture, BUFIMG<float> *probaWeed,
	char *msg1000, void *pUser, FProgression fprogression)
{
	DBOUT("traitement r�gion LDA");
	int NbRegions = ER->ListeRegions.nbElem;
	float LDA_val = 0;
	int decision;
	int y = 0;
	char stProg[1000];
	BalayeListe(ER->ListeRegions)
	{
		if (fprogression)
		{
			sprintf(stProg, "R�gion %d/%d\n", y + 1, ER->ListeRegions.nbElem);

			if (!fprogression(stProg, 0, pUser)) { sprintf(msg1000, "Arr�t utilisateur");		break; }
		}
		_list_cell< RegionUFHyper> *cell = ER->ListeRegions.curPos;
		RegionUFHyper &R = cell->obj;
		TValHyper & valH = R.Valeur;
		if ((R.NbPixels < (ER->dimX*ER->dimY) / 2) & (!SpectreNul(valH.Tab, valH.DimTab))) { 
			//Classification
			std::vector<float> TabSpectreVec(valH.Tab, valH.Tab + valH.DimTab);
			std::vector<float> TabSpectreVecReduc;
			if (!dimReduc(TabSpectreVec, LDA_val, M)) {
				sprintf(msg1000, "Erreur dimReduc");
				return false;
			}
			//Ecriture de la d�cision pour chaque pixel du segment
			LDA_val > seuil ? decision = 0 : decision = 1;
			BalayeListe(R.Segments) //On remplit la r�gion en bouclant sur les segments
			{
				SEGMENT *pS = R.Segments.current();
				for (int x = pS->xg; x <= pS->xd; x++) {
					probaCulture->EcritPixel(x, pS->y, decision);
					probaWeed->EcritPixel(x, pS->y, 1-decision);
				}
			}
		}
		y++;
	}
	return true;
}

bool traitementLDA::TraitementBalayage(TImageHyper<float> *src, BUFIMG<float> *probaCulture, BUFIMG<float> *probaWeed,
	char *msg1000, void *pUser, FProgression fprogression)
{
	DBOUT("seuil: " << seuil);
	int dimX = src->H.Samples;
	int dimY = src->H.Lines;
	int NbBandes = src->H.Bands;
	char stProg[1000];
	vector<vector<float>> cur_spec;
	float *TabSpectre = new float[NbBandes];
	float LDA_val=0;
	for (int y = 0; y < dimY; y++) //Boucle sur les �chantillons
	{
		if (fprogression)
		{
			sprintf(stProg, "Ligne %d/%d\n", y + 1, dimY);

			if (!fprogression(stProg, 0, pUser)) { sprintf(msg1000, "Arr�t utilisateur");		break; }
		}
		for (int x = 0; x < dimX; x++)
		{
			src->LitPixel(x, y, TabSpectre);
			if (SpectreNul(TabSpectre, NbBandes))
			{
				probaCulture->Tab[y][x] = 0;
				probaWeed->Tab[y][x] = 0;
				continue;
			}
			else {
				std::vector<float> TabSpectreVec(TabSpectre, TabSpectre + NbBandes);
				std::vector<float> TabSpectreVecReduc;
				if (!dimReduc(TabSpectreVec, LDA_val, M)) {
					sprintf(msg1000, "Erreur dimReduc");
					return false;
				}
				if (LDA_val > seuil) {
					probaCulture->Tab[y][x] = 0;
					probaWeed->Tab[y][x] = 1;
				}
				else
				{
					probaCulture->Tab[y][x] = 1;
					probaWeed->Tab[y][x] = 0;
				}
			}
		}

	}

	delete[] TabSpectre;

	return true;
}


bool traitementLDA::TraitementSpectre(TValHyper &valH, vector<float> &classProb, char *msg1000) {
	std::vector<float> TabSpectreVec(valH.Tab, valH.Tab + valH.DimTab);
	std::vector<float> TabSpectreVecReduc;
	float LDA_val=0;
	if (!dimReduc(TabSpectreVec, LDA_val, M)) {
		sprintf(msg1000, "Erreur dimReduc");
		return false;
	}
	if (LDA_val > seuil) {
		classProb.push_back(0);
		classProb.push_back(1);
	}
	else
	{
		classProb.push_back(1);
		classProb.push_back(0);
	}
	return true;
}