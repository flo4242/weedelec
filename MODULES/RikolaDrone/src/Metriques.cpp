//#include <stdafx.h>
#include <math.h>
#include "Metriques.h"
#include "commtype.h"
#include "matrice.h"

TMetriqueRMS TRMS;
TMetriqueSAM TSAM;
TMetriquek1k2 Tk1k2;

TMetriqueRMS *MetriqueRMS = &TRMS;
TMetriqueSAM *MetriqueSAM = &TSAM;
TMetriquek1k2 *Metriquek1k2 = &Tk1k2;

double TMetriqueRMS::Distance(double *spectre1, double *spectre2, int dim)
/************************************************************************/
{
double d2=0;
for(int i=0; i<dim; i++)
	d2+= (spectre2[i]-spectre1[i])*(spectre2[i]-spectre1[i]);

return (d2<1E-30 ? 0 : sqrt(d2/dim));
}


double TMetriqueSAM::Distance(double *spectre1, double *spectre2, int dim)
/************************************************************************/
{
double produit = 0;
double norme1 =0;
double norme2 = 0;

for (int i = 0; i <dim; i++)
	{
	norme1 += spectre1[i]*spectre1[i];
	norme2 += spectre2[i]*spectre2[i];
	produit += spectre1[i]*spectre2[i];
	}

double S2 = NormeMin*NormeMin;
norme1 = (norme1 <S2 ? 0 : sqrt(norme1));
norme2 = (norme2 <S2 ? 0 : sqrt(norme2));
if(norme1==0 && norme2==0)	return 0;
if(norme1==0 || norme2==0)	return M_PI;	//distance maxi possible

return acos(produit/(norme1*norme2));
}


double TMetriquek1k2::Distance(double *spectre1, double *spectre2, int dim)
/************************************************************************/
{
MATRICE X(2, dim);
MATRICE Y(1, dim);

for(int j=0; j<dim; j++)
	{
	X[0][j] = spectre1[j];
	X[1][j] = 1;
	Y[0][j] = spectre2[j];
	}

MATCARREE XXT = X*(~X);
MATCARREE INVXXT = XXT.InversionCholesky();

MATRICE phi = Y*(~X)*INVXXT;

//résidu:
double r= (Y-phi*X).Norme();

return (Y-phi*X).Norme();
}