

#include "FRikolaDrone.h"      
#include "Utilfic.h"
#include "ImTemplate.h"
#include "HoughProba.h"
#include "arbtpl2.h"
#include "derich2.h"
#include "list_tpl.h"
#include <deque> //conteneur � double queue pour filtre max
#include "DBOUT.h"
#include <iterator>


struct TParamHough
{
	int dimXAcc, dimYAcc; //dimensions accumulateur
	float SeuilProbaMin; //valeur min pour qu'un point de l'image proba g�n�re un point de droite dans l'accumulateur
	int exposantProba;  //permet d'accentuer les points image de forte probabilit� dans l'accumulateur
	float AngleMaxDeg;	//inclinaison max des droites sur l'image (utilis� pour le filtrage des points maximaux sur l'accumulateur)
	double ConstanteLissage;  //en 1/pixel. 0.1 --> 10 pixels
	double GainGradient;

	TParamHough() : dimXAcc(500), dimYAcc(500), SeuilProbaMin(0.5), exposantProba(1), AngleMaxDeg(10), ConstanteLissage(0.1), GainGradient(10)
	{ ; }
};

struct TParamMaxFilter
{
	int windows_size;
	int facteurToleranceMax;
	TParamMaxFilter() : windows_size(25), facteurToleranceMax(2) { ; }
};


TParamHough ParamHoughG;
TParamMaxFilter ParamMaxFilter;
TAccumulateurHough AccG;
IMTPLGRIS<float> * visuProbaG = NULL; //indispensable car AccG ne continet comme info que le BUFIMG
IMTPLGRIS<float> * visuAccProbaG = NULL; //indispensable car AccG ne continet comme info que le BUFIMG


_list<POINTINT> ListeMaximaG;



bool DoGereParametresMaxFilter(HWND hwnd, TParamMaxFilter &param, bool charge)
{

	GROUPE G(hwnd, "maxFilter", "maxFilter.cfg",
		ENT("Taille de la fen�tre (impair)", param.windows_size, 1, 10000),
		ENT("Facteur limite maximum global/maxima locaux", param.facteurToleranceMax, 1, 100),
		NULL);

	return (charge ? G.Charge() : G.Gere());
}

bool TAvecRikolaDrone::GereParametresMaxFilter(HWND hwnd, bool charge)
{
	return DoGereParametresMaxFilter(hwnd, ParamMaxFilter, charge);
}

void  TAvecRikolaDrone::CMParamMaxFilter()
{
	GereParametresMaxFilter(HWindow(), false);
}

bool DoGereParametresHough(HWND hwnd, TParamHough &param, bool charge)
{

	GROUPE G(hwnd, "Hough", "Hough.cfg",
		ENT("Dim X accumulateur", param.dimXAcc, 1, 10000),
		ENT("Dim Y accumulateur", param.dimYAcc, 1, 10000),
		FLT("Seuil proba image minimale", param.SeuilProbaMin, 0.1, 1),
		ENT("Exposant de renforcement probabilit�", param.exposantProba, 1, 10000),
		FLT("Inclinaison max des droites (�)", param.AngleMaxDeg, 0, 90),
		DBL("Constante lissage pour gradient Deriche (1/pixel)", param.ConstanteLissage, 0.01, 10),
		DBL("Gain gradient Deriche", param.GainGradient, 0.001, 1000),


		NULL);

	return (charge ? G.Charge() : G.Gere());
}

bool TAvecRikolaDrone::GereParametresHough(HWND hwnd, bool charge)
{
	return DoGereParametresHough(hwnd, ParamHoughG, charge);
}


void  TAvecRikolaDrone::CMParamHough()
{
	GereParametresHough(HWindow(), false);
}

void TAvecRikolaDrone::CMLoadImageProba()
{
	char  *titre = "Carte probabilit�s";
	TNomFic nomcomplet = "";
	char *extension = "proba";
	char *fichrep = "DirProba.cfg";

	if (!ChoixFichierOuvrir(titre, nomcomplet, extension, fichrep)) return;

	char errmsg[1000];
	BUFIMG<float> *Proba = LoadProba(nomcomplet, errmsg);
	if (!Proba) { Message(errmsg);		return; }


	//visualisation proba
	char Titre[100];
	sprintf(Titre, "Image proba (%s)", NomSansPath(nomcomplet));

	visuProbaG = new IMTPLGRIS<float>(this, Proba, Titre, MVISUPLUS, true, true, true);

}

IMTPLGRIS<float> * ImageActiveFloat(DRVTRAITIM *pD)
/***********************************************/
{
	IMTRAITIM *im = pD->GetImageActive();
	if (!im) { pD->Message("Pas d'image active");	return NULL; }

	if (!im->pObjVis)	return NULL;
	BUFIMG<float> *pRet0 = dynamic_cast<BUFIMG<float> *>(im->pObjVis);
	IMTPLGRIS<float> *pRet = dynamic_cast<IMTPLGRIS<float> *>(im->pObjVis);
	if (!pRet) { pD->Message("Pas de IMTPLGRIS<float> sous-jacent");	return NULL; }

	return pRet;
}

void  TAvecRikolaDrone::CMInitAcc()
{
	if (!GereParametresHough(HWindow(), false)) return;

	if (!DesigneImage("D�signer l'image de proba"))	return;

	IMTPLGRIS<float> *ImDesigne = ImageActiveFloat(this);
	
	if (!ImDesigne)	return;

	if (visuProbaG)	visuProbaG->Im->Indestructible = false;
	visuProbaG = ImDesigne;
	visuProbaG->Im->Indestructible = true;
	visuProbaG->IncrustationCouleur = true;
	visuProbaG->SetPaletteIncrustation();

	BUFIMG<float> *Proba = visuProbaG->Bufimg; //facilit� �criture
	AccG.Init(Proba->dimX, Proba->dimY, ParamHoughG.dimXAcc, ParamHoughG.dimYAcc);


	for (int y = 0; y < Proba->dimY; y++)
		for (int x = 0; x < Proba->dimX; x++)
		{
			if (Proba->Tab[y][x] < ParamHoughG.SeuilProbaMin)	continue;

			float p = pow(Proba->Tab[y][x], ParamHoughG.exposantProba);

			AccG.AddImagePoint(x, y, p);
		}


	AccG.NormaliseMax();

	//visualisation
	char titreProba[100]; sprintf(titreProba, "Accumulateur proba; exposant:  %d", ParamHoughG.exposantProba);
	visuAccProbaG = new IMTPLGRIS<float>(this, AccG.AccProba, titreProba, MVISUPLUS, true, true, true);
	
	delete AccG.AccNbPoints; //non utilis� actuellement. A modifier si besoin
	//(� d�truire maintenant car impossible apr�s si pas de visu)
	//new IMTPLGRIS<int>(this, AccG.AccNbPoints, "Accumulateur points", MVISUPLUS, true, true);


}

void  TAvecRikolaDrone::CMAffichageLignes()
{
	if (!AccG.Initialise)
	{
		Message("Cr�er d'abord l'accumulateur � partir d'une image Proba !");
		return;
	}

	//NB: visuProbaG est d�j� indestructible

	POINTINT P;
	LIGNE Lprevious;
	bool previous = false;

	while (DesignePoint(P, "Point dans l'image accumulateur ?"))
	{
		//v�rification qu'on a bien point� dans l'image accumulateur
		IMTGRIS *active = ImageActiveGris();
		if (active !=visuAccProbaG->Im)
		{
			Message("Pointer sur Accumulateur proba !!! ");
			continue;
		}

		//effacement �ventuel ligne pr�c�dente
		if (previous)    visuProbaG->EffaceLigne(Lprevious);
		else previous = true;

		LIGNE L = AccG.SetLine(P);

		//trac� nouvelle ligne

		visuProbaG->TraceLigne(L, visuProbaG->Rouge);

		Lprevious = L;
	}

	if (previous)    visuProbaG->EffaceLigne(Lprevious);
	
}


_list<POINTINT> GetMaximaHoughGR(BUFIMG<float> *Proba)
{

	//r�cup�ration des max de la courbe dans l'acc correspondant � chaque point image,
	//et stockage dans arbre binaire pour �viter les doublons
	ARBRE2(POINTINT) Arbre;
	for (int y = 0; y < Proba->dimY; y++)
		for (int x = 0; x < Proba->dimX; x++)
		{
			if (Proba->Tab[y][x] < ParamHoughG.SeuilProbaMin)	continue;
			Arbre.push(AccG.GetImagePointCurveMax(x, y, ParamHoughG.AngleMaxDeg));
		}

	_list<POINTINT> Liste;
	BalayeListe(Arbre)		Liste.push_back(Arbre.Valeur());

	return Liste;
}



/**
* Transposition simple d'un BUFIMG contenant une matrice
* on utilise un BUFIMG temporaire pour contenir les r�sultats
* @param[in] : entree : BUFIMG que l'on souhaite transposer
* @return bool : pas utilis�
*/
template <class T>
bool transposeBufimg(BUFIMG<T> *entree)
{
	BUFIMG<float> temp(entree->dimY, entree->dimX); //image invers�e
	for (unsigned int y = 0; y < entree->dimY; ++y) {
		for (unsigned int x = 0; x < entree->dimX; ++x) {
			temp.EcritPixel(y, x, entree->LitPixel(x, y));
		}
	}
	entree->ReAlloue(entree->dimY, entree->dimX);
	for (unsigned int y = 0; y < temp.dimY; ++y) {
		for (unsigned int x = 0; x < temp.dimX; ++x) {
			entree->Tab[y][x] = temp.Tab[y][x];
		}
	}
	return true;
}


/**
* D�tection de maxima locaux en comparant la matrice originale de l'image avec celle de son filtre maximum
* Pour �tre consid�r� comme un maximum local, un pixel doit avoir la m�me valeur dans les deux images et �tre au dessus d'un seuil donn� (>(vMax / ParamMaxFilter.facteurToleranceMax))
* @param[in] maxFilter : BUFIMG de l'image � traiter � laquelle le filtre maximum a �t� appliqu�
* @param[in] originalImage : BUFIMG de l'image � traiter
* @return list : liste des maxima locaux d�tect�s
*/
_list<POINTINT> getLocalMaxima(BUFIMG<float> *maxFilter, BUFIMG<float> *originalImage) {
	_list<POINTINT> list;
	float vMin;
	float vMax;
	unsigned int facteurToleranceMax = 2;
	maxFilter->MinMax(vMin, vMax);
	for (unsigned int y = 0; y < maxFilter->dimY; ++y) {
		for (unsigned int x = 0; x < maxFilter->dimX; ++x) {
			float curVal = maxFilter->LitPixel(x, y);
			if (originalImage->LitPixel(x, y) == curVal & curVal > (vMax / facteurToleranceMax)) {
				list.push_back(POINTINT(x, y));
			}
		}
	}
	return list;
}

/**
* M�thode d'application de filtre maximum (valeur maximale dans une fen�tre donn�e pour chaque pixel) sur les rang�es d'une image
* Afin de lire le moins de pixels possible, on g�re une queue contenant des indices sur la ligne, sa taille ne d�passe pas window_size. A chaque nouveau pixel lu:
* - On regarde si le nouvel �l�ment lu par readIter dans entree est sup�rieur � l'�l�ment de l'indice queue.back(). Si c'est le cas, on fait un pop_back() et on r�p�te tant que c'est vrai (potentiellement jusqu'� que la queue soit vide)
* - On ajoute l'�l�ment lu � la queue
* - On �crit l'�l�ment correspondant � l'indice queue.front() dans la matrice Resultat � la position writeIter
* - Si l'indice contenu dans front() ne rentre plus dans la fen�tre glissante, on fait un pop_front()
* On r�p�te jusqu'� la fin de la ligne en veillant � ne lire et �crire que quand cela est possible (les indices de lecture et d'�criture sont d�cal�s de (window_size/2) + 1)
* On r�p�te l'op�ration sur chaque ligne de l'image
* @param[in] entree :
* @param[out] resultat :
* @param[in] window_size : taille de la fen�tre glissante
* @return bool : pas utilis� pour le moment
*/
bool maxFilter1D(BUFIMG<float> *entree, BUFIMG<float> *resultat, const unsigned int window_width) {
	_list<POINTINT> Liste;
	std::deque<unsigned int> queue;
	unsigned int radius=(window_width-1)/2;
	for (unsigned int y = 0; y < entree->dimY; ++y)
	{ //BOUCLE LIGNES
		unsigned int readIter = 0; //indice de la valeur lue
		unsigned int writeIter = 0; //indice de la valeur �crite
		unsigned int startIter = 0; //indice du d�but de la queue
		//Remplissage initial de la queue pour les premi�res fen�tres, il n'y a pas d'�criture
		for (; readIter < radius; ++readIter) {
			while ((!queue.empty()) && (entree->LitPixel(queue.back(), y) <= entree->LitPixel(readIter, y))) {
				queue.pop_back();
			}
			queue.push_back(readIter);
		}
		//Remplissage et �criture r�alis�s conjointement
		for (; readIter < entree->dimX; ++readIter, ++writeIter) {
			//Si la valeur lue est sup�rieure ou �gale � ce qui est contenu dans le d�but de la queue, on enl�ve la premi�re valeur et on r�p�te l'op�ration tant que la condition est vrai
			while ((!queue.empty()) && (entree->LitPixel(queue.back(), y) <= entree->LitPixel(readIter, y))) {
				queue.pop_back();
			}
			queue.push_back(readIter);
			resultat->EcritPixel(writeIter, y, entree->LitPixel(queue.front(), y));
			//Si le premier �l�ment de la queue est au d�but de la fen�tre on l'enl�ve
			if (startIter + 1 >= window_width)
			{
				if (queue.front() == startIter) {
					queue.pop_front();
				}
				++startIter;
			}
		}
		//Derni�res valeurs de la ligne: plus de remplissage mais �criture � g�rer
		for (; writeIter < entree->dimX; ++writeIter)
		{
			resultat->EcritPixel(writeIter, y, entree->LitPixel(queue.front(), y));
			if (queue.front() == startIter)
			{
				queue.pop_front();
			}
			++startIter;
		}
		queue.clear();
		} //FIN BOUCLE LIGNES
	return true;
}

/**
* Obtention des maxima locaux sur un accumulateur reli� � une image Proba en appliquant un filtre maximum
* Le filtre est appliqu� en plusieurs �tapes:
* - Application d'un filtre maximum 1D sur toutes les lignes de l'image
* - Transposition de l'image obtenue
* - Application d'un filtre maximum 1D sur toutes les lignes de l'image transpos�e
* - Transposition de l'image obtenue
* - Comparaison de l'image obtenue avec l'image originale de l'accumulateur pour retenir les maxima locaux
* @param[in] Proba : image de proba ayant servi de source au calcul de l'accumulateur (pas utilis� en pratique ici
* @return Liste : liste des maxima locaux
*/
_list<POINTINT> GetMaximaHoughFR(BUFIMG<float> *Proba)
{
	_list<POINTINT> Liste; //Liste de maximas dans l'espace de l'image originale de l'accumulateur
	//buffers interm�diaires
	BUFIMG<float> FilterBuffer_A(AccG.AccProba->dimX, AccG.AccProba->dimY);
	BUFIMG<float> FilterBuffer_B(AccG.AccProba->dimY, AccG.AccProba->dimX); //image invers�e
	//PREMIERE PASSE SUR LES LIGNES
	maxFilter1D(AccG.AccProba, &FilterBuffer_A, ParamMaxFilter.windows_size);
	//SECONDE PASSE SUR LES COLONNES
	transposeBufimg(&FilterBuffer_A); //On retourne l'image pour appliquer le m�me algo sur les colonnes
	maxFilter1D(&FilterBuffer_A, &FilterBuffer_B, ParamMaxFilter.windows_size); //Deuxi�me passe sur les colonnes
	//DETERMINATION DES MAXIMA LOCAUX
	transposeBufimg(&FilterBuffer_B); //on remet l'image filtr�e � l'endroit, pour la comparer � l'accumlateur initial
	Liste = getLocalMaxima(&FilterBuffer_B, AccG.AccProba);
	return Liste;
}



void  TAvecRikolaDrone::DoGetMaximaHough(int methode)
{
	if (!AccG.Initialise)
	{
		Message("Cr�er d'abord l'accumulateur � partir d'une image Proba !");
		return;
	}

	if (ListeMaximaG.nbElem) //effacement des points
	{
		BalayeListe(ListeMaximaG)
		{
			POINTINT *P = ListeMaximaG.current();
			visuAccProbaG->EffaceRond(P->x, P->y, 4);
		}

	}

	ListeMaximaG = (methode == 0 ? GetMaximaHoughGR(visuProbaG->Bufimg) : GetMaximaHoughFR(visuProbaG->Bufimg) );


	Message("%d maxima trouv�s", ListeMaximaG.nbElem);

	//trac� des max 
	visuAccProbaG->Im->EcritAussiSurEcran = false;
	BalayeListe(ListeMaximaG)
	{
		POINTINT *P = ListeMaximaG.current();
		visuAccProbaG->TraceRond(P->x, P->y, 4, visuAccProbaG->Vert);
	}
	visuAccProbaG->Im->EcritAussiSurEcran = true;
	visuAccProbaG->Im->Redessine();
}


void TAvecRikolaDrone::CMGetMaximaHoughGR() {	DoGetMaximaHough(0);  }
void TAvecRikolaDrone::CMGetMaximaHoughFR() { DoGetMaximaHough(1); }





void  TAvecRikolaDrone::CMMigrationMaximaHough()
{


	struct PointMigrant : public POINTFLT
	{
		bool Stable;
		PointMigrant(POINTINT depart) : POINTFLT(depart), Stable(false) { ; }
		PointMigrant() { ; }
	};

	if (!AccG.Initialise)
	{
		Message("Cr�er d'abord l'accumulateur � partir d'une image Proba !");
		return;
	}

	if (ListeMaximaG.nbElem == 0)
	{
		Message("Liste des maxima vide !");
		return;
	}

	// trac� des maxima avant migration
	visuAccProbaG->Im->EcritAussiSurEcran = false;
	BalayeListe(ListeMaximaG)
	{
		POINTINT *P = ListeMaximaG.current();
		visuAccProbaG->TraceRond(P->x, P->y, 4, visuAccProbaG->Bleu);
	}
	visuAccProbaG->Im->EcritAussiSurEcran = true;
	visuAccProbaG->Im->Redessine();

	if (!GereParametresHough(HWindow(), false))	return; //mani�re faire une pause

	// effacement des maxima avant migration (on ne pourra plus apr�s)
	visuAccProbaG->Im->EcritAussiSurEcran = false;
	BalayeListe(ListeMaximaG)
	{
		POINTINT *P = ListeMaximaG.current();
		visuAccProbaG->EffaceRond(P->x, P->y, 4);
	}
	visuAccProbaG->Im->EcritAussiSurEcran = true;



	BUFIMG<float> *Proba = visuProbaG->Bufimg; //facilit� �criture

	//cr�ation images d�riv�es

	BUFIMG<float> Temp(AccG.AccProba->dimX, AccG.AccProba->dimY);

	BUFIMG<float> DX(AccG.AccProba->dimX, AccG.AccProba->dimY);
	Derivee2DX(AccG.AccProba, &DX, &Temp, ParamHoughG.ConstanteLissage, ParamHoughG.GainGradient);
	BUFIMG<float> DY(AccG.AccProba->dimX, AccG.AccProba->dimY);
	Derivee2DY(AccG.AccProba, &DY, &Temp, ParamHoughG.ConstanteLissage, ParamHoughG.GainGradient);

	int NbP = ListeMaximaG.nbElem; //facilit� �criture

	// cr�ation tableau de points migrants

	PointMigrant * TabP = new PointMigrant[NbP];

	{ //pour garder i local
		int i = 0;
		BalayeListe(ListeMaximaG)
		{
			POINTINT *P = ListeMaximaG.current();
			TabP[i++] = PointMigrant(*P);
			
		}
	}

	
	
	//boucle d'�volution

	OuvreTexte();
	EffaceTexte();

	while (true)
	{
		if (PauseSurCliqueGauche())
		{
			delete TabP; return;
		}

		int NbStables = 0;
		for (int i = 0; i < NbP; i++)
		{
			if (TabP[i].Stable)	NbStables++;
		}

		PrintTexte("\nNb points stables: %d//%d", NbStables, NbP);

		if (NbP == NbStables) break;


		//migration selon gradient
		for (int i = 0; i < NbP; i++)
		{
			PointMigrant &P = TabP[i];	//facilit� �criture

			if (!P.Stable)
			{
				POINTFLT Gradient;
				Gradient.x = DX.GetInterpolValue(P.x, P.y, -1E30, 1E30);
				Gradient.y = DY.GetInterpolValue(P.x, P.y, -1E30, 1E30);

				if (Gradient.Norme2() < 1E-5) P.Stable = true;
				else P += Gradient;
			}

		}

		
	}

	//nb de maxima distincts restants
	ARBRE2(POINTINT) Arbre;
	
	for (int i = 0; i < NbP; i++)		Arbre.push(TabP[i]);
	delete TabP;

	
	//Affichage nouveaux maxima et transfert dans liste globale
	visuAccProbaG->Im->EcritAussiSurEcran = false;
	BalayeListe(Arbre)
	{
		POINTINT P = Arbre.Valeur();
		visuAccProbaG->TraceRond(P.x, P.y, 4, visuAccProbaG->Vert);
		ListeMaximaG.push_back(P);
	}
	visuAccProbaG->Im->EcritAussiSurEcran = true;
	visuAccProbaG->Im->Redessine();

	Message("%d maxima sur %d restants apr�s migration", Arbre.NbNoeuds, NbP);


}

void  TAvecRikolaDrone::CMAnalyseProfils()
{
	Message("A faire!");

}

void TAvecRikolaDrone::CMDetruitImageProba()
{
	if (visuProbaG == 0)		return;

	if (!MessageOuiNon("D�truire l'image de proba courante et l'accumulateur associ� ?")) return;

	IMTRAITIM *Im = visuProbaG->Im;
	delete visuProbaG; visuProbaG = NULL;
	AccG.Initialise = false;

	Im->pObjVis = NULL;
	Im->Indestructible = false;
	DetruitImage(Im);


}