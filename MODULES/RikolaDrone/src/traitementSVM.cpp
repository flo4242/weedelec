﻿                            

#include "TraitementImages.h"
#include "DBOUT.h"
//libsvm
//NOTES COMPILATION: ajouter    _CRT_SECURE_NO_WARNINGS   et     _CRT_NONSTDC_NO_DEPRECATE    au préprocesseur
#include "svm.h"
#include <ctype.h>
#include <stdlib.h>


#define Malloc(type,n) (type *)malloc((n)*sizeof(type)) //macro allocation simplifiée



class traitementSVM : public TraitementHyper
{

protected:
	TNomFic NfModeleMaisSVM, NfModeleHaricotsSVM;
	TNomFic NfMatriceReduction;
	TNomFic NfScaling;
	vector<vector<double>> scaling;
	bool dimReducFlag;
	MATRICE M; //réduction de dimensionnalité
	struct svm_parameter param;
	struct svm_problem prob;
	struct svm_model *model;
	struct svm_node *x_space;
public:

	traitementSVM() {
		strcpy(Nom, "traitement_SVM"); NfModeleMaisSVM[0] = '\0'; NfModeleHaricotsSVM[0] = '\0'; NfMatriceReduction[0] = '\0'; NfScaling[0] = '\0';
	}


	virtual bool GereParametres(HWND hwnd, bool charge);
	virtual bool ChargeModele(TypeCulture type);
	virtual bool TraitementBalayage(TImageHyper<float> *src, BUFIMG<float> *probaCulture, BUFIMG<float> *probaWeed,
						char *msg1000, void *pUser, FProgression fprogression);
	virtual bool TraitementRegions(TEnsembleRegionsTPL<RegionUFHyper> *ER, BUFIMG<float> *probaCulture, BUFIMG<float> *probaWeed,
		char *msg1000, void *pUser, FProgression fprogression);
	virtual bool TraitementSpectre(TValHyper &valH, vector<float> &classProb, char *msg1000);
	bool dimReduc(vector<float> &TabSpectre, vector<float> &TabSpectreReduc, MATRICE &M);
	bool read_txt_mat(const std::string &filename, vector<vector<double>> &out);
	bool apply_scaling(vector<float> &data_test, vector<vector<double> > minmax_scaling);
	bool prepare_data(const vector<float> &data, svm_node *x_space, svm_problem prob, bool verbose);
	void print_sample(vector<float> &TabSpectreVec);
};




TraitementHyper* GetTraitement_SVM() { return new traitementSVM; }

bool traitementSVM::prepare_data(const vector<float> &data, svm_node *x_space, svm_problem prob, bool verbose) {

	int sizeOfProblem = 1;
	int elements = data.size();
	//initialisation de toutes les  données
	int j = 0; //compteur sur x_space (=tableau de svm_nodes) => on doit traverser toutes les valeurs pour un échantillon avant de récupérer le suivant
	for (int i = 0; i < prob.l; ++i)
	{
		prob.x[i] = &x_space[j]; //on pointe vers le début d'un des svm_nodes alloué
		for (int k = 0; k < data.size(); ++k, ++j) //le svm_node est rempli
		{
			x_space[j].index = k + 1;
			x_space[j].value = data[k];
		}
		//libsvm demande obligatoirement un indice -1 à la fin d'un svm_node
		x_space[j].index = -1;
		x_space[j].value = 0;
		j++; //marque implictement le passage au svm_node suivant dans la mémoire
	}

	if (verbose) { //pour verif visuelle des labels et des indices
		DBOUT("revue verbose x_space pour verif");
		for (int i = 0; i < prob.l; ++i)
		{
			DBOUT("line " << i);
			for (int k = 0; k < elements; ++k)
			{
				int index = (prob.x[i][k].index);
				double value = (prob.x[i][k].value);
				DBOUT(index << ":" << value);
			}
		}
	}

	return true;
}


//Fonction de lecture d'un fichier contenant une matrice de valeurs (séparées par ",")
bool traitementSVM::read_txt_mat(const std::string &filename, vector<vector<double>> &out)
{
	setlocale(LC_NUMERIC, "en_US.UTF-8");
	string STRING;
	ifstream infile(filename);
	double val;
	if (infile.fail()) {
		DBOUT("Erreur lecture fichier");
		return false;
	}
	else {
		while (!infile.eof()) 
		{
			getline(infile, STRING); 
			if (!STRING.empty()) {
				vector<double> line_values;
				string token;
				istringstream tokenStream(STRING);
				while (std::getline(tokenStream, token, ','))
				{
					val = stod(token);
					line_values.push_back(val);
				}
				out.push_back(line_values);
			}
		}
		infile.close();
		return true;

	}
}

bool traitementSVM::apply_scaling(vector<float> &data_test, vector<vector<double> > minmax_scaling) {
	int n_features = data_test.size();
	for (int i_feat = 0; i_feat < n_features; i_feat++) {
		data_test[i_feat] = (data_test[i_feat] - minmax_scaling[0][i_feat]) / (minmax_scaling[1][i_feat] - minmax_scaling[0][i_feat]);
		if (data_test[i_feat] > 1) data_test[i_feat] = 1;
		if (data_test[i_feat] <0) data_test[i_feat] = 0;
	}
	return true;
}

bool traitementSVM::dimReduc(vector<float> &TabSpectre, vector<float> &TabSpectreReduc, MATRICE &M) {
	for (int k = 0; k < M.dimC(); k++) { //nouvelles dimensions
		TabSpectreReduc.push_back(0);
		for (int z = 0; z < TabSpectre.size(); z++) { //dimensions de base
			TabSpectreReduc.back() += M[z][k] * TabSpectre[z];
		}
	}
	return true;
}

void traitementSVM::print_sample(vector<float> &vec) {
	DBOUT("log vector:");
	for (auto t = vec.begin(); t != vec.end(); ++t) {
		DBOUT("=" << *t);
	}
}

bool traitementSVM::GereParametres(HWND hwnd, bool charge)
{
	GROUPE Param(hwnd, "Paramètres", "traitement_SVM.cfg",
		FICH("Modèle maïs", NfModeleMaisSVM, "txt"),
		FICH("Modèle haricots", NfModeleHaricotsSVM, "txt"),
		PBOOL("Réduction de dimensions", dimReducFlag),
		FICH("Matrice de réduction", NfMatriceReduction, "txt"),
		FICH("Fichier scaling min max", NfScaling, "txt"),
		NULL);

	Param.NbCharEdition = 100; // utile si saisie de noms de fichiers

	return (charge ? Param.Charge() : Param.Gere());
}





bool traitementSVM::ChargeModele(TypeCulture type)
{
	bool ret;
	DBOUT("Chargement matrice reduc");
	if (dimReducFlag) {
		ret = M.Charge(NfMatriceReduction); //matrice de reduction de la dimensionnalité
	}
	DBOUT("Chargement scaling");
	if (!read_txt_mat(NfScaling, scaling)) {
		DBOUT("Chargement scaling echec");
		ret = false;
	}

	DBOUT("Chargement modele");
	switch (type)
	{ //utilisation d'open cv pour créer le modele
		case TypeCulture::Mais: model=svm_load_model(NfModeleMaisSVM); break;
		case TypeCulture::Haricots: model = svm_load_model(NfModeleHaricotsSVM);	 break;
		default:						ret = false;
	}

	if (!ret)	return false;
	
	return true;
}


bool traitementSVM::TraitementRegions(TEnsembleRegionsTPL<RegionUFHyper> *ER, BUFIMG<float> *probaCulture, BUFIMG<float> *probaWeed,
	char *msg1000, void *pUser, FProgression fprogression) {
	DBOUT("traitement région SVM");
	int NbRegions = ER->ListeRegions.nbElem;
	float LDA_val = 0;
	int decision;
	int y = 0;
	char stProg[1000];
	float signedDist;
	prob.l = 1; //les échantillons sont examinés un par un
	vector<vector<float>> cur_spec;
	double ProbVal[2];
	BalayeListe(ER->ListeRegions)
	{
		if (fprogression)
		{
			sprintf(stProg, "Région %d/%d\n", y + 1, ER->ListeRegions.nbElem);

			if (!fprogression(stProg, 0, pUser)) { sprintf(msg1000, "Arrêt utilisateur");		break; }
		}
		_list_cell< RegionUFHyper> *cell = ER->ListeRegions.curPos;
		RegionUFHyper &R = cell->obj;
		TValHyper & valH = R.Valeur;
		if ((R.NbPixels < (ER->dimX*ER->dimY) / 2) & (!SpectreNul(valH.Tab, valH.DimTab))) {
			//Classification
			std::vector<float> TabSpectreVec(valH.Tab, valH.Tab + valH.DimTab);
			//DBOUT("echantillon de base");
			//print_sample(TabSpectreVec);
			std::vector<float> TabSpectreVecReduc;
			if (dimReducFlag) {
				if (!dimReduc(TabSpectreVec, TabSpectreVecReduc, M)) {
					sprintf(msg1000, "Erreur dimReducFlag");
					return false;
				}
			}
			//DBOUT("echantillon après dimreduc");
			//print_sample(TabSpectreVecReduc);
			if (!apply_scaling(TabSpectreVecReduc, scaling)) {
				sprintf(msg1000, "Erreur apply_scaling");
				return false;
			}
			//DBOUT("echantillon après scaling");
			//print_sample(TabSpectreVecReduc);
			//Application du SVM (classes -1 et 1)
			prob.y = Malloc(double, 1);
			prob.x = Malloc(struct svm_node *, 1); //allocation des cases qui contiennent les pointeurs vers un svm_node
			x_space = Malloc(struct svm_node, (TabSpectreVecReduc.size() + 1));
			if (!prepare_data(TabSpectreVecReduc, x_space, prob, true)) {
				sprintf(msg1000, "Erreur prepareData SVM");
				return false;
			}
			try {
				double pred = svm_predict_probability(model, prob.x[0], ProbVal);
			}
			catch (exception &e) {
				sprintf(msg1000, e.what());
				free(prob.y);
				free(prob.x);
				free(x_space);
				return false;
			}

			//Nettoyage
			free(prob.y);
			free(prob.x);
			free(x_space);
			BalayeListe(R.Segments) //On remplit la région en bouclant sur les segments
			{
				SEGMENT *pS = R.Segments.current();
				for (int x = pS->xg; x <= pS->xd; x++) {
					probaCulture->EcritPixel(x, pS->y, ProbVal[1]);
					probaWeed->EcritPixel(x, pS->y, ProbVal[0]);
				}
			}
		}
		y++;
	}



	return true;
 }


bool traitementSVM::TraitementBalayage(TImageHyper<float> *src, BUFIMG<float> *probaCulture, BUFIMG<float> *probaWeed,
							char *msg1000, void *pUser, FProgression fprogression)
{
	prob.l = 1; //les échantillons sont examinés un par un
	int dimX = src->H.Samples;
	int dimY = src->H.Lines;
	int NbBandes = src->H.Bands;
	char stProg[1000];
	float signedDist;
	vector<vector<float>> cur_spec;
	float *TabSpectre = new float[NbBandes];
	double *ProbVal = new double[1];



	for (int y = 0; y < dimY; y++) //Boucle sur les échantillons
	{
		if (fprogression)
		{
			sprintf(stProg, "Ligne %d/%d\n", y + 1, dimY);

			if (!fprogression(stProg, 0, pUser)) { sprintf(msg1000, "Arrêt utilisateur");		break; }
		}
		for (int x = 0; x < dimX; x++)
		{
			src->LitPixel(x, y, TabSpectre);
			if (SpectreNul(TabSpectre, NbBandes))
			{
				probaCulture->Tab[y][x] = 0;
				probaWeed->Tab[y][x] = 0;
				continue;
			}
			else {
				std::vector<float> TabSpectreVec(TabSpectre, TabSpectre + NbBandes);
				//DBOUT("echantillon de base");
				//print_sample(TabSpectreVec);
				std::vector<float> TabSpectreVecReduc;
				if (dimReducFlag) {
					if (!dimReduc(TabSpectreVec, TabSpectreVecReduc, M)) {
						sprintf(msg1000, "Erreur dimReducFlag");
						return false;
					}
				}
				//DBOUT("echantillon après dimreduc");
				//print_sample(TabSpectreVecReduc);
				if (!apply_scaling(TabSpectreVecReduc,scaling)) {
					sprintf(msg1000, "Erreur apply_scaling");
					return false;
				}
				//DBOUT("echantillon après scaling");
				//print_sample(TabSpectreVecReduc);
				//Application du SVM (classes -1 et 1)
				prob.y = Malloc(double, 1); 
				prob.x = Malloc(struct svm_node *, 1); //allocation des cases qui contiennent les pointeurs vers un svm_node
				x_space = Malloc(struct svm_node, (TabSpectreVecReduc.size() + 1));
				if (!prepare_data(TabSpectreVecReduc, x_space, prob,false)) {
					sprintf(msg1000, "Erreur prepareData SVM");
					return false;
				}
				try {
					double pred = svm_predict_probability(model, prob.x[0], ProbVal);
				}
				catch (exception &e) {
					sprintf(msg1000, e.what());
					free(prob.y);
					free(prob.x);
					free(x_space);
					return false;
				}
				probaCulture->Tab[y][x] = ProbVal[1];
				probaWeed->Tab[y][x] = ProbVal[0];
				//Nettoyage
				free(prob.y);
				free(prob.x);
				free(x_space);
			}
		}

	}

	delete[] TabSpectre;
	return true;

	
}


bool traitementSVM::TraitementSpectre(TValHyper &valH, vector<float> &classProb, char *msg1000) {
	
	prob.l = 1; //les échantillons sont examinés un par un
	std::vector<float> TabSpectreVec(valH.Tab, valH.Tab + valH.DimTab);
	std::vector<float> TabSpectreVecReduc;
	if (dimReducFlag) {
		if (!dimReduc(TabSpectreVec, TabSpectreVecReduc, M)) {
			sprintf(msg1000, "Erreur dimReducFlag");
			return false;
		}
	}
	//DBOUT("echantillon après dimreduc");
	//print_sample(TabSpectreVecReduc);
	if (!apply_scaling(TabSpectreVecReduc, scaling)) {
		sprintf(msg1000, "Erreur apply_scaling");
		return false;
	}
	//DBOUT("echantillon après scaling");
	//print_sample(TabSpectreVecReduc);
	//Application du SVM (classes -1 et 1)
	prob.y = Malloc(double, 1);
	prob.x = Malloc(struct svm_node *, 1); //allocation des cases qui contiennent les pointeurs vers un svm_node
	x_space = Malloc(struct svm_node, (TabSpectreVecReduc.size() + 1));
	if (!prepare_data(TabSpectreVecReduc, x_space, prob, false)) {
		sprintf(msg1000, "Erreur prepareData SVM");
		return false;
	}
	try {
		double ProbVal[2];
		double pred = svm_predict_probability(model, prob.x[0], ProbVal);
		classProb.push_back(ProbVal[1]);
		classProb.push_back(ProbVal[0]);
	}
	catch (exception &e) {
		sprintf(msg1000, e.what());
		free(prob.y);
		free(prob.x);
		free(x_space);
		return false;
	}	
	free(prob.y);
	free(prob.x);
	free(x_space);
	

	return true;
}