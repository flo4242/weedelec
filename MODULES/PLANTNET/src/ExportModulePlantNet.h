#pragma once


//fonctions export�es pour utilisation externe (autres modules)

#include <string>
#include "commtype.h"
#include "WeedelecCommon.h"
#include "GereWeeds.h"
#include "imgris.h"
struct TParamPlantNet
{
	char AdresseIP[100];
	char PortIP[10];
	int TimeOutMs;
	TParamPlantNet() : TimeOutMs(1000)
	{
		strcpy(AdresseIP, "localhost");
		strcpy(PortIP, "4567");
	}
};


struct TRetourDetectionPlantNet : public TRetourTask
{
	// InfoRetour : message avec nb de weeds si succ�s (state Completed) , ou sinon description de l'erreur
	_list<TWeedRobot> groupeWeeds;
	int DureeMs;
};

struct _EXPORT_ TModulePlantNet
{
	static void LaunchAcquisition(TRetourDetectionPlantNet &ret);
	static void testfichier(TRetourDetectionPlantNet &ret);
	//static void AttenteDetection(TRetourDetectionPlantNet &ret);
};



