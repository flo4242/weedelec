﻿#include <chrono>
#include "drvtraiti.h"
#include "ExportModulePlantNet.h" //fonctions exportées pour utilisation externe (autres modules)
#include <cpprest/details/basic_types.h>
#include <cpprest/json.h>
#include <cpprest/ws_client.h>
#include <cpprest/http_client.h>
#include <cpprest/filestream.h>
#include "PasserelleTraitimOpenCV.h"
#include "TaskUtil.h"
#include "Calage.h"
#include "GereWeeds.h"
#include "drvtraiti.h"

//Soucis possible cpprest avec la macro U et incompatibilités avec boost
#ifndef _TURN_OFF_PLATFORM_STRING
#define U(x) _XPLATSTR(x)
#endif // !_TURN_OFF_PLATFORM_STRING

#include <iostream>
#include <fstream>
#include <sstream>
#define DBOUT( s )            \
{                             \
   std::ostringstream os_;    \
   os_ << "DEBUG PLANTNET:" << s << "\n";                   \
   OutputDebugString( os_.str().c_str() );  \
}

using namespace utility;                    // Common utilities like string conversions
using namespace web;                        // Common features like URIs.
using namespace web::http;                  // Common HTTP functionality
using namespace web::http::client;          // HTTP client features
using namespace concurrency::streams;       // Asynchronous streams
using namespace std;

DRVTRAITIM *pDriverG; //driver affichage, manipulation d'image

//pour rappel, la classe TPlantNet ne peut contenir aucune variable, seulement des méthodes, car elle n'est jamais instanciée
//toutes les données doivent être globales et peuvent être insérées dans la structure ci-dessous
struct Data 
{
	TParamPlantNet Param; //classe de paramètres unique à ce module
	TParamCalage ParamCalage; //classe de paramètres partagée par plusieurs de modules par l'inclusion de calage.h
} DataG;


bool GereParametres(HWND hwnd, ModeGereParametres modeGereParam, TParamPlantNet &param, TParamCalage &paramCalage, DRVTRAITIM *pD = NULL)
/**********************************************************************************************************/
//pD nécessaire pour la saisie PAR_Region (mode ModeGereParametres::Gere)
{

	GROUPE G(hwnd, "Paramètres acquisition PlantNet", "PlantNet.cfg",
		new PAR_STR_TPL<TYPEGROUPEWIN>("adresse IP", param.AdresseIP, 99),
		new PAR_STR_TPL<TYPEGROUPEWIN>("port IP", param.PortIP, 9),
		ENT("Timeout connexion (ms)", param.TimeOutMs, 100, 50000),
		FICHS("Fichier cibles de calage", paramCalage.FichierCalageCibles, "cfg"),
		FICHS("Fichier de calage caméra", paramCalage.FichierCalage, "cfg"),
		FICHS("Fichier calage LUT", paramCalage.FichierLUT, "cfg"),
		ENT("Surface minimale cibles  (pixels)", paramCalage.SurfaceMin, 5, 50000),
		ENT("Surface maximale cibles (pixels)", paramCalage.SurfaceMax, 5, 50000),
		NULL);

	bool OK;

	switch (modeGereParam)
	{
	case(ModeGereParametres::Charge): OK = G.Charge(); break;
	case(ModeGereParametres::Sauve): OK = G.Sauve(); break;
	case(ModeGereParametres::Gere): OK = G.Gere(false); break; //false pour avoir une saisie de groupe non modale
	}

	return OK;
}


struct TPlantNet : public DRVTRAITIM
{
	void CMParametres();
	void CMAcquisition();
	void CMCalagePlantNet();
};

//fonctions canoniques exportées en tant qu'outil Traitimexportées
extern "C"
{
	int _EXPORT_ Open_T_PlantNet(DRVTRAITIM *pD, HMODULE hinst)
		/*********************************************/
	{
		if (pD->SizeClass != sizeof(DRVTRAITIM))
		{
			char st[100];
			sprintf(st, "taille DRVTRAITIM non compatible - Driver: %d  DLL: %d",
				pD->SizeClass, sizeof(DRVTRAITIM));
			::MessageBox(NULL, st, "T_PlantNet", MB_OK);
			return 0;
		}
		pDriverG = pD; 	 return 1;
	}



	int _EXPORT_ Close_T_PlantNet(DRVTRAITIM *)	{ return 1;}

	HMENU _EXPORT_ Ajoute_T_PlantNet(HMENU pere)
	/********************************/
	{
		HMENU Hprincipal = pDriverG->AjouteSousMenu(pere, "PlantNet",
		"Paramètres", &TPlantNet::CMParametres,
		"Ordre acquisition", &TPlantNet::CMAcquisition,
		"Calage", &TPlantNet::CMCalagePlantNet,
		NULL);

		HWND hwnd = pDriverG->HWindow();
		if (!GereParametres(hwnd, ModeGereParametres::Charge, DataG.Param, DataG.ParamCalage))
			GereParametres(hwnd, ModeGereParametres::Gere, DataG.Param, DataG.ParamCalage, pDriverG);

		return Hprincipal;
	}
}

/***************  FONCTIONS DU MENU ****************/
void TPlantNet::CMParametres() { GereParametres(HWindow(), ModeGereParametres::Gere, DataG.Param, DataG.ParamCalage, this); }

//fonction de lancement d'une acquisition menu, pas très utile mais peut être utilisée pour debug rapidement
void TPlantNet::CMAcquisition() {
	//Appel à la fonction exportée
	TRetourDetectionPlantNet ret;
	TModulePlantNet::LaunchAcquisition(ret);
	//résultats contenus dans ret.groupeWeeds.ListeWeedsRobot
	DBOUT(ret.InfoRetour);
}



//Appel au module de calage, le calage est calculé puis enregistré selon les chemins indiqués dans ParamCalage
void TPlantNet::CMCalagePlantNet()
{
	string err;
	err = CalageGenerique(DataG.ParamCalage); //on passe aux fonctions communes de calage les paramètres
	if (err.empty()) {
		Message("Calage PlantNet terminé avec succès:\n");
	}
	else {
		Message("Echec du calage PlantNet avec l'erreur:\n%s", err);
	}
}


//récupération du champ d'un json, retourne false si échec
bool GetKeyValueJson(web::json::value & data_in, std::string key, web::json::value& keyvalue)
{
	utility::string_t Key = utility::conversions::to_string_t(key);
	if (data_in.has_field(Key))
	{
		keyvalue = data_in.at(Key);
	}
	else
	{
		return false;
	}
	return true;
}



/***************  DEFINITION DES FONCTIONS EXPORTEES DE ExportModulePlantNet.h ****************/
void TModulePlantNet::LaunchAcquisition(TRetourDetectionPlantNet &ret) 
{
	DBOUT("Entrée LaunchAcquisition\n");
	TParamPlantNet Param;
	TParamCalage ParamCalage;
	TGroupeWeeds groupePlantNet;
	web::json::value result, keyvalue_x, keyvalue_y, keyvalue_s, keyvalue_h;
	if (!GereParametres(NULL, ModeGereParametres::Charge, Param, ParamCalage))
	{
		return;
	}
	//Le chargement du calage de la caméra plantnet est indispensable pour pouvoir continuer 
	TCalage2D* Calage = new Trapeze;
	if (!Calage->Charge(ParamCalage.FichierCalage))
	{
		pDriverG->PrintTexte("Erreur chargement de la matrice de calage caméra plantnet");
		ret.InfoRetour = "Echec chargement fichier";
		ret.State = TRetourState::Error;
		delete Calage;
		return;
	}
	else { 
		DBOUT("Calage plantnet chargé \n");
		//création client
		string stdest = "http://" + string(Param.AdresseIP) + ":" + string(Param.PortIP) + "/capture";
		utility::string_t addrdest = utility::conversions::to_string_t(stdest);
		http_client client(addrdest);
		//envoi du post
		http_request request;
		request.set_method(web::http::methods::POST);
		concurrency::cancellation_token_source cts;
		concurrency::task_completion_event<void> tce;
		task<http_response> postRequest = client.request(request, cts.get_token());
		TaskWithTimeoutState State;
		http_response reponse = DoTaskWithTimeout(postRequest, cts, Param.TimeOutMs, State, &tce);
		//Retour s'il y a eu un soucis
		switch (State.state)
		{
		case TaskWithTimeoutState::Timeout:	 ret.InfoRetour = "Timeout";	ret.State = TRetourState::TimeOut; return;
		case TaskWithTimeoutState::ExternallyCancelled:	ret.InfoRetour = "External stop";	ret.State = TRetourState::ExternalStop; return;
		case TaskWithTimeoutState::Error:	ret.InfoRetour = State.ErrorMsg;	ret.State = TRetourState::Error; return;
		}
		ret.InfoRetour = "";	ret.State = TRetourState::Completed;
		//Récupération du json en sortie
		result= reponse.extract_json().get(); //NB: get() pour récupérer le résultat de la tache

		DBOUT("requete POST ok, json obtenu:\n");

		//Récupération du champ adventices
		auto json_adventices = result.at(U("adventices")).as_array();
		for (auto Iter = json_adventices.begin(); Iter != json_adventices.end(); ++Iter)
		{
			auto& json_cur_adventice = *Iter;
			//On récupère les champs
			int x, y, s, h;
			if (json_cur_adventice.has_field(U("x"))) { x = json_cur_adventice.at(U("x")).as_integer(); }
			else { pDriverG->PrintTexte("Erreur récupération du champ obligatoire x"); continue; }
			if (json_cur_adventice.has_field(U("y"))) { y = json_cur_adventice.at(U("y")).as_integer(); }
			else { pDriverG->PrintTexte("Erreur récupération du champ obligatoire y"); continue;
			}
			if (json_cur_adventice.has_field(U("surface"))) { s = json_cur_adventice.at(U("surface")).as_integer(); }
			else { s = 0; }
			if (json_cur_adventice.has_field(U("hauteur"))) { h = json_cur_adventice.at(U("hauteur")).as_integer(); }
			else { h = 0; }
			DBOUT("POINT IMAGE ,x=" << x << " ,y=" << y << "\n");
			//On crèe un POINTFLT que l'on passe dans le repère robot à la volée
			POINTFLT PM = Calage->Pixels2Metres(POINTFLT(x, y));
			DBOUT("POINT ROBOT, x="<< PM.x << " ,y=" << PM.y << "\n");
			//On peut alors l'ajouter au groupe de mauvaises herbes détectées
			ret.groupeWeeds.push_back(TWeedRobot(PM, s, h, NONE));
		}
		DBOUT("toutes les détections ont été ajoutées \n");
		delete Calage; //nettoyage allocation du calage
		DBOUT("nbelemn Weedrobot:" << ret.groupeWeeds.nbElem << " \n");
		pDriverG->PrintTexte("Récupération de %d mauvaises herbes ok \n", ret.groupeWeeds.nbElem);
		ret.State = TRetourState::Completed;
	}
}

