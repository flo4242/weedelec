
#ifndef ___GETPORTSCOM__H
#define ___GETPORTSCOM__H


//#include <Winreg.h>
#include <stdio.h> //pour sprintf
#include "WINUTIL2.h"
#include "GetPortsCOM.h"

//#include "GetListePortsCOM.h"
//#include "GetListeFichiers.h"


struct TParamComGPS
{
	TNomFic DescriptionPort;
	HANDLE HCom;
	TParamComGPS(){ HCom = NULL; DescriptionPort[0] = '\0'; }

	char *GetPort(char *DescriptionPort); //retourne "COMxx"
};


bool GereGroupeConfig(HWND hParent, TParamComGPS &param, bool charge);

#endif