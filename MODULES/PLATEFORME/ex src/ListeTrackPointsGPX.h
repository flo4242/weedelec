#ifndef __LISTETRACKPOINTSGPX__H
#define __LISTETRACKPOINTSGPX__H


#include "list_tpl.h"
#include "TrackPointGPX.h"

class ListeTrackPointGPX
{
public:
	_list <TrackPoint> ListePointGPX;
	TrackPoint PointCourant;

	UINT_PTR pTimer;

	ListeTrackPointGPX(UINT_PTR pT = NULL) { if (pT) pTimer = pT; else pTimer = NULL; }
	~ListeTrackPointGPX() { if (pTimer) KillTimer(NULL, pTimer); }

	void SetPointCourant(double Lat, double Lon, double Vitesse, double Cap, SYSTEMTIME st);

	void StartTimer(int incT);

	void ShareTimer(UINT_PTR pT) { if (!pTimer) pTimer = pT; }

	void StopTimer();

};

bool DoReleveTrackPoint(char *msgerr);

bool StopReleveTrackPoint(char *msgerr);

#endif // !__LISTETRACKPOINTSGPX__H