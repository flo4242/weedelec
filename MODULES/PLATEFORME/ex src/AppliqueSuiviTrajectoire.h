#ifndef  __APPLIQUESUIVITRAJECTOIRE_H
#define __APPLIQUESUIVITRAJECTOIRE_H

#include "CommandeCinetique.h"
#include "Waypoint.h"


bool InitSuiviTrajectoire(char* NfKML, _list<Waypoint>& ListeWp, char* msgerr);

bool AppliqueSuiviTrajectoire(char* msgerr);


#endif // ! __APPLIQUESUIVITRAJECTOIRE_H


