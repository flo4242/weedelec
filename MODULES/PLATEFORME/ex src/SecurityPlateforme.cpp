
#include <chrono>
#include <thread>

#include "SecurityPlateforme.h"

#include "CommandeCinetique.h"
#include "ListeTrackPointsGPX.h"
#include "ControlRobot.h"


extern TControlRobot ControlRobotG;
extern TCommandeCinetique TCommandeG;
extern ListeTrackPointGPX ListePointsRobotG;

extern DRVTRAITIM *pDriverG;


void SecurityStopKeyboardThread()
{
	while (true)
	{
		int count = 0;
		while (true)
		{
			if (GetKeyState(VK_ESCAPE) & 0x8000)
			{
				TCommandeG.StopCommande();
				ControlRobotG.StopMouvement();
				ListePointsRobotG.StopTimer();

				pDriverG->Message("Arr�te securit� !");
			}
			else
				count++;
		}
		if (count == 100)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(500));
			break;
		}
	}
}