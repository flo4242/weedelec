
#include "CommandeCinetique.h"

#include "ExportModulePlateforme.h"
#include "Plateforme.h"


#define ENTRAXE_ROBOT   2.20 // entraxe: 2.2m
#define RAYON_RATTRAPAGE  2.0  // rayon de ratrappage en m�tres
#define THETA_CST  45.0*(M_PI/180)  // constante pour la fonction exponentielle : v(err_theta) = Vmax * exp{-err_theta / THETA_CST}
#define GAIN_ORIENT  0.5  // gain de commande pour l'orientation


extern DRVTRAITIM *pDriverG;

extern TParamRobot ParamG;
extern TCommandeCinetique TCommandeG;



// call back fonction
void CALLBACK CALLBACKCommandeSuiviTrajectoire(HWND hwnd, UINT uMsg, UINT timerId, DWORD dwTime)
{
	TCommandeG.AppliqueCommandeSuiviTrajectoire();
}

void CALLBACK CALLBACKCommandeSuiviChemin(HWND hwnd, UINT uMsg, UINT timerId, DWORD dwTime)
{
	TCommandeG.AppliqueCommandeSuiviChemin();
}


// lance la commande
bool TCommandeCinetique::StartCommande(bool suiviChemin, char* msgerr)
{
	if (!Fk || !pR)
	// v�rifie si l'initialisation est d�j� faite
	{
		sprintf(msgerr, "Pas encore initialis� la structure pour la commande (pointeur filtre Kalman : %s\tpointeur rep�re local : %s)",
			(Fk ? "Oui" : "NULL"), (pR ? "Oui" : "NULL"));
		return false;
	}

	if (suiviChemin)
	{
		if (TrajDesire.NbsPts < 2 || TrajDesire.NbsPts - index < 2)
		// au moins deux points restant dans la trajectoire pour la suivi de chemin (au moins deux points pour d�finir un chemin)
		{
			sprintf(msgerr, "Pas assez de points d�finis dans la trajectoire");
			return false;
		}
		pTimerCom = SetTimer(NULL, 0, dT, (TIMERPROC)&CALLBACKCommandeSuiviChemin);
	}
		
	else
	{
		if (TrajDesire.NbsPts < 1 || TrajDesire.NbsPts - 1 < index)
		// au moins 1 point pour la commande
		{
			sprintf(msgerr, "Pas assez de points d�finis dans la trajectoire");
			return false;
		}
		pTimerCom = SetTimer(NULL, 0, dT, (TIMERPROC)&CALLBACKCommandeSuiviTrajectoire);
	}
		
	return true;
}


// v�rification d'atteinte de la position d�sir�e
bool VerificationErreur(POINTFLT currentPt, POINTFLT desirePt)
{
	double err_x = abs(desirePt.x - currentPt.x);
	double err_y = abs(desirePt.y - currentPt.y);
	return (err_x < ParamG.precisionPos && err_y < ParamG.precisionPos);
}


// commande cin�matique sur le suivi de l'orientation
double GetVitesseAvanceParErrOrient(double Err)
// Calculer la vitesse d'avanc�e � partir de l'erreur de l'orientation d�sir�e ()
// Fonction potentielle : y(theta) = V*exp{-theta/theta_c} ;
{
	double v = ParamG.VitesseMaxLineaire * exp(-Err / THETA_CST);
	return v;
}

POINTFLT TCommandeCinetique::GetConsigneVitesseParErrOrient()
// Commande cap : 1/E*(vD - vG) = K*(theta_desire - theta_actuel)
// Commande vitesse d'avancement : V = 1/2*(vG + vD)  -> constante(pourrait aussi r�gler en fonction de l'erreur de suivi de cap)
// Consigne de vitesse : vG = V - E/2*K*(theta_desire - theta_actuel), vD = V + E/2*K*(theta_desire - theta_actuel)
{
	double ErrOrient = OrientDesire - OrientActuel;

	if (abs(ErrOrient + 2*M_PI) < abs(ErrOrient)) //v�rification de la zone de discontinuit�
		ErrOrient += 2 * M_PI;
	else if (abs(ErrOrient - 2 * M_PI) < abs(ErrOrient))
		ErrOrient -= 2 * M_PI;

	double consignV_theta = ENTRAXE_ROBOT / 2 * GAIN_ORIENT*ErrOrient;

	double V = GetVitesseAvanceParErrOrient(abs(ErrOrient));

	double vG, vD;

	vG = V - consignV_theta;
	vD = V + consignV_theta;

	return POINTFLT(vG, vD);
}


void TCommandeCinetique::SetPointActuel(double x, double y, double orient)
// Set point actuel dans le rep�re local qui a initialis� par pointeur pR
{
	PtActuel.x = x; PtActuel.y = y; OrientActuel = orient;
}


// filtre de Kalman en temps r�el
void TCommandeCinetique::FiltreKalmanTempsReel(double x, double y, double orient)
// x,y les coordonn�es dans le rep�re m�trique et orient en radian
{
	Fk->EtatSuivantNonCorrige();

	VECTEUR obs(3);
	
	obs[0] = x; obs[1] = y; obs[2] = orient;
	Fk->Obs = obs;

	Fk->EtatSuivantCorrige(); // corrige

	// mise � jour des donn�es
	VECTEUR Etat = Fk->GetEtatCourant();
	
	double v, theta;
	x = Etat[0]; y = Etat[1]; v = Etat[2]; theta = Etat[3]; 

	// correction du d�calage entre l'axe du robot et la position d'installation de GPS
	x -= DIST_DECALAGE * sin(theta);
	y += DIST_DECALAGE * cos(theta);

	SetPointActuel(x, y, theta);
}


void TCommandeCinetique::AppliqueCommandeSuiviChemin()
{
	char msgerr[1000];

	// mettre � jours le point de passage d�sir� � l'instant
	PtPassageDesire = TrajDesire.ListePtsPassage[index + 1].obj;

	// r�cup�ration de nouvelles mesures
	double lat_mesure, lon_mesure, orient_mesure, vitesse_mesure;

	if (!GetMesureEnTempsReel(lat_mesure, lon_mesure, orient_mesure, vitesse_mesure, msgerr))  // recevoir nouvelles mesures
	{
		StopCommande();
		pDriverG->Message("Erreur lecture capteur : %s", msgerr);
		return;
	}

	double x_mesure, y_mesure;
	pR->WGS84VersMetres(lat_mesure, lon_mesure, x_mesure, y_mesure);

	// applique le filtre Kalman pour corriger les mesures courantes :
	FiltreKalmanTempsReel(x_mesure, y_mesure, orient_mesure* (M_PI / 180));

	// calculer la position et l'orientation du point d�sir� actuel
	CalculCapDesireSuiviChemin(); 

	double vitesseG, vitesseD;
	POINTFLT V = GetConsigneVitesseParErrOrient();
	vitesseG = V.x; vitesseD = V.y;


	pDriverG->OuvreTexte();
	pDriverG->PrintTexte("VG: %f\tVD: %f\n", vitesseG, vitesseD);
	pDriverG->PrintTexte("Orient actuel: %f\tOrient d�sir�: %f\n", OrientActuel, OrientDesire);
	pDriverG->PrintTexte("X actuel: %f\tY actuel: %f\n", PtActuel.x, PtActuel.y);
	pDriverG->PrintTexte("X passage d�sir�: %f\tY passage d�sir�: %f\n", PtPassageDesire.x, PtPassageDesire.y);


	SendSpeedRobot(vitesseG, vitesseD); // envoyer la commande au robot

	// v�rification d'atteinte au dernier point de la trajectoire
	if (VerificationErreur(PtActuel, PtPassageDesire))
	{
		index++;
		if (index < TrajDesire.NbsPts - 1)
			pDriverG->Message("Point de passage P%d arriv�!", index);
		else
		{
			StopCommande();
			pDriverG->Message("Suivi de chemin fini!");
			return;
		}
	}

	return;
}


void TCommandeCinetique::CalculCapDesireSuiviChemin()
// la fonction qui sert au suivi de chemin
{
	POINTFLT P1 = Pchemin1;
	POINTFLT P2 = Pchemin2;

	POINTFLT PC = PtActuel;

	POINTFLT U = (P2 - P1) / (P2 - P1).Norme();
	POINTFLT P1C = PC - P1;
	POINTFLT PH = P1 + U * (P1C | U);

	double dist = (PC - PH).Norme();

	POINTFLT PCible;

	double longueur = RAYON_RATTRAPAGE * RAYON_RATTRAPAGE - dist * dist;

	if (longueur <= 0)
		PCible = PH;
	else
		PCible = PH + U * sqrt(longueur);

	OrientDesire = atan2(PC.x - PCible.x, PCible.y - PC.y);
}


void TCommandeCinetique::AvanceDistanceSuiviChemin(POINTFLT p1, POINTFLT p2, double dis)
{
	Pchemin1 = p1; Pchemin2 = p2;
	DisAvancement = dis;
}


// applique la commande de suivi de trajectoire
void TCommandeCinetique::AppliqueCommandeSuiviTrajectoire()
{
	char msgerr[1000];

	// r�cup�ration de nouvelles mesures
	double lat_mesure, lon_mesure, orient_mesure, vitesse_mesure;

	if (!GetMesureEnTempsReel(lat_mesure, lon_mesure, orient_mesure, vitesse_mesure, msgerr))  // recevoir nouvelles mesures
	{
		StopCommande();
		pDriverG->Message("Erreur lecture capteur : %s", msgerr);
		return;
	}

	double x_mesure, y_mesure;
	pR->WGS84VersMetres(lat_mesure, lon_mesure, x_mesure, y_mesure);

	// applique le filtre Kalman pour corriger les mesures courantes :
	FiltreKalmanTempsReel(x_mesure, y_mesure, orient_mesure* (M_PI / 180));

	// calculer la position et l'orientation du point d�sir� actuel
	CalculCapDesireActuelSuiviTrajectoire(); // mettre � jour de point d�sir� � int�rieur de la fonction

	double vitesseG, vitesseD;
	POINTFLT V;
	V = GetConsigneVitesseParErrOrient();

	vitesseG = V.x; vitesseD = V.y;

	pDriverG->OuvreTexte();
	pDriverG->PrintTexte("VG: %f\tVD: %f\n", vitesseG, vitesseD);
	pDriverG->PrintTexte("Orient actuel: %f\tOrient d�sir�: %f\n", OrientActuel, OrientDesire);
	pDriverG->PrintTexte("X actuel: %f\tY actuel: %f\n", PtActuel.x, PtActuel.y);
	pDriverG->PrintTexte("X passage d�sir�: %f\tY passage d�sir�: %f\n", PtPassageDesire.x, PtPassageDesire.y);

	SendSpeedRobot(vitesseG, vitesseD); // envoyer la commande au robot

	// v�rification d'atteinte au dernier point de la trajectoire
	if (VerificationErreur(PtActuel, PtPassageDesire))
	{
		index++;
		if (index < TrajDesire.NbsPts)
			pDriverG->Message("Point de passage P%d arriv�!", index);
		else
		{
			StopCommande();
			pDriverG->Message("Point d�sir� arriv�!");
			return;
		}
	}

	return;
}

void TCommandeCinetique::CalculCapDesireActuelSuiviTrajectoire()
// calcule le cap d�sir� pour le suivi de trajectoire
{
	POINTFLT P1 = PtActuel;
	POINTFLT P2 = PtPassageDesire;

	double diffx = P1.x - P2.x;
	double diffy = P2.y - P1.y;

	double capD = atan2(diffx, diffy);
	
	OrientDesire = capD;
}



