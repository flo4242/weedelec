#ifndef __APPLIQUEFILTREKALMANERX_H_
#define __APPLIQUEFILTREKALMANERX_H_


#include <Windows.h>
#include <ppltasks.h> 
#include <agents.h> // pour timer
#include "FiltreKalmanRobotErx.h"
#include "Plateforme.h"

using namespace Concurrency;

struct TMesureRobot
{
	double latDeg;
	double lonDeg;
	double capDeg;

	TMesureRobot() : latDeg(0), lonDeg(0), capDeg(0) {};
};

struct TAppliqueKalman
{

	timer<bool> *Timer_Kalman;
	bool GPSExterne;

	TFiltreKalman * pKalman;
	TPositionRobot PosRobotKalman;
	TMesureRobot MesureRobot;

	TAppliqueKalman() { pKalman = NULL; }

	~TAppliqueKalman() 
	{ 
		if (pKalman) delete pKalman; 
		if (Timer_Kalman) delete Timer_Kalman;
	}


	void Init(TFiltreKalman *kalman, bool GPSexterne)
	{
		pKalman = kalman; GPSExterne = GPSexterne;
	};

	bool SetTimer(char *msgerr);

	void StartTimer();

	void StopTimer();


	bool EvolutionMesuresKalman();

	void UpdatePositionKalman(double x, double y, double capdeg);

	void GetPositionRobotKalman(TPositionRobot &Pos);

};


void AfficheKalman();



#endif // !__APPLIQUEFILTREKALMANERX_H_