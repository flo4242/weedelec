//#include <Winreg.h>
#include "GetPortsCOM.h"
#include "ComRS232.h"

#include <stdio.h> //pour sprintf
#include "WINUTIL2.h"
#include "GetListePortsCOM.h"
#include "GetListeFichiers.h"


char* TParamComGPS::GetPort(char *DescriptionPort)
/******************************************/
{
	int l = strlen(DescriptionPort);
	for (int i = 0; i < l - 1; i++)
		if (DescriptionPort[i] == '\t') return DescriptionPort + i + 1;

	return NULL;
}

bool GereGroupeConfig(HWND hParent, TParamComGPS &param, bool charge)
/****************************************************************/
{
	//récupération de la liste des ports disponibles
	_list<TNf> Liste;
	GetListePortsCOM(Liste);

	//création de la liste multichoix
	AligneDescriptions(Liste);

	char** rubriques = new char*[Liste.nbElem + 1];
	int index = 0;
	BalayeListe(Liste)
	{
		rubriques[index++] = *Liste.current();
	}
	rubriques[index] = NULL;


	int IndexChoix = -1;

	GROUPE G(hParent, "Configuration Communication Serielle Port", "ComPortGPS.cfg",
		MULCHOIX("Choix port COM USB pour liaison PIC", IndexChoix, rubriques),
		NULL);

	bool Modif = (charge ? G.Charge() : G.Gere());

	if (!Modif) { delete rubriques; return false; }

	if (IndexChoix != -1)
	{
		//on retrouve la description choisie
		int cnt = 0;
		char *ChoixCOM = "";
		BalayeListe(Liste)
		{
			if (cnt == IndexChoix)
			{
				ChoixCOM = *Liste.current();
				break;
			}
			cnt++;
		}

		//fermeture éventuelle du port précédent
		if (param.HCom != NULL)
		{
			CloseHandle(param.HCom);
			param.HCom = NULL;
		}
				
		//ouverture du nouveau
		param.HCom = InitComAsync(param.GetPort(ChoixCOM));

		if (param.HCom)            strcpy(param.DescriptionPort, ChoixCOM);
		else
		{
			char st[100];
			sprintf(st, "Echec ouverture %s", GetPort(ChoixCOM));
			::MessageBox(hParent, st, "Port NMEA", MB_OK & MB_ICONERROR);
			strcpy(param.DescriptionPort, "");
			IndexChoix = -1;
			G.Sauve();
		}
	}

	delete rubriques;
	return true;
}
