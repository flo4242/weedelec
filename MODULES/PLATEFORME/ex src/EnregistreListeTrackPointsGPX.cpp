
#include "ControlRobot.h"
#include <Windows.h>


#include "drvtraiti.h"
#include "ExportModulePlateforme.h"

#include "Plateforme.h"
#include "EnregistreListeTrackPointsGPX.h"

extern ListeTrackPointGPX ListePointsRobotG;

extern TParamRobot ParamG;

extern DRVTRAITIM *pDriverG;

extern TControlRobot ControlRobotG;

extern TRepereLocal *RepereG;


void CALLBACK GetMesureCourant(HWND hwnd, UINT uMsg, UINT timerId, DWORD dwTime)
{
	double LatDeg = 0, LonDeg = 0, CapDeg = 0;
	char msgerr[1000];

	SYSTEMTIME st;
	GetSystemTime(&st);


	if (!ControlRobotG.GetRobotSensorData(LatDeg, LonDeg, CapDeg, msgerr))
	{
		ListePointsRobotG.StopTimer();
		pDriverG->Message(msgerr);
		return;
	}

	if (ParamG.GPSExterne)
	{
		int fix;
		if (!GetMesureGPSExterne(LatDeg, LonDeg, fix, msgerr))
		{
			ListePointsRobotG.StopTimer();
			pDriverG->Message(msgerr);
			return;
		}
	}

	ListePointsRobotG.SetPointCourant(LatDeg, LonDeg, 0, CapDeg, st);
	pDriverG->PrintTexte("System Time: %02u-%02u-%02uT%02u:%02u:%02u\n", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
	pDriverG->PrintTexte("Relev� %d points fini pour capteur robot\n", ListePointsRobotG.ListePointGPX.size());
}


void ListeTrackPointGPX::SetPointCourant(double Lat, double Lon, double Vitesse, double Cap, SYSTEMTIME st)
{
	PointCourant.LatDeg = Lat;
	PointCourant.LonDeg = Lon;
	PointCourant.GroundSpeed = Vitesse;
	PointCourant.HeadingDeg = Cap;
	PointCourant.STime = st;
	ListePointGPX.push_back(PointCourant);
}


void ListeTrackPointGPX::StartTimer(int incT)
{
	pTimer = SetTimer(NULL, 0, incT, (TIMERPROC)&GetMesureCourant);
}


void ListeTrackPointGPX::StopTimer()
{
	if (pTimer) KillTimer(NULL, pTimer);
}



bool DoReleveTrackPoint(char *msgerr)
{
	int incT = ParamG.incT_Kalman;

	ListePointsRobotG.StartTimer(incT);
	return true;
}

bool StopReleveTrackPoint(char *msgerr)
{
	if (!ListePointsRobotG.pTimer)
	{
		sprintf(msgerr, "Echec : par de timer �tabli");
		return false;
	}
	ListePointsRobotG.StopTimer();
	return true;
}
