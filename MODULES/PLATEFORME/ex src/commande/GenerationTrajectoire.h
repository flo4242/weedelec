#ifndef __GENERATIONTRAJECTOIRE_H
#define __GENERATIONTRAJECTOIRE_H

#include "list_tpl.h"
#include "ligne.h"

double GetTimeNow();


struct Trajectoire
{

	_list<POINTFLT> ListePtsPassage; // liste de points de passage d�finis en flottant dans rep�re m�trique

	_list<double> AbscissesPtsPassage; // abscisse curviligne pour les points de passage

	double startTime;  // en seconde, par exemple 201032.359 -> 201032 sec 359 milisecondes

	double vitesseLineaire; // vitesse lin�aire pour calculer les points d�sir�s, en m/s
	double LongueurTotale;

	int NbsPts;

	// functions de membre:

	Trajectoire() 
	// Initialisation de structure Trajectoire, param�tre : vitesse lin�aire pour calculer les points d�sir�s
	{ 
		ListePtsPassage.clear(); AbscissesPtsPassage.clear(); NbsPts = 0; LongueurTotale = 0;
	};

	void InitVitesseLineaire(double vitesse) { vitesseLineaire = vitesse; };

	void AjoutePointPassage(double xd, double yd); 

	void start() { startTime = GetTimeNow(); };

	POINTFLT GetPointDesireActuel();
};




#endif // !__GENERATIONTRAJECTOIRE_H
