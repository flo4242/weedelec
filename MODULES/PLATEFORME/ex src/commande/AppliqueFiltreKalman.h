#ifndef ___GETPOSITIONKALMAN_H
#define ___GETPOSITIONKALMAN_H

#include <Windows.h>
#include "drvtraiti.h"
#include "KALMAN.H"

#ifndef DIM_KALMAN
#define DIM_KALMAN	5
#endif 

 

class TFiltreKalman : public SYSTEMEKALMAN
{

protected:

	//matrices de calcul
	virtual MATCARREE Fk();	//retourne la matrice dXk+1 / dXk
	virtual MATRICE Hk();		//retourne la matrice d Observations/ d Xk
	virtual MATCARREE Qk();	//retourne la covariance instantan_e
	virtual MATCARREE Rk(); //retourne la covariance sur observations
	
	//fonctions de calcul
	virtual void EvolutionEtat(void);
	virtual VECTEUR PredictionObservations();
	virtual VECTEUR MesureObservations();
	virtual void CorrigeEtat(VECTEUR deltaX);
	virtual VECTEUR EtatCourant();
	
	virtual void Init(void);


public:

	double x, y; // ordre d'�tat: x, y, v, theta, deltaH
	double theta;
	double v;
	double dtheta;

	double deltaT; // dt: pas de temps 

	//membres de calcul rajout�s:

	MATCARREE Q0;
	MATCARREE FF;     //seule une petite partie est variable
	MATRICE HH;	
	MATRICE RR;	
	VECTEUR Obs;	//nouvelle observation

	TFiltreKalman(int Dim) : SYSTEMEKALMAN(Dim) { Init(); };

	void InitKalman(double x0, double y0, double theta0, double v0, double dtheta0);

	void ChargeParametres(double deltaT, double sig_x0, double sig_theta0, double sig_v0, double sig_dtheta0, double sig_x,
		double sig_theta, double sig_v, double sig_dtheta, double sig_obsx, double sig_obstheta);

	VECTEUR GetEtatCourant();
};



#endif