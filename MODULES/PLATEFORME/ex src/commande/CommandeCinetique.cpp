
#include "CommandeCinetique.h"
#include "ExportModulePlateforme.h"
#include "PseudoInverseOpenCV.h"

#define THETA_CST (1.0/6)*M_PI  // constante de fonction exponentielle


extern DRVTRAITIM *pDriverG;

extern TParamRobot ParamG;
extern TCommandeCinetique TCommandeG;


void CALLBACK CommandeEnVitesseCALLBACK(HWND hwnd, UINT uMsg, UINT timerId, DWORD dwTime)
{
	if(ParamG.boolSuiviChemin)
		TCommandeG.AppliqueCommandeSuiviChemin();
	else
		TCommandeG.AppliqueCommandeSuiviTrajectoire();
}


bool VerificationErreur(POINTFLT currentPt, POINTFLT lastPt)
// v�rification d'atteinte de la position d�sir�e
{
	double err_x = abs(lastPt.x - currentPt.x);
	double err_y = abs(lastPt.y - currentPt.y);
	return (err_x < ParamG.precisionPos && err_y < ParamG.precisionPos);
}

// commande cin�matique de syst�me complet
MATRICE PinvMatriceJacobinne(double theta, double E)
{
	MATRICE J(3, 2);
	J.Constante(0);

	J[0][0] = -1.0 / 2 * sin(theta);	J[0][1] = -1.0 / 2 * sin(theta);
	J[1][0] = 1.0 / 2 * cos(theta);	J[1][1] = 1.0 / 2 * cos(theta);
	J[2][0] = -1.0 / E;				J[2][1] = 1.0 / E;

	MATRICE pInvM = PseudoInverseMatrice(J);

	return pInvM;
}

VECTEUR ErreurSuiviTrajectoire(VECTEUR err)
{
	VECTEUR K(3);
	K[0] = err[0] * ParamG.gainPos;
	K[1] = err[1] * ParamG.gainPos;
	K[2] = err[2] * ParamG.gainOrient;
	return K;
}

POINTFLT TCommandeCinetique::GetConsigneVitesse()
{
	double x_actuel, y_actuel;
	double x_desire, y_desire;

	x_actuel = PtActuel.x; y_actuel = PtActuel.y;
	x_desire = PtDesireActuel.x;
	y_desire = PtDesireActuel.y;

	MATRICE pinv_J = PinvMatriceJacobinne(OrientActuel, ENTRAXE_ROBOT);
	VECTEUR v(2); MATRICE V(2, 1); VECTEUR Erreur(3);

	Erreur[0] = x_desire - x_actuel;
	Erreur[1] = y_desire - y_actuel;
	Erreur[2] = OrientDesire - OrientActuel;

	if (abs(Erreur[2]) > (340 * M_PI / 180)) //v�rification de la zone de discontinuit� (10� de chaque c�t�)
	{
		if (Erreur[2] > 0)
			Erreur[2] = Erreur[2] - 2 * M_PI;
		else
			Erreur[2] = Erreur[2] + 2 * M_PI;
	}

	V = pinv_J * ErreurSuiviTrajectoire(Erreur);
	v = V.Colonne(0);

	pDriverG->OuvreTexte();
	pDriverG->PrintTexte("ERR_X: %f\tERR_Y: %f\tERR_O: %f\n", Erreur[0], Erreur[1], Erreur[2]);

	double vG = v[0];  double vD = v[1];

	return POINTFLT(vG, vD);
}

// commande cin�matique sur le suivi de l'orientation
double GetVitesseAvanceParErrOrient(double Err)
// Calculer la vitesse d'avanc�e � partir de l'erreur de l'orientation d�sir�e ()
// Fonction potentielle : y(theta) = V*exp{-theta/theta_c} ;
{
	double v = ParamG.VitesseLinSuivi * exp(-Err / THETA_CST);
	return v;
}

POINTFLT TCommandeCinetique::GetConsigneVitesseErrOrient()
// Commande cap : 1/E*(vD - vG) = K*(theta_desire - theta_actuel)
// Commande vitesse d'avancement : V = 1/2*(vG + vD)  -> constante(pourrait aussi r�gler en fonction de l'erreur de suivi de cap)
// Consigne de vitesse : vG = V - E/2*K*(theta_desire - theta_actuel), vD = V + E/2*K*(theta_desire - theta_actuel)
{
	double ErrOrient = OrientDesire - OrientActuel;

	if (abs(ErrOrient + 2*M_PI) < abs(ErrOrient)) //v�rification de la zone de discontinuit�
		ErrOrient += 2 * M_PI;
	else if (abs(ErrOrient - 2 * M_PI) < abs(ErrOrient))
		ErrOrient -= 2 * M_PI;

	double consignV_theta = ENTRAXE_ROBOT / 2 * ParamG.gainOrient *ErrOrient;

	double V = GetVitesseAvanceParErrOrient(abs(ErrOrient));

	double vG, vD;

	vG = V - consignV_theta;
	vD = V + consignV_theta;

	return POINTFLT(vG, vD);
}

// commence la commande
bool TCommandeCinetique::StartCommande(char* msgerr)
{
	if (!Fk || !pR)
	{
		sprintf(msgerr, "Pas encore initialis� la structure pour la commande (pointeur filtre Kalman : %s\tpointeur rep�re local : %s)",
			(Fk ? "Oui" : "NULL"), (pR ? "Oui" : "NULL"));
		return false;
	}

	index = 0;
	pTimerCom = SetTimer(NULL, 0, dT, (TIMERPROC)&CommandeEnVitesseCALLBACK);
	TrajDesire.start();

	return true;
}

// filtre de Kalman en temps r�el
void TCommandeCinetique::FiltreKalmanTempsReel(double x, double y, double orient)
// x,y les coordonn�es dans le rep�re m�trique et orient en radian
{
	if (!Fk || !pR) return;  // variable public pour le fitre Kalman et pour la projection de rep�re local

	Fk->EtatSuivantNonCorrige();

	VECTEUR obs(3);
	
	obs[0] = x; obs[1] = y; obs[2] = orient;
	Fk->Obs = obs;

	Fk->EtatSuivantCorrige(); // corrige

	// mise � jour des donn�es
	VECTEUR Etat = Fk->GetEtatCourant();
	
	double v, theta;
	x = Etat[0]; y = Etat[1]; v = Etat[2]; theta = Etat[3]; 

	// correction du d�calage entre l'axe du robot et la position d'installation de GPS
	x -= DIST_DECALAGE * sin(theta);
	y += DIST_DECALAGE * cos(theta);

	SetPointActuel(x, y, theta);
}


void TCommandeCinetique::AppliqueCommandeSuiviTrajectoire()
{
	char msgerr[1000];

	// r�cup�ration de nouvelles mesures
	double lat_mesure, lon_mesure, orient_mesure, vitesse_mesure;

	if (!GetMesureEnTempsReel(lat_mesure, lon_mesure, orient_mesure, vitesse_mesure, msgerr))  // recevoir nouvelles mesures
	{
		if (pTimerCom) KillTimer(NULL, pTimerCom);
		pDriverG->Message("Erreur lecture capteur : %s", msgerr);
		return;
	}

	double x_mesure, y_mesure;
	pR->WGS84VersMetres(lat_mesure, lon_mesure, x_mesure, y_mesure);

	// applique le filtre Kalman pour corriger les mesures courantes :
	FiltreKalmanTempsReel(x_mesure, y_mesure, orient_mesure* (M_PI / 180));

	// calculer la position et l'orientation du point d�sir� actuel
	CalculPointDesireActuel(); // mettre � jour de point d�sir� � int�rieur de la fonction

	double vitesseG, vitesseD;
	POINTFLT V;
	V = GetConsigneVitesseErrOrient();

	vitesseG = V.x; vitesseD = V.y;

	pDriverG->OuvreTexte();
	pDriverG->PrintTexte("VG: %f\tVD: %f\n", vitesseG, vitesseD);
	pDriverG->PrintTexte("Orient actuel: %f\tOrient d�sir�: %f\n", OrientActuel, OrientDesire);
	pDriverG->PrintTexte("X actuel: %f\tY actuel: %f\n", PtActuel.x, PtActuel.y);
	pDriverG->PrintTexte("X d�sir�: %f\tY d�sir�: %f\n", PtDesireActuel.x, PtDesireActuel.y);
	pDriverG->PrintTexte("X passage d�sir�: %f\tY passage d�sir�: %f\n", PtPassageDesire.x, PtPassageDesire.y);

	SendSpeedRobot(vitesseG, vitesseD); // envoyer la commande au robot

	// v�rification d'atteinte au dernier point de la trajectoire
	if (VerificationErreur(PtActuel, PtPassageDesire))
	{
		index++;
		if (index < TrajDesire.NbsPts)
		{
			POINTFLT PtSuivant = TrajDesire.ListePtsPassage[index].obj;
			SetPointPassageDesire(PtSuivant.x, PtSuivant.y);
			pDriverG->Message("Point de passage P%d arriv�!", index);
		}
		else
		{
			StopCommande();
			pDriverG->Message("Point d�sir� arriv�!");
			return;
		}
	}

	/*while (true) // arr�te protection par Touche Echap
	{
		int count = 0;
		if (GetKeyState(VK_ESCAPE) & 0x8000)
		{
			StopCommande(); // arr�ter la commande 
			return;
		}
		else
			count++;

		if (count == 100)
			break;
	}*/

	return;
}


// les fonctions de membres �l�mentaires
void TCommandeCinetique::SetPointActuel(double x, double y, double orient)
// Set point actuel dans le rep�re local qui a initialis� par pointeur pR
{
	PtActuel.x = x; PtActuel.y = y; OrientActuel = orient;
}

void TCommandeCinetique::SetPointDesireActuel(double x, double y, double orient)
// Set point d�sir� actuel dans le rep�re local qui a initialis� par pointeur pR
{
	PtDesireActuel.x = x; PtDesireActuel.y = y; OrientDesire = orient;
}

void TCommandeCinetique::SetPointPassageDesire(double x, double y)
{
	PtPassageDesire.x = x; PtPassageDesire.y = y;
}

void TCommandeCinetique::CalculPointDesireActuel()
{
	POINTFLT P1 = PtActuel;
	POINTFLT P2 = PtPassageDesire;

	double diffx = P1.x - P2.x;
	double diffy = P2.y - P1.y;

	OrientDesire = atan2(diffx, diffy);

	// calculer la position de point d�sir� actuel
	POINTFLT D = (P2 - P1) / (P2 - P1).Norme(); // vecteur unitaire suivant la direction P2 - P1

	double Echelle = VitesseSuivi * double(dT) / 1000;
	D *= Echelle;

	PtDesireActuel = P1 + D; // point d�sir�
}



void TCommandeCinetique::AppliqueCommandeSuiviChemin()
{
	char msgerr[1000];

	if (TrajDesire.NbsPts < 2)
	{
		StopCommande();
		return;
	}
	
	PtPassageDesire = TrajDesire.ListePtsPassage[index].next->obj;

	// r�cup�ration de nouvelles mesures
	double lat_mesure, lon_mesure, orient_mesure, vitesse_mesure;

	if (!GetMesureEnTempsReel(lat_mesure, lon_mesure, orient_mesure, vitesse_mesure, msgerr))  // recevoir nouvelles mesures
	{
		StopCommande();
		pDriverG->Message("Erreur lecture capteur : %s", msgerr);
		return;
	}

	double x_mesure, y_mesure;
	pR->WGS84VersMetres(lat_mesure, lon_mesure, x_mesure, y_mesure);

	// applique le filtre Kalman pour corriger les mesures courantes :
	FiltreKalmanTempsReel(x_mesure, y_mesure, orient_mesure* (M_PI / 180));

	// calculer la position et l'orientation du point d�sir� actuel
	CalculCapDesireActuel(index); // mettre � jour de point d�sir� � int�rieur de la fonction

	double vitesseG, vitesseD;
	POINTFLT V = GetConsigneVitesseErrOrient();
	vitesseG = V.x; vitesseD = V.y;


	pDriverG->OuvreTexte();
	pDriverG->PrintTexte("VG: %f\tVD: %f\n", vitesseG, vitesseD);
	pDriverG->PrintTexte("Orient actuel: %f\tOrient d�sir�: %f\n", OrientActuel, OrientDesire);
	pDriverG->PrintTexte("X actuel: %f\tY actuel: %f\n", PtActuel.x, PtActuel.y);
	pDriverG->PrintTexte("X d�sir�: %f\tY d�sir�: %f\n", PtDesireActuel.x, PtDesireActuel.y);
	pDriverG->PrintTexte("X passage d�sir�: %f\tY passage d�sir�: %f\n", PtPassageDesire.x, PtPassageDesire.y);


	SendSpeedRobot(vitesseG, vitesseD); // envoyer la commande au robot

	// v�rification d'atteinte au dernier point de la trajectoire
	if (VerificationErreur(PtActuel, PtPassageDesire))
	{
		index++;
		if (index < TrajDesire.NbsPts - 1)
			pDriverG->Message("Point de passage P%d arriv�!", index);
		else
		{
			StopCommande();
			pDriverG->Message("Suivi de chemin fini!");
			return;
		}
	}

	/*while (true) // arr�te protection par Touche Echap
	{
		int count = 0;
		if (GetKeyState(VK_ESCAPE) & 0x8000)
		{
			StopCommande(); // arr�ter la commande 
			return;
		}
		else
			count++;

		if (count == 100)
			break;
	}*/

	return;
}


#define RAYON_CORRECTION  2.5  // m�tres de rayon de correction

void TCommandeCinetique::CalculCapDesireActuel(int indexSegment)
{
	POINTFLT P1 = TrajDesire.ListePtsPassage[indexSegment].obj;
	POINTFLT P2 = TrajDesire.ListePtsPassage[indexSegment].next->obj;

	POINTFLT PC = PtActuel;

	POINTFLT U = (P2 - P1) / (P2 - P1).Norme();
	POINTFLT P1C = PC - P1;
	POINTFLT PH = P1 + U * (P1C | U);

	double dist = (PC - PH).Norme();

	POINTFLT PCible;

	double longueur = RAYON_CORRECTION * RAYON_CORRECTION - dist * dist;
	
	if (longueur <= 0)
		PCible = PH;
	else
		PCible = PH + U * sqrt(longueur);

	OrientDesire = atan2(PC.x - PCible.x, PCible.y - PC.y);
}