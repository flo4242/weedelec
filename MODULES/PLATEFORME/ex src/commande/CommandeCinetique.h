#ifndef __COMMANDECINETIQUE_H
#define __COMMANDECINETIQUE_H

#include "ControlRobot.h"
#include "AppliqueFiltreKalman.h"
#include "RepereLocal.h"
#include "GenerationTrajectoire.h"

#ifndef ENTRAXE_ROBOT
#define ENTRAXE_ROBOT   2.20 // entraxe: 2.2m
#endif // define entraxe 

#ifndef DIST_DECALAGE
#define DIST_DECALAGE  1.08     // distance de d�calage du GPS (m�tres)
#endif


void CALLBACK CommandeEnVitesseCALLBACK(HWND hwnd, UINT uMsg, UINT timerId, DWORD dwTime);

class TCommandeCinetique
{
public:
	TRepereLocal *pR; // rep�re local utilis�
	TFiltreKalman *Fk; // filtre Kalman appliqu�
	Trajectoire TrajDesire;

	POINTFLT PtPassageDesire; // point de passage d�sir� (le but d'atteinte)
	
	POINTFLT PtDesireActuel; // point d�sir� pour la commande actuelle
	POINTFLT PtActuel; // point actuel

	double OrientActuel; // en radian
	double OrientDesire; // en radian

	double VitesseSuivi; // vitesse de suivi de trajectoire

	UINT_PTR pTimerCom; // timer ID pour �tablir une p�riode constante de commande (timer for callback function)
	int dT; // temps d'increment de commande

	int index;
	
	TCommandeCinetique() 
	{
		OrientActuel = 0; OrientDesire = 0; index = 0;
		pTimerCom = NULL; pR = NULL; Fk = NULL;
	};

	~TCommandeCinetique() 
	{
		if (pTimerCom)  KillTimer(NULL, pTimerCom); 
		if (pR) delete pR; 
		if (Fk) delete Fk; 
	}

	void InitParam(TFiltreKalman FKalman, Trajectoire traj, double LatM, int deltaT)
	{
		Fk = new TFiltreKalman(DIM_KALMAN); *Fk = FKalman; dT = deltaT;
		pR = new TRepereLocal(LatM); 
		TrajDesire = traj; VitesseSuivi = traj.vitesseLineaire;
	};

	bool StartCommande(char* msgerr);

	void StopCommande() { if (pTimerCom) KillTimer(NULL, pTimerCom); };


	void FiltreKalmanTempsReel(double x, double y, double orient);

	
	void AppliqueCommandeSuiviTrajectoire();

	void AppliqueCommandeSuiviChemin();


	void SetPointActuel(double x, double y, double orient);

	void SetPointDesireActuel(double x, double y, double orient);

	void SetPointPassageDesire(double x, double y);


	void CalculPointDesireActuel();

	void CalculCapDesireActuel(int indexSegment);


	POINTFLT GetConsigneVitesse();

	POINTFLT GetConsigneVitesseErrOrient();
	

};


#endif // !__COMMANDECINETIQUE_H
