#ifndef __COMMANDECINETIQUE_H
#define __COMMANDECINETIQUE_H

#include "ControlRobot.h"

#include "drvtraiti.h"
#include "ExportModulePlateforme.h"
#include "AppliqueFiltreKalman.h"
#include "RepereLocal.h"

#ifndef ENTRAXE_ROBOT
#define ENTRAXE_ROBOT   1.73
#endif // define entraxe 


void CALLBACK CommandeEnVitesseCALLBACK(HWND hwnd, UINT uMsg, UINT timerId, DWORD dwTime);

class TCommandeCinetique
{
public:
	TRepereLocal *pR; // rep�re local utilis�
	TFiltreKalman *Fk; // filtre Kalman appliqu�

	double LatCurant;
	double LonCurant;
	double OrientCurant;
	double VitesseCurent;

	double LatDesire;
	double LonDesire;
	double OrientDesire;

	//double VitesseG;
	//double VitesseD;

	UINT_PTR pTimerCom; // timer ID
	int dT; // temps d'increment de commande
	
	TCommandeCinetique() {
		LatCurant = 0; LonCurant = 0; OrientCurant = 0; VitesseCurent = 0;
		LatDesire = 0; LonDesire = 0; OrientDesire = 0; 
		//VitesseG = 0; VitesseD = 0; 
		pTimerCom = NULL; pR = NULL; Fk = NULL;
	};

	~TCommandeCinetique() {
		if (pTimerCom) { KillTimer(NULL, pTimerCom); } if (pR) delete pR; if (Fk) delete Fk; }

	void InitParam(TFiltreKalman& FKalman, TRepereLocal& Repere) { Fk = &FKalman; pR = &Repere; dT = FKalman.deltaT; };

	void SetCommande() { pTimerCom = SetTimer(NULL, 0, dT, (TIMERPROC)&CommandeEnVitesseCALLBACK); };

	void StopCommande() { if (pTimerCom) KillTimer(NULL, pTimerCom); };

	void SetEtatCurant(double lat, double lon, double orient, double vitesse) {
		LatCurant = lat; LonCurant = lon; OrientCurant = orient; VitesseCurent = vitesse; };

	void SetEtatDesire(double lat, double lon, double orient) { 
		LatDesire = lat; LonDesire = lon; OrientDesire = orient; };

	void CommandeEnVitesse();


private:

	void FiltreKalmanTempsReel(double lat, double lon, double orient, double vitesse);

	void CalculCommandeVitesse(double& vitesseG, double& vitesseD, double& err_x, double& err_y, double& err_theta);

};

bool AppliqueCommandeCinetique(double LatD, double LonD, double OrientD, char* msgerr);




#endif // !__COMMANDECINETIQUE_H
