#ifndef __COMRS32_H
#define __COMRS32_H

#include <windows.h>

//valeurs de retour de AttenteSequenceAsync()
#define ATTENTESEQUENCE_OK	0
#define ATTENTESEQUENCE_ABORTED	1
#define ATTENTESEQUENCE_ERROR	2
#define ATTENTESEQUENCE_ARRETDEMANDE	3

HANDLE InitComAsync(char *nomPort);
int AttenteSequenceAsync(HANDLE HCom, char *ReadBuffer, int taillebuffer, const bool &ArretDemande, char *msgerr1000);

#endif