
#include "ControlRobot.h"
#include <Windows.h>


#include "drvtraiti.h"
#include "ExportModulePlateforme.h"

#include "Plateforme.h"
#include "SensorAPI.h"
#include "ListeTrackPointsGPX.h"

extern ListeTrackPointGPX ListePointsRobotG;

extern TParamRobot ParamG;
extern DRVTRAITIM *pDriverG;

extern TControlRobot ControlRobotG;
extern ISensor *sensor;


void CALLBACK GetMesureCourant(HWND hwnd, UINT uMsg, UINT timerId, DWORD dwTime)
{
	double Latitude = 0, Longitude = 0, Vitesse = 0, Cap = 0;
	double latGPS = 0, lonGPS = 0;
	char msg[1000];

	SYSTEMTIME st;
	GetSystemTime(&st);
	if (ParamG.ActiveExterneGPS)
	{
		if (!GetDonneesExterneGPS(Latitude, Longitude, Vitesse, msg))
		{
			ListePointsRobotG.StopTimer();
			pDriverG->Message("Echec callback fonction: %s", msg);
			return;
		}
		if (!GetDonneesCapteurRobot(latGPS, lonGPS, Cap, msg))
		{
			ListePointsRobotG.StopTimer();
			pDriverG->Message("Echec callback fonction: %s", msg);
			return;
		}

		ListePointsRobotG.SetPointCourant(Latitude, Longitude, Vitesse, Cap, st);

		pDriverG->PrintTexte("System Time: %02u-%02u-%02uT%02u:%02u:%02u\n", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
		pDriverG->PrintTexte("Relev� %d points fini pour capteur robot\n", ListePointsRobotG.ListePointGPX.size());
	}

	else
	{
		if (!GetDonneesCapteurRobot(Latitude, Longitude, Cap, msg))
		{
			ListePointsRobotG.StopTimer();
			pDriverG->Message("Echec callback fonction: %s", msg);
			return;
		}

		ListePointsRobotG.SetPointCourant(Latitude, Longitude, 0, Cap, st);
		pDriverG->PrintTexte("System Time: %02u-%02u-%02uT%02u:%02u:%02u\n", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
		pDriverG->PrintTexte("Relev� %d points fini pour capteur robot\n", ListePointsRobotG.ListePointGPX.size());
	}
}

void ListeTrackPointGPX::SetPointCourant(double Lat, double Lon, double Vitesse, double Cap, SYSTEMTIME st)
{
	PointCourant.LatDeg = Lat;
	PointCourant.LonDeg = Lon;
	PointCourant.GroundSpeed = Vitesse;
	PointCourant.HeadingDeg = Cap;
	PointCourant.STime = st;
	ListePointGPX.push_back(PointCourant);
}


void ListeTrackPointGPX::StartTimer(int incT)
{
	pTimer = SetTimer(NULL, 0, incT, (TIMERPROC)&GetMesureCourant);
}


void ListeTrackPointGPX::StopTimer()
{
	if (pTimer) KillTimer(NULL, pTimer);
}



bool DoReleveTrackPoint(char *msgerr)
{
	int incT = ParamG.incT_Kalman;

	if (ParamG.ActiveExterneGPS) // on utilise le GPS externe
	{
		if (!sensor)
		{
			sprintf(msgerr, "Echec : il faut tout d'abord initialiser le capteur");
			return false;
		}
		if (!ControlRobotG.RobotIsConnected())
		{
			sprintf(msgerr, "Echec : pas de connexion avec le robot");
			return false;
		}
	}
	else
	{
		if (!ControlRobotG.RobotIsConnected())
		{
			sprintf(msgerr, "Echec : pas de connexion avec le robot");
			return false;
		}
	}

	ListePointsRobotG.StartTimer(incT);
	return true;
}

bool StopReleveTrackPoint(char *msgerr)
{
	if (!ListePointsRobotG.pTimer)
	{
		sprintf(msgerr, "Echec : par de timer �tabli");
		return false;
	}
	ListePointsRobotG.StopTimer();
	return true;
}
