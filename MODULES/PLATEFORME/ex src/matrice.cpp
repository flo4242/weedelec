/*****************************   MATRICE.CPP   ******************************/

#include "matrice.h"

/*************************************************************************/

   //gard� pour compatibilit�
int MATRICE::ChargeF(char *fichier, int NbLignes, int NbColonnes) { return Charge(fichier, NbLignes, NbColonnes);	}
int MATRICE::SauveAscii(char *file)	{ return Sauve(file);	}

/**************************************************************/
bool MATRICE::Charge(char *fichier, int NbLignes, int NbColonnes, int SkipNfirstLines)
/**************************************************************/
{
   ifstream is(fichier);
   if( (!is.is_open()) || (!is.good()))   return false;

   for(int i=0; i<SkipNfirstLines; i++)
   {
	   char dummyline[10000];
	   is.getline(dummyline, 10000);
   }
   		
   istream::pos_type DebutData = is.tellg();
   

   if((NbLignes==0) || (NbColonnes==0))    //non pr�cis�es
		{
		//on les d�termine � partir du fichier

		//nombre caract�res premi�re ligne ?
		char ch;   int cnt=0;
		do
			{
			ch = (char)is.get();          cnt++;
			if(ch=='\n') break;
			}while(!is.fail());

		if(cnt<=1) return false;

	  //determination nombre de colonnes

	  //cnt contient maintenant le nbre de caract�res de la 1�re ligne
	  /* on va determiner le nombre de colonnes. Pour cela, on passe par
		 stream sur buffer car la fonction is.tellg() a un bug (!)
		 (perturbe le curseur de lecture...) */

		char *buf1 = new char[cnt+1];

		is.seekg(DebutData);   //retour au debut de fichier
		is.getline(buf1,cnt);

			{//bloc pour destruction immediate (avant delete [] buf)
			istrstream isb(buf1);
			NbColonnes = 0;
			TYPE_MATRICE dummy;

			int loop = 1;
			while(loop)
				{
				isb>>dummy;
				if(isb.fail())	loop = 0;
				else		   NbColonnes++;
				}
			}
		delete [] buf1;


		//determination nombre de lignes

		int taillemax = NbColonnes*30;
		char *buf2 = new char[taillemax]; // max: 30 caract�res par nombre!!

		NbLignes = 0;
		is.seekg(DebutData);   //retour au debut de fichier

		int loop2 = 1;
		while(loop2)
			{
			is.getline(buf2, taillemax);
			if(is.fail())			loop2 = 0;
			else			NbLignes++;
			}

		delete [] buf2;
		}


//lecture des donnees
// ******************
   MATRICE A(NbLignes, NbColonnes);
   if (!A.M) return false;

   is.close();
   ifstream isd(fichier);  //on reouvre, car le eof interdit d'autres lectures
    for(int i=0; i<SkipNfirstLines; i++)
   {
	   char dummyline[10000];
	   isd.getline(dummyline, 10000);
   }

   int i,j;

   for(i=0;i<NbLignes;i++)
      for(j=0;j<NbColonnes;j++)
         isd>>A[i][j];


   *this = A;
   isd.close ();
   return(true);
}

/**************************** ANCIENNE VERSION AVEC FILE *
int MATRICE::ChargeF(char *fichier, int NbLignes, int NbColonnes)
{
FILE *fich;
fich = fopen(fichier,"rt");
TYPE_MATRICE dummy;

if (!fich) { ErreurMathutil("erreur de lecture\n"); return(0);}

if((NbLignes==0) || (NbColonnes==0))    //non pr�cis�es
        {
        //on les d�termine � partir du fichier


        NbColonnes = 0;


        //nombre caract�res premi�re ligne ?
        char ch;
         int cnt=0;

        while(1)
                {
                if(fread(&ch, 1, sizeof(char), fich)!=1)        {return(0);}
                  if(ch=='\n') break;
                cnt++;
                }

                //cnt contient maintenant le nbre de caract�res de la 1�re ligne
        fseek(fich, 0l, SEEK_SET);      // revient au d�but
        while   (fscanf(fich,"%lf ",&dummy)==1)
                {
                NbColonnes++;
                if(ftell(fich)>=cnt)    break;
                }



        NbLignes = 1;

         char *buf = new char[NbColonnes*30]; // max: 30 caract�res par nombre!!

        do
                {
                //lecture lignes suivantes

                if(!fgets(buf, NbColonnes*30-1, fich))  break;

                NbLignes++;

                }while(1);

        delete [] buf;
        }


MATRICE A(NbLignes, NbColonnes);
if (!A.M) return 0;

fseek(fich, 0l, SEEK_SET);      // revient au d�but
fclose(fich);

//remplissage de la matrice

int i,j;
ifstream is(fichier);

        for(i=0;i<NbLignes;i++)
                 for(j=0;j<NbColonnes;j++)
                         {
//                      fscanf(fich, "%lf ", &dummy);
//                      A[i][j] = dummy;
                        is>>A[i][j];
                          }




                          *this = A;

return(1);
}

**************************************/

/*********************************/
bool MATRICE::Sauve(char *file)
/*********************************/

{
   int line,col;


   ofstream os(file);
   if (!os.good())	return false;

   os.precision(16);

   for(line=0;line<l;line++)
   {
      for(col=0;col<c;col++)                os<<M[line][col]<<'\t';
      os<<'\n';
   }
   os.close ();
   return(true);
}


/***************************/
void MATRICE::LibereM(void)
/***************************/
{  
   int y;
   if(M)
   {
      for(y = 0; y < l; y++)
      {if(M[y])       {delete [] M[y]; M[y]=NULL; }   }
      delete [] M;    M = NULL;
   }

   l=c=0;
}


/*************************************************************/
void MATRICE::AlloueM(int l0, int c0, bool *err, char *msg1000)
/*************************************************************/
//allocation m�moire pour matrice [l0][c0]
{
if(err) *err = false;

   int PbMemoire=0;
   LibereM();      //au cas o�...(utilise valeurs pr�c�dentes l et c)
   int y;
   l=l0; c=c0;

   if (l==0 || c==0)
      M = NULL;
   else
   {
      M = new TYPE_MATRICE*[l];       //un pointeur par ligne
      
      if(!M)  PbMemoire=1;
      else    //allocation pour chaque ligne
         for(y = 0; y < l; y++)
         {
            M[y] = new TYPE_MATRICE[c0];
            if(!M[y])       {PbMemoire=1;}
         }
      
      if(PbMemoire)   //on ressort avec M = NULL
         //lib�ration de ce qui a �t� allou� avant saturation:
      {
         LibereM();
		if(!err)   ErreurMathutil("\nALLOUE: erreur memoire"); 
         else { sprintf(msg1000, "\nALLOUE: erreur memoire"); *err = true; }
      }
   }
}


MATRICE::MATRICE(int i , int j)
{
   pRef = new int; *pRef = 1; M=NULL;
   AlloueM(i,j);
}



MATRICE::MATRICE(const MATRICE &Mat)
{
   memcpy(this, &Mat, sizeof(MATRICE));    //recopie des champs
   (*pRef)++;      // une r�f�rence de plus...
}


MATRICE::~MATRICE()
{
   (*pRef)--; if(*pRef==0) {delete pRef;   LibereM(); }
}


/*********************************************/
void MATRICE::operator = (const MATRICE &Mat)
/*********************************************/
{

// d�truit le contenu actuel:
   (*pRef)--; if(*pRef==0) {delete pRef;   LibereM();}
// et recopie les nouvelles valeurs:
   memcpy(this, &Mat, sizeof(MATRICE));    //recopie des champs
   (*pRef)++;      // une r�f�rence de plus...
}


/****************************/
MATRICE MATRICE::Copie(void)
/****************************/
{
   MATRICE Mat(l, c);      //inclue la lib�ration matrice pr�c�dente
   int y;
   if(Mat.M)       // si allocation OK
      for(y = 0; y < l; y++)
         memcpy(Mat.M[y], M[y], c *sizeof(TYPE_MATRICE)) ;
   return(Mat);
}

/************************************************/
MATRICE MATRICE :: operator+(const MATRICE &Mat)
/************************************************/
{
   MATRICE pnt(l,c);
   int i,j;
   if(pnt.M)
   {
      if ((l==Mat.l) & (c==Mat.c))
      {
         for(i=0 ; i<l ; i++)
            for(j=0 ; j<c ; j++)
               pnt.M[i][j] = M[i][j]+Mat.M[i][j];
      }
      else
      {
         ErreurMathutil
            ("\n ICI MATRICE::operator+() : DIMENSIONS INCOMPATIBLES") ;
         exit(1);
      }
   }
   return pnt;
}

/************************************************/
MATRICE MATRICE :: operator+=(const MATRICE &Mat)
/************************************************/
{
   int i,j;
   if ((l==Mat.l) & (c==Mat.c))
   {
      for(i=0 ; i<l ; i++)
         for(j=0 ; j<c ; j++)
            M[i][j] += Mat.M[i][j];
   }
   else
   {
      ErreurMathutil
         ("\n ICI MATRICE::operator+() : DIMENSIONS INCOMPATIBLES") ;
      exit(1);
   }
   return (*this);
}
         

/************************************************/
MATRICE MATRICE :: operator-(const MATRICE &Mat)
/************************************************/
{
   MATRICE pnt(l,c);
   int i,j;
   if(pnt.M)
   {
      if ((l==Mat.l) & (c==Mat.c))
      {
         for(i=0 ; i<l ; i++)
            for(j=0 ; j<c ; j++)
               pnt.M[i][j] = M[i][j]-Mat.M[i][j];
      }
      else
      {
         ErreurMathutil
            ("\n ICI MATRICE::operator-() : DIMENSIONS INCOMPATIBLES") ;
         exit(1) ;
      }
   }
   return pnt;
}


/************************************************/
MATRICE MATRICE :: operator*(const MATRICE &Mat)
/************************************************/
{
   MATRICE pnt(l,Mat.c);

   int i,j,k;

   if(pnt.M)
   {
      if (c == Mat.l )
      {
         for(i=0 ; i<l ; i++)
            for(j=0 ; j<Mat.c ; j++)    //calcul du terme [i][j]
            {
               pnt.M[i][j] = 0 ;
               for(k=0 ; k<c ; k++)
                  pnt.M[i][j] += M[i][k]*Mat.M[k][j];
            }
      }

      else
      {
         ErreurMathutil
            ("\n ICI MATRICE::operator*()  : DIMENSIONS INCOMPATIBLES") ;
         exit(1) ;
      }
   }
   return pnt;
}


/********************************************/
MATRICE MATRICE :: operator*(TYPE_MATRICE k)
/********************************************/
{
   MATRICE pnt(l,c);
   int i,j;
   if(pnt.M)
      for(i=0 ; i<l ; i++)
         for(j=0 ; j<c ; j++)
            pnt.M[i][j] = M[i][j] * k ;
   return  pnt;
}


/**********************************/
MATRICE MATRICE :: operator~(void)
/**********************************/
{                                      // Transpos�e !!
   MATRICE pnt(c,l);
   int i,j;
   if(pnt.M)
      for(i=0 ; i<l ; i++)
         for(j=0 ; j<c ; j++)
            pnt.M[j][i] = M[i][j];
   return  pnt;
}


/*********************************/
VECTEUR MATRICE :: Colonne(int j)
/*********************************/
{
   VECTEUR U(l) ;
   int i;
   if(U.V)
   {
      for(i=0 ; i<l ;i++)
         U[i] = M[i][j] ;
   }
   else { ErreurMathutil("erreur memoire"); exit(0); }
   return U ;
}


/*********************************/
VECTEUR MATRICE :: Ligne(int i)
{
   VECTEUR U(c);
   int j;
   if (U.V)
   {
      for(j=0; j<c; j++)
         U[j] = M[i][j];
   }
   else { ErreurMathutil("erreur memoire"); exit(0); }

   return U ;
}
/*********************************/
void MATRICE :: Ligne(int n,VECTEUR V)
{
   if (c == V.dim())
   {
      for (int i=0;i<V.dim();i++)
         M[n][i] = V[i];
   }
   else ErreurMathutil
           (" ICI MATRICE::Ligne(i,V) dimensions incompatibles!!");
}
/*****************************************/

/*****************************************/
void MATRICE :: Colonne(int n, VECTEUR V)
{
   if (l == V.dim())
   {
      for (int i=0;i<V.dim();i++)
         M[i][n] = V[i];
   }
   else ErreurMathutil
           ("ICI MATRICE::Colonne(i,V) dimensions incompatibles!!");
}
/*****************************************/
void MATRICE :: Constante(TYPE_MATRICE r)
/*****************************************/
{
   int i,j;
   for(i=0 ; i<l ; i++)
      for(j=0 ; j<c ; j++)
         M[i][j]=r;
}

/*****************************************************/
void MATRICE::InitPartielle(int lig, int col, MATRICE &MM)
{
   int imax = min(l, lig+MM.l);
   int jmax = min(c, col+MM.c);
   int i,j;
   for(i = lig; i<imax ; i++)
      for(j = col; j< jmax; j++)
         M[i][j]=        MM[i-lig][j-col];
}

MATRICE MATRICE::ExtraitPartielle(int lig, int col, int diml, int dimc)
/*********************************************************************/
{
   int imax = min(l, lig+diml);
   int jmax = min(c, col+dimc);
//r�ajustement �ventuel de la dimension matrice r�ceptrice
   diml = imax - lig;
   dimc = jmax - col;
   int i,j;
   MATRICE MM(diml, dimc);
   for(i = lig; i<imax ; i++)
      for(j = col; j< jmax; j++)
         MM[i-lig][j-col] = M[i][j];
   return MM;
}

/*******************************/
double MATRICE :: Norme()
/*******************************/
{
   double CarNorme = 0 ;
   int i,j;
   for(i=0 ; i<l ; i++)
      for(j=0 ; j<c ; j++)
         CarNorme += (double)(M[i][j]*M[i][j]);

   return(CarNorme >= ABSMIN ? sqrt(CarNorme): 0) ;
}

/**************************************************/
void MATRICE :: Affiche(ostream &os, int a, int b)
/**************************************************/
{
   int i,j;
   for(i=a ; i<min(l,a+10) ; i++)
   {
      os<<'\n' ;
      for(j=b ; j<min(c,b+20) ; j++)	         os<<M[i][j]<<"  ";
   }
      os<<'\n' ;
}
/****************************************************/
MATCARREE :: MATCARREE( MATRICE M) : MATRICE(M)
/****************************************************/
//sortie si casting impossible !!
{if(l!=c)
 {
    ErreurMathutil
       ("\n ICI casting MATCARREE(MATRICE): DIMENSIONS INCOMPATIBLES") ;
    exit(1);
 }
}
/****************************************************/
void MATCARREE :: DiagonaleConstante(TYPE_MATRICE d)
/****************************************************/
{
   int i,j;
   for(i=0 ; i<l ; i++)
      for(j=0 ; j<l ; j++)
         if(j!=i)
            M[i][j] = 0 ;
         else
            M[i][j] = d ;

}

/****************************************************/
void MATCARREE :: Remplitdiagonale(VECTEUR &V)
/****************************************************/
{
   int i,j;
   if (V.dim()!=l)
   {ErreurMathutil("erreur dimension dans Remplitdiagonale!\n"); exit(0); }
   for(i=0 ; i<l ; i++)
      for(j=0 ; j<l ; j++)
         if(j!=i)
            M[i][j] = 0 ;
         else
            M[i][j] = V[i];
}

/*
 // ********************************************************************
int MATCARREE :: DiagonaliseSymetrique(VECTEUR &ValP, MATCARREE &VectP, int nbrotmax)
 // ********************************************************************
 //reserve aux matrices symetriques
 //retourne le nb de rotations Jacobi n�cessaires
 //(d'apres Numerical Recipes 2nde edition p467)
 {
 MATCARREE A = this->Copie();    //car sera modifiee
     if( (A-~A).Norme()>ABSMIN)
     {
     ErreurMathutil("matrive non symetrique dans DiagonaliseParJacobi!\n");
     exit(0);
     }
 VectP = MATCARREE(1); VectP.Identite();   

 VECTEUR B(l), D(l), Z(l);
 //initialisation B et D selon la diagonale de A:
 int i,j,k;
 for(i=0; i<l; i++)  B[i]=D[i]= A[i][i];
 Z.Constante(0); //initialise accumulateur

 int nbrot=0;    //nb de rotations de Jacobi

 for(i=0; i<nbrotmax; i++)
     {
     //sommation des termes hors diagonale
     double sm=0;
     for(j=0; j<l-1; j++)        for(k=j+1; k<l; k++)        sm += fabs(A[j][k]);

     if(fabs(sm)<ABSMIN)      return i;
     //plus de termes non diagonaux: fin normale
     }

 return 0;
 }

 */
/*****************************/
int MATRICE :: MatNulle(void)
/*****************************/
{
   int i,j=0;

   for(i=0 ; i<l ; i++)
      for(j=0 ; (j<c) && (M[i][j]==0 ) ; j++)
      {
      }
   return((i*j) == (l*c)) ;
}
/**************************************************************************/
MATCARREE MATRICE::Covariance(void)
{
   int i,j;
   MATRICE A = Copie();//appel du constructeur par recopie.

   for(i=0;i<c;i++)//pour chaque dimension.
   {
      VECTEUR V = this->Colonne(i);//extraction de la colonne i;
      TYPE_VECTEUR Moyenne = V.Moy();//calcul de la moyenne.
      for(j=0;j<l;j++)
         V[j] = V[j] - Moyenne;//diff�rence avec la moyenne.
      A.Colonne(i,V);//on remplie la colonne de A avec V;
   }

   MATCARREE out = (~A * A) * (1./(TYPE_VECTEUR)(l-1.));
   return out;
}
/**************************************************************************/
VECTEUR MATCARREE :: VALP(bool *err, char *msg1000)
{
if(err) *err = false;
// MATRICE cop = Copie();
   MATCARREE T(Copie());

   VECTEUR V1(T.dimC());
   VECTEUR V2(T.dimC());
   T = T.Tred2(V1,V2);
   MATCARREE Q;
   Q = T.Tqli(V1,V2, err, msg1000);
   if (!Q.M)
   {
      if(!err) ErreurMathutil("MATCARREE :: VALP probleme tqli"); 
      else { sprintf(msg1000, "MATCARREE :: VALP probleme tqli"); *err = true; }
      return VECTEUR(0);
   }
   this->eigsrt(V1,Q);
   return V1;
}
/**************************************************************************/
MATRICE MATRICE :: Normalisee(VECTEUR &E)
{
   VECTEUR Et(c); // pour �cart-type de chaque colonne

   MATRICE A = this->Copie();
   if (!A.M||!E.V)
   {ErreurMathutil("probleme memoire dans MATRICE::Normalisee"); exit(0);}
   for (int i=0;i<c;i++)
   {
      VECTEUR V = A.Colonne(i);
      Et[i] = V.Ecarttype();
      if (Et[i]!=0) V = V * (1./Et[i]);
      A.Colonne(i,V);
   }
   E = Et.Copie();
   return A;
}
/*************************************************************************/
MATRICE MATRICE :: Reduit(VECTEUR &E)
{
   MATRICE A = this->Copie();
   if (!A.M)
   {ErreurMathutil("probleme memoire dans MATRICE::Reduit"); exit(0);}

   for (int i=0;i<c;i++)
   {
      VECTEUR V = A.Colonne(i);
      if (E[i]!=0) V = V * (1./E[i]);
      A.Colonne(i,V);
   }
   return A;
}
/**************************************************************************/
MATRICE  MATRICE :: Centre(VECTEUR &Moyenne)
{
   int i;
   MATRICE T = this->Copie();
   VECTEUR A(c);//vecteur interne des moyennes de chaque colonne.
   VECTEUR M(l);//vecteur de travail.
   if (!T.M || !A.V || !M.V)
   { ErreurMathutil("MATRICE::centre:  probleme de memoire!"); exit(0); }

   for(i=0;i<c;i++)
   {
      VECTEUR V = this->Colonne(i);//extraction de la colonne i.
      A[i] = V.Moy();//calcul de la moyenne.
      M.Constante(A[i]);//cast d'une valeur dans un vecteur...
      //T.Colonne(i,(V-M));//Colonne i centr�e.
      // VECTEUR VM = V-M;
      T.Colonne(i,(V-M));//Colonne i centr�e.
   }
   Moyenne = A.Copie();
   return(T);
}
/***************************************************************************/
MATRICE  MATRICE :: Decale(VECTEUR &Moyenne)
{
   MATRICE T = this->Copie();
   VECTEUR M(l);
   int i;
   for(i=0;i<c;i++)
   {
      VECTEUR V = this->Colonne(i);//extraction de la colonne i.
      M.Constante(Moyenne[i]);//cast d'une valeur dans un vecteur...
      T.Colonne(i,(V-M));//Colonne i centr�e.
   }
   return(T);
}
/**************************************************************************/
MATCARREE MATCARREE :: VECTP(void)
{
   //MATCARREE T = this->Copie(); ne fonctionne pas avec C++builder
  
   MATCARREE T(Copie());

   VECTEUR V1(T.dimC());
   VECTEUR V2(T.dimC());
   T = T.Tred2(V1,V2);//retourne V1 et V2.
   MATCARREE Q;
   Q = T.Tqli(V1,V2);
   this->eigsrt(V1,Q);
   return Q;
}
/*************************************************************************/
void MATCARREE :: eig(VECTEUR &Val,MATCARREE &Vec)
{
   //MATCARREE T = this->Copie(); ne fonctionne pas avec C++builder
   MATRICE cop = Copie();
   MATCARREE T(cop);

   VECTEUR V1(T.dimC());
   VECTEUR V2(T.dimC());
   T = T.Tred2(V1,V2);
   MATCARREE Q;
   Q = T.Tqli(V1,V2);
   Val = V1;
   Vec = Q;
}
/*********** version Fred: contient un ordonnancement des vp!!!
void MATCARREE :: eig(VECTEUR &Val,MATCARREE &Vec)
{
  MATCARREE T = this->Copie();

  VECTEUR V1(T.dimC());
  VECTEUR V2(T.dimC());
  T = T.Tred2(V1,V2);
  MATCARREE Q = T.Tqli(V1,V2);
  this->eigsrt(V1,Q);
  Val = V1.Copie();
  Vec = Q.Copie();
}
****************************************************************/
/*************************************************************************/
MATRICE MATRICE :: operator&(MATRICE &S)
{
   int i;
   if (dimL()==1 && dimC()==1)   return S.Copie();
   if ( dimC()!=S.dimC())
   {printf("probleme dimensions dans MATRICE::operator &"); exit(0);}
   MATRICE p(dimL() + S.dimL(),dimC());
   if (!p.M)
   {printf("probleme memoire dans MATRICE::operator &"); exit(0);}
   for(i=0;i<p.dimL();i++)
      if (i<dimL())//la premi�re matrice.
      {
         VECTEUR A = Ligne(i);
         p.Ligne(i,A);
      }
      else//la deuxi�me matrice.
      {
         VECTEUR A = S.Ligne(i - dimL());
         p.Ligne(i,A);
      }
   return p;
}
/*************************************************************************/
//concat�nation sur les lignes.
MATRICE MATRICE :: operator/(MATRICE &S)
{
   int i;
   if (dimL()==1 && dimC()==1)   return S.Copie();

   if ( dimL()!=S.dimL())
   {ErreurMathutil("probleme dimensions dans MATRICE::operator /");exit(0);}

   MATRICE p(dimL(),S.dimC() + dimC());//nouvelle matrice.
   if (!p.M)
   {ErreurMathutil("probleme memoire dans MATRICE::operator /");exit(0);}

   for(i=0;i<p.dimC();i++)
      if (i<dimC())//la premi�re matrice.
      {
         VECTEUR A = Colonne(i);//extraction de la colonne.
         p.Colonne(i,A);
      }
      else//la deuxi�me matrice.
      {
         VECTEUR A = S.Colonne(i - dimC());
         p.Colonne(i,A);
      }
   return p;
}
/*************************************************************************/
MATRICE  MATRICE :: CoupeC(int x0,int x1)
{
   int i;

   if ((x1 - x0)<=0)
   { ErreurMathutil("probleme dans MATRICE::CoupeL");      exit(0); }

   MATRICE T(l,(x1-x0)); //matrice � construire.
   for (i=x0;i<x1;i++)//pour toutes les composantes conserv�es.
   {
      VECTEUR A = Colonne(i);
      T.Colonne((i - x0),A);
   }
   return T;
}
/*************************************************************************/
MATRICE MATRICE :: CoupeL(int x0,int x1)
{
   int i;

   if ((x1 - x0)<=0)
   { ErreurMathutil("probleme MATRICE::CoupeL");exit(0); }
   MATRICE T((x1-x0),c); //matrice � construire.
   if(!T.M)
   { ErreurMathutil("probleme memoire dans MATRICE::CoupeL");exit(0); }

   for (i=x0;i<x1;i++)//pour toutes les composantes conserv�es.
   {
      VECTEUR A = Ligne(i);
      T.Ligne((i - x0),A);
   }
   return T;
}

void MATCARREE::Identite(void)
/****************************/
{ VECTEUR V(l); V.Constante(1); Remplitdiagonale(V);    }

/*************************************************************************/
