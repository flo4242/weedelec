#include <stdio.h>
#include <math.h>
#include "pseudo.h"
#include "gpsclass.h"
#include "texte.h"
#include "utilfic.h"
#include "winparam.h"
#include "matrice.h"
#include "kalman.h"

extern float ElevationMinDeg;
extern float ElevationMaxDeg;

class TKalman1: public SYSTEMEKALMAN
	{
	protected:

	//matrices de calcul
	virtual MATCARREE Fk();	//retourne la matrice dXk+1 / dXk
	virtual MATRICE Hk();		//retourne la matrice d Observations/ d Xk
	virtual MATCARREE Qk();	//retourne la covariance instantan_e
	virtual MATCARREE Rk();	//retourne la covariance sur observations

	//fonctions de calcul
	virtual int EtatSuivantNonCorrige(void);
	virtual VECTEUR PredictionObservations();
	virtual VECTEUR MesureObservations();
	virtual void CorrigeEtat(VECTEUR deltaX);
	virtual void Init(void);

	public:

   double x,y,z;
   double theta;
   double v;
   double deltaH;

   double deltaT;

   //membres de calcul rajout�s:

   MATCARREE Q0;
  	MATCARREE FF;     //seule une petite partie est variable
   MATRICE HH;	//d�pend des satellites en pr�sence: calcul�e par programme appelant
   MATRICE RR;	//d�pend des satellites en pr�sence: calcul�e par programme appelant
   VECTEUR Obs;	//d�pend des satellites en pr�sence: calcul�e par programme appelant
	VECTEUR EtatCourant();
	};

//variances initiales
static double sigmax0 = 10000;	//m�tres
static double sigmatheta0 = M_PI;	//radians
static double sigmav0 = 1E4;	//m/s
static double sigmadeltaH0 = 1E8;	//m	(soit c/3)

//variances instantan�es
static double sigmax = 0;	//m�tres
static double sigmatheta = M_PI/10;	//radians
static double sigmav = 0.01;	//m/s
static double sigmadeltaH = 1000;	//m	(soit c/3)

static double sigmaPS = 10;	//m	(soit c/3)

MATRICE TKalman1::Hk()	{return HH;	}		//retourne la matrice d'observations/ d Xk
MATCARREE TKalman1::Qk()	{ return Q0;	}	//retourne la covariance instantan_e
MATCARREE TKalman1::Rk()	{return RR;	}		//retourne la matrice d'observations/ d Xk

void TKalman1::Init(void)
/***********************/
{
dim = 6;

x=y=z= 0;
theta= 0; //M_PI/2;
v=0;
deltaH=0;
deltaT= 1;	//seconde

P = MATCARREE(dim); P.Constante(0);
P[0][0]=P[1][1]=P[2][2]= sigmax0*sigmax0;
P[3][3]= sigmatheta0*sigmatheta0;
P[4][4]= sigmav0*sigmav0;
P[5][5]= sigmadeltaH0*sigmadeltaH0;

Q0 = MATCARREE(dim); Q0.Constante(0);
Q0[0][0]=Q0[1][1]=Q0[2][2]= sigmax*sigmax;
Q0[3][3]= sigmatheta*sigmatheta;
Q0[4][4]= sigmav*sigmav;
Q0[5][5]= sigmadeltaH*sigmadeltaH;


FF = MATCARREE(dim);	FF.Identite();
FF[0][3] = v*deltaT*cos(theta);
FF[1][3] = -v*deltaT*sin(theta);

FF[0][4] = deltaT*sin(theta);
FF[1][4] = deltaT*cos(theta);
}

MATCARREE TKalman1::Fk()	//retourne la matrice dXk+1 / dXk
/**********************/
{
FF[0][3] = v*deltaT*cos(theta);
FF[1][3] = -v*deltaT*sin(theta);

FF[0][4] = deltaT*sin(theta);
FF[1][4] = deltaT*cos(theta);
return FF;
}


int TKalman1::EtatSuivantNonCorrige(void)
/***************************************/
{
x += v*deltaT*sin(theta);
y += v*deltaT*cos(theta);
return 1;
}

VECTEUR TKalman1::EtatCourant()
/*****************************/
{
VECTEUR V(6);
V[0]=x;
V[1]=y;
V[2]=z;
V[3]=theta;
V[4]=v;
V[5]=deltaH;
return V;
}

VECTEUR TKalman1::PredictionObservations()
/****************************************/
{
VECTEUR X = EtatCourant();
VECTEUR Y = (HH*X).Colonne(0);
return Y;
//return (HH*X).Colonne(0);
}


VECTEUR TKalman1::MesureObservations()
/************************************/
{	return Obs;	}


void TKalman1::CorrigeEtat(VECTEUR V)
/****************************************/
{
x+=V[0];
y+=V[1];
z+=V[2];
theta += V[3];
if(theta>= 2*M_PI)	theta -= 2*M_PI;
if(theta< 0)	theta += 2*M_PI;
v +=V[4];	v = max(v,0.0);	//v est une norme de vitesse
deltaH += V[5];
}




int HHObsRR(TGPSVectDiff diff, TGPSVectPos Pos, TKalman1 &K)
/*********************************************************/
{
//nb de satellites pour la r�solution (de 1 a 12...)
//necessaire pour allouer les matrices
int tabindex[NBSAT];	//contiendra les index des nbsat satellites retenus

int nbsat = 0;
int k;
for(k = 0; k< NBSAT; k++)
	{
//   int u = diff.NumSat(k);
	if(diff.NumSat(k))	//a pu etre annule lors de l'interpolation positions
	if(diff.Ponderation(k)>1E-30)
		{
		int dummy;
		if(Pos.RetrouveSat(dummy, diff.NumSat(k))) //et on a la position...
			{
			//XXXXXXXXXX 	AVRIL 2000
			TYPE_MATRICE elevation = Pos.Elevationdeg(dummy);
			if(elevation>ElevationMinDeg)
			if(elevation<ElevationMaxDeg)
				{	tabindex[nbsat]=k;	nbsat++;	}
			}
		 }
	}
if(nbsat < 1)
	return 0;

MATRICE X(6,nbsat);	//matrice des ui
VECTEUR Y(nbsat);	//matrice des PSi

MATCARREE R(nbsat);  R.Constante(0);
double sommepond=0;
//initialisation des matrices X, Y et R
for(int n = 0; n< nbsat; n++)
	{
	int nn =	tabindex[n];
	int sat = diff.NumSat(nn);

	Pos.RetrouveSat(k, sat);	//position du satellite concerne -> dans k

	TYPE_MATRICE elevation = Pos.Elevationdeg(k)*M_PI/180;
	TYPE_MATRICE azimut = Pos.Azimutdeg(k)*M_PI/180;


	X[0][n] = cos(elevation) * sin(azimut);
	X[1][n] = cos(elevation) * cos(azimut);
	X[2][n] = sin(elevation);
	X[3][n] = 0;
	X[4][n] = 0;
	X[5][n] = 1;    //pour obtenir H en metres

	Y[n] = -diff.PseudoRange(nn); //ecart pseudoranges
	//signe moins pour retrouver le bon signe sur DM

   R[n][n] = sigmaPS * sigmaPS /diff.Ponderation(nn);
   sommepond += 1/diff.Ponderation(nn);
	}
if(sommepond>1E-30)	R = R *(1/sommepond);
K.HH = ~X;	K.Obs = Y;	K.RR = R;
//K.HH = MATRICE(4,6);	K.Obs = VECTEUR(4);	K.RR = MATCARREE(4);
//K.HH.Constante(0);	K.Obs.Constante(0);	K.RR.Constante(1);
return 1;
}


void MyWin::CMKalman1(void)
/*************************/
{

GROUPE G(this, "Covariances", "kalman.cfg",
DBL("sigmax0", sigmax0, 0, 1E6 ),
DBL("sigmatheta0", sigmatheta0, 0, 2*M_PI ),
DBL("sigmav0", sigmav0, 0, 1E6 ),
DBL("sigmadeltaH0", sigmadeltaH0, 0, 1E10 ),
DBL("sigmax", sigmax, 0, 1E6 ),
DBL("sigmatheta", sigmatheta, 0, 2*M_PI ),
DBL("sigmav", sigmav, 0, 1E6 ),
DBL("sigmadeltaH", sigmadeltaH, 0, 1E10 ),
DBL("sigmaPS", sigmaPS, 0, 1E10 ),
NULL);

G.Gere();

Efface();

//ResiduG =0;

	//chargement de la matrice des positions satellites
TNomFic sourceS = "";
if(!ChoixFichierOuvrir("positions satellites", sourceS, "ref","dirdat.cfg"))	return;
 // mieux vaut charger poss.txt car la station voit plus de sat. que le mobile
TGPSPos Position;

if(Position.ChargeF(sourceS))
	{
	printf("\nmatrice positions charg�e: %d lignes, %d colonnes",
	Position.dimL(), Position.dimC());
	}
else
	{
	printf("\nEchec chargement matrice positions");
	return;
	}

	//chargement de la matrice des differences
TGPSDiff DIFF;

if(DIFF.ChargeF("diff.txt"))
	{
	printf("\nmatrice diff�rences charg�e: %d lignes, %d colonnes",
	DIFF.dimL(), DIFF.dimC());
	}
else
	{
	printf("\nEchec chargement matrice diff�rences");
	return;
	}

MATRICE SOL(DIFF.dimL()-1, 7);	SOL.Constante(0);	//on exclut la premi�re it�ration
MATRICE RESIDU(DIFF.dimL(), 2);
MATRICE POS(DIFF.dimL(),Position.dimC()+1 );

TKalman1 K1;
int nn=0;

for(int i = 0; i < DIFF.dimL(); i++)
	{

	//recherche de la position satellite dispo la plus proche en temps:
	TYPE_MATRICE min = 1E30;	//arbitrairement grand
	TYPE_MATRICE t = DIFF.Temps(i);
	int index;
	for(int k=0; k < Position.dimL(); k++)
		{
		TYPE_MATRICE ecart = fabs(t-Position.Temps(k));
		if(ecart < min)	{ min = ecart; index = k;	}
		}

	int index2;
	if(t>Position.Temps(index))	//date anterieure
		index2 =(index< Position.dimL()-1 ? index+1: index);

	else
		{index2 = index;			index = max(index2-1, 0);	}

	//maintenant index et index2 entoures le temps courant

	TYPE_MATRICE lambda;

	if(index2>index)
		lambda = (t-Position.Temps(index))/
				(Position.Temps(index2)- Position.Temps(index));

	else lambda = 1; // index == index2
	TGPSVectPos P1(Position, index);
	TGPSVectPos P2(Position, index2);

		//interpolation entre P1 et P2 pour toutes les positions:
	for(int n = 0; n< NBSAT; n++)
		{
		int sat = P1.NumSat(n);
		if(sat)
			{
			//recherche dans P2 du satellite correspondant
			//(il a pu y avoir commutation entre deux lignes)
			int m;
			if(P2.RetrouveSat(m, sat))
				{
				P1.SetElevationdeg(n, lambda*P2.Elevationdeg(m)
									+ (1-lambda)*P1.Elevationdeg(n));
				P1.SetAzimutdeg(n, lambda*P2.Azimutdeg(m)
									+ (1-lambda)*P1.Azimutdeg(n));
				}
			else P1.SetNumSat(n, 0);	//interpolation impossible
			}
		 }

	 POS.InitPartielle(i, 1, ~P1);
	 POS[i][0] = t;

		//et resolution du probleme

	 TGPSVectDiff diff(DIFF, i);
	 VECTEUR Solution(6);

  if(!HHObsRR(diff, P1, K1))
		{  	MessageBox("Pb lecteur donn�es satellites (saut de temps)");
				HHObsRR(diff, P1, K1);



      	return;	}


	K1.EtatSuivantCorrige();
   if(nn>0)
   	{
	   SOL.InitPartielle(nn-1, 1, ~K1.EtatCourant());
		SOL[nn-1][0] = t;
      }

   Tracer(5,1, "%d", nn);
	nn++;

	}


if(nn)
	{
	MATRICE SOL2 = SOL.ExtraitPartielle(0,0, nn, SOL.dimC());
	SOL2.SauveAscii("solution.dat");

	char st[1000];
	int z = SOL2.dimL() -1;
	sprintf(st, "deb: x %6Lf y %6Lf z %6Lf \n fin:x %6Lf y %6Lf z %6Lf",
	SOL[0][1], SOL[0][2],SOL[0][3],
	SOL[z][1], SOL[z][2],SOL[z][3]);

	sprintf(st+strlen(st)," \n\n Nb points: %d      Residu moyen: %Lf", nn, 0 /*ResiduG/nn*/);
	sprintf(st+strlen(st)," \n Equivalent pseudorange: %Lf", 0);
//							(TYPE_MATRICE) sqrt(ResiduG/nn));
	MessageBox(st, "", MB_OK);
	}

else	MessageBox("Aucun point trouv�!!!", "", MB_OK);

}


