#include "FiltreKalmanRobotErx.h"


#ifndef ENTRAXE_ROBOT
#define ENTRAXE_ROBOT   2.20 // entraxe: 2.2m
#endif

// les param�tres par d�faut
//variances initiales
static double sigmax0 = 0.5;	//m�tres
static double sigmatheta0 = 0.2;	//radians
static double sigmav0 = 0.5;	//m/s
static double sigmadtheta0 = 0.3;	//rad/s

//variances instantan�es
static double sigmax = 0.2;	//m�tres
static double sigmatheta = 0.05;	//radians
static double sigmav = 0.5;	//m/s
static double sigmadtheta = 0.1;	//rad/s

static double sigmaObsx = 0.2;	//m	(soit c/3)
static double sigmaObstheta = 0.2;


MATRICE TFiltreKalman::Hk()  { return HH; }		//retourne la matrice d'observations
MATCARREE TFiltreKalman::Qk()  { return Q0; }	//retourne la covariance instantan_e
MATCARREE TFiltreKalman::Rk()  { return RR; }		//retourne la matrice d'observations



void TFiltreKalman::Init()
{
	dim = 5;

	x = 0, y = 0;
	theta = 0; 
	v = 0;
	dtheta = 0;

	Vg = 0; Vd = 0;

	deltaT = 1;	//secondes

	// matrice covariance d'erreur 
	P = MATCARREE(dim);
	P.Constante(0);
	P[0][0] = P[1][1] = sigmax0 * sigmax0;
	P[2][2] = sigmav0 * sigmav0;
	P[3][3] = sigmatheta0 * sigmatheta0;
	P[4][4] = sigmadtheta0 * sigmadtheta0;

	// matrice covariance pour l'incertitude mod�le 
	Q0 = MATCARREE(dim);
	Q0.Constante(0);
	Q0[0][0] = Q0[1][1] = sigmax * sigmax;
	Q0[2][2] = sigmav * sigmav;
	Q0[3][3] = sigmatheta * sigmatheta;
	Q0[4][4] = sigmadtheta * sigmadtheta;

	// matrice d'observation (mesure sur x, y et theta):
	HH = MATRICE(3, dim);
	HH.Constante(0);
	HH[0][0] = HH[1][1] = 1;
	HH[2][3] = 1;

	// matrice covariance pour les bruits d'observation
	RR = MATCARREE(3);
	RR.Constante(0);
	RR[0][0] = RR[1][1] = sigmaObsx * sigmaObsx;
	RR[2][2] = sigmaObstheta * sigmaObstheta;
}

MATCARREE TFiltreKalman::Fk()
{
	FF = MATCARREE(dim);	
	FF.Identite();

	FF[0][2] = -deltaT * sin(theta);
	FF[1][2] = deltaT * cos(theta);

	FF[0][3] = -v * deltaT*cos(theta);
	FF[1][3] = -v * deltaT*sin(theta);

	FF[3][4] = deltaT;

	return FF;
}

void TFiltreKalman::EvolutionEtat()
{
	x -= v * deltaT * sin(theta);
	y += v * deltaT * cos(theta);
	theta += deltaT * dtheta;
	v = (Vg + Vd) / 2;
	dtheta = (Vd - Vg) / ENTRAXE_ROBOT;
}

VECTEUR TFiltreKalman::EtatCourant()
{
	VECTEUR V(dim);
	V[0] = x;
	V[1] = y;
	V[2] = v;
	V[3] = theta;
	V[4] = dtheta;
	return V;
}

VECTEUR TFiltreKalman::PredictionObservations()
{
	VECTEUR X = EtatCourant();
	VECTEUR Y = (HH*X).Colonne(0);
	return Y;
	//return (HH*X).Colonne(0);
}

VECTEUR TFiltreKalman::MesureObservations()
/************************************/
{
	return Obs;
}

void TFiltreKalman::CorrigeEtat(VECTEUR V)
/****************************************/
{
	x += V[0];
	y += V[1];
	v += V[2]; v = max(v, 0.0);	//v est une norme de vitesse
	theta += V[3];
	if (theta > M_PI)	theta -= 2 * M_PI;
	if (theta < -M_PI)	theta += 2 * M_PI;
	dtheta += V[4];
}


void TFiltreKalman::ChargePosInit(double x0, double y0, double v0, double thetarad0, double dthetarad0)
/***********************/
{
	x = x0; y = y0; theta = thetarad0;
	v = v0; dtheta = dthetarad0;
}


void TFiltreKalman::InitParametres(double dT, double sig_x0, double sig_theta0, double sig_v0, double sig_dtheta0, double sig_x,
	double sig_theta, double sig_v, double sig_dtheta, double sig_obsx, double sig_obstheta)
{
	deltaT = dT; // p�riode de Kalman en secondes

	// matrice covariance d'erreur 
	P = MATCARREE(dim);
	P.Constante(0);
	P[0][0] = P[1][1] = sig_x0 * sig_x0;
	P[2][2] = sig_v0 * sig_v0;
	P[3][3] = sig_theta0 * sig_theta0;
	P[4][4] = sig_dtheta0 * sig_dtheta0;

	// matrice covariance pour l'incertitude mod�le 
	Q0 = MATCARREE(dim);
	Q0.Constante(0);
	Q0[0][0] = Q0[1][1] = sig_x * sig_x;
	Q0[2][2] = sig_v * sig_v;
	Q0[3][3] = sig_theta * sig_theta;
	Q0[4][4] = sig_dtheta * sig_dtheta;

	// matrice covariance pour les bruits d'observation
	RR = MATCARREE(3);
	RR.Constante(0);
	RR[0][0] = RR[1][1] = sig_obsx * sig_obsx;
	RR[2][2] = sig_obstheta * sig_obstheta;
}


VECTEUR TFiltreKalman::GetEtatCourant()
{
	return EtatCourant();
}

void TFiltreKalman::SetConsigneMoteursActuelle(POINTFLT Consigne)
{
	// consigne moteurs: x-> gauche, y-> droite
	Vg = Consigne.x; Vd = Consigne.y;
}

