#include "ComRS232.h"
#include <stdio.h>


//valeurs de retour de AttenteSequenceAsync()
#define ATTENTESEQUENCE_OK	0
#define ATTENTESEQUENCE_ABORTED	1
#define ATTENTESEQUENCE_ERROR	2
#define ATTENTESEQUENCE_ARRETDEMANDE	3



int AttenteSequenceAsync(HANDLE HCom, char *ReadBuffer, int taillebuffer, const bool &ArretDemande, char *msgerr1000)
/*******************************************************************************************************************/
{
// d'apr�s https://mssqlwiki.com/2012/02/15/asynchronous-io-example/

	OVERLAPPED ol; // = {0}; ????
	ZeroMemory (&ol,sizeof(ol));
		
	//lancement demande de lecture
	bool RF = ReadFile(HCom, ReadBuffer, taillebuffer, NULL, &ol);
			
	if ((RF==0) && GetLastError()==ERROR_IO_PENDING) //situation normale
	{
		//boucle d'attente de r�ception

		DWORD NbLus;
		
		while( !GetOverlappedResult( HCom, &ol, &NbLus, FALSE))
            {
				if(ArretDemande) return ATTENTESEQUENCE_ARRETDEMANDE;

				if (GetLastError()==ERROR_IO_INCOMPLETE)
				//r�ception toujours en attente; on transmet les messages courants en attendant
                {
					MSG msg;
					if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 
					//si PM_NOREMOVE, �a tourne en rond sur le m�me message
					{
						TranslateMessage(&msg);
						DispatchMessage(&msg);
					}
				}
					
				else //autre probl�me
                {
					if(GetLastError()==ERROR_OPERATION_ABORTED)
					{
						return ATTENTESEQUENCE_ABORTED;
						//possible si on a chang� de port entretemps via la bo�te de dialogue
					}

					sprintf(msgerr1000,"GetOverlappedResult failed with error:%d", GetLastError());
					return ATTENTESEQUENCE_ERROR;
                }

            } //fin !GetOverlappedResult()

	}


	else if ((RF==0)  && GetLastError()!=ERROR_IO_PENDING)
    {
		sprintf(msgerr1000,"Error reading file :%d", GetLastError());
		return ATTENTESEQUENCE_ERROR;
    }
      
	return ATTENTESEQUENCE_OK;

}


HANDLE InitComAsync(char *nomPort)
/**************************/
{

	char st[20] = "\\\\.\\";
	strcat(st, nomPort);
	// voir http://support.microsoft.com/default.aspx?scid=kb;EN-US;q115831


	HANDLE H =  CreateFile(st, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, 0);

	if(H==INVALID_HANDLE_VALUE) return NULL;
	
	SetupComm(H, 2000, 2000);
	return H;

#ifdef bof
COMMTIMEOUTS TimeOut;
TimeOut.ReadIntervalTimeout = 200;
//pour sortie en fin de r�ception d'une trame

TimeOut.ReadTotalTimeoutMultiplier = 0;
TimeOut.ReadTotalTimeoutConstant = 2000; //pour sortie si r�ception inexistante
TimeOut.WriteTotalTimeoutMultiplier = 50;
TimeOut.WriteTotalTimeoutConstant = 0;
/*TimeOut.ReadIntervalTimeout = 0;	//MAXDWORD;
TimeOut.ReadTotalTimeoutMultiplier = 0;
TimeOut.ReadTotalTimeoutConstant = 0;
TimeOut.WriteTotalTimeoutMultiplier = 0;
TimeOut.WriteTotalTimeoutConstant = 0; */
return SetCommTimeouts(HComGpsG, &TimeOut);
#endif
}

