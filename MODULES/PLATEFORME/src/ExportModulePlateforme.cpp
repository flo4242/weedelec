
#include "ControlRobot.h"
#include "ExportModulePlateforme.h"
#include "Plateforme.h"
#include "AppliqueFiltreKalmanErx.h"
#include "CommandeNavigation.h"
#include "KML.h"
#include "DialogueHauteTension.h"

#include <Windows.h>
#include <ppltasks.h> // pour multitasking
#include <agents.h> // pour timer


using namespace Concurrency;

#define TIMEOUT_STEP  10000	// time-out p�riode pour avancer � la position suivante
#define DELAY_INITARM  3000  // delay pour l'initialisation de bras
#define DELAY_RESETARM  1500  // delay pour le reset de bras (� la position d'origine)


#define VARIATION_POSROBOT  0.05    // variation de mesures position robot inf�rieure � 0.002m / SLEEP_POSROBOT
#define SLEEP_POSROBOT      1000     // delay pour �tre s�r que les mesures ne changent plus apr�s l'arr�t du robot

extern TParamRobot ParamG;
extern TControlRobot ControlRobotG;
extern TAppliqueKalman AppliqueKalmanG;
extern CommandeHT CommandeHTG;
extern TControlBrasManuel ControlBrasManuelG;

extern TRepereLocal *RepereG;
extern TCommandeNavigation *CommandeNavigationG;
extern Hexagone *pHexagoneBras;


bool StopUrgenceG = false;


bool TestArretAvancement(POINTFLT Debut, POINTFLT PosActuelle, double distance,
	int TimeDebut, TRetourState &state)
{
	double dist2 = (PosActuelle - Debut).Norme2();

	if (dist2 > distance*distance) // v�rification d'arriv�e de point suivant
	{
		state = TRetourState::Completed;
		return true;
	}

	if (GetTickCount() - TimeDebut > TIMEOUT_STEP)
	{
		state = TRetourState::TimeOut;
		return true;
	}

	if (StopUrgenceG)
	{
		state = TRetourState::ExternalStop;
		return true;
	}

	return false;
}

void TModulePlateforme::AvancementSimple(TRetourPlateforme &ret, double distance, double vitesse)
{
	int TimeDebut = GetTickCount();

	StopUrgenceG = false;

	if (!RepereG || !ControlRobotG.RobotIsConnected())
	{
		RetourneStateError(ret, TimeDebut, "Robot non initialis�");
		return;
	}

	double latDeg, lonDeg, capDeg;
	char msgerr[1000];

	TPositionRobot Pos;
	bool GPSExterne = ParamG.GPSExterne;

	if(!GetPositionRobotBrute(Pos, GPSExterne, *RepereG, msgerr))
	{
		RetourneStateError(ret, TimeDebut, msgerr);
		return;
	}

	POINTFLT Pdebut(Pos.x, Pos.y);
	
	task_completion_event<void> tce;

	auto callback_step = new call<bool>([distance, vitesse, tce, &ret, GPSExterne, Pdebut, TimeDebut](bool) // fonction callback qui est appel�e pour la commande
	// cr�ation de fonction callback pour la t�che d'avancement du robot
	{
		ControlRobotG.SetRobotSpeed(vitesse, vitesse); //commande des moteurs
		
		TPositionRobot Pos;
		char msgerr[1000];
		if (!GetPositionRobotBrute(Pos, GPSExterne, *RepereG, msgerr))
		{
			ControlRobotG.SetRobotSpeed(0, 0);
			RetourneStateError(ret, TimeDebut, msgerr);
			tce.set();
		}


		if (TestArretAvancement(Pdebut, POINTFLT(Pos.x, Pos.y), distance, TimeDebut, ret.State))
		{
			ControlRobotG.SetRobotSpeed(0, 0);
			tce.set();
		}
		
	});

	int period = ParamG.incT_Commande;

	//cr�e le timer pour la commande de suivi de chemin
	timer<bool> *Timer_step = new timer<bool>(period, NULL, callback_step, true);

	

	// Create a task that completes after the completion event is set.
	task<void> event_set(tce);

	// Create a continuation task
	event_set.then([callback_step, Timer_step, &ret, TimeDebut]()
	{
		delete callback_step;
		delete Timer_step;

		ret.DureeMs = GetTickCount() - TimeDebut;
	});

	// start timer
	Timer_step->start();

	event_set.wait();
}



void RetourneStateError(TRetourPlateforme &ret, int timedebut, char* infoRet)
{
	ret.State = TRetourState::Error;
	ret.InfoRetour = infoRet;
	ret.DureeMs = GetTickCount() - timedebut;
}

void TModulePlateforme::GoToNextStep(TStep &step, TPositionRobot &posLocaleDebut, pcallbackStep cb)
{
	int TimeDebut = GetTickCount();
	TRetourPlateforme ret;
	ret.CropRowFinished = false;

	StopUrgenceG = false;

	char msgerr[1000];
	TCommandeAutomatique *pComAuto;

	if (CommandeNavigationG != NULL)
	{
		delete CommandeNavigationG;
		CommandeNavigationG = NULL;
	}
	//on r�initialise tout � chaque step !

	// r�initialisation du pointeur pour la structure de la commande

	CommandeNavigationG = new TCommandeAutomatique(ParamG.incT_Commande, cb);
	pComAuto = dynamic_cast<TCommandeAutomatique*> (CommandeNavigationG);

	pComAuto->InitVitesse(ParamG.VitesseMaxLineaire); // charge vitesse d'avancement


	if (!(RepereG != NULL && ControlRobotG.RobotIsConnected() && AppliqueKalmanG.Timer_Kalman != NULL))
	{
		RetourneStateError(ret, TimeDebut, "Robot non initialis�!");
		if(pComAuto->callbackRetour != NULL) pComAuto->callbackRetour(ret);
		delete CommandeNavigationG; CommandeNavigationG = NULL;
		return;
	}
		
	AppliqueKalmanG.GetPositionRobotKalman(pComAuto->PosActuelle); 
	// r�ception de la position du point de d�but

	POINTFLT PDebut(pComAuto->PosActuelle.x, pComAuto->PosActuelle.y);
	// point d�but d'un mouvement de robot

	if (pComAuto->TrajDesire.nbElem == 0)  
	// v�rifie si la trajectoire est initialis�e
	// sinon, on initialise la trajectoire par le param�tre d'entr�e de Step
	{
		switch (step.Type) // initialisation de la trajectoire � partir du type de Step
		{
			case TypeStep::trajectoire:
			{
				_list<Waypoint> ListeW;

				if (!ImporteKML(ParamG.NfTrajectoire, ListeW, msgerr))
					// charge de la trajectoire d�finie dans le fichier dont le nom est stock� dans ParamG.NfTrajectoire
				{
					RetourneStateError(ret, TimeDebut, msgerr);
					return;
				}
				
				_list <POINTFLT> listPt; // projection des points GPS de trajectoire d�sir�e en coordonn�es m�trique

				if (ListeW.nbElem < 2)
				// si le nombre des points dans la liste inf�rieur � 2 (=1), on va ajouter la position actuelle du robot dans la liste  
				{
					TPositionRobot Pos;
					AppliqueKalmanG.GetPositionRobotKalman(Pos);
					listPt.push_back(POINTFLT(Pos.x, Pos.y)); // ajoute de position actuelle dans la trajectoire

				}

				BalayeListe(ListeW)
				{
					Waypoint * Wp = ListeW.current();
					double x, y;
					RepereG->WGS84VersMetres(Wp->LatDeg, Wp->LonDeg, x, y);
					listPt.push_back(POINTFLT(x, y));
					// charge de points de passage d�sir�s dans la trajectoire
				}

				pComAuto->TrajDesire = listPt;
				break;
			}
			case TypeStep::cible:
			{
				TStepCible *stepc = (TStepCible*)&step;
				pComAuto->TrajDesire.push_back(PDebut);
				pComAuto->TrajDesire.push_back(stepc->Cible);
				break;
			}

			case TypeStep::distance:
			{
				TStepDistance *stepd = (TStepDistance*)&step;
				
				double OrientActuelle = pComAuto->PosActuelle.CapDeg * M_PI / 180;
				POINTFLT D(-sin(OrientActuelle), cos(OrientActuelle));
				POINTFLT cible = PDebut + D * stepd->Distance;
				
				pComAuto->TrajDesire.push_back(PDebut);
				pComAuto->TrajDesire.push_back(cible);

				break;
			}
		}
	}

	//on a �tablit la trajectoire d�sir�e

	//gestion du substep
	ControlRobotG.ResetGroundFrame(); // reset ground frame pour commencer le step prochain

	TSubStepLocal *sub = new TSubStepLocal;

	double XLocal, YLocal, CapDeg;

	if (!ControlRobotG.GetRobotEstimatedPosition(XLocal, YLocal, CapDeg, msgerr))
	{
		RetourneStateError(ret, TimeDebut, msgerr);
		pComAuto->callbackRetour(ret);
		delete CommandeNavigationG; CommandeNavigationG = NULL;
		return;
	}
	//AppliqueKalmanG.GetPositionRobotKalman(posLocaleDebut); // retour de position d�part mesur�e par filtre Kalman

	
	sub->Debut = POINTFLT(XLocal, YLocal);
	sub->DistanceMax = ParamG.PasAvancement;

	pComAuto->Substep = sub;

	pComAuto->InitVitesse(ParamG.VitesseMaxLineaire);

	if (!pComAuto->DoStartCommande(msgerr))
	{
		RetourneStateError(ret, TimeDebut, msgerr);
		pComAuto->callbackRetour(ret);
		delete CommandeNavigationG; CommandeNavigationG = NULL;
		return;
	}

	posLocaleDebut.x = XLocal;
	posLocaleDebut.y = YLocal;
	posLocaleDebut.CapDeg = CapDeg;
	// retour de la position actuelle dans le rep�re sol (Erx) et le cap actuel
}



MATRICE ChangementRepere(double Tx, double Ty, double ThetaRad)
{
	MATRICE PR1R2(3, 3);
	// matrice pour le changement du rep�re, R1 rep�re robot, R2 rep�re bras
	PR1R2.Constante(0);

	PR1R2[0][0] = cos(ThetaRad); PR1R2[0][1] = sin(ThetaRad);
	PR1R2[1][0] = -sin(ThetaRad); PR1R2[1][1] = cos(ThetaRad);
	// matrice de rotation : [cos(theta)  sin(theta); -sin(theta)  cos(theta)]

	PR1R2[0][2] = -Tx; PR1R2[1][2] = -Ty;
	// on fait la rotation du rep�re dans un premier temps, et puis la translation
	// donc la translation suivant l'axe y est invers�e que celle dans le rep�re robot
	// Tx~= 0.475; Ty ~= -0.773m dans le rep�re robot, 
	// --> c'est-�-dire Tx ~= -0.475; Ty ~= 0.773m dans le rep�re de bras

	PR1R2[2][2] = 1;

	//PR1R2.Sauve("MatriceChangement.txt");

	return PR1R2;
}


void TModulePlateforme::Weeding(_list<POINTFLT> &weeds, TRetourPlateforme &ret, TPositionRobot posLocaleDebut, 
	bool AvecChangementRepere, bool AvecHauteTension, bool AvecRecalageManuel)
//les coordonn�es des mauvaises herbes sont fournies dans l'espace du robot au moment des prises d'image
//pr�c�dentes (� charge de la plateforme d'ajuster en fonction du d�pacement effectif depuis)
{
	int TimeDebut = GetTickCount();
	ret.CropRowFinished = false;

	if (!(ControlRobotG.RobotIsConnected() && AppliqueKalmanG.Timer_Kalman && pHexagoneBras))
	// v�rification de la connexion au robot
	{
		RetourneStateError(ret, TimeDebut, "Robot non initialis�");
		return;
	}

	if (AvecHauteTension)
	{
		if (CommandeHTG.ComHT == NULL)
		{
			RetourneStateError(ret, TimeDebut, "Module Haute Tension non initialis�");
			return;
		}
	}

	double xCourant, yCourant, capDegCourant;
	char msgerr[1000];

	if (!ControlRobotG.ArmInitialised)
	{
		ControlRobotG.InitRightArm();
		ControlRobotG.ArmInitialised = true;
		Sleep(DELAY_INITARM);
	}
	// start inject weeds, on profit de la p�riode d'initialisation du bras 
	// pour que les mesures de position robot soit stables

	
	double xMesArret, yMesArret, capDegArret;
	if (!ControlRobotG.GetRobotEstimatedPosition(xMesArret, yMesArret, capDegArret, msgerr))
		// les mesures de position robot dans le rep�re local de sol fournies par Erx
	{
		RetourneStateError(ret, TimeDebut, msgerr);
		return;
	}
	
	
	while (true)
		// on attend que les valeurs de position robot estim�e ne change plus, 
		// ces valeurs sont fournies par Erx (l'odom�trie visuelle) dans leur rep�re de sol 

	{
		// r�ception de nouvelles mesures jusqu'� une valeur stable
		Sleep(SLEEP_POSROBOT);
		if (!ControlRobotG.GetRobotEstimatedPosition(xCourant, yCourant, capDegCourant, msgerr))
		// les mesures de position robot dans le rep�re local de sol fournies par Erx
		{
			RetourneStateError(ret, TimeDebut, msgerr);
			return;
		}
		
		if ((abs(xMesArret - xCourant) < VARIATION_POSROBOT) && (abs(yMesArret - yCourant) < VARIATION_POSROBOT))
			break;

		xMesArret = xCourant; 
		yMesArret = yCourant;
	} 
	
	MATCARREE TR1R2(3); TR1R2.Identite();
	POINTFLT TranslationRobot(0, 0);
	// translation pure qui correspond au mouvement du robot (distance d'avancement)

	if (AvecChangementRepere)
	{
		double Tx_sol = xCourant - posLocaleDebut.x; // translation(mouvement du robot) dans le rep�re du sol relative du robot
		double Ty_sol = yCourant - posLocaleDebut.y;

		double cap_rad = posLocaleDebut.CapDeg * M_PI / 180;
		TranslationRobot.x = cos(cap_rad) * Tx_sol + sin(cap_rad) * Ty_sol;
		TranslationRobot.y = -sin(cap_rad) * Tx_sol + cos(cap_rad) * Ty_sol;
		// changement dans le rep�re du robot, si le rep�re robot et rep�re sol n'est pas superpos�
		// si le cap du point de d�but = 0, les deux rep�res sont superpos�s

		double theta = (capDegCourant - posLocaleDebut.CapDeg)*M_PI / 180; // �cart d'orientation entre position de d�but et position actuelle
		if (theta > M_PI) theta -= 2 * M_PI;
		if (theta < -M_PI) theta += 2 * M_PI;

		TR1R2 = ChangementRepere(ParamG.TranslationDeltaX, ParamG.TranslationDeltaY, theta);  
		// cr�ation de la matrice de changement de rep�re
		TR1R2.Sauve("MatriceChangementRepere.txt");
	}

	//MATRICE Temp(weeds.nbElem + 2, 4); Temp.Constante(0);
	//int no_temp = 1;
	//Temp[0][0] = TranslationRobot.x; Temp[0][1] = TranslationRobot.y; Temp[0][2] = capDegCourant - posLocaleDebut.CapDeg;
	//Temp[1][0] = xCourant; Temp[1][1] = yCourant; Temp[1][2] = posLocaleDebut.x; Temp[1][3] = posLocaleDebut.y;

	_list<POINTFLT> weeds_NR; // coordonn�es des weeds dans le nouveau rep�re
	_list<POINTFLT> weeds_enleve; // liste de weeds qu'on va enlever, ce qui seraint trait�s dans ce cycle

	BalayeListe(weeds)
	{
		POINTFLT *weed = weeds.current();
		MATRICE VR1(3,1);

		VR1[0][0] = weed->x - TranslationRobot.x;
		VR1[1][0] = weed->y - TranslationRobot.y;
		// prise en compte le mouvement effectu� du robot, seulement si le param�tre AvecChangementRepere = true
		VR1[2][0] = 1;

		MATRICE VR2 = TR1R2 * VR1;
		VECTEUR Weed = VR2.Colonne(0);

		Weed[1] = -Weed[1];
		// la sens de l'axe Y du rep�re de bras est inverse que celle du rep�re robot

		//Temp[++no_temp][0] = weed->x; Temp[no_temp][1] = weed->y;
		//Temp[no_temp][2] = Weed[0]; Temp[no_temp][3] = Weed[1];

		if (pHexagoneBras->Contient(Weed[0], Weed[1]))   // on v�rifie si le weed se trouve dans l'espace de travail
		{
			weeds_enleve.push_back(*weed);
			weeds_NR.push_back(POINTFLT(Weed[0], Weed[1]));  
		}
		else
		{
			weed->x = Weed[0] + ParamG.TranslationDeltaX;
			weed->y = -Weed[1] + ParamG.TranslationDeltaY;
			// si les coordonn�es du weed est inatteignable, on retrouve ses coordonn�es dans le rep�re du robot
			// et on repasse dans la liste de weeds � traiter pour essayer de le traiter � la position suivante du weeding 
		}
	}

	BalayeListe(weeds_enleve) // on enl�ve les weeds qui seraint trait�s dans ce cycle
	{
		POINTFLT *weed = weeds_enleve.current();
		weeds.remove(*weed);
	}

	//Temp.Sauve("Coordonn�es_Weeds.txt");

	if (weeds_NR.nbElem == 0)
	// v�rification de l'existance de weeds atteignables dans le liste
	{
		ret.InfoRetour = "Liste de weeds atteignables vide";
		ret.State = TRetourState::Completed;
		ret.DureeMs = GetTickCount() - TimeDebut;
		return;
	}


	if (AvecRecalageManuel) 
	// on positionne relativement le bras sur le weed et puis on fait le r�calage manuel de la position
	{
		BalayeListe(weeds_NR)
		{
			POINTFLT weed = *weeds_NR.current();
			ControlRobotG.SetPositionXYWeedR(weed);

			ControlBrasManuelG.UpdateConsigneCourante(weed);
			ControlBrasManuelG.ActiveControlManuel = true;

			DialogHauteTension("T_Plateforme");

			MSG msg;

			while (true)
			{
				if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 	DispatchMessage(&msg);
				
				if (!ControlBrasManuelG.ActiveControlManuel)
					break;

				Sleep(300);
			}
		}	
	}

	else  // sinon, on passe le positionnement auto du bras
	{
		if (AvecHauteTension)
		{
			BalayeListe(weeds_NR)
			{
				POINTFLT Weed = *weeds_NR.current();

				ControlRobotG.SetPositionXYWeedR(Weed);
				Sleep(DELAY_POSITIONING);
				ControlRobotG.SetPositionZWeedingR();
				Sleep(DELAY_POSITIONING);

				int CurrentMax = 0, SparkVoltage = 0, SparkDuration = 0;
				CommandeHTG.StartSequence(CurrentMax, SparkVoltage, SparkDuration, msgerr);
				Sleep(DELAY_WEEDING);
			}
		}
		else // si on ne lance pas le d�charge Haute tension, on teste le positionnement auto sur les positions weeds
			ControlRobotG.TestWeedingRightArm(weeds_NR);
	}
		
	ControlRobotG.SetPositionAwaitedR(); // retour � la position d'attente du bras, position d'attente=(0, 0, 0.8)
	Sleep(DELAY_RESETARM);

	ret.State = TRetourState::Completed;
	ret.DureeMs = GetTickCount() - TimeDebut;
	return;
}


void TModulePlateforme::Stop() //demande d'arr�t utilisateur
{
	StopUrgenceG = true;
	ControlRobotG.SetRobotSpeed(0, 0);
	ControlRobotG.CloseRightArm();
	ControlRobotG.ArmInitialised = false;
}



#include "ManualControlPad.h"
#include "drvtraiti.h"

DRVTRAITIM *pDriverG;

void TModulePlateforme::AvancementManuel(TPositionRobot &posLocaleDebut, pcallbackStep cb)
{
	int TimeDebut = GetTickCount();
	TRetourPlateforme ret;
	char msgerr[1000];

	if (!ControlRobotG.RobotIsConnected())
	{
		RetourneStateError(ret, TimeDebut, "Robot non connect�");
		if (cb != NULL) cb(ret);
		return;
	}

	ControlRobotG.ResetGroundFrame(); // reset ground frame pour commencer le step prochain

	double XLocal, YLocal, CapDeg;
	if (!ControlRobotG.GetRobotEstimatedPosition(XLocal, YLocal, CapDeg, msgerr))
	{
		delete CommandeNavigationG; CommandeNavigationG = NULL;
		RetourneStateError(ret, TimeDebut, msgerr);
		if (cb != NULL) cb(ret);
		return;
	}


	if (pDriverG->MessageOuiNon("Avancement tout droit %.2f m", ParamG.PasAvancement)) // avancement simple 1.3m
	{
		//ControlRobotG.Avancement(ParamG.PasAvancement); // on n'utilise pas cette fonction � cause de vitesse inchangeable
		//envoi de consigne succesivement
		double V = ParamG.PasAvancement;
		int cnt = 0;
		while (true)
		{
			ControlRobotG.SetRobotSpeed(V, V);
			Sleep(150);

			cnt++;
			if (cnt == 5) break;
		}
	}
	else // avancement manuel avec la bo�te de dialogue (arr�t manuel) 
	{
		if (CommandeNavigationG != NULL)  // on r�initialise le pointeur de commande dans tous les cas
		{
			delete CommandeNavigationG;
			CommandeNavigationG = NULL;
		}

		CommandeNavigationG = new TCommandeManuelle(ParamG.incT_Commande); // initialisation avec la commande manuelle
		TCommandeManuelle * pCom = dynamic_cast <TCommandeManuelle*> (CommandeNavigationG);

		if (!pCom->DoStartCommande(msgerr))
		{
			RetourneStateError(ret, TimeDebut, msgerr);
			if (cb != NULL) cb(ret);
			return;
		}

		//AppliqueKalmanG.GetPositionRobotKalman(posLocaleDebut); // Mesure de filtre Kalman

		DialogControlPad("T_Plateforme");  // ouvert de dialogue de commande

		MSG msg;

		while (true)
		{
			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 	DispatchMessage(&msg);

			if (CommandeNavigationG == NULL)	break;
		}
	}

	posLocaleDebut.x = XLocal;
	posLocaleDebut.y = YLocal;
	posLocaleDebut.CapDeg = CapDeg;
	// retour de la position actuelle dans le rep�re sol (Erx) et le cap actuel

	ret.DureeMs = GetTickCount() - TimeDebut;
	ret.State = TRetourState::Completed;
	if (cb != NULL) cb(ret);  // fonction retour de r�ussite
}



bool TModulePlateforme::GetPositionGPSActuelle(double &LatDeg, double &LonDeg, char *msgerr)
{
	if (AppliqueKalmanG.pKalman == NULL || RepereG == NULL)
	{
		sprintf(msgerr, "Robot non initialis�");
		return false;
	}
	TPositionRobot Pos;
	AppliqueKalmanG.GetPositionRobotKalman(Pos);

	RepereG->MetresVersWGS84(LatDeg, LonDeg, Pos.x, Pos.y);
	return true;
}


bool TModulePlateforme::VerifLancementPlateforme(bool AvecAvancement, bool AvecHauteTension, char *msgerr)
{
	if (AvecAvancement)
	{
		if (AppliqueKalmanG.pKalman == NULL || RepereG == NULL || !ControlRobotG.RobotIsConnected())
		{
			sprintf(msgerr, "Robot non initialis�");
			return false;
		}
	}

	if (AvecHauteTension)
	{
		if (CommandeHTG.ComHT == NULL)
		{
			sprintf(msgerr, "Module Haute Tension non initialis�");
			return false;
		}
	}
	
	return true;
}