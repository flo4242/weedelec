
#include "ControlRobot.h"
#include "Plateforme.h"
#include "CommandeHT.h"

#include "DialogueHauteTension.h"
#include "IDControlPad.h"

extern TControlBrasManuel ControlBrasManuelG;
extern CommandeHT CommandeHTG;
extern TControlRobot ControlRobotG;
extern TParamRobot ParamG;

extern DRVTRAITIM *pDriverG;


#ifndef POSITIONZ_POSITIONING
#define POSITIONZ_POSITIONING  0.8
#endif


BOOL CALLBACK HauteTensionDialogProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{

		case WM_INITDIALOG:
		{
			if (!ControlRobotG.RobotIsConnected())
			{
				pDriverG->OuvreTexte();
				pDriverG->EffaceTexte();
				pDriverG->PrintTexte("Echec : Robot non initialis�");
				return FALSE;
			}

			if (!ControlBrasManuelG.ActiveControlManuel)
				ControlBrasManuelG.ActiveControlManuel = true;

			if (!ControlRobotG.ArmInitialised)
			{
				ControlRobotG.InitRightArm();
				ControlRobotG.ArmInitialised = true;
			}
			
			return TRUE;
		}


		case WM_COMMAND:
		{
			switch (LOWORD(wParam))
			{
				case IDC_BUTTON_BRASUP:
				{
					ControlBrasManuelG.Hauteur = POSITIONZ_POSITIONING;
					ControlRobotG.SetHeightR(ControlBrasManuelG.Hauteur);
					break;
				}

				case IDC_BUTTON_BRASDOWN:
				{
					double hauteur = ParamG.HauteurWeeding;
					if (hauteur >= 0.8 && hauteur < 0.95)
						ControlBrasManuelG.Hauteur = hauteur;
					else
						ControlBrasManuelG.Hauteur = POSITIONZ_WEEDING;

					ControlRobotG.SetHeightR(ControlBrasManuelG.Hauteur);
					break;
				}

				case IDC_BUTTON_XPLUS:
				{
					ControlBrasManuelG.PositionXPlus();
					POINTFLT posBras = ControlBrasManuelG.GetConsignePositionBras();
					ControlRobotG.SetPositionXYWeedR(posBras);
					break;
				}

				case IDC_BUTTON_XMOINS:
				{
					ControlBrasManuelG.PositionXMoins();
					POINTFLT posBras = ControlBrasManuelG.GetConsignePositionBras();
					ControlRobotG.SetPositionXYWeedR(posBras);
					break;
				}

				case IDC_BUTTON_YPLUS:
				{
					ControlBrasManuelG.PositionYMoins();  // attention : la sens du bras est invers�e !!
					POINTFLT posBras = ControlBrasManuelG.GetConsignePositionBras();
					ControlRobotG.SetPositionXYWeedR(posBras);
					break;
				}

				case IDC_BUTTON_YMOINS:
				{
					ControlBrasManuelG.PositionYPlus();  // attention : la sens du bras est invers�e !!
					POINTFLT posBras = ControlBrasManuelG.GetConsignePositionBras();
					ControlRobotG.SetPositionXYWeedR(posBras);
					break;
				}

				case IDC_BUTTON_LANCEHT:
				{
					if (CommandeHTG.ComHT == NULL) break;
					else
					{
						int courantMax, duration, voltage;
						char msgerr[1000];
						CommandeHTG.StartSequence(courantMax, voltage, duration, msgerr);
						pDriverG->PrintTexte("Info S�quence : \nCourant max: %d (mA); Tension moyenne: %d (V); Dur�e: %d (us)\n",
							courantMax, voltage, duration);
					}
					break;
				}

				case IDC_BUTTON_ZPLUS:
				{
					ControlBrasManuelG.PositionZPlus();  
					ControlRobotG.SetHeightR(ControlBrasManuelG.Hauteur);
					break;
				}

				case IDC_BUTTON_ZMOINS:
				{
					ControlBrasManuelG.PositionZMoins();
					ControlRobotG.SetHeightR(ControlBrasManuelG.Hauteur);
					break;
				}
				default:
				{
					ControlBrasManuelG.ActiveControlManuel = false;
					ControlRobotG.SetPositionAwaitedR();
					ControlBrasManuelG.ResetPositionBras();
					ControlBrasManuelG.Hauteur = POSITIONZ_POSITIONING;  // retourne � la position Wait
					EndDialog(hwnd, 0); // fermer la dialogue
					return TRUE;
				}
			}
			break;
		}

		default:
			return FALSE;
	}
}


void DialogHauteTension(TNomFic NomModule)
{
	// r�cup�ration du nom du module dll 
	TNomFic NomComplet;

#ifdef _DEBUG
	sprintf(NomComplet, "%s_64d", NomModule);
#else
	sprintf(NomComplet, "%s_64", NomModule);
#endif

	HMODULE HVS = GetModuleHandle(NomComplet);

	DialogBox(HVS, MAKEINTRESOURCE(ID_HauteTension), NULL, (DLGPROC)HauteTensionDialogProc);

	return;
}