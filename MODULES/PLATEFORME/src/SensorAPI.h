#ifndef __SENSORAPI_H
#define __SENSORAPI_H

#include <olectl.h>
#include <sensorsapi.h>
#include <sensors.h>
#include <stdio.h>

#pragma comment(lib, "sensorsapi.lib")
#pragma comment(lib, "PortableDeviceGUIDs.lib")
#pragma comment(lib, "ole32.lib")
#pragma comment(lib, "user32.lib")

/*class SensorEvent : public ISensorEvents {
public:
	SensorEvent() : mCount(0) {
	}

	// IUnknown interface

	STDMETHODIMP_(ULONG) AddRef() {
		return InterlockedIncrement(&mCount);
	}

	STDMETHODIMP_(ULONG) Release() {
		ULONG count = InterlockedDecrement(&mCount);
		if (!count) {
			delete this;
			return 0;
		}
		return count;
	}

	STDMETHODIMP QueryInterface(REFIID iid, void** ppv) {
		if (iid == IID_IUnknown) {
			*ppv = static_cast<IUnknown*>(this);
		}
		else if (iid == IID_ISensorEvents) {
			*ppv = static_cast<ISensorEvents*>(this);
		}
		else {
			return E_NOINTERFACE;
		}
		AddRef();
		return S_OK;
	}

	// ISensorEvents interface

	STDMETHODIMP OnEvent(ISensor *aSensor, REFGUID aId, IPortableDeviceValues *aData) {
		return S_OK;
	}

	STDMETHODIMP OnLeave(REFSENSOR_ID aId) {
		return S_OK;
	}

	STDMETHODIMP OnStateChanged(ISensor *aSensor, SensorState state) {
		return S_OK;
	}

	STDMETHODIMP OnDataUpdated(ISensor *aSensor, ISensorDataReport *aReport) {
		printf("Updated\n");
		return S_OK;
	}

private:
	ULONG mCount;
};*/


/*struct SensorGPS
{
	double Latitude;
	double Longitude;
	double Speed;
};*/

bool InitSensorGPS(char* msgerr);

bool GetDataSensorGPS(double& Latitude, double& Longitude, double& Vitesse, char* msg);

#endif // __SENSORAPI_H