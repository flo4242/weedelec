
#include <fstream>
#include <strstream>
using namespace std;

#include <Windows.h>

#include "ParseXML.h"
#include "utilfic.h"
#include "TrackPointGPX.h"



///////////////////// IMPORT KML via outil ParseXML /////////////////////////////
// (en fin de fichier, importation directe d'un waypoint depuis le press-papier,
// hors outil libexpat.dll car ant�rieur )



//structures interm�diaires de r�cup�ration des donn�es sous forme de cha�nes

struct STrackPointGPX
{
	char Name[500];
	char Coordinates[500];
	char Elevation[100];
	char Heading[100];
	char GroundSpeed[100];
	char SystemeTime[100];
	STrackPointGPX() { Name[0] = Coordinates[0] = Elevation[0] = Heading[0] = GroundSpeed[0] = SystemeTime[0] = '\0'; }
};


struct TdataParserGPX
{
	char *Buffer; //allou� dynamiquement selon taille fichier
	int Index;
	bool TrackPointEnCours;
	bool WayPointEnCours;
	bool NameEnCours;
	bool ElevationEnCours;
	bool TimeEnCours;
	bool ExtensionEnCours;
	bool SpeedEnCours;
	bool HeadingEnCours;

	//bool PointAStocker;


	int TailleF;

	STrackPointGPX courant;
	_list<STrackPointGPX> Liste;

	TdataParserGPX(char *Nf)
	{
		TrackPointEnCours = NameEnCours = WayPointEnCours = false;
		ElevationEnCours = HeadingEnCours = false;
		TimeEnCours = ExtensionEnCours = SpeedEnCours = false;
		Index = 0;

		if (TailleFichier(Nf, TailleF))	Buffer = new char[TailleF];
		else Buffer = NULL;
	}

	~TdataParserGPX() { if (Buffer)	delete Buffer; }
};


void ConcateneCoordonnees(char *dest, const char** attr)
{
	const char *pnt = attr[0];
	int i = 0;
	char *ptemp = dest;

	while (true)
	{
		strcpy(ptemp, attr[i]);
		i++;
		ptemp += strlen(ptemp);
		pnt = attr[i];
		if (!pnt) break;
	}
	ptemp = '\0';
}

//handlers de r�cup�ration lors du parsing

void  HStartGPX(void *userData, const char *el, const char **attr)
/***************************************************************/
{
	TdataParserGPX * pdata = (TdataParserGPX *)userData;
	STrackPointGPX *pTp = &(pdata->courant);

	if (strcmp(el, "trkpt") == 0)
	{
		pdata->TrackPointEnCours = true;
		ConcateneCoordonnees(pTp->Coordinates, attr);
	}
	if (strcmp(el, "wpt") == 0)
	{
		pdata->WayPointEnCours = true;
		ConcateneCoordonnees(pTp->Coordinates, attr);
	}

	if (strcmp(el, "name") == 0) { pdata->NameEnCours = true; pTp->Name[0] = '\0'; }
	if (strcmp(el, "ele") == 0) { pdata->ElevationEnCours = true; pTp->Elevation[0] = '\0'; }
	if (strcmp(el, "time") == 0) { pdata->TimeEnCours = true; pTp->SystemeTime[0] = '\0'; }
	if (strcmp(el, "extensions") == 0) pdata->ExtensionEnCours = true;
	if (strcmp(el, "GroundSpeed") == 0) { pdata->SpeedEnCours = true; pTp->GroundSpeed[0] = '\0'; }
	if (strcmp(el, "heading") == 0) { pdata->HeadingEnCours = true; pTp->Heading[0] = '\0'; }
}  /* End of start handler */

void ConcateneDest(char *dest, const char* src, int len, int taillemax)
/********************************************************************/
{
	//conversion depuis UTF 8

	//cr�ation d'une chaine pour fonctions de conversion

	char *st = new char[len + 1];
	memcpy(st, src, len);
	st[len] = '\0';

	//conversion utf8 --> unicode
	wchar_t * stw = ConversionUnicode(st, NULL, true);
	//conversion unicode --> ansi
	char * stansi = ConversionDepuisUnicode(stw, NULL, false);

	int len2 = strlen(stansi);

	int offset = strlen(dest); if (len2 >= taillemax - offset)	return;
	//memcpy(dest+offset, src, len); dest[offset+len] = '\0';
	memcpy(dest + offset, stansi, len2); dest[offset + len2] = '\0';

	delete stw;
	delete stansi;
}


void HCharGPX(void *userData, const char *s, int len)
/***************************************************/
{
	TdataParserGPX * pdata = (TdataParserGPX *)userData;
	STrackPointGPX *pTp = &(pdata->courant);

	if (CDATAEnCours())	return;   //XXXXX

	if (pdata->TrackPointEnCours || pdata->WayPointEnCours)
	{
		if (pdata->NameEnCours)		ConcateneDest(pTp->Name, s, len, 500);
		if (pdata->ElevationEnCours)		ConcateneDest(pTp->Elevation, s, len, 500);
		if (pdata->TimeEnCours)		ConcateneDest(pTp->SystemeTime, s, len, 500);
		if (pdata->SpeedEnCours)	ConcateneDest(pTp->GroundSpeed, s, len, 500);
		if (pdata->HeadingEnCours)	ConcateneDest(pTp->Heading, s, len, 500);
	}
	
	//if (pdata->CoordinatesEnCours)	ConcateneDest(pTp->Coordinates, s, len, 500);
		/*else if (pdata->WayPointEnCours)//c'est une piste --> toutes les coordonn�es � la suite
		{
			memcpy(pdata->Buffer + pdata->Index, s, len); pdata->Index += len;
		}*/

}



void HEndGPX(void *userData, const char *el)
/******************************************/
{
	TdataParserGPX * pdata = (TdataParserGPX *)userData;
	STrackPointGPX *pTp = &(pdata->courant);


	if (strcmp(el, "name") == 0)	pdata->NameEnCours = false;
	if (strcmp(el, "ele") == 0)		pdata->ElevationEnCours = false;
	if (strcmp(el, "time") == 0)		pdata->TimeEnCours = false;
	if (strcmp(el, "extensions") == 0)		pdata->ExtensionEnCours = false;
	if (strcmp(el, "GroundSpeed") == 0)		pdata->SpeedEnCours = false;
	if (strcmp(el, "heading") == 0)		pdata->HeadingEnCours = false;

	if (strcmp(el, "trkpt") == 0) 
	{ 
		pdata->TrackPointEnCours = false; 
		pdata->Liste.push_back(*pTp);
	}
	if (strcmp(el, "wpt") == 0) 
	{ 
		pdata->WayPointEnCours = false; 
		pdata->Liste.push_back(*pTp);
	}
	//s'il y en a plusieurs dans le PlaceMark, on ne stockera que le dernier...
	
}

bool GetGPXCoordinates(char *s, double &lat, double &lon)
/********************************************************************/
//format: "lat41.314141lon1.359008" ; doit contenir fin de chaine
{
	char* plat = s; char* plon = s; 
	char temp[100];char* ptemp = temp;

	memcpy(ptemp, s, 3 * sizeof(char));

	if (strncmp(ptemp, "lat", 3) == 0)
	{
		plat = s + 3;
	}
	else
		return false;

	char *pnt = plat;
	
	for (; ; pnt++)
	{
		if (!isdigit(*pnt) && *pnt != '.')
			break;
	}

	memcpy(ptemp, pnt, 3 * sizeof(char));
	if (strncmp(ptemp, "lon", 3) == 0)
	{
		plon = pnt + 3;
		*pnt = '\0';
	}
	else
		return false;

	lat = atof(plat); lon = atof(plon);
	return true;
}

bool GetGPXdoubleValue(char *s, double &Val)
{
	char *pnt = s;
	if (*pnt == '\0') { Val = 0; return true; }

	int i = 0;
	for (; *pnt != '\0'; pnt++)
	{
		if (!isdigit(*pnt) && *pnt != '.' && *pnt != '-') break;
		i++;
	}
	if (i == 0 || i < strlen(s)) return false;
	*pnt = '\0';
	Val = atof(s);
	return true;
}

bool GetGPXSystemTime(char *s, SYSTEMTIME& st)
/********************************************************************/
//format: "2019-02-11T09:25:47Z"
{
	
	char* YY = s; char* MM = NULL; char* DD = NULL;
	char* Heure = NULL; char* Min = NULL; char* Seconde = NULL;
	if (*YY == '\0')
	{ 
		st.wYear = 0; st.wMonth = 0; st.wDay = 0;
		st.wHour = 0; st.wMinute = 0; st.wSecond = 0;
		return true;
	}
		
	char* temp = s;
	int i = 0;
	for (*temp; *temp != '-';temp++)
	{
		if (!isdigit(*temp)) return false;
		i++;
	}
	if (i != 4) return false;
	*temp = '\0'; i = 0;
	st.wYear = atof(YY);


	MM = ++temp;
	for (*temp; *temp != '-';temp++)
	{
		if (!isdigit(*temp)) return false;
		i++;
	}
	if (i != 2) return false;
	*temp = '\0'; i = 0;
	st.wMonth = atof(MM);


	DD = ++temp;
	for (*temp; *temp != 'T';temp++)
	{
		if (!isdigit(*temp)) return false;
		i++;
	}
	if (i != 2) return false;
	*temp = '\0'; i = 0;
	st.wDay = atof(DD);
	

	Heure = ++temp;
	for (*temp; *temp != ':';temp++)
	{
		if (!isdigit(*temp)) return false;
		i++;
	}
	if (i != 2) return false;
	*temp = '\0'; i = 0;
	st.wHour = atof(Heure);
	
	Min = ++temp;
	for (*temp; *temp != ':';temp++)
	{
		if (!isdigit(*temp)) return false;
		i++;
	}
	if (i != 2) return false;
	*temp = '\0'; i = 0;
	st.wMinute = atof(Min);


	Seconde = ++temp;
	for (*temp; *temp != 'Z';temp++)
	{
		if (!isdigit(*temp)) return false;
		i++;
	}
	if (i != 2) return false;
	*temp = '\0';
	st.wSecond = atof(Seconde);

	
	return true;
}

bool ImportGPX(char *Nf, _list<TrackPoint>& ListeTP, char* msg1000)
{
	TdataParserGPX Data(Nf);
	if (!Data.Buffer)
	{
		sprintf(msg1000, "Echec allocation buffer pour %s", Nf); return -1;
	}

	TParamParse ParamGPX;
	ParamGPX.pUserData = &Data;
	ParamGPX.HandlerStart = HStartGPX;
	ParamGPX.HandlerEnd = HEndGPX;
	ParamGPX.HandlerChar = HCharGPX;

	if (!ParseFichierXML(Nf, ParamGPX, msg1000))	return false;
	
	for (Data.Liste.begin(); !Data.Liste.end(); Data.Liste++)
	{
		TrackPoint TP;
		STrackPointGPX TPst = *Data.Liste.current();
		
		if(TPst.Name[0] != '\0')
			TP.AlloueName(TPst.Name, 128);
	
		if (!GetGPXCoordinates(TPst.Coordinates, TP.LatDeg, TP.LonDeg)) continue; //ERREUR
		if(!GetGPXdoubleValue(TPst.Elevation, TP.Elevation)) continue;
		if (!GetGPXdoubleValue(TPst.Heading, TP.HeadingDeg)) continue;
		if (!GetGPXdoubleValue(TPst.GroundSpeed, TP.GroundSpeed)) continue;
		
		if (!GetGPXSystemTime(TPst.SystemeTime, TP.STime)) continue;
		//TPst.SystemeTime

		ListeTP.push_back(TP);
	}

	return true;
}