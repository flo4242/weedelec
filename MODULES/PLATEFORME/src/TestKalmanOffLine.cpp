#include "TestKalmanOffLine.h"
#include <sstream>
#include <string>

using namespace std;

bool TestKalmanOffLine::EvolutionKalman(char *msgerr)
{
	if (pKalman == NULL)
	{
		sprintf(msgerr, "Filtre Kalman non initialis�");
		return false;
	}

	// charge de la consigne des moteurs
	POINTFLT ConsigneMoteurs;
	TPositionRobot Pos;

	if (!LireMesure(Pos, ConsigneMoteurs, msgerr))
		return false;

	pKalman->SetConsigneMoteursActuelle(ConsigneMoteurs);

	// estimation kalman
	pKalman->EtatSuivantNonCorrige();


	VECTEUR obs(3);

	obs[0] = Pos.x; obs[1] = Pos.y; obs[2] = Pos.CapDeg * (M_PI / 180);
	pKalman->Obs = obs;

	pKalman->EtatSuivantCorrige(); // corrige kalman

	// mise � jour des donn�es actuelles
	VECTEUR Etat = pKalman->GetEtatCourant();

	double capdeg = Etat[2] * (180 / M_PI);  // r�cup�ration du cap actuel

	PosRobotKalman.x = Etat[0];
	PosRobotKalman.y = Etat[1];
	PosRobotKalman.CapDeg = capdeg;

	return true;
}

bool TestKalmanOffLine::LireMesure(TPositionRobot& MesRobot, POINTFLT& ConsigneMoteurs, char *msgerr)
{
	static int Index = 1;
	if (Index >= Mesures.dimL() - 1)
	{
		sprintf(msgerr, "Fin de ligne"); 
		return false;
	}

	MesRobot.x = Mesures[Index][1];
	MesRobot.y = Mesures[Index][2];
	MesRobot.CapDeg = Mesures[Index][3];

	ConsigneMoteurs.x = Mesures[Index][4];
	ConsigneMoteurs.y = Mesures[Index][5];

	Index++;

	return true;
}

#ifdef BOF
bool TestKalmanOffLine::LireMesure(TPositionRobot& MesRobot, POINTFLT& ConsigneMoteurs, char *msgerr)
{
	if (!pFInput)
	{
		sprintf(msgerr, "Ficher d'entr�e des mesures non initialis�");
		return false;
	}

	string line;
	if (!getline(*pFInput, line) || line[0] == '\0')
	{
		sprintf(msgerr, "Fin de ligne");
		return false;
	}

	int dt;
	istringstream iss(line);
	iss >> dt >> MesRobot.x >> MesRobot.y >> MesRobot.CapDeg >> ConsigneMoteurs.x >> ConsigneMoteurs.y;

	return true;
}
#endif