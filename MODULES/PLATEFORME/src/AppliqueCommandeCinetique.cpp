
#include "AppliqueCommandeCinetique.h"

extern DRVTRAITIM *pDriverG;

extern TControlRobot ControlRobotG;
extern TParamRobot ParamG;

TCommandeCinetique TComCinetiqueG;


bool GetMesureEnTempsReel(double& lat, double& lon, double& orient, double& vitesse, char* msgerr)
{
	if(!GetDonneesCpateurRobot(lat, lon, orient, msgerr)) return false;
	double latitude = 0, longitude = 0;
	if (!GetDonneesExterneGPS(latitude, longitude, vitesse, msgerr)) return false;
}


bool VerificationErreur(double err_x, double err_y, double err_theta)
{
	return (err_x < ParamG.precisionPos) && (err_y < ParamG.precisionPos) && (err_theta < ParamG.precisionOrient);
}

MATRICE PinvMatriceJacobinne(double theta, double E)
{
	MATRICE J(3, 2);
	J.Constante(0);

	J[0][0] = 1 / 2 * sin(theta);	J[0][1] = 1 / 2 * sin(theta);
	J[1][0] = 1 / 2 * cos(theta);	J[1][1] = 1 / 2 * cos(theta);
	J[2][0] = -1 / E;				J[2][1] = 1 / E;

	MATCARREE M;
	M = ~J*J;
	MATCARREE InvM = M.InversionCholesky();

	return InvM * ~J;
}

VECTEUR ErreurSuiviTrajectoire(VECTEUR err)
{
	VECTEUR K(3);
	K[0] = err[0] * ParamG.gainPos;
	K[1] = err[1] * ParamG.gainPos;
	K[2] = err[2] * ParamG.gainOrient;
	return K;
}

void TCommandeCinetique::FiltreKalmanTempsReel(double lat, double lon, double orient, double vitesse)
{
	if (!Fk || !pR) return;  // variable public pour le fitre Kalman et pour la projection de rep�re local

	double x = 0, y = 0;
	Fk->EtatSuivantNonCorrige();

	VECTEUR obs(4);
	pR->WGS84VersMetres(lat, lon, x, y); // nouvelles mesures
	
	obs[0] = x; obs[1] = y; obs[2] = vitesse; obs[3] = orient * M_PI / 180;
	Fk->Obs = obs;

	Fk->EtatSuivantCorrige(); // corrige

	// mise � jour des donn�es
	VECTEUR Etat = Fk->GetEtatCourant();
	
	double v, theta;
	double LatDeg, LonDeg;
	x = Etat[0]; y = Etat[1]; v = Etat[2]; theta = Etat[3]; 

	pR->MetresVersWGS84(LatDeg, LonDeg, x, y);

	SetEtatCurant(LatDeg, LonDeg, theta * 180 / M_PI, v);
}

void TCommandeCinetique::CalculCommandeVitesse(double& vitesseG, double& vitesseD, double& err_x, double& err_y, double& err_theta)
{
	double x_curant = 0, y_curant = 0;
	double x_desire = 0, y_desire = 0;
	double theta_curant = OrientCurant * M_PI / 180;
	double theta_desire = OrientDesire * M_PI / 180;

	pR->WGS84VersMetres(LatCurant, LonCurant, x_curant, y_curant);
	pR->WGS84VersMetres(LatDesire, LonDesire, x_desire, y_desire);

	MATRICE pinv_J = PinvMatriceJacobinne(theta_curant, ENTRAXE_ROBOT);
	VECTEUR v(2); MATRICE V(2, 1);
	VECTEUR Erreur(3);

	Erreur[0] = x_desire - x_curant;
	Erreur[1] = y_desire - y_curant;
	Erreur[2] = theta_desire - theta_curant;

	V = pinv_J * ErreurSuiviTrajectoire(Erreur);
	v = V.Colonne(0);

	vitesseG = v[0];  vitesseD = v[1];
	err_x = Erreur[0]; err_y = Erreur[1]; err_theta = Erreur[2];

	pDriverG->PrintTexte("Erreur x: %.4f\t Erreur y: %.4f\t Erreur orient: %.4f", err_x, err_y, err_theta);
}

void TCommandeCinetique::CommandeEnVitesse()
{
	char msgerr[1000];
	double lat_mesure, lon_mesure, orient_mesure, vitesse_mesure;

	if (!GetMesureEnTempsReel(lat_mesure, lon_mesure, orient_mesure, vitesse_mesure, msgerr))
	{
		if (pTimerCom) KillTimer(NULL, pTimerCom);
		pDriverG->Message("Erreur lecture capteur : %s", msgerr);
		return;
	}

	FiltreKalmanTempsReel(lat_mesure, lon_mesure, orient_mesure, vitesse_mesure);
	
	double vitesseG, vitesseD;
	double Err_x, Err_y, Err_theta;
	CalculCommandeVitesse(vitesseG, vitesseD, Err_x, Err_y, Err_theta);

	SendSpeedRobot(vitesseG, vitesseD);

	if (VerificationErreur(Err_x, Err_y, Err_theta))
	{
		StopCommande();
		pDriverG->Message("Point d�sir� arriv�!");
		return;
	}
}

void CALLBACK CommandeEnVitesseCALLBACK(HWND hwnd, UINT uMsg, UINT timerId, DWORD dwTime) 
{ 
	TComCinetiqueG.CommandeEnVitesse(); 
}


bool AppliqueCommandeCinetique(double LatD, double LonD, double OrientD, char* msgerr)
{
	// initialisation de filtre Kalman
	TFiltreKalman Fk(DIM_KALMAN);
	Fk.ChargeParametres(ParamG.sigmax0, ParamG.sigmatheta0, ParamG.sigmav0, ParamG.sigmadtheta0, ParamG.sigmax, ParamG.sigmatheta,
		ParamG.sigmav, ParamG.sigmadtheta, ParamG.sigmaObsx, ParamG.sigmaObsv, ParamG.sigmaObstheta);

	char msg[1000];
	double Lat = 0, Lon = 0, Orient = 0, Vitesse = 0;
	double x_local = 0, y_local = 0;
	int incT = ParamG.incT_Commande * 1.0 / 1000;

	if (!GetMesureEnTempsReel(Lat, Lon, Orient, Vitesse, msg))
	{
		sprintf(msgerr, "Echec initialisation : %s", msg);
		return false;
	}
	TRepereLocal Rp(Lat);
	Rp.WGS84VersMetres(Lat, Lon, x_local, y_local);
	Fk.InitKalman(x_local, y_local, Orient * M_PI / 180, Vitesse, 0, incT);
	
	TComCinetiqueG.InitParam(Fk, Rp);
	TComCinetiqueG.SetEtatCurant(Lat, Lon, Orient, Vitesse);
	TComCinetiqueG.SetEtatDesire(LatD, LonD, OrientD);

	TComCinetiqueG.SetCommande();

	return true;
}
