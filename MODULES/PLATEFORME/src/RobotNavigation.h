#ifndef  __ROBOTNAVIGATION_H
#define __ROBOTNAVIGATION_H

#include "CommandeCinetique.h"
#include "ExportModulePlateforme.h"
#include "Waypoint.h"


//bool InitRobotNavigation(char* msgerr);

bool InitRobotNavigation2(char* NfKML, _list<Waypoint>& ListeWp, char* msgerr);

bool UpdateRobotPositionForNavigation(char* msgerr);


#endif // ! __ROBOTNAVIGATION_H


