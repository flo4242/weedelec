#include "ControlRobot.h" 
// il faut toujours inclure "ControlRobot.h" en premier pour que "erxCom.h" soit d�clar� en premier 
// (sinon incompatibilit� se trouve pour le module 'chrono') 

#include "Plateforme.h"
#include "CommandeNavigation.h"
#include "AppliqueFiltreKalmanErx.h"
#include "ExportModulePlateforme.h"
#include "NmeaPosition.h"
#include "GetPortsCOM.h"


#ifndef VITESSEMAX
#define VITESSEMAX  0.4   
#endif


TRepereLocal *RepereG = NULL;
NmeaPosition *pNmea = NULL;
Hexagone *pHexagoneBras = NULL;

extern TParamRobot ParamG;
extern TControlRobot ControlRobotG;
extern TAppliqueKalman AppliqueKalmanG;

extern TCommandeNavigation *CommandeNavigationG;


extern int TimeDebutG;  // time variable pour l'affichage Filtre Kalman


bool InitRobot(char *msgerr) 
// Connexion du robot en chargant l'adresse saisie, cr�ation du rep�re local lisant la position initiale de capteur du robot (ou GPS externe)
// Et initialisation de filtre Kalman qui tourne tout le temps d�s la connexion jusqu'� la d�connexion (voir fonction CloseRobot())
{
	if (ControlRobotG.RobotIsConnected())
	{
		sprintf(msgerr, "Robot d�j� connect�");
		return false;
	}

	char* AdresseConnexion;

	// charge l'adresse de connexion (connexion soit par wifi soit par ethernet)
	if (ParamG.ConnexionWifi)
		AdresseConnexion = ParamG.AdresseConnexionWifi;
	else
		AdresseConnexion = ParamG.AdresseConnexionEth;

	// envoie de l'adresse connexion au robot et connecte
	ControlRobotG.SetConnectAddress(AdresseConnexion);

	if (!ControlRobotG.RobotIsConnected())
		if (!ControlRobotG.ConnectRobot(msgerr))
			return false;
	
	ControlRobotG.HauteurWeeding = ParamG.HauteurWeeding; // set-up de la hauteur du weeding
	ControlRobotG.ResetGroundFrame();  // initialise le rep�re du sol

	//lecture latitude pour initialiser le rep�re local
	double latdeg, londeg, capdeg;
	
	// si utilisant le gps externe, on �tablit une connexion port COM pour la lecture
	if (ParamG.GPSExterne)
	{
		if (pNmea == NULL)
			pNmea = new NmeaPosition(ParamG.ParamCOMGPSExterne.DescriptionPort, ParamG.ParamCOMGPSExterne.Baud);
		
		int fix;
		if (!GetMesureGPSExterne(latdeg, londeg, fix, msgerr))
			return false;

#define GPS_EXTERNE
#define DECALAGE_Y_GPS_EXT  ParamG.DecalageGPSExt
	}
	else
	{
#ifdef GPS_EXTERNE
#undef GPS_EXTERNE
#endif
		/*
		// v�rification de la connexion et du statut RTK
		char connexionRTK[100], statutRTK[100];
		if (!ControlRobotG.GetRTKStatus(connexionRTK, statutRTK, msgerr)) return false;

		if(!strcmp(connexionRTK, "connected"))
		{
			sprintf(msgerr, "RTK non conforme: pas de connexion RTK");
			return false;
		}

		if(!strcmp(statutRTK, "fix") || strcmp(statutRTK, "float"))
		{
			sprintf(msgerr, "RTK non conforme: le statut de RTK n'est pas fix ou float");
			return false;
		}
		*/
		if (!ControlRobotG.GetRobotSensorData(latdeg, londeg, capdeg, msgerr))
			return false;
	}


	// cr�ation de nouvel rep�re local
	RepereG = new TRepereLocal(latdeg);
	
	// calcul de la position initiale du robot
	double x, y;
	RepereG->WGS84VersMetres(latdeg, londeg, x, y);


	// initialisation de filtre kalman
	double incT = double(ParamG.incT_Kalman) / 1000;  //p�riode filtre de Kalman en secondes

	TFiltreKalman *pFk = new TFiltreKalman(incT);

	// initialisation des param�tres pour le filtre de Kalman
	pFk->InitParametres(ParamG.sigmaConsigneMoteurs, ParamG.sigmaObsPos, ParamG.sigmaObsCapDeg, ParamG.sigmaPos0, ParamG.sigmaCapDeg0,
		ParamG.sigmaCapOffsetDeg, ParamG.sigmaCoeffMoteur, ParamG.sigmaCapOffsetDeg0, ParamG.sigmaCoeffMoteur0);


	// charge de cap initial du robot
	double CapRad = capdeg * (M_PI / 180);

	double CapOffsetRad = ParamG.offsetCapDeg * (M_PI / 180);
	double Coeff = ParamG.coeffMoteur;

	// correction d�calage du GPS pour initialiser les valeurs de d�part de Kalman
	x += DECALAGE_Y_GPS_ERX * sin(CapRad - CapOffsetRad);  // cap_erx = cap_vrai + cap_offset ->  cap_vrai = cap_erx - cap_offset
	y -= DECALAGE_Y_GPS_ERX * cos(CapRad - CapOffsetRad);

	// initialisation de l'�tat intial : x0, y0, cap0, offsetcap0, coeffmoteur
	pFk->ChargePosInit(x, y, CapRad - CapOffsetRad, CapOffsetRad, Coeff);
	
	// initialisation pour l'affichage de Kalman
	TimeDebutG = GetTickCount(); // temps courant

	// initialisation de timer pour le filtre Kalman
	if (ParamG.NfLogKalman[0] != '\0')
	{
		TNomFic NomFicherLogKalman;
		sprintf(NomFicherLogKalman, "%s_%d", ParamG.NfLogKalman, TimeDebutG);

		std::ofstream *pOs = new std::ofstream(ParamG.NfLogKalman, std::ofstream::out);
		AppliqueKalmanG.Init(pFk, ParamG.GPSExterne, pOs);		
	}
	else
		AppliqueKalmanG.Init(pFk, ParamG.GPSExterne);

	// on �tablit le timer de filtre Kalman
	if (!AppliqueKalmanG.SetTimer(msgerr)) return false;

	// on d�finit l'espace de travail du bras:
	if (pHexagoneBras != NULL)
	{
		delete pHexagoneBras; pHexagoneBras = NULL;
	}
	pHexagoneBras = new Hexagone(ParamG.RayonHexagone, ParamG.ResolutionHexagone);

	if (!ControlRobotG.ArmInitialised)
	{
		ControlRobotG.InitRightArm();
		ControlRobotG.ArmInitialised = true;
	}
	return true;
}

bool CloseRobot(char *msgerr)
// D�connecter le robot et d�truire, arr�ter le timer pour le filtre Kalman et d�truire le rep�re local
{
	if (ControlRobotG.ArmInitialised) // �teint le bras tout d'abord s'il est allum�
	{
		ControlRobotG.CloseRightArm();
		ControlRobotG.ArmInitialised = false;
		Sleep(1000);
	}

	if (RepereG) // d�truit rep�re local
	{
		delete RepereG;
		RepereG = NULL; // permettant la reconstruction de rep�re local d�s la re-connexion
	}

	if (pNmea) // d�truit la connexion port COM
	{
		delete pNmea; pNmea = NULL;
	}

	if (pHexagoneBras)
	{
		delete pHexagoneBras; pHexagoneBras = NULL;
	}

	if (CommandeNavigationG) // d�truit la commande de navigation (soit manuelle, soit automatique)
	{
		if (CommandeNavigationG->Timer_Commande) CommandeNavigationG->StopTimer();
		delete CommandeNavigationG; CommandeNavigationG = NULL;
	}

	if (AppliqueKalmanG.pKalman) // d�truit le timer pour filtre Kalman
	{
		AppliqueKalmanG.StopTimer();
		if (AppliqueKalmanG.pFichierLog) AppliqueKalmanG.pFichierLog->close();// ferme du fichier log

		// lecture de derniers r�sultats de filtre Kalman
		AppliqueKalmanG.RetourKalman = MATRICE(2,5);
		AppliqueKalmanG.RetourKalman.Constante(0);
		AppliqueKalmanG.pKalman->GetParametresKalman(AppliqueKalmanG.RetourKalman); 
	}

	if (!ControlRobotG.DeconnectRobot(msgerr)) return false;

	return true;
}


bool GetPositionRobotBrute(TPositionRobot &pos, bool GPSExterne, TRepereLocal &R, char *msgerr)
// r�ception de position brute du robot (soit par GPS Erx, soit par GPS RTK externe), sans correction de d�calage de la position GPS suivant l'axe y
{
	double latdeg, londeg;
	//dans tous les cas, cap fourni par erx.
	if (!ControlRobotG.GetRobotSensorData(latdeg, londeg, pos.CapDeg, msgerr))
		return false;

	if (GPSExterne)
	{
		int fix;
		if (!GetMesureGPSExterne(latdeg, londeg, fix, msgerr))
			return false;
	}

	R.WGS84VersMetres(latdeg, londeg, pos.x, pos.y);
	double theta = pos.CapDeg * M_PI / 180;

	return true;
}

//extern bool StopUrgenceG;

void StopPlateforme()
{

	if (CommandeNavigationG != NULL)
	{
		//StopUrgenceG = true; // � v�rifier si cette commande est oblig�e
		CommandeNavigationG->StopTimer();  // � tester seulement le stop timer
		delete CommandeNavigationG;
		CommandeNavigationG = NULL;
	}

	if (ControlRobotG.RobotIsConnected())
		ControlRobotG.SetRobotSpeed(0, 0);

}



bool InitGPSExterne(char* msgerr)
{
	// cherche du port com et choix de baud de communication:
	if (!GereGroupePortCOM(NULL, ParamG.ParamCOMGPSExterne, "PortComGPSExterne.cfg", false))
	{
		sprintf(msgerr, "Initialisation Port COM �chou�e");
		return false;
	}

	if (pNmea == NULL)
	{
		pNmea = new NmeaPosition(ParamG.ParamCOMGPSExterne.DescriptionPort, ParamG.ParamCOMGPSExterne.Baud);
	}

	NMEAData data;
	NmeaPositionState State = pNmea->GetData(data);

	if (State != NmeaPositionState::OK)
	{
		string msg = pNmea->GetStateString();
		string lastmsg = pNmea->GetLastError();
		strcpy(msgerr, (msg + '\n' + lastmsg).data());
		return false;
	}

	return true;
}


bool GetMesureGPSExterne(double& latDeg, double& lonDeg, int& fix, char* msgerr)
{
	if (!pNmea)
	{
		sprintf(msgerr, "GPS externe non initialis�");
		return true;
	}

	NMEAData Data;

	NmeaPositionState State = pNmea->GetData(Data);
	
	if (State != NmeaPositionState::OK)
	{
		string msg = pNmea->GetStateString();
		sprintf(msgerr, "Erreur GPS externe: %s", msg.c_str());
		return false;
	}

	latDeg = Data.LatDeg;
	lonDeg = Data.LonDeg;
	fix = Data.Fix;
	return true;
}



