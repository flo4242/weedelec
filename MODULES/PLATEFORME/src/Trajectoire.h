#ifndef __TRAJECTOIRE_H
#define __TRAJECTOIRE_H

#include "list_tpl.h"
#include "ligne.h"

struct Trajectoire
{

	_list<POINTFLT> ListePtsPassage; // liste de points de passage d�finis en flottant dans rep�re m�trique

	_list<double> AbscissesPtsPassage; // abscisse curviligne pour les points de passage

	//double startTime;  // en seconde, par exemple 201032.359 -> 201032 sec 359 milisecondes

	double LongueurTotale;

	int NbsPts;

	// functions de membre:

	Trajectoire() 
	// Initialisation de structure Trajectoire, param�tre : vitesse lin�aire pour calculer les points d�sir�s
	{ 
		ListePtsPassage.clear(); AbscissesPtsPassage.clear(); NbsPts = 0; LongueurTotale = 0;
	};

	void AjoutePointPassage(double xd, double yd); 

};




#endif // !__GENERATIONTRAJECTOIRE_H
