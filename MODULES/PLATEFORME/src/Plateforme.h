#ifndef __PLATEFORME_H__
#define __PLATEFORME_H__

#include "WeedelecCommon.h"
#include "ExportModulePlateforme.h"
#include "RepereLocal.h"
#include "Region.h"
#include <fstream>

#ifndef ENTRAXE_ROBOT
#define ENTRAXE_ROBOT   2.112528 // entraxe en m�tres
#endif


#define UNITE_MOUVEMENT_BRAS  0.01     // unit� de mouvement du bras en cm

class Hexagone
{
private:
	Region R;
	double Resolution;

public:

	Hexagone(double rayon, double resolution) : Resolution(resolution)
	{
		//d�termination des 6 sommets
		POINTFLT Sf[6];
		for (int i = 0; i < 6; i++)
		{
			double anglerad = i * M_PI / 3 + M_PI / 6;
			Sf[i] = POINTFLT(rayon * cos(anglerad), rayon*sin(anglerad));
		}

		//passage en coordonn�es enti�res
		LIGNE L(7);
		for (int i = 0; i < 6; i++)
		{
			L[i].x = Sf[i].x / resolution;
			L[i].y = Sf[i].y / resolution;
		}
		L[6] = L[0]; //fermeture de la ligne

		R = Region(~L);
	}

	bool Contient(double x, double y)
	{
		int xi = x / Resolution;
		int yi = y / Resolution;
		return R.ContientPoint(xi, yi);
	}
};


bool UpdateConsigneMoteursG = true;

struct TConsigneMoteurs
{
	double VG;  // vitesse de moteur gauche
	double VD;  // vitesse de moteur droite

	TConsigneMoteurs() : VG(0), VD(0) {};

	void UpdateConsigneCourante(double vg, double vd) { if (UpdateConsigneMoteursG) { VG = vg; VD = vd; } }
	void LireConsigneCourante(double &vg, double &vd) 
	{
		UpdateConsigneMoteursG = false;
		vg = this->VG; vd = this->VD;
		UpdateConsigneMoteursG = true;
	}

} ConsigneMoteursG;



struct TControlBrasManuel
{
	POINTFLT PositionBras;
	double Hauteur;

	bool UpdatePositionBras;

	bool ActiveControlManuel;

	TControlBrasManuel() 
	{
		PositionBras.x = 0; PositionBras.y = 0;
		Hauteur = 0.8;
		this->UpdatePositionBras = true;
		this->ActiveControlManuel = false;
	}

	void UpdateConsigneCourante(POINTFLT pos)
	{
		if (this->UpdatePositionBras) { PositionBras = pos; }
	}

	POINTFLT GetConsignePositionBras()
	{
		this->UpdatePositionBras = false;
		POINTFLT pos = PositionBras;
		this->UpdatePositionBras = true;
		return pos;
	}

	void PositionXPlus() { if (this->UpdatePositionBras) PositionBras.x += UNITE_MOUVEMENT_BRAS; }
	void PositionXMoins() { if (this->UpdatePositionBras) PositionBras.x -= UNITE_MOUVEMENT_BRAS; }
	void PositionYPlus() { if (this->UpdatePositionBras)  PositionBras.y += UNITE_MOUVEMENT_BRAS; }
	void PositionYMoins() { if (this->UpdatePositionBras) PositionBras.y -= UNITE_MOUVEMENT_BRAS; }
	
	void PositionZPlus() { if (this->UpdatePositionBras) Hauteur -= UNITE_MOUVEMENT_BRAS; }
	void PositionZMoins() { if (this->UpdatePositionBras) Hauteur += UNITE_MOUVEMENT_BRAS; }

	void ResetPositionBras() { PositionBras.x = 0; PositionBras.y = 0; };

} ControlBrasManuelG;




bool InitRobot(char *msgerr);

bool CloseRobot(char *msgerr);

bool GetPositionRobotBrute(TPositionRobot &pos, bool GPSExterne, TRepereLocal &R, char *msgerr);

void StopPlateforme();


bool InitGPSExterne(char* msgerr);

bool GetMesureGPSExterne(double& latDeg, double& lonDeg, int& fix, char* msgerr);

#endif // !__PLATEFORME_H__

