#include "ControlRobot.h" 
// il faut toujours inclure "ControlRobot.h" en premier pour que "erxCom.h" soit d�clar� en premier 
// (sinon incompatibilit� se trouve pour le module 'chrono') 

#include "drvtraiti.h"

#include "TPlateforme.h"
#include "WeedelecCommon.h"

#include "Plateforme.h"

#include "AppliqueFiltreKalmanErx.h"
#include "CommandeNavigation.h"
#include "KML.h"
#include "NmeaPosition.h"
#include "CommandeHT.h"


DRVTRAITIM *pDriverG;

TControlRobot ControlRobotG;

TParamRobot ParamG;

extern TAppliqueKalman AppliqueKalmanG;
extern TRepereLocal *RepereG;
extern NmeaPosition *pNmea;
extern TCommandeNavigation* CommandeNavigationG;

CommandeHT CommandeHTG;
TParamHT ParamHTG;



bool GereParametresHT(HWND hwnd, ModeGereParametres modeGereParam, TParamHT &paramHT);


extern "C"
{
	int _EXPORT_ Open_T_Plateforme(DRVTRAITIM *pD, HMODULE hinst)
		/*********************************************/
	{
		if (pD->SizeClass != sizeof(DRVTRAITIM))
		{
			char st[100];
			sprintf(st, "taille DRVTRAITIM non compatible - Driver: %d  DLL: %d",
				pD->SizeClass, sizeof(DRVTRAITIM));
			::MessageBox(NULL, st, "T_Plateforme", MB_OK);
			return 0;
		}
		pDriverG = pD;
		return 1;
	}

	int _EXPORT_ Close_T_Plateforme(DRVTRAITIM *) { return 1; }

	HMENU _EXPORT_ Ajoute_T_Plateforme(HMENU pere)
	/********************************/
	{
		HMENU Hprincipal = pDriverG->AjouteSousMenu(pere, "Plateforme", 
			"Param�tres", &TPlateforme::CMParametre,
			NULL);

		HMENU ret = pDriverG->AjouteSousMenu(Hprincipal, "Connexion Robot",
			"Connexion", &TPlateforme::CMConnectRobot,
			"D�connexion", &TPlateforme::CMDeconnectRobot,
			"Lecture mesures robot", &TPlateforme::CMTestMesureRobot, 
			"Active correction RTK", &TPlateforme::CMActiveRTKRobot,
			"Statut correction RTK", &TPlateforme::CMGetStatutRTKRobot,
			"Reset ground frame robot", &TPlateforme::CMResetGroundFrame,
			NULL);

		ret = pDriverG->AjouteSousMenu(Hprincipal, "GPS RTK externe",
			"Initialisation", &TPlateforme::CMInitCapteurGPS,
			"Lecture meseures GPS externe", &TPlateforme::CMTestCapteurGPSExterne,
			NULL);

		ret = pDriverG->AjouteSousMenu(Hprincipal, "Haute Tension",
			"Param�tres HT", &TPlateforme::CMParametreHT,
			"Initialisation HT", &TPlateforme::CMInitCommandeHT,
			"Lecture param�tres sur la carte HT", &TPlateforme::CMLireParametreHT,
			"Lecture condition de la carte HT", &TPlateforme::CMLireStatusHT,
			"Lance s�quence HT", &TPlateforme::CMLanceSequenceHT,
			NULL);

		ret = pDriverG->AjouteSousMenu(Hprincipal, "Weeding",
			"Contr�le de position des bras", &TPlateforme::CMDialogueHWCommand,
			"D�placement manuel du bras droit avec d�charge HT", &TPlateforme::CMDialogueControlBrasManuel,
			NULL);

		ret = pDriverG->AjouteSousMenu(Hprincipal, "G�n�ration de trajectoire",
			"Trajectoire sur fichier", &TPlateforme::CMInitTrajectoire,
			"Conversion linestring GoogleEarth to waypoints .kml", &TPlateforme::CMConvertPisteToWaypointKML,
			"Conversion points GPS .txt to waypoints .kml", &TPlateforme::CMConvertTxtToWaypointKML,
			NULL);
		
		ret = pDriverG->AjouteSousMenu(Hprincipal, "Commande de navigation",
			"Commande manuelle", &TPlateforme::CMNavigationManuelleRobot,
			"Suivi de trajectoire", &TPlateforme::CMNavigationAutoRobot,
			NULL);


		int Ajoutelist = pDriverG->AjouteListeItems(Hprincipal, "Stop Mouvement",
			&TPlateforme::CMStopMouvement, NULL);
		
		Ajoutelist = pDriverG->AjouteListeItems(Hprincipal, "D�monstration Weedelec",
			&TPlateforme::CMDemonstrationWeedelec, NULL);
		
		if (!TModulePlateforme::ChargeParametres(ParamG))
			pDriverG->Message("Echec chargement param�tres Robot");

		if(!GereParametresHT(NULL, ModeGereParametres::Charge, ParamHTG))
			pDriverG->Message("Echec chargement param�tres HT");
		
		/*if (!GereGroupeConfigPortCOM(pDriverG->HWindow(), ParamG.ParamCOMGPSExterne, true))
			pDriverG->Message("Echec chargement param�tres du port COM, GPS externe non initialis�");
			*/

		return Hprincipal;
	}
}


//////////////////////////
// G�n�rer des param�tres
//////////////////////////

bool GereParametres(HWND hwnd, ModeGereParametres modeGereParam, TParamRobot &param) //export
/***********************************************************************************/
{
	double HauteurWeedingSol = 1 - param.HauteurWeeding;

	GROUPE G(hwnd, "Param�tres Plateforme - Ecorobotix", "Plateforme.cfg",

		SEPART("****************************"),
		SEPART("***     Param�tres d'utilisateur     ***"),
		SEPART("****************************"),
		DBL("Vitesse nominale d'avancement (m/s)", param.VitesseMaxLineaire, 0, 5),
		DBL("Pas d'avancement pour le weeding (m)", param.PasAvancement, 0, 1E10),
		DBL("Hauteur du weeding par rapport au sol (m)", HauteurWeedingSol, 0, 1),
		FICH("Trajectoire d�sir�e (kml)", param.NfTrajectoire, "kml"),
		FICH("Fichier sauvegarde dernier �tat Kalman (txt)", param.NfResultsKalman, "txt"),
		FICH("Fichier log de filtre Kalman (txt)", param.NfLogKalman, "txt"),

		SEPART("      "),
		SEPART("****************************"),
		SEPART("***      Param�tres de d�tail        ***"),
		SEPART("****************************"),
		STR("Adresse IP Wifi", param.AdresseConnexionWifi),
		STR("Adresse Ethernet", param.AdresseConnexionEth),
		PBOOL("Connexion par Wifi", param.ConnexionWifi),
		PBOOL("Capteur GPS externe", param.GPSExterne),
		DBL("Decalage Y du GPS externe (m)", param.DecalageGPSExt, -1E5, 1E5),
		ENT("P�riode de commande (ms)", param.incT_Commande, 0, 1000000),
		ENT("P�riode de filtre Kalman (ms)", param.incT_Kalman, 0, 1000000),
		DBL("Cap Offset (�)", param.offsetCapDeg, -180, 180),
		DBL("Coefficient Moteur", param.coeffMoteur, 0, 1E10),

		DBL("Rayon zone hexagonale du bras (m)", param.RayonHexagone, 0, 1E10),
		DBL("R�solution zone hexagonale du bras (m)", param.ResolutionHexagone, 0, 1E10),
		DBL("Translation X du centre bras dans rep�re robot (m)", param.TranslationDeltaX, -1E5, 1E5),
		DBL("Translation Y du centre bras dans rep�re robot (m)", param.TranslationDeltaY, -1E5, 1E5),


		SEPART("      "),
		SEPART("****************************"),
		SEPART("***     Param�tres filtre Kalman    ***"),
		SEPART("****************************"),
		
		SEPART("--- Incertitudes instantan�es (Matrice Q) ---"),
		DBL("Incertitude de consigne moteurs (m/s)", param.sigmaConsigneMoteurs, 0, 1E6),
		DBL("Incertitude de cap offset (deg)", param.sigmaCapOffsetDeg, 0, 1E10),
		DBL("Incertitude de coefficient moteurs", param.sigmaCoeffMoteur, 0, 1E10),

		SEPART("--- Incertitudes d'observation (Matrice R) ---"),
		DBL("Incertitude d'observation sur la position (m)", param.sigmaObsPos, 0, 1E6),
		DBL("Incertitude d'observation sur le cap (deg)", param.sigmaObsCapDeg, 0, 1E6),

		SEPART("--- Incertitudes initiales (Matrice P0) ---"),
		DBL("Incertitude de la position initiale (m)", param.sigmaPos0, 0, 1E6),
		DBL("Incertitude du cap initial (deg)", param.sigmaCapDeg0, 0, 1E6),
		DBL("Incertitude du cap offset initial (deg)", param.sigmaCapOffsetDeg0, 0, 1E10),
		DBL("Incertitude de coefficient moteurs initial", param.sigmaCoeffMoteur0, 0, 1E10),

		NULL);

	bool OK;

	switch (modeGereParam)
	{
	case(ModeGereParametres::Charge): OK = G.Charge(); param.HauteurWeeding = 1 - HauteurWeedingSol; break;
	case(ModeGereParametres::Gere): OK = G.Gere(false); param.HauteurWeeding = 1 - HauteurWeedingSol; break; //modal pour pouvoir d�placer la bo�te de dialogue
	case(ModeGereParametres::Sauve): HauteurWeedingSol = 1 - param.HauteurWeeding; OK = G.Sauve(); break;
	}

	return OK;
}

void TPlateforme::CMParametre() {	GereParametres(HWindow(), ModeGereParametres::Gere, ParamG); }

bool TModulePlateforme::ChargeParametres(TParamRobot &param) { return GereParametres(NULL, ModeGereParametres::Charge, param); }



////////////////////////////////////////
// Initialisation et connexion du robot
////////////////////////////////////////

void TPlateforme::CMConnectRobot()
{
	char msg[1000];

	MATRICE ParamKalman;

	bool ret;
	if (ParamG.NfResultsKalman)
		ret = ParamKalman.Charge(ParamG.NfResultsKalman);
	
	if (ret && ParamKalman.dimC() == 5 && ParamKalman.dimL() == 2)  // v�rification de matrice charg�e
	{
		if (MessageOuiNon("Charge les derniers r�sultats pour initialiser filtre Kalman ?")) // charge dernier r�sultats de Kalman
		{
			ParamG.offsetCapDeg = ParamKalman[0][3];
			ParamG.coeffMoteur = ParamKalman[0][4];

			ParamG.sigmaPos0 = (ParamKalman[1][0] + ParamKalman[1][1]) / 2;  // incertitude de position prendra la moyenne des incertitudes de x et y
			ParamG.sigmaCapDeg0 = ParamKalman[1][2];
			ParamG.sigmaCapOffsetDeg0 = ParamKalman[1][3];
			ParamG.sigmaCoeffMoteur0 = ParamKalman[1][4];
		}
	}

	if (!InitRobot(msg))		Message(msg);
	else Message("Robot connect�");
}

void TPlateforme::CMDeconnectRobot()
{
	char msgerr[1000];

	if (!CloseRobot(msgerr))		Message(msgerr);
	else
	{
		Message("Robot d�connect�");
		
		if (MessageOuiNon("Sauve derniers r�sultats de Kalman?"))
			AppliqueKalmanG.RetourKalman.Sauve(ParamG.NfResultsKalman);
	}
}

void TPlateforme::CMTestMesureRobot()
{
	char msgerr[1000];

	if (!InitRobot(msgerr))
	{
		Message(msgerr);
		return;
	}

	else
	{
		OuvreTexte();
		EffaceTexte();
	}

	PrintTexte("Ici test mesures capteurs Ecorobotix\nEntrer pour demarrer puis touche espace pour stopper\n");

	while (true)
	{
		if ((GetAsyncKeyState(VK_RETURN) & 0x8000) != 0)			break;
	}

	int TDebut = GetTickCount();
	while (true)
	{
		double latDeg, lonDeg, capDeg;
		if (!ControlRobotG.GetRobotSensorData(latDeg, lonDeg, capDeg, msgerr))
		{
			Message(msgerr); return;
		}

		PrintTexte("Time: %.3f, Mesure Robot:   Lat: %.5f;      Lon: %.5f;      Cap: %.5f\n",
			double(GetTickCount() - TDebut) / 1000, latDeg, lonDeg, capDeg);

		if ((GetAsyncKeyState(' ') & 0x8000) != 0) 			break;
	}

}



// activation GPS rtk et reception de statut 

void TPlateforme::CMActiveRTKRobot()
{
	if (!ControlRobotG.RobotIsConnected())
	{
		Message("Pas de connexion!");
		return;
	}
		

	ControlRobotG.ActiveRTK();
	Message("Activation RTK done!");
}

void TPlateforme::CMGetStatutRTKRobot()
{
	char msgerr[1000];
	char ntripMsg[100];
	char rtkMsg[100];

	if (!ControlRobotG.GetRTKStatus(ntripMsg, rtkMsg, msgerr))
		Message(msgerr);
	else
		Message("Ntrip message : %s\nRTK message : %s", ntripMsg, rtkMsg);
}

void TPlateforme::CMResetGroundFrame()
{
	if (!ControlRobotG.RobotIsConnected())
		return;

	ControlRobotG.m_client_manager->resetErxGroundFrame();
	Message("Reset ground frame done.");
}



///////////////////////////////////////////
// intialisation du capteur externe de GPS
///////////////////////////////////////////

void TPlateforme::CMInitCapteurGPS()
{
	char msgerr[1000];

	if(!InitGPSExterne(msgerr))
		Message(msgerr);

	else
		Message("Initialisation GPS externe OK!");
}


void TPlateforme::CMTestCapteurGPSExterne()
{
	if (pNmea == NULL)
	{
		Message("GPS externe non initialis�");
		return;
	}

	OuvreTexte();
	EffaceTexte();
	PrintTexte("Ici test mesures capteurs GPS externe\nEntrer pour demarrer puis touche espace pour stopper\n");

	while (true)
	{
		if ((GetAsyncKeyState(VK_RETURN) & 0x8000) != 0)	break;
	}

	int TDebut = GetTickCount();

	while (true)
	{
		NMEAData Data;
		NmeaPositionState State = pNmea->GetData(Data);

		if (State == NmeaPositionState::OK)
		{
			PrintTexte("Time: %.3f  Mesure GPS:\n Lat: %.5f;   Lon: %.5f;   Altitude: %.3f;   Fix: %d\n\n",
				double(GetTickCount() - TDebut) / 1000, Data.LatDeg, Data.LonDeg, Data.Altitude, Data.Fix);
		}
		else
		{
			std::string Msg = pNmea->GetLastError();
			PrintTexte("%s\n", Msg.c_str());
		}

		if ((GetAsyncKeyState(' ') & 0x8000) != 0)	break;
	}
}



//////////////////////////////////
// commande de suivi trajectoire:
//////////////////////////////////

void TPlateforme::CMInitTrajectoire()
{
	char msgerr[1000];
	
	TNomFic NfKML = "";

	if (!ChoixFichierOuvrir("Initialisation de suivi de trajectoire", NfKML, "kml", "", HWindow()))
		return;

	sprintf(ParamG.NfTrajectoire, NfKML);

	Message("Fichier %s charg�", NfKML);
}


// fonction convertie des trajectoire 
#include "ConvertLineStringKML.h"

void TPlateforme::CMConvertPisteToWaypointKML()
{
	char msgerr[1000];

	TNomFic NfKML = "";
	TNomFic DossierSorti = "";

	_list<Waypoint> ListeWp;

	GROUPE G(HWindow(), "Conversion de trajectoire LineString en Waypoints (KML)", NULL,
		FICH("Fichier KML de LineString (pistes)", NfKML, "kml"),
		REPI("Dossier de fichier KML de sortie", DossierSorti),
		NULL);

	if (!G.Gere())	return;

	TNomFic Nf;
	strcpy(Nf, NomSansPath(NfKML));
	NomSansExt(Nf);

	TNomFic NfsortiKML;
	sprintf(NfsortiKML, "%s\\%s_Waypoint.kml", DossierSorti, Nf);
	
	if (!ConvertLineStringToWayPoint(NfKML, NfsortiKML, ListeWp, msgerr))
	{
		Message(msgerr);
		return;
	}
	else
		Message("%d Waypoints sauv�s dans %s", ListeWp.nbElem, NfsortiKML);

}

void TPlateforme::CMConvertTxtToWaypointKML()
{
	char msgerr[1000];

	TNomFic NfTxt = "";
	TNomFic DossierSorti = "";

	GROUPE G(HWindow(), "Conversion des points GPS .txt en Waypoints (KML)", NULL,
		FICH("Fichier txt des points GPS (pistes)", NfTxt, "txt"),
		REPI("Dossier de fichier KML de sortie", DossierSorti),
		NULL);

	if (!G.Gere())	return;

	MATRICE PointsGPS;
	if (!PointsGPS.Charge(NfTxt))
	{
		Message("Impossible de charger le fichier %s", NfTxt);
		return;
	}

	TNomFic Nf;
	strcpy(Nf, NomSansPath(NfTxt));
	NomSansExt(Nf);

	TNomFic NfsortiKML;
	sprintf(NfsortiKML, "%s\\%s_Waypoint.kml", DossierSorti, Nf);

	_list<Waypoint> ListeW;

	for (int i = 0; i < PointsGPS.dimL(); i++)
	{
		Waypoint Wp;
		Wp.LatDeg = PointsGPS[i][0];
		Wp.LonDeg = PointsGPS[i][1];
		Wp.elevation = 0;
		sprintf(Wp.Name, "P%d", i);
		sprintf(Wp.Description, "");

		ListeW.push_back(Wp);
	}

	if (!ExportKML(NfsortiKML, Nf, ListeW, msgerr))
	{
		Message(msgerr);
		return;
	}

	Message("%d Waypoints sauv�s dans %s", ListeW.nbElem, NfsortiKML);

}



///////////////////////////
// fonction stop plateforme
///////////////////////////

void TPlateforme::CMStopMouvement()
{
	StopPlateforme();
}




/*************************************************/
// commande navigation du robot
/*************************************************/
#include "ManualControlPad.h"

//commande manuelle (dialogue de commande)
void TPlateforme::CMNavigationManuelleRobot()
{
	/*if (!ControlRobotG.RobotIsConnected())
	{
		Message("Robot non connect�");
		return;
	}*/

	OuvreTexte();
	EffaceTexte();

	if (CommandeNavigationG != NULL)
	{
		delete CommandeNavigationG;  CommandeNavigationG = NULL;
	}

	CommandeNavigationG = new TCommandeManuelle(ParamG.incT_Commande); // initialisation avec la commande manuelle
	TCommandeManuelle * pCom = dynamic_cast <TCommandeManuelle*> (CommandeNavigationG);

	// �tabli timer pour la commande
	char msgerr[1000];
	if (!pCom->DoStartCommande(msgerr))
	{
		Message(msgerr); return; 
	}
	
	DialogControlPad("T_Plateforme");	
}



struct RetNavigationAuto
	// le retour de navigation automatique
{
	bool FinNavigationAuto; // indique la fin de navigation auto (soit finie avec succ�s, soit erreur)
	bool Succes; // indique si la navigation auto est bien finie sans erreurs
	std::string MsgRetour; // message de retour en cas d'erreur

	RetNavigationAuto() { FinNavigationAuto = false; Succes = false; MsgRetour = '\0'; };
	void Reset() { FinNavigationAuto = false; Succes = false; MsgRetour = '\0'; };

} RetNavigationAutoG;


void CallbackNavigationAuto(TRetourPlateforme ret)
{
	switch (ret.State)
	{
	case TRetourState::Completed:
		RetNavigationAutoG.Succes = true;
		break;

	case TRetourState::Error:
		RetNavigationAutoG.MsgRetour = ret.InfoRetour;
		break;

	case TRetourState::ExternalStop:
		RetNavigationAutoG.MsgRetour = "Stop Urgence";
		break;

	case TRetourState::TimeOut:
		RetNavigationAutoG.MsgRetour = "Time Out";
		break;

	default: break;
	}

	RetNavigationAutoG.FinNavigationAuto = true;
}


void TPlateforme::CMNavigationAutoRobot()
{
	char msgerr[1000];
	if (!ControlRobotG.RobotIsConnected() || AppliqueKalmanG.pKalman == NULL || RepereG == NULL)
	{
		Message("Robot non initialis�");
		return;
	}

	_list<Waypoint> ListeW;

	if (!ImporteKML(ParamG.NfTrajectoire, ListeW, msgerr))
	{
		Message(msgerr);
		return;
	}

	if (ListeW.nbElem < 2)
	{
		Message("Veuillez v�rifier la d�finition de la trajectoire\nAu moins deux points de passage d�sir�s sont n�cessaires");
		return;
	}


	CommandeNavigationG = new TCommandeAutomatique(ParamG.incT_Commande); // initialisation de pointeur pour la commande automatique
	TCommandeAutomatique *pCom = dynamic_cast <TCommandeAutomatique*> (CommandeNavigationG);

	pCom->InitVitesse(ParamG.VitesseMaxLineaire);

	AppliqueKalmanG.GetPositionRobotKalman(pCom->PosActuelle);

	BalayeListe(ListeW)
	{
		Waypoint * Wp = ListeW.current();
		double x, y;
		RepereG->WGS84VersMetres(Wp->LatDeg, Wp->LonDeg, x, y);
		pCom->TrajDesire.push_back(POINTFLT(x, y));
		// charge de points de passage d�sir�s dans la trajectoire
	}

	if (!pCom->DoStartCommande(msgerr))
	{
		Message(msgerr);
		delete CommandeNavigationG; CommandeNavigationG = NULL;
		return;
	}

	RetNavigationAutoG.Reset();  // reinitialise la structure de retour navigation auto

	OuvreTexte();
	EffaceTexte();
	PrintTexte("Commande automatique start ! \n");

	MSG msg;

	while (true)
	{
		if (PeekMessage(&msg, NULL, 0, 0, MB_OK)) DispatchMessage(&msg);

		if (RetNavigationAutoG.FinNavigationAuto) break;
	}

	if (RetNavigationAutoG.Succes)
		PrintTexte("Navigation auto finie avec la trajectoire d�sir�e parcourue");
	else
		PrintTexte("Navigation est arr�t�e pour l'erreur: %s", RetNavigationAutoG.MsgRetour.data());
}

/*************************************************/





/*********************************************************************************/
// commande et communication du bo�tier Haute Tension pour la d�charge �lectrique
/*********************************************************************************/

#include "ComPortSerieHT.h"
#include "GetListePortsCOM.h"

bool GroupeChoixPortCOM(HWND hwnd, char* PortCOM, char* msgerr)
// fonction qui cherche le port s�rie pour la communication avec le bo�tier de d�charge Haute tension
{
	// on va chercher tous les ports COM et choisir un dans la liste:
	_list<TNf> Liste;
	bool ret = GetListePortsCOM(Liste);

	if (Liste.nbElem == 0 || !ret)
	{
		sprintf(msgerr, "Pas de port COM pr�sent");
		return false;
	}

	//cr�ation de la liste multichoix
	AligneDescriptions(Liste);

	char** rubriques = new char*[Liste.nbElem + 1];
	int index = 0;

	BalayeListe(Liste)
	{
		char *temp = *Liste.current();
		while (true)
		{
			if (strncmp(temp, "COMx", 3) == 0) break;
			temp++;
		}
		rubriques[index++] = temp;
	}

	rubriques[index] = NULL;

	int IndexChoix = -1;

	GROUPE G(hwnd, "Configuration de Communication Port S�rie", NULL,
		MULCHOIX("Choix port COM USB", IndexChoix, rubriques),
		NULL);

	if (!G.Gere())
	{
		sprintf(msgerr, "Echec g�re groupe");
		delete rubriques;
		return false;
	}

	if (IndexChoix != -1)
		strcpy(PortCOM, rubriques[IndexChoix]);

	else
	{
		sprintf(msgerr, "Port COM non choisi");
		delete rubriques;
		return false;
	}

	return true;
}

bool GereParametresHT(HWND hwnd, ModeGereParametres modeGereParam, TParamHT &paramHT) 
/***********************************************************************************/
{
	GROUPE G(hwnd, "Param�tres Bo�tier Haute Tension - Weedelec", "ParametresHT.cfg",
		STR("Port COM (faut mieux ne pas changer ici)", paramHT.PortCOM),
		ENT("P�riode de charge de la bobine (us)", paramHT.DwellTime, 0, 65536),
		ENT("Fr�quence de r�p�tition des tirs (Hz)", paramHT.Frequence, 10, 1E7),
		ENT("Nombre de tirs r�alis�s par s�quence", paramHT.NbCycles, 1, 65536),
		ENT("Limite de courant dans la bobine (mA)", paramHT.CurrentLimit, 0, 65536),
		NULL);

	bool OK;

	switch (modeGereParam)
	{
	case(ModeGereParametres::Charge): OK = G.Charge(); break;
	case(ModeGereParametres::Gere): OK = G.Gere(true); break; //modal pour pouvoir d�placer la bo�te de dialogue
	case(ModeGereParametres::Sauve): OK = G.Sauve(); break;
	}

	return OK;
}

void TPlateforme::CMParametreHT()
{
	char msgerr[1000];

	if (!GroupeChoixPortCOM(HWindow(), ParamHTG.PortCOM, msgerr))
	{
		Message(msgerr);
		return;
	}
	GereParametresHT(NULL, ModeGereParametres::Sauve, ParamHTG); // sauv� le port com choisi

	if(!GereParametresHT(HWindow(), ModeGereParametres::Gere, ParamHTG))	return;

	GereParametresHT(NULL, ModeGereParametres::Sauve, ParamHTG); // sauv� tous les param�tres
}

void TPlateforme::CMInitCommandeHT()
{
	char msgerr[1000];

	if (!CommandeHTG.InitCommandeHT(ParamHTG, msgerr))
		Message(msgerr);
	else
		Message("Initialisation HT OK!");
}

void TPlateforme::CMLireParametreHT()
{
	char msgerr[1000];
	TParamHT paramHT;
	if (!CommandeHTG.ReadParamHT(paramHT, msgerr))
		Message(msgerr);
	else
	{
		Message("Param�res sur la carte:\n\nDwell_Time : %d (us)\nFrequence : %d\nNb_Cycles : %d\nCurrent_Limit : %d (mA)",
			paramHT.DwellTime, paramHT.Frequence, paramHT.NbCycles, paramHT.CurrentLimit);
	}
}

void TPlateforme::CMLireStatusHT()
{
	char msgerr[1000];
	double Temp, Voltage;

	if (!CommandeHTG.ReadTemperature(Temp, msgerr))
	{
		Message(msgerr);
		return;
	}
	
	if (!CommandeHTG.ReadVoltageBattery(Voltage, msgerr))
	{
		Message(msgerr);
		return;
	}
		
	Message("Condition actuelle de la carte:\n\nTemp�rature : %.2f (�C)\nVoltage batterie : %.2f (V)", Temp, Voltage);
}

void TPlateforme::CMLanceSequenceHT()
{
	char msgerr[1000];
	int CourantMax, VoltageTir, DureeTir;
	if (!CommandeHTG.StartSequence(CourantMax, VoltageTir, DureeTir, msgerr))
	{
		Message(msgerr);
		return;
	}
	
	Message("S�quence lanc�e avec l'info retour :\n\nCourant max: %d (mA)\nTension moyenne de s�quence : %d (V)\nDur�e de l'arc : %d (us)",
			CourantMax, VoltageTir, DureeTir);
}

/*********************************************************************************/




/**********************************************************************************/
// fonctions pour le weeding
/**********************************************************************************/

// dialogue de contr�le des bras par la fonction d'API SendHWcommand de l'Ecorobotix
#include "DialogSendHWCommand.h"
#include "IDControlPad.h"

void TPlateforme::CMDialogueHWCommand()
{
	/*if (!ControlRobotG.RobotIsConnected())
	{
		Message("Robot non connect�");
		return;
	}*/
	OuvreTexte();
	EffaceTexte();

	DialogSendHWCommand("T_Plateforme");
}

// dialogue de weeding avec la d�charge haute tension
#include "DialogueHauteTension.h"
void TPlateforme::CMDialogueControlBrasManuel()
{
	/*if (!ControlRobotG.RobotIsConnected())
	{
		Message("Robot non connect�");
		return;
	}*/
	OuvreTexte();
	EffaceTexte();

	DialogHauteTension("T_Plateforme");
}

/***********************************************************************************/





// fonctions provisoires:

task<void> CreateTaskTestClavier(unsigned int period)
{
	// �tabli si action clavier
	task_completion_event<void> tce;

	// Create a call object for timer events.
	// cr�� par new car doit survivre � la construction de la t�che 
	auto callback = new call<bool>([tce](bool)
	{
		if ((GetAsyncKeyState(VK_SPACE) & 0x8000) != 0)	tce.set();
	});


	// Create a repeating timer.

	timer<bool> *Timer = new timer<bool>(period, NULL, callback, true);
	// cr�� par new car doit survivre � la construction de la t�che 
	//bool�en arbitraitrement car doit forc�ment avoir un type non void (2�me argument: T&)

	Timer->start();

	// Create a task that completes after the completion event is set.
	// Create a task that completes after the completion event is set.
	task<void> event_set(tce);

	// Create a continuation task that cleans up resources and
	// and return that continuation task.
	return event_set.then([callback, Timer]()
	{
		delete callback;
		delete Timer;
		return;
	});
}

void TPlateforme::CMDemonstrationWeedelec()
{
	bool verif = MessageOuiNon(
		"<D�monstration Weedelec>\nAvancement robot d'un m�tre et d�placement du bras droit avec la position des weeds fixe (0.1,0.3) et (-0.15,-0.1)\n\nCommencer ?");

	if (!verif) return;

	double weed1x = 0.1, weed1y = 0.3;
	double weed2x = -0.15, weed2y = -0.1;

	ControlRobotG.HauteurWeeding = ParamG.HauteurWeeding;

	if (!ControlRobotG.ArmInitialised)
	{
		ControlRobotG.InitRightArm();
		ControlRobotG.ArmInitialised = true;
	}

	MSG msg;
	task<void> arret = CreateTaskTestClavier(100);
	
	while (true)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 	DispatchMessage(&msg);

		//ControlRobotG.Avancement(1); // avancement d'un m�tre ne fonctionne plus si pas de vision
		int cnt = 0;
		while (true)
		{
			if (arret.is_done())
			{
				ControlRobotG.SetRobotSpeed(0, 0);
				ControlRobotG.ResetRightArm();
				break;
			}

			double V = ParamG.VitesseMaxLineaire;
			ControlRobotG.SetRobotSpeed(V, V);
			Sleep(150);

			cnt++;
			if (cnt == 5) break;
		}

		Sleep(2500);

		ControlRobotG.SetPositionXYWeedR(POINTFLT(weed1x, weed1y));
		Sleep(300);
		ControlRobotG.SetPositionZWeedingR();
		Sleep(500);

		ControlRobotG.SetPositionAwaitedR();
		Sleep(300);

		ControlRobotG.SetPositionXYWeedR(POINTFLT(weed2x, weed2y));
		Sleep(300);
		ControlRobotG.SetPositionZWeedingR();
		Sleep(500);

		ControlRobotG.SetPositionAwaitedR();
		Sleep(300);

		if (arret.is_done())
		{
			ControlRobotG.SetRobotSpeed(0, 0);
			ControlRobotG.ResetRightArm();
			break;
		}
	}

	Message("D�monstration finie");
}



/*
// ex weeding
void TPlateforme::CMTestInsertWeeds()
{
	if (!ControlRobotG.RobotIsConnected())
		return;
	ControlRobotG.m_client_manager->startInjectWeeds(true);
}

void TPlateforme::CMInsertWeeds()
{
	if (!ControlRobotG.RobotIsConnected())
		return;

	double gndxp1 = -0.5;
	double gndyp1 = -0.8;

	double gndxp2 = -0.3;
	double gndyp2 = -0.4;

	GROUPE G(HWindow(), "Coordonn�es inject weeds :", NULL,
		DBL("Ground X", gndxp1, -1E10, 1E10),
		DBL("Ground Y", gndyp1, -1E10, 1E10),
		DBL("Ground X", gndxp2, -1E10, 1E10),
		DBL("Ground Y", gndyp2, -1E10, 1E10),
		NULL);

	if (!G.Gere()) return;

	web::json::value bufferweeds;


	bufferweeds[0][L"gx"] = web::json::value::number(gndxp1);
	bufferweeds[0][L"gy"] = web::json::value::number(gndyp1);

	bufferweeds[1][L"gx"] = web::json::value::number(gndxp2);
	bufferweeds[1][L"gy"] = web::json::value::number(gndyp2);

	ControlRobotG.m_client_manager->injectWeeds(bufferweeds);

	return;
}

void TPlateforme::CMStopInsertWeeds()
{
	if (!ControlRobotG.RobotIsConnected())
		return;

	ControlRobotG.m_client_manager->startInjectWeeds(false);

}
*/



/*
//////////////////////////////////////////////////////////////
// Test de Filtre Kalman (off-line et on-line)
//////////////////////////////////////////////////////////////

// test on-line:
extern TAppliqueKalman AppliqueKalmanG;

bool TestFiltreKalmanPasAPas(char* msgerr)
{
	if (!ControlRobotG.RobotIsConnected())
	{
		sprintf(msgerr, "Pas de connexion!");
		return false;
	}

	if (AppliqueKalmanG.pKalman == NULL)
	{
		sprintf(msgerr, "Filtre Kalman non initialis�!");
		return false;
	}

	while (true)
	{
		if (!AppliqueKalmanG.EvolutionMesuresKalman())
		{
			sprintf(msgerr, "Evolution Kalman echou�e");
			return false;
		}

		AfficheKalman();

		if (!pDriverG->MessageOuiNon("Etape suivante?"))
			break;
	}
	return true;
}

void TPlateforme::CMTestFiltreKalmanOnLine()
{
	char msgerr[1000];
	if (!InitRobot(false, msgerr))
		Message(msgerr);
	else
	{
		ImKalmanG = NouvelleImageGris("Affichage Filtre Kalman", 700, 400);
		ImKalmanG->Indestructible = true;
		ImKalmanG->Efface(0);
		ImKalmanG->Redessine();
	}

	if (!TestFiltreKalmanPasAPas(msgerr))
		Message(msgerr);
}



// test off-line par un fichier de donn�es pr�sauv� et charg�

#include "TestKalmanOffLine.h"
#include <fstream>

int CountKalmanOffline = 0;

void AfficheKalmanOffline(TestKalmanOffLine Kalman, ofstream *pOs)  // affichage de l'�tat courant, variance courante et la consigne de moteurs courante
{
	// if (!AffichageKalmanG) return;
	// Cet affichage de Kalman est �tabli dans la m�me callback fonction (le m�me Timer) que Kalman
	// Donc pas besoin de v�rifier si les valeurs sont en train d'�tre modifi�es (jamais le cas)
	if (ImKalmanG == NULL) return;

	int Ts = CountKalmanOffline++;

	VECTEUR EtatCourant = Kalman.pKalman->GetEtatCourant();
	MATCARREE Covariance = Kalman.pKalman->P;

	VECTEUR EcartType(5);
	EcartType[0] = sqrt(Covariance[0][0]); EcartType[1] = sqrt(Covariance[1][1]);
	EcartType[2] = sqrt(Covariance[2][2]) * (180 / M_PI);
	EcartType[3] = sqrt(Covariance[3][3]) * (180 / M_PI);
	EcartType[4] = sqrt(Covariance[4][4]);

	double CMgauche = Kalman.pKalman->Vg;
	double CMdroite = Kalman.pKalman->Vd;

	VECTEUR Mes = Kalman.pKalman->Obs;

	if (pOs)
		SauveLogKalman(pOs, Ts, EtatCourant, EcartType, POINTFLT(CMgauche, CMdroite), Mes);

	ImKalmanG->Printf(5, 0, "   Working Time : %d  ", Ts);

	ImKalmanG->Printf(25, 0, "   Current state :   ");
	ImKalmanG->Printf(50, 0, "   X (m) : %.2f      Y (m) : %.2f      CAP (deg) : %.2f   ", EtatCourant[0], EtatCourant[1], EtatCourant[2] * (180 / M_PI));
	ImKalmanG->Printf(75, 0, "   CAP offset (deg) : %.2f      K moteur : %.2f    ", EtatCourant[3] * (180 / M_PI), EtatCourant[4]);

	ImKalmanG->Printf(125, 0, "   Current Ecart-type :   ");
	ImKalmanG->Printf(150, 0, "   X (m) : %.2f      Y (m) : %.2f      CAP (deg) : %.2f   ", EcartType[0], EcartType[1], EcartType[2]);
	ImKalmanG->Printf(175, 0, "   CAP offset (deg) : %.2f      K moteur : %.2f    ", EcartType[3], EcartType[4]);

	ImKalmanG->Printf(225, 0, "   Motor commands (m/s):   ");
	ImKalmanG->Printf(250, 0, "   Gauche : %.2f      Droite : %.2f   ", CMgauche, CMdroite);

	ImKalmanG->Printf(300, 0, "   Current Measurement :   ");
	ImKalmanG->Printf(325, 0, "   X (m): %.2f       Y (m) : %.2f      Cap (deg) : %.2f   ", Mes[0], Mes[1], Mes[2] * 180 / M_PI);

	ImKalmanG->Redessine();

}


void TPlateforme::CMTestFiltreKalmanOffLine()
{
	char msgerr[1000];

	double incT = 0.5;
	TNomFic Finput = "C:\\Users\\shiyu.liu\\Desktop\\Kalman\\Apres_Calibration\\Mes_2.txt";

	GROUPE G(HWindow(), "Test filtre Kalman off-line", "",
		DBL("P�riode de Kalman (sec)", incT, 0, 1000),
		FICH("Charge de ficher des mesures", Finput, "txt"),
		NULL);

	if (!G.Gere()) return;

	TFiltreKalman *pFk = new TFiltreKalman(DIM_KALMAN);

	// initialisation des param�tres pour le filtre de Kalman
	pFk->InitParametres(incT, ParamG.sigmaConsigneMoteurs, ParamG.sigmaObsPos, ParamG.sigmaObsCapDeg, ParamG.sigmaPos0, ParamG.sigmaCapDeg0,
		ParamG.sigmaCapOffsetDeg, ParamG.sigmaCoeffMoteur, ParamG.sigmaCapOffsetDeg0, ParamG.sigmaCoeffMoteur0);

	std::ofstream outfile(ParamG.NfLogKalman);

	TPositionRobot Pos;

	TestKalmanOffLine TestKalman;
	bool ret = TestKalman.Mesures.Charge(Finput);

	Pos.x = TestKalman.Mesures[0][1];
	Pos.y = TestKalman.Mesures[0][2];
	Pos.CapDeg = TestKalman.Mesures[0][3];

	double x = Pos.x + DECALAGE_Y_GPS_ERX * sin(Pos.CapDeg*M_PI / 180);
	double y = Pos.y - DECALAGE_Y_GPS_ERX * cos(Pos.CapDeg*M_PI / 180);

	pFk->ChargePosInit(x, y, Pos.CapDeg*M_PI / 180, 0, 1);

	TestKalman.Init(pFk);


	ImKalmanG = NouvelleImageGris("Affichage Filtre Kalman", 700, 400);
	ImKalmanG->Indestructible = true;
	ImKalmanG->EcritAussiSurEcran = false;
	ImKalmanG->Efface(0);
	ImKalmanG->Redessine();


	while (true)
	{
		if ((GetAsyncKeyState(VK_RETURN) & 0x8000) != 0)	break;
	}

	bool step = true;

	while (true)
	{

		while (true)
		{
			if ((GetAsyncKeyState(VK_RETURN) & 0x8000) != 0)	break;

			if ((GetAsyncKeyState(VK_ESCAPE) & 0x8000) != 0)
			{
				ImKalmanG->Indestructible = false;
				return;
			}
		}

		if (!TestKalman.EvolutionKalman(msgerr))
		{
			ImKalmanG->Indestructible = false;
			Message(msgerr);
			return;
		}

		AfficheKalmanOffline(TestKalman, &outfile);

		while (true)
		{
			if ((GetAsyncKeyState(VK_RETURN) & 0x0001) != 0)	break;
		}
	}
}
*/


/*
// test fonction weeding
void TPlateforme::CMTestDoWeeding()
{
	char msgerr[1000];

	if (!ControlRobotG.ArmInitialised)
	{
		ControlRobotG.InitRightArm();
		ControlRobotG.ArmInitialised = false;
	}


	TRetourPlateforme ret;
	TPositionRobot Pos;

	MSG msg;

	while (true)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 	DispatchMessage(&msg);

		_list<POINTFLT> liste_weeds;

		liste_weeds.push_back(POINTFLT(0.3, 0.1));
		liste_weeds.push_back(POINTFLT(-0.1, 0.4));


		TModulePlateforme::Weeding(liste_weeds, ret, Pos, false, false, false);

		if (!MessageOuiNon("Continue le pas suivant ?"))
			break;
	}
	//ControlRobotG.ResetRightArm();
	ControlRobotG.CloseRightArm();
	Message("Test fini");
}*/


