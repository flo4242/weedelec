#ifndef __REPERELOCAL__H
#define __REPERELOCAL__H



//TRepereLocal: rep�re permettant de convertir localement des coordonn�es WGS84 en m�tres.

//Dans le sens Nord-Sud, on n'a pas de relation lin�aire directe entre latitude et position m�trique du fait de l'aplatissement
// de l'ellipso�de --> on lin�arise autour d'une latitude moyenne qu'il faut fournir � la construction. La valeur en m�tres
// fournie est relative � cette latitude moyenne.
//Pour le sens Est-Ouest, la relation entre longitude et position m�trique reste lin�aire --> pas besoin de lin�ariser
//autour d'une valeur moyenne. La valeur en m�tres est donc comptabilis�e � partir du m�ridien d'origine (GreenWich).
// (par ailleurs, ca �vite au programme appelant le calcul d'une longitude moyenne, avec la gestion des probl�mes �ventuels
// de discontinuit� [180�;-180�]).

class TRepereLocal
	{
	public:
	double a, b;	//grand axe et petit axe de l'ellipso�de
	double LatDeg0;	//latitude centre du rep�re en degr�s
	double RlocalH;	//rayon en m�tres du parall�le passant par le rep�re (conversion des longitudes)
	double RlocalV;	//rayon �quivalent en m�tres � utiliser pour les variations de latitude
	
	public:
	TRepereLocal(double latdeg0);
	
	void WGS84VersMetres(double LatDeg, double LonDeg, double &Xm, double &Ym);
	void MetresVersWGS84(double &LatDeg, double &LonDeg, double Xm, double Ym);

	};

#endif