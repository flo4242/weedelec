#ifndef __TESTKALMANOFFLINE_H_
#define __TESTKALMANOFFLINE_H_

#include "FiltreKalmanRobotErx.h"
#include "Plateforme.h"
#include <ostream>


struct TestKalmanOffLine
{
	//ofstream *pFLog;
	ifstream *pFInput;
	MATRICE Mesures;

	TFiltreKalman *pKalman;

	TPositionRobot PosRobotKalman;

	TestKalmanOffLine() { pKalman = NULL; };


	bool EvolutionKalman(char *msgerr);

	bool LireMesure(TPositionRobot& MesRobot, POINTFLT& ConsigneMoteurs, char *msgerr);
	
	void Init(TFiltreKalman *kalman, ifstream *pInput = NULL)
	{
		pKalman = kalman; pFInput = pInput;
	};

};



#endif // !__TESTKALMANOFFLINE_H_

