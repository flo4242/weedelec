
#include "AppliqueSuiviTrajectoire.h"
#include "ExportModulePlateforme.h"
#include "KML.h"

extern TParamRobot ParamG;

extern TCommandeCinetique TCommandeG;
extern DRVTRAITIM *pDriverG;

bool InitSuiviTrajectoire(char* NfKML, _list<Waypoint>& ListeWp, char* msgerr)
{
	// charge fichier KML contenant des points d�sir�s (stock�s par liste de waypoints)
	_list<Waypoint> ListeW;

	if (!ImporteKML(NfKML, ListeW, msgerr)) return false;


	double LatM = 0; // trouver le rep�re local par la moyenne de latitude des points d�sir�s

	BalayeListe(ListeW)
	{
		Waypoint Wp = *ListeW.current();
		LatM += Wp.LatDeg;
	}
	LatM /= ListeW.nbElem;

	TRepereLocal TRp(LatM); // rep�re local (variable locale pour calculer la trajectoire)

	Trajectoire Traj; // initialiser la trajectoire d�sir�e

	BalayeListe(ListeW)  // construire la trajectoire d�sir�e � partir des points d�sir�s
	{
		double x = 0, y = 0;
		double lat, lon;
		Waypoint wp = *ListeW.current();
		lat = wp.LatDeg;
		lon = wp.LonDeg;

		TRp.WGS84VersMetres(lat, lon, x, y);
		Traj.AjoutePointPassage(x, y);
	}

	// initialisation de filtre Kalman
	TFiltreKalman Fk(DIM_KALMAN);
	double incT = ParamG.incT_Commande * 1.0 / 1000;

	Fk.InitParametres(incT, ParamG.sigmax0, ParamG.sigmatheta0, ParamG.sigmav0, ParamG.sigmadtheta0, ParamG.sigmax, ParamG.sigmatheta,
		ParamG.sigmav, ParamG.sigmadtheta, ParamG.sigmaObsx, ParamG.sigmaObstheta);

	// initialisation de structure de la commande (qui va contenir les structures de rep�re local, filtre Kalman et trajectoire d�sir�e)
	TCommandeG.InitParam(Fk, Traj, LatM, ParamG.VitesseLinSuivi, ParamG.incT_Commande);

	ListeWp = ListeW;
	return true;
}

bool AppliqueSuiviTrajectoire(char* msgerr)
{
	char msg[1000];
	double Lat = 0, Lon = 0, Orient = 0, Vitesse = 0;
	double x_local = 0, y_local = 0;

	if (!GetMesureEnTempsReel(Lat, Lon, Orient, Vitesse, msg))
	{
		sprintf(msgerr, "Echec : %s", msg);
		return false;
	}

	TCommandeG.pR->WGS84VersMetres(Lat, Lon, x_local, y_local);

	double theta = Orient * M_PI / 180;
	x_local -= DIST_DECALAGE * sin(theta);
	y_local += DIST_DECALAGE * cos(theta); // correction d�calage GPS

	TCommandeG.Fk->ChargePosInit(x_local, y_local, theta, Vitesse, 0);

	TCommandeG.SetPointActuel(x_local, y_local, theta);

	TCommandeG.SetPointDesireActuel(x_local, y_local, theta);

	POINTFLT P0 = TCommandeG.TrajDesire.ListePtsPassage[0].obj;
	TCommandeG.SetPointPassageDesire(P0.x, P0.y);

	if (!TCommandeG.StartCommande(msgerr)) return false;

	return true;
}
