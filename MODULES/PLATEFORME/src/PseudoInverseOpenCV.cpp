#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "PseudoInverseOpenCV.h"

MATRICE PseudoInverseMatrice(MATRICE Matrice)
{
	int dimX, dimY;
	dimX = Matrice.dimC(); // dimension colonne
	dimY = Matrice.dimL(); // dimension ligne

	cv::Mat M = cv::Mat(dimY, dimX, CV_64F);

	for (int i = 0; i < dimY; i++)
		for (int j = 0; j < dimX; j++)
		{
			double val = Matrice[i][j];
			M.at<double>(i, j) = val;
		}
			
		
	cv::Mat Mdest; 

	cv::invert(M, Mdest, cv::DECOMP_SVD); // DECOMP_SVD: calcule pseudo-inverse si matrice est sigulaire
	
	int dimdestX, dimdestY;
	dimdestX = Mdest.cols; // dimension colonne
	dimdestY = Mdest.rows; // dimension ligne

	MATRICE pInvMatrice(dimdestY, dimdestX);
	pInvMatrice.Constante(0);

	for (int i = 0; i < dimdestY; i++)
		for (int j = 0; j < dimdestX;  j++)
		{
			double val = Mdest.at<double>(i, j);
			pInvMatrice[i][j] = val;
		}
			

	return pInvMatrice;
}
