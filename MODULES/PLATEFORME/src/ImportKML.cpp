﻿#include <fstream>
#include <strstream>
using namespace std;


#include "ParseXML.h"
#include "utilfic.h"
#include "KML.h"




///////////////////// IMPORT KML via outil ParseXML /////////////////////////////
// (en fin de fichier, importation directe d'un waypoint depuis le press-papier,
// hors outil libexpat.dll car antérieur )



//structures intermédiaires de récupération des données sous forme de chaînes


struct STWayPointKML
	{
	char Name[500];
	char Description[500];
	char Coordinates[500];
	STWayPointKML()	{ Name[0] = Description[0] = Coordinates[0] ='\0';	}
	};

struct STPisteKML
	{
	char *Coordinates;
	int Taille;

	STPisteKML(int taille) : Taille(taille)	{ Coordinates = new char[taille];	}
	STPisteKML(void) : Taille(0)	{ Coordinates = NULL;	}
	~STPisteKML()	{ if(Coordinates)	delete Coordinates;	}
	STPisteKML(const STPisteKML& l)  : Taille(l.Taille)
		 { Coordinates = new char[l.Taille]; memcpy(Coordinates, l.Coordinates, Taille);	}
		void operator = (const STPisteKML &l)
			{
			 if(Coordinates)	delete Coordinates;
			 Taille = l.Taille;		 Coordinates = new char[l.Taille];
			 memcpy(Coordinates, l.Coordinates, Taille);
			 }

    };


struct TdataParserKML
	{
	char *Buffer; //alloué dynamiquement selon taille fichier
	int Index;
	bool PlaceMarkEnCours;
	bool NameEnCours;
	bool DescriptionEnCours;
	bool PointEnCours;
	bool PisteEnCours;
	bool CoordinatesEnCours;

	bool PointAStocker;
	bool PisteAStocker;


	int TailleF;

	STWayPointKML courant;
	_list<STWayPointKML> Liste;
	_list<STPisteKML> ListeP;

	TdataParserKML(char *Nf)
		{
		PlaceMarkEnCours =	NameEnCours = DescriptionEnCours = false;
		PointEnCours = CoordinatesEnCours = false;
		Index=0;

		if(TailleFichier(Nf, TailleF))	Buffer = new char[TailleF];
		else Buffer = NULL;
		}

	~TdataParserKML()	{ if(Buffer)	delete Buffer;	}
	};


//handlers de récupération lors du parsing

void  HStartKML(void *userData, const char *el, const char **attr)
/***************************************************************/
{
TdataParserKML * pdata = (TdataParserKML *)userData;
STWayPointKML *pWp = &(pdata->courant);

if(strcmp(el, "Placemark")==0)
		 {
		 pdata->PlaceMarkEnCours=true;
		 pdata->PointAStocker=false;
		 pdata->PisteAStocker=false;
		 }

if(strcmp(el, "name")==0)	{	pdata->NameEnCours=true; pWp->Name[0]='\0';	}
if(strcmp(el, "description")==0)
		{ 	pdata->DescriptionEnCours=true; 	pWp->Description[0]='\0';	}
if(strcmp(el, "Point")==0) 	 pdata->PointEnCours=true;
if(strcmp(el, "LineString")==0)	pdata->PisteEnCours=true;

if(strcmp(el, "coordinates")==0)
		{ 	pdata->CoordinatesEnCours=true; pdata->Index =0; pWp->Coordinates[0]='\0';	}

}  /* End of start handler */

void exConcateneDest(char *dest, const char* src, int len, int taillemax)
/********************************************************************/
{
int offset = strlen(dest); if(len>= taillemax-offset)	return;
memcpy(dest+offset, src, len); dest[offset+len] = '\0';
}

void ConcateneDest(char *dest, const char* src, int len, int taillemax)
/********************************************************************/
{
	//conversion depuis UTF 8

	//création d'une chaine pour fonctions de conversion

	char *st = new char[len+1];
	memcpy(st, src, len);
	st[len] = '\0';

	//conversion utf8 --> unicode
	wchar_t * stw =  ConversionUnicode(st, NULL, true);
	//conversion unicode --> ansi
	char * stansi = ConversionDepuisUnicode(stw, NULL, false);

	int len2 = strlen(stansi);

	int offset = strlen(dest); if(len2>= taillemax-offset)	return;
	//memcpy(dest+offset, src, len); dest[offset+len] = '\0';
	memcpy(dest+offset, stansi, len2); dest[offset+len2] = '\0';

	delete stw;
	delete stansi;
}


void HCharKML(void *userData, const char *s, int len)
/***************************************************/
{
TdataParserKML * pdata = (TdataParserKML *)userData;
STWayPointKML *pWp = &(pdata->courant);

if(CDATAEnCours())	return;   //XXXXX

if(pdata->NameEnCours)		ConcateneDest(pWp->Name, s, len, 500);
if(pdata->DescriptionEnCours)		ConcateneDest(pWp->Description, s, len, 500);

if(pdata->CoordinatesEnCours)
	{
	 if(pdata->PointEnCours)	ConcateneDest(pWp->Coordinates, s, len, 500);
	 else if(pdata->PisteEnCours)//c'est une piste --> toutes les coordonnées à la suite
		{	memcpy(pdata->Buffer + pdata->Index, s, len); pdata->Index += len;	}
    }

}



void HEndKML(void *userData, const char *el)
/******************************************/
{
TdataParserKML * pdata = (TdataParserKML *)userData;
STWayPointKML *pWp = &(pdata->courant);


if(strcmp(el, "name")==0)	pdata->NameEnCours=false;
if(strcmp(el, "description")==0) 	pdata->DescriptionEnCours=false;
if(strcmp(el, "coordinates")==0)	pdata->CoordinatesEnCours=false;
if(strcmp(el, "Point")==0) 	 { pdata->PointEnCours=false; pdata->PointAStocker=true;	}
//s'il y en a plusieurs dans le PlaceMark, on ne stockera que le dernier...
if(strcmp(el, "LineString")==0) 	 { 
	pdata->Buffer[pdata->Index++] = '\0';
	pdata->PisteEnCours = false; pdata->PisteAStocker = true;
}

if(strcmp(el, "Placemark")==0)
	 {
	 pdata->PlaceMarkEnCours=false;

	if(pdata->PointAStocker)	pdata->Liste.push_back(*pWp);

	else if(pdata->PisteAStocker)
		{
		STPisteKML STP(pdata->Index);
		if(STP.Coordinates)
			{
			memcpy(STP.Coordinates, pdata->Buffer, pdata->Index);
			pdata->ListeP.push_back(STP);
			}
		}
	}
}

bool GetKMLCoordinates(char *s, double &lat, double &lon, double &alt)
/********************************************************************/
//format: 3.5862,43.8634,212 (lon, lat, alt) ; doit contenir fin de chaine
{
char *plon= s;	char *plat = NULL;	char *palt = NULL;
//remplacement des virgules par des fins de chaîne
for(char *pnt =s;  *pnt!='\0'; pnt++)
	if(*pnt==',')	{ *pnt='\0'; plat = pnt+1; break;}
if(!plat)	return false;	//pas trouvé
for(char *pnt =plat;  *pnt!='\0'; pnt++)
	if(*pnt==',')	{ *pnt='\0'; palt = pnt+1; break;}
if(!palt)	return false;	//pas trouvé
lat = atof(plat); lon = atof(plon); alt = atof(palt);
return true;
}

void GetWayPointFromPiste(TdataParserKML* pdata)
{
	int indexPistePt = 1;  // index du point extrait depuis le linestring

	if (!pdata->ListeP.nbElem) return;

	BalayeListe(pdata->ListeP)
	{
		STPisteKML Piste = *pdata->ListeP.current();
		STWayPointKML Wp;

		char *pnt = Piste.Coordinates;
		char *coordinates;
		int len = 0;

		for (; *pnt != '\0'; pnt++)
		{	
			if (!isdigit(*pnt)) continue;

			coordinates = pnt;
			while (true)
			{
				len++;
				if (*pnt == ' ')
				{
					*pnt = '\0';
					break;
				}
				pnt++;
			}
			memcpy(Wp.Coordinates, coordinates, len);
			sprintf(Wp.Name, "LS_P%d", indexPistePt++);
			pdata->Liste.push_back(Wp);
			len = 0;
		}
	}
}

bool ImporteKML(char *NfKML, _list<Waypoint> &ListeW, char *msg1000)
/********************************************************************/
{
TdataParserKML Data(NfKML);
if(!Data.Buffer)
		{ sprintf(msg1000, "Echec allocation buffer pour %s", NfKML); return -1; }

TParamParse ParamKML;
ParamKML.pUserData = &Data;
ParamKML.HandlerStart = HStartKML;
ParamKML.HandlerEnd = HEndKML;
ParamKML.HandlerChar = HCharKML;

if(!ParseFichierXML(NfKML, 	ParamKML, msg1000))	return false;

// ajoute des waypoints dans la liste depuis les pistes
GetWayPointFromPiste(&Data);

//mise à jour de la liste des waypoints

for(Data.Liste.begin();	!Data.Liste.end();	Data.Liste++)
	{
	STWayPointKML TPst = *Data.Liste.current();
	Waypoint W;
	strncpy(W.Name, TPst.Name, 128);
	strncpy(W.Description, TPst.Description, 128);
	
	if(!GetKMLCoordinates(TPst.Coordinates, W.LatDeg, W.LonDeg, W.elevation)) continue; //ERREUR

	ListeW.push_back(W);
	}


return true;
}


