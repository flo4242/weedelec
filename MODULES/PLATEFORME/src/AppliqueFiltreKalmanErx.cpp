#include "ControlRobot.h" 
// il faut toujours inclure "ControlRobot.h" en premier pour que "erxCom.h" soit d�clar� en premier 
// (sinon incompatibilit� se trouve pour le module 'chrono') 

#include "AppliqueFiltreKalmanErx.h"
#include "drvtraiti.h"


extern TRepereLocal *RepereG;

bool UpdatePosKalmanG = true;

TAppliqueKalman AppliqueKalmanG;


////////// affichage des r�sultats Kalman courants ///////////////
IMTGRIS * ImKalmanG = NULL;
int TimeDebutG;

void AfficheKalman()  // affichage de l'�tat courant, variance courante et la consigne de moteurs courante
{
	// if (!AffichageKalmanG) return;  
	// Cet affichage de Kalman est �tabli dans la m�me callback fonction (le m�me Timer) que Kalman
	// Donc pas besoin de v�rifier si les valeurs sont en train d'�tre modifi�es (jamais le cas) 
	if (ImKalmanG == NULL) return;

	double Ts = double(GetTickCount() - TimeDebutG) / 1000;

	VECTEUR EtatCourant = AppliqueKalmanG.pKalman->GetEtatCourant();
	MATCARREE Covariance = AppliqueKalmanG.pKalman->P;

	VECTEUR EcartType(5);
	EcartType[0] = sqrt(Covariance[0][0]); EcartType[1] = sqrt(Covariance[1][1]);
	EcartType[2] = sqrt(Covariance[2][2]) * (180 / M_PI);
	EcartType[3] = sqrt(Covariance[3][3]) * (180 / M_PI);
	EcartType[4] = sqrt(Covariance[4][4]);

	double CMgauche = AppliqueKalmanG.pKalman->Vg;
	double CMdroite = AppliqueKalmanG.pKalman->Vd;

	VECTEUR Mes = AppliqueKalmanG.pKalman->Obs;

	if (AppliqueKalmanG.pFichierLog)
		SauveLogKalman(AppliqueKalmanG.pFichierLog, Ts, EtatCourant, EcartType, POINTFLT(CMgauche, CMdroite), Mes);

	ImKalmanG->Printf(5, 0, "   Working Time : %d  ", int(Ts));

	ImKalmanG->Printf(25, 0, "   Current state :   ");
	ImKalmanG->Printf(50, 0, "   X (m) : %.2f      Y (m) : %.2f      CAP (deg) : %.2f   ", EtatCourant[0], EtatCourant[1], EtatCourant[2] * (180 / M_PI));
	ImKalmanG->Printf(75, 0, "   CAP offset (deg) : %.2f      K moteur : %.2f    ", EtatCourant[3] * (180 / M_PI), EtatCourant[4]);

	ImKalmanG->Printf(125, 0, "   Current Ecart-type :   ");
	ImKalmanG->Printf(150, 0, "   X (m) : %.2f      Y (m) : %.2f      CAP (deg) : %.2f   ", EcartType[0], EcartType[1], EcartType[2]);
	ImKalmanG->Printf(175, 0, "   CAP offset (deg) : %.2f      K moteur : %.2f    ", EcartType[3], EcartType[4]);

	ImKalmanG->Printf(225, 0, "   Motor commands (m/s):   ");
	ImKalmanG->Printf(250, 0, "   Gauche : %.2f      Droite : %.2f   ", CMgauche, CMdroite);

	ImKalmanG->Printf(300, 0, "   Current Measurement :   ");
	ImKalmanG->Printf(325, 0, "   X (m): %.2f       Y (m) : %.2f      Cap (deg) : %.2f   ", Mes[0], Mes[1], Mes[2] * 180 / M_PI);

	//ImKalmanG->Redessine();

}


void SauveLogKalman(ofstream *pOs, double Ts, VECTEUR EtatCourant, VECTEUR EcartType, POINTFLT CMoteurs, VECTEUR Mes)
// format de log Kalman: 
// Current Kalman:;XX.X (temps);Xmes;Ymes;CapDegmes;ConsigneMoteurG;ConsigneMoteurD;Xcorr;Ycorr;CapdegCorr;OffsetDegCorr;
// KmoteurCorr;SigmaX;SigmaY;SigmaCapDeg;SigmaOffsetDeg;SigmaKmoteur;Latitude brute;Longitude brute
{
	char temp[100];
	
	sprintf(temp, "Current Kalman:\t%.1f\t%f\t%f\t%.3f\t%.3f\t%.3f\t%f\t%f\t%.3f\t", Ts, Mes[0], Mes[1], Mes[2] * 180 / M_PI,
		CMoteurs.x, CMoteurs.y, EtatCourant[0], EtatCourant[1], EtatCourant[2] * 180 / M_PI);
	*pOs << temp;

	sprintf(temp, "%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t", EtatCourant[3] * 180 / M_PI, EtatCourant[4],
		EcartType[0], EcartType[1], EcartType[2], EcartType[3], EcartType[4]);
	*pOs << temp;

	double lat, lon;
	RepereG->MetresVersWGS84(lat, lon, EtatCourant[0], EtatCourant[1]);

	sprintf(temp, "%.16f\t%.16f\0", lat, lon);
	*pOs << temp << endl;
}



auto callback_kalman = new call<bool>([](bool)
// �tabli la fonction callback pour le filtre de Kalman
{
	if (!AppliqueKalmanG.EvolutionMesuresKalman())
		AppliqueKalmanG.StopTimer();
	// get nouvelles mesures, �volution de filtre et mise � jour de position filtr�e

	AfficheKalman();
});


bool TAppliqueKalman::SetTimer(char *msgerr)
{
	if (pKalman == NULL)
	{
		sprintf(msgerr, "Kalman non initialis�");
		return false;
	}

	UpdatePosKalmanG = true;

	int period = pKalman->deltaT * 1000;  // p�riode de timer en milisecondes

	//cr�e le timer pour la commande de suivi de chemin
	Timer_Kalman = new timer<bool>(period, NULL, callback_kalman, true);

	StartTimer();
}

void TAppliqueKalman::StartTimer()
// start timer
{
	if (Timer_Kalman) Timer_Kalman->start();
}

void TAppliqueKalman::StopTimer()
{
	if (Timer_Kalman) Timer_Kalman->stop();
}


void TAppliqueKalman::UpdatePositionKalman(double x, double y, double capdeg)
{
	if (UpdatePosKalmanG)
	{
		PosRobotKalman.x = x;
		PosRobotKalman.y = y;
		PosRobotKalman.CapDeg = capdeg;
	}
}


extern TConsigneMoteurs ConsigneMoteursG;

bool TAppliqueKalman::EvolutionMesuresKalman()
// R�ception de nouvelles mesures des capteurs, charge de la consigne actuelle des moteurs 
// Ensuite, �volution de variables du filtre Kalman
{
	if (pKalman == NULL) return false;
	
	// charge de la consigne des moteurs
	POINTFLT ConsigneMoteurs;
	ConsigneMoteursG.LireConsigneCourante(ConsigneMoteurs.x, ConsigneMoteurs.y);
	pKalman->SetConsigneMoteursActuelle(ConsigneMoteurs);

	// estimation kalman
	pKalman->EtatSuivantNonCorrige();

	// r�ception de nouvelles mesures
	TPositionRobot Pos;
	char msgerr[1000];
	if (!GetPositionRobotBrute(Pos, GPSExterne, *RepereG, msgerr))
		return false;

	VECTEUR obs(3);

	obs[0] = Pos.x; obs[1] = Pos.y; obs[2] = Pos.CapDeg * (M_PI / 180);
	pKalman->Obs = obs;

	pKalman->EtatSuivantCorrige(); // corrige kalman

	// mise � jour des donn�es actuelles
	VECTEUR Etat = pKalman->GetEtatCourant();

	double capdeg = Etat[2] * (180 / M_PI);  // r�cup�ration du cap actuel

	UpdatePositionKalman(Etat[0], Etat[1], capdeg);

	return true;
}

void TAppliqueKalman::GetPositionRobotKalman(TPositionRobot &Pos)
{
	UpdatePosKalmanG = false; // d�validation de mise � jour des donn�es actuelles si la lecture est en cours
	Pos = PosRobotKalman;
	UpdatePosKalmanG = true;
}
