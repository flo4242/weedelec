
#include "CommandeCinetique.h"

#include "ExportModulePlateforme.h"
#include "Plateforme.h"


#define ENTRAXE_ROBOT   2.20 // entraxe: 2.2m
#define RAYON_RATTRAPAGE  2.0  // rayon de rattrapage en m�tres
#define THETA_CST  45.0*(M_PI/180)  // constante pour la fonction exponentielle : v(err_theta) = Vmax * exp{-err_theta / THETA_CST}
#define GAIN_ORIENT  0.5  // gain de commande pour l'orientation


extern DRVTRAITIM *pDriverG;

extern TParamRobot ParamG;
extern TCommandeCinetique TCommandeG;



// call back fonction

void CALLBACK CALLBACKCommandeSuiviTrajectoire(HWND hwnd, UINT uMsg, UINT timerId, DWORD dwTime)
{
	TCommandeG.AppliqueCommande();
}


// lance la commande
bool TCommandeCinetique::StartCommande(_list<POINTFLT> &liste, char* msgerr)
{
	BalayeListe(liste)
	{
		POINTFLT *P = liste.current();
		TrajDesire.AjoutePointPassage(P->x, P->y);
	}
	return DoStartCommande(msgerr);
}
bool TCommandeCinetique::StartCommande(double distance, char* msgerr)
{
	//on part du point actuel avec le cap actuel
	POINTFLT D(-sin(OrientActuelle), cos(OrientActuelle));
	POINTFLT cible = PtActuel + D * distance;
	return StartCommande(cible, msgerr);

}
bool TCommandeCinetique::StartCommande(POINTFLT cible, char* msgerr)
{
	TrajDesire.AjoutePointPassage(PtActuel.x, PtActuel.y);
	TrajDesire.AjoutePointPassage(cible.x, cible.y);
	return DoStartCommande(msgerr);
}


bool TCommandeCinetique::DoStartCommande(char* msgerr)
{
	if (!Fk || !pR)
	// v�rifie si l'initialisation est d�j� faite
	{
		sprintf(msgerr, "Pas encore initialis� la structure pour la commande (pointeur filtre Kalman : %s\tpointeur rep�re local : %s)",
			(Fk ? "Oui" : "NULL"), (pR ? "Oui" : "NULL"));
		return false;
	}

	
	if (TrajDesire.NbsPts < 2 || TrajDesire.NbsPts - index < 2)
	// au moins deux points restant dans la trajectoire pour la suivi de chemin (au moins deux points pour d�finir un chemin)
	{
		sprintf(msgerr, "Pas assez de points d�finis dans la trajectoire");
		return false;
	}

	pTimerCom = SetTimer(NULL, 0, dT, (TIMERPROC)&CALLBACKCommandeSuiviTrajectoire);

	if(pTimerCom == NULL)
	{
		sprintf(msgerr, "Erreur cr�ation timer");
		return false;
	}
	return true;
}


// v�rification d'atteinte de la position d�sir�e
bool VerificationErreur(POINTFLT Ptprecedent, POINTFLT Ptcurant, POINTFLT Ptdesire)
{
	return ((Ptcurant - Ptprecedent).Norme2() >= (Ptdesire - Ptprecedent).Norme2());
	//permet au robot d'aller jusqu'au bout (si pas d'erreur de cap et pas de rattrapage)
}


// commande cin�matique sur le suivi de l'orientation
double GetVitesseLineaireSelonErreurCap(double ErrCapRad, double vitessenominale)
// Calculer la vitesse d'avanc�e � partir de l'erreur de l'orientation d�sir�e ()
// Fonction potentielle : y(theta) = V*exp{-theta/theta_c} ;
{
	return vitessenominale * exp(-ErrCapRad / THETA_CST);
}

POINTFLT TCommandeCinetique::GetConsignesMoteurs(double CapRad, double vitessenominale)
// Commande cap : 1/E*(vD - vG) = K*(theta_desire - theta_actuel)
// Commande vitesse d'avancement : V = 1/2*(vG + vD)  -> constante(pourrait aussi r�gler en fonction de l'erreur de suivi de cap)
// Consigne de vitesse : vG = V - E/2*K*(theta_desire - theta_actuel), vD = V + E/2*K*(theta_desire - theta_actuel)
{
	double ErrOrient = CapRad - OrientActuelle;

	if (abs(ErrOrient + 2*M_PI) < abs(ErrOrient)) //v�rification de la zone de discontinuit�
		ErrOrient += 2 * M_PI;
	else if (abs(ErrOrient - 2 * M_PI) < abs(ErrOrient))
		ErrOrient -= 2 * M_PI;

	double consignV_theta = ENTRAXE_ROBOT / 2 * GAIN_ORIENT*ErrOrient;

	double V = GetVitesseLineaireSelonErreurCap(abs(ErrOrient), vitessenominale);

	double vG, vD;

	vG = V - consignV_theta;
	vD = V + consignV_theta;

	return POINTFLT(vG, vD);
}


void TCommandeCinetique::SetPointActuel(double x, double y, double orient)
// Set point actuel dans le rep�re local qui a initialis� par pointeur pR
{
	PtActuel.x = x; PtActuel.y = y; OrientActuelle = orient;
}


// filtre de Kalman en temps r�el
void TCommandeCinetique::FiltreKalmanTempsReel(double x, double y, double orient)
// x,y les coordonn�es dans le rep�re m�trique et orient en radian
{
	Fk->EtatSuivantNonCorrige();

	VECTEUR obs(3);
	
	obs[0] = x; obs[1] = y; obs[2] = orient;
	Fk->Obs = obs;

	Fk->EtatSuivantCorrige(); // corrige

	// mise � jour des donn�es
	VECTEUR Etat = Fk->GetEtatCourant();
	
	double v, theta;
	x = Etat[0]; y = Etat[1]; v = Etat[2]; theta = Etat[3]; 

	// correction du d�calage entre l'axe du robot et la position d'installation de GPS
	x -= DIST_DECALAGE * sin(theta);
	y += DIST_DECALAGE * cos(theta);

	SetPointActuel(x, y, theta);
}



// applique la commande de suivi de chemin
//appel� par le callback du timer
void TCommandeCinetique::AppliqueCommande()
{
	char msgerr[1000];

	// mettre � jour le point de passage d�sir� � l'instant
	POINTFLT Pchemin1 = TrajDesire.ListePtsPassage[index].obj;
	POINTFLT Pchemin2 = TrajDesire.ListePtsPassage[index + 1].obj;

	// r�cup�ration de nouvelles mesures
	double lat_mesure, lon_mesure, orient_mesure, vitesse_mesure;

	if (!GetMesureEnTempsReel(lat_mesure, lon_mesure, orient_mesure, vitesse_mesure, msgerr))  // recevoir nouvelles mesures
	{
		StopCommande();
		pDriverG->Message("Erreur lecture capteur : %s", msgerr);
		return;
	}

	double x_mesure, y_mesure;
	pR->WGS84VersMetres(lat_mesure, lon_mesure, x_mesure, y_mesure);

	// applique le filtre Kalman pour corriger les mesures courantes :
	FiltreKalmanTempsReel(x_mesure, y_mesure, orient_mesure* (M_PI / 180));

	// calculer la position et l'orientation du point d�sir� actuel
	CalculCapDesire(Pchemin1, Pchemin2, PtActuel); 

	double vitesseG, vitesseD;
	POINTFLT V = GetConsignesMoteurs(OrientDesiree, VitesseNominale);
	vitesseG = V.x; vitesseD = V.y;


	SendSpeedRobot(vitesseG, vitesseD); // envoyer la commande au robot

	// v�rification d'atteinte au dernier point de la trajectoire
	if (VerificationErreur(Pchemin1, PtActuel, Pchemin2))
	{
		index++; //passage au segment suivant
		if (index >= TrajDesire.NbsPts - 1)	
			StopCommande(); //arr�t du timer
			
	}

}


void TCommandeCinetique::CalculCapDesire(POINTFLT P1, POINTFLT P2, POINTFLT PC)
// calcule le cap d�sir� � partir du segment de chemin actuel(d�fini par P1, P2) et du point actuel
{

	POINTFLT U = (P2 - P1) / (P2 - P1).Norme();
	POINTFLT P1C = PC - P1;
	POINTFLT PH = P1 + U * (P1C | U);

	double dist = (PC - PH).Norme();

	POINTFLT PCible;

	double longueur = RAYON_RATTRAPAGE * RAYON_RATTRAPAGE - dist * dist;

	if (longueur <= 0)
		PCible = PH;
	else
		PCible = PH + U * sqrt(longueur);

	OrientDesiree = atan2(PC.x - PCible.x, PCible.y - PC.y);
}





