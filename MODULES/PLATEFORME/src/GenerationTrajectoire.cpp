
#include "GenerationTrajectoire.h"

void Trajectoire::AjoutePointPassage(double xd, double yd)
{
	POINTFLT pt(xd, yd);  
	ListePtsPassage.push_back(pt); 
	NbsPts++;

	if (NbsPts == 1) // d�finit abscisse = 0 pour le premier point
		AbscissesPtsPassage.push_back(0);
	else // pour les points suivants, on calcule abscisse par rapport au premier point
	{
		POINTFLT P1, P2;
		P1 = ListePtsPassage.lastCell->obj; // point actuel
		P2 = ListePtsPassage.lastCell->previous->obj; // point dernier

		double dis = (P2 - P1).Norme(); // distance entre deux points

		double abscissePt_dernier = AbscissesPtsPassage.lastCell->obj;
		double abscissePt_actuel = abscissePt_dernier + dis; // accumule les longueurs pour trouver l'abscisse actuelle
	
		AbscissesPtsPassage.push_back(abscissePt_actuel);

		LongueurTotale += dis;
	}
}


/*
double GetTimeNow()
{
	SYSTEMTIME time;
	GetSystemTime(&time); // obtient temps de syst�me actuel

	DWORD timesec = time.wHour * 3600 + time.wMinute * 60 + time.wSecond;
	double timenow = double(timesec) + double(time.wMilliseconds) / 1000; // calcule le temps en secondes

	return timenow;
}

POINTFLT Trajectoire::GetPointDesireActuel()
{
	double timenow = GetTimeNow();
	double deltaT = timenow - startTime;

	double abscisseCurant = vitesseLineaire * deltaT; // abscisse de point d�sir� curant

	if (abscisseCurant >= LongueurTotale) // trajectoire finie, on retourne le dernier point comme le point d�sir�
	{
		double NbElem = ListePtsPassage.nbElem;
		double xd = ListePtsPassage[NbElem - 1].obj.x;
		double yd = ListePtsPassage[NbElem - 1].obj.y;

		POINTFLT PtDesire(xd, yd);
		return PtDesire;
	}

	int n_segment = 0; // num�ro de segment de la trajectoire (0, 1, ..., NbPtsPassage - 1)

	for (int i = 0; i < AbscissesPtsPassage.nbElem - 1; i++) 
	// balayer la liste des abscisses de point passage pour retrouver le segment o� on est
	{
		double abscisse_PtSuivant = AbscissesPtsPassage[i].next->obj;

		if (abscisseCurant < abscisse_PtSuivant)
			break;
		n_segment++;
	}

	// trouver les deux points qui sont � l'extr�mit� de segment courant
	POINTFLT P1 = ListePtsPassage[n_segment].obj;
	POINTFLT P2 = ListePtsPassage[n_segment].next->obj;

	POINTFLT D = (P2 - P1) / (P2 - P1).Norme(); // vecteur unitaire suivant la direction P2 - P1

	double abscisse_PtDernier = AbscissesPtsPassage[n_segment].obj; // abscisse de dernier point de passage
	double deltaS = abscisseCurant - abscisse_PtDernier; // �chelle

	D *= deltaS;
	POINTFLT PtDesire = P1 + D; // point d�sir�

	return PtDesire;
}*/
