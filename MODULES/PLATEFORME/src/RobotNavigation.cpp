
#include "RobotNavigation.h"

#include "ExportModulePlateforme.h"
#include "Plateforme.h"

#include "KML.h"


extern TParamRobot ParamG;
extern TCommandeCinetique TCommandeG;


bool TModulePlateforme::InitRobotNavigation(char* msgerr)
{
	double LatDeg = 0, LonDeg = 0, CapDeg = 0, Vitesse = 0;
	double XLocal = 0, YLocal = 0;

	if (!GetMesureEnTempsReel(LatDeg, LonDeg, CapDeg, Vitesse, msgerr)) // re�oit les mesures actuelles
		return false;

	// initialise le rep�re local par le point actuel du robot
	TCommandeG.pR = new TRepereLocal(LatDeg);

	TCommandeG.pR->WGS84VersMetres(LatDeg, LonDeg, XLocal, YLocal);

	// initialisation de filtre Kalman
	double incT = ParamG.incT_Commande * 1.0 / 1000;  //p�riode filtre de Kalman en secondes

	TCommandeG.Fk = new TFiltreKalman(DIM_KALMAN);

	// charge des param�tres pour le filtre de Kalman
	TCommandeG.Fk->InitParametres(incT, ParamG.sigmax0, ParamG.sigmatheta0, ParamG.sigmav0, ParamG.sigmadtheta0, ParamG.sigmax, ParamG.sigmatheta,
		ParamG.sigmav, ParamG.sigmadtheta, ParamG.sigmaObsx, ParamG.sigmaObstheta);

	TCommandeG.dT = ParamG.incT_Commande;

	// correction d�calage de la position GPS
	double theta = CapDeg * M_PI / 180;
	XLocal -= DIST_DECALAGE * sin(theta);
	YLocal += DIST_DECALAGE * cos(theta);


	// initialise la position et l'orientation du point d�part
	TCommandeG.Fk->ChargePosInit(XLocal, YLocal, theta, Vitesse, 0);
	TCommandeG.SetPointActuel(XLocal, YLocal, theta);

	TCommandeG.index = 0; // initialise index du segment de trajectoire
	TCommandeG.VitesseNominale = ParamG.VitesseMaxLineaire; // initialise la vitesse nominale d'avancement

	return true;
}


bool InitRobotNavigation2(char* NfKML, _list<Waypoint>& ListeWp, char* msgerr)
{
	// charge fichier KML contenant des points d�sir�s (stock�s par liste de waypoints)
	_list<Waypoint> ListeW;

	if (!ImporteKML(NfKML, ListeW, msgerr)) return false;


	double LatM = 0; // trouver le rep�re local par la moyenne de latitude des points d�sir�s

	BalayeListe(ListeW)
	{
		Waypoint Wp = *ListeW.current();
		LatM += Wp.LatDeg;
	}
	LatM /= ListeW.nbElem;

	TRepereLocal TRp(LatM); // rep�re local (variable locale pour calculer la trajectoire)

	Trajectoire Traj; // initialiser la trajectoire d�sir�e

	BalayeListe(ListeW)  // construire la trajectoire d�sir�e � partir des points d�sir�s
	{
		double x = 0, y = 0;
		double lat, lon;
		Waypoint wp = *ListeW.current();
		lat = wp.LatDeg;
		lon = wp.LonDeg;

		TRp.WGS84VersMetres(lat, lon, x, y);
		Traj.AjoutePointPassage(x, y);
	}

	// initialisation de filtre Kalman
	TFiltreKalman Fk(DIM_KALMAN);
	double incT = ParamG.incT_Commande * 1.0 / 1000;
	//p�riode filtre de Kalman en secondes

	// charge des param�tres pour le filtre de Kalman
	Fk.InitParametres(incT, ParamG.sigmax0, ParamG.sigmatheta0, ParamG.sigmav0, ParamG.sigmadtheta0, ParamG.sigmax, ParamG.sigmatheta,
		ParamG.sigmav, ParamG.sigmadtheta, ParamG.sigmaObsx, ParamG.sigmaObstheta);

	// initialisation de structure de la commande 
	//(qui va contenir les structures de rep�re local, filtre Kalman et trajectoire d�sir�e)
	TCommandeG.InitParam(Fk, Traj, LatM, ParamG.incT_Commande);

	ListeWp = ListeW;
	return true;
}

bool UpdateRobotPositionForNavigation(char* msgerr)
{
	double Lat = 0, Lon = 0, Orient = 0, Vitesse = 0;
	double x_local = 0, y_local = 0;

	if (!GetMesureEnTempsReel(Lat, Lon, Orient, Vitesse, msgerr)) // re�oit les mesures actuelles
		return false;

	TCommandeG.pR->WGS84VersMetres(Lat, Lon, x_local, y_local);

	// correction d�calage de la position GPS
	double theta = Orient * M_PI / 180;
	x_local -= DIST_DECALAGE * sin(theta);
	y_local += DIST_DECALAGE * cos(theta); 
	

	// initialise les coordonn�es de d�part
	TCommandeG.Fk->ChargePosInit(x_local, y_local, theta, Vitesse, 0);
	TCommandeG.SetPointActuel(x_local, y_local, theta);
	
	/*int index_listpt = TCommandeG.index;
	TCommandeG.PtPassageDesire = TCommandeG.TrajDesire.ListePtsPassage[index_listpt].obj;*/

	return true;
}
