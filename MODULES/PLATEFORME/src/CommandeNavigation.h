#ifndef __COMMANDENAVIGATION_H__
#define __COMMANDENAVIGATION_H__

#include <Windows.h>
#include <ppltasks.h> 
#include <agents.h> // pour timer

#include "ligne.h"
#include "Plateforme.h"


using namespace Concurrency;

#define UNITE_VITESSE  0.05   // unit� de contr�le manuel de vitesse (incr�ment de vitesse 0.05m/s pour chaque action)
#define SEUIL_VITESSE  0.4    // vitesse max (contraint�e par le robot Erx)


struct TSubStepLocal
// d�finition de sub step dans le rep�re local de sol (m�trique), les coordon�es fournies par Erx
{
	POINTFLT Debut;  // coordonn�es du point d�but d'un mouvement (dans le rep�re local de sol, fourni par Erx)
	double DistanceMax;
};


struct TCommandeNavigation
// structure de base qui g�re le timer pour la commande de navigation du robot
{
	timer<bool> *Timer_Commande;

	int PeriodeCommande; // en milisecondes
	bool CommandeManuelle;

	TCommandeNavigation(int periode_commande, bool commande_manuelle) : PeriodeCommande(periode_commande), 
		CommandeManuelle(commande_manuelle) { Timer_Commande = NULL; }

	~TCommandeNavigation() { if (Timer_Commande) delete Timer_Commande; }


	virtual bool DoStartCommande(char* msgerr) = 0;

	void SetTimer();
	void StartTimer();
	void StopTimer();
};


struct TCommandeManuelle : public TCommandeNavigation
// classe d�riv�e pour la commande manuelle
{

	double VitesseNominale;
	double VitesseDifferentielle; // positive dans la sens anti-horaire
	bool SetValeurValide;


	TCommandeManuelle(int periode_commande) : VitesseNominale(0), VitesseDifferentielle(0), 
		TCommandeNavigation(periode_commande, true) { SetValeurValide = true; };
	

	void StopVitesse() { VitesseNominale = 0; VitesseDifferentielle = 0; }

	void VitesseNominaleAdd(); 
	void VitesseNominaleSub(); 
	void VitesseDiffAdd(); 
	void VitesseDiffSub();
	

	virtual bool DoStartCommande(char *msgerr);

	POINTFLT GetVitesseCommandeManuelle();
};


struct TCommandeAutomatique : public TCommandeNavigation
// classe d�riv�e pour la commande automatique
{
	_list<POINTFLT> TrajDesire;

	TPositionRobot PosActuelle;
	TPositionRobot PosDesiree;

	TSubStepLocal * Substep;
	pcallbackStep callbackRetour;

	int indexSegmentTraj;
	double vitesseNominale;


	TCommandeAutomatique(int periode_commande, pcallbackStep cb = NULL) : indexSegmentTraj(0), vitesseNominale(0),
		Substep(NULL) , callbackRetour(cb),
		TCommandeNavigation(periode_commande, false) { };
	// constructeur


	void InitVitesse(double vitesse) { vitesseNominale = vitesse; };

	
	virtual bool DoStartCommande(char* msgerr);

	bool RestartCommande(char* msgerr);


	// suivi de chemin 
	POINTFLT CalculConsignesMoteurs();

	void CalculCapDesire(POINTFLT P1, POINTFLT P2, POINTFLT PC);
};
 


bool TestArret(POINTFLT Ptprecedent, POINTFLT Ptcourant, double DistanceMax);

#endif