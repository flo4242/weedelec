
#include "CommandeHT.h"

#include <stdio.h>
#include <stdlib.h>


#define TIMOUT_PORTCOM	    5000   // timout de 5s pour la communication port s�rie


void ConvertDecToHex(int NumDecIn, char* ChaineHexOut, int NbChiffre)
// conversion de d�cimal en hexad�cimal, rempliant le chiffre 0 fournissant le nombre de chiffres de sortie
// par exemple: 1000 (d�c) -> 03E8 (hex) avec NbChiffre = 4
// attention: la taille de chaine de caract�res doit �tre au moins NbChiffre + 1, une caract�re '\0' � la fin
{
	int temp, r;
	int i = 0, j;

	char *hex = new char[NbChiffre+1];
	temp = NumDecIn;

	while (temp != 0)  // les r�sultats de conversion en hex sont stock�s dans hex avec l'ordre inverse
	{
		if (i >= NbChiffre)  // i max: NbChiffre - 1 
			break;
		
		r = temp % 16;

		if (r < 10)
			hex[i++] = r + 48;
		else
			hex[i++] = r + 55;

		temp = temp / 16;
	}

	int index = NbChiffre - 1;
	for (j = index; j >= 0; j--)
	{
		if(index - j >= i) // on remplit "0" pour les chiffres manquants
			ChaineHexOut[j] = '0';
		else
			ChaineHexOut[j] = hex[index - j];
	}
	ChaineHexOut[NbChiffre] = '\0';
}

void ConvertHexToDec(char* ChaineHexIn, int& NumDecOut)
// conversion de hexad�cimal en d�cimal
{
	int length = strlen(ChaineHexIn);
	int r;
	int dec = 0;

	for (int i = 0; ChaineHexIn[i] != '\0'; i++)
	{
		length--;

		if (ChaineHexIn[i] >= '0' && ChaineHexIn[i] <= '9')
			r = ChaineHexIn[i] - 48;

		else if (ChaineHexIn[i] >= 'a' && ChaineHexIn[i] <= 'f')
			r = ChaineHexIn[i] - 87;

		else if (ChaineHexIn[i] >= 'A' && ChaineHexIn[i] <= 'F')
			r = ChaineHexIn[i] - 55;

		dec += r * pow(16, length);
	}
	NumDecOut = dec;
}

void GetCharArrayValueHex(char* chaineIn, char* chaineOut)
// extrait de chaine de caract�res pour les valeurs hex � partir du message retour
// e.g. Input:'<R03E8>', Ouput: '03E8' 
{
	char *temp = chaineIn;

	for (int i = 0; i < strlen(chaineIn); i++)
	{
		if (chaineIn[i] == '<')
		{
			i += 2;
			temp += i;
		}

		if (chaineIn[i] == '>')
		{
			chaineIn[i] = '\0';
			break;
		}
	}

	strcpy(chaineOut, temp);
}

bool VerifPortCOM(PortComHT* ComHT, char *msgerr)
{
	if (ComHT == NULL)
	{
		sprintf(msgerr, "Port COM non initialis�");
		return false;
	}

	if (!ComHT->isConnected(msgerr)) return false;
	// lecture de l'erreur si l'ouverture de port COM est �chou�e

	return true;
}


bool CommandeHT::InitCommandeHT(TParamHT paramHT, char *msgerr)
{
	if (paramHT.PortCOM == NULL)
	{
		sprintf(msgerr, "Param�tres non initialis�s!");
		return false;
	}

	if (ComHT != NULL)  // on reinitialise le port s�rie
	{
		delete ComHT; ComHT = NULL;
	}

	// ouverture de port s�rie
	ComHT = new PortComHT(paramHT.PortCOM);

	if (!WriteParamHT(paramHT, msgerr))  // l'�criture des param�tres valid�s sur la carte
		return false;

	return true;
}

bool CommandeHT::WriteParamHT(TParamHT paramHT, char *msgerr)
// l'�criture � la carte pour configurer les paramt�res, en v�rifiant la coh�rence des valeurs saisies
{
	if (!VerifPortCOM(ComHT, msgerr)) return false;

	char msgParamInvalid[600] = "";
	char temp[200];

	// v�rification des contraintes de mat�riel :
	double dutty_cycle = paramHT.DwellTime * 1E-6 * paramHT.Frequence;
	double duration_sequence = double(paramHT.NbCycles) / double(paramHT.Frequence);

	bool ParamInvalide = false;
	if (paramHT.DwellTime > DWELL_TIME_LIMIT)  // v�rifie si la dur�e de charge de la bobine d�passe la limite
	{
		sprintf(temp, "Dwell_time(%d) d�passe la limite(%d)\n", paramHT.DwellTime, DWELL_TIME_LIMIT);
		strcat(msgParamInvalid, temp);
		ParamInvalide = true;
	}
	if (dutty_cycle > DUTTY_CYCLE) // v�rifie si le dutty cycle d�passe la limite
	{
		sprintf(temp, "Dutty_cycle(%.2f) d�passe la limite(%.2f)\n", dutty_cycle, DUTTY_CYCLE);
		strcat(msgParamInvalid, temp);
		ParamInvalide = true;
	}
	if (duration_sequence > DURATION_SEQUENCE_LIMIT) // v�rifie si la dur�e de s�quence d�passe la limite
	{
		sprintf(temp, "Dur�e_s�quence(%.2f) d�passe la limite(%.2f)\n", duration_sequence, DURATION_SEQUENCE_LIMIT);
		strcat(msgParamInvalid, temp);
		ParamInvalide = true;
	}

	if (ParamInvalide)
	{
		sprintf(msgerr, "Param�tres non valides :/n%s", msgParamInvalid);
		return false;
	}

	// l'�criture de param�res par le port COM:

	char msgcmd[100]; // message de commande envoy�
	char msgret[100]; // message re�u
	char buff[5];
	
	// configuration des param�tres
	ConvertDecToHex(paramHT.DwellTime, buff, 4); // conversion de d�cimal en hexad�cimal
	sprintf(msgcmd, "<W%s%s>", ADDRESS_DWELL_TIME, buff);
	if (!ConfigurationParam(ComHT, msgcmd, msgerr))
	{  strcat(msgerr, " (Dwell Time)");	return false; }

	ConvertDecToHex(paramHT.Frequence, buff, 4);
	sprintf(msgcmd, "<W%s%s>", ADDRESS_FREQ, buff);
	if (!ConfigurationParam(ComHT, msgcmd, msgerr)) 
	{  strcat(msgerr, " (Fr�quence)");	return false; }

	ConvertDecToHex(paramHT.NbCycles, buff, 4);
	sprintf(msgcmd, "<W%s%s>", ADDRESS_NBCYCLE, buff);
	if (!ConfigurationParam(ComHT, msgcmd, msgerr))
	{  strcat(msgerr, " (NbCycle)");	return false; }

	ConvertDecToHex(paramHT.CurrentLimit, buff, 4);
	sprintf(msgcmd, "<W%s%s>", ADDRESS_CURRLIMIT, buff);
	if (!ConfigurationParam(ComHT, msgcmd, msgerr))
	{  strcat(msgerr, " (Current Limit)");	return false; }

	return true;
}


bool CommandeHT::ReadParamHT(TParamHT& paramHT, char *msgerr)
{
	if (!VerifPortCOM(ComHT, msgerr)) return false;

	char msgcmd[100];
	char msgret[100];
	char value[10];

	sprintf(msgcmd, "<R%s>", ADDRESS_DWELL_TIME);
	if (!EmissionCommande(ComHT, msgcmd, msgret, LENGTH_MSGRET_READ))
	{ sprintf(msgerr, "Erreur lecture param: %s", msgret);  return false; }
	GetCharArrayValueHex(msgret, value);
	ConvertHexToDec(value, paramHT.DwellTime);

	sprintf(msgcmd, "<R%s>", ADDRESS_FREQ);
	if (!EmissionCommande(ComHT, msgcmd, msgret, LENGTH_MSGRET_READ))
	{ sprintf(msgerr, "Erreur lecture param: %s", msgret);  return false; }
	GetCharArrayValueHex(msgret, value);
	ConvertHexToDec(value, paramHT.Frequence);

	sprintf(msgcmd, "<R%s>", ADDRESS_NBCYCLE);
	if (!EmissionCommande(ComHT, msgcmd, msgret, LENGTH_MSGRET_READ))
	{ sprintf(msgerr, "Erreur lecture param: %s", msgret);  return false; }
	GetCharArrayValueHex(msgret, value);
	ConvertHexToDec(value, paramHT.NbCycles);

	sprintf(msgcmd, "<R%s>", ADDRESS_CURRLIMIT);
	if (!EmissionCommande(ComHT, msgcmd, msgret, LENGTH_MSGRET_READ))
	{ sprintf(msgerr, "Erreur lecture param: %s", msgret);  return false; }
	GetCharArrayValueHex(msgret, value);
	ConvertHexToDec(value, paramHT.CurrentLimit);

	return true;
}


bool CommandeHT::ReadTemperature(double& temperature, char *msgerr)
{
	if (!VerifPortCOM(ComHT, msgerr)) return false;

	int TemperRet;

	if (!ReadBoardStatus(ComHT, "<T>", TemperRet, msgerr))
		return false;

	temperature = double(TemperRet) / 10;
	return true;
}

bool CommandeHT::ReadVoltageBattery(double& volts, char *msgerr)
// 
{
	if (!VerifPortCOM(ComHT, msgerr)) return false;

	int voltage;

	if (!ReadBoardStatus(ComHT, "<B>", voltage, msgerr))
		return false;
	
	volts = double(voltage) / 10;
	return true;
}


bool CommandeHT::StartSequence(int& CurrentMax, int& SparkVoltage, int& SparkDuration, char *msgerr)
{
	if (!VerifPortCOM(ComHT, msgerr)) return false;

	char msgret[100];
	if (!EmissionCommande(ComHT, "<S00>", msgret, LENGTH_MSGRET_SEQUENCE))
	{
		sprintf(msgerr, "Erreur lancement s�quence : %s", msgret);
		return false;
	}

	char courantMax[5];    char *tempCourant = msgret + 4;
	char durationSpark[5]; char *tempDuration = tempCourant + 4;
	char voltageSpark[5];  char *tempVoltage = tempDuration + 4;

	for (int i = 0; i < 4; i++)
	{
		courantMax[i] = *(tempCourant + i);
		durationSpark[i] = *(tempDuration + i);
		voltageSpark[i] = *(tempVoltage + i);
	}
	courantMax[4] = '\0'; durationSpark[4] = '\0'; voltageSpark[4] = '\0';

	ConvertHexToDec(courantMax, CurrentMax);
	ConvertHexToDec(durationSpark, SparkDuration);
	ConvertHexToDec(voltageSpark, SparkVoltage);

	return true;
}


bool EmissionCommande(PortComHT* ComHT, char* MsgCmd, char* MsgRet, int tailleMsgRet)
// ComHT : Handle de port COM, 
// MsgCmd : message envoy�, 
// MsgRet : message de retour
// tailleMsgRet : taille de message retour attendu
{
	ComHT->WriteMessage(MsgCmd, strlen(MsgCmd));
	int TimeSendMsg = GetTickCount();
	Sleep(100);
	while (true)
	{
		int ret = ComHT->ReadMessage(MsgRet, tailleMsgRet);

		if (ret == tailleMsgRet) break;

		if (GetTickCount() - TimeSendMsg > TIMOUT_PORTCOM)
		{
			sprintf(MsgRet, "Timout port communication");
			return false;
		}
	}
	return true;
}

bool ConfigurationParam(PortComHT* ComHT, char* msgcmd, char* msgerr)
{
	char msgret[1000];
	if (!EmissionCommande(ComHT, msgcmd, msgret, LENGTH_MSGRET_CONFIG))
	{
		strcpy(msgerr, msgret);
		return false;
	}
	char acquittement = msgret[1];

	if (acquittement != 'K')
	{
		sprintf(msgerr, "Configuration param�tres �chou�e");
		return false;
	}
	return true;
}

bool ReadBoardStatus(PortComHT* ComHT, char* msgcmd, int& valdec, char* msgerr)
{
	char msgret[100];
	char value[10]; // valeur hex en caract�res

	if (!EmissionCommande(ComHT, msgcmd, msgret, LENGTH_MSGRET_READ))
	{
		sprintf(msgerr, "Erreur lecture status: %s", msgret);
		return false;
	}

	GetCharArrayValueHex(msgret, value);
	ConvertHexToDec(value, valdec);

	return true;
}