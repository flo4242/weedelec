#ifndef __FILTREKALMANROBOTERX_H_
#define __FILTREKALMANROBOTERX_H_


#include "Kalman.h"
#include "ligne.h"


#ifndef DECALAGE_Y_GPS_ERX
#define DECALAGE_Y_GPS_ERX  -1.2468     // d�calage Y du GPS, en m�tres
#endif

#ifndef DECALAGE_Y_GPS_EXT
#define DECALAGE_Y_GPS_EXT  -1.08     // d�calage Y du GPS externe (Emlid) par d�faut, en m�tres
#endif


#ifndef DIM_KALMAN
#define DIM_KALMAN	5
#endif 


class TFiltreKalman : public SYSTEMEKALMAN
{

protected:

	//matrices de calcul
	virtual MATCARREE Fk();	//retourne la matrice dXk+1 / dXk
	virtual MATRICE Hk();		//retourne la matrice d Observations/ d Xk
	virtual MATCARREE Qk();	//retourne la covariance instantan_e
	virtual MATCARREE Rk(); //retourne la covariance sur observations
	
	//fonctions de calcul
	virtual void EvolutionEtat(void);
	virtual VECTEUR PredictionObservations();
	virtual VECTEUR MesureObservations();
	virtual void CorrigeEtat(VECTEUR deltaX);
	virtual VECTEUR EtatCourant();
	
	virtual void Init(double dTsec);


public:

	// Etat: x, y, v, theta, deltaH

	double x, y; // m�tres
	double theta; // theta_erx (cap du capteur Erx), en radian
	double theta_offset; // offset cap, en radian
	double k_moteur; // coefficient de moteurs

	double deltaT; // dt: p�riode de filtre (en secondes)

	// input consigne moteurs:
	double Vg;  // vitesse de roue gauche  (m/s)
	double Vd;  // vitesse de roue droite  (m/s)

	//membres de calcul rajout�s:

	MATCARREE Q0;
	MATCARREE FF;     //seule une petite partie est variable
	MATRICE HH;	
	MATRICE RR;	
	VECTEUR Obs;	//nouvelle observation

	TFiltreKalman(double dTsec) : SYSTEMEKALMAN(DIM_KALMAN) { Init(dTsec); };

	void ChargePosInit(double x0, double y0, double thetarad0, double theta_offsetrad0, double k_moteur0);

	void InitParametres(double SigmaConsigneMoteurs, double SigmaObsPos, double SigmaObsCapDeg, double SigmaPos0, double SigmaCapDeg0,
		double SigmaOffsetDeg, double SigmaCoeffMoteur, double SigmaOffsetDeg0, double SigmaCoeffMoteur0);

	VECTEUR GetEtatCourant();

	void SetConsigneMoteursActuelle(POINTFLT Consigne);

	void GetParametresKalman(MATRICE& MRetour);
};



#endif // !__FILTREKALMANROBOTERX_H_