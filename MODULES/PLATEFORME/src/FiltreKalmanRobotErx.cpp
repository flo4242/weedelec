#include "FiltreKalmanRobotErx.h"

#ifndef ENTRAXE_ROBOT
#define ENTRAXE_ROBOT   2.112528 // entraxe en m�tres
#endif

// les param�tres par d�faut
// incertitudes de mesure
static double sigma_obs_pos = 0.04;  // m�tre, incertitude sur la mesure de la position GPS
static double sigma_obs_cap = 0.5;  // degr�s (incertitude sur la mesure du cap)

// incertitudes initiales
static double sigma_pos0 = 2; // incertitude sur la position initiale, 2 m�tres
static double sigma_cap0 = 2; // incertitude sur le cap initial, 2�
static double sigma_off0 = 2; // incertitude sur le cap offset initial, 2�
static double sigma_k0 = 0.5; // incertitude sur le coefficient moteurs

// incertitudes instantan�es
static double sigma_consigne_moteur = 0.04;
static double sigma_off = 0;
static double sigma_k = 0;



MATCARREE TFiltreKalman::Qk()  { return Q0; }	//retourne la covariance instantan_e
MATCARREE TFiltreKalman::Rk()  { return RR; }		//retourne la matrice d'observations



void TFiltreKalman::Init(double dTsec)
{
	dim = 5;

	x = 0, y = 0;
	theta = 0; 
	theta_offset = 0;
	k_moteur = 1;

	Vg = 0; Vd = 0;

	deltaT = dTsec;	//secondes


	// matrice covariance d'erreur 
	P = MATCARREE(dim);
	P.Constante(0);
	
	// covariance d'estimation (initialisation) 
	P[0][0] = P[1][1] = sigma_pos0 * sigma_pos0;
	P[2][2] = sigma_cap0 * (M_PI / 180) * sigma_cap0  * (M_PI / 180);
	P[3][3] = sigma_off0 * (M_PI / 180) * sigma_off0 * (M_PI / 180);
	P[4][4] = sigma_k0 * sigma_k0;

	// matrice covariance pour l'incertitude mod�le 
	Q0 = MATCARREE(dim);
	Q0.Constante(0);
	Q0[0][0] = Q0[1][1] = sigma_consigne_moteur * sigma_consigne_moteur * deltaT * deltaT;
	Q0[2][2] = sigma_consigne_moteur * sigma_consigne_moteur * (4 / ENTRAXE_ROBOT * ENTRAXE_ROBOT) * deltaT * deltaT;
	Q0[3][3] = sigma_off * (M_PI / 180) * sigma_off * (M_PI / 180) * deltaT * deltaT;
	Q0[4][4] = sigma_k * sigma_k * deltaT * deltaT;

	// matrice covariance de bruits d'observation
	RR = MATCARREE(3);
	RR.Constante(0);
	RR[0][0] = RR[1][1] = sigma_obs_pos * sigma_obs_pos;
	RR[2][2] = sigma_obs_cap * (M_PI / 180) * sigma_obs_cap * (M_PI / 180);
}

MATCARREE TFiltreKalman::Fk()
{
	FF = MATCARREE(dim);	
	FF.Identite();

	FF[0][2] = -k_moteur * (Vg + Vd) / 2 * deltaT * cos(theta); // d�riv�e de fonction d'�volution x par rapport � theta
	FF[0][4] = -(Vg + Vd) / 2 * deltaT * sin(theta); // d�riv�e de fonction d'�volution x par rapport � k

	FF[1][2] = -k_moteur * (Vg + Vd) / 2 * deltaT * sin(theta); // d�riv�e de fonction d'�volution y par rapport � theta
	FF[1][4] = (Vg + Vd) / 2 * deltaT * cos(theta); // d�riv�e de fonction d'�volution y par rapport � k

	FF[2][4] = (Vd - Vg) / ENTRAXE_ROBOT * deltaT; // d�riv�e de fonction d'�volution theta par rapport � k

	return FF;
}

MATRICE TFiltreKalman::Hk() 
//retourne la matrice d'observations
{ 
	// matrice d'observation (mesure sur x, y et theta):
	HH = MATRICE(3, dim);
	HH.Constante(0);
	 
	HH[0][0] = 1; HH[0][2] = -DECALAGE_Y_GPS_ERX * cos(theta);
	HH[1][1] = 1; HH[1][2] = -DECALAGE_Y_GPS_ERX * sin(theta);
	HH[2][2] = 1; HH[2][3] = 1;  

	// x_mes = x - DECALAGE * sin(theta);  ici DECALAGE ~= -1.24 m
	// y_mes = y + DECALAGE * cos(theta);
	// theta_mes = theta + theta_offset

	return HH; 
}		

void TFiltreKalman::EvolutionEtat()
{
	double Vgcorr = Vg * k_moteur;
	double Vdcorr = Vd * k_moteur;

	x += (Vgcorr + Vdcorr) / 2 * deltaT * (-sin(theta));
	y += (Vgcorr + Vdcorr) / 2 * deltaT * cos(theta);
	theta += (Vdcorr - Vgcorr) / ENTRAXE_ROBOT * deltaT;

	if (theta > M_PI)	theta -= 2 * M_PI;
	if (theta < -M_PI)	theta += 2 * M_PI;
}

VECTEUR TFiltreKalman::EtatCourant()
{
	VECTEUR V(dim);
	V[0] = x;
	V[1] = y;
	V[2] = theta;
	V[3] = theta_offset;
	V[4] = k_moteur;
	return V;
}

VECTEUR TFiltreKalman::PredictionObservations()
{
	double DECALAGE;
#ifdef GPS_EXTERNE
	DECALAGE = DECALAGE_Y_GPS_EXT;
#else
	DECALAGE = DECALAGE_Y_GPS_ERX;
#endif // GPS_EXTERNE

	VECTEUR X = EtatCourant();
	VECTEUR Y(3);
	Y[0] = X[0] - DECALAGE * sin(theta);
	Y[1] = X[1] + DECALAGE * cos(theta);
	Y[2] = X[2] + X[3];
	return Y;
	//return (HH*X).Colonne(0);
}

VECTEUR TFiltreKalman::MesureObservations()
/************************************/
{
	return Obs;
}

void TFiltreKalman::CorrigeEtat(VECTEUR V)
/****************************************/
{
	x += V[0];
	y += V[1];

	theta += V[2];
	if (theta > M_PI)	theta -= 2 * M_PI;
	if (theta < -M_PI)	theta += 2 * M_PI;

	theta_offset += V[3];
	if (theta_offset > M_PI)	theta -= 2 * M_PI;
	if (theta_offset < -M_PI)	theta += 2 * M_PI;

	k_moteur += V[4];
}


void TFiltreKalman::ChargePosInit(double x0, double y0, double thetarad0, double theta_offsetrad0, double k_moteur0)
/*charger la position initiale du robot (l'�tat initial du Kalman)*/
{
	x = x0; y = y0; theta = thetarad0;
	theta_offset = theta_offsetrad0; 
	k_moteur = k_moteur0;
}


void TFiltreKalman::InitParametres(double SigmaConsigneMoteurs, double SigmaObsPos, double SigmaObsCapDeg, double SigmaPos0, double SigmaCapDeg0,
	double SigmaOffsetDeg, double SigmaCoeffMoteur, double SigmaOffsetDeg0, double SigmaCoeffMoteur0)
{
	// matrice covariance d'erreur (incertidues initiales)
	P = MATCARREE(dim);
	P.Constante(0);
	P[0][0] = P[1][1] = SigmaPos0 * SigmaPos0;
	P[2][2] = SigmaCapDeg0 * (M_PI / 180) * SigmaCapDeg0 * (M_PI / 180);
	P[3][3] = SigmaOffsetDeg0 * (M_PI / 180) * SigmaOffsetDeg0 * (M_PI / 180);
	P[4][4] = SigmaCoeffMoteur0 * SigmaCoeffMoteur0;

	// matrice covariance pour les incertitudes de mod�le 
	Q0 = MATCARREE(dim);
	Q0.Constante(0);
	Q0[0][0] = Q0[1][1] = SigmaConsigneMoteurs * SigmaConsigneMoteurs * deltaT * deltaT;
	Q0[2][2] = SigmaConsigneMoteurs * SigmaConsigneMoteurs * (4 / ENTRAXE_ROBOT * ENTRAXE_ROBOT) * deltaT * deltaT;
	Q0[3][3] = SigmaOffsetDeg * (M_PI / 180) * SigmaOffsetDeg * (M_PI / 180) * deltaT * deltaT;
	Q0[4][4] = SigmaCoeffMoteur * SigmaCoeffMoteur * deltaT * deltaT;

	// matrice covariance pour les incertitudes d'observation
	RR = MATCARREE(3);
	RR.Constante(0);
	RR[0][0] = RR[1][1] = SigmaObsPos * SigmaObsPos;
	RR[2][2] = SigmaObsCapDeg * (M_PI / 180) * SigmaObsCapDeg * (M_PI / 180);
}


VECTEUR TFiltreKalman::GetEtatCourant()
{
	return EtatCourant();
}

void TFiltreKalman::SetConsigneMoteursActuelle(POINTFLT Consigne)
{
	// consigne moteurs: x-> gauche, y-> droite
	Vg = Consigne.x; Vd = Consigne.y;
}

void TFiltreKalman::GetParametresKalman(MATRICE& MRetour)
// retourne les r�sultats de dernier point du filtre Kalman (premi�re ligne est l'�tat dernier; deuxi�me ligne est les �cart-types)
{
	VECTEUR V = EtatCourant();

	MRetour[0][0] = V[0]; MRetour[0][1] = V[1];
	MRetour[0][2] = V[2] * (180 / M_PI); // cap en deg
	MRetour[0][3] = V[3] * (180 / M_PI); // cap offset en deg
	MRetour[0][4] = V[4];

	MRetour[1][0] = sqrt(P[0][0]); MRetour[1][1] = sqrt(P[1][1]);
	MRetour[1][2] = sqrt(P[2][2]) * (180 / M_PI); 
	MRetour[1][3] = sqrt(P[3][3]) * (180 / M_PI);
	MRetour[1][4] = sqrt(P[4][4]);
}