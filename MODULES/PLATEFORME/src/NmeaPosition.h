#pragma once

#include <string>
#include "PortCOMBaudrate.h"

enum NmeaPositionState
{
	OK =0,
	NoPosition,
	ReceptionError,
	ConnectionError
};


struct NMEAData
{
	double LatDeg, LonDeg;
	double Altitude;
	int Fix;
};





class __declspec (dllexport)  NmeaPosition
{
public:

	NmeaPosition(char *comPort, PortCOMBaudrate baudrate, bool SortieSiErreurReception = true);
	~NmeaPosition();

	NmeaPositionState GetData(NMEAData &data);

	std::string  GetLastError();
	

	std::string GetStateString()
	{
		const char* temp[] = { "OK", "NoPosition", "ReceptionError", "ConnectionError" };
		return std::string(temp[State]);
	}


private:
	NmeaPositionState State;
	char MsgErr[1000];
	struct NMEAData Data;

	bool ParsingEnCours;	//pour �viter lecture data durant leur �criture
	bool Initialised;
	bool ArretDemande;
	bool SortieSiErreurReception;

	bool ParseBuffer(char *buffer, int size, char * buffersuite, int &sizesuite, char *msgerr);
	bool Init(char *comPort, PortCOMBaudrate baudrate, char *msgerr);
	void ListenNMEA(char *msgerr);

	DWORD PreviousTickCount; //dernier instant de r�ception trame NMEA
		
	
};

