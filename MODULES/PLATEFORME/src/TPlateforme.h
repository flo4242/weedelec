#ifndef  __TPLATEFORME_H__
#define __TPLATEFORME_H__

#include "drvtraiti.h"
#include "ExportModulePlateforme.h"
#include "Plateforme.h"


extern TParamRobot ParamG;


struct TPlateforme : public DRVTRAITIM
{
	void CMParametre();

	// fonctions pour la connexion, la r�ception des mesures du robot
	void CMConnectRobot();
	void CMDeconnectRobot();
	void CMTestMesureRobot();


	// fonctions pour activation de RTK Erx
	void CMActiveRTKRobot();
	void CMGetStatutRTKRobot();

	// 
	void CMResetGroundFrame();



	// fonctions pour utiliser le capteur GPS externe
	void CMInitCapteurGPS();
	void CMTestCapteurGPSExterne();


	// g�n�ration de trajectoire

	void CMConvertPisteToWaypointKML();
	void CMConvertTxtToWaypointKML();
	void CMInitTrajectoire();


	// stop
	void CMStopMouvement();


	// commande de navigation du robot
	void CMNavigationAutoRobot();
	void CMNavigationManuelleRobot();


	// test filtre kalman
	//void CMTestFiltreKalmanOnLine();
	//void CMTestFiltreKalmanOffLine();
	//void CMTestDoWeeding();
	//void CMTestComHT();



	// fonctions pour le weeding

	void CMDialogueHWCommand();
	void CMDialogueControlBrasManuel();



	// commande de la t�te Haute tension
	void CMParametreHT();
	void CMInitCommandeHT();
	void CMLireParametreHT();
	void CMLireStatusHT();
	void CMLanceSequenceHT();


	// fonctions provisoires 
	void CMDemonstrationWeedelec();

};






#endif 

