
#include "ControlRobot.h"

#include "ExportModulePlateforme.h"
#include "Plateforme.h"

#include "CommandeCinetique.h"
#include "RobotNavigation.h"

#include <Windows.h>
#include "ppltasks.h"


#define  PAS_AVANCEE  1.5  // pas d'avanc�e pour chaque position de d�sherbage
#define  TIMEOUT_STEP  60000	// time-out p�riode pour avancer � la position suivante


extern TControlRobot ControlRobotG;
extern TCommandeCinetique TCommandeG;
extern TParamRobot ParamG;

bool StopUrgenceG = false;




void TModulePlateforme::GoToNextStep(TRetourPlateforme &ret, TStep &step)
{
	int TimeDebut = GetTickCount();
	ret.CropRowFinished = false;

	if (!ControlRobotG.RobotIsConnected())
	// v�rification de la connexion au robot
	{
		ret.InfoRetour = "Pas de connexion au robot";
		ret.State = TRetourTask::Error;
		ret.DureeMs = GetTickCount() - TimeDebut;
		return;
	}

	char msgerr[1000];
	bool retStart;
	switch (step.Type)
	{
		case TypeStep::trajectoire:
			retStart = TCommandeG.StartCommande(NULL, msgerr);
			break;
		case TypeStep::distance:
		{
			TStepDistance *stepd = (TStepDistance*)&step;
			retStart = TCommandeG.StartCommande(stepd->Distance, msgerr);
			break;
		}

		case TypeStep::cible:
		{
			TStepCible *stepc = (TStepCible*)&step;
			retStart = TCommandeG.StartCommande(stepc->Cible, msgerr);
			break;
		}
	}

	if(!retStart)
	{
		ret.InfoRetour = msgerr;
		ret.State = TRetourTask::Error;
		ret.DureeMs = GetTickCount() - TimeDebut;
		return;
	}

	POINTFLT P1 = TCommandeG.PtActuel;
	POINTFLT P2 = TCommandeG.Pchemin2;

	POINTFLT precedent = P1;
	
	POINTFLT Dir = (P2 - P1) / (P2 - P1).Norme();
	POINTFLT Pc = P1 + Dir*ParamG.PasAvancement;
	// point cible de la position suivante


	while (true)
	{

		MSG msg;
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			DispatchMessage(&msg);
		

		POINTFLT Pa = TCommandeG.PtActuel; // point actuel
		
		if (VerificationErreur(precedent, Pa, Pc)) // v�rification d'arriv�e de point suivant
		{
			StopPlateforme();
			break;
		}
		
		if (StopUrgenceG) // stop urgence par utilisateur
		{
			StopPlateforme(true);
			ret.InfoRetour = "Emergency stop !";
			ret.State = TRetourTask::ExternalStop;
			ret.DureeMs = GetTickCount() - TimeDebut;
			return;
		}

		if (GetTickCount() - TimeDebut > TIMEOUT_STEP) // timeout
		{
			StopPlateforme();
			ret.InfoRetour = "TimeOut";
			ret.State = TRetourTask::TimeOut;
			ret.DureeMs = GetTickCount() - TimeDebut;
			return;
		}
	}

	if (TCommandeG.index >= TCommandeG.TrajDesire.NbsPts - 1)
		ret.CropRowFinished = true;

	ret.State = TRetourTask::Completed;
	ret.DureeMs = GetTickCount() - TimeDebut;

	return;
}

void TModulePlateforme::Weeding(_list<POINTFLT> &weeds, TRetourPlateforme &ret)
//les coordonn�es des mauvaises herbes sont fournies dans l'espace du robot au moment des prises d'image
//pr�c�dentes (� charge de la plateforme d'ajuster en fonction du d�pacement effectif depuis)
{
	int TimeDebut = GetTickCount();
	ret.CropRowFinished = false;

	if (!ControlRobotG.RobotIsConnected())
	// v�rification de la connexion au robot
	{
		ret.InfoRetour = "Pas de connexion au robot";
		ret.State = TRetourTask::Error;
		ret.DureeMs = GetTickCount() - TimeDebut;
		return;
	}
	
	ControlRobotG.m_client_manager->resetErxGroundFrame();
	//reset ground frame

	ControlRobotG.m_client_manager->startInjectWeeds(true);
	// start inject weeds
	
	// recevoir les mesures de robot pour la position estim�e actuelle par rapport au rep�re sol
	/*double x, y, orient;
	ControlRobotG.GetEstimatedPosition(x, y, orient);
	orient *= (M_PI / 180);*/

	Sleep(3000);
	ControlRobotG.DoWeeding(weeds);
	//remplit le buffer � partir de la liste fournie

	Sleep(7000); 

	ControlRobotG.m_client_manager->startInjectWeeds(false);
	//arr�t des bras

	ret.State = TRetourTask::Completed;
	ret.DureeMs = GetTickCount() - TimeDebut;
	return;
}

void TModulePlateforme::Stop() //demande d'arr�t utilisateur
{
	StopUrgenceG = true;
	StopPlateforme(true);
	ControlRobotG.m_client_manager->startInjectWeeds(false);
}