#ifndef __COMMANDECINETIQUE_H
#define __COMMANDECINETIQUE_H

#include "ControlRobot.h"
#include "AppliqueFiltreKalman.h"
#include "RepereLocal.h"
#include "GenerationTrajectoire.h"


#ifndef DIST_DECALAGE
#define DIST_DECALAGE  1.08     // distance de d�calage du GPS (m�tres)
#endif


bool VerificationErreur(POINTFLT precedent, POINTFLT currentPt, POINTFLT desirePt);


class TCommandeCinetique
{
public:
	TRepereLocal *pR; // rep�re local utilis�
	TFiltreKalman *Fk; // filtre Kalman appliqu�
	Trajectoire TrajDesire; // trajectoire d�sir�e

	//POINTFLT PtPassageDesire; // point de passage d�sir� pour le suivi de trajectoire
	POINTFLT PtActuel; // point actuel
	
	double OrientActuelle; // en radian
	double OrientDesiree; // en radian

	double VitesseNominale;


	UINT_PTR pTimerCom; // timer ID pour �tablir une p�riode constante de commande (timer for callback function)
	int dT; // temps d'increment de commande


	int index; // index pour �num�rer les points de passage d�sir�s
	
	TCommandeCinetique() 
	{
		OrientActuelle = 0; OrientDesiree = 0; index = 0;
		pTimerCom = NULL; pR = NULL; Fk = NULL;
	};

	~TCommandeCinetique() 
	{
		if (pTimerCom)  KillTimer(NULL, pTimerCom); 
		if (pR) delete pR; 
		if (Fk) delete Fk; 
	}

	void InitParam(TFiltreKalman FKalman, Trajectoire traj, double LatM, int deltaT)
	{
		Fk = new TFiltreKalman(DIM_KALMAN); *Fk = FKalman; dT = deltaT;
		pR = new TRepereLocal(LatM); 
		TrajDesire = traj; 
		index = 0; // chaque fois (r�)initialiser les param�tres, le robot (re)commence d'aller au premier point d�sir�
	};


	// fonctions qui g�n�rent le timer
	bool DoStartCommande(char* msgerr);

	void StopCommande() { if (pTimerCom) KillTimer(NULL, pTimerCom); };


	// fonctions �l�mentaires pour la commande
	void SetPointActuel(double x, double y, double orient);

	POINTFLT GetConsignesMoteurs(double CapRad, double vitessenominale);
	
	void FiltreKalmanTempsReel(double x, double y, double orient);


	//fonctions pour le suivi de chemin
	void AppliqueCommande();

	void CalculCapDesire(POINTFLT P1, POINTFLT P2, POINTFLT PC);


	bool StartCommande(_list<POINTFLT> &liste, char* msgerr);

	bool StartCommande(double distance, char* msgerr);

	bool StartCommande(POINTFLT cible, char* msgerr);

	
};


#endif 
