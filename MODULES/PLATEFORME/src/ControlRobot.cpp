#include "ControlRobot.h"
#include "utilfic.h"
#include <string>


// connexion:
bool TControlRobot::ConnectRobot(char* errmsg)
{
	utility::string_t temp = utility::conversions::to_string_t(connectAddress);

	m_client_manager->connect(temp);
	if (!RobotIsConnected())
	{
		char addtemp[100];
		strcpy(addtemp, connectAddress.c_str());
		sprintf(errmsg, "Connexion �chou�e pour l'adresse %s", addtemp);
		return false;
	}
	return true;
		
}

bool TControlRobot::DeconnectRobot(char* msgerr)
{
	if (!RobotIsConnected())
	{
		sprintf(msgerr, "Pas de connexion en cours avec le robot");
		return false;
	}
	m_client_manager->close();
	return true;
}


// rec�ption de donn�es du capteur Erx
bool TControlRobot::GetRobotSensorData(double& Lat, double& Lon, double& CapDeg, char* msgerr)
{
	if (!RobotIsConnected())
	{
		sprintf(msgerr, "Pas de connexion au robot");
		return false;
	}
	web::json::value temp = m_client_manager->getRawAbsPosition();

	Lat = temp.at(L"lat").as_double();
	Lon = temp.at(L"lon").as_double();
	CapDeg = temp.at(L"orient").as_double();

	return true;
}


bool TControlRobot::GetRobotEstimatedPosition(double& x, double& y, double& CapDeg, char* msgerr)
{
	if (!RobotIsConnected())
	{
		sprintf(msgerr, "Pas de connexion au robot");
		return false;
	}

	web::json::value Pos = m_client_manager->getEstimatedPosition();
	x = Pos.at(L"x").as_double();
	y = Pos.at(L"y").as_double();
	CapDeg = Pos.at(L"orient").as_double();

	return true;
}



// activation du GPS rtk
void TControlRobot::ActiveRTK()
{
	if (RobotIsConnected())
		m_client_manager->activateRTKCorrection(true);
}

bool TControlRobot::GetRTKStatus(char* ntripState, char* rtkState, char* msgerr)
{
	if (!RobotIsConnected())
	{
		sprintf(msgerr, "Pas de connexion en cours avec le robot");
		return false;
	}
	web::json::value rtkStatus = m_client_manager->getRTKStatus();

	if (rtkStatus.has_field(L"rtkStat"))
	{
		utility::string_t rtk = rtkStatus.at(L"rtkStat").as_string();
		std::string rtkString = utility::conversions::to_utf8string(rtk);
		strcpy(rtkState, rtkString.c_str());
	}

	if (rtkStatus.has_field(L"ntripStat"))
	{
		utility::string_t ntrip = rtkStatus.at(L"ntripStat").as_string();
		std::string ntripString = utility::conversions::to_utf8string(ntrip);
		strcpy(ntripState, ntripString.c_str());
	}
	
	return true;
}


void TControlRobot::ResetGroundFrame()
{
	if (!RobotIsConnected()) return;
	m_client_manager->resetErxGroundFrame();
}

// mouvement simple
void TControlRobot::Avancement(double distance)
{
	if(RobotIsConnected())
		m_client_manager->moveStraight(distance);
}

void TControlRobot::RotateAngle(double angle, double radius)
{
	if (RobotIsConnected())
		m_client_manager->rotateAngle(angle, radius);
}

void TControlRobot::StopMouvement()
{
	if (RobotIsConnected())
		m_client_manager->stopMovement();
}


// envoie de la consigne de moteurs
void TControlRobot::SetRobotSpeed(double vL, double vR)
{
	if (RobotIsConnected())
		m_client_manager->setSpeed(vL, vR);
}


void TControlRobot::SendHWCommand(std::string cmd)
{
	utility::string_t command = utility::conversions::to_string_t(cmd);

	m_client_manager->sendHWcommand(command);
}


// initialisation de bras
void TControlRobot::InitRightArm()
{
	std::string cmd = "d2on";
	SendHWCommand(cmd);

	cmd = "d2home";
	SendHWCommand(cmd);

	char msg_cmd[100];
	sprintf(msg_cmd, "d2tz %f", POSITIONZ_POSITIONING);
	SendHWCommand(msg_cmd);

	ArmInitialised = true;
}

void TControlRobot::InitLeftArm()
{
	std::string cmd = "d1on";
	SendHWCommand(cmd);

	cmd = "d1home";
	SendHWCommand(cmd);

	char msg_cmd[100];
	sprintf(msg_cmd, "d1tz %f", POSITIONZ_POSITIONING);
	SendHWCommand(msg_cmd);
}


// reset de bras
void TControlRobot::ResetRightArm()
{
	std::string cmd = "d2reset";
	SendHWCommand(cmd);

	cmd = "d2home";
	SendHWCommand(cmd);
}

void TControlRobot::ResetLeftArm()
{
	std::string cmd = "d1reset";
	SendHWCommand(cmd);

	cmd = "d1home";
	SendHWCommand(cmd);

}


// fermeture de bras
void TControlRobot::CloseRightArm()
{
	std::string cmd = "d2reset";
	SendHWCommand(cmd);

	cmd = "d2home";
	SendHWCommand(cmd);

	cmd = "d2off";
	SendHWCommand(cmd);
}

void TControlRobot::CloseLeftArm()
{
	std::string cmd = "d1reset";
	SendHWCommand(cmd);

	cmd = "d1home";
	SendHWCommand(cmd);

	cmd = "d1off";
	SendHWCommand(cmd);
}


// weeding 
void TControlRobot::TestWeedingRightArm(_list<POINTFLT> weeds)
// fonction test pour le weeding 
// l'envoi de liste des weeds, contr�le de d�placement de bras sans d�charge �lectrique(un tempon � la place)
{
	char msg_cmd[100];

	BalayeListe(weeds)
	{
		POINTFLT *Weed = weeds.current();
		sprintf(msg_cmd, "d2tx %f", Weed->x);
		SendHWCommand(msg_cmd);
		sprintf(msg_cmd, "d2ty %f", Weed->y);
		SendHWCommand(msg_cmd);

		Sleep(DELAY_POSITIONING);

		if( HauteurWeeding > 0.8 && HauteurWeeding < 0.95)
			sprintf(msg_cmd, "d2tz %f", HauteurWeeding);
		else
			sprintf(msg_cmd, "d2tz %f", POSITIONZ_WEEDING);

		SendHWCommand(msg_cmd);
		Sleep(DELAY_WEEDING);

		sprintf(msg_cmd, "d2tz %f", POSITIONZ_POSITIONING);
		SendHWCommand(msg_cmd);
		Sleep(DELAY_POSITIONING);
	}

	return;
}

void TControlRobot::TestWeedingLeftArm(_list<POINTFLT> weeds)
{
	char msg_cmd[100];

	BalayeListe(weeds)
	{
		POINTFLT *Weed = weeds.current();
		sprintf(msg_cmd, "d1tx %f", Weed->x);
		SendHWCommand(msg_cmd);
		sprintf(msg_cmd, "d1ty %f", Weed->y);
		SendHWCommand(msg_cmd);
		Sleep(DELAY_POSITIONING);

		sprintf(msg_cmd, "d1tz %f", HauteurWeeding);
		SendHWCommand(msg_cmd);
		Sleep(DELAY_WEEDING);

		sprintf(msg_cmd, "d1tz %f", POSITIONZ_POSITIONING);
		SendHWCommand(msg_cmd);
		Sleep(DELAY_POSITIONING);
	}

	return;
}



void TControlRobot::SetPositionXYWeedR(POINTFLT weed)
{
	char msg_cmd[100];

	sprintf(msg_cmd, "d2tz %f", POSITIONZ_POSITIONING);
	SendHWCommand(msg_cmd);

	sprintf(msg_cmd, "d2tx %f", weed.x);
	SendHWCommand(msg_cmd);
	sprintf(msg_cmd, "d2ty %f", weed.y);
	SendHWCommand(msg_cmd);

	return;
}


void TControlRobot::SetPositionZWeedingR()
{
	if (HauteurWeeding > 0.8 && HauteurWeeding < 0.95)
		SetHeightR(HauteurWeeding);
	else
		SetHeightR(POSITIONZ_WEEDING);

	Sleep(DELAY_POSITIONING);
}


void TControlRobot::SetHeightR(double dis)
{
	char cmd[100];
	sprintf(cmd, "d2tz %f", dis);
	SendHWCommand(cmd);
}


void TControlRobot::SetPositionAwaitedR()
{
	SetHeightR(POSITIONZ_POSITIONING);
	Sleep(DELAY_POSITIONING);

	SendHWCommand("d2tx 0");
	SendHWCommand("d2ty 0");
	Sleep(DELAY_POSITIONING);

	return;
}




void TControlRobot::SetPositionXYWeedL(POINTFLT weed)
{
	char msg_cmd[100];

	sprintf(msg_cmd, "d1tz %f", POSITIONZ_POSITIONING);
	SendHWCommand(msg_cmd);

	sprintf(msg_cmd, "d1tx %f", weed.x);
	SendHWCommand(msg_cmd);
	sprintf(msg_cmd, "d1ty %f", weed.y);
	SendHWCommand(msg_cmd);

	return;
}


void TControlRobot::SetPositionZWeedingL()
{
	if (HauteurWeeding > 0.8 && HauteurWeeding < 0.95)
		SetHeightR(HauteurWeeding);
	else
		SetHeightR(POSITIONZ_WEEDING);

	Sleep(DELAY_POSITIONING);
}


void TControlRobot::SetHeightL(double dis)
{
	char cmd[100];
	sprintf(cmd, "d1tz %f", dis);
	SendHWCommand(cmd);
}


void TControlRobot::SetPositionAwaitedL()
{
	SetHeightL(POSITIONZ_POSITIONING);
	Sleep(DELAY_POSITIONING);

	SendHWCommand("d1tx 0");
	SendHWCommand("d1ty 0");

	Sleep(DELAY_POSITIONING);

	return;
}
