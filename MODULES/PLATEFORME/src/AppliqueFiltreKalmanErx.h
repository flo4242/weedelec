#ifndef __APPLIQUEFILTREKALMANERX_H_
#define __APPLIQUEFILTREKALMANERX_H_

#include "Plateforme.h"
#include "FiltreKalmanRobotErx.h"

#include <Windows.h>
#include <ppltasks.h> 
#include <agents.h> // pour timer

using namespace Concurrency;


struct TAppliqueKalman
{

	timer<bool> *Timer_Kalman;
	bool GPSExterne;

	TFiltreKalman *pKalman;

	TPositionRobot PosRobotKalman;

	ofstream *pFichierLog;

	MATRICE RetourKalman;

	TAppliqueKalman() { pKalman = NULL; pFichierLog = NULL; }

	~TAppliqueKalman() 
	{ 
		if (pKalman) delete pKalman; 
		if (Timer_Kalman) delete Timer_Kalman;
		if (pFichierLog) delete pFichierLog;
	}


	void Init(TFiltreKalman *kalman, bool GPSexterne, ofstream * pLog = NULL)
	{
		pKalman = kalman; GPSExterne = GPSexterne; pFichierLog = pLog;	
	};

	bool SetTimer(char *msgerr);

	void StartTimer();

	void StopTimer();


	bool EvolutionMesuresKalman();

	void UpdatePositionKalman(double x, double y, double capdeg);

	void GetPositionRobotKalman(TPositionRobot &Pos);

};


void AfficheKalman();

void SauveLogKalman(ofstream *pOs, double Ts, VECTEUR EtatCourant, VECTEUR Variance, POINTFLT CMoteurs, VECTEUR Mes);



#endif 