#ifndef __LISTETRACKPOINTSGPX_H
#define __LISTETRACKPOINTSGPX_H

#include "list_tpl.h"

struct TrackPoint
{
	double LatDeg, LonDeg;
	double GroundSpeed;
	double Elevation;
	SYSTEMTIME STime;

	//extensions eventuelles:
	double HeadingDeg; //format g�o: nord � 0�
	char Name[100];
	char *pName;

	TrackPoint() { Name[0] = '\0'; pName = Name; Elevation = 0; LatDeg = 0; LonDeg = 0; GroundSpeed = 0; HeadingDeg = 0; }

	TrackPoint(const TrackPoint& l)
	{
		memcpy(this, &l, sizeof(l));
		if (l.pName != '\0') 
		{ 
			int len = strlen(l.pName) + 1; 
			strcpy(Name, l.pName); memset(Name + len, 0, sizeof(char));
		}
	}

	TrackPoint& operator=(const TrackPoint& l)
	{
		memcpy(this, &l, sizeof(l));
		if (l.pName != '\0')
		{
			int len = strlen(l.pName) + 1;
			strcpy(Name, l.pName); memset(Name + len, 0, sizeof(char));
		}
		return *this;
	}

	void AlloueName(char * name, int taillemax) 
	{ 
		int len = strlen(name) + 1;
		strcpy(Name, name); 
		memset(Name + len, 0, sizeof(char));
	}
	~TrackPoint() 	{}

};

bool ExportGPX(char *Nf, char* NomTrk, _list<TrackPoint> ListePointGPX, char* msgerr);

bool ImportGPX(char *Nf, _list<TrackPoint>& ListePointGPX, char* msgerr);

#endif