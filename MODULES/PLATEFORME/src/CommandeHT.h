#pragma once

/*D�finition de la structure pour la commande de bo�tier Haute Tension Weedelec*/
// Les commandes sont envoy�es � la carte STM32 par la liaison s�rielle (communication port s�rie RS232)

#include "ComPortSerieHT.h"
#include "commtype.h"


// les param�tres pour caract�riser la commande Haute Tension
struct TParamHT
{
	char PortCOM[10];  // nom de port COM, "COMx"
	int DwellTime;    // entre 1 us - 65535 us, ne pas d�passer 10 ms (10000us)
	int Frequence;    // entre 500 - 1500 Hz en r�alit�, th�oriquement 10 Hz - 10000 KHz
	int NbCycles;	  // nombre de cycles r�alis�s par s�quence, 1 - 65536
	int CurrentLimit; // limite de courant dans la bobine, en mA (0 = pas de limitation)
};

// d�finition de l'adresse pour saisir des param�tres
#define ADDRESS_DWELL_TIME  "00"
#define ADDRESS_FREQ		"01"
#define ADDRESS_NBCYCLE		"02"
#define ADDRESS_CURRLIMIT	"03"

// d�finition de la taille des messages envoy�s
#define LENGTH_MSGRET_CONFIG		3		// taille de message retour pour configurer des paramt�res
											// toujours 3 carat�res '<K>', K: r�ussi, N: param�tre non support� ou lecture seul

#define LENGTH_MSGRET_READ			7		// taille de message retour pour la lecture des valeurs des paramt�res
											// '<Raaaa>', aaaa: valeur en hexad�cimal

#define LENGTH_MSGRET_SEQUENCE		21		// taille de message retour apr�s le lancement d'une s�quence
											// retour : '<S0012AC014000530000>'
											// --> 12AC : courant max 0x12AC = 4780 mA
											// --> 0140 : dur�e de l'arc 0x0140 = 320 us
											// --> 0053 : tension de l'arc 0x0053 = 83V


// Contraintes de mat�riel fournies par Alciom:
// Les param�tres doivent donc �tre programm�s de mani�re � respecter les points suivants

#define DWELL_TIME_LIMIT  10000 // dur�e de charge de la bobine,
								// inf�rieur � la dur�e maximale sp�cifi�e par le fabricant de la bobine (10 ms)

#define DUTTY_CYCLE  0.9		// dutty cycle = Dwell_time * fr�quence, inf�rieur � 90% (s�curity)

#define DURATION_SEQUENCE_LIMIT  1.0   // dur�e globale de la s�quence ne soit pas trop longue
									   // dur�e_s�quence = Nb_Cycles / fr�quence


struct CommandeHT
{
	PortComHT* ComHT; // communication port s�rie

	CommandeHT() { ComHT = NULL; };
	~CommandeHT() { if (ComHT) delete ComHT; ComHT = NULL; };

	bool InitCommandeHT(TParamHT paramHT, char *msgerr);

	bool WriteParamHT(TParamHT paramHT, char *msgerr);

	bool ReadParamHT(TParamHT& paramHT, char *msgerr);

	bool ReadTemperature(double& temperature, char *msgerr);

	bool ReadVoltageBattery(double& volts, char *msgerr);

	bool StartSequence(int& CurrentMax, int& SparkVoltage, int& SparkDuration, char *msgerr);
};


bool ConfigurationParam(PortComHT* ComHT, char* msgcmd, char* msgerr);

bool ReadBoardStatus(PortComHT* ComHT, char* msgcmd, int& valdec, char* msgerr);

bool EmissionCommande(PortComHT* ComHT, char* MsgCmd, char* MsgRet, int tailleMsgRet);