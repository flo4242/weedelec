#include "ControlRobot.h" 
// il faut toujours inclure "ControlRobot.h" en premier pour que "erxCom.h" soit d�clar� en premier 
// (sinon incompatibilit� se trouve pour le module 'chrono') 

#include "CommandeNavigation.h"

#include "ExportModulePlateforme.h"
#include "AppliqueFiltreKalmanErx.h"

#include "drvtraiti.h"


#define GAIN_ORIENT  0.4  // gain de commande pour l'orientation
#define RAYON_RATTRAPAGE  2.0  // rayon de rattrapage en m�tres pour le suivi chemin
#define THETA_CST  45.0*(M_PI/180)  // constante pour la fonction exponentielle : v(err_theta) = Vmax * exp{-err_theta / THETA_CST}


extern TConsigneMoteurs ConsigneMoteursG; // structure globale pour la consigne moteurs
extern TControlRobot ControlRobotG; // structure globale pour appeler les fonctions API Ecorobotix
extern TAppliqueKalman AppliqueKalmanG; // structure globale de filtre Kalman

extern bool StopUrgenceG;  // arr�t d'urgence
 
TCommandeNavigation *CommandeNavigationG = NULL;

/////////////////////////////////////////
/********* Fonction callback ***********/
auto callback_commande = new call<bool>([](bool)
// �tabli la fonction callback pour la commande
{
	/* cette fonction timer maintient l'envoi r�gulier des consignes moteur d'avancement (sinon les
	 moteurs s'arr�tent).
	 En mode auto, elle g�re aussi le d�tail du rattrapage de trajectoire
	 */

	if (CommandeNavigationG->CommandeManuelle)
	// si commande manuelle, les consignes de moteurs sont donn�es par le control pad
	{
		TCommandeManuelle *pComManuelle = dynamic_cast <TCommandeManuelle*> (CommandeNavigationG);
		if (pComManuelle == NULL) // v�rification de dynamic_cast de pointeur
		{
			CommandeNavigationG->StopTimer();
			return;
		}

		POINTFLT ComMot = pComManuelle->GetVitesseCommandeManuelle();
		ControlRobotG.SetRobotSpeed(ComMot.x, ComMot.y);
		ConsigneMoteursG.UpdateConsigneCourante(ComMot.x, ComMot.y);
	}

	else // commande automatique de suivi de chemin
	{
		int TimeDebut = GetTickCount();

		TCommandeAutomatique *pComAuto = dynamic_cast <TCommandeAutomatique*> (CommandeNavigationG);
		
		TRetourPlateforme ret;
		ret.CropRowFinished = false;

		if (pComAuto == NULL) // v�rification de dynamic_cast de pointeur
		{
			CommandeNavigationG->StopTimer();
			
			RetourneStateError(ret, TimeDebut, "Pointeur commande non conforme!");
			if(pComAuto->callbackRetour != NULL)	 pComAuto->callbackRetour(ret);
			return;
		}

		// mettre � jour le segment courant de trajectoire
		int indexActuel = pComAuto->indexSegmentTraj;
		POINTFLT P1 = pComAuto->TrajDesire[indexActuel].obj;
		POINTFLT P2 = pComAuto->TrajDesire[indexActuel + 1].obj;

		// r�cup�ration de mesures actuelles corrig�es par le filtre Kalman
		AppliqueKalmanG.GetPositionRobotKalman(pComAuto->PosActuelle);
		POINTFLT Pc(pComAuto->PosActuelle.x, pComAuto->PosActuelle.y); // point actuel

		// calcul du cap d�sir� � l'instant, le r�sultat de cap d�sir� sera stock� dans la variable PosDesiree.CapDeg
		pComAuto->CalculCapDesire(P1, P2, Pc);

		POINTFLT ComMot = pComAuto->CalculConsignesMoteurs();

		ControlRobotG.SetRobotSpeed(ComMot.x, ComMot.y); // envoie la consigne de moteurs
		ConsigneMoteursG.UpdateConsigneCourante(ComMot.x, ComMot.y); // mise � jour de consignes moteurs pour le filtre Kalman
		
		//on a maintenant �tabli le cap et la vitesse en fonction du segment de trajectoire � rattraper

		//on va maintenant tester les conditions d'arr�t
		POINTFLT PtDebutDistance;
		double DistanceMax;
		TSubStepLocal *TS = pComAuto->Substep;

		if (TS != NULL)
		{
			DistanceMax = TS->DistanceMax;
			if (DistanceMax*DistanceMax >= (P2 - Pc).Norme2())
				//le dernier substep d�passe la fin du segment de trajectoire
				// --> on se replace dans le cas sans substep
			{
				//if(TS != NULL)   delete TS;   // ici, delete ne fonctionne pas !!!
				TS = NULL;
			}
		}


		if (TS != NULL)
		// si on avanve � la position suivant par un step
		// --> on v�rifie les coordonn�es fournies par Erx dans leur rep�re de sol
		{
			PtDebutDistance = TS->Debut;

			double XLocal, YLocal, CapDegLocal; 
			char msgerr[1000];

			if (!ControlRobotG.GetRobotEstimatedPosition(XLocal, YLocal, CapDegLocal, msgerr))
			{
				RetourneStateError(ret, TimeDebut, msgerr);
				if (pComAuto->callbackRetour != NULL)	 pComAuto->callbackRetour(ret);
				return;
			}
			
			Pc.x = XLocal; Pc.y = YLocal;
		}

		else
		{
			PtDebutDistance = P1;
			DistanceMax = (P2-P1).Norme();
		}
		

		if(TestArret(PtDebutDistance, Pc, DistanceMax) || StopUrgenceG)
		{
						
			if (TS == NULL)
			{
				// v�rification de passage au segment suivant de la trajectoire
				if (pComAuto->indexSegmentTraj < pComAuto->TrajDesire.nbElem - 1)
					pComAuto->indexSegmentTraj++; //passage au segment suivant

				else
				{
					pComAuto->StopTimer(); //arr�t du timer
					StopPlateforme();
					ret.CropRowFinished = true;
				}
					
			}

			if (TS != NULL)
			{
				pComAuto->StopTimer(); //arr�t du timer
			}

			if (StopUrgenceG == true)  // v�rification si c'est un arr�t d'urgence
			{
				pComAuto->StopTimer();
				ControlRobotG.SetRobotSpeed(0, 0);
				ret.CropRowFinished = true;

				ret.State = TRetourState::ExternalStop;
				ret.DureeMs = GetTickCount() - TimeDebut;
				ret.InfoRetour = "Stop Urgence!";
				if (pComAuto->callbackRetour != NULL)	pComAuto->callbackRetour(ret);
			}
			else
			{
				ret.State = TRetourState::Completed;
				ret.DureeMs = GetTickCount() - TimeDebut;
				if (pComAuto->callbackRetour != NULL)	pComAuto->callbackRetour(ret);
			}
		}
	}
});


/////////////////////////////////////////////////////////////////////////////////////////////////////
/********************************Commande navigation - classe de base ******************************/

void TCommandeNavigation::SetTimer()
{
	int period = PeriodeCommande;  // p�riode de timer en milisecondes

	//cr�e le timer pour la commande de suivi de chemin
	Timer_Commande = new timer<bool>(period, NULL, callback_commande, true);

	StartTimer();
}

void TCommandeNavigation::StartTimer()
// start timer
{
	if (Timer_Commande != NULL)		Timer_Commande->start();
}

void TCommandeNavigation::StopTimer()
{
	if (Timer_Commande != NULL)		Timer_Commande->pause();
}





//////////////////////////////////////////////////////////////////////////////////
/********************************Commande manuelle ******************************/

bool TCommandeManuelle::DoStartCommande(char *msgerr)
{
	if (Timer_Commande)
	{
		delete Timer_Commande;
		Timer_Commande = NULL;
	}
	// --> on va r�initialiser le timer dans le redemarrage de mouvement du robot

	SetTimer();
	
	if (Timer_Commande == NULL)
	{
		sprintf(msgerr, "Set Timer �chou�");
		return false;
	}

	return true;
}

void TCommandeManuelle::VitesseNominaleAdd()
{
	if (SetValeurValide) VitesseNominale += UNITE_VITESSE;

	if (abs(VitesseNominale) > SEUIL_VITESSE) // contrainte sur la vitesse nominale
		VitesseNominale = SEUIL_VITESSE; // vitesse d'avancement max si le robot ne tourne pas
}

void TCommandeManuelle::VitesseNominaleSub()
{
	if (SetValeurValide) VitesseNominale -= UNITE_VITESSE;
	
	if (abs(VitesseNominale) > SEUIL_VITESSE)
		VitesseNominale = -SEUIL_VITESSE;
}

void TCommandeManuelle::VitesseDiffAdd() 
{ 
	if (SetValeurValide) VitesseDifferentielle += UNITE_VITESSE; 

	if (abs(VitesseDifferentielle) > SEUIL_VITESSE) // contrainte sur la vitesse diff�rentielle (�gual � 0.4 m/s selon l'essai r�el)
		VitesseDifferentielle = SEUIL_VITESSE; // vitesse diff�rentielle max si le robot tourne sur place
}

void TCommandeManuelle::VitesseDiffSub()
{
	if (SetValeurValide) VitesseDifferentielle -= UNITE_VITESSE;

	if (abs(VitesseDifferentielle) > SEUIL_VITESSE)
		VitesseDifferentielle = -SEUIL_VITESSE;
}


POINTFLT TCommandeManuelle::GetVitesseCommandeManuelle()
{
	SetValeurValide = false;
	double vg = VitesseNominale - VitesseDifferentielle;
	double vd = VitesseNominale + VitesseDifferentielle;
	SetValeurValide = true;

	// mis en place du seuil de vitesse
	if (abs(vg) > SEUIL_VITESSE) vg = (vg / abs(vg))* SEUIL_VITESSE;
	if (abs(vd) > SEUIL_VITESSE) vd = (vd / abs(vd))* SEUIL_VITESSE;
	return POINTFLT(vg, vd);
}






/////////////////////////////////////////////////////////////////////////////////////////////////////
/********************************Commande Automatique de navigation*********************************/

// lance la commande automatique


bool TCommandeAutomatique::DoStartCommande(char* msgerr)
{
	if (TrajDesire.nbElem < 2 || TrajDesire.nbElem - 2 < indexSegmentTraj) 
	// au moins 2 points restant dans la trajectoire	
	{
		sprintf(msgerr, "Pas assez de points restant � suivre");
		return false;
	}

	if (Timer_Commande) 
	{
		delete Timer_Commande;
		Timer_Commande = NULL;
	}
	// --> on va r�initialiser le timer dans le redemarrage de mouvement du robot

	SetTimer();

	return true;
}

bool TCommandeAutomatique::RestartCommande(char* msgerr)
{
	indexSegmentTraj = 0; // reset index pour recommencer depuis le premier point
	return DoStartCommande(msgerr);
}



/*********************Suivi de chemin effectu� par un suivi de cap d�sir�***************************/

// v�rification de la distance d'avancement du robot par rapport au point pr�c�dent
bool TestArret(POINTFLT Ptprecedent, POINTFLT Ptcourant, double distancemax)
{
	return ((Ptcourant - Ptprecedent).Norme2() >= distancemax*distancemax);
	//permet au robot d'aller jusqu'au bout (si pas d'erreur de cap et pas de rattrapage)
}


// commande cin�matique sur le suivi de l'orientation
double GetVitesseLineaireSelonErreurCap(double ErrCapRad, double vitessenominale)
// Calculer la vitesse d'avanc�e � partir de l'erreur de l'orientation d�sir�e ()
// Fonction potentielle : y(theta) = V*exp{-theta/theta_c} ;
{
	return vitessenominale * exp(-ErrCapRad / THETA_CST);
}

POINTFLT TCommandeAutomatique::CalculConsignesMoteurs()
// Commande cap : 1/E*(vD - vG) = K*(theta_desire - theta_actuel)
// Commande vitesse d'avancement : V = 1/2*(vG + vD)  -> constante(pourrait aussi r�gler en fonction de l'erreur de suivi de cap)
// Consigne de vitesse : vG = V - E/2*K*(theta_desire - theta_actuel), vD = V + E/2*K*(theta_desire - theta_actuel)
{
	double ErrOrient = (PosDesiree.CapDeg - PosActuelle.CapDeg)*M_PI/180;

	if (abs(ErrOrient + 2 * M_PI) < abs(ErrOrient)) //v�rification de la zone de discontinuit�
		ErrOrient += 2 * M_PI;
	else if (abs(ErrOrient - 2 * M_PI) < abs(ErrOrient))
		ErrOrient -= 2 * M_PI;

	double consignV_theta = ENTRAXE_ROBOT / 2 * GAIN_ORIENT * ErrOrient;

	double V = GetVitesseLineaireSelonErreurCap(abs(ErrOrient), vitesseNominale);

	double vg, vd;

	vg = V - consignV_theta;
	vd = V + consignV_theta;

	// mis en place du seuil de vitesse
	if (abs(vg) > SEUIL_VITESSE) vg = (vg / abs(vg))* SEUIL_VITESSE;
	if (abs(vd) > SEUIL_VITESSE) vd = (vd / abs(vd))* SEUIL_VITESSE;

	return POINTFLT(vg, vd);
}



void TCommandeAutomatique::CalculCapDesire(POINTFLT P1, POINTFLT P2, POINTFLT PC)
// calcule le cap d�sir� � partir du segment de chemin actuel(d�fini par P1, P2) et du point actuel
{

	POINTFLT U = (P2 - P1) / (P2 - P1).Norme();
	POINTFLT P1C = PC - P1;
	POINTFLT PH = P1 + U * (P1C | U);

	double dist = (PC - PH).Norme();
	
	POINTFLT PCible;

	double longueur = RAYON_RATTRAPAGE * RAYON_RATTRAPAGE - dist * dist;

	if (longueur <= 0)
		PCible = PH;
	else
		PCible = PH + U * sqrt(longueur);

	PosDesiree.x = PCible.x;
	PosDesiree.y = PCible.y;
	PosDesiree.CapDeg = atan2(PC.x - PCible.x, PCible.y - PC.y) * 180 / M_PI;
}
