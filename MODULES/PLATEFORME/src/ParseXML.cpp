﻿#include <Windows.h>
#include <stdio.h>
#include "ParseXML.h"

#define XML_STATIC //pour VS_Expat

#include "expat.h"



//#include "utilfic.h"	//pour TailleFichier

/* NB: la définition
	void (* HandlerChar)(void *userData, const XML_Char *s, int len);

a été remplacée par:
	void (* HandlerEnd)(void *userData, const char *el);

pour éviter une dépendance vers "expat.h" dans  "ParseXML.h".
C'est faisable car on impose dans le code que le parser fournisse des caractères
de type UTF_8		*/

bool CDataDefault = true;	//valide le comportement par défaut (skip des zones CDATA)
bool CDataEncoursG = false;

void  HandlerStartCdataDefault(void *userData)		{	CDataEncoursG = true;	}
void  HandlerEndCdataDefault(void *userData)		{	CDataEncoursG = false;	}

bool CDATAEnCours()	{ return    CDataEncoursG;	}


#ifdef WIN32
//repris de Utilfic.cpp (et renommé) pour être indépendant de Winutil
//version linux à adapter

static bool GetBigFileSize(char *nf, DWORD &FileSizeLow, DWORD &FileSizeHigh )
/****************************************************************************/
{
WIN32_FILE_ATTRIBUTE_DATA WFA;

//conversion unicode du nom de fichier
int taille= (strlen(nf)+1);	wchar_t *nfW= new wchar_t[taille];
MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, nf, -1, nfW, taille);

int ret = GetFileAttributesExW(nfW, GetFileExInfoStandard, &WFA);

delete nfW;	if(!ret)	return false;
FileSizeHigh = WFA.nFileSizeHigh;
FileSizeLow = WFA.nFileSizeLow;
return true;
}

static bool GetFileFize(char *nf, int &taille )
/****************************************/
{
DWORD FileSizeLow, FileSizeHigh;
if(!GetBigFileSize(nf, FileSizeLow, FileSizeHigh ))	return false;
if(FileSizeHigh!=0)	return false;	//anormal
taille = FileSizeLow;
return true;
}
#endif


bool ParseFichierXML(char *Nf, 	 TParamParse &pm, char *msg5000)
/*****************************************************************/
{
if(strlen(Nf)==0)	{ sprintf(msg5000, "Parse XML: nom de fichier vide ! ", Nf); return false;	}

int TailleF; if(!GetFileFize(Nf, TailleF))
	{ sprintf(msg5000, "Parse XML: Echec lecture taille %s", Nf); return false;	}

FILE * F= fopen(Nf, "rb");
if(!F){ sprintf(msg5000, "Echec ouverture %s", Nf); return -1;	}


//création du Parser p
XML_Parser p = XML_ParserCreate("UTF-8");
if (! p)	{ strcpy(msg5000, "Couldn't allocate memory for parser");
			   fclose(F); return false;	}

//spécification des handlers
if(pm.pUserData) XML_SetUserData(p, pm.pUserData);
if(pm.HandlerStart)
	{
	if(pm.HandlerEnd) XML_SetElementHandler(p, pm.HandlerStart, pm.HandlerEnd);
	else     { strcpy(msg5000, "Handle stop non spécifié!! (revoir code)");
			   XML_ParserFree(p);	fclose(F); return false;	}
	}

if(pm.HandlerChar)	XML_SetCharacterDataHandler(p, pm.HandlerChar);

if(pm.HandlerStartCdata)
	{
	if(pm.HandlerEndCdata)
    	{
		XML_SetCdataSectionHandler(p, pm.HandlerStartCdata, pm.HandlerEndCdata);
		CDataDefault =false;
		}
	else     { strcpy(msg5000, "Handle stop non spécifié!! (revoir code)");
			   XML_ParserFree(p);	fclose(F); return false;	}
	}

else	//handler Cdata par défaut
	{
	XML_SetCdataSectionHandler(p, HandlerStartCdataDefault, HandlerEndCdataDefault);
	CDataDefault = true;
	}



//création et remplissage du buffer de fichier
char *BuffF = new char[TailleF];
if(!BuffF)  { sprintf(msg5000, "Echec allocation buffer de %d octets", TailleF);
			   XML_ParserFree(p);	fclose(F); return false;	}

if(fread(BuffF, 1, TailleF, F)!= TailleF)
	{ sprintf(msg5000, "Echec lecture %s", Nf);
	delete BuffF;	XML_ParserFree(p);	fclose(F); return false;	}

//lancement du parsing
if (! XML_Parse(p, BuffF, TailleF, 0))
	{ sprintf(msg5000, "Parse error at line %d:\n%s",
		  XML_GetCurrentLineNumber(p), XML_ErrorString(XML_GetErrorCode(p)));
	delete BuffF;	XML_ParserFree(p);	fclose(F); return false;	}
                                                                    
delete BuffF;	XML_ParserFree(p);	fclose(F);
return true;
}
