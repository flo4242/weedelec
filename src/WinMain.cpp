#include <Windows.h>

#include "drvtraiti.h"


int PASCAL WinMain(HINSTANCE hinst, HINSTANCE,  LPSTR, int)
/***************************************************/
{
DisableProcessWindowsGhosting(); //// JUILLET 2013 : maintien des affichages lors des calculs longs !!!

   DRVTRAITIM *  pDriver = GetTraitimDriver(hinst);
   if(!pDriver)	return 0;
   if(!pDriver->OK())  	{    delete pDriver;    return 0;      }

   MSG msg;
   do   {
	  ::GetMessage(&msg, NULL, 0,0); ::DispatchMessage(&msg);
   }while(!pDriver->FermetureDemandee());


  delete pDriver;
  return 0;
}
