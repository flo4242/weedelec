

#include "MainWeedelec.h"

using namespace std;




TMainWeedelec::TMainWeedelec(HINSTANCE hinst)
/****************************************/
: DRVTRAITIM("Projet WEEDELEC - IRSTEA 2019", hinst, true, "VS_TraitimAPI.dll", "ListeOutilsWeedelec.txt")
{
	HMENU Pere = GetMenu(HWindow());
	AjouteListeItems(Pere,
		"A propos", &TMainWeedelec::CMAPropos,
		NULL);
	DrawMenuBar(HWindow());
	
}



DRVTRAITIM *  GetTraitimDriver(HINSTANCE hinst)
/*********************************************/
{
	return new TMainWeedelec(hinst);
}







