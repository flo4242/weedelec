/***
*
* 
* erxComInt.h
* This is the communication interface with ecorobotix's p1 robot.  
*
****/
#pragma once

#include <map>
#include <string>
#include <atlbase.h>

#include <cpprest/streams.h>
#include <cpprest/astreambuf.h>
#include <cpprest/containerstream.h>
#include <cpprest/ws_client.h>
#include <cpprest/json.h>
#include <cpprest/asyncrt_utils.h>

#ifdef _WIN32
# define ERXEXPORTLIB __declspec( dllexport )
#else
# define ERXEXPORTLIB 
#endif

class ErxComManager
{
	public:
		ERXEXPORTLIB ErxComManager();

            /**
             * @brief      { Used to connect to the robot.
             * 				Must be called before atempting any communication with robot. }
             * @param[in]	ip_interface { ip adress of the robot to connect to including port number }
             */
		ERXEXPORTLIB void connect(utility::string_t ip_interface);

            /**
             * @brief      { Close connection to robot. }
             *
             */
		ERXEXPORTLIB void close();

            /**
             * @brief      { Connection status }
             *
             * @return     { True if connected, false otherwise. }
             */
		ERXEXPORTLIB bool isConnected();

            /**
             * @brief      { Can be called to create new connection with robot.}
             */
		ERXEXPORTLIB void reset();

            /**
             * @brief		{ Sets the robot wheel speed. This can be used to 
             * 				make a joystick }
             *
             * @param[in]  leftS   The left wheel speed [m/s]
             * @param[in]  rightS  The right wheel speed [m/s]
             */
		ERXEXPORTLIB void setSpeed(float leftS, float rightS);

            /**
             * @brief      { The robot moves in a straight line for the specified distance. }
             *
             * @param[in]	dist	{ The distance to move in a straight line [m/s] }
             */
		ERXEXPORTLIB void moveStraight(double dist);

            /**
             * @brief      { rotates the robot of a given angle with the given radius.
             * 				if angle > 0 && radius > 0 the robot turns ACW moving forward
							if angle > 0 %% radius < 0 the robot truns ACW going backwards
							if angle < 0 && radius < 0 the robot turns CW moving forward
							if angle < 0 %% radius > 0 the robot truns CW going backwards}
             *
             * @param[in]  angle   { The angle }
             * @param[in]  radius  { The radius }
             */	
		ERXEXPORTLIB void rotateAngle(double angle, double radius);

			/**
			 * @brief		{Stops all current autonomous movement.}
			 *
			 */
		ERXEXPORTLIB void stopMovement();

            /**
             * @brief      Gets the position.
             *
             * @return		{ A json containing the current robot positions :
			 *				    {
			 *						"lat" : (double), // in degrees of latitude
			 *						"lon" : (double), // in degrees of longitude
			 *						"orient" : (double) // in degrees from the magnetic north
			 *					}
			 *				}
             */
		ERXEXPORTLIB web::json::value getPosition();
            
            /**
             * @brief      Returns the current robot status
             * 
             *
             * @return     A json contiaining the current robot status.
             * 				This json can take the form:
             *              {
             *                "mainStatus" : (bool), //currently not updated 
             *                "imgLog" : (bool) //returns if images are saved to the disk.
             *              }
             * 				
             */
		ERXEXPORTLIB web::json::value getStatus();

	private:
            
		web::experimental::web_sockets::client::websocket_client m_ws_client;
            // Utility functions
		ERXEXPORTLIB static pplx::task<void> async_do_while(std::function<pplx::task<bool>(void)> func);
		ERXEXPORTLIB static pplx::task<bool> _do_while_iteration(std::function<pplx::task<bool>(void)> func);
		ERXEXPORTLIB static pplx::task<bool> _do_while_impl(std::function<pplx::task<bool>(void)> func);
		ERXEXPORTLIB static web::json::value parseJson(const std::string& str_msg);
            /**
             * @brief       Establishes a websocket connection to a robot 
             *              handles the handshake protocol
             * @return      pplx task
             */
		ERXEXPORTLIB pplx::task<void> connectionHandshake(utility::string_t ip);
            
            /**
             * @brief       Close the websocket
             * @return 
             */
		ERXEXPORTLIB pplx::task<void> disconnect();
            
            /**
             * brief        creates a listening thread to monitor incoming
             *              messages coming through the open websocket
             */
		ERXEXPORTLIB void listenToIncomingMessage();
            
            /**
             * @brief       Send raw json command as string
             * @param		erx_command
             * @return 
             */
		ERXEXPORTLIB pplx::task<void> sendCommand(const std::string& erx_command);
            
            /**
             * @brief       Send formatted command
             * @param		erx_command
             * @return 
             */
		ERXEXPORTLIB pplx::task<void> sendCommand(web::json::value &erx_command);
            
            /**
             * @brief       Receive and decode command 
             * @param		robot_msg
             */
		ERXEXPORTLIB void receiveCommand(web::json::value robot_msg);
            
            /**
             * @brief       takes care of sending a position update request 
             */
		ERXEXPORTLIB void updatePosition();
            
            /**
             * @brief       takes care of sending a status update request
             */
		ERXEXPORTLIB void updateStatus();
            

		bool m_is_connected_ = false;

		web::json::value connection_status_;

		utility::string_t local_id_ = U("wlcModule");
		utility::string_t remote_peer_id_;

        //int local_Instance_ = 2018;
        utility::string_t remote_peer_instance_ ;

        int msg_num_ = 0;

        std::map<utility::string_t, utility::string_t> com_int_map_;
		utility::string_t current_com_int_;
            
        // Robot info to be updated via the communication channel
           
        web::json::value robot_status_;
        volatile bool is_robot_status_updated_ = false;
            
        web::json::value robot_positions_;
        volatile bool is_robot_position_updated_ = false;
            
        //web::json::value robot_sensors_;
};
