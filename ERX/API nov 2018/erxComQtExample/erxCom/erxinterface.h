#ifndef ERXINTERFACE_H
#define ERXINTERFACE_H

#include <QMainWindow>
#include <QTimer>
#include <C:/Users/IEUser/source/repos/liberxcom/erxCom.h>
#include <cpprest/json.h>

namespace Ui {
class erxInterface;
}

class erxInterface : public QMainWindow
{
    Q_OBJECT

public:
    explicit erxInterface(QWidget *parent = nullptr);
    ~erxInterface();

private slots:

    // connection buttons
    void on_interface_dropdown_currentIndexChanged(int index);

    void on_button_connect_clicked();

    void on_disconnect_button_clicked();

    // autonomous movements
    void on_stop_mov_clicked();

    void on_move_straight_clicked();

    void on_rotate_clicked();


    // Joystick
    void on_button_forward_clicked();

    void on_button_stop_clicked();

    void on_button_right_clicked();

    void on_button_back_clicked();

    void on_button_left_clicked();

    void speed_keep_alive();


    // update functions
    void update_connection_status();

    void update_robot_status();

    void update_robot_gps_pos();

private:
    Ui::erxInterface *ui;
    std::shared_ptr<ErxComManager> m_client_manager;
    web::json::value robotStatus;
    web::json::value robotPosition;
    utility::string_t connectionInterface = U("192.168.8.8:8080");

    void joystick_action(int dx, int dy, bool stop);

    float joystickSpeedX = 0, joystickSpeedY = 0;
    float joystickSpeedStep = 0.01f;
    float leftSpeed = 0.0f, rightSpeed = 0.0f;

    QTimer *update_timer = new QTimer(this);
    QTimer *speed_timer = new QTimer(this);
    QTimer *gps_pos_timer = new QTimer(this);
};

#endif // ERXINTERFACE_H
