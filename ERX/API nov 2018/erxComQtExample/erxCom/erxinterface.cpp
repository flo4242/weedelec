#include "ui_erxinterface.h"
#include "erxinterface.h"



erxInterface::erxInterface(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::erxInterface)
{
    ui->setupUi(this);
    m_client_manager = std::make_shared<ErxComManager>();

    connect(update_timer, SIGNAL(timeout()), this, SLOT(update_connection_status()));
    connect(update_timer, SIGNAL(timeout()), this, SLOT(update_robot_status()));
    connect(update_timer, SIGNAL(timeout()), this, SLOT(update_robot_gps_pos()));
    update_timer->start(500);

    connect(speed_timer, SIGNAL(timeout()), this, SLOT(speed_keep_alive()));
}

erxInterface::~erxInterface()
{
    delete ui;
}

void erxInterface::on_interface_dropdown_currentIndexChanged(int index)
{
    switch(index){
    //WLAN
    case 0:
        ui->connectionAdress->setText("192.168.8.8:8080");
        //connection_interface = U("192.168.8.8:8080");
        break;
    case 1:
        ui->connectionAdress->setText("set fixed eth ip");
        //connection_interface = U("192.168.0.0:8080");
        break;
    default:
        break;
    }
}

void erxInterface::on_button_connect_clicked()
{
    QString conAdr = ui->connectionAdress->toPlainText();
    std::cout<<"connection adress is : "<<conAdr.toStdString()<<std::endl;
    connectionInterface = utility::conversions::to_string_t(conAdr.toStdString());

    m_client_manager->connect(connectionInterface);
    if(m_client_manager->isConnected()){
        ui->connection_status->setText("Connected");
    }else{
        ui->connection_status->setText("Disconnected");
    }
}

void erxInterface::update_connection_status(){
    if(m_client_manager->isConnected()){
        ui->connection_status->setText("Connected");
    }else{
        ui->connection_status->setText("Disconnected");
    }
}

void erxInterface::update_robot_status(){

    if(m_client_manager->isConnected()){
       robotStatus = m_client_manager->getStatus();
       QString imgLogSta = robotStatus[U("imgLog")].as_bool() ? "true" : "false";
       ui->img_log_status->setText(imgLogSta);

       QString robotSta = robotStatus[U("mainStatus")].as_bool() ? "Running" : "Error";
       ui->robot_main_status->setText(robotSta);
    }
}

void erxInterface::update_robot_gps_pos(){
    if(m_client_manager->isConnected()){
        robotPosition = m_client_manager->getPosition();
        double lat = robotPosition[U("lat")].as_double();
        double lon = robotPosition[U("lon")].as_double();
        double orient = robotPosition[U("orient")].as_double();

        ui->lat->setText(QString::number(lat));
        ui->lon->setText(QString::number(lon));
        ui->orient->setText(QString::number(orient));
    }
}

void erxInterface::on_stop_mov_clicked()
{
    m_client_manager->stopMovement();
}

void erxInterface::on_move_straight_clicked()
{
    QString dist = ui->ms_dist->toPlainText();

    m_client_manager->moveStraight(dist.toDouble());
}

void erxInterface::on_rotate_clicked()
{
    QString angle = ui->rot_ang->toPlainText();
    QString radi = ui->rot_radi->toPlainText();

    m_client_manager->rotateAngle(angle.toDouble(), radi.toDouble());
}

void erxInterface::on_button_forward_clicked()
{
    joystick_action(0, 1, false);
}

void erxInterface::on_button_stop_clicked()
{
    joystick_action(0, 0, true);
}

void erxInterface::on_button_right_clicked()
{
    joystick_action(1, 0, false);
}

void erxInterface::on_button_left_clicked()
{
    joystick_action(-1, 0, false);
}

void erxInterface::on_button_back_clicked()
{
    joystick_action(0, -1, false);
}

void erxInterface::on_disconnect_button_clicked()
{
    m_client_manager->close();
}

void erxInterface::joystick_action(int dx, int dy, bool stop){
    if (stop) {
        joystickSpeedX = 0;
        joystickSpeedY = 0;
        speed_timer->stop();
    }else {
        joystickSpeedX += dx * joystickSpeedStep;
        joystickSpeedY += dy * joystickSpeedStep;
        speed_timer->start(500);
    }
    leftSpeed = joystickSpeedY + joystickSpeedX;
    rightSpeed = joystickSpeedY - joystickSpeedX;

    m_client_manager->setSpeed(leftSpeed, rightSpeed);
}

void erxInterface::speed_keep_alive(){
    m_client_manager->setSpeed(leftSpeed, rightSpeed);
}
