/****************************************************************************
** Meta object code from reading C++ file 'erxinterface.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../erxCom/erxinterface.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'erxinterface.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_erxInterface_t {
    QByteArrayData data[18];
    char stringdata0[382];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_erxInterface_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_erxInterface_t qt_meta_stringdata_erxInterface = {
    {
QT_MOC_LITERAL(0, 0, 12), // "erxInterface"
QT_MOC_LITERAL(1, 13, 41), // "on_interface_dropdown_current..."
QT_MOC_LITERAL(2, 55, 0), // ""
QT_MOC_LITERAL(3, 56, 5), // "index"
QT_MOC_LITERAL(4, 62, 25), // "on_button_connect_clicked"
QT_MOC_LITERAL(5, 88, 28), // "on_disconnect_button_clicked"
QT_MOC_LITERAL(6, 117, 19), // "on_stop_mov_clicked"
QT_MOC_LITERAL(7, 137, 24), // "on_move_straight_clicked"
QT_MOC_LITERAL(8, 162, 17), // "on_rotate_clicked"
QT_MOC_LITERAL(9, 180, 25), // "on_button_forward_clicked"
QT_MOC_LITERAL(10, 206, 22), // "on_button_stop_clicked"
QT_MOC_LITERAL(11, 229, 23), // "on_button_right_clicked"
QT_MOC_LITERAL(12, 253, 22), // "on_button_back_clicked"
QT_MOC_LITERAL(13, 276, 22), // "on_button_left_clicked"
QT_MOC_LITERAL(14, 299, 16), // "speed_keep_alive"
QT_MOC_LITERAL(15, 316, 24), // "update_connection_status"
QT_MOC_LITERAL(16, 341, 19), // "update_robot_status"
QT_MOC_LITERAL(17, 361, 20) // "update_robot_gps_pos"

    },
    "erxInterface\0on_interface_dropdown_currentIndexChanged\0"
    "\0index\0on_button_connect_clicked\0"
    "on_disconnect_button_clicked\0"
    "on_stop_mov_clicked\0on_move_straight_clicked\0"
    "on_rotate_clicked\0on_button_forward_clicked\0"
    "on_button_stop_clicked\0on_button_right_clicked\0"
    "on_button_back_clicked\0on_button_left_clicked\0"
    "speed_keep_alive\0update_connection_status\0"
    "update_robot_status\0update_robot_gps_pos"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_erxInterface[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   89,    2, 0x08 /* Private */,
       4,    0,   92,    2, 0x08 /* Private */,
       5,    0,   93,    2, 0x08 /* Private */,
       6,    0,   94,    2, 0x08 /* Private */,
       7,    0,   95,    2, 0x08 /* Private */,
       8,    0,   96,    2, 0x08 /* Private */,
       9,    0,   97,    2, 0x08 /* Private */,
      10,    0,   98,    2, 0x08 /* Private */,
      11,    0,   99,    2, 0x08 /* Private */,
      12,    0,  100,    2, 0x08 /* Private */,
      13,    0,  101,    2, 0x08 /* Private */,
      14,    0,  102,    2, 0x08 /* Private */,
      15,    0,  103,    2, 0x08 /* Private */,
      16,    0,  104,    2, 0x08 /* Private */,
      17,    0,  105,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void erxInterface::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        erxInterface *_t = static_cast<erxInterface *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_interface_dropdown_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_button_connect_clicked(); break;
        case 2: _t->on_disconnect_button_clicked(); break;
        case 3: _t->on_stop_mov_clicked(); break;
        case 4: _t->on_move_straight_clicked(); break;
        case 5: _t->on_rotate_clicked(); break;
        case 6: _t->on_button_forward_clicked(); break;
        case 7: _t->on_button_stop_clicked(); break;
        case 8: _t->on_button_right_clicked(); break;
        case 9: _t->on_button_back_clicked(); break;
        case 10: _t->on_button_left_clicked(); break;
        case 11: _t->speed_keep_alive(); break;
        case 12: _t->update_connection_status(); break;
        case 13: _t->update_robot_status(); break;
        case 14: _t->update_robot_gps_pos(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject erxInterface::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_erxInterface.data,
    qt_meta_data_erxInterface,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *erxInterface::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *erxInterface::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_erxInterface.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int erxInterface::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 15;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
